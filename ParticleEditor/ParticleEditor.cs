﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using ParticleEngine;
using PSEditor.EditorUI;
using I_XNAUtility;

namespace PSEditor
{
    public class ParticleEditor
    {
        GraphicsDevice m_Device;

        ContentManager m_Content;

        ParticleSettingsUI m_ParticleSettingsUI;

        ParticleSystemWithEmitter m_ParticleSystem;

        public ParticleLocationData[] m_ParticleWorldLocation;
        public ParticleLocationData[] m_ParticleLocalLocation;

        public ContentManager Content { get { return m_Content; } }

        //int m_PSNameIndex = 0;

        const float UI_Y_PERCENTAGE = 0.55f;
        const float UI_X_PERCENTAGE = 0.02f;

        public bool IsEditorUIVisible { get { return m_ParticleSettingsUI.Visible; } }

        public ParticleEditor(GraphicsDevice gameGraphicsDevice, ContentManager gameContent)
        {
            Init(@"ParticleTextures\DefaultSettings.xml",  gameGraphicsDevice,  gameContent);
        }

        public ParticleEditor(GraphicsDevice gameGraphicsDevice, ContentManager gameContent, String FilePath)
        {
            Init(FilePath,  gameGraphicsDevice,  gameContent);           
        }

        void Init(string fileName, GraphicsDevice gameGraphicsDevice, ContentManager gameContent)
        {
            m_Device = gameGraphicsDevice;
            m_Content = gameContent;

            m_ParticleWorldLocation = new ParticleLocationData[1];
            m_ParticleLocalLocation = new ParticleLocationData[1];

            m_ParticleWorldLocation[0].WorldMatrix = Matrix.CreateRotationX(MathHelper.PiOver2);
            m_ParticleWorldLocation[0].emitterIndex = 0;
            m_ParticleLocalLocation[0].WorldMatrix = Matrix.CreateRotationX(MathHelper.PiOver2);
            m_ParticleLocalLocation[0].emitterIndex = 0;

            if (m_ParticleSystem != null)
                m_ParticleSystem.Dispose();
            m_ParticleSystem = new ParticleSystemWithEmitter(m_Content, m_Device, Utility.GetFullPath(fileName), m_ParticleWorldLocation);

            if (m_ParticleSettingsUI != null)
                m_ParticleSettingsUI.Dispose();
            m_ParticleSettingsUI = new ParticleSettingsUI();

            m_ParticleSettingsUI.LinkParticleEditor(this);

            //ParticleSettingsUI.ParticleName = @"ParticleTextures\DefaultSettings.xml";
        }

        int ccount = 0;

        /// <summary>
        /// 更新ParticleEditor
        /// </summary>
        public void Update(float gameTime)
        {
            if (m_ParticleSettingsUI != null && ccount == 0)
            {
                ccount++;
                m_ParticleSettingsUI.FormClosing += UIClosing;
            }

            if (m_ParticleSettingsUI == null && ccount != 0)
                ccount = 0;

            if(m_ParticleSystem!=null)
            m_ParticleSystem.Update(gameTime);
        }


        /// <summary>
        /// ParticleEditor
        /// </summary>
        public void Draw(Matrix View, Matrix projection, Vector3 camPos)
        {
            if(m_ParticleSystem!=null)
            m_ParticleSystem.Draw(ref View, ref projection, ref camPos);
        }


        #region Methods

        //public void AddParticleSystem()
        //{
        //    m_PSNameIndex++;
        //    m_ParticleSystem = new ParticleSystemWithEmitter(m_Content, m_Device, @"XNBData\ParticleTextures\DefaultSettings.xml", m_ParticleWorldLocation);
        //}





        /// <summary>
        /// 關閉表單會觸發的事件
        /// </summary>
        public void UIClosing(object sender, EventArgs e)
        {
            m_ParticleSettingsUI.UnLinkParticleEditor();

            m_ParticleSystem.Dispose();
            m_ParticleSystem = null;
        }


        /// <summary>
        /// Show設定particle的表單
        /// </summary>
        public void ShowParticleSettingsUI( ParticleSettingsUI.SaveLoadParticleFileName delSave, ParticleSettingsUI.SaveLoadParticleFileName delLoad )
        {
            if (m_ParticleSettingsUI.IsDisposed)
            {
                m_ParticleSettingsUI = new ParticleSettingsUI();
                m_ParticleSettingsUI.LinkParticleEditor(this);
            }

            if (m_ParticleSettingsUI.IsDisposed == false)
                m_ParticleSettingsUI.Show();

            m_ParticleSettingsUI.m_SaveXML = delSave;
            m_ParticleSettingsUI.m_LoadXML = delLoad;
        }


        ///// <summary>
        /// 回傳particle system的設定
        /// </summary>
        public InputSettings GetSelectedPSInputSettings()
        {
            return m_ParticleSystem.GetInputSettings();
        }


        /// <summary>
        /// copy目前的particle設定到指定的ParticleSettings class裡
        /// </summary>
        public void CopyInputSettingsTo(ref InputSettings settings)
        {
            InputSettings tempSettings = GetSelectedPSInputSettings();

            settings.particleSettings.TextureName = tempSettings.particleSettings.TextureName;
            settings.particleSettings.MaxParticles = tempSettings.particleSettings.MaxParticles;
            settings.particleSettings.Duration = tempSettings.particleSettings.Duration;
            settings.particleSettings.DurationRandomness = tempSettings.particleSettings.DurationRandomness;
            settings.particleSettings.EmitterVelocitySensitivity = tempSettings.particleSettings.EmitterVelocitySensitivity;

            settings.particleSettings.MinHorizontalVelocity = tempSettings.particleSettings.MinHorizontalVelocity;
            settings.particleSettings.MaxHorizontalVelocity = tempSettings.particleSettings.MaxHorizontalVelocity;

            settings.particleSettings.MinVerticalVelocity = tempSettings.particleSettings.MinVerticalVelocity;
            settings.particleSettings.MaxVerticalVelocity = tempSettings.particleSettings.MaxVerticalVelocity;

            settings.particleSettings.Gravity = tempSettings.particleSettings.Gravity;

            settings.particleSettings.EndVelocity = tempSettings.particleSettings.EndVelocity;

            settings.particleSettings.MinColor = tempSettings.particleSettings.MinColor;
            settings.particleSettings.MaxColor = tempSettings.particleSettings.MaxColor;

            settings.particleSettings.MinStartSize = tempSettings.particleSettings.MinStartSize;
            settings.particleSettings.MaxStartSize = tempSettings.particleSettings.MaxStartSize;

            settings.particleSettings.MinEndSize = tempSettings.particleSettings.MinEndSize;
            settings.particleSettings.MaxEndSize = tempSettings.particleSettings.MaxEndSize;

            settings.particleSettings.MinStartHeight = tempSettings.particleSettings.MinStartHeight;
            settings.particleSettings.MaxStartHeight = tempSettings.particleSettings.MaxStartHeight;

            settings.particleSettings.MinEndHeight = tempSettings.particleSettings.MinEndHeight;
            settings.particleSettings.MaxEndHeight = tempSettings.particleSettings.MaxEndHeight;

            settings.particleSettings.MinRotateSpeed = tempSettings.particleSettings.MinRotateSpeed;
            settings.particleSettings.MaxRotateSpeed = tempSettings.particleSettings.MaxRotateSpeed;

            settings.particleSettings.RotateOnce = tempSettings.particleSettings.RotateOnce;

            settings.particleSettings.UseCurveAlpha = tempSettings.particleSettings.UseCurveAlpha;
            settings.particleSettings.UseLocalWorld = tempSettings.particleSettings.UseLocalWorld;
            settings.particleSettings.DoNotChangeAlpha = tempSettings.particleSettings.DoNotChangeAlpha;

            settings.particleSettings.SourceBlend = tempSettings.particleSettings.SourceBlend;
            settings.particleSettings.DestinationBlend = tempSettings.particleSettings.DestinationBlend;

            settings.UseEmitter = tempSettings.UseEmitter;
            settings.UseDirectionParticle = tempSettings.UseDirectionParticle;
            settings.ParticlesPerSecond = tempSettings.ParticlesPerSecond;
            settings.particleSettings.StartVelocity = tempSettings.particleSettings.StartVelocity;


        }


        /// <summary>
        /// 根據目前的參數重新設定particle
        /// </summary>
        public void ResetInputSettings(InputSettings settings)
        {
            m_ParticleSystem.Dispose();
            m_ParticleSystem = new ParticleSystemWithEmitter(m_Content, m_Device, settings, m_ParticleWorldLocation);
        }


        public InputSettings LoadXMLSettings(string fileName)
        {
            return SettingReader.Read(fileName);
        }


        public void SaveXMLSettings(string fileName)
        {
            SettingWriter.Write(fileName, GetSelectedPSInputSettings());
        }


        public void UpdateBornLocation(Matrix worldMatrix)
        {
            if (m_ParticleSystem == null)
                return;
            if (m_ParticleSystem.m_Settings.particleSettings.UseLocalWorld)
            {
                m_ParticleSystem.UpdateLocalLocationData(m_ParticleLocalLocation, worldMatrix, 1);
            }
            else
            {
                m_ParticleWorldLocation[0].WorldMatrix = m_ParticleLocalLocation[0].WorldMatrix * worldMatrix;
                m_ParticleSystem.UpdateWorldLocationData(m_ParticleWorldLocation, 1);
            }

        }

        #endregion
    }
}
