﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using ParticleEngine;
using I_XNAUtility;

namespace PSEditor.EditorUI
{
    public partial class ParticleSettingsUI : Form
    {
        ParticleEditor m_ParticleEditor;
        InputSettings m_InputSettings;

        public delegate void SaveLoadParticleFileName(string fileName);
        public SaveLoadParticleFileName m_SaveXML = null;
        public SaveLoadParticleFileName m_LoadXML = null;

        public ParticleSettingsUI()
        {
            InitializeComponent();
        }

        public void LinkParticleEditor(ParticleEditor particleEditor)
        {
            m_ParticleEditor = particleEditor;

            m_InputSettings = new InputSettings();

            m_ParticleEditor.CopyInputSettingsTo(ref m_InputSettings);

            DestinationBlend_comboBox.Items.Add(Blend.One.ToString());
            DestinationBlend_comboBox.Items.Add(Blend.InverseSourceAlpha.ToString());

            ShowParticleSettingToUI();
        }


        /// <summary>
        /// 把particle的參數show在ui上
        /// </summary>
        public void ShowParticleSettingToUI()
        {
            TextureName.Text = m_InputSettings.particleSettings.TextureName;
            MaxParticles_textBox.Text = m_InputSettings.particleSettings.MaxParticles.ToString();
            Duration_textBox.Text = m_InputSettings.particleSettings.Duration.ToString();
            DurationRandomness_textBox.Text = m_InputSettings.particleSettings.DurationRandomness.ToString();
            EmitterVelocitySensitivity_textBox.Text = m_InputSettings.particleSettings.EmitterVelocitySensitivity.ToString();

            MinHorizontalVelocity_textBox.Text = m_InputSettings.particleSettings.MinHorizontalVelocity.ToString();
            MaxHorizontalVelocity_textBox.Text = m_InputSettings.particleSettings.MaxHorizontalVelocity.ToString();

            MinVerticalVelocity_textBox.Text = m_InputSettings.particleSettings.MinVerticalVelocity.ToString();
            MaxVerticalVelocity_textBox.Text = m_InputSettings.particleSettings.MaxVerticalVelocity.ToString();

            GravityX_textBox.Text = m_InputSettings.particleSettings.Gravity.X.ToString();
            GravityY_textBox.Text = m_InputSettings.particleSettings.Gravity.Y.ToString();
            GravityZ_textBox.Text = m_InputSettings.particleSettings.Gravity.Z.ToString();

            EndVelocity_textBox.Text = m_InputSettings.particleSettings.EndVelocity.ToString();

            MinColorR_label.Text = m_InputSettings.particleSettings.MinColor.X.ToString();
            MinColorG_label.Text = m_InputSettings.particleSettings.MinColor.Y.ToString();
            MinColorB_label.Text = m_InputSettings.particleSettings.MinColor.Z.ToString();
            MinColorA_textBox.Text = m_InputSettings.particleSettings.MinColor.W.ToString();

            MaxColorR_label.Text = m_InputSettings.particleSettings.MaxColor.X.ToString();
            MaxColorG_label.Text = m_InputSettings.particleSettings.MaxColor.Y.ToString();
            MaxColorB_label.Text = m_InputSettings.particleSettings.MaxColor.Z.ToString();
            MaxColorA_textBox.Text = m_InputSettings.particleSettings.MaxColor.W.ToString();

            MinStartSize_textBox.Text = m_InputSettings.particleSettings.MinStartSize.ToString();
            MaxStartSize_textBox.Text = m_InputSettings.particleSettings.MaxStartSize.ToString();

            MinEndSize_textBox.Text = m_InputSettings.particleSettings.MinEndSize.ToString();
            MaxEndSize_textBox.Text = m_InputSettings.particleSettings.MaxEndSize.ToString();

            MinRotateSpeed_textBox.Text = m_InputSettings.particleSettings.MinRotateSpeed.ToString();
            MaxRotateSpeed_textBox.Text = m_InputSettings.particleSettings.MaxRotateSpeed.ToString();

            if (m_InputSettings.particleSettings.RotateOnce)
                RotateOnce_checkBox.Checked = true;
            else
                RotateOnce_checkBox.Checked = false;

            if (m_InputSettings.particleSettings.DestinationBlend == Blend.One)
                DestinationBlend_comboBox.SelectedIndex = 0;
            else
                DestinationBlend_comboBox.SelectedIndex = 1;

            if (m_InputSettings.particleSettings.DoNotChangeAlpha)
                DoNotChangeAlpha_checkBox.Checked = true;
            else
                DoNotChangeAlpha_checkBox.Checked = false;

            if (m_InputSettings.particleSettings.UseCurveAlpha)
                UseCurveAlpha_checkBox.Checked = true;
            else
                UseCurveAlpha_checkBox.Checked = false;

            if (m_InputSettings.particleSettings.UseLocalWorld)
                UseLocalWorld_checkBox.Checked = true;
            else
                UseLocalWorld_checkBox.Checked = false;

            if (m_InputSettings.UseEmitter)
                UseEmitter_checkBox.Checked = true;
            else
                UseEmitter_checkBox.Checked = false;

            BornPerSecond_textBox.Text = m_InputSettings.ParticlesPerSecond.ToString();
            StartVelocity_textBox.Text = m_InputSettings.particleSettings.StartVelocity.ToString();

            if (m_InputSettings.UseDirectionParticle)
            {
                QuadPS_radioButton.Checked = true;
                PointList_radioButton.Checked = false;
                Size_groupBox.Text = "QuadPS Size :";

                MinStartHeight_label.Visible = true;
                MinStartHeight_textBox.Visible = true;
                MaxStartHeight_label.Visible = true;
                MaxStartHeight_textBox.Visible = true;

                MinEndHeight_label.Visible = true;
                MinEndHeight_textBox.Visible = true;
                MaxEndHeight_label.Visible = true;
                MaxEndHeight_textBox.Visible = true;
            }
            else
            {
                QuadPS_radioButton.Checked = false;
                PointList_radioButton.Checked = true;
                Size_groupBox.Text = "PointListPS Size :";

                MinStartHeight_label.Visible = false;
                MinStartHeight_textBox.Visible = false;
                MaxStartHeight_label.Visible = false;
                MaxStartHeight_textBox.Visible = false;

                MinEndHeight_label.Visible = false;
                MinEndHeight_textBox.Visible = false;
                MaxEndHeight_label.Visible = false;
                MaxEndHeight_textBox.Visible = false;
            }

            MinStartHeight_textBox.Text = m_InputSettings.particleSettings.MinStartHeight.ToString();
            MaxStartHeight_textBox.Text = m_InputSettings.particleSettings.MaxStartHeight.ToString();
            MinEndHeight_textBox.Text = m_InputSettings.particleSettings.MinEndHeight.ToString();
            MaxEndHeight_textBox.Text = m_InputSettings.particleSettings.MaxEndHeight.ToString();
        }


        /// <summary>
        /// 把ui上的參數存入particle的設定
        /// </summary>
        public void SetTextBoxValueToParticleSetting()
        {
            m_InputSettings.particleSettings.MaxParticles = System.Convert.ToInt32(MaxParticles_textBox.Text);
            m_InputSettings.particleSettings.Duration = (float)System.Convert.ToDouble(Duration_textBox.Text);
            m_InputSettings.particleSettings.DurationRandomness = (float)System.Convert.ToDouble(DurationRandomness_textBox.Text);
            m_InputSettings.particleSettings.EmitterVelocitySensitivity = (float)System.Convert.ToDouble(EmitterVelocitySensitivity_textBox.Text);

            m_InputSettings.particleSettings.MinHorizontalVelocity = (float)System.Convert.ToDouble(MinHorizontalVelocity_textBox.Text);
            m_InputSettings.particleSettings.MaxHorizontalVelocity = (float)System.Convert.ToDouble(MaxHorizontalVelocity_textBox.Text);

            m_InputSettings.particleSettings.MinVerticalVelocity = (float)System.Convert.ToDouble(MinVerticalVelocity_textBox.Text);
            m_InputSettings.particleSettings.MaxVerticalVelocity = (float)System.Convert.ToDouble(MaxVerticalVelocity_textBox.Text);

            m_InputSettings.particleSettings.Gravity.X = (float)System.Convert.ToDouble(GravityX_textBox.Text);
            m_InputSettings.particleSettings.Gravity.Y = (float)System.Convert.ToDouble(GravityY_textBox.Text);
            m_InputSettings.particleSettings.Gravity.Z = (float)System.Convert.ToDouble(GravityZ_textBox.Text);

            m_InputSettings.particleSettings.EndVelocity = (float)System.Convert.ToDouble(EndVelocity_textBox.Text);

            m_InputSettings.particleSettings.MinColor.X = (float)System.Convert.ToDouble(MinColorR_label.Text);
            m_InputSettings.particleSettings.MinColor.Y = (float)System.Convert.ToDouble(MinColorG_label.Text);
            m_InputSettings.particleSettings.MinColor.Z = (float)System.Convert.ToDouble(MinColorB_label.Text);
            m_InputSettings.particleSettings.MinColor.W = (float)System.Convert.ToDouble(MinColorA_textBox.Text);

            m_InputSettings.particleSettings.MaxColor.X = (float)System.Convert.ToDouble(MaxColorR_label.Text);
            m_InputSettings.particleSettings.MaxColor.Y = (float)System.Convert.ToDouble(MaxColorG_label.Text);
            m_InputSettings.particleSettings.MaxColor.Z = (float)System.Convert.ToDouble(MaxColorB_label.Text);
            m_InputSettings.particleSettings.MaxColor.W = (float)System.Convert.ToDouble(MaxColorA_textBox.Text);

            m_InputSettings.particleSettings.MinStartSize = (float)System.Convert.ToDouble(MinStartSize_textBox.Text);
            m_InputSettings.particleSettings.MaxStartSize = (float)System.Convert.ToDouble(MaxStartSize_textBox.Text);

            m_InputSettings.particleSettings.MinEndSize = (float)System.Convert.ToDouble(MinEndSize_textBox.Text);
            m_InputSettings.particleSettings.MaxEndSize = (float)System.Convert.ToDouble(MaxEndSize_textBox.Text);

            m_InputSettings.particleSettings.MinStartHeight = (float)System.Convert.ToDouble(MinStartHeight_textBox.Text);
            m_InputSettings.particleSettings.MaxStartHeight = (float)System.Convert.ToDouble(MaxStartHeight_textBox.Text);

            m_InputSettings.particleSettings.MinEndHeight = (float)System.Convert.ToDouble(MinEndHeight_textBox.Text);
            m_InputSettings.particleSettings.MaxEndHeight = (float)System.Convert.ToDouble(MaxEndHeight_textBox.Text);

            m_InputSettings.particleSettings.MinRotateSpeed = (float)System.Convert.ToDouble(MinRotateSpeed_textBox.Text);
            m_InputSettings.particleSettings.MaxRotateSpeed = (float)System.Convert.ToDouble(MaxRotateSpeed_textBox.Text);

            if (RotateOnce_checkBox.Checked)
                m_InputSettings.particleSettings.RotateOnce = true;
            else
                m_InputSettings.particleSettings.RotateOnce = false;

            if (DestinationBlend_comboBox.SelectedItem.ToString() == "InverseSourceAlpha")
                m_InputSettings.particleSettings.DestinationBlend = Blend.InverseSourceAlpha;
            else
                m_InputSettings.particleSettings.DestinationBlend = Blend.One;

            if (DoNotChangeAlpha_checkBox.Checked)
                m_InputSettings.particleSettings.DoNotChangeAlpha = true;
            else
                m_InputSettings.particleSettings.DoNotChangeAlpha = false;

            if (UseCurveAlpha_checkBox.Checked)
                m_InputSettings.particleSettings.UseCurveAlpha = true;
            else
                m_InputSettings.particleSettings.UseCurveAlpha = false;

            if (UseLocalWorld_checkBox.Checked)
                m_InputSettings.particleSettings.UseLocalWorld = true;
            else
                m_InputSettings.particleSettings.UseLocalWorld = false;

            if (UseEmitter_checkBox.Checked)
                m_InputSettings.UseEmitter = true;
            else
                m_InputSettings.UseEmitter = false;

            m_InputSettings.ParticlesPerSecond = System.Convert.ToInt32(BornPerSecond_textBox.Text);
            m_InputSettings.particleSettings.StartVelocity = (float)System.Convert.ToDouble(StartVelocity_textBox.Text);

            if (QuadPS_radioButton.Checked)
                m_InputSettings.UseDirectionParticle = true;
            else
                m_InputSettings.UseDirectionParticle = false;
        }


        public InputSettings GetBasicParticleSystemSetting()
        {
            return m_InputSettings;
        }


        public void UnLinkParticleEditor()
        {
            m_ParticleEditor = null;
        }


        public void LoadXNBTexture(string fileFullName)
        {
            Cursor = Cursors.WaitCursor;

            //去除副檔名，因為content會自己加入.xnb在檔名後面
            //int fileNameEndLocation = fileName.IndexOf('.');
            //string XNBfileName = fileName.Substring(0, fileNameEndLocation);

            fileFullName = Utility.GetOppositeFilePath(fileFullName);

            m_InputSettings.particleSettings.TextureName = fileFullName;

            TextureName.Text = fileFullName;

            Cursor = Cursors.Arrow;
        }


        public void LoadXMLSettings(string fileFullName)
        {
            Cursor = Cursors.WaitCursor;

            m_InputSettings = m_ParticleEditor.LoadXMLSettings(fileFullName);

            m_ParticleEditor.ResetInputSettings(m_InputSettings);

            ShowParticleSettingToUI();

            Cursor = Cursors.Arrow;
        }


        private void LoadTexture_button_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();

            // Default to the directory which contains our content files.
            string assemblyLocation = Assembly.GetExecutingAssembly().Location;
            string relativePath = Path.GetDirectoryName(assemblyLocation);
            string contentPath = Path.GetFullPath(relativePath + "\\ParticleTextures");

            fileDialog.InitialDirectory = contentPath;

            fileDialog.Title = "Load Texture";

            fileDialog.Filter = "Texture Files (*.png;*.tga;*.jpg;*.dds)|*.png;*.tga;*.jpg;*.dds|"
                + "png Files (*.png)|*.png|"
                + "tga Files (*.tga)|*.tga|"
                + "jpg Files (*.jpg)|*.jpg|"
                + "dds Files (*.dds)|*.dds|"
                + "All Files (*.*)|*.*";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadXNBTexture(fileDialog.FileName);
            }
        }


        private void Reset_button_Click(object sender, EventArgs e)
        {
            SetTextBoxValueToParticleSetting();

            m_ParticleEditor.ResetInputSettings(m_InputSettings);
        }


        private void LoadSettings_button_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();

            // Default to the directory which contains our content files.
            string assemblyLocation = Utility.WorkingDirectory; //Assembly.GetExecutingAssembly().Location;
            string relativePath = Path.GetDirectoryName(assemblyLocation);
            string contentPath = Path.GetFullPath(relativePath);

            fileDialog.InitialDirectory = contentPath;

            fileDialog.Title = "LoadXMLSettings";

            fileDialog.Filter = "ParticleSettings Files (*.xml)|*.xml|" + "All Files (*.*)|*.*";

            string ParticleName = null;
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                if (fileDialog.FileName.Contains(fileDialog.InitialDirectory))
                {
                    ParticleName = fileDialog.FileName.Remove(0, fileDialog.InitialDirectory.Length);
                }
                else
                {
                    ParticleName = fileDialog.FileName;
                }

                LoadXMLSettings(fileDialog.FileName);
            }

            if (m_LoadXML != null)
                m_LoadXML(ParticleName);
        }


        private void SaveSettings_button_Click(object sender, EventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();

            string assemblyLocation = Utility.WorkingDirectory; //Assembly.GetExecutingAssembly().Location;
            string relativePath = Path.GetDirectoryName(assemblyLocation);
            string contentPath = Path.GetFullPath(relativePath);

            fileDialog.InitialDirectory = contentPath;

            fileDialog.Filter = "Texture Files (*.xml)|*.xml|" + "All Files (*.*)|*.*";

            string ParticleName = null;
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                if (fileDialog.FileName.Contains(fileDialog.InitialDirectory))
                {
                    ParticleName = fileDialog.FileName.Remove(0, fileDialog.InitialDirectory.Length);
                }
                else
                {
                    ParticleName = fileDialog.FileName;
                }
                m_ParticleEditor.SaveXMLSettings(fileDialog.FileName);
            }

            if (m_SaveXML != null)
                m_SaveXML(ParticleName);
        }


        private void ChangeMinColor_button_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                MinColorR_label.Text = colorDialog.Color.R.ToString();
                MinColorG_label.Text = colorDialog.Color.G.ToString();
                MinColorB_label.Text = colorDialog.Color.B.ToString();

                m_InputSettings.particleSettings.MinColor.X = colorDialog.Color.R;
                m_InputSettings.particleSettings.MinColor.Y = colorDialog.Color.G;
                m_InputSettings.particleSettings.MinColor.Z = colorDialog.Color.B;
            }
        }


        private void ChangeMaxColor_button_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                MaxColorR_label.Text = colorDialog.Color.R.ToString();
                MaxColorG_label.Text = colorDialog.Color.G.ToString();
                MaxColorB_label.Text = colorDialog.Color.B.ToString();

                m_InputSettings.particleSettings.MaxColor.X = colorDialog.Color.R;
                m_InputSettings.particleSettings.MaxColor.Y = colorDialog.Color.G;
                m_InputSettings.particleSettings.MaxColor.Z = colorDialog.Color.B;
            }
        }

        private void QuadPS_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            Size_groupBox.Text = "QuadPS Size :";

            MinStartHeight_label.Visible = true;
            MinStartHeight_textBox.Visible = true;
            MaxStartHeight_label.Visible = true;
            MaxStartHeight_textBox.Visible = true;

            MinEndHeight_label.Visible = true;
            MinEndHeight_textBox.Visible = true;
            MaxEndHeight_label.Visible = true;
            MaxEndHeight_textBox.Visible = true;

            MinStartHeight_textBox.Text = m_InputSettings.particleSettings.MinStartHeight.ToString();
            MaxStartHeight_textBox.Text = m_InputSettings.particleSettings.MaxStartHeight.ToString();
            MinEndHeight_textBox.Text = m_InputSettings.particleSettings.MinEndHeight.ToString();
            MaxEndHeight_textBox.Text = m_InputSettings.particleSettings.MaxEndHeight.ToString();
        }

        private void PointList_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            Size_groupBox.Text = "PointListPS Size :";

            MinStartHeight_label.Visible = false;
            MinStartHeight_textBox.Visible = false;
            MaxStartHeight_label.Visible = false;
            MaxStartHeight_textBox.Visible = false;

            MinEndHeight_label.Visible = false;
            MinEndHeight_textBox.Visible = false;
            MaxEndHeight_label.Visible = false;
            MaxEndHeight_textBox.Visible = false;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ParticleSettingsUI_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

    }
}
