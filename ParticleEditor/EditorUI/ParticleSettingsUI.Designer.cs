﻿namespace PSEditor.EditorUI
{
    partial class ParticleSettingsUI
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.Reset_button = new System.Windows.Forms.Button();
            this.LoadSettings_button = new System.Windows.Forms.Button();
            this.SaveSettings_button = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Settings_groupBox = new System.Windows.Forms.GroupBox();
            this.UseCurveAlpha_checkBox = new System.Windows.Forms.CheckBox();
            this.DoNotChangeAlpha_checkBox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RotateOnce_checkBox = new System.Windows.Forms.CheckBox();
            this.MinRotateSpeed_textBox = new System.Windows.Forms.TextBox();
            this.MinRotateSpeed_label = new System.Windows.Forms.Label();
            this.MaxRotateSpeed_label_label = new System.Windows.Forms.Label();
            this.MaxRotateSpeed_textBox = new System.Windows.Forms.TextBox();
            this.DestinationBlend_comboBox = new System.Windows.Forms.ComboBox();
            this.DestinationBlend_label = new System.Windows.Forms.Label();
            this.Number_groupBox = new System.Windows.Forms.GroupBox();
            this.BornPerSecond_textBox = new System.Windows.Forms.TextBox();
            this.MaxParticles_label = new System.Windows.Forms.Label();
            this.MaxParticles_textBox = new System.Windows.Forms.TextBox();
            this.BornPerSecond_label = new System.Windows.Forms.Label();
            this.Size_groupBox = new System.Windows.Forms.GroupBox();
            this.MaxEndHeight_label = new System.Windows.Forms.Label();
            this.MaxStartHeight_label = new System.Windows.Forms.Label();
            this.MaxEndHeight_textBox = new System.Windows.Forms.TextBox();
            this.MaxStartHeight_textBox = new System.Windows.Forms.TextBox();
            this.MinEndHeight_textBox = new System.Windows.Forms.TextBox();
            this.MinStartHeight_textBox = new System.Windows.Forms.TextBox();
            this.MinEndHeight_label = new System.Windows.Forms.Label();
            this.MinStartHeight_label = new System.Windows.Forms.Label();
            this.MaxEndSize_label = new System.Windows.Forms.Label();
            this.MaxStartSize_label = new System.Windows.Forms.Label();
            this.MinEndSize_label = new System.Windows.Forms.Label();
            this.MinStartSize_label = new System.Windows.Forms.Label();
            this.MaxStartSize_textBox = new System.Windows.Forms.TextBox();
            this.MinStartSize_textBox = new System.Windows.Forms.TextBox();
            this.MaxEndSize_textBox = new System.Windows.Forms.TextBox();
            this.MinEndSize_textBox = new System.Windows.Forms.TextBox();
            this.PSType_groupBox = new System.Windows.Forms.GroupBox();
            this.QuadPS_radioButton = new System.Windows.Forms.RadioButton();
            this.PointList_radioButton = new System.Windows.Forms.RadioButton();
            this.Duration_groupBox = new System.Windows.Forms.GroupBox();
            this.Duration_label = new System.Windows.Forms.Label();
            this.Duration_textBox = new System.Windows.Forms.TextBox();
            this.DurationRandomness_label = new System.Windows.Forms.Label();
            this.DurationRandomness_textBox = new System.Windows.Forms.TextBox();
            this.Velocity_groupBox = new System.Windows.Forms.GroupBox();
            this.StartVelocity_textBox = new System.Windows.Forms.TextBox();
            this.EndVelocity_textBox = new System.Windows.Forms.TextBox();
            this.StartVelocity_label = new System.Windows.Forms.Label();
            this.EndVelocity_label = new System.Windows.Forms.Label();
            this.MaxVerticalVelocity_textBox = new System.Windows.Forms.TextBox();
            this.MaxVerticalVelocity_label = new System.Windows.Forms.Label();
            this.MinVerticalVelocity_textBox = new System.Windows.Forms.TextBox();
            this.MinVerticalVelocity_label = new System.Windows.Forms.Label();
            this.MaxHorizontalVelocity_textBox = new System.Windows.Forms.TextBox();
            this.MaxHorizontalVelocity_label = new System.Windows.Forms.Label();
            this.MinHorizontalVelocity_textBox = new System.Windows.Forms.TextBox();
            this.MinHorizontalVelocity_label = new System.Windows.Forms.Label();
            this.EmitterVelocitySensitivity_textBox = new System.Windows.Forms.TextBox();
            this.EmitterVelocitySensitivity_label = new System.Windows.Forms.Label();
            this.BornColor_groupBox = new System.Windows.Forms.GroupBox();
            this.MaxColorA_textBox = new System.Windows.Forms.TextBox();
            this.MinColorA_textBox = new System.Windows.Forms.TextBox();
            this.ChangeMaxColor_button = new System.Windows.Forms.Button();
            this.MaxColor_label = new System.Windows.Forms.Label();
            this.MaxColorB_label = new System.Windows.Forms.Label();
            this.MinColorB_label = new System.Windows.Forms.Label();
            this.MaxColorG_label = new System.Windows.Forms.Label();
            this.MaxColorR_label = new System.Windows.Forms.Label();
            this.MinColorG_label = new System.Windows.Forms.Label();
            this.MaxColorBTitle_label = new System.Windows.Forms.Label();
            this.MinColorR_label = new System.Windows.Forms.Label();
            this.MaxColorGTitle_label = new System.Windows.Forms.Label();
            this.MaxColorATitle_label = new System.Windows.Forms.Label();
            this.MinColorATitle_label = new System.Windows.Forms.Label();
            this.MinColorBTitle_label = new System.Windows.Forms.Label();
            this.MinColorGTitle_label = new System.Windows.Forms.Label();
            this.MaxColorRTitle_label = new System.Windows.Forms.Label();
            this.MinColorRTitle_label = new System.Windows.Forms.Label();
            this.ChangeMinColor_button = new System.Windows.Forms.Button();
            this.MinColor_label = new System.Windows.Forms.Label();
            this.Gravity_groupBox = new System.Windows.Forms.GroupBox();
            this.GravityZ_textBox = new System.Windows.Forms.TextBox();
            this.GravityY_textBox = new System.Windows.Forms.TextBox();
            this.GravityX_textBox = new System.Windows.Forms.TextBox();
            this.Gravity_label_Z = new System.Windows.Forms.Label();
            this.Gravity_label_Y = new System.Windows.Forms.Label();
            this.Gravity_label_X = new System.Windows.Forms.Label();
            this.UseEmitter_checkBox = new System.Windows.Forms.CheckBox();
            this.UseLocalWorld_checkBox = new System.Windows.Forms.CheckBox();
            this.LoadTexture_button = new System.Windows.Forms.Button();
            this.TextureName = new System.Windows.Forms.Label();
            this.TextureName_label = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.Settings_groupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.Number_groupBox.SuspendLayout();
            this.Size_groupBox.SuspendLayout();
            this.PSType_groupBox.SuspendLayout();
            this.Duration_groupBox.SuspendLayout();
            this.Velocity_groupBox.SuspendLayout();
            this.BornColor_groupBox.SuspendLayout();
            this.Gravity_groupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // Reset_button
            // 
            this.Reset_button.Location = new System.Drawing.Point(30, 479);
            this.Reset_button.Name = "Reset_button";
            this.Reset_button.Size = new System.Drawing.Size(373, 29);
            this.Reset_button.TabIndex = 7;
            this.Reset_button.Text = "Reset";
            this.Reset_button.UseVisualStyleBackColor = true;
            this.Reset_button.Click += new System.EventHandler(this.Reset_button_Click);
            // 
            // LoadSettings_button
            // 
            this.LoadSettings_button.Location = new System.Drawing.Point(30, 21);
            this.LoadSettings_button.Name = "LoadSettings_button";
            this.LoadSettings_button.Size = new System.Drawing.Size(104, 21);
            this.LoadSettings_button.TabIndex = 8;
            this.LoadSettings_button.Text = "LoadSettings";
            this.LoadSettings_button.UseVisualStyleBackColor = true;
            this.LoadSettings_button.Click += new System.EventHandler(this.LoadSettings_button_Click);
            // 
            // SaveSettings_button
            // 
            this.SaveSettings_button.Location = new System.Drawing.Point(298, 21);
            this.SaveSettings_button.Name = "SaveSettings_button";
            this.SaveSettings_button.Size = new System.Drawing.Size(104, 21);
            this.SaveSettings_button.TabIndex = 9;
            this.SaveSettings_button.Text = "SaveSettings";
            this.SaveSettings_button.UseVisualStyleBackColor = true;
            this.SaveSettings_button.Click += new System.EventHandler(this.SaveSettings_button_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.Settings_groupBox);
            this.panel1.Location = new System.Drawing.Point(30, 65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(404, 408);
            this.panel1.TabIndex = 10;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Settings_groupBox
            // 
            this.Settings_groupBox.Controls.Add(this.UseCurveAlpha_checkBox);
            this.Settings_groupBox.Controls.Add(this.DoNotChangeAlpha_checkBox);
            this.Settings_groupBox.Controls.Add(this.groupBox3);
            this.Settings_groupBox.Controls.Add(this.DestinationBlend_comboBox);
            this.Settings_groupBox.Controls.Add(this.DestinationBlend_label);
            this.Settings_groupBox.Controls.Add(this.Number_groupBox);
            this.Settings_groupBox.Controls.Add(this.Size_groupBox);
            this.Settings_groupBox.Controls.Add(this.PSType_groupBox);
            this.Settings_groupBox.Controls.Add(this.Duration_groupBox);
            this.Settings_groupBox.Controls.Add(this.Velocity_groupBox);
            this.Settings_groupBox.Controls.Add(this.BornColor_groupBox);
            this.Settings_groupBox.Controls.Add(this.Gravity_groupBox);
            this.Settings_groupBox.Controls.Add(this.UseEmitter_checkBox);
            this.Settings_groupBox.Controls.Add(this.UseLocalWorld_checkBox);
            this.Settings_groupBox.Controls.Add(this.LoadTexture_button);
            this.Settings_groupBox.Controls.Add(this.TextureName);
            this.Settings_groupBox.Controls.Add(this.TextureName_label);
            this.Settings_groupBox.Location = new System.Drawing.Point(13, 13);
            this.Settings_groupBox.Name = "Settings_groupBox";
            this.Settings_groupBox.Size = new System.Drawing.Size(360, 1139);
            this.Settings_groupBox.TabIndex = 7;
            this.Settings_groupBox.TabStop = false;
            this.Settings_groupBox.Text = "Settings";
            // 
            // UseCurveAlpha_checkBox
            // 
            this.UseCurveAlpha_checkBox.AutoSize = true;
            this.UseCurveAlpha_checkBox.Location = new System.Drawing.Point(221, 817);
            this.UseCurveAlpha_checkBox.Name = "UseCurveAlpha_checkBox";
            this.UseCurveAlpha_checkBox.Size = new System.Drawing.Size(98, 16);
            this.UseCurveAlpha_checkBox.TabIndex = 30;
            this.UseCurveAlpha_checkBox.Text = "UseCurveAlpha";
            this.UseCurveAlpha_checkBox.UseVisualStyleBackColor = true;
            // 
            // DoNotChangeAlpha_checkBox
            // 
            this.DoNotChangeAlpha_checkBox.AutoSize = true;
            this.DoNotChangeAlpha_checkBox.Location = new System.Drawing.Point(44, 817);
            this.DoNotChangeAlpha_checkBox.Name = "DoNotChangeAlpha_checkBox";
            this.DoNotChangeAlpha_checkBox.Size = new System.Drawing.Size(119, 16);
            this.DoNotChangeAlpha_checkBox.TabIndex = 29;
            this.DoNotChangeAlpha_checkBox.Text = "DoNotChangeAlpha";
            this.DoNotChangeAlpha_checkBox.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RotateOnce_checkBox);
            this.groupBox3.Controls.Add(this.MinRotateSpeed_textBox);
            this.groupBox3.Controls.Add(this.MinRotateSpeed_label);
            this.groupBox3.Controls.Add(this.MaxRotateSpeed_label_label);
            this.groupBox3.Controls.Add(this.MaxRotateSpeed_textBox);
            this.groupBox3.Location = new System.Drawing.Point(27, 466);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(308, 76);
            this.groupBox3.TabIndex = 56;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Rotate :";
            // 
            // RotateOnce_checkBox
            // 
            this.RotateOnce_checkBox.AutoSize = true;
            this.RotateOnce_checkBox.Location = new System.Drawing.Point(17, 31);
            this.RotateOnce_checkBox.Name = "RotateOnce_checkBox";
            this.RotateOnce_checkBox.Size = new System.Drawing.Size(78, 16);
            this.RotateOnce_checkBox.TabIndex = 44;
            this.RotateOnce_checkBox.Text = "RotateOnce";
            this.RotateOnce_checkBox.UseVisualStyleBackColor = true;
            // 
            // MinRotateSpeed_textBox
            // 
            this.MinRotateSpeed_textBox.Location = new System.Drawing.Point(227, 15);
            this.MinRotateSpeed_textBox.Name = "MinRotateSpeed_textBox";
            this.MinRotateSpeed_textBox.Size = new System.Drawing.Size(63, 22);
            this.MinRotateSpeed_textBox.TabIndex = 16;
            this.MinRotateSpeed_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MinRotateSpeed_label
            // 
            this.MinRotateSpeed_label.AutoSize = true;
            this.MinRotateSpeed_label.Location = new System.Drawing.Point(135, 18);
            this.MinRotateSpeed_label.Name = "MinRotateSpeed_label";
            this.MinRotateSpeed_label.Size = new System.Drawing.Size(88, 12);
            this.MinRotateSpeed_label.TabIndex = 43;
            this.MinRotateSpeed_label.Text = "MinRotateSpeed :";
            // 
            // MaxRotateSpeed_label_label
            // 
            this.MaxRotateSpeed_label_label.AutoSize = true;
            this.MaxRotateSpeed_label_label.Location = new System.Drawing.Point(135, 50);
            this.MaxRotateSpeed_label_label.Name = "MaxRotateSpeed_label_label";
            this.MaxRotateSpeed_label_label.Size = new System.Drawing.Size(90, 12);
            this.MaxRotateSpeed_label_label.TabIndex = 43;
            this.MaxRotateSpeed_label_label.Text = "MaxRotateSpeed :";
            // 
            // MaxRotateSpeed_textBox
            // 
            this.MaxRotateSpeed_textBox.Location = new System.Drawing.Point(227, 45);
            this.MaxRotateSpeed_textBox.Name = "MaxRotateSpeed_textBox";
            this.MaxRotateSpeed_textBox.Size = new System.Drawing.Size(63, 22);
            this.MaxRotateSpeed_textBox.TabIndex = 16;
            this.MaxRotateSpeed_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DestinationBlend_comboBox
            // 
            this.DestinationBlend_comboBox.FormattingEnabled = true;
            this.DestinationBlend_comboBox.Location = new System.Drawing.Point(145, 776);
            this.DestinationBlend_comboBox.Name = "DestinationBlend_comboBox";
            this.DestinationBlend_comboBox.Size = new System.Drawing.Size(174, 20);
            this.DestinationBlend_comboBox.TabIndex = 46;
            // 
            // DestinationBlend_label
            // 
            this.DestinationBlend_label.AutoSize = true;
            this.DestinationBlend_label.Location = new System.Drawing.Point(42, 782);
            this.DestinationBlend_label.Name = "DestinationBlend_label";
            this.DestinationBlend_label.Size = new System.Drawing.Size(91, 12);
            this.DestinationBlend_label.TabIndex = 47;
            this.DestinationBlend_label.Text = "DestinationBlend :";
            // 
            // Number_groupBox
            // 
            this.Number_groupBox.Controls.Add(this.BornPerSecond_textBox);
            this.Number_groupBox.Controls.Add(this.MaxParticles_label);
            this.Number_groupBox.Controls.Add(this.MaxParticles_textBox);
            this.Number_groupBox.Controls.Add(this.BornPerSecond_label);
            this.Number_groupBox.Location = new System.Drawing.Point(27, 51);
            this.Number_groupBox.Name = "Number_groupBox";
            this.Number_groupBox.Size = new System.Drawing.Size(308, 78);
            this.Number_groupBox.TabIndex = 54;
            this.Number_groupBox.TabStop = false;
            this.Number_groupBox.Text = "Number :";
            // 
            // BornPerSecond_textBox
            // 
            this.BornPerSecond_textBox.Location = new System.Drawing.Point(173, 49);
            this.BornPerSecond_textBox.Name = "BornPerSecond_textBox";
            this.BornPerSecond_textBox.Size = new System.Drawing.Size(63, 22);
            this.BornPerSecond_textBox.TabIndex = 27;
            this.BornPerSecond_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MaxParticles_label
            // 
            this.MaxParticles_label.AutoSize = true;
            this.MaxParticles_label.Location = new System.Drawing.Point(100, 24);
            this.MaxParticles_label.Name = "MaxParticles_label";
            this.MaxParticles_label.Size = new System.Drawing.Size(70, 12);
            this.MaxParticles_label.TabIndex = 3;
            this.MaxParticles_label.Text = "MaxParticles :";
            // 
            // MaxParticles_textBox
            // 
            this.MaxParticles_textBox.Location = new System.Drawing.Point(173, 21);
            this.MaxParticles_textBox.Name = "MaxParticles_textBox";
            this.MaxParticles_textBox.Size = new System.Drawing.Size(63, 22);
            this.MaxParticles_textBox.TabIndex = 4;
            this.MaxParticles_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BornPerSecond_label
            // 
            this.BornPerSecond_label.AutoSize = true;
            this.BornPerSecond_label.Location = new System.Drawing.Point(85, 53);
            this.BornPerSecond_label.Name = "BornPerSecond_label";
            this.BornPerSecond_label.Size = new System.Drawing.Size(84, 12);
            this.BornPerSecond_label.TabIndex = 48;
            this.BornPerSecond_label.Text = "BornPerSecond :";
            // 
            // Size_groupBox
            // 
            this.Size_groupBox.Controls.Add(this.MaxEndHeight_label);
            this.Size_groupBox.Controls.Add(this.MaxStartHeight_label);
            this.Size_groupBox.Controls.Add(this.MaxEndHeight_textBox);
            this.Size_groupBox.Controls.Add(this.MaxStartHeight_textBox);
            this.Size_groupBox.Controls.Add(this.MinEndHeight_textBox);
            this.Size_groupBox.Controls.Add(this.MinStartHeight_textBox);
            this.Size_groupBox.Controls.Add(this.MinEndHeight_label);
            this.Size_groupBox.Controls.Add(this.MinStartHeight_label);
            this.Size_groupBox.Controls.Add(this.MaxEndSize_label);
            this.Size_groupBox.Controls.Add(this.MaxStartSize_label);
            this.Size_groupBox.Controls.Add(this.MinEndSize_label);
            this.Size_groupBox.Controls.Add(this.MinStartSize_label);
            this.Size_groupBox.Controls.Add(this.MaxStartSize_textBox);
            this.Size_groupBox.Controls.Add(this.MinStartSize_textBox);
            this.Size_groupBox.Controls.Add(this.MaxEndSize_textBox);
            this.Size_groupBox.Controls.Add(this.MinEndSize_textBox);
            this.Size_groupBox.Location = new System.Drawing.Point(27, 943);
            this.Size_groupBox.Name = "Size_groupBox";
            this.Size_groupBox.Size = new System.Drawing.Size(308, 171);
            this.Size_groupBox.TabIndex = 53;
            this.Size_groupBox.TabStop = false;
            this.Size_groupBox.Text = "Size :";
            // 
            // MaxEndHeight_label
            // 
            this.MaxEndHeight_label.AutoSize = true;
            this.MaxEndHeight_label.Location = new System.Drawing.Point(155, 135);
            this.MaxEndHeight_label.Name = "MaxEndHeight_label";
            this.MaxEndHeight_label.Size = new System.Drawing.Size(91, 12);
            this.MaxEndHeight_label.TabIndex = 56;
            this.MaxEndHeight_label.Text = "~ MaxEndHeight :";
            // 
            // MaxStartHeight_label
            // 
            this.MaxStartHeight_label.AutoSize = true;
            this.MaxStartHeight_label.Location = new System.Drawing.Point(155, 100);
            this.MaxStartHeight_label.Name = "MaxStartHeight_label";
            this.MaxStartHeight_label.Size = new System.Drawing.Size(93, 12);
            this.MaxStartHeight_label.TabIndex = 56;
            this.MaxStartHeight_label.Text = "~ MaxStartHeight :";
            // 
            // MaxEndHeight_textBox
            // 
            this.MaxEndHeight_textBox.Location = new System.Drawing.Point(253, 132);
            this.MaxEndHeight_textBox.Name = "MaxEndHeight_textBox";
            this.MaxEndHeight_textBox.Size = new System.Drawing.Size(46, 22);
            this.MaxEndHeight_textBox.TabIndex = 55;
            this.MaxEndHeight_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MaxStartHeight_textBox
            // 
            this.MaxStartHeight_textBox.Location = new System.Drawing.Point(253, 97);
            this.MaxStartHeight_textBox.Name = "MaxStartHeight_textBox";
            this.MaxStartHeight_textBox.Size = new System.Drawing.Size(46, 22);
            this.MaxStartHeight_textBox.TabIndex = 55;
            this.MaxStartHeight_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MinEndHeight_textBox
            // 
            this.MinEndHeight_textBox.Location = new System.Drawing.Point(95, 132);
            this.MinEndHeight_textBox.Name = "MinEndHeight_textBox";
            this.MinEndHeight_textBox.Size = new System.Drawing.Size(46, 22);
            this.MinEndHeight_textBox.TabIndex = 55;
            this.MinEndHeight_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MinStartHeight_textBox
            // 
            this.MinStartHeight_textBox.Location = new System.Drawing.Point(95, 97);
            this.MinStartHeight_textBox.Name = "MinStartHeight_textBox";
            this.MinStartHeight_textBox.Size = new System.Drawing.Size(46, 22);
            this.MinStartHeight_textBox.TabIndex = 55;
            this.MinStartHeight_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MinEndHeight_label
            // 
            this.MinEndHeight_label.AutoSize = true;
            this.MinEndHeight_label.Location = new System.Drawing.Point(10, 135);
            this.MinEndHeight_label.Name = "MinEndHeight_label";
            this.MinEndHeight_label.Size = new System.Drawing.Size(80, 12);
            this.MinEndHeight_label.TabIndex = 54;
            this.MinEndHeight_label.Text = "MinEndHeight :";
            // 
            // MinStartHeight_label
            // 
            this.MinStartHeight_label.AutoSize = true;
            this.MinStartHeight_label.Location = new System.Drawing.Point(10, 100);
            this.MinStartHeight_label.Name = "MinStartHeight_label";
            this.MinStartHeight_label.Size = new System.Drawing.Size(82, 12);
            this.MinStartHeight_label.TabIndex = 54;
            this.MinStartHeight_label.Text = "MinStartHeight :";
            // 
            // MaxEndSize_label
            // 
            this.MaxEndSize_label.AutoSize = true;
            this.MaxEndSize_label.Location = new System.Drawing.Point(157, 56);
            this.MaxEndSize_label.Name = "MaxEndSize_label";
            this.MaxEndSize_label.Size = new System.Drawing.Size(79, 12);
            this.MaxEndSize_label.TabIndex = 50;
            this.MaxEndSize_label.Text = "~ MaxEndSize :";
            // 
            // MaxStartSize_label
            // 
            this.MaxStartSize_label.AutoSize = true;
            this.MaxStartSize_label.Location = new System.Drawing.Point(155, 28);
            this.MaxStartSize_label.Name = "MaxStartSize_label";
            this.MaxStartSize_label.Size = new System.Drawing.Size(81, 12);
            this.MaxStartSize_label.TabIndex = 51;
            this.MaxStartSize_label.Text = "~ MaxStartSize :";
            // 
            // MinEndSize_label
            // 
            this.MinEndSize_label.AutoSize = true;
            this.MinEndSize_label.Location = new System.Drawing.Point(3, 56);
            this.MinEndSize_label.Name = "MinEndSize_label";
            this.MinEndSize_label.Size = new System.Drawing.Size(68, 12);
            this.MinEndSize_label.TabIndex = 52;
            this.MinEndSize_label.Text = "MinEndSize :";
            // 
            // MinStartSize_label
            // 
            this.MinStartSize_label.AutoSize = true;
            this.MinStartSize_label.Location = new System.Drawing.Point(4, 28);
            this.MinStartSize_label.Name = "MinStartSize_label";
            this.MinStartSize_label.Size = new System.Drawing.Size(70, 12);
            this.MinStartSize_label.TabIndex = 53;
            this.MinStartSize_label.Text = "MinStartSize :";
            // 
            // MaxStartSize_textBox
            // 
            this.MaxStartSize_textBox.Location = new System.Drawing.Point(237, 25);
            this.MaxStartSize_textBox.Name = "MaxStartSize_textBox";
            this.MaxStartSize_textBox.Size = new System.Drawing.Size(63, 22);
            this.MaxStartSize_textBox.TabIndex = 47;
            this.MaxStartSize_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MinStartSize_textBox
            // 
            this.MinStartSize_textBox.Location = new System.Drawing.Point(79, 25);
            this.MinStartSize_textBox.Name = "MinStartSize_textBox";
            this.MinStartSize_textBox.Size = new System.Drawing.Size(63, 22);
            this.MinStartSize_textBox.TabIndex = 46;
            this.MinStartSize_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MaxEndSize_textBox
            // 
            this.MaxEndSize_textBox.Location = new System.Drawing.Point(237, 53);
            this.MaxEndSize_textBox.Name = "MaxEndSize_textBox";
            this.MaxEndSize_textBox.Size = new System.Drawing.Size(63, 22);
            this.MaxEndSize_textBox.TabIndex = 48;
            this.MaxEndSize_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MinEndSize_textBox
            // 
            this.MinEndSize_textBox.Location = new System.Drawing.Point(78, 53);
            this.MinEndSize_textBox.Name = "MinEndSize_textBox";
            this.MinEndSize_textBox.Size = new System.Drawing.Size(63, 22);
            this.MinEndSize_textBox.TabIndex = 49;
            this.MinEndSize_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PSType_groupBox
            // 
            this.PSType_groupBox.Controls.Add(this.QuadPS_radioButton);
            this.PSType_groupBox.Controls.Add(this.PointList_radioButton);
            this.PSType_groupBox.Location = new System.Drawing.Point(27, 873);
            this.PSType_groupBox.Name = "PSType_groupBox";
            this.PSType_groupBox.Size = new System.Drawing.Size(308, 61);
            this.PSType_groupBox.TabIndex = 51;
            this.PSType_groupBox.TabStop = false;
            this.PSType_groupBox.Text = "Paiticle Type";
            // 
            // QuadPS_radioButton
            // 
            this.QuadPS_radioButton.AutoSize = true;
            this.QuadPS_radioButton.Location = new System.Drawing.Point(10, 30);
            this.QuadPS_radioButton.Name = "QuadPS_radioButton";
            this.QuadPS_radioButton.Size = new System.Drawing.Size(60, 16);
            this.QuadPS_radioButton.TabIndex = 5;
            this.QuadPS_radioButton.TabStop = true;
            this.QuadPS_radioButton.Text = "QuadPS";
            this.QuadPS_radioButton.UseVisualStyleBackColor = true;
            this.QuadPS_radioButton.CheckedChanged += new System.EventHandler(this.QuadPS_radioButton_CheckedChanged);
            // 
            // PointList_radioButton
            // 
            this.PointList_radioButton.AutoSize = true;
            this.PointList_radioButton.Location = new System.Drawing.Point(85, 30);
            this.PointList_radioButton.Name = "PointList_radioButton";
            this.PointList_radioButton.Size = new System.Drawing.Size(76, 16);
            this.PointList_radioButton.TabIndex = 4;
            this.PointList_radioButton.TabStop = true;
            this.PointList_radioButton.Text = "PointListPS";
            this.PointList_radioButton.UseVisualStyleBackColor = true;
            this.PointList_radioButton.CheckedChanged += new System.EventHandler(this.PointList_radioButton_CheckedChanged);
            // 
            // Duration_groupBox
            // 
            this.Duration_groupBox.Controls.Add(this.Duration_label);
            this.Duration_groupBox.Controls.Add(this.Duration_textBox);
            this.Duration_groupBox.Controls.Add(this.DurationRandomness_label);
            this.Duration_groupBox.Controls.Add(this.DurationRandomness_textBox);
            this.Duration_groupBox.Location = new System.Drawing.Point(27, 135);
            this.Duration_groupBox.Name = "Duration_groupBox";
            this.Duration_groupBox.Size = new System.Drawing.Size(308, 73);
            this.Duration_groupBox.TabIndex = 50;
            this.Duration_groupBox.TabStop = false;
            this.Duration_groupBox.Text = "Duration :";
            // 
            // Duration_label
            // 
            this.Duration_label.AutoSize = true;
            this.Duration_label.Location = new System.Drawing.Point(116, 20);
            this.Duration_label.Name = "Duration_label";
            this.Duration_label.Size = new System.Drawing.Size(52, 12);
            this.Duration_label.TabIndex = 5;
            this.Duration_label.Text = "Duration :";
            // 
            // Duration_textBox
            // 
            this.Duration_textBox.Location = new System.Drawing.Point(174, 17);
            this.Duration_textBox.Name = "Duration_textBox";
            this.Duration_textBox.Size = new System.Drawing.Size(63, 22);
            this.Duration_textBox.TabIndex = 6;
            this.Duration_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DurationRandomness_label
            // 
            this.DurationRandomness_label.AutoSize = true;
            this.DurationRandomness_label.Location = new System.Drawing.Point(56, 48);
            this.DurationRandomness_label.Name = "DurationRandomness_label";
            this.DurationRandomness_label.Size = new System.Drawing.Size(111, 12);
            this.DurationRandomness_label.TabIndex = 7;
            this.DurationRandomness_label.Text = "DurationRandomness :";
            // 
            // DurationRandomness_textBox
            // 
            this.DurationRandomness_textBox.Location = new System.Drawing.Point(173, 45);
            this.DurationRandomness_textBox.Name = "DurationRandomness_textBox";
            this.DurationRandomness_textBox.Size = new System.Drawing.Size(63, 22);
            this.DurationRandomness_textBox.TabIndex = 8;
            this.DurationRandomness_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Velocity_groupBox
            // 
            this.Velocity_groupBox.Controls.Add(this.StartVelocity_textBox);
            this.Velocity_groupBox.Controls.Add(this.EndVelocity_textBox);
            this.Velocity_groupBox.Controls.Add(this.StartVelocity_label);
            this.Velocity_groupBox.Controls.Add(this.EndVelocity_label);
            this.Velocity_groupBox.Controls.Add(this.MaxVerticalVelocity_textBox);
            this.Velocity_groupBox.Controls.Add(this.MaxVerticalVelocity_label);
            this.Velocity_groupBox.Controls.Add(this.MinVerticalVelocity_textBox);
            this.Velocity_groupBox.Controls.Add(this.MinVerticalVelocity_label);
            this.Velocity_groupBox.Controls.Add(this.MaxHorizontalVelocity_textBox);
            this.Velocity_groupBox.Controls.Add(this.MaxHorizontalVelocity_label);
            this.Velocity_groupBox.Controls.Add(this.MinHorizontalVelocity_textBox);
            this.Velocity_groupBox.Controls.Add(this.MinHorizontalVelocity_label);
            this.Velocity_groupBox.Controls.Add(this.EmitterVelocitySensitivity_textBox);
            this.Velocity_groupBox.Controls.Add(this.EmitterVelocitySensitivity_label);
            this.Velocity_groupBox.Location = new System.Drawing.Point(27, 214);
            this.Velocity_groupBox.Name = "Velocity_groupBox";
            this.Velocity_groupBox.Size = new System.Drawing.Size(308, 246);
            this.Velocity_groupBox.TabIndex = 49;
            this.Velocity_groupBox.TabStop = false;
            this.Velocity_groupBox.Text = "Velocity :";
            // 
            // StartVelocity_textBox
            // 
            this.StartVelocity_textBox.Location = new System.Drawing.Point(174, 213);
            this.StartVelocity_textBox.Name = "StartVelocity_textBox";
            this.StartVelocity_textBox.Size = new System.Drawing.Size(63, 22);
            this.StartVelocity_textBox.TabIndex = 30;
            this.StartVelocity_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // EndVelocity_textBox
            // 
            this.EndVelocity_textBox.Location = new System.Drawing.Point(174, 183);
            this.EndVelocity_textBox.Name = "EndVelocity_textBox";
            this.EndVelocity_textBox.Size = new System.Drawing.Size(63, 22);
            this.EndVelocity_textBox.TabIndex = 30;
            this.EndVelocity_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // StartVelocity_label
            // 
            this.StartVelocity_label.AutoSize = true;
            this.StartVelocity_label.Location = new System.Drawing.Point(98, 216);
            this.StartVelocity_label.Name = "StartVelocity_label";
            this.StartVelocity_label.Size = new System.Drawing.Size(71, 12);
            this.StartVelocity_label.TabIndex = 29;
            this.StartVelocity_label.Text = "StartVelocity :";
            // 
            // EndVelocity_label
            // 
            this.EndVelocity_label.AutoSize = true;
            this.EndVelocity_label.Location = new System.Drawing.Point(98, 186);
            this.EndVelocity_label.Name = "EndVelocity_label";
            this.EndVelocity_label.Size = new System.Drawing.Size(69, 12);
            this.EndVelocity_label.TabIndex = 29;
            this.EndVelocity_label.Text = "EndVelocity :";
            // 
            // MaxVerticalVelocity_textBox
            // 
            this.MaxVerticalVelocity_textBox.Location = new System.Drawing.Point(174, 152);
            this.MaxVerticalVelocity_textBox.Name = "MaxVerticalVelocity_textBox";
            this.MaxVerticalVelocity_textBox.Size = new System.Drawing.Size(63, 22);
            this.MaxVerticalVelocity_textBox.TabIndex = 28;
            this.MaxVerticalVelocity_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MaxVerticalVelocity_label
            // 
            this.MaxVerticalVelocity_label.AutoSize = true;
            this.MaxVerticalVelocity_label.Location = new System.Drawing.Point(61, 155);
            this.MaxVerticalVelocity_label.Name = "MaxVerticalVelocity_label";
            this.MaxVerticalVelocity_label.Size = new System.Drawing.Size(107, 12);
            this.MaxVerticalVelocity_label.TabIndex = 27;
            this.MaxVerticalVelocity_label.Text = "MaxVerticalVelocity :";
            // 
            // MinVerticalVelocity_textBox
            // 
            this.MinVerticalVelocity_textBox.Location = new System.Drawing.Point(174, 120);
            this.MinVerticalVelocity_textBox.Name = "MinVerticalVelocity_textBox";
            this.MinVerticalVelocity_textBox.Size = new System.Drawing.Size(63, 22);
            this.MinVerticalVelocity_textBox.TabIndex = 26;
            this.MinVerticalVelocity_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MinVerticalVelocity_label
            // 
            this.MinVerticalVelocity_label.AutoSize = true;
            this.MinVerticalVelocity_label.Location = new System.Drawing.Point(63, 123);
            this.MinVerticalVelocity_label.Name = "MinVerticalVelocity_label";
            this.MinVerticalVelocity_label.Size = new System.Drawing.Size(105, 12);
            this.MinVerticalVelocity_label.TabIndex = 25;
            this.MinVerticalVelocity_label.Text = "MinVerticalVelocity :";
            // 
            // MaxHorizontalVelocity_textBox
            // 
            this.MaxHorizontalVelocity_textBox.Location = new System.Drawing.Point(174, 87);
            this.MaxHorizontalVelocity_textBox.Name = "MaxHorizontalVelocity_textBox";
            this.MaxHorizontalVelocity_textBox.Size = new System.Drawing.Size(62, 22);
            this.MaxHorizontalVelocity_textBox.TabIndex = 24;
            this.MaxHorizontalVelocity_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MaxHorizontalVelocity_label
            // 
            this.MaxHorizontalVelocity_label.AutoSize = true;
            this.MaxHorizontalVelocity_label.Location = new System.Drawing.Point(48, 90);
            this.MaxHorizontalVelocity_label.Name = "MaxHorizontalVelocity_label";
            this.MaxHorizontalVelocity_label.Size = new System.Drawing.Size(120, 12);
            this.MaxHorizontalVelocity_label.TabIndex = 23;
            this.MaxHorizontalVelocity_label.Text = "MaxHorizontalVelocity :";
            // 
            // MinHorizontalVelocity_textBox
            // 
            this.MinHorizontalVelocity_textBox.Location = new System.Drawing.Point(174, 57);
            this.MinHorizontalVelocity_textBox.Name = "MinHorizontalVelocity_textBox";
            this.MinHorizontalVelocity_textBox.Size = new System.Drawing.Size(62, 22);
            this.MinHorizontalVelocity_textBox.TabIndex = 22;
            this.MinHorizontalVelocity_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MinHorizontalVelocity_label
            // 
            this.MinHorizontalVelocity_label.AutoSize = true;
            this.MinHorizontalVelocity_label.Location = new System.Drawing.Point(50, 60);
            this.MinHorizontalVelocity_label.Name = "MinHorizontalVelocity_label";
            this.MinHorizontalVelocity_label.Size = new System.Drawing.Size(118, 12);
            this.MinHorizontalVelocity_label.TabIndex = 21;
            this.MinHorizontalVelocity_label.Text = "MinHorizontalVelocity :";
            // 
            // EmitterVelocitySensitivity_textBox
            // 
            this.EmitterVelocitySensitivity_textBox.Location = new System.Drawing.Point(174, 29);
            this.EmitterVelocitySensitivity_textBox.Name = "EmitterVelocitySensitivity_textBox";
            this.EmitterVelocitySensitivity_textBox.Size = new System.Drawing.Size(63, 22);
            this.EmitterVelocitySensitivity_textBox.TabIndex = 20;
            this.EmitterVelocitySensitivity_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // EmitterVelocitySensitivity_label
            // 
            this.EmitterVelocitySensitivity_label.AutoSize = true;
            this.EmitterVelocitySensitivity_label.Location = new System.Drawing.Point(36, 32);
            this.EmitterVelocitySensitivity_label.Name = "EmitterVelocitySensitivity_label";
            this.EmitterVelocitySensitivity_label.Size = new System.Drawing.Size(132, 12);
            this.EmitterVelocitySensitivity_label.TabIndex = 19;
            this.EmitterVelocitySensitivity_label.Text = "EmitterVelocitySensitivity :";
            // 
            // BornColor_groupBox
            // 
            this.BornColor_groupBox.Controls.Add(this.MaxColorA_textBox);
            this.BornColor_groupBox.Controls.Add(this.MinColorA_textBox);
            this.BornColor_groupBox.Controls.Add(this.ChangeMaxColor_button);
            this.BornColor_groupBox.Controls.Add(this.MaxColor_label);
            this.BornColor_groupBox.Controls.Add(this.MaxColorB_label);
            this.BornColor_groupBox.Controls.Add(this.MinColorB_label);
            this.BornColor_groupBox.Controls.Add(this.MaxColorG_label);
            this.BornColor_groupBox.Controls.Add(this.MaxColorR_label);
            this.BornColor_groupBox.Controls.Add(this.MinColorG_label);
            this.BornColor_groupBox.Controls.Add(this.MaxColorBTitle_label);
            this.BornColor_groupBox.Controls.Add(this.MinColorR_label);
            this.BornColor_groupBox.Controls.Add(this.MaxColorGTitle_label);
            this.BornColor_groupBox.Controls.Add(this.MaxColorATitle_label);
            this.BornColor_groupBox.Controls.Add(this.MinColorATitle_label);
            this.BornColor_groupBox.Controls.Add(this.MinColorBTitle_label);
            this.BornColor_groupBox.Controls.Add(this.MinColorGTitle_label);
            this.BornColor_groupBox.Controls.Add(this.MaxColorRTitle_label);
            this.BornColor_groupBox.Controls.Add(this.MinColorRTitle_label);
            this.BornColor_groupBox.Controls.Add(this.ChangeMinColor_button);
            this.BornColor_groupBox.Controls.Add(this.MinColor_label);
            this.BornColor_groupBox.Location = new System.Drawing.Point(27, 607);
            this.BornColor_groupBox.Name = "BornColor_groupBox";
            this.BornColor_groupBox.Size = new System.Drawing.Size(308, 149);
            this.BornColor_groupBox.TabIndex = 44;
            this.BornColor_groupBox.TabStop = false;
            this.BornColor_groupBox.Text = "BornColor :";
            // 
            // MaxColorA_textBox
            // 
            this.MaxColorA_textBox.Location = new System.Drawing.Point(255, 83);
            this.MaxColorA_textBox.Name = "MaxColorA_textBox";
            this.MaxColorA_textBox.Size = new System.Drawing.Size(37, 22);
            this.MaxColorA_textBox.TabIndex = 61;
            this.MaxColorA_textBox.Text = "255";
            this.MaxColorA_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MinColorA_textBox
            // 
            this.MinColorA_textBox.Location = new System.Drawing.Point(255, 23);
            this.MinColorA_textBox.Name = "MinColorA_textBox";
            this.MinColorA_textBox.Size = new System.Drawing.Size(37, 22);
            this.MinColorA_textBox.TabIndex = 62;
            this.MinColorA_textBox.Text = "255";
            this.MinColorA_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ChangeMaxColor_button
            // 
            this.ChangeMaxColor_button.Location = new System.Drawing.Point(6, 113);
            this.ChangeMaxColor_button.Name = "ChangeMaxColor_button";
            this.ChangeMaxColor_button.Size = new System.Drawing.Size(113, 23);
            this.ChangeMaxColor_button.TabIndex = 60;
            this.ChangeMaxColor_button.Text = "ChangeMaxColor";
            this.ChangeMaxColor_button.UseVisualStyleBackColor = true;
            this.ChangeMaxColor_button.Click += new System.EventHandler(this.ChangeMaxColor_button_Click);
            // 
            // MaxColor_label
            // 
            this.MaxColor_label.AutoSize = true;
            this.MaxColor_label.Location = new System.Drawing.Point(8, 89);
            this.MaxColor_label.Name = "MaxColor_label";
            this.MaxColor_label.Size = new System.Drawing.Size(59, 12);
            this.MaxColor_label.TabIndex = 59;
            this.MaxColor_label.Text = "MaxColor :";
            // 
            // MaxColorB_label
            // 
            this.MaxColorB_label.AutoSize = true;
            this.MaxColorB_label.Location = new System.Drawing.Point(200, 89);
            this.MaxColorB_label.Name = "MaxColorB_label";
            this.MaxColorB_label.Size = new System.Drawing.Size(23, 12);
            this.MaxColorB_label.TabIndex = 57;
            this.MaxColorB_label.Text = "255";
            // 
            // MinColorB_label
            // 
            this.MinColorB_label.AutoSize = true;
            this.MinColorB_label.Location = new System.Drawing.Point(198, 28);
            this.MinColorB_label.Name = "MinColorB_label";
            this.MinColorB_label.Size = new System.Drawing.Size(23, 12);
            this.MinColorB_label.TabIndex = 58;
            this.MinColorB_label.Text = "255";
            // 
            // MaxColorG_label
            // 
            this.MaxColorG_label.AutoSize = true;
            this.MaxColorG_label.Location = new System.Drawing.Point(146, 89);
            this.MaxColorG_label.Name = "MaxColorG_label";
            this.MaxColorG_label.Size = new System.Drawing.Size(23, 12);
            this.MaxColorG_label.TabIndex = 55;
            this.MaxColorG_label.Text = "255";
            // 
            // MaxColorR_label
            // 
            this.MaxColorR_label.AutoSize = true;
            this.MaxColorR_label.Location = new System.Drawing.Point(93, 89);
            this.MaxColorR_label.Name = "MaxColorR_label";
            this.MaxColorR_label.Size = new System.Drawing.Size(23, 12);
            this.MaxColorR_label.TabIndex = 53;
            this.MaxColorR_label.Text = "255";
            // 
            // MinColorG_label
            // 
            this.MinColorG_label.AutoSize = true;
            this.MinColorG_label.Location = new System.Drawing.Point(146, 28);
            this.MinColorG_label.Name = "MinColorG_label";
            this.MinColorG_label.Size = new System.Drawing.Size(23, 12);
            this.MinColorG_label.TabIndex = 56;
            this.MinColorG_label.Text = "255";
            // 
            // MaxColorBTitle_label
            // 
            this.MaxColorBTitle_label.AutoSize = true;
            this.MaxColorBTitle_label.Location = new System.Drawing.Point(179, 89);
            this.MaxColorBTitle_label.Name = "MaxColorBTitle_label";
            this.MaxColorBTitle_label.Size = new System.Drawing.Size(19, 12);
            this.MaxColorBTitle_label.TabIndex = 52;
            this.MaxColorBTitle_label.Text = "B :";
            // 
            // MinColorR_label
            // 
            this.MinColorR_label.AutoSize = true;
            this.MinColorR_label.Location = new System.Drawing.Point(93, 28);
            this.MinColorR_label.Name = "MinColorR_label";
            this.MinColorR_label.Size = new System.Drawing.Size(23, 12);
            this.MinColorR_label.TabIndex = 54;
            this.MinColorR_label.Text = "255";
            // 
            // MaxColorGTitle_label
            // 
            this.MaxColorGTitle_label.AutoSize = true;
            this.MaxColorGTitle_label.Location = new System.Drawing.Point(126, 89);
            this.MaxColorGTitle_label.Name = "MaxColorGTitle_label";
            this.MaxColorGTitle_label.Size = new System.Drawing.Size(19, 12);
            this.MaxColorGTitle_label.TabIndex = 47;
            this.MaxColorGTitle_label.Text = "G :";
            // 
            // MaxColorATitle_label
            // 
            this.MaxColorATitle_label.AutoSize = true;
            this.MaxColorATitle_label.Location = new System.Drawing.Point(231, 88);
            this.MaxColorATitle_label.Name = "MaxColorATitle_label";
            this.MaxColorATitle_label.Size = new System.Drawing.Size(19, 12);
            this.MaxColorATitle_label.TabIndex = 50;
            this.MaxColorATitle_label.Text = "A :";
            // 
            // MinColorATitle_label
            // 
            this.MinColorATitle_label.AutoSize = true;
            this.MinColorATitle_label.Location = new System.Drawing.Point(230, 27);
            this.MinColorATitle_label.Name = "MinColorATitle_label";
            this.MinColorATitle_label.Size = new System.Drawing.Size(19, 12);
            this.MinColorATitle_label.TabIndex = 51;
            this.MinColorATitle_label.Text = "A :";
            // 
            // MinColorBTitle_label
            // 
            this.MinColorBTitle_label.AutoSize = true;
            this.MinColorBTitle_label.Location = new System.Drawing.Point(179, 28);
            this.MinColorBTitle_label.Name = "MinColorBTitle_label";
            this.MinColorBTitle_label.Size = new System.Drawing.Size(19, 12);
            this.MinColorBTitle_label.TabIndex = 49;
            this.MinColorBTitle_label.Text = "B :";
            // 
            // MinColorGTitle_label
            // 
            this.MinColorGTitle_label.AutoSize = true;
            this.MinColorGTitle_label.Location = new System.Drawing.Point(126, 28);
            this.MinColorGTitle_label.Name = "MinColorGTitle_label";
            this.MinColorGTitle_label.Size = new System.Drawing.Size(19, 12);
            this.MinColorGTitle_label.TabIndex = 48;
            this.MinColorGTitle_label.Text = "G :";
            // 
            // MaxColorRTitle_label
            // 
            this.MaxColorRTitle_label.AutoSize = true;
            this.MaxColorRTitle_label.Location = new System.Drawing.Point(73, 90);
            this.MaxColorRTitle_label.Name = "MaxColorRTitle_label";
            this.MaxColorRTitle_label.Size = new System.Drawing.Size(19, 12);
            this.MaxColorRTitle_label.TabIndex = 45;
            this.MaxColorRTitle_label.Text = "R :";
            // 
            // MinColorRTitle_label
            // 
            this.MinColorRTitle_label.AutoSize = true;
            this.MinColorRTitle_label.Location = new System.Drawing.Point(71, 27);
            this.MinColorRTitle_label.Name = "MinColorRTitle_label";
            this.MinColorRTitle_label.Size = new System.Drawing.Size(19, 12);
            this.MinColorRTitle_label.TabIndex = 46;
            this.MinColorRTitle_label.Text = "R :";
            // 
            // ChangeMinColor_button
            // 
            this.ChangeMinColor_button.Location = new System.Drawing.Point(6, 53);
            this.ChangeMinColor_button.Name = "ChangeMinColor_button";
            this.ChangeMinColor_button.Size = new System.Drawing.Size(113, 23);
            this.ChangeMinColor_button.TabIndex = 44;
            this.ChangeMinColor_button.Text = "ChangeMinColor";
            this.ChangeMinColor_button.UseVisualStyleBackColor = true;
            this.ChangeMinColor_button.Click += new System.EventHandler(this.ChangeMinColor_button_Click);
            // 
            // MinColor_label
            // 
            this.MinColor_label.AutoSize = true;
            this.MinColor_label.Location = new System.Drawing.Point(6, 27);
            this.MinColor_label.Name = "MinColor_label";
            this.MinColor_label.Size = new System.Drawing.Size(57, 12);
            this.MinColor_label.TabIndex = 43;
            this.MinColor_label.Text = "MinColor :";
            // 
            // Gravity_groupBox
            // 
            this.Gravity_groupBox.Controls.Add(this.GravityZ_textBox);
            this.Gravity_groupBox.Controls.Add(this.GravityY_textBox);
            this.Gravity_groupBox.Controls.Add(this.GravityX_textBox);
            this.Gravity_groupBox.Controls.Add(this.Gravity_label_Z);
            this.Gravity_groupBox.Controls.Add(this.Gravity_label_Y);
            this.Gravity_groupBox.Controls.Add(this.Gravity_label_X);
            this.Gravity_groupBox.Location = new System.Drawing.Point(27, 545);
            this.Gravity_groupBox.Name = "Gravity_groupBox";
            this.Gravity_groupBox.Size = new System.Drawing.Size(308, 57);
            this.Gravity_groupBox.TabIndex = 41;
            this.Gravity_groupBox.TabStop = false;
            this.Gravity_groupBox.Text = "Gravity :";
            // 
            // GravityZ_textBox
            // 
            this.GravityZ_textBox.Location = new System.Drawing.Point(174, 21);
            this.GravityZ_textBox.Name = "GravityZ_textBox";
            this.GravityZ_textBox.Size = new System.Drawing.Size(33, 22);
            this.GravityZ_textBox.TabIndex = 31;
            this.GravityZ_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // GravityY_textBox
            // 
            this.GravityY_textBox.Location = new System.Drawing.Point(108, 21);
            this.GravityY_textBox.Name = "GravityY_textBox";
            this.GravityY_textBox.Size = new System.Drawing.Size(33, 22);
            this.GravityY_textBox.TabIndex = 30;
            this.GravityY_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // GravityX_textBox
            // 
            this.GravityX_textBox.Location = new System.Drawing.Point(40, 21);
            this.GravityX_textBox.Name = "GravityX_textBox";
            this.GravityX_textBox.Size = new System.Drawing.Size(33, 22);
            this.GravityX_textBox.TabIndex = 29;
            this.GravityX_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Gravity_label_Z
            // 
            this.Gravity_label_Z.AutoSize = true;
            this.Gravity_label_Z.Location = new System.Drawing.Point(150, 24);
            this.Gravity_label_Z.Name = "Gravity_label_Z";
            this.Gravity_label_Z.Size = new System.Drawing.Size(18, 12);
            this.Gravity_label_Z.TabIndex = 28;
            this.Gravity_label_Z.Text = "Z :";
            // 
            // Gravity_label_Y
            // 
            this.Gravity_label_Y.AutoSize = true;
            this.Gravity_label_Y.Location = new System.Drawing.Point(83, 24);
            this.Gravity_label_Y.Name = "Gravity_label_Y";
            this.Gravity_label_Y.Size = new System.Drawing.Size(19, 12);
            this.Gravity_label_Y.TabIndex = 27;
            this.Gravity_label_Y.Text = "Y :";
            // 
            // Gravity_label_X
            // 
            this.Gravity_label_X.AutoSize = true;
            this.Gravity_label_X.Location = new System.Drawing.Point(15, 24);
            this.Gravity_label_X.Name = "Gravity_label_X";
            this.Gravity_label_X.Size = new System.Drawing.Size(19, 12);
            this.Gravity_label_X.TabIndex = 26;
            this.Gravity_label_X.Text = "X :";
            // 
            // UseEmitter_checkBox
            // 
            this.UseEmitter_checkBox.AutoSize = true;
            this.UseEmitter_checkBox.Location = new System.Drawing.Point(44, 841);
            this.UseEmitter_checkBox.Name = "UseEmitter_checkBox";
            this.UseEmitter_checkBox.Size = new System.Drawing.Size(75, 16);
            this.UseEmitter_checkBox.TabIndex = 29;
            this.UseEmitter_checkBox.Text = "UseEmitter";
            this.UseEmitter_checkBox.UseVisualStyleBackColor = true;
            // 
            // UseLocalWorld_checkBox
            // 
            this.UseLocalWorld_checkBox.AutoSize = true;
            this.UseLocalWorld_checkBox.Location = new System.Drawing.Point(222, 843);
            this.UseLocalWorld_checkBox.Name = "UseLocalWorld_checkBox";
            this.UseLocalWorld_checkBox.Size = new System.Drawing.Size(97, 16);
            this.UseLocalWorld_checkBox.TabIndex = 28;
            this.UseLocalWorld_checkBox.Text = "UseLocalWorld";
            this.UseLocalWorld_checkBox.UseVisualStyleBackColor = true;
            // 
            // LoadTexture_button
            // 
            this.LoadTexture_button.Location = new System.Drawing.Point(54, 21);
            this.LoadTexture_button.Name = "LoadTexture_button";
            this.LoadTexture_button.Size = new System.Drawing.Size(56, 21);
            this.LoadTexture_button.TabIndex = 4;
            this.LoadTexture_button.Text = "Load";
            this.LoadTexture_button.UseVisualStyleBackColor = true;
            this.LoadTexture_button.Click += new System.EventHandler(this.LoadTexture_button_Click);
            // 
            // TextureName
            // 
            this.TextureName.AutoSize = true;
            this.TextureName.Location = new System.Drawing.Point(200, 25);
            this.TextureName.Name = "TextureName";
            this.TextureName.Size = new System.Drawing.Size(65, 12);
            this.TextureName.TabIndex = 1;
            this.TextureName.Text = "sheep_fire_7";
            // 
            // TextureName_label
            // 
            this.TextureName_label.AutoSize = true;
            this.TextureName_label.Location = new System.Drawing.Point(116, 25);
            this.TextureName_label.Name = "TextureName_label";
            this.TextureName_label.Size = new System.Drawing.Size(74, 12);
            this.TextureName_label.TabIndex = 0;
            this.TextureName_label.Text = "TextureName :";
            // 
            // ParticleSettingsUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 538);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.SaveSettings_button);
            this.Controls.Add(this.LoadSettings_button);
            this.Controls.Add(this.Reset_button);
            this.Name = "ParticleSettingsUI";
            this.Text = "ParticleSettingUI";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ParticleSettingsUI_FormClosed);
            this.panel1.ResumeLayout(false);
            this.Settings_groupBox.ResumeLayout(false);
            this.Settings_groupBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.Number_groupBox.ResumeLayout(false);
            this.Number_groupBox.PerformLayout();
            this.Size_groupBox.ResumeLayout(false);
            this.Size_groupBox.PerformLayout();
            this.PSType_groupBox.ResumeLayout(false);
            this.PSType_groupBox.PerformLayout();
            this.Duration_groupBox.ResumeLayout(false);
            this.Duration_groupBox.PerformLayout();
            this.Velocity_groupBox.ResumeLayout(false);
            this.Velocity_groupBox.PerformLayout();
            this.BornColor_groupBox.ResumeLayout(false);
            this.BornColor_groupBox.PerformLayout();
            this.Gravity_groupBox.ResumeLayout(false);
            this.Gravity_groupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Reset_button;
        private System.Windows.Forms.Button LoadSettings_button;
        private System.Windows.Forms.Button SaveSettings_button;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox Settings_groupBox;
        private System.Windows.Forms.GroupBox Duration_groupBox;
        private System.Windows.Forms.GroupBox Velocity_groupBox;
        private System.Windows.Forms.TextBox EndVelocity_textBox;
        private System.Windows.Forms.Label EndVelocity_label;
        private System.Windows.Forms.TextBox MaxVerticalVelocity_textBox;
        private System.Windows.Forms.Label MaxVerticalVelocity_label;
        private System.Windows.Forms.TextBox MinVerticalVelocity_textBox;
        private System.Windows.Forms.Label MinVerticalVelocity_label;
        private System.Windows.Forms.TextBox MaxHorizontalVelocity_textBox;
        private System.Windows.Forms.Label MaxHorizontalVelocity_label;
        private System.Windows.Forms.TextBox MinHorizontalVelocity_textBox;
        private System.Windows.Forms.Label MinHorizontalVelocity_label;
        private System.Windows.Forms.TextBox EmitterVelocitySensitivity_textBox;
        private System.Windows.Forms.Label EmitterVelocitySensitivity_label;
        private System.Windows.Forms.Label BornPerSecond_label;
        private System.Windows.Forms.Label DestinationBlend_label;
        private System.Windows.Forms.ComboBox DestinationBlend_comboBox;
        private System.Windows.Forms.GroupBox BornColor_groupBox;
        private System.Windows.Forms.TextBox MaxColorA_textBox;
        private System.Windows.Forms.TextBox MinColorA_textBox;
        private System.Windows.Forms.Button ChangeMaxColor_button;
        private System.Windows.Forms.Label MaxColor_label;
        private System.Windows.Forms.Label MaxColorB_label;
        private System.Windows.Forms.Label MinColorB_label;
        private System.Windows.Forms.Label MaxColorG_label;
        private System.Windows.Forms.Label MaxColorR_label;
        private System.Windows.Forms.Label MinColorG_label;
        private System.Windows.Forms.Label MaxColorBTitle_label;
        private System.Windows.Forms.Label MinColorR_label;
        private System.Windows.Forms.Label MaxColorGTitle_label;
        private System.Windows.Forms.Label MaxColorATitle_label;
        private System.Windows.Forms.Label MinColorATitle_label;
        private System.Windows.Forms.Label MinColorBTitle_label;
        private System.Windows.Forms.Label MinColorGTitle_label;
        private System.Windows.Forms.Label MaxColorRTitle_label;
        private System.Windows.Forms.Label MinColorRTitle_label;
        private System.Windows.Forms.Button ChangeMinColor_button;
        private System.Windows.Forms.Label MinColor_label;
        private System.Windows.Forms.Label MaxRotateSpeed_label_label;
        private System.Windows.Forms.Label MinRotateSpeed_label;
        private System.Windows.Forms.GroupBox Gravity_groupBox;
        private System.Windows.Forms.TextBox GravityZ_textBox;
        private System.Windows.Forms.TextBox GravityY_textBox;
        private System.Windows.Forms.TextBox GravityX_textBox;
        private System.Windows.Forms.Label Gravity_label_Z;
        private System.Windows.Forms.Label Gravity_label_Y;
        private System.Windows.Forms.Label Gravity_label_X;
        private System.Windows.Forms.CheckBox UseCurveAlpha_checkBox;
        private System.Windows.Forms.CheckBox UseEmitter_checkBox;
        private System.Windows.Forms.CheckBox DoNotChangeAlpha_checkBox;
        private System.Windows.Forms.CheckBox UseLocalWorld_checkBox;
        private System.Windows.Forms.TextBox BornPerSecond_textBox;
        private System.Windows.Forms.Button LoadTexture_button;
        private System.Windows.Forms.TextBox MaxRotateSpeed_textBox;
        private System.Windows.Forms.TextBox MinRotateSpeed_textBox;
        private System.Windows.Forms.TextBox DurationRandomness_textBox;
        private System.Windows.Forms.Label DurationRandomness_label;
        private System.Windows.Forms.TextBox Duration_textBox;
        private System.Windows.Forms.Label Duration_label;
        private System.Windows.Forms.TextBox MaxParticles_textBox;
        private System.Windows.Forms.Label MaxParticles_label;
        private System.Windows.Forms.Label TextureName;
        private System.Windows.Forms.Label TextureName_label;
        private System.Windows.Forms.TextBox StartVelocity_textBox;
        private System.Windows.Forms.Label StartVelocity_label;
        private System.Windows.Forms.GroupBox PSType_groupBox;
        private System.Windows.Forms.RadioButton QuadPS_radioButton;
        private System.Windows.Forms.RadioButton PointList_radioButton;
        private System.Windows.Forms.GroupBox Size_groupBox;
        private System.Windows.Forms.Label MaxEndSize_label;
        private System.Windows.Forms.Label MaxStartSize_label;
        private System.Windows.Forms.Label MinEndSize_label;
        private System.Windows.Forms.Label MinStartSize_label;
        private System.Windows.Forms.TextBox MaxStartSize_textBox;
        private System.Windows.Forms.TextBox MinStartSize_textBox;
        private System.Windows.Forms.TextBox MaxEndSize_textBox;
        private System.Windows.Forms.TextBox MinEndSize_textBox;
        private System.Windows.Forms.GroupBox Number_groupBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label MaxEndHeight_label;
        private System.Windows.Forms.Label MaxStartHeight_label;
        private System.Windows.Forms.TextBox MaxEndHeight_textBox;
        private System.Windows.Forms.TextBox MaxStartHeight_textBox;
        private System.Windows.Forms.TextBox MinEndHeight_textBox;
        private System.Windows.Forms.TextBox MinStartHeight_textBox;
        private System.Windows.Forms.Label MinEndHeight_label;
        private System.Windows.Forms.Label MinStartHeight_label;
        private System.Windows.Forms.CheckBox RotateOnce_checkBox;
    }
}