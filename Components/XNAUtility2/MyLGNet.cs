﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Lidgren.Network;
//using Lidgren.Network.Xna;

//namespace I_XNAUtility
//{
//    public class MyLGNetP2P : IDisposable
//    {
//        NetPeer m_NetPeer;
//        NetBuffer s_readBuffer;

//        public delegate void SeverDataReceive(NetBuffer readBuffer, NetConnection source);
//        SeverDataReceive m_SeverDataReceive;

//        public delegate void StatusChanged(NetConnection source);
//        StatusChanged m_PlayerJoin, m_PlayerLeft;

//        public MyLGNetP2P()
//        {
//        }

//        public void initialize(SeverDataReceive recieve, int port, StatusChanged playerJoin, StatusChanged playerLeft)
//        {
//            NetConfiguration myConfig = new NetConfiguration("MyLGNetP2P"); // application identifier and local port (can be omitted for clients) 名稱要一樣的才能連
//            myConfig.Port = port;
//            myConfig.MaxConnections = 8; // necessary for server only 

//            m_NetPeer = new NetPeer(myConfig);

//            //m_NetPeer.Configuration.DefaultBufferCapacity = 128;
//            //m_NetPeer.SimulatedLoss = 0;// 0.03f; // 3 %
//            //m_NetPeer.SimulatedMinimumLatency = 0;// 0.1f; // 100 ms
//            //m_NetPeer.SimulatedLatencyVariance = 0;// 0.05f; // 100-150 ms actually


//            // create a buffer to read data into
//            s_readBuffer = m_NetPeer.CreateBuffer();

//            m_SeverDataReceive = recieve;
//            m_PlayerJoin = playerJoin;
//            m_PlayerLeft = playerLeft;

//            m_NetPeer.Start();
//        }

//        //public void Start()
//        //{
//        //    m_NetPeer.Start();
//        //}

//        public void Shutdown()
//        {
//            m_NetPeer.Shutdown("Application exiting");            
//        }

//        public void Dispose()
//        {
//            Shutdown();
//            m_NetPeer.Dispose();
//            m_NetPeer = null;
//        }

//        public void Connect(string ip, int port)
//        {
//            //connect
//            m_NetPeer.Connect(ip, port);
//        }

//        public void Update()
//        {
//            NetMessageType type;
//            NetConnection source;
//            while (m_NetPeer.ReadMessage(s_readBuffer, out type, out source))
//                HandleMessage(type, source, s_readBuffer);
//        }

//        private void HandleMessage(NetMessageType type, NetConnection source, NetBuffer buffer)
//        {
//            switch (type)
//            {
//                case NetMessageType.StatusChanged:
//                    {
//                        string sss = buffer.ReadString();

//                        if (sss == "Connected") //我連上對方，或是對方連上我都會收到
//                        {
//                            m_PlayerJoin(source);
//                        }
//                        else if (sss == "Disconnected")
//                        {
//                            m_PlayerLeft(source);
//                        }
//                        else
//                        {
//                        }
//                    }
//                    break;
//                case NetMessageType.DebugMessage:
//                case NetMessageType.VerboseDebugMessage:
//                case NetMessageType.BadMessageReceived:
//                case NetMessageType.ConnectionRejected:
//                    //WriteToConsole(buffer.ReadString());
//                    break;
//                case NetMessageType.Data:
//                    m_SeverDataReceive(buffer, source);
//                    //WriteToConsole(source.RemoteEndpoint + " writes: " + buffer.ReadString());
//                    break;
//                case NetMessageType.ServerDiscovered:
//                    // discovered another peer!
//                    //s_peer.Connect(buffer.ReadIPEndPoint(), Encoding.ASCII.GetBytes("Hi; I'm " + s_peer.GetHashCode()));
//                    break;
//                default:
//                    // unhandled
//                    break;
//            }
//        }

//        public void SendToAllExceptMe(NetBuffer buffer)
//        {
//            m_NetPeer.SendToAll(buffer, NetChannel.ReliableInOrder1);
//        }

//        public void SendToConnection(NetConnection connection, NetBuffer buffer)
//        {
//            connection.SendMessage(buffer, NetChannel.ReliableInOrder2);
//        }

//        public NetConnection GetConnection(long ip, int port)
//        {
//            return m_NetPeer.GetConnection(new System.Net.IPEndPoint(ip, port));
//        }
//    }
//}
