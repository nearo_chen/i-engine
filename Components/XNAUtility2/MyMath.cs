﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using MeshDataDefine;

namespace I_XNAUtility
{
    static public class MyMath
    {
        static public Vector3 UpVector = new Vector3(0, 1, 0);
        static public Matrix MatScale2 = Matrix.CreateScale(2);
        static public Matrix MatScale3 = Matrix.CreateScale(3);
        static public Matrix MatScale4 = Matrix.CreateScale(4);
        static public Matrix MatScale5 = Matrix.CreateScale(5);
        static public Matrix MatScale6 = Matrix.CreateScale(6);
        static public Matrix MatScale7 = Matrix.CreateScale(7);

        /// <summary>
        /// MatRotateX -90 degree
        /// </summary>
        static public Matrix MatRotateX_N90 = Matrix.CreateRotationX(MathHelper.ToRadians(-90f));
        static public Matrix MatRotateY_P180 = Matrix.CreateRotationY(MathHelper.ToRadians(180f));
        static public Matrix MatRotateY_P90 = Matrix.CreateRotationY(MathHelper.ToRadians(90f));

        /// <summary>
        /// 無條件捨去
        /// </summary>
        static public double Cieling(double value) { return Math.Ceiling(value); }

        static Random rand = new Random(Environment.TickCount);

        /// <summary>
        /// 雙精度浮點數大於或等於 0.0，且小於 1.0。
        /// </summary>
        static public double GetRand() { return rand.NextDouble(); }

        /// <summary>
        /// 取得包含最小值和包含最大值之間的亂數數字。
        /// </summary>
        static public int GetNext(int contain_min, int contain_max) { return rand.Next(contain_min, contain_max + 1); }

        /// <summary>
        /// 傳回:
        ///     32 位元帶正負號的整數，大於或等於零並且小於 maxValue，也就是說，傳回值的範圍通常包含零，但不包含 maxValue。然而，如果 maxValue
        ///     等於零，則會傳回 maxValue。
        /// </summary>     
        static public int GetNext(int NotContain_max) { return rand.Next(NotContain_max); }

        /// <summary>
        /// 將matrix所有元素都設為0
        /// </summary>     
        static public void MatrixZero(ref Matrix mat)
        {
            mat.M11 = mat.M12 = mat.M13 = mat.M14 =
            mat.M21 = mat.M22 = mat.M23 = mat.M24 =
            mat.M31 = mat.M32 = mat.M33 = mat.M34 =
            mat.M41 = mat.M42 = mat.M43 = mat.M44 = 0;
        }

        /// <summary>
        /// 指定matB的值給matA
        /// </summary>     
        public static void MatrixAsignBtoA(ref Matrix matA, ref Matrix matB)
        {
            matA.M11 = matB.M11;
            matA.M12 = matB.M12;
            matA.M13 = matB.M13;
            matA.M14 = matB.M14;
            matA.M21 = matB.M21;
            matA.M22 = matB.M22;
            matA.M23 = matB.M23;
            matA.M24 = matB.M24;
            matA.M31 = matB.M31;
            matA.M32 = matB.M32;
            matA.M33 = matB.M33;
            matA.M34 = matB.M34;
            matA.M41 = matB.M41;
            matA.M42 = matB.M42;
            matA.M43 = matB.M43;
            matA.M44 = matB.M44;
        }

        /// <summary>
        /// matrix根據percent作slerp
        /// </summary>     
        public static Matrix MatrixSlerp(Matrix now, Matrix dest, float percent)
        {
            RSTMatrix rstMat1 = RSTMatrix.Init();
            RSTMatrix rstMat2 = RSTMatrix.Init();
            rstMat1.SetData(now);
            rstMat2.SetData(dest);
            rstMat1.m_Scale = Vector3.Lerp(rstMat1.m_Scale, rstMat2.m_Scale, percent);
            rstMat1.m_RTMatrix.m_Translation =
                Vector3.Lerp(rstMat1.m_RTMatrix.m_Translation, rstMat2.m_RTMatrix.m_Translation, percent);
            rstMat1.m_RTMatrix.m_Quaternion =
                Quaternion.Slerp(rstMat1.m_RTMatrix.m_Quaternion, rstMat2.m_RTMatrix.m_Quaternion, percent);
            return rstMat1.mat;
        }

        /// <summary>
        /// 設定Matrix的方向，cross使用右手座標，內部會將lookDir, lookUp作normalize。
        /// </summary>
        static public void MatrixSetDirection(Vector3 pos, Vector3 lookDir, Vector3 lookUp, out Matrix mat)
        {
            mat = Matrix.Identity;

            //Vector3 forward;
            Vector3.Normalize(ref lookDir, out lookDir);
            Vector3.Normalize(ref lookUp, out lookUp);

            Vector3 right;
            Vector3.Cross(ref lookDir, ref lookUp, out right);
            Vector3.Normalize(ref right, out right);

            //Vector3 up;
            Vector3.Cross(ref right, ref lookDir, out lookUp);
            Vector3.Normalize(ref lookUp, out lookUp);

            //Forward 是代表指向哪裡，所以XNA的Matrix.Forward他又會把正負號改變，
            //這樣用很容易混淆，會產生三角型面錯誤的問題，多注意！！！
            //其他都是這樣，要非常注意才行！！
            //mat.Forward = vvv * -1f;
            //mat.Right = Vector3.Cross(Vector3.Up, mat.Forward)*-1f;
            //mat.Up = Vector3.Cross(mat.Forward, mat.Right)*-1f;

            //指定matrix元素請這樣使用，不然matrix會有奇怪的計算會壞掉。
            mat.Forward = lookDir;
            mat.Right = right;
            mat.Up = lookUp;
            mat.Translation = pos;

            //mat.M31 = forward.X;
            //mat.M32 = forward.Y;
            //mat.M33 = forward.Z;
            //mat.M34 = 0;

            //mat.M11 = right.X;
            //mat.M12 = right.Y;
            //mat.M13 = right.Z;
            //mat.M14 = 0;

            //mat.M21 = up.X;
            //mat.M22 = up.Y;
            //mat.M23 = up.Z;
            //mat.M24 = 0;

            //mat.M41 = pos.X;
            //mat.M42 = pos.Y;
            //mat.M43 = pos.Z;
            //mat.M44 = 1;



            //   return mat;
        }

        //static public void GetMatrixRightElement(ref Matrix mat, out Vector3 value)
        //{
        //     value.X=mat.M11;
        //     value.Y=mat.M12;
        //     value.Z = mat.M13;
        //}
        //static public void GetMatrixForwardElement(ref Matrix mat, out Vector3 value)
        //{
        //    value.X = mat.M31;
        //    value.Y = mat.M32;
        //    value.Z = mat.M33;
        //}
        //static public void GetMatrixUpElement(ref Matrix mat, out Vector3 value)
        //{
        //    value.X = mat.M21;
        //    value.Y = mat.M22;
        //    value.Z = mat.M23;
        //}

        //static public void SetMatrixRightElement(ref Matrix mat, ref Vector3 value)
        //{
        //    mat.M11 = value.X;
        //    mat.M12 = value.Y;
        //    mat.M13 = value.Z;
        //}
        //static public void SetMatrixForwardElement(ref Matrix mat, ref Vector3 value)
        //{
        //    mat.M31 = value.X;
        //    mat.M32 = value.Y;
        //    mat.M33 = value.Z;
        //}
        //static public void SetMatrixUpElement(ref Matrix mat, ref Vector3 value)
        //{
        //    mat.M21 = value.X;
        //    mat.M22 = value.Y;
        //    mat.M23 = value.Z;
        //}
        //static public void SetMatrixPositionElement(ref Matrix mat, ref Vector3 value)
        //{
        //    mat.M41 = value.X;
        //    mat.M42 = value.Y;
        //    mat.M43 = value.Z;
        //}


        static public UInt32 BitInsert(UInt32 srcNumber, UInt32 insertNumber)
        {
            return srcNumber | insertNumber;
        }

        static public UInt32 BitRemove(UInt32 srcNumber, UInt32 removeNumber)
        {
            return srcNumber & (~removeNumber);
        }

        static public bool ConsiderBit(UInt32 srcNumber, UInt32 considerNumber)
        {
            srcNumber = srcNumber & considerNumber;
            if (srcNumber == considerNumber)
                return true;
            return false;
        }


        /// <summary>
          ///根據bounding長度計算合適的camera Matrix
        /// </summary>    
        static public void GetCameraFocusMatrix(Vector3 targetPosition, float boundingRadius, out Matrix cameraMat)
        {
            Vector3 cameraPos = targetPosition + (Vector3.UnitZ * boundingRadius * 2f + Vector3.UnitY * boundingRadius * 1); 
            MyMath.MatrixSetDirection(cameraPos, (targetPosition - cameraPos), Vector3.Up, out cameraMat);   
        }

    }

    //[Serializable()]
    //public struct RTMatrix
    //{
    //    public Quaternion m_Quaternion;
    //    public Vector3 m_Translation;

    //    public static void RTMatrixLerp(ref RTMatrix src, ref RTMatrix dest, float percent, out RTMatrix final)
    //    {
    //        Quaternion                .Slerp(ref src.m_Quaternion, ref dest.m_Quaternion, percent, out final.m_Quaternion);
    //        Vector3.Lerp(ref src.m_Translation, ref dest.m_Translation, percent, out final.m_Translation);
    //    }

    //    public static void CreateMatrix(ref Quaternion quaternion, ref Vector3 translation, out Matrix mat)
    //    {       
    //        Matrix.CreateFromQuaternion(ref quaternion, out mat);
    //        mat.Translation = translation;
    //    }

    //    public static Matrix CreateMatrix(ref Quaternion quaternion, ref Vector3 translation)
    //    {
    //        Matrix mat;
    //        Matrix.CreateFromQuaternion(ref quaternion, out mat);
    //        mat.Translation = translation;
    //        return mat;
    //    }

    //    public static RTMatrix Init()
    //    {
    //        RTMatrix rtMat;
    //        rtMat.m_Translation = Vector3.Zero;
    //        rtMat.m_Quaternion = Quaternion.Identity;
    //        return rtMat;
    //    }

    //    public void SetData(Quaternion q, Vector3 t)
    //    {
    //        m_Quaternion = q; m_Translation = t;
    //    }

    //    public void SetData(Matrix mat)
    //    {
    //        Vector3 scale;
    //        mat.Decompose(out scale, out m_Quaternion, out m_Translation);            
    //    }

    //    public void GetMatrix(out Matrix mat)
    //    {
    //        Matrix.CreateFromQuaternion(ref m_Quaternion, out mat);
    //        mat.M41 = m_Translation.X;
    //        mat.M42 = m_Translation.Y;
    //        mat.M43 = m_Translation.Z;
    //    }

    //    public Matrix mat  
    //    {          
    //        get
    //        {
    //            Matrix m1;
    //            GetMatrix(out m1);               
    //            return m1;
    //        }     
    //    }
    //}

    //[Serializable()]
    //public struct RSTMatrix
    //{
    //   public  static RSTMatrix[] GetArray(int size) {return new RSTMatrix[size]; }

    //    public Vector3 m_Scale;
    //    public RTMatrix m_RTMatrix;

    //    public static void RSTMatrixLerp(ref RSTMatrix src, ref RSTMatrix dest, float percent, out RSTMatrix final)
    //    {
    //        RTMatrix.RTMatrixLerp(ref src.m_RTMatrix, ref dest.m_RTMatrix, percent, out final.m_RTMatrix);
    //        Vector3.Lerp(ref src.m_Scale, ref dest.m_Scale, percent, out final.m_Scale);
    //    }

    //    public void SetData(Vector3 s, Quaternion q, Vector3 t)
    //    {
    //        m_Scale = s;
    //        m_RTMatrix.SetData(q, t);
    //    }

    //    public static RSTMatrix Init()
    //    {
    //        RSTMatrix rstMatrix;
    //        rstMatrix.m_Scale = Vector3.One;
    //        rstMatrix.m_RTMatrix = RTMatrix.Init();
    //        return rstMatrix;
    //    }

    //    public void SetData(Matrix mat)
    //    {
    //        mat.Decompose(out m_Scale, out m_RTMatrix.m_Quaternion, out m_RTMatrix.m_Translation);
    //    }

    //    public void GetMatrix(out Matrix mat)
    //    {
    //        Matrix scaleMat;
    //        Matrix.CreateScale(ref m_Scale, out scaleMat);
    //        m_RTMatrix.GetMatrix(out mat);
    //        Matrix.Multiply(ref scaleMat, ref mat, out mat);
    //    }

    //    public Matrix mat
    //    {
    //        get
    //        {
    //            Matrix m;
    //            GetMatrix(out m);
    //            return m;
    //        }
    //    }
    //}

    //public class MatrixControl
    //{
    //    //由src->dest,隨時間做不同的內插...
    //    float m_FadingPercent;//內插百分比 0~1之間的值
    //    bool m_bActive;
    //    Matrix m_SrcMatrix;
    //    Matrix m_DestMatrix;
    //    float m_Percent;
    //    float m_StopPercent;//看m_Percent到多少就停下來

    //    public bool Active
    //    {
    //        get { return m_bActive; }
    //    }

    //    public MatrixControl()
    //    {
    //        m_bActive = false;
    //        m_SrcMatrix = Matrix.Identity;
    //        m_DestMatrix = Matrix.Identity;
    //    }

    //    public void initialize(Matrix srcMatrix, Matrix destMatrix, float FadingPercent, float StopPercent)
    //    {
    //        m_SrcMatrix = srcMatrix;
    //        m_DestMatrix = destMatrix;
    //        m_FadingPercent = FadingPercent;
    //        m_StopPercent = StopPercent;            
    //    }

    //    public void StartFading()
    //    {
    //        m_bActive = true;
    //        m_Percent = 1;
    //    }

    //    public Matrix update()
    //    {
    //        if (m_bActive == false)
    //            return m_DestMatrix;

    //        m_Percent = m_Percent * m_FadingPercent;
    //        if (m_Percent < m_StopPercent)
    //            m_bActive = false;

    //        //return Matrix.Lerp(m_SrcMatrix, m_DestMatrix, 1-m_Percent);
    //        return (m_SrcMatrix * m_Percent) + (m_DestMatrix * (1 - m_Percent));
    //    }


    //}



}
