﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MeshDataDefine;

namespace I_XNAUtility
{
    public abstract class FrameNode : BasicNode
    {
        //public delegate void SetLocalMatrixFunction(FrameNode frameNode);//給外部控制在設定Local matrix之後做事情。
        //public SetLocalMatrixFunction SetLocalMatrixCallback = null;

        ///// <summary>
        ///// 另外累加上的RST量，會於取得local matrix時將加量乘上去
        ///// </summary>
        //public class AddRST
        //{
        //    public AddRST()
        //    {
        //        AddRotationXDegree = AddRotationYDegree = AddRotationZDegree =
        //     AddTranXRatio = AddTranYRatio = AddTranZRatio = 0;
        //        AddScaleXRatio = AddScaleYRatio = AddScaleZRatio = 1;
        //    }
        //    public float AddRotationXDegree, AddRotationYDegree, AddRotationZDegree,
        //     AddScaleXRatio, AddScaleYRatio, AddScaleZRatio,
        //     AddTranXRatio, AddTranYRatio, AddTranZRatio;
        //}

        ///// <summary>
        ///// 另外累加上的RST量
        ///// </summary>
        //AddRST AdditionalRST = null;
        //請寫這邊將RST存檔做完並且CAMERA  RITATION 位移也做一做

        Matrix m_LocalMatrix;
        Matrix m_WorldMatrix;
        bool m_bRSTChange;//紀錄local matrix 是否有RST改變
        //public bool IfWorldMatrixChange = true;//用來在繪圖時判斷world matrix是否有改變來決定要不要傳入參數給EFFECT。

        void SetAllTreeRSTChanged()
        {
            m_bRSTChange = true;
            //IfWorldMatrixChange = true;//紀錄world matrix改變了。

            FrameNode children = FirstChild as FrameNode;
            while (children != null)
            {
                children.SetAllTreeRSTChanged();
                children = children.Sibling as FrameNode;
            }
        }

        public FrameNode()
        {
            m_LocalMatrix = Matrix.Identity;
            m_WorldMatrix = Matrix.Identity;
            SetAllTreeRSTChanged();
        }

        public Matrix LocalMatrix
        {
            get { return m_LocalMatrix; }
            set
            {
                m_LocalMatrix = value;
                SetAllTreeRSTChanged();

                //if (SetLocalMatrixCallback != null)
                //    SetLocalMatrixCallback(this);
            }
        }

        public Vector3 LocalPosition
        {
            get { return new Vector3(LocalMatrix.M41, LocalMatrix.M42, LocalMatrix.M43); }
            set
            {
                m_LocalMatrix.M41 = value.X;
                m_LocalMatrix.M42 = value.Y;
                m_LocalMatrix.M43 = value.Z;
                SetAllTreeRSTChanged();
            }
        }
        public Vector3 LocalForward
        {
            get { return new Vector3(LocalMatrix.M31, LocalMatrix.M32, LocalMatrix.M33); }
            set
            {
                m_LocalMatrix.M31 = value.X;
                m_LocalMatrix.M32 = value.Y;
                m_LocalMatrix.M33 = value.Z;
                SetAllTreeRSTChanged();
            }
        }
        public Vector3 LocalUp
        {
            get { return new Vector3(LocalMatrix.M21, LocalMatrix.M22, LocalMatrix.M23); }
            set
            {
                m_LocalMatrix.M21 = value.X;
                m_LocalMatrix.M22 = value.Y;
                m_LocalMatrix.M23 = value.Z;
                SetAllTreeRSTChanged();
            }
        }
        public Vector3 LocalRight
        {
            get { return new Vector3(LocalMatrix.M11, LocalMatrix.M12, LocalMatrix.M13); }
            set
            {
                m_LocalMatrix.M11 = value.X;
                m_LocalMatrix.M12 = value.Y;
                m_LocalMatrix.M13 = value.Z;
                SetAllTreeRSTChanged();
            }
        }

        public void SetLocalDirection(ref Vector3 vector)
        {
            Vector3 position = LocalPosition;
            MyMath.MatrixSetDirection(position, vector, Vector3.Up, out m_LocalMatrix);

            SetAllTreeRSTChanged();
        }

        //重新計算自己的world matrix
        void RefreshMatWithParent()
        {
            FrameNode pNode = Parent as FrameNode;

            if (pNode != null)
            {
                Matrix parentWorld = pNode.WorldMatrix;
                Matrix.Multiply(ref m_LocalMatrix, ref parentWorld, out m_WorldMatrix);
            }
            else
                m_WorldMatrix = m_LocalMatrix;
        }

        ////在做world元素存取的時候，都一定要更新refresh world matrix...
        //protected void RefreshWorldMatrix()
        //{
        //    m_WorldMatrix = LocalMatrix;
        //    BasicNode pNode = Parent;
        //    while (pNode != null)
        //    {
        //        Matrix parentLocal = ((FrameNode)pNode).LocalMatrix;
        //        Matrix.Multiply(ref m_WorldMatrix, ref parentLocal, out m_WorldMatrix);
        //        pNode = pNode.Parent;
        //    }
        //}

        public Matrix InverseWorlMatrix
        {
            get
            {
                return Matrix.Invert(WorldMatrix);
            }
        }

        /// <summary>
        /// 抓XYZ SCALE
        /// </summary>
        public Vector3 WorldmatrixScale
        {
            get
            {
                if (m_bRSTChange)
                {
                    RefreshMatWithParent();
                    m_bRSTChange = false;
                }

                RSTMatrix rtmat = new RSTMatrix();
                rtmat.SetData(m_WorldMatrix);
                return rtmat.m_Scale;
            }
        }

        /// <summary>
        /// 抓XYZ SCALE，回傳之中最大者。
        /// </summary>
        public float WorldmatrixScaleMin
        {
            get
            {
                Vector3 scale = WorldmatrixScale;
                float sss = Math.Min(scale.X, scale.Y);
                sss = Math.Min(sss, scale.Z);
                return sss;
            }
        }

        /// <summary>
        /// 把SCALE去掉
        /// </summary>
        public Matrix WorldmatrixRT
        {
            get
            {
                if (m_bRSTChange)
                {
                    RefreshMatWithParent();
                    m_bRSTChange = false;
                }

                RTMatrix rtmat = new RTMatrix();
                rtmat.SetData(m_WorldMatrix);
                return rtmat.mat;
            }
        }

        public Matrix WorldmatrixR
        {
            get
            {
                if (m_bRSTChange)
                {
                    RefreshMatWithParent();
                    m_bRSTChange = false;
                }

                RTMatrix rtmat = new RTMatrix();
                rtmat.SetData(m_WorldMatrix);
                return Matrix.CreateFromQuaternion(rtmat.m_Quaternion); ;
            }
        }

        public Matrix WorldMatrix
        {
            get
            {
                if (m_bRSTChange)
                {
                    RefreshMatWithParent();
                    m_bRSTChange = false;
                }
                return m_WorldMatrix;
            }
            set
            {
                if (Parent != null)
                {
                    FrameNode pNode = Parent as FrameNode;
                    Matrix parentWorld = pNode.WorldMatrix;
                    Matrix parentInvWorld;
                    Matrix.Invert(ref parentWorld, out parentInvWorld);
                    Matrix.Multiply(ref value, ref parentInvWorld, out m_LocalMatrix);
                }
                else
                    m_LocalMatrix = value;
                SetAllTreeRSTChanged();
            }
        }

        public Matrix ParentWorldMatrix
        {
            get
            {
                if (Parent == null)
                    return Matrix.Identity;
                else
                    return ((FrameNode)Parent).WorldMatrix;
            }
        }

        public Vector3 WorldPosition
        {
            get
            {
                Matrix mat = WorldMatrix;
                return new Vector3(mat.M41, mat.M42, mat.M43);
            }
            set
            {
                Matrix parentWorld = ParentWorldMatrix;
                Matrix parentInvWorld;
                Matrix.Invert(ref parentWorld, out parentInvWorld);

                m_WorldMatrix.M41 = value.X;
                m_WorldMatrix.M42 = value.Y;
                m_WorldMatrix.M43 = value.Z;
                Matrix.Multiply(ref m_WorldMatrix, ref parentInvWorld, out m_LocalMatrix);
                SetAllTreeRSTChanged();
            }
        }

        public Vector3 WorldForward
        {
            get
            {
                Matrix mat = WorldMatrix;
                return new Vector3(mat.M31, mat.M32, mat.M33);
            }
        }

        public Vector3 WorldUp
        {
            get
            {
                Matrix mat = WorldMatrix;
                return new Vector3(mat.M21, mat.M22, mat.M23);
            }

        }

        public Vector3 WorldRight
        {
            get
            {
                Matrix mat = WorldMatrix;
                return new Vector3(mat.M11, mat.M12, mat.M13);
            }
        }

        Vector3 m_Scale = Vector3.One;
        public Vector3 GetScale()
        {
            return m_Scale;
        }
        public void Scale(Vector3 scale)
        {
            m_Scale = scale;
            LocalMatrix = Matrix.CreateScale(scale) * LocalMatrix;
        }
        public void Translate(ref Vector3 ttt)
        {
            LocalMatrix = Matrix.CreateTranslation(ttt) * LocalMatrix;
        }
        public void TranslateX(float length)
        {
            LocalMatrix = Matrix.CreateTranslation(length, 0, 0) * LocalMatrix;
        }
        public void TranslateY(float length)
        {
            LocalMatrix = Matrix.CreateTranslation(0, length, 0) * LocalMatrix;
        }
        public void TranslateZ(float length)
        {
            LocalMatrix = Matrix.CreateTranslation(0, 0, length) * LocalMatrix;
        }
        public void RotationAdd(ref Matrix rotMat)
        {
            Matrix.Multiply(ref rotMat, ref m_LocalMatrix, out m_LocalMatrix);
            SetAllTreeRSTChanged();
        }

        public void RotateX(float radius)
        {
            Matrix rot;
            Matrix.CreateRotationX(radius, out rot);
            LocalMatrix = rot * LocalMatrix;
        }

        public void RotateY(float radius)
        {
            Matrix rot;
            Matrix.CreateRotationY(radius, out rot);
            LocalMatrix = rot * LocalMatrix;
        }

        public void RotateZ(float radius)
        {
            Matrix rot;
            Matrix.CreateRotationZ(radius, out rot);
            LocalMatrix = rot * LocalMatrix;
        }


        /// <summary>
        /// Get all visible=true nodes, under tree structure
        /// </summary>   
        //public static void GetVisibleList(FrameNode rootNode, NodeArray getVisibleNodeArray)
        //{
        //    getVisibleNodeArray.Reset();
        //    AddVisibleNode(getVisibleNodeArray);
        //}




        bool m_bDrawAxis;
        public bool IfDrawAxis
        {
            get { return m_bDrawAxis; }
            set { m_bDrawAxis = value; }
        }

        public void SetAllDrawAxis(bool bbb)
        {
            IfDrawAxis = bbb;

            FrameNode node = FirstChild as FrameNode;
            while (node != null)
            {
                node.SetAllDrawAxis(bbb);
                node = node.Sibling as FrameNode;
            }
        }

        //將要attach的node座標系，轉換到父節點的座標系中
        public void AttachChild_Transform(FrameNode transformNode)
        {
            AttachChild(transformNode);//綁到parent上
            transformNode.WorldMatrix = InverseWorlMatrix * transformNode.WorldMatrix;
        }
    }
}
