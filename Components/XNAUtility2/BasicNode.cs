﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace I_XNAUtility
{
    public class NodeArray
    {
        //const int maxNodeAmount = 1000;
        public NodeArray(int maxNodeAmount)
        {
            nowNodeAmount = 0;
                nodeArray = new BasicNode[maxNodeAmount];
        }

         ~NodeArray()
        {
            Dispose();
        }

        public void Reset()
        {
            nowNodeAmount = 0;

            if(nodeArray != null)
                Array.Clear(nodeArray, 0, nodeArray.Length);
        }

        public void Add(BasicNode node)
        {
            if (nowNodeAmount < nodeArray.Length)
            {
                nodeArray[nowNodeAmount] = node;
                nowNodeAmount++;
            }
        }

        public void Dispose()
        {
            Reset();
            nodeArray = null;
        }

        public int nowNodeAmount;
        public BasicNode [] nodeArray;

        /// <summary>
        /// 將nodeArray裡有同樣type的物件加到addNodeArray裡。
        /// </summary>
        static public void AddSameType(NodeArray checkNodeArray, Type type, NodeArray addNodeArray)
        {
            for (int a = 0; a < checkNodeArray.nowNodeAmount; a++)
            {
                BasicNode ooo = checkNodeArray.nodeArray.GetValue(a) as BasicNode;
                if (ooo.GetType() == type)
                    addNodeArray.Add(ooo);
            }
        }
    }


    public abstract class BasicNode : IDisposable
    {
      public  string NodeName;
        private BasicNode m_Parent;
        private BasicNode m_Sibling;
        private BasicNode m_FirstChild;

        //abstract protected void v_TreeRecursiveProcess();
        abstract protected void v_Dispose();
   

        virtual public void Dispose()
        {
            v_Dispose();

            m_Parent=null;
            m_Sibling=null;
            m_FirstChild = null;
        }

        /// <summary>
        /// 將自己以及底下的node都呼叫Dispose
        /// </summary>
        /// <param name="rootNode"></param>
        static public void DisposeAllTree(BasicNode rootNode)
        {
            if (rootNode == null)
                return;

            //int count=0;
            //CountTree(rootNode, ref count);
            //BasicNode[] nodes = new BasicNode[count];

            //count=0;
            //FlattenTree(rootNode, nodes, ref count);
            //for (int a = nodes.Length -1 ; a >=0 ; a--)
            //{
            //    nodes[a].DetachFromParent();
            //    nodes[a].Dispose();
            //    nodes[a] = null;
            //}

            BasicNode children = (BasicNode)rootNode.FirstChild;
            while (children != null)
            {
                BasicNode disposeNode = children;               
                children = (BasicNode)children.Sibling;

                DisposeAllTree(disposeNode);//內部會改變tree結構，所以上面先將下一個children記錄好，再跑DisposeAllTree()
            }

  

            rootNode.DetachFromParent();
            rootNode.Dispose();
            //rootNode = null;
        }

        static void CountTree(BasicNode rootNode, ref int Count)
        {
            Count++;
            BasicNode children = (BasicNode)rootNode.FirstChild;
            while (children != null)
            {
                CountTree(children, ref Count);

                children = (BasicNode)children.Sibling;                
            }
        }

        static void FlattenTree(BasicNode rootNode, BasicNode[] recNodes, ref int countID)
        {
            recNodes[countID] = rootNode;
            countID++;

            BasicNode children = (BasicNode)rootNode.FirstChild;
            while (children != null)
            {
                FlattenTree(children, recNodes, ref countID);
                children = (BasicNode)children.Sibling;
            }
        }

        public BasicNode()
        {
            m_Parent = null;
            m_Sibling = null;
            m_FirstChild = null;
        }

        //public string NodeName
        //{
        //    get { return m_NodeName; }
        //    set { m_NodeName = value; }
        //}

        public BasicNode FirstChild
        {
            get { return m_FirstChild; }
            set { m_FirstChild = value; }
        }

        public BasicNode Sibling
        {
            get { return m_Sibling; }
            set { m_Sibling = value; }
        }

        public BasicNode Parent
        {
            get { return m_Parent; }
            set { m_Parent = value; }
        }

        /// <summary>
        /// just for first child to attach a sibling
        /// </summary>
        /// <param name="AddNode"></param>
        void AttachSibling(BasicNode AddNode)
        {
            if (AddNode == null || AddNode == this)
                return;
            if (AddNode.m_Parent == this)
                return;

            BasicNode s = this;
            while (s.m_Sibling != null)//繞一圈表示最後一個
                s = s.m_Sibling;
            s.m_Sibling = AddNode;
            AddNode.m_Parent = m_Parent;
            AddNode.m_Sibling = null;
        }

        public void AttachChild(BasicNode AddNode)
        {
            if (AddNode == null || AddNode == this)
                return;
            if (m_FirstChild == null)
            {
                m_FirstChild = AddNode;
                AddNode.m_Parent = this;
                //AddNode.sibling = AddNode;//第一個頭尾都是同一個要設定
            }
            else
            {
                m_FirstChild.AttachSibling(AddNode);
            }
        }

        //just for the first child to detache a sibling
        void DetachSibling(BasicNode DelNode)
        {
            if (DelNode == null)
                return;

            if (DelNode == this)//移除FirstChild
            {
                m_Parent.FirstChild = this.Sibling;
                DelNode.m_Sibling = null;
                DelNode.m_Parent = null;
                return;
            }
            else
            {
                BasicNode s = this;
                while (s.m_Sibling != this)
                {
                    if (s.m_Sibling == DelNode)
                    {
                        s.m_Sibling = DelNode.m_Sibling;
                        DelNode.m_Sibling = null;
                        DelNode.m_Parent = null;
                        return;
                    }
                    s = s.m_Sibling;
                }
            }
        }

        //detach a child
        public void DetachChild(BasicNode DelNode)
        {
            if (DelNode == null || DelNode == this || m_FirstChild == null)
                return;
            m_FirstChild.DetachSibling(DelNode);
        }

        /// <summary>
        /// detach from parent
        /// </summary>
        public void DetachFromParent()
        {
            if (Parent != null)
                Parent.DetachChild(this);          
        }

        public void DatachAllChild()
        {
            BasicNode c = FirstChild;
            while (c != null)
            {
                BasicNode s = c.Sibling;
                c.DetachFromParent();
                c = s;
            }           
        }

        /// <summary>
        /// 取得所有包含名字的node，並存入List中。
        /// </summary>   
        public void SeekAllNodeContainName(string nodeName, List<BasicNode> nodeArray)
        {
            if (NodeName != null && NodeName.Contains(nodeName))
            {
                nodeArray.Add(this);              
            }
         
            BasicNode children = (BasicNode)this.FirstChild;
            while (children != null)
            {
                children.SeekAllNodeContainName(nodeName, nodeArray);
                children = (BasicNode)children.Sibling;
            }
        }


        static void GetAllNode(BasicNode rootNode, List<BasicNode> nodeArray)//BasicNode[] nodeArray, ref int nowAmount)
        {
            //updte here by way...
            nodeArray.Add(rootNode);
            //nodeArray.nodeArray[nodeArray.nowNodeAmount] = this;
            //nodeArray.nowNodeAmount++;

            BasicNode c = rootNode.FirstChild;
            while (c != null)
            {
                GetAllNode(c, nodeArray);
                c = c.Sibling;
            }
        }

        /// <summary>
        /// 抓取底下一層的所有child node。
        /// </summary>
        public void GetAllChildNode(NodeArray getAllNodeArray)
        {
            BasicNode c = FirstChild;
            while (c != null)
            {
                getAllNodeArray.Add(c);
                //getAllNodeArray.nodeArray[getAllNodeArray.nowNodeAmount] = c;
                //getAllNodeArray.nowNodeAmount++;
                c = c.Sibling;
            }
        }

        static public void CountTreeAllNode(BasicNode rootNode, ref int count, Type type)
        {            
            if (rootNode.GetType() == type)
                count++;

            BasicNode c = rootNode.FirstChild;
            while (c != null)
            {
                CountTreeAllNode(c, ref count, type);
                c = c.Sibling;
            }            
        }

        /// <summary>
        /// 計算底下一層的child數量
        /// </summary>
        /// <returns></returns>
        public int CountAllChildNode()
        {
            int count = 0;
            BasicNode c = FirstChild;
            while (c != null)
            {
                count++;            
                c = c.Sibling;
            }
            return count;
        }

        /// <summary>
        /// Flattern Tree To NodeArray
        /// </summary>
        public List<BasicNode> FlatternTree()
        {
            List<BasicNode> getAllNodeArray = new List<BasicNode>(256);
           // getAllNodeArray.Clear();
      //      getAllNodeArray.Reset();
            
            GetAllNode(this, getAllNodeArray);
            return getAllNodeArray;
        }

        /// <summary>
        /// 抓出第一個名字完全符合的Node
        /// </summary>
        public BasicNode SeekFirstNodeAllName(ref string nodeName)
        {
            if (NodeName != null && NodeName.Equals(nodeName))
                return this;
            else
            {
                BasicNode children = (BasicNode)this.FirstChild;
                while (children != null)
                {
                    BasicNode seekNode = children.SeekFirstNodeAllName(ref nodeName);

                    if (seekNode != null)
                        return seekNode;
                    else
                        children = (BasicNode)children.Sibling;
                }

                return null;
            }
        }

        /// <summary>
        /// 抓出第一個名字完全符合的Node
        /// </summary>
        public BasicNode SeekFirstNodeAllName(string nodeName)
        {
            return SeekFirstNodeAllName(ref nodeName);
        }
        /// <summary>
        /// 抓出第一個名字有包含的Node
        /// </summary>
        public BasicNode SeekFirstNodeContainName(string nodeName)
        {
            if (NodeName != null && NodeName.Contains(nodeName))
                return this;
       
            BasicNode children = (BasicNode)this.FirstChild;
            while (children != null)
            {
                BasicNode seekNode = children.SeekFirstNodeContainName(nodeName);

                if (seekNode != null)
                    return seekNode;
                else
                    children = (BasicNode)children.Sibling;
            }
            return null;        
        }

        /// <summary>
        /// 由目前node一直由parent往上，取得樹的根
        /// </summary>
        public BasicNode GetRootNode()
        {
            BasicNode rootNode = this;
            while (rootNode.Parent != null)
            {
                rootNode = rootNode.Parent;
            }
            return rootNode;
        }
    }

    public class CheckList
    {
        List<object> m_CheckList = new List<object>(4096);

        public int GetListAmount()
        {
            return m_CheckList.Count;
        }

        public object AddCheckNode(object ooo)
        {        
            m_CheckList.Add(ooo);
            return ooo;
        }

        public void ReleaseCheckNode(object node)
        {
            if (m_CheckList.Remove(node) == false)
            {
                ////OutputBox.SwitchOnOff = true;
                //OutputBox.ShowMessage("Release Node Not Exist!  " + node.GetType().ToString() + ":  " + ((BasicNode)node).NodeName);
            }
        }

        public bool FinalCheck()
        {
            if (m_CheckList.Count > 0)
            {
                ////OutputBox.SwitchOnOff = true;
                //OutputBox.ShowMessage(this.GetType().ToString() + ":  m_NodeList Not Empty!  ");
                return false;
            }
            return true;
        }
    }
}
