﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace I_XNAUtility
{
    /// <summary>
    /// Example control inherits from GraphicsDeviceControl, which allows it to
    /// render using a GraphicsDevice. This control shows how to draw animating
    /// 3D graphics inside a WinForms application. It hooks the Application.Idle
    /// event, using this to invalidate the control, which will cause the animation
    /// to constantly redraw.
    /// </summary>
    abstract public partial  class SimpleXNAControl : GraphicsDeviceControl
    {
        protected ContentManager GetContent()
        {
            return m_Content;
        }

        protected SpriteBatch GetSpriteBatch()
        {
            return m_SpriteBatch;
        }
    }
}
