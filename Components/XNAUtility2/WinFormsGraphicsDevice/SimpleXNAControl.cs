﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace I_XNAUtility
{
    /// <summary>
    /// Example control inherits from GraphicsDeviceControl, which allows it to
    /// render using a GraphicsDevice. This control shows how to draw animating
    /// 3D graphics inside a WinForms application. It hooks the Application.Idle
    /// event, using this to invalidate the control, which will cause the animation
    /// to constantly redraw.
    /// </summary>
    abstract public partial   class SimpleXNAControl : GraphicsDeviceControl
    {
        /// <summary>
        /// 控制是否要render到back buffer
        /// </summary>
        public bool NotDrawBackBuffer = false;

        Stopwatch timer;
        int oldTimeCount = 0;
        ContentManager m_Content = null;
        SpriteBatch m_SpriteBatch = null;
        SpriteFont m_Font = null;

        protected abstract void SimpleLoadContent(ContentManager content, SpriteFont font);
        protected abstract void SimpleUpdate(int ellapse);
        protected abstract void SimpleRender(int ellapse);
        protected abstract void SimpleUnLoadContent();

        public SimpleXNAControl()
        {
        }

        /// <summary>
        /// Initializes the control.
        /// </summary>
        protected override void Initialize()
        {
            if (!DesignMode)
            {
                m_Content = new ContentManager(Services);
                m_SpriteBatch = new SpriteBatch(GraphicsDevice);
                m_Font = m_Content.Load<SpriteFont>("content\\SpriteFont1");

                // Start the animation timer.
                timer = Stopwatch.StartNew();
                // Hook the idle event to constantly redraw our animation.
                Application.Idle += delegate { Invalidate(); };

                SimpleLoadContent(m_Content, m_Font);
            }
        }


        /// <summary>
        /// Draws the control.
        /// </summary>
        protected override void Draw()
        {
            if (NotDrawBackBuffer)
                return;

            //要檔這個null不然關VC會當，應為這邊VC的設計工具也會跑這
            if (!DesignMode)
            {
                int ellapse = (int)timer.ElapsedMilliseconds - oldTimeCount;
          
                oldTimeCount = (int)timer.ElapsedMilliseconds;

                if (m_Content != null)
                {
           
                    SimpleUpdate(ellapse);
                    SimpleRender(ellapse);
                }
            }
        }

        /// <summary>
        /// Disposes the control.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            //要檔這個null不然關VC會當，應為這邊VC的設計工具也會跑這
            if (!DesignMode)
            {
                SimpleUnLoadContent();
                if (m_Content != null)
                {
                    m_Content.Unload();
                    m_Content.Dispose();
                    m_Content = null;
                    m_SpriteBatch.Dispose();
                    m_SpriteBatch = null;
                }
            }

            base.Dispose(disposing);
        }


    }
}
