﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.Text;
using System.IO;

namespace I_XNAUtility
{
    /// <summary>
    /// 提供繪圖或是IO的功能類別
    /// </summary>
    public static class I_XNATools
    {
        /// <summary>
        /// CopyDirectory() 函式以遞迴方式處理子目錄下檔案 http://programer.blog.richiestyle.org/2008/04/c-copydirectory.html
        /// subFileName可限制只複製特定副檔名的
        /// </summary>
        public static void CopyDirectory(string srcFolder, string dstFolder, string subFileName)
        {
            if (Directory.Exists(srcFolder) == true)
            {
                if (Directory.Exists(dstFolder) == false)
                    Directory.CreateDirectory(dstFolder);

                DirectoryInfo srcDirectory = new DirectoryInfo(srcFolder);
                FileInfo[] ffffff = null;
                if (subFileName != null)
                    ffffff = srcDirectory.GetFiles(subFileName);
                else
                    ffffff = srcDirectory.GetFiles();

                foreach (FileInfo fi in ffffff)
                {
                    try { System.IO.File.Copy(fi.FullName, dstFolder + Path.DirectorySeparatorChar + fi.Name); }
                    catch { }
                }
                foreach (DirectoryInfo di in srcDirectory.GetDirectories())
                {
                    try { CopyDirectory(di.FullName, dstFolder + Path.DirectorySeparatorChar + di.Name, subFileName); }
                    catch { }
                }
            }
        }

        ///// <summary>
        /////使用
        ///// </summary>
        //public static void DrawFullscreenQuad(Texture2D texture, int width, int height, Effect effect, SpriteBatch spriteBatch)
        //{
        //    spriteBatch.Begin(SpriteBlendMode.None,
        //                      SpriteSortMode.Immediate,
        //                      SaveStateMode.None);

        //    // Begin the custom effect.
        //    effect.Begin();
        //    effect.CurrentTechnique.Passes[0].Begin();

        //    // Draw the quad.
        //    spriteBatch.Draw(texture, new Rectangle(0, 0, width, height), Color.White);
        //    spriteBatch.End();

        //    // End the custom effect.         
        //    effect.CurrentTechnique.Passes[0].End();
        //    effect.End();
        //}

        public static class QuadRenderComponent
        {
            static bool bInit = false;
            static VertexDeclaration vertexDecl = null;
            static VertexPositionTexture[] verts = null;
            static short[] ib = null;

            static void LoadContent(GraphicsDevice device)
            {
                if (bInit)
                    return;
                bInit = true;

                vertexDecl = new VertexDeclaration(device, VertexPositionTexture.VertexElements);

                verts = new VertexPositionTexture[]
                        {
                            new VertexPositionTexture(
                                new Vector3(0,0,1),
                                new Vector2(1,1)),
                            new VertexPositionTexture(
                                new Vector3(0,0,1),
                                new Vector2(0,1)),
                            new VertexPositionTexture(
                                new Vector3(0,0,1),
                                new Vector2(0,0)),
                            new VertexPositionTexture(
                                new Vector3(0,0,1),
                                new Vector2(1,0))
                        };

                ib = new short[] { 0, 1, 2, 2, 3, 0 };
            }

            static public void Render(Vector2 v1, Vector2 v2, GraphicsDevice device)
            {           
                LoadContent(device);

                device.VertexDeclaration = vertexDecl;

                verts[0].Position.X = v2.X;
                verts[0].Position.Y = v1.Y;

                verts[1].Position.X = v1.X;
                verts[1].Position.Y = v1.Y;

                verts[2].Position.X = v1.X;
                verts[2].Position.Y = v2.Y;

                verts[3].Position.X = v2.X;
                verts[3].Position.Y = v2.Y;

                device.DrawUserIndexedPrimitives<VertexPositionTexture>
                    (PrimitiveType.TriangleList, verts, 0, 4, ib, 0, 2);

                device.VertexDeclaration = null;
            }
        }
    }

    public static class I_String
    {
        static StringBuilder m_StringBuilder = new StringBuilder(1024);

        /// <summary>
        /// 清空並設定字串，S2傳NULL就只會設定S1的值。
        /// </summary>
        public static void SetString(ref string s1, ref string s2)
        {
            m_StringBuilder.Remove(0, m_StringBuilder.Length);
            if (s2 == null)
            {
                AddString(ref s1);
            }
            else
            {
                m_StringBuilder.AppendFormat("{0}{1}", s1, s2);
            }
        }

        /// <summary>
        /// 將字串累加上去
        /// </summary>
        public static void AddString(ref string s1)
        {
            m_StringBuilder.Append(s1, 0, s1.Length);
        }

        /// <summary>
        /// 將字串累加上去
        /// </summary>
        public static void AddString(string s1)
        {
            m_StringBuilder.Append(s1, 0, s1.Length);
        }

        public static void GetString(out string sOut)
        {
            sOut = m_StringBuilder.ToString();
            m_StringBuilder.Remove(0, m_StringBuilder.Length);
        }
    }

    public static class I_Utility
    {
        public static int FPS;

        static bool bInit = false;
        public static bool IfDrawDebug = true;

        /// <summary>
        /// 程式啟動時只會執行一次
        /// </summary>
        public static void GameInitializeSetting(bool bDrawDebug, bool bContentOutSide, bool bTextureFromXNB)
        {
            if (bInit)
                return;
            bInit = true;

            IfDrawDebug = bDrawDebug;

            Utility.IfContentOutSide = bContentOutSide;
            Utility.bTextureLoadFromXNB = bTextureFromXNB;
        }

        public static void GameUpdateSetting()
        {
            I_GetTime.Update();
        }

        public static void GameUpdateRenderSetting()
        {
            FPS = Utility.getFPS();
            I_GetTime.UpdateInRender();
        }

        /// <summary>
        ///將System.Drawing.Color範圍為0~255轉換成xna.Color範圍0~1
        /// </summary>    
        public static Vector3 SystemColorToXNAColorV3(System.Drawing.Color color)
        {
            return new Vector3(color.R / 255f, color.G / 255f, color.B / 255f);
        }

        /// <summary>
        ///將System.Drawing.Color範圍為0~255轉換成xna.Color範圍0~1
        /// </summary>    
        public static Color SystemColorToXNAColor(System.Drawing.Color color)
        {
            return new Color(color.R / 255f, color.G / 255f, color.B / 255f);
        }

        /// <summary>
        ///將xna.Color範圍0~1轉換成System.Drawing.Color範圍為0~255
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static System.Drawing.Color XNAColorToSystemColor(Vector3 color)
        {
            return System.Drawing.Color.FromArgb(
                (int)(1 * 255f),
                (int)(color.X * 255f),
                (int)(color.Y * 255f),
                (int)(color.Z * 255f));
        }
    }

    public static class I_GetTime
    {
        static int oldUpdateTime = System.Environment.TickCount;
        static int oldRenderTime = System.Environment.TickCount;
        public static int ellipseUpdateTotalMillisecond = 0;
        public static int ellipseUpdateMillisecond;
        public static float ellipseUpdateSecond;

        public static int ellipseRenderMillisecond;
        public static float ellipseRenderSecond;

        public static void Update()
        {
            ellipseUpdateMillisecond = System.Environment.TickCount - oldUpdateTime;
            oldUpdateTime = System.Environment.TickCount;

            ellipseUpdateSecond = (float)ellipseUpdateMillisecond / 1000f;
            ellipseUpdateTotalMillisecond += ellipseUpdateMillisecond;
        }

        public static void UpdateInRender()
        {
            ellipseRenderMillisecond = System.Environment.TickCount - oldRenderTime;
            oldRenderTime = System.Environment.TickCount;

            ellipseRenderSecond = (float)ellipseRenderMillisecond / 1000f;
        }

        //public static int GetCurrentEllapseMillisecond()
        //{
        //    return ellipseMillisecond;
        //}

        //   public static float GetCurrentEllapseSecond()
        //{
        //    return ellipseSecond;
        //}
    }
}
