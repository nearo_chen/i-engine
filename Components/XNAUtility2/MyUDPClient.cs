﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Microsoft.Xna.Framework.Net;
using Lidgren.Network;
using Lidgren.Network.Xna;
using System.Threading;

namespace I_XNAUtility
{
    public partial class MyUDPClient : IDisposable
    {
        public enum UDPHeader : ushort
        {
            ConnectRequest,
            Connected,
            DisconnectRequest,
            Disconnected,
            DisposeUDP,
            UserDefineStart,//使用者定義封包標頭的起始號碼
        }

        public string BaseDebugString = string.Empty;
        UdpClient m_UDPClient;
        public delegate void RecieveData(ushort UserDefineHeader, NetBuffer netBuffer, IPEndPoint recieveIPE);
        RecieveData m_RecieveData;

        bool disconnecting = false;
        bool threading = false;
        int m_ThreadID;
        int m_MyPort;
        //string m_HostIP;
        IPEndPoint m_HostIP = null;
        public IPEndPoint HostIP { get { return m_HostIP; } }
        IPEndPoint RecieveIPE;// = new IPEndPoint(IPAddress.Any, 0); 
        IPEndPoint BroadCastIPE = null;
        IPEndPoint m_SelfIP = null;
        public IPEndPoint SelfIP { get { return m_SelfIP; } }

        public static NetBuffer CreateBuffer()
        {
            NetBuffer buffer = new NetBuffer();
            buffer.Reset();
            return buffer;
        }

        public static NetBuffer CreateBuffer(UDPHeader UDPHeader)
        {
            NetBuffer buffer = new NetBuffer();
            buffer.Reset();
            buffer.Write((ushort)UDPHeader);
            return buffer;
        }

        public static IPHostEntry CalculateSelfIPIPHostEntry()
        {
            IPEndPoint selfIPEndPoint = null;
            string hostNaME = System.Net.Dns.GetHostName();
            return System.Net.Dns.GetHostByName(hostNaME); ;
        }

        public static IPEndPoint CalculateSelfIPEndPoint(int port)
        {
            IPEndPoint selfIPEndPoint = null;
            IPHostEntry selfIP = CalculateSelfIPIPHostEntry();
            if (selfIP != null)
            {
                IPAddress ip = new IPAddress(selfIP.AddressList[0].Address);
                selfIPEndPoint = new IPEndPoint(ip, port);
            }
            return selfIPEndPoint;
        }

        public MyUDPClient(int port, ThreadPriority threadPriority, RecieveData recieveData)
        {
            try
            {
                m_RecieveData = recieveData;
                m_MyPort = port;
                m_UDPClient = new UdpClient(port);
                //m_UDPClient.DontFragment = true;                
                m_UDPClient.EnableBroadcast = true;
                m_ThreadID = ThreadManager.CreateThread(UpdateThread, null, "MyUDPClient Thread", threadPriority);

                m_SelfIP = CalculateSelfIPEndPoint(port);
                //string hostNaME = System.Net.Dns.GetHostName();
                //IPHostEntry selfIP = System.Net.Dns.GetHostByName(hostNaME);
                //if (selfIP != null)
                //{
                //    IPAddress ip = new IPAddress(selfIP.AddressList[0].Address);
                //    m_SelfIP = new IPEndPoint(ip, m_MyPort);
                //}

                //BroadCastIPE = new IPEndPoint(IPAddress.Broadcast, m_MyPort);//broadcast的port要設定，才能在同port之間廣播。
                //RecieveIPE = new IPEndPoint(IPAddress.Any, 0);//m_MyPort);//接收用的port隨便打個數字就好似乎沒啥意義
                BaseDebugString += "\nCreate UDP port :" + port.ToString();
                BaseDebugString += "\nNo Host...";
            }
            catch (Exception e)
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage(e.Message);
                System.Diagnostics.Debugger.Launch();
            }
        }

        public void UDPConnect(string HostIP, int port)
        {
            IPEndPoint hostIP = new IPEndPoint(IPAddress.Parse(HostIP), port);
            m_UDPClient.Connect(hostIP);
            BaseDebugString += "\nUDPConnect to Host: " + hostIP.Address.ToString() + " : " + port.ToString();
        }

        public void Connect(string HostIP, int port)
        {
            IPEndPoint hostIP = new IPEndPoint(IPAddress.Parse(HostIP), port);
            NetBuffer buff = CreateBuffer(UDPHeader.ConnectRequest);
            SendToIP(hostIP, buff);
            BaseDebugString += "\nConnectRequest to Host: " + hostIP.Address.ToString() + " : " + port.ToString();
        }

        public void Disconnect()
        {
            if (m_HostIP == null)
                return;
            //     try
            {
                disconnecting = true;
                while (disconnecting)
                {
                    lock (m_HostIP)
                    {
                        NetBuffer buff = CreateBuffer(UDPHeader.DisconnectRequest);
                        SendToIP(m_HostIP, buff);
                        BaseDebugString += "\nDisconnectRequest to Host" + m_HostIP.Address.ToString();
                    }
                    ThreadManager.Sleep(500);
                }
            }
            //catch (Exception e)
            //{
            //    //OutputBox.SwitchOnOff = true;
            //    OutputBox.ShowMessage(e.Message);
            //}
        }

        public void Dispose()
        {
            Disconnect();
            DisconnectedAll();

            while (threading)
            {
                //傳送Dispose跳出thread的訊息給自己          
                SendToIP(SelfIP, CreateBuffer(UDPHeader.DisposeUDP));
                ThreadManager.Sleep(500);
            }

            if (m_UDPClient != null)
                m_UDPClient.Close();
            m_UDPClient = null;

            ThreadManager.ReleaseThreadID(ref m_ThreadID);

        }

        //public void SendToHost(NetBuffer netBuffer)
        //{
        //    Byte[] sendBytes = netBuffer.Data;//Encoding.ASCII.GetBytes("Is anybody there?");
        //    m_UDPClient.Send(sendBytes, sendBytes.Length, m_HostIP);            
        //}

        /// <summary>
        /// 同PORT之內的廣播
        /// </summary>
        /// <param name="netBuffer"></param>
        public void BroadCast(int port, NetBuffer netBuffer)
        {
            if (BroadCastIPE == null)
            {
                if (SelfIP.Address.ToString() == "127.0.0.1")
                    BroadCastIPE = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);
                else
                    BroadCastIPE = new IPEndPoint(IPAddress.Broadcast, port);
            }

            SendToIP(BroadCastIPE, netBuffer);
        }

        public void SendToIP(IPEndPoint ip, NetBuffer netBuffer)
        {
            m_SendTimeCount += Environment.TickCount - m_OldSendTimeCount;
            m_OldSendTimeCount = Environment.TickCount;
            if (m_SendTimeCount < 1000)
            {
                m_SendCount++;
            }
            else
            {
                m_FinalSendCount = m_SendCount;
                m_SendTimeCount = 0;
                m_SendCount = 0;
            }

            try
            {
                Byte[] sendBytes = netBuffer.Data;//Encoding.ASCII.GetBytes("Is anybody there?");
                m_UDPClient.Send(sendBytes, sendBytes.Length, ip);
            }
            catch (Exception e)
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage(e.Message);

                System.Diagnostics.Debugger.Launch();
            }
        }

        void UpdateThread(object o)
        {
            threading = true;
            while (m_ThreadID != -1)
            {
                Byte[] receiveBytes = null;
                try
                {
                    receiveBytes = m_UDPClient.Receive(ref RecieveIPE);
                }
                catch (SocketException e)
                {
                    if (e.SocketErrorCode != SocketError.ConnectionReset)// e.ErrorCode != 10054)
                    {
                        //OutputBox.SwitchOnOff = true;
                        OutputBox.ShowMessage(ThreadManager.GetThreadName(m_ThreadID) + e.Message);
                        //System.Diagnostics.Debugger.Launch();
                    }
                    continue;
                }

                NetBuffer buffer = new NetBuffer();
                buffer.Reset();
                buffer.Data = receiveBytes;
                buffer.LengthBytes = receiveBytes.Length;
                ushort header = (ushort)buffer.ReadInt16();
                if (header == (ushort)UDPHeader.DisposeUDP)
                    break;

                m_RecieveTimeCount += Environment.TickCount - m_OldRecieveTimeCount;
                m_OldRecieveTimeCount = Environment.TickCount;
                if (m_RecieveTimeCount < 1000)
                    m_RecieveCount++;
                else
                {
                    m_FinalRecieveCount = m_RecieveCount;
                    m_RecieveTimeCount = 0;
                    m_RecieveCount = 0;
                }

                //BaseDebugString += "\nThis message was sent from " +
                //                 RecieveIPE.Address.ToString() +
                //                 " on their port number " +
                //                 RecieveIPE.Port.ToString();

                if (header < (ushort)UDPHeader.UserDefineStart)
                {
                    UDPHeader baseHeader = (UDPHeader)header;
                    switch (baseHeader)
                    {
                        case UDPHeader.ConnectRequest:
                            {
                                BaseDebugString += "\nRecieve ConnectRequest ==> " + RecieveIPE.Address.ToString() + ":" + RecieveIPE.Port.ToString();
                                AddClient(RecieveIPE.Address.Address, RecieveIPE.Port);
                            }
                            break;
                        case UDPHeader.Connected:
                            {
                                long address = (long)buffer.ReadInt64();
                                ushort id = (ushort)buffer.ReadInt16();
                                if (address == SelfIP.Address.Address)
                                {
                                    m_HostIP = new IPEndPoint(RecieveIPE.Address, RecieveIPE.Port);
                                    m_MyID = id;
                                }
                                else if (address == IPAddress.Parse("127.0.0.1").Address)
                                {
                                    m_HostIP = new IPEndPoint(RecieveIPE.Address, RecieveIPE.Port);
                                    m_MyID = id;
                                }

                                BaseDebugString += "\nConnected to Host" + RecieveIPE.Address.ToString() + " : " +
                                    RecieveIPE.Port.ToString() + " ; ID =>" + id.ToString();
                            }
                            break;
                        case UDPHeader.DisconnectRequest:
                            {
                                BaseDebugString += "\nRecieve DisconnectRequest ==> " + RecieveIPE.Address.ToString() + ":" + RecieveIPE.Port.ToString();
                                RemoveClient(RecieveIPE.Address.Address);
                            }
                            break;
                        case UDPHeader.Disconnected:
                            ushort playerID = (ushort)buffer.ReadInt16();
                            if (playerID == m_MyID)
                            {
                                if (m_HostIP != null)
                                {
                                    lock (m_HostIP)
                                    {
                                        BaseDebugString += "\nI am Disconnected to Host~";
                                        m_HostIP = null;
                                        disconnecting = false;
                                    }
                                }
                            }
                            else
                            {
                                BaseDebugString += "\nPlayer: " + playerID.ToString() + " Disconnected to Host~";
                            }
                            break;
                    }
                }
                else
                {
                    if (m_RecieveData != null)
                        m_RecieveData(header, buffer, RecieveIPE);
                }

                //if (m_HostIP == null)
                //{                          
                //}
                //else//有連上server就接收server
                //{
                //}

                //ThreadManager.Sleep(1);
            }

            threading = false;


        }

        public int SendCount { get { return m_FinalSendCount; } }
        public int RecieveCount { get { return m_FinalRecieveCount; } }
        int m_SendCount, m_RecieveCount, m_RecieveTimeCount, m_SendTimeCount,
            m_OldRecieveTimeCount, m_OldSendTimeCount, m_FinalSendCount, m_FinalRecieveCount;
    }
}
