﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;// windows form...
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Content.Pipeline;

namespace I_XNAUtility
{
    public static class OutputBox
    {
        //static SpriteObject Sprite1;
        //static public bool SwitchOnOff = true;//這裡開全都開，關就全都關。

        static public void initialize(GraphicsDevice g, ContentManager c)
        {
            //Sprite1 = new SpriteObject(g, c, "Arial 14", null);
        }

        static public void ShowMessage(string text)
        {
            //if (SwitchOnOff == false)
            //    return;
            //Console.WriteLine(text);
            //throw new InvalidContentException(text);
            MessageBox.Show(text);
        }

        //static public void AddScreenText(string text)
        //{
        //    Sprite1.AddText(text);
        //}
        //static public void OutputScreen()
        //{
        //    Sprite1.render(Color.AntiqueWhite, 1);
        //    Sprite1.TextClear();
        //}
    }

    public static class MyTextBoxRule
    {
        /// <summary>
        /// 轉換字串為數值，入轉換失敗就回傳0。
        /// </summary>
        /// <param name="number"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float TryToParseString(string number, int min, int max)//out fixNumber)
        {
            try
            {
                float nnn = float.Parse(number);
                nnn = MathHelper.Clamp(nnn, min, max);
            //    fixNumber = nnn.ToString();
                return nnn;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// 處理TextBox的輸入只能輸入數字
        /// </summary>
        /// <param name="nowText"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static string OnlyNumberKeyPress(string nowText, KeyPressEventArgs e)
        {
            if (string.IsNullOrEmpty(nowText))
                return nowText;

            if (e.KeyChar == '.')
            {
                e.Handled = true;

                if (nowText.Contains(".") == false)
                    e.Handled = false;//要小數點
            }
            else if (e.KeyChar == ' ')
            {
                e.Handled = true;//幹掉空白
            }
            else if (e.KeyChar == '-')
            {
                string ttt = nowText;
                if (ttt[0] != '-')
                {
                    //         ((KryptonMaskedTextBox)sender).Mask = "-" + MASK_POSITION;
                    nowText = "-" + ttt.Replace("-", "");
                }

                e.Handled = true;
            }
            else if (e.KeyChar == '+')
            {
                string ttt = nowText;
                if (ttt[0] == '-')
                {
                    //          ((KryptonMaskedTextBox)sender).Mask = MASK_POSITION;
                    nowText = ttt.Replace("-", "");
                }
                e.Handled = true;
            }
            else if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

            return nowText;
        }
    }
}
