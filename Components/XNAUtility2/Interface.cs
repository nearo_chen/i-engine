﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.Collections.ObjectModel;	//For using ‘Clollection’

namespace I_XNAUtility
{
    public enum MyButtonState
    {
        Normal = 0,
        MouseOn,
        MouseDown,
    }

    public abstract class UIComponent
    {
        static IDManager m_IDManager = new IDManager("UIComponent", 512);

        public byte LayOut = 0;
        public static int CompareLayOut(UIComponent a, UIComponent b)
        {
            return a.LayOut.CompareTo(b.LayOut);
        }

        abstract protected void v_Dispose();
        abstract protected void v_Render(GameTime gameTime, SpriteBatch spritBatch);

        protected Texture2D m_Picture = null;

        public int ID;
        public bool Show = true;
        protected string Name;
        protected GraphicsDevice m_Device;
        //protected SpriteBatch m_SpriteBatch;
        protected Color m_Color = Color.White;

        //XY和WH都是相對於parent的比例
        protected float fPosX = 0;
        protected float fPosY = 0;
        protected float fWidth = 0;
        protected float fHeight = 0;

        //計算出要畫出的pixel XYWH
        protected int RenderPosX = 0;
        protected int RenderPosY = 0;
        protected int RenderWidth = 0;
        protected int RenderHeight = 0;

        //int m_ParentPixelX;
        //int m_ParentPixelY;
        //int m_ParentPixelW;
        //int m_ParentPixelH;

        public void SetPicture(Texture2D tex)
        {
            m_Picture = tex;
        }

        public void LoadPicture(ContentManager content, string picturePath)
        {
            m_Picture = Utility.Load_Texture2D(m_Device, content, picturePath, false);
        }

        public void SetColor(byte R, byte G, byte B, byte A)
        {
            m_Color = new Color(R, G, B, A);
        }

        public void SetAlpha(byte A)
        {
            m_Color.A = A;
            //m_Color = new Color(m_Color.R, m_Color.G, m_Color.B, A);
        }

        float offsetA;
        byte  srcA;
        int DestATimeLength, DestATimeLengthCount;
        /// <summary>
        /// 設定alpha值過多少時間要變多少，1000為1秒。
        /// </summary>
        public void SetDestAlpha(byte a, int timeLength)
        {
            srcA = m_Color.A;
            offsetA = a - srcA;

            DestATimeLength = timeLength;
            DestATimeLengthCount = timeLength;
        }

        virtual public void Initialize(
            string name, float X, float Y, float W, float H, GraphicsDevice device, ContentManager content)
        {
            ID = m_IDManager.GetID();
            Name = name;
            fPosX = X; fPosY = Y; fWidth = W; fHeight = H;
            m_Device = device;           
        }

        public void SetPositionRatio(float X, float Y)
        {
            fPosX = X;
            fPosY = Y;

            m_DestPosX = X;
            m_DestPosY = Y;
         //   RefreshRenderData(m_ParentPixelX, m_ParentPixelY, m_ParentPixelW, m_ParentPixelH);
        }

        float m_DestPosX, m_DestPosY;
        int PositionAnimationTimeLength=0;
        int PositionAnimationTimeLengthCount=0;
        float m_PositionAnimationRatio = 0;
        public float GetPositionAnimationRatio()         {             return m_PositionAnimationRatio;         }
        public void SetPositionRatioAnimation(float destX, float destY, int timeLength)
        {
            m_DestPosX = destX;
            m_DestPosY = destY;
            PositionAnimationTimeLengthCount = 0;
            PositionAnimationTimeLength = timeLength;
        }

        /// <summary>
        /// 即時更新父視窗的範圍資訊
        /// </summary>
        /// <param name="parentPixelX"></param>
        /// <param name="parentPixelY"></param>
        /// <param name="parentPixelW"></param>
        /// <param name="parentPixelH"></param>
        public void RefreshRenderData(float parentPixelX, float parentPixelY, float parentPixelW, float parentPixelH)
        {
            //m_ParentPixelX = (int)parentPixelX;
            //m_ParentPixelY = (int)parentPixelY;
            //m_ParentPixelW = (int)parentPixelW;
            //m_ParentPixelH = (int)parentPixelH;
            RenderPosX = (int)(parentPixelX + fPosX * parentPixelW);
            RenderPosY = (int)(parentPixelY + fPosY * parentPixelH);
            RenderWidth = (int)(fWidth * parentPixelW);
            RenderHeight = (int)(fHeight * parentPixelH);
        }

        public void Render(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!Show)
                return;

            //做位置線性內差的動態
            if (PositionAnimationTimeLengthCount < PositionAnimationTimeLength)
            {
                m_PositionAnimationRatio = (float)PositionAnimationTimeLengthCount / (float)PositionAnimationTimeLength;

                fPosX = MathHelper.Lerp(fPosX, m_DestPosX, m_PositionAnimationRatio);
                fPosY = MathHelper.Lerp(fPosY, m_DestPosY, m_PositionAnimationRatio);

                PositionAnimationTimeLengthCount += gameTime.ElapsedRealTime.Milliseconds;
            }
            else
                m_PositionAnimationRatio = 1;


            //做alpha blending的動態
            if (DestATimeLengthCount > 0)
            {
                DestATimeLengthCount -= gameTime.ElapsedRealTime.Milliseconds;
                if (DestATimeLengthCount < 0)
                    DestATimeLengthCount = 0;
                float ratio = 1.0f - (float)DestATimeLengthCount / (float)DestATimeLength;
                m_Color.A = (byte)((float)srcA + offsetA * ratio);
            }

            v_Render(gameTime, spriteBatch);
        }

        public void Dispose()
        {
            Name = null;
    
            m_Picture = null;

            v_Dispose();
        }
    }

    //public class MyButton : UIComponent
    //{
    //    DynamicTexture m_DTNormal = null;
    //    DynamicTexture m_DTMouseOn = null;
    //    DynamicTexture m_DTMouseDown = null;

    //    MyButtonState m_ButtonState = MyButtonState.Normal;

    //    public MyButton()
    //    {
    //    }

    //    //public void AddTexture(ButtonState state, string textureName)
    //    //{
    //    //    m_DTNormal = new DynamicTexture();
    //    //    DynamicTexture.CreateDynamicTextureXML(textureName, dtMesh.fileNumberDigit, dtMesh.startPlayTime,
    //    //                        dtMesh.startFileNumber, dtMesh.endFileNumber, dtMesh.timeIntervel, dtMesh.loopTo);
    //    //    m_DTNormal.LoadDynamicTextureXML(dtMesh.textureName + ".xml", content);
    //    //}
    //}

    public class RSTPictureBox : UIComponent
    {
        override protected void v_Dispose()
        {            
        }

        float RotateRadian = 0;
        Vector2 Scale = Vector2.One;
        float m_CenterPixelX, m_CenterPixelY;
        /// <summary>
        /// 設定這picture box要會旋轉，必須要傳入圖原始尺寸的中心座標當作旋轉軸。
        /// 使用此方法旋轉圖，retangle限制畫圖範圍會無效，所以圖必須原圖尺寸就要做好。
        /// </summary>
        public void AddRotateDegree(float centerPixelX, float centerPixelY, float rotDgree)
        {
            m_CenterPixelX = centerPixelX;
            m_CenterPixelY = centerPixelY;
            RotateRadian += MathHelper.ToRadians(rotDgree);
        }

        public void SetRotateDegree(float centerPixelX, float centerPixelY, float rotDgree)
        {
            m_CenterPixelX = centerPixelX;
            m_CenterPixelY = centerPixelY;
            RotateRadian = MathHelper.ToRadians(rotDgree);
        }

        //float m_CenterPixelX, m_CenterPixelY;
        bool bRotateAnimation = false;
        bool bScaleAnimation = false;
        float m_NowDegree, m_DegreeAcc, m_DegreeVelocity;      
        public void SetRotateAnimation(float centerPixelX, float centerPixelY, float StartDegree, float velocity, float accelerate)
        {
            bRotateAnimation = true;
            m_CenterPixelX = centerPixelX;
            m_CenterPixelY = centerPixelY;

            m_NowDegree = StartDegree;
         //   m_EndDegree = EndDegree;
            m_DegreeAcc = accelerate;
            m_DegreeVelocity = velocity;
        }

        Vector2 m_NowScale;
        float m_ScaleAcc, m_ScaleVelocity;
        public void SetScaleAnimation(float centerPixelX, float centerPixelY, float StartScaleX, float StartScaleY, float velocity, float accelerate)
        {
            bScaleAnimation = true;
            m_CenterPixelX = centerPixelX;
            m_CenterPixelY = centerPixelY;

            m_NowScale.X = StartScaleX;
            m_NowScale.Y = StartScaleY;
     //       m_EndScale = EndScale;
            m_ScaleAcc = accelerate;
            m_ScaleVelocity = velocity;
        }

        public Vector2 GetNowScale() { return Scale; }
        public float GetNowRotDegree() { return MathHelper.ToDegrees(RotateRadian); }
        public void SetRotateAnimationStop() { bRotateAnimation = false; }
        public void SetScaleAnimationStop() { bScaleAnimation = false; }

        bool bScaleSinLoop = false;
        int ScaleSinLoopTimeCount = 0;
        int m_ScaleSinLoopMaxTime;
        float m_ScaleSinLoopAmplitude = 0;//震動的幅度
        float m_ScaleSinLoopFrequence = 0;//震動的頻率
        Vector2 m_OriginScale;
        float m_FreqAcc, m_AmpAcc;
        public void SetScaleSinLoop(float centerPixelX, float centerPixelY, float freq, float amplitude, float FreqAcc, float AmpAcc, int maxTime)
        {
            m_CenterPixelX = centerPixelX;
            m_CenterPixelY = centerPixelY;
            m_FreqAcc = FreqAcc;
            m_AmpAcc = AmpAcc;

            bScaleSinLoop = true;
            ScaleSinLoopTimeCount = 0;
            m_ScaleSinLoopFrequence = freq;
            m_ScaleSinLoopAmplitude = amplitude;
            m_OriginScale = Scale;
            m_ScaleSinLoopMaxTime = maxTime;
        }
        public void SetScaleSinLoopStop() { bScaleSinLoop = false; }

        public void AddScale(float centerPixelX, float centerPixelY, float scaleX, float scaleY)
        {
            m_CenterPixelX = centerPixelX;
            m_CenterPixelY = centerPixelY;
            Scale.X += scaleX;
            Scale.Y += scaleY;
        }

        public void SetScale(float centerPixelX, float centerPixelY, float scale)
        {
            SetScale(centerPixelX, centerPixelY, scale, scale);
        }

        public void SetScale(float centerPixelX, float centerPixelY, float scaleX, float scaleY)
        {
            m_CenterPixelX = centerPixelX;
            m_CenterPixelY = centerPixelY;
            Scale.X = scaleX;
            Scale.Y = scaleY;
        }

        protected override void v_Render(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (m_Picture == null)
                return;

            //更新按鈕動態資訊--------------------------------------------------------------
            float rrr = (float)gameTime.ElapsedRealTime.Milliseconds / 1000f;
            if (bRotateAnimation)
            {
                m_NowDegree += m_DegreeVelocity * rrr;
                m_DegreeVelocity += rrr * m_DegreeAcc;
                SetRotateDegree(m_CenterPixelX, m_CenterPixelY, m_NowDegree);
            }

            if (bScaleAnimation)
            {
                m_NowScale.X += m_ScaleVelocity * rrr;
                m_NowScale.Y += m_ScaleVelocity * rrr;

                m_ScaleVelocity += rrr * m_ScaleAcc;
                SetScale(m_CenterPixelX, m_CenterPixelY, m_NowScale.X, m_NowScale.Y);
            }

            if (bScaleSinLoop)
            {
                if (ScaleSinLoopTimeCount > m_ScaleSinLoopMaxTime)
                {
                    //SetScale(m_CenterPixelX, m_CenterPixelY, m_OriginScale);
                }
                else
                {
                    ScaleSinLoopTimeCount += gameTime.ElapsedRealTime.Milliseconds;
                    float rad = (float)ScaleSinLoopTimeCount * m_ScaleSinLoopFrequence;
                    m_ScaleSinLoopFrequence += rrr * m_FreqAcc;
                    if (m_ScaleSinLoopFrequence < 0)//頻率沒有在小於0的
                        m_ScaleSinLoopFrequence = 0;

                    float radX = (float)Math.Sin(rad);
                    radX = m_OriginScale.X + radX * m_ScaleSinLoopAmplitude;
                    m_ScaleSinLoopAmplitude += rrr * m_AmpAcc;

                    float radY = (float)Math.Sin(rad);
                    radY = m_OriginScale.Y + radY * m_ScaleSinLoopAmplitude;
                    m_ScaleSinLoopAmplitude += rrr * m_AmpAcc;

                    SetScale(m_CenterPixelX, m_CenterPixelY, radX, radY);
                }
            }
            //更新按鈕動態資訊-------------------------------------------------------------

            //m_SpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Texture, SaveStateMode.None
            ////    , Matrix.CreateScale(AdditionalScale)
            //    ) ;


            spriteBatch.Draw(m_Picture, new Vector2(RenderPosX, RenderPosY), 
                null, 
                m_Color,
            RotateRadian, new Vector2(m_CenterPixelX, m_CenterPixelY), Scale, SpriteEffects.None, 0.0f);

            //m_SpriteBatch.End();                
        }
    }

    public class PictureBox : UIComponent
    {
        DynamicTexture m_DynamicTexture = null;

        //特效用
        Texture2D waterfallTexture;
        Effect refractionEffect=null;
        float m_DistortMapRange, m_circleSpeedX, m_circleSpeedY, m_xScale, m_yScale;
        bool m_bCircleX, m_bCircleY;

        override protected void v_Dispose()
        {
            waterfallTexture = null;
            refractionEffect = null;
         

            if (m_DynamicTexture != null)
            {
                m_DynamicTexture.Dispose();
                m_DynamicTexture = null;
            }
                            
        }
        

        EffectParameter parDisplacementScroll;
        EffectParameter parDistortMapRange;
        EffectParameter parXScale;
        EffectParameter parYScale;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="distortTexture">distort材質路徑</param>
        /// <param name="DistortMapRange">在distort材質上取樣的範圍</param>
        /// <param name="circleSpeed">取樣範圍移動的速度</param>
        /// <param name="xScale">X振幅大小</param>
        /// <param name="xScale">Y振幅大小</param>        
        public void SetRefractionEffect(ContentManager content, string distortTexture,
            float DistortMapRange, bool bCircleX, bool bCircleY, float circleSpeedX, float circleSpeedY,
            float xScale, float yScale)
        {
            m_DistortMapRange = DistortMapRange;
            m_circleSpeedX = circleSpeedX;
            m_circleSpeedY = circleSpeedY;

            refractionEffect = Utility.ContentLoad_Effect(content, "UIEffect\\refraction");         
            refractionEffect.CurrentTechnique = refractionEffect.Techniques["Refraction"];

            waterfallTexture = Utility.Load_Texture2D(m_Device, content, distortTexture, false);
         
            m_xScale = xScale;
            m_yScale = yScale;
    
            m_bCircleX = bCircleX;
            m_bCircleY = bCircleY;

             parDisplacementScroll = refractionEffect.Parameters["DisplacementScroll"];
             parDistortMapRange = refractionEffect.Parameters["DistortMapRange"];
             parXScale = refractionEffect.Parameters["m_xScale"];
             parYScale = refractionEffect.Parameters["m_yScale"];
        }

        public void SetRefractionEffectScale(float scaleX, float scaleY)
        {
            m_xScale = scaleX;
            m_yScale = scaleY;
        }

        public void SetRefractionEffectSpeed(float speedX, float speedY)
        {
            m_circleSpeedX = speedX;
            m_circleSpeedY = speedY;
        }

        /// <summary>
        /// 圖片更新率自動設定播放間格為每秒24張
        /// </summary>
        /// <param name="textureName"></param>
        public void CreateDynamicTexture(string textureName, Digit digit, int start, int end, int loopTo, ContentManager content, GraphicsDevice device)
        {
            CreateDynamicTexture(textureName, digit, start, end, loopTo, (int)(1000f / 24f), content, device);
        }

        /// <summary>
        /// 圖片更新率必須自行設定1000/24為每秒更新24張
        /// </summary>      

        public void CreateDynamicTexture(string textureName, Digit digit, int start, int end, int loopTo, int timeFrame, ContentManager content, GraphicsDevice device)
        {
            m_DynamicTexture = new DynamicTexture();
            m_DynamicTexture.CreateDynamicTexture(textureName, digit, 0, start, end, timeFrame, loopTo, "", content, device);       
            m_DynamicTexture.Play(false, 1);
        }

        public long GetDTPlayTime()
        {
            return m_DynamicTexture.GetCountPlayTime();
        }

        public void SetTexturePlay()
        {
            m_DynamicTexture.Play(false, 1);
        }
        public void SetTexturePlay(float speed)
        {
            m_DynamicTexture.Play(false, speed);
        }
        public void SetTexturePlayBack(float speed)
        {
            m_DynamicTexture.PlayBack(false, speed);
        }
        public void SetTextureStop()
        {
            m_DynamicTexture.Stop();
        }

        public bool IfTexturePlay()
        {
            return m_DynamicTexture.IfPlay();
        }

        protected override void v_Render(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Show == false)
                return;

            if (m_DynamicTexture != null)       
            {
                m_DynamicTexture.RefreshTime(gameTime.ElapsedRealTime.Milliseconds);
                m_Picture = m_DynamicTexture.GetTexture();
            }

            if (m_Picture != null)
            {
                if (refractionEffect != null)
                {
                    //要用SpriteSortMode.Immediate畫才能使用fx
                    //m_SpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None);

                    // Set the displacement texture.
                    m_Device.Textures[1] = waterfallTexture;

                    // Set an effect parameter to make the
                    // displacement texture scroll in a giant circle.

                    SamplerState samplerState = m_Device.SamplerStates[0];
                    samplerState.AddressU = TextureAddressMode.Border;
                    samplerState.AddressV = TextureAddressMode.Border;

                    
                    Vector2 displacementScroll = Vector2.Zero;
                    if (m_bCircleX)
                    {
                        double time = gameTime.TotalGameTime.TotalSeconds * m_circleSpeedX;
                        displacementScroll.X = (float)Math.Cos(time);
                    }
                    else
                    {
                        displacementScroll.X += (float)gameTime.TotalGameTime.TotalSeconds * m_circleSpeedX;
                    }

                    if (m_bCircleY)
                    {
                        double time = gameTime.TotalGameTime.TotalSeconds * m_circleSpeedY;
                        displacementScroll.Y = (float)Math.Sin(time);
                    }
                    else
                    {
                        displacementScroll.Y += (float)gameTime.TotalGameTime.TotalSeconds * m_circleSpeedY;
                    }

                    parDisplacementScroll.SetValue(displacementScroll);
                    parDistortMapRange.SetValue(m_DistortMapRange);
                    parXScale.SetValue(m_xScale);
                    parYScale.SetValue(m_yScale);
                    
                    // Begin the custom effect.
                    refractionEffect.Begin();
                    refractionEffect.CurrentTechnique.Passes[0].Begin();

                    // Draw the sprite.
                    spriteBatch.Draw(m_Picture, new Rectangle(RenderPosX, RenderPosY, RenderWidth, RenderHeight),
                        //     MoveInCircle(gameTime, catTexture, 1),
                                     m_Color);

                    // End the sprite batch, then end our custom effect.
                    //m_SpriteBatch.End();

                    refractionEffect.CurrentTechnique.Passes[0].End();
                    refractionEffect.End();
                }              
                else
                {
                    //m_SpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Texture, SaveStateMode.None);
               
                        //Matrix.CreateRotationZ((float)Math.Sin(gameTime.TotalRealTime.Milliseconds * 0.001f) * (float)Math.PI * 2) *
                        //Matrix.CreateScale(1) * Matrix.CreateTranslation(RenderPosX, RenderPosY, 0));
              
                    spriteBatch.Draw(
                                 m_Picture, new Rectangle(RenderPosX, RenderPosY, RenderWidth, RenderHeight), m_Color);

                    //m_SpriteBatch.End();
                }
            }
        }
    }

    abstract public class MyWindow : UIComponent
    {
        abstract public void AddComponent(ContentManager content);

        List<UIComponent> UIMemberList=null;
      //  DynamicTexture m_BackGroundTexture = null;

        public MyWindow()
        {
            UIMemberList = new List<UIComponent>();
        }

        override protected void v_Dispose()
        {
            if (UIMemberList != null)
            {
                foreach (UIComponent component in UIMemberList)
                    component.Dispose();
                UIMemberList.Clear();
                UIMemberList = null;
            }
        }
        //public UIComponent GetComponent(int componentID)
        //{
        //    foreach
        //}

        override public void Initialize(
            string name, float X, float Y, float W, float H, GraphicsDevice device, ContentManager content)
        {
            base.Initialize(name, X, Y, W, H, device, content);

            AddComponent(content);
        }

        //public void CreateBackGroundTexture(string textureName, Digit digit, int start, int end, ContentManager content)
        //{
        //    m_BackGroundTexture = new DynamicTexture();
        //    DynamicTexture.CreateDynamicTextureXML(textureName, digit, 0, start, end, 1000f / 24f, start);
        //    m_BackGroundTexture.LoadDynamicTextureXML(textureName + ".xml", content);
        //}

        /// <summary>
        /// 自動設定動態圖每秒更新率為24張。
        /// </summary>     
        public PictureBox AddMemberDynamicPictureBox(string name, float X, float Y, float W, float H,
                                                                Digit digit, int start, int end, int loopTo, ContentManager content, GraphicsDevice device
            )
        {
            return AddMemberDynamicPictureBox(name, X, Y, W, H, digit, start, end, loopTo, (int)(1000f / 24f), content, device);
        }

        /// <summary>
        /// 此function細節都需自行設定，傳入的參數較複雜一點，timeFrame為1000/24表示每秒更新24張圖片。
        /// </summary>    
        public PictureBox AddMemberDynamicPictureBox(string name, float X, float Y, float W, float H, 
                                                                Digit digit, int start, int end, int loopTo, int timeFrame, ContentManager content, GraphicsDevice device
            )
        {
            PictureBox PBox = new PictureBox();
            PBox.Initialize(name, X, Y, W, H, m_Device, content);
            PBox.CreateDynamicTexture(name, digit, start, end, loopTo, timeFrame, content, device);
            UIMemberList.Add(PBox);
            return PBox;
        }

        /// <summary>
        /// 動態圖自動重複播放，每秒更新率為24張。
        /// </summary>     
        public PictureBox AddMemberDynamicPictureBox(string name, float X, float Y, float W, float H,
                                                                Digit digit, int start, int end, ContentManager content, GraphicsDevice device)
        {
            PictureBox PBox = new PictureBox();
            PBox.Initialize(name, X, Y, W, H, m_Device, content);
            PBox.CreateDynamicTexture(name, digit, start, end, start, content, device);
            UIMemberList.Add(PBox);
            return PBox;
        }

        /// <summary>
        /// 建立一個空的框，之後再決定要放的圖片。
        /// </summary>   
        public PictureBox AddMemberEmptyPictureBox(string name, float X, float Y, float W, float H, ContentManager content)
        {
            PictureBox PBox = new PictureBox();
            PBox.Initialize(name, X, Y, W, H, m_Device, content);
            UIMemberList.Add(PBox);
            return PBox;
        }

        /// <summary>
        /// 建立一個可以RST的2D介面框，name傳入null表示之後再設定圖不先load圖。
        /// </summary>     
        public RSTPictureBox AddRSTPictureBox(string name, float X, float Y, ContentManager content)
        {
            RSTPictureBox RSTBox = new RSTPictureBox();
            RSTBox.Initialize(name, X, Y, 1, 1, m_Device, content);

            if(name != null)
                RSTBox.LoadPicture(content, name);

            UIMemberList.Add(RSTBox);
            return RSTBox;
        }

        //設定圖層改變
        public void SetLayOutChange(UIComponent component, byte layout)
        {
            List<UIComponent>.Enumerator itr = UIMemberList.GetEnumerator();
            while (itr.MoveNext())
            {
                if (itr.Current == component)
                {
                    itr.Current.LayOut = layout;
                }
            }
            itr.Dispose();

            UIMemberList.Sort(UIComponent.CompareLayOut);
        }


        protected override void v_Render(GameTime gameTime, SpriteBatch spriteBatch)
        {         
            ////先畫背景
            //if (m_BackGroundTexture != null)
            //{
            //    m_BackGroundTexture.RefreshTime(EllapsedMilliseonds);
            //    m_SpriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Texture, SaveStateMode.SaveState);
            //    m_SpriteBatch.Draw(
            //        m_BackGroundTexture.GetTexture(), new Rectangle(RenderPosX, RenderPosY, RenderWidth, RenderHeight), Color.White);
            //    m_SpriteBatch.End();
            //}

            spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None);
            foreach (UIComponent ui in UIMemberList)
            {
                ui.RefreshRenderData(RenderPosX, RenderPosY, RenderWidth, RenderHeight);
                ui.Render(gameTime, spriteBatch);
            }

            spriteBatch.End();
        }

        /// <summary>
        /// 關閉所有window元件，使他不畫出。
        /// </summary>
        public void CloseAllComponent()
        {
            foreach (UIComponent ui in UIMemberList)
                ui.Show = false;
        }
    }
}
