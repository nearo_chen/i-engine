﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;


namespace I_XNAUtility
{
    public class MyObjectContainer<T> : IDisposable
    {
        public uint MaxObjectAmount { get { return m_MaxObjectAmount; } }
        uint m_MaxObjectAmount = 0;
        T [] ObjectsArray = null;
        uint nowUsedAmount = 0;

        public static void AsignAtoB(MyObjectContainer<T> A, MyObjectContainer<T> B)
        {
            B.Clear();
            for (int a = 0; a < A.nowUsedAmount; a++)
                B.GetObjectArray()[a] = A.GetObjectArray()[a];
                //B.AddObject(ObjectsArray[a]);// A.GetObjectArrayValue(a));
        }

        public static MyObjectContainer<T> CloneFetchNewContainer(MyObjectContainer<T> A)
        {
            if (A.nowUsedAmount <= 0)
                return null;

            MyObjectContainer<T> B = new MyObjectContainer<T>(A.nowUsedAmount);
            AsignAtoB(A, B);
            return B;
        }

        public uint NowUsedAmount { get { return nowUsedAmount; } }

        //public Object GetObject(int id)
        //{
        //    if (id >= nowUsedAmount) { OutputBox.ShowMessage("GetObject out of array!"); }
        //    return ((Object[])ObjectsArray)[id];
        //}

        public MyObjectContainer()
        {
        }

        ~MyObjectContainer()
        {
            Dispose();
        }

        public MyObjectContainer(uint ObjectAmount)
        {
            InitObjectContainer(ObjectAmount);
        }


        public void Dispose()
        {
            Clear();
            ObjectsArray = null;
        }

        public void InitObjectContainer(uint maxAmount)
        {
            m_MaxObjectAmount = maxAmount;
            ObjectsArray = new T[maxAmount];
        }

        //public void InitObjectContainer(uint maxAmount)
        //{
        //    MaxObjectAmount = maxAmount;
        //    ObjectsArray = new Object[maxAmount];
        //}
        //public void InitMatrixContainer(uint maxAmount)
        //{
        //    MaxObjectAmount = maxAmount;
        //    ObjectsArray = Utility.GetMatrixArray((int)maxAmount);//new Matrix[maxAmount];
        //}
        //public void InitVector3Container(uint maxAmount)
        //{
        //    MaxObjectAmount = maxAmount;
        //    ObjectsArray = Utility.GetVector3Array((int)maxAmount);//new Matrix[maxAmount];
        //}

        public void Clear()
        {
            nowUsedAmount = 0;

            if(ObjectsArray != null)
                Array.Clear(ObjectsArray, 0, ObjectsArray.Length);
        }

        public void AddObject(T ooo)
        {
            //if (nowUsedAmount >= MaxObjectAmount) 
            //{
            //    //OutputBox.SwitchOnOff = true;
            //    OutputBox.ShowMessage("MyObjectContainer: Array Full!");
            //    return;
            //}

            ObjectsArray[nowUsedAmount] = ooo;
            //((Array)ObjectsArray).SetValue(ooo, nowUsedAmount);
            nowUsedAmount++;
        }

        public T[] GetObjectArray()
        {
            return ObjectsArray;
        }

        public T GetObjectArrayValue(int index)
        {
            return ObjectsArray[index];
            //return ObjectsArray.GetValue(index);
        }

        /// <summary>
        /// 回傳目前有資訊的新陣列
        /// </summary>
        /// <returns></returns>
        //public Array GetUsedObjectArray()
        //{
        //    Object[] aaa = new Object[nowUsedAmount];
        //    for (int a = 0; a < aaa.Length; a++)
        //    {
        //        aaa[a] = ObjectsArray.GetValue(a);
        //    }
        //    return aaa;
        //}

        //public Matrix[] GetMatrixArray()
        //{ 
        //}

        //static Matrix ObjectToMatrix(Object obj)
        //{
        //    return 
        //}
    }
}
