﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace I_XNAUtility
{
    public static class Collision
    {
        [Serializable()]
        public struct IBVB
        {
            public uint[] ib;
            public Vector3[] vb;
        }

        static public void SaveIBVB(string fileName, ModelMesh modelMesh, Matrix world)
        {
            List<uint> ib = new List<uint>();
            List<Vector3> vb = new List<Vector3>();
            //int partID;
            ////ModelMesh modelMesh = nnn.GetModelMeshPart(out partID);
            Collision.GetMeshVBIB(modelMesh, ib, vb, null, null, null, null);
            Collision.IBVB ibvb;
            ibvb.ib = ib.ToArray();       
            ibvb.vb = vb.ToArray();
            Vector3.Transform(ibvb.vb, ref world, ibvb.vb);
            Utility.SaveBinFile(fileName, ibvb);
        }
        static public Collision.IBVB LoadIBVB(string fileName)
        {
            return (Collision.IBVB)Utility.LoadBinFile(fileName);
        }

        public struct byte4
        {
            public byte b1, b2, b3, b4;          
        }

        /// <summary>
        /// Get Vertex buffer, index buffer...else, from a model.
        /// </summary>
        static public unsafe void GetMeshVBIB(ModelMesh mesh, List<uint> indexList, List<Vector3> positionList, List<Vector3> normalList,
            List<Vector2> texCoordList, List<byte4> boneIndexList, List<Vector4> boneWeightList)
        {
            if (mesh.IndexBuffer == null)
                return;

            if (indexList != null)
            {
                //List<Vector3> positions = new List<Vector3>();
                //List<Vector3> normals = new List<Vector3>();
                //List<Vector2> texcoords = new List<Vector2>();
                //List<short> index = new List<short>();
                int indexOffset = 0;

                //Get Index buffer
                //if (mesh.IndexBuffer.IndexElementSize == IndexElementSize.ThirtyTwoBits)
                //    throw new InvalidOperationException("CreateBoundingBox: It's not safe to use IndexElementSize.ThirtyTwoBits");
                if (mesh.IndexBuffer.SizeInBytes % 2 != 0)
                    throw new InvalidOperationException("CreateBoundingBox: mesh.IndexBuffer.SizeInBytes % 2 !=0");

                ushort[] tmpIndexArray16=null;
                uint [] tmpIndexArray32=null;
                if (mesh.IndexBuffer.IndexElementSize == IndexElementSize.ThirtyTwoBits)
                {
                    tmpIndexArray32 = new uint[mesh.IndexBuffer.SizeInBytes / 4];
                    mesh.IndexBuffer.GetData<uint>(tmpIndexArray32);
                }
                else
                {
                    tmpIndexArray16 = new ushort[mesh.IndexBuffer.SizeInBytes / 2];
                    mesh.IndexBuffer.GetData<ushort>(tmpIndexArray16);
                }

                foreach (ModelMeshPart meshpart in mesh.MeshParts)
                {
                    for (int i = meshpart.StartIndex; i < meshpart.StartIndex + meshpart.PrimitiveCount * 3; i++)
                    {
                        if(tmpIndexArray32 != null)
                            indexList.Insert(0, (uint)(tmpIndexArray32[i] + indexOffset)); 
                        else
                            indexList.Insert(0, (uint)(tmpIndexArray16[i] + indexOffset));
                        //indexList.Add((int)(tmpIndexArray[i] + indexOffset));

                        //calculate maxIndex
                        //if (tmpIndexArray[i] + indexOffset >= maxIndex)
                        //    maxIndex = tmpIndexArray[i] + indexOffset;
                    }

                    indexOffset += meshpart.NumVertices;
                }
            }

            //Get all vertex buffer
            VertexElement[] VE = mesh.MeshParts[0].VertexDeclaration.GetVertexElements();
            int vertexstride = mesh.MeshParts[0].VertexStride;
            if (mesh.VertexBuffer.SizeInBytes % vertexstride != 0)
                throw new InvalidOperationException("CreateBoundingBox: mesh.VertexBuffer.SizeInBytes % (12 + 12 + 8) != 0");

            byte[] VertexDataArray = new byte[mesh.VertexBuffer.SizeInBytes];
            mesh.VertexBuffer.GetData<byte>(VertexDataArray);

            //unsafe to get Vertex Data in byte array...
            int offsetByte = 0;
            Vector3 vec3;
            Vector2 vec2;
            byte4 boneIndex;
            Color boneWeight;
            Vector4 boneWeightV4;
            while (offsetByte < VertexDataArray.Length)
            {
                //get data according VertexElementUsage flow....             
                for(int a=0 ; a<VE.Length ; a++) 
                {
                    VertexElement ve = VE[a];
                    switch (ve.VertexElementUsage)
                    {
                        case VertexElementUsage.Position:
                            {
                                fixed (byte* pSrc = &VertexDataArray[offsetByte + ve.Offset])
                                {
                                    Utility.MyMemCopy(pSrc, (byte*)&vec3, sizeof(Vector3));
                                }
                                if(positionList!=null)
                                    positionList.Add(vec3);
                            }
                            break;
                        case VertexElementUsage.Normal:
                            {
                                fixed (byte* pSrc = &VertexDataArray[offsetByte + ve.Offset])
                                {
                                    Utility.MyMemCopy(pSrc, (byte*)&vec3, sizeof(Vector3));
                                }
                                if(normalList!=null)
                                    normalList.Add(vec3);                           
                            }
                            break;
                        case VertexElementUsage.TextureCoordinate:
                            {
                                if (ve.UsageIndex > 0)//只抓第一層貼圖
                                    break;
                                fixed (byte* pSrc = &VertexDataArray[offsetByte + ve.Offset])
                                {
                                    Utility.MyMemCopy(pSrc, (byte*)&vec2, sizeof(Vector2));
                                }
                                if (texCoordList != null)
                                    texCoordList.Add(vec2);                         
                            }
                            break;
                        case VertexElementUsage.BlendIndices:
                            {
                                fixed (byte* pSrc = &VertexDataArray[offsetByte + ve.Offset])
                                {
                                    Utility.MyMemCopy(pSrc, (byte*)&boneIndex, sizeof(byte4));
                                }
                                if (boneIndexList != null)
                                    boneIndexList.Add(boneIndex);                           
                            }
                            break;
                        case VertexElementUsage.BlendWeight:
                            {
                                fixed (byte* pSrc = &VertexDataArray[offsetByte + ve.Offset])
                                {
                                    if (ve.VertexElementFormat == VertexElementFormat.Color)
                                    {
                                        Utility.MyMemCopy(pSrc, (byte*)&boneWeight, sizeof(Color));
                                        boneWeightV4 = boneWeight.ToVector4();
                                    }
                                    else
                                        Utility.MyMemCopy(pSrc, (byte*)&boneWeightV4, sizeof(Vector4));
                                }
                                if (boneWeightList != null)
                                    boneWeightList.Add(boneWeightV4);                           
                            }
                            break;
                        default:
                            {
                                ////OutputBox.SwitchOnOff = true;
                                //OutputBox.ShowMessage("GetMeshVBIB vertex element get lost!!!");
                            }
                            break;
                    }
                }
                offsetByte += vertexstride;
            }


           // indexBuffer = index.ToArray();
            //positionBuffer = positions.ToArray();
         //   VertexNormalArray = normals.ToArray();
          //  VertexTexArray = texcoords.ToArray();
        }

        //static public void GetModelVB(Model model, out int[] IndexArray, out Vector3[] VertexPositionArray)
        //{
        //}

        static public void GetModelVBIB(Model model, out uint[] IndexArray, out Vector3[] VertexPositionArray)
        {
            GetModelVBIB(model, out IndexArray, out VertexPositionArray, Vector3.One);
        }
        static public void GetModelVBIB(Model model, out uint [] IndexArray, out Vector3[] VertexPositionArray, Vector3 scale)
        {
            List<Vector3> positionList = new List<Vector3>();
           // List<Vector3> normals = new List<Vector3>();
           // List<Vector2> texcoords = new List<Vector2>();
            List<uint> indexList = new List<uint>();
            //int indexOffset = 0;
            //int maxIndex = 0;

            Matrix[] transforms = Utility.GetMatrixArray(model.Bones.Count);//new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
            for (int a = 0; a < transforms.Length; a++)
                transforms[a] = Matrix.CreateScale(scale) * transforms[a];

            foreach (ModelMesh mesh in model.Meshes)
            {
                List<Vector3> tmpPositionList = new List<Vector3>();
                GetMeshVBIB(mesh, indexList, tmpPositionList, null, null, null, null);

               // Vector3[] positionBuffer = tmpPositionList.ToArray();
                //foreach (Vector3 pos in tmpPositionList)
                //    positionList.Add(Vector3.Transform(pos, transforms[mesh.ParentBone.Index]));

                List<Vector3>.Enumerator itr = tmpPositionList.GetEnumerator();
                while(itr.MoveNext())
                    positionList.Add(Vector3.Transform(itr.Current, transforms[mesh.ParentBone.Index]));
                itr.Dispose();

                tmpPositionList.Clear();
                tmpPositionList = null;
            }

            //check max index match vertex buffer...
            //if (maxIndex != positions.Count - 1)
            //{
            //    throw new InvalidOperationException("maxIndex != vertices.Count - 1");
            //}

            //assign value...
            IndexArray = null;
            VertexPositionArray = null;
            //VertexNormalArray = null;
            //VertexTexArray = null;

            //indexList.Reverse();
            if (indexList.Count > 0)
                IndexArray = indexList.ToArray();
            if (positionList.Count > 0)
                VertexPositionArray = positionList.ToArray();
            //if (normals.Count > 0)
            //    VertexNormalArray = normals.ToArray();
            //if (texcoords.Count > 0)
            //    VertexTexArray = texcoords.ToArray();

            indexList.Clear();
            positionList.Clear();
            indexList = null;
            positionList = null;
            //normals.Clear();
            //texcoords.Clear();
            
        }

        //檢測模型Bounding sphere與ray的碰撞測試,回傳與模型的最短距離
        //Sphere偵測到有SCALE的東西會有錯...
        static public float? RayIntersectBoundSphere(Matrix world, Ray ray, BoundingSphere sphere)
        {
            //transform ray into local space....
            //Matrix inverseTransform = Matrix.Invert(world);
            //ray.Position = Vector3.Transform(ray.Position, inverseTransform);
            //ray.Direction = Vector3.TransformNormal(ray.Direction, inverseTransform);

            //ray transform into local space,i can't resolve scale problem...so i scale radius and transform center into world space...
            //BoundingSphere sphere = boundingSphere;
            Vector3.Transform(ref sphere.Center, ref world, out sphere.Center);
            //sphere.Radius *= scale;

            float? distance = null;
            sphere.Intersects(ref ray, out distance);
            //if (distance == null)
            //{
            //    ContainmentType type = ContainmentType.Disjoint;
            //    sphere.Contains(ref ray.Position, out type);
            //    if (type != ContainmentType.Disjoint)
            //    {
            //        distance = float.MaxValue;
            //    }
            //}
            return distance;
        }

        //檢測模型Bounding sphere與ray的碰撞測試,回傳與模型的最短距離
        static public float? RayIntersectBoundBox(Matrix world, Ray ray, BoundingBox boundbox)
        {
            //transform ray into local space....
            Matrix inverseTransform = Matrix.Invert(world);
            Vector3.Transform(ref ray.Position, ref inverseTransform, out ray.Position);
            Vector3.TransformNormal(ref ray.Direction, ref inverseTransform, out ray.Direction);

            float? distance = null;
            boundbox.Intersects(ref ray, out distance);
            if (distance == null)
            {
                if (boundbox.Contains(ray.Position) != ContainmentType.Disjoint)
                {
                    distance = float.MaxValue;
                }
            }
            return distance;
        }

        //檢測模型Bounding frustrum與ray的碰撞測試,回傳與模型的最短距離
        static public float? RayIntersectBoundFrustrum(Matrix world, Ray ray, Matrix BoundFrustumMatrix)
        {
            //refresh bounding frustrum
            //update bounding box offset possition
            //Matrix tmpTranOffset = Matrix.CreateTranslation(BoundBoxOffsetPos) * world;
            //orthogornal projection will invert matrix, so we invert before matrix multiply...
            //Matrix tmpWorldInvert = Matrix.Invert(Matrix.CreateTranslation(BoundBoxOffsetPos)* world);
            BoundingFrustum boundfrustum =
                Utility.TransformFrustum(world, BoundFrustumMatrix);

            float? distance = null;
                boundfrustum.Intersects(ref ray, out distance);
            return distance;
        }

        //檢測模型triangle與ray的碰撞測試,回傳與模型的最短距離
        static Vector3 m_CollideTriangleNormal;
        static public Vector3 CollideTriangleNormal
        {
            get { return m_CollideTriangleNormal; }
        }
        static Vector3[] m_ColTriangle = new Vector3[3];
        static public Vector3 CollideTrianglePointA
        {            get { return m_ColTriangle[0]; }        }
        static public Vector3 CollideTrianglePointB
        { get { return m_ColTriangle[1]; } }
        static public Vector3 CollideTrianglePointC
        { get { return m_ColTriangle[2]; } }
        static public float? RayIntersectModel(Matrix world, Ray ray, uint [] IndexBuffer, Vector3[] VertexPositionArray)
        {
            //transform ray into local space.................
            Matrix inverseTransform = Matrix.Invert(world);
            ray.Position = Vector3.Transform(ray.Position, inverseTransform);
            ray.Direction = Vector3.TransformNormal(ray.Direction, inverseTransform);

            float tmpNearest = float.MaxValue;
            float? nearDistance = null;

            for (int i = 0; i < IndexBuffer.Length; i += 3)
            {
                // Perform a ray to triangle intersection test.
                float? intersection =
                RayIntersectsTriangle(ref ray,
                                      ref VertexPositionArray[IndexBuffer[i]],
                                      ref VertexPositionArray[IndexBuffer[i + 1]],
                                      ref VertexPositionArray[IndexBuffer[i + 2]]);

                // Does the ray intersect this triangle?
                if (intersection != null)
                {
                    if (tmpNearest > intersection)
                    {
                        tmpNearest = (float)intersection;
                        // Store the distance to this triangle.
                        nearDistance = intersection;

                        //Vector3 p1 = Vector3.Transform(VertexPositionArray[IndexBuffer[i]], world);
                        //Vector3 p2 = Vector3.Transform(VertexPositionArray[IndexBuffer[i+1]], world);
                        //Vector3 p3 = Vector3.Transform(VertexPositionArray[IndexBuffer[i+2]], world);
                        //Vector3 vec1 = p2 - p1;
                        //Vector3 vec2 = p3 - p1;
                        ////Vector3 vec1 = Vector3.Normalize(p2 - p1);
                        ////Vector3 vec2 = Vector3.Normalize(p3 - p1);
                        // m_CollideTriangleNormal = Vector3.Cross(vec2, vec1);
                        //m_CollideTriangleNormal = Vector3.Normalize(m_CollideTriangleNormal);

                        Vector3 vec1 = VertexPositionArray[IndexBuffer[i + 1]] - VertexPositionArray[IndexBuffer[i]];
                        Vector3 vec2 = VertexPositionArray[IndexBuffer[i + 2]] - VertexPositionArray[IndexBuffer[i]];
                 //       vec1 = Vector3.Normalize(vec1);
                 //       vec2 = Vector3.Normalize(vec2);
                        m_CollideTriangleNormal = Vector3.Cross(vec2, vec1);
                        //m_CollideTriangleNormal = Vector3.Normalize(m_CollideTriangleNormal);
                        m_CollideTriangleNormal = Vector3.TransformNormal(m_CollideTriangleNormal, world);
                        m_CollideTriangleNormal = Vector3.Normalize(m_CollideTriangleNormal);


                        Vector3.Transform(ref VertexPositionArray[IndexBuffer[i]], ref world, out m_ColTriangle[0]);
                        Vector3.Transform(ref VertexPositionArray[IndexBuffer[i + 1]], ref world, out m_ColTriangle[1]);
                        Vector3.Transform(ref VertexPositionArray[IndexBuffer[i + 2]], ref world, out m_ColTriangle[2]);
                    }
                }
            }

            return nearDistance;
        }

        static public float? RayIntersectsTriangle(ref Ray ray,
                                  ref Vector3 vertex1,
                                  ref Vector3 vertex2,
                                  ref Vector3 vertex3)
        {
            float? result;
            // Compute vectors along two edges of the triangle.
            Vector3 edge1, edge2;

            Vector3.Subtract(ref vertex2, ref vertex1, out edge1);
            Vector3.Subtract(ref vertex3, ref vertex1, out edge2);

            // Compute the determinant.
            Vector3 directionCrossEdge2;
            Vector3.Cross(ref ray.Direction, ref edge2, out directionCrossEdge2);

            float determinant;
            Vector3.Dot(ref edge1, ref directionCrossEdge2, out determinant);

            // If the ray is parallel to the triangle plane, there is no collision.
            if (determinant > -float.Epsilon && determinant < float.Epsilon)
            {
                result = null;
                return result;
            }

            float inverseDeterminant = 1.0f / determinant;

            // Calculate the U parameter of the intersection point.
            Vector3 distanceVector;
            Vector3.Subtract(ref ray.Position, ref vertex1, out distanceVector);

            float triangleU;
            Vector3.Dot(ref distanceVector, ref directionCrossEdge2, out triangleU);
            triangleU *= inverseDeterminant;

            // Make sure it is inside the triangle.
            if (triangleU < 0 || triangleU > 1)
            {
                result = null;
                return result;
            }

            // Calculate the V parameter of the intersection point.
            Vector3 distanceCrossEdge1;
            Vector3.Cross(ref distanceVector, ref edge1, out distanceCrossEdge1);

            float triangleV;
            Vector3.Dot(ref ray.Direction, ref distanceCrossEdge1, out triangleV);
            triangleV *= inverseDeterminant;

            // Make sure it is inside the triangle.
            if (triangleV < 0 || triangleU + triangleV > 1)
            {
                result = null;
                return result;
            }

            // Compute the distance along the ray to the triangle.
            float rayDistance;
            Vector3.Dot(ref edge2, ref distanceCrossEdge1, out rayDistance);
            rayDistance *= inverseDeterminant;

            // Is the triangle behind the ray origin?
            if (rayDistance < 0)
            {
                result = null;
                return result;
            }

            result = rayDistance;
            return result;
        }

        /// <summary>
        /// frustum contain frustum calculating has some fault .......
        /// </summary>
        static public bool FrustumContainFrustum(ref Matrix bigFrustumMatrix, BoundingFrustum smallFrustum, ref Matrix smallFrustumWorld)
        {
            //transform bigFrustum into local space....
            //get local space...transform bigFrustumMatrix into small frustum space...
            //Matrix smallTransformMatrix = Matrix.Invert(smallFrustumWorld);

            //transfer frustum into node local space...
            //BoundingFrustum bigFrustum = Utility.TransformFrustum(smallTransformMatrix, bigFrustumMatrix);
            BoundingFrustum bigFrustum = new BoundingFrustum(smallFrustumWorld * bigFrustumMatrix);

            ContainmentType containtype = bigFrustum.Contains(smallFrustum);
            if (containtype == ContainmentType.Disjoint)
                return false;
            return true;
        }

     

        static public bool FrustumContainBox(ref Matrix bigFrustumMatrix, BoundingBox smallBox, ref Matrix smallBoxWorld)
        {
            //transform bigFrustum into local space....

            //get local space...transform bigFrustumMatrix into small space...
            //Matrix smallTransformMatrix = Matrix.Invert(smallBoxWorld);

            //transfer frustum into node local space...and also we resole scale problem!
            //BoundingFrustum bigFrustum = Utility.TransformFrustum(smallTransformMatrix, bigFrustumMatrix);
            BoundingFrustum bigFrustum = new BoundingFrustum(smallBoxWorld * bigFrustumMatrix);

            ContainmentType containtype = ContainmentType.Disjoint;
            bigFrustum.Contains(ref smallBox, out containtype);
            if (containtype == ContainmentType.Disjoint)
                return false;
            return true;
        }

        //frustum 對ＳＣＡＬＥ之後的node,碰撞測試似乎都正常
        static public bool FrustumContainSphere(ref Matrix bigFrustumMatrix, BoundingSphere smallSphere, ref Matrix smallBoxWorld)
        {
            //transform bigFrustum into local space....

            //get local space...transform bigFrustumMatrix into small space...
            //Matrix smallTransformMatrix = Matrix.Invert(smallBoxWorld);

            //transfer frustum into node local space...and also we resole scale problem!
            //BoundingFrustum bigFrustum = Utility.TransformFrustum(smallTransformMatrix, bigFrustumMatrix);
            BoundingFrustum bigFrustum = new BoundingFrustum(smallBoxWorld * bigFrustumMatrix);

            ContainmentType containtype = ContainmentType.Disjoint;
            bigFrustum.Contains(ref smallSphere, out containtype);
            if (containtype == ContainmentType.Disjoint)
                return false;
            return true;
        }

        //static public bool SphereIntersectBoundSphere(Matrix world, BoundingSphere sphereA, BoundingSphere sphereB)
        //{
        //    //transform ray into local space....
        //    //Matrix inverseTransform = Matrix.Invert(world);
        //    //ray.Position = Vector3.Transform(ray.Position, inverseTransform);
        //    //ray.Direction = Vector3.TransformNormal(ray.Direction, inverseTransform);

        //    //ray transform into local space,i can't resolve scale problem...so i scale radius and transform center into world space...
        //    //BoundingSphere sphere = boundingSphere;
        //    Vector3.Transform(ref sphereB.Center, ref world, out sphereB.Center);

        //    if (sphereB.Intersects(sphereA))
        //        return true;
        //    return false;        
        //}  
    }
}
