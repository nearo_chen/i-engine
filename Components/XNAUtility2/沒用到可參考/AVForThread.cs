﻿using System;
using System.Collections.Generic;
using System.Threading;

using MyWaitCallback = System.Threading.WaitCallback;

namespace I_XNAUtility
{
    /// <summary>
    /// delegate for "for" thread
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    public delegate void avForThreadDeleg(int start, int end);

    /// <summary>
    /// avForThread is a very simple version of the kind of operations possible with OpenMP 
    /// eg:
    ///    int [] data = new int[1000];
    ///    using( new avForThread( 0, 1000, 4, 
    ///       delegate( int start, int end )
    ///       {
    ///            for( int i=start; i (lessthan) end; i++)
    ///            {
    ///                 data[i] = calculateSomething(i);
    ///				}
    ///		  })
    /// 	  );
    /// 
    /// </summary>
    public class avForThread : IDisposable
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="start">the starting value range</param>
        /// <param name="end">the neding value range</param>
        /// <param name="numThreads">the number of threads to use</param>
        /// <param name="deleg">the function to call</param>
        public avForThread(int start, int end, int numThreads, avForThreadDeleg deleg)
        {
            Threads = new Thread[numThreads];
            Deleg = deleg;
            threadDone = new bool[numThreads];
            for (int i = 0; i < numThreads; i++)
            {
                Threads[i] = new Thread(startThread);

                int steps = (end - start);
                int tStart = (i * steps) / numThreads;
                int tEnd = ((i + 1) * steps) / numThreads - 1;

                Threads[i].Start(new int[] { tStart, tEnd });
            }
        }


        /// <summary>
        /// dont call this
        /// </summary>
        public void Dispose()
        {
            if (disposed == false)
                WaitToFinish();
            disposed = true;
        }

        public void WaitToFinish()
        {
            if (disposed) return;

            bool done = false;
            while (!done)
            {
                done = true;
                for (int i = 0; i < Threads.Length; i++)
                {
                    if (threadDone[i] == false && Threads[i].IsAlive)
                    {
                        done = false;
                        threadDone[i] = true;
                        break;
                    }
                }
            }
            Thread.Sleep(10);
        }

        #region privates
        void startThread(object o)
        {
            int[] startEnd = o as int[];
            Deleg(startEnd[0], startEnd[1]);
        }
        avForThreadDeleg Deleg;
        bool disposed;
        bool[] threadDone;
        Thread[] Threads;
        #endregion
    }

    public class MyAVThread : IDisposable
    {
        CalculateInThreadPoolData[] m_CalculateInThreadPoolDataList = null;
        class CalculateInThreadPoolData
        {
            public CalculateInThreadPoolData(int start, int end, object obj, MyThreadPoolFunction funcCallback)
            {
                m_Start = start;
                m_End = end;
                m_Done = false;
                m_FuncCallback = funcCallback;
                m_Obj = obj;
            }
            public int m_Start, m_End;
            public object m_Obj;
            public bool m_Done;
            public MyThreadPoolFunction m_FuncCallback;
        }

        public delegate void MyThreadPoolFunction(object myObj, int arrayID);

        void CalculateInThreadPool(object obj)
        {
            CalculateInThreadPoolData ddd = (CalculateInThreadPoolData)obj;
            for (int i = ddd.m_Start; i < ddd.m_End; i++)
                ddd.m_FuncCallback(ddd.m_Obj, i);
            ddd.m_Done = true;
        }

        public MyAVThread(int start, int end, int numThreads, MyThreadPoolFunction funcCallback, object obj)
        {
            //lock (this)
            //{
                m_CalculateInThreadPoolDataList = new CalculateInThreadPoolData[numThreads];
                for (int i = 0; i < numThreads; i++)
                {
                    int steps = (end - start);
                    int tStart = (i * steps) / numThreads;
                    int tEnd = ((i + 1) * steps) / numThreads;

                    CalculateInThreadPoolData ddd = new CalculateInThreadPoolData(tStart, tEnd, obj, funcCallback);
                    ThreadPool.QueueUserWorkItem(CalculateInThreadPool, ddd);

                    m_CalculateInThreadPoolDataList[i] = ddd;
                }
            //}
        }

        ~MyAVThread()
        {
            Dispose();
        }

        bool disposed;
        public void Dispose()
        {
            if (!disposed)
                WaitToFinish();
            disposed = true;
         }

        public void WaitToFinish()
        {
            if (disposed) return;

            //lock (this)
            //{
                bool done = false;
                while (done == false)
                {
                    done = true;
                    for (int i = 0; i < m_CalculateInThreadPoolDataList.Length; i++)
                    {
                        if (m_CalculateInThreadPoolDataList[i].m_Done == false)
                        {
                            done = false;
                            break;
                        }
                    }
                }
            //}
            //Thread.Sleep(10);
        }
    }
}