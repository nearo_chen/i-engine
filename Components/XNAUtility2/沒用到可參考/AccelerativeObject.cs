﻿using System;
using Microsoft.Xna.Framework;

namespace I_XNAUtility
{
    public class AccelerateObject
    {
        Vector3 m_Velocity;

        public Vector3 Velocity { get { return m_Velocity; } set { m_Velocity = value; } }
        //------------------------------------------------------------------------------------
        public AccelerateObject()
        {
            m_Velocity = Vector3.Zero;
        }



        //------------------------------------------------------------------------------------
        /// <summary>
        /// 增加一個向量到原本的方向與力量上
        /// </summary>
        public void AddForce(Vector3 addForce)
        {
            m_Velocity += addForce;
        }

       //------------------------------------------------------------------------------------

        public void GetDirectionAndForce(out Vector3 direction, out float force)
        {
            force = m_Velocity.Length();

            if (force != 0)
            {
                direction.X = m_Velocity.X / force;
                direction.Y = m_Velocity.Y / force;
                direction.Z = m_Velocity.Z / force;
                //direction = new Vector3(m_Velocity.X / force, m_Velocity.Y / force, m_Velocity.Z / force);
            }
            else
                direction = Vector3.Zero;
        }



        ////------------------------------------------------------------------------------------
        ///// <summary>
        ///// 單純改變目前力量的方向
        ///// </summary>
        //public void ChangeNormal(Vector3 normal)
        //{
        //     float length = m_ForceVec.Length();
        //     m_ForceVec = length * normal;
        //}



        //------------------------------------------------------------------------------------
        /// <summary>
        /// 減少目前的力量
        /// </summary>
        public void AddFriction(float friction, float EllapseMillionSecond)
        {
            if (friction >= 0)
            {
                Vector3 direction;
                float force;

                GetDirectionAndForce(out direction, out force);

                force = force - (friction * EllapseMillionSecond / 1000.0f);
                if (force <= 0)
                {
                    force = 0;
                }

                Velocity = force * direction;
            }
        }


        ////------------------------------------------------------------------------------------
        ///// <summary>
        ///// 減少目前的力量，並取得normal與force
        ///// </summary>
        //public void GetDecreasedForceAndNormal(float friction, float time, out Vector3 normal, out float force)
        //{
        //    GetNormalAndForce(out normal, out force);

        //    force = force - (friction * time);
        //    if (force <= 0)
        //    {
        //        force = 0;
        //    }
        //    m_ForceVec = force * normal;
        //}
    }
}
