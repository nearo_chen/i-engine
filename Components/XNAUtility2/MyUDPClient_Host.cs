﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Microsoft.Xna.Framework.Net;
using Lidgren.Network;
using Lidgren.Network.Xna;
using System.Threading;

namespace I_XNAUtility
{
    public partial class MyUDPClient : IDisposable
    {
        //當Host用的參數==================================================
        public class ClientData
        {
            public ClientData(IPEndPoint ipe, int id)
            {
                IPE = ipe;
                ID = id;
            }

            public IPEndPoint IPE;
            public int ID = -1;
        }

        //Dictionary<long, IPEndPoint> m_ClientIPDictionary = new Dictionary<long, IPEndPoint>();       
        MyObjectContainer<ClientData> m_ClientArray = new MyObjectContainer<ClientData>(256);
        ushort m_MyID = ushort.MaxValue;
        public ushort MyID { get { return m_MyID; } }

        public MyObjectContainer<ClientData> ClientList
        {
            get { return m_ClientArray; }
        }

        /// <summary>
        /// 如果sever關閉就使用這個告訴大家。
        /// </summary>
        public void DisconnectedAll()
        {
            for (int a = 0; a < m_ClientArray.NowUsedAmount; a++)
            {
                ClientData clientData = m_ClientArray.GetObjectArrayValue(a) as ClientData;
                if (clientData.IPE.Address.Address != 0)
                    RemoveClient(clientData.IPE.Address.Address);
            }
            m_ClientArray.Clear();
        }

        public int CheckClientID(long address)
        {
            for (int a = 0; a < m_ClientArray.NowUsedAmount; a++)
            {
                ClientData clientData = m_ClientArray.GetObjectArrayValue(a) as ClientData;
                if (clientData.IPE.Address.Address == address)
                    return a;
            }
            return -1;
        }

        bool AddClient(long address, int port)
        {
            int A = CheckClientID(address);
            if (A != -1)
            {
                BaseDebugString += "\nAdd deny ~";
                return false;
            }

            ushort id = (ushort)(m_ClientArray.NowUsedAmount);
            ClientData clientData = new ClientData(new IPEndPoint(address, port), id);
            m_ClientArray.AddObject(clientData);

            NetBuffer buff = CreateBuffer(UDPHeader.Connected);

            buff.Write((Int64)clientData.IPE.Address.Address);
            buff.Write(id);//塞ID給他
            
            SendToAllClient(buff);

            BaseDebugString += "\nAdd success ~ ID : " + id.ToString();
            return true;
        }

        bool RemoveClient(long address)
        {
            int A = CheckClientID(address);
            if (A == -1)
            {
                BaseDebugString += "\nRemove deny ~";
                return false;
            }

            NetBuffer buff = CreateBuffer(UDPHeader.Disconnected);
            buff.Write((ushort)A);
            SendToAllClient(buff);

            ClientData[] array = m_ClientArray.GetObjectArray();
            ClientData clientData = new ClientData(new IPEndPoint(0, 0), A);
            array[A] = clientData;
            //array.SetValue(clientData, A);

            BaseDebugString += "\nRemove success ~ ID : " + A.ToString();
            return true;
        }

        public void SendToAllClient(NetBuffer buffer)
        {
            ClientData[] array = m_ClientArray.GetObjectArray();
            for (int a = 0; a < m_ClientArray.NowUsedAmount; a++)
            {           
                if (array[a].IPE.Address.Address != 0)
                    SendToIP(array[a].IPE, buffer);
            }
        }
    }
}
