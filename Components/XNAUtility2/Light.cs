﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace I_XNAUtility
{
    ////just simple light data...
    //public class DirectionalLight
    //{
    //    Vector3 m_Direction;
    //    Vector3 m_Position;
    //    Vector3 m_LightColor;

    //    public DirectionalLight()
    //    {
    //    }

    //    //public void initialize(Vector3 Direction, Vector3 Position, Vector3 LightColor)
    //    //{
    //    //    m_Direction = Direction;
    //    //    m_Position = Position;
    //    //    m_LightColor = LightColor;
    //    //}

    //    public Vector3 Direction
    //    {
    //        set { 
    //            m_Direction = Vector3.Normalize(value); 
    //        }
    //        get { return m_Direction; }
    //    }

    //    public Vector3 Position
    //    {
    //        set { m_Position = value; }
    //        get { return m_Position; }
    //    }

    //    public Vector3 LightColor
    //    {
    //        set { m_LightColor = value; }
    //        get { return m_LightColor; }
    //    }
    //}



    public struct PointLight
    {
        static public PointLight Default(Vector3? pos)
        {
            Vector3 position = Vector3.Zero;
            if (pos.HasValue)
                position = pos.Value;
            PointLight lll;
            lll.postionX = position.X;
            lll.postionY = position.Y;
            lll.postionZ = position.Z;
            lll.colorR = lll.colorG = lll.colorB = 1;
            lll.Strength = 1f;
            lll.Decay = 2f;
            lll.rangeValue = 100f;
            lll.SpecularIntensity = 2;
            return lll;
        }

        public float postionX;
        public float postionY;
        public float postionZ;

        public byte colorR;
        public byte colorG;
        public byte colorB;

        public float Strength;
        public float Decay;         //衰簡直
        public float rangeValue;//光的範圍
        public float SpecularIntensity;//光的specular強度直接乘上的倍率，反射地上光圈大小。
    };

    public class PointLights
    {
        public PointLights()
        {
            Amount = 0;
            PointLight = null;
        }

        public int Amount; //存給人看的，不然不需要。
        public PointLight[] PointLight;
    };

    [Serializable]
    public class SpotLightData
    {
        public float angleTheta;//SPOT的內圈
        public float anglePhi;//SPOT的外圈
        public float strength;//計算後光的強度在加強的倍數
        public float decay;//SPOT的衰減強度
        public Vector3 colorValue = Vector3.One;
        public float range = 10000;
    }
    public class SpotLight
    {
        public Vector3 position;
        public Vector3 direction;
        SpotLightData spotLightData;

        public SpotLight() { spotLightData = new SpotLightData(); }
        public SpotLight(ref Vector3 initialPosition, ref Vector3 initialdirection,
            float initTheta, float initialPhi, float initialStrength, float InitialDecay, ref Vector3 InitialColor, float initRange)
        {
            spotLightData = new SpotLightData();
            position = initialPosition;
            direction = initialdirection;
            spotLightData.angleTheta = initTheta;
            spotLightData.anglePhi = initialPhi;
            spotLightData.strength = initialStrength;
            spotLightData.decay = InitialDecay;
            spotLightData.colorValue = InitialColor;
            spotLightData.range = initRange;
        }
        public float angleTheta {
            get { return spotLightData.angleTheta; }
            set { spotLightData.angleTheta = value; } }
        public float anglePhi { 
            get { return spotLightData.anglePhi; }
            set { spotLightData.anglePhi = value; }        }
        public float strength { 
            get { return spotLightData.strength; }
            set { spotLightData.strength = value; }        }
        public float decay {
            get { return spotLightData.decay; }
            set { spotLightData.decay = value; }        }
        public Vector3 colorValue { 
            get { return spotLightData.colorValue; }
            set { spotLightData.colorValue = value; }        }
        public float range { 
            get { return spotLightData.range; }
            set { spotLightData.range = value; }        }
    }
}
