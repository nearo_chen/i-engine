﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.IO;

namespace I_XNAUtility
{
    public class MySound
    {
        AudioEngine MyAudioEngine;
        List<WaveBank> MyWaveBankList = new List<WaveBank>(8);
        Dictionary<string, SoundBank> MySoundBankList = new Dictionary<string, SoundBank>(8);

        AudioListener m_AudioListener = null;

        MyObjectContainer<Cue>[] m_CueContainer = null; //存放有呼叫GetCue() 的CUE，CUE的BUFFER。
        int totalCueContainerSwitch = 0;

        public bool SoundPlayEnable = true;
        MyObjectContainer<Cue>[] m_PreparePlayCues = null;//把要撥放的cue都先丟進這裡    
        int SwitchCueContainer = 0;
        
        public MySound()
        {
            if (m_PreparePlayCues == null)
            {
                m_PreparePlayCues = new MyObjectContainer<Cue>[2];
                m_PreparePlayCues[0] = new MyObjectContainer<Cue>(256);
                m_PreparePlayCues[1] = new MyObjectContainer<Cue>(256);
                SwitchCueContainer = 0;
            }
        }

        //public void AddWaveBank(string file, bool bLoadInMemory)
        //{
        //    if (bLoadInMemory)
        //        MyWaveBankList.Add( new WaveBank(MyAudioEngine, file));
        //    else
        //        MyWaveBankList.Add( new WaveBank(MyAudioEngine, file));
        //}

        //public void AddSoundBank(string file, string soundBankName)
        //{
        //    MySoundBankList.Add(soundBankName,  new SoundBank(MyAudioEngine, file));
        //}

        /// <summary>
        /// 傳入XACT由VC建出來的xgs檔，他會自動讀取同目錄下的wave bank.xwb和sound bank.xsb。
        /// </summary>
        /// <param name="xgsFile"></param>
        /// <param name="bLoadInMemory"></param>
        public void Initailize(string xgsFile, bool bLoadInMemory)
        {
            try
            {
                MyAudioEngine = new AudioEngine(xgsFile);

                string SoundFolder = Utility.GetFileFolderName(xgsFile);//取得資料夾路徑

                if (bLoadInMemory)
                    MyWaveBankList.Add(new WaveBank(MyAudioEngine, SoundFolder + "\\wave bank.xwb"));
                else
                    MyWaveBankList.Add(new WaveBank(MyAudioEngine, SoundFolder + "\\wave bank.xwb", 0, 16));
                MySoundBankList.Add("!@#$%^", new SoundBank(MyAudioEngine, SoundFolder + "\\sound bank.xsb"));


                m_AudioListener = new AudioListener();
                //m_AudioEmmiter = new AudioEmitter();
                //BackGroundMusic = MySoundBank.GetCue("Victory");
                //PlayBackGroundMusic();

                if (m_CueContainer == null)
                {
                    m_CueContainer = new MyObjectContainer<Cue>[2];
                    m_CueContainer[0] = new MyObjectContainer<Cue>();
                    m_CueContainer[0].InitObjectContainer(4096);

                    m_CueContainer[1] = new MyObjectContainer<Cue>();
                    m_CueContainer[1].InitObjectContainer(4096);
                }

                //因為讀取完馬上讓thread跑Update會造成當掉，所以不用多執行續了
                //m_ThreadID = ThreadManager.CreateThread(UpdateMySoundInThread, null, "UpdateMySoundInThread", System.Threading.ThreadPriority.Lowest);
            }
            catch (Exception e)
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage(e.Message);
            }
        }

        Matrix m_InvCamera;
        //public void UpdateCamera(ref Matrix invCamera)
        //{
        //    m_InvCamera = invCamera;
        //}

        //int m_ThreadID = -1;
        //void UpdateMySoundInThread(object o)
        //{
        //    while (m_ThreadID != -1)
        //    {
        //        //lock (this)
        //        {
        //            //  if (MyAudioEngine == null)
        //            //       break;                 
        //            update(ref m_InvCamera);
        //        }
        //        ThreadManager.Sleep(100);
        //    }
        //}

        ~MySound()
        {
            Dispose();
        }

        /// <summary>
        /// 幹掉
        /// </summary>
        public void Dispose()
        {
            //ThreadManager.ReleaseThreadID(ref m_ThreadID);

            //lock (this)
            {
                if (MyWaveBankList != null)
                {
                    foreach (WaveBank w in MyWaveBankList)
                        w.Dispose();
                    MyWaveBankList.Clear();
                    MyWaveBankList = null;
                }

                if (MySoundBankList != null)
                {
                    foreach (string key in MySoundBankList.Keys)
                        MySoundBankList[key].Dispose();
                    MySoundBankList.Clear();
                    MySoundBankList = null;
                }
                //if (MyWaveBank != null)
                //    MyWaveBank.Dispose();
                //MyWaveBank = null;

                //if (MySoundBank != null)
                //    MySoundBank.Dispose();
                //MySoundBank = null;

                if (MyAudioEngine != null)
                    MyAudioEngine.Dispose();
                MyAudioEngine = null;

                ReleaseAllCue();

                if (m_CueContainer != null)
                {
                    m_CueContainer[0].Dispose();
                    m_CueContainer[1].Dispose();
                }
                m_CueContainer = null;

            }
        }

        void ReleaseAllCue()
        {
            if (m_CueContainer != null)
            {
                for (int a = 0; a < m_CueContainer[0].NowUsedAmount; a++)
                    ((Cue)m_CueContainer[0].GetObjectArrayValue(a)).Dispose();
                m_CueContainer[0].Clear();

                for (int a = 0; a < m_CueContainer[1].NowUsedAmount; a++)
                    ((Cue)m_CueContainer[1].GetObjectArrayValue(a)).Dispose();
                m_CueContainer[1].Clear();
            }

            if (m_PreparePlayCues != null)
            {
                m_PreparePlayCues[0].Dispose();
                m_PreparePlayCues[0] = null;

                m_PreparePlayCues[1].Dispose();
                m_PreparePlayCues[1] = null;
                m_PreparePlayCues = null;
            }

            if (m_PreparePlayCues == null)
            {
                m_PreparePlayCues = new MyObjectContainer<Cue>[2];
                m_PreparePlayCues[0] = new MyObjectContainer<Cue>(128);
                m_PreparePlayCues[1] = new MyObjectContainer<Cue>(128);
                SwitchCueContainer = 0;
            }
        }

        //public uint GetTatalCueAmount()
        //{
        //    //return m_PreparePlayCues[SwitchCueContainer].NowUsedAmount;
        //    return m_CueContainer[totalCueContainerSwitch].NowUsedAmount;
        //}
        public Cue GetCue(string name)
        {
            return GetCue(name, null);
        }

        public Cue GetCue(string name, string soundBankName)
        {
            //lock (this)
            {
                Cue ccc = null;
                try
                {
                    if (soundBankName == null)
                    {
                        Dictionary<string, SoundBank>.Enumerator itr = MySoundBankList.GetEnumerator();
                        itr.MoveNext();
                        ccc = itr.Current.Value.GetCue(name);
                    }
                    else
                        ccc = MySoundBankList[soundBankName].GetCue(name);
                    m_CueContainer[totalCueContainerSwitch].AddObject(ccc);
                }
                catch (Exception e)
                {
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("GetCue(string name) : " + e.Message + name +
                                                              "\n totalCueContainerSwitch = " + totalCueContainerSwitch.ToString() +
                                                              "\n NowUsedAmount = " + m_CueContainer[totalCueContainerSwitch].NowUsedAmount.ToString());
                }
                return ccc;
            }
        }

        public void CheckStartPlaySound(ref Cue soundCue)
        {
            if (SoundPlayEnable == false)
                return;
            CheckPlaySound(ref soundCue, false, null);
        }

        public void CheckContinuePlaySound(ref Cue soundCue)
        {
            if (SoundPlayEnable == false)
                return;
            CheckPlaySound(ref soundCue, true, null);
        }

        public void CheckStartPlaySound3D(ref Cue soundCue, AudioEmitter emmiter)
        {
            if (SoundPlayEnable == false)
                return;

            CheckPlaySound(ref soundCue, false, emmiter);
        }

        public void CheckContinuePlaySound3D(ref Cue soundCue, AudioEmitter emmiter)
        {
            if (SoundPlayEnable == false)
                return;

            CheckPlaySound(ref soundCue, true, emmiter);
        }


        void CheckPlaySound(ref Cue soundCue, bool bContinue, AudioEmitter emmiter)
        {
            //lock (this)
            {
                if (soundCue == null)
                    return;

                if (soundCue.IsPrepared || soundCue.IsPreparing)
                {
                    if (emmiter != null)
                        UpdateSoundEmmiter(soundCue, emmiter);

                    //soundCue.Play();
                    m_PreparePlayCues[SwitchCueContainer].AddObject(soundCue);
                }
                else if (soundCue.IsPaused && soundCue.IsPlaying)
                {
                    if (bContinue)
                    {
                        //從上次暫停繼續撥
                        soundCue.Resume();
                    }
                    else
                    {
                        //讓她從頭撥
                        soundCue.Stop(AudioStopOptions.Immediate);
                        soundCue.Dispose();
                        soundCue = GetCue(soundCue.Name);

                        if (emmiter != null)
                            UpdateSoundEmmiter(soundCue, emmiter);
                        //soundCue.Play();
                        m_PreparePlayCues[SwitchCueContainer].AddObject(soundCue);
                    }
                }
                else if (soundCue.IsStopped)
                {
                    soundCue.Dispose();
                    soundCue = GetCue(soundCue.Name);

                    if (emmiter != null)
                        UpdateSoundEmmiter(soundCue, emmiter);

                    //soundCue.Play();
                    m_PreparePlayCues[SwitchCueContainer].AddObject(soundCue);
                }
            }
        }

        /// <summary>
        /// 設定XACT內部指定的變數值
        /// </summary>
        /// <param name="par"></param>
        /// <param name="value"></param>
        /// <param name="cue"></param>
        public void SetVariable(string par, float value, ref Cue cue)
        {
            //lock (this)
            {
                if (SoundPlayEnable == false)
                    return;
                if (cue == null)
                    return;
                //if (cue.IsDisposed)
                //    return;

                if (cue.IsPlaying == false)
                    return;
                cue.SetVariable(par, value);
            }
        }

        public void CheckPlayStop(ref Cue soundCue)
        {
            //lock (this)
            {
                if (SoundPlayEnable == false)
                    return;
                if (soundCue == null)
                    return;
                if (soundCue.IsDisposed)
                {
                    soundCue = null;
                    return;
                }
                if (soundCue.IsStopped == false && soundCue.IsStopping == false)
                {
                    soundCue.Stop(AudioStopOptions.Immediate);
                }
                soundCue.Dispose();
                soundCue = null;
            }
        }

        public void CheckPlayPause(ref Cue soundCue)
        {
            //lock (this)
            {
                if (SoundPlayEnable == false)
                    return;
                if (soundCue == null)
                    return;
                if (soundCue.IsDisposed)
                    return;
                if (soundCue.IsPlaying && soundCue.IsPaused == false)
                {
                    soundCue.Pause();
                }
            }
        }

        //struct UpdateEmm
        //{
        //    public Cue ccc;
        //    public AudioEmitter eee;
        //}
        //MyObjectContainer m_UpdateEmmiterContainer = new MyObjectContainer(1024);
   

        /// <summary>
        /// emmiter也要一直update
        /// </summary>
        /// <param name="soundCue"></param>
        /// <param name="audioEmmiter"></param>
        public void UpdateSoundEmmiter(Cue soundCue, AudioEmitter audioEmmiter)
        {
            //lock (this)
            {
                if (soundCue.IsDisposed)
                    return;
                if (SoundPlayEnable == false)
                    return;

                //UpdateEmm mmm;
                //mmm.ccc = soundCue;
                //mmm.eee = audioEmmiter;
                //m_UpdateEmmiterContainer.AddObject(mmm);         
                soundCue.Apply3D(m_AudioListener, audioEmmiter);
                //soundCue.SetVariable("Distance", 1000f);
            }
        }

        bool bReleaseAllCue = false;

        /// <summary>
        /// 設定將所有cue都幹掉，確實停止所有音效。
        /// </summary>
        public void SetReleaseAllCue()
        {
            bReleaseAllCue = true;
        }

        public void Update(ref Matrix invCamera)
        {
            m_InvCamera = invCamera;

            //lock (this)
            {
                if (bReleaseAllCue)
                {
                    bReleaseAllCue = false;
                    ReleaseAllCue();
                }

                //for (int a = 0; a < m_UpdateEmmiterContainer.NowUsedAmount; a++)
                //{
                //    UpdateEmm mmm = (UpdateEmm)m_UpdateEmmiterContainer.GetObjectArrayValue(a);
                //    mmm.ccc.Apply3D(m_AudioListener, mmm.eee);
                //}
                //m_UpdateEmmiterContainer.Clear();

                MyAudioEngine.Update();

                m_AudioListener.Position = invCamera.Translation;
                m_AudioListener.Forward = invCamera.Forward;
                m_AudioListener.Up = invCamera.Up;

                int nextSwitch = SwitchCueContainer + 1;
                if (nextSwitch > 1)
                    nextSwitch = 0;

                for (int a = 0; a < m_PreparePlayCues[SwitchCueContainer].NowUsedAmount; a++)
                {
                    Cue ccc = (Cue)m_PreparePlayCues[SwitchCueContainer].GetObjectArrayValue(a);
                    if (ccc.IsPrepared)
                        ccc.Play();
                    else if (ccc.IsPreparing)
                        m_PreparePlayCues[nextSwitch].AddObject(ccc);
                    else if (ccc.IsStopped)
                        CheckPlayStop(ref ccc);
                }
                m_PreparePlayCues[SwitchCueContainer].Clear();
                SwitchCueContainer = nextSwitch;

                //檢查CUE停止的就讓他去了
                int nextTotalCueContainerSwitch = totalCueContainerSwitch + 1;
                if (nextTotalCueContainerSwitch >= 2)
                    nextTotalCueContainerSwitch = 0;
                for (int a = 0; a < m_CueContainer[totalCueContainerSwitch].NowUsedAmount; a++)
                {
                    Cue tmpCue = (Cue)m_CueContainer[totalCueContainerSwitch].GetObjectArrayValue(a);
                    if (tmpCue.IsStopped)
                        tmpCue.Dispose();
                    else
                        m_CueContainer[nextTotalCueContainerSwitch].AddObject(tmpCue);
                }
                m_CueContainer[totalCueContainerSwitch].Clear();
                totalCueContainerSwitch = nextTotalCueContainerSwitch;
            }
        }
    }
}
