﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace I_XNAUtility
{
    static public class ThreadManager
    {
        const int MaxThreadAmount = 256;
        static IDManager m_IDmanager = new IDManager("ThreadManager", MaxThreadAmount);
        static Thread[] m_ThreadArray = new Thread[MaxThreadAmount];       

        public delegate void ThreadFunction(object o);    
        static public int CreateThread(ThreadFunction threadFunction, object o, string threadName, ThreadPriority priority)
        {
            int id = m_IDmanager.GetID();

            if (id >= 0)
            {
                // Start a new physics simulation.
                ThreadStart threadStarter = delegate { threadFunction(o); };
                m_ThreadArray[id] = new Thread(threadStarter)
                {
                    Name = threadName,
                    Priority = priority,
                };
                m_ThreadArray[id].Start();
            }
            else
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage("Thread Amount: " + MaxThreadAmount.ToString() + " is full!!");
            }
            return id;
        }

        static public void CreateLoadingThread(ThreadFunction threadFunction, ThreadPriority priority)
        {
            // Start a new physics simulation.
            ThreadStart threadStarter = delegate { threadFunction(null); };
            Thread thread = new Thread(threadStarter)
            {
                //Name = threadName,
                Priority = priority,
            };
            thread.Start();
        }

        public static string GetThreadName(int threadID)
        {
            if (m_ThreadArray[threadID] != null)
                return m_ThreadArray[threadID].Name;
            return string.Empty;
        }
        //static public void ResumeThread(int id)
        //{           
        //    if (id >= 0 && m_ThreadArray[id] != null)
        //    {          
        //        m_ThreadArray[id].Resume();          
        //    }
        //}

        //static public void SuspendThread(int id)
        //{
        //    if (id >= 0 && m_ThreadArray[id] != null)
        //    {
        //        m_ThreadArray[id].Suspend();
        //    }
        //}

        static public void ReleaseThreadID(ref int id)
        {
            if (id >= 0)
            {
                m_IDmanager.ReturnID(id);//歸還ID給MANAGER
            }
            id = -1;
        }

        //static public void ReleaseThread(ref int id)
        //{
        //    if (id >= 0 && m_ThreadArray[id] != null)
        //    {
        //        //if(m_ThreadArray[id].IsAlive)
        //            m_ThreadArray[id].Abort();
        //        m_ThreadArray[id] = null;
        //    }
        //    ReleaseThreadID(ref id);            
        //}


        static public void Sleep(int time)
        {
            Thread.Sleep(time);
        }
    }

  
}
