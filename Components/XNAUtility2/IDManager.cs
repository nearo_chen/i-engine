﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace I_XNAUtility
{
    /// <summary>
    /// 用來管理ID的class，使用get ID()取得唯一ID，使用return ID()將不再使用的ID歸還。
    /// </summary>
    public class IDManager : IDisposable
    {
        string m_IDManagerName;
        int m_MaxID;
        //static int IDCount = (int)IDNumber.UIComponentID;
        List<int> m_IDList = null;

        public IDManager(string name, int maxID)
        {
            m_IDManagerName = name;
            m_MaxID = maxID;
            m_IDList = new List<int>(m_MaxID);
            for (int a = 0; a < m_MaxID; a++)
                m_IDList.Add(a);
        }
        ~IDManager()
        {
            Dispose();
        }

        public void Dispose()
        {
            m_IDList.Clear();
            m_IDList = null;
        }

        public int GetID()
        {
            if (m_IDList.Count == 0)
            {
                ////OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage("ID Manager ==>" + m_IDManagerName + " Empty");
                return -1;
            }

            int iidd = m_IDList[0];
            m_IDList.RemoveAt(0);
            return iidd;
        }

        public void ReturnID(int id)
        {
            m_IDList.Add(id);
        }
    }
}
