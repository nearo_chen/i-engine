﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;

namespace I_XNAUtility
{


    public class TimePlayControl
    {
        [Serializable()]
        [StructLayout(LayoutKind.Sequential, Pack = 2)]//alignment改成1byte
        public struct SPLAYBLOCK
        {
            //public SPLAYBLOCK()
            //{
            //}

            //public void Dispose() { }

            //可變值
            //public bool bActive;      //紀錄是否正在使用這資料去撥放
            //public long BlockPlayTime;//這一段已經跑多久了,紀錄撥放到哪邊,BlockPlayTime > PlayLength 就是撥放完畢

            //不可變
            public uint BlockStart;   //這一段開始撥放的時間,以m_StartPlayTime為基準的相對時間
            public ushort PlayLength;   //這一段要撥放多久
            //public bool bPlayLoop;    //是否要loop
            public short GoToBlock;    //跳到哪一格loop
        }

        static public TimePlayControl[] GetTimePlayControlArray(int ArrayAmount)
        {
            return new TimePlayControl[ArrayAmount];
        }

        bool m_bPlayBack;
        bool m_bPlayLoop;
        float m_PlaySpeed = 1;//播放速度控制

        //long m_iStartPlayTime;//開始撥放時間,其他的時間都是以他為基準
        int m_BlockPlayTime;
        //long m_iOffsetStart;  //loop時,迴圈開始點距離start的時間差

        //long PlayBlockAmount;                       //Block資料有幾個
        SPLAYBLOCK[] m_PlayBlocks;       //存放撥放資料陣列
        SPLAYBLOCK m_NowPlayData; //記錄正在播放哪個block

        bool m_bPlay;        //撥放控制
        long m_iNowPlay;      //紀錄現在撥到哪一格
        //long m_iNextPlay;      //紀錄下一步撥哪一格，-1表示下一步停止。

        public bool IfPlay
        {
            get { return m_bPlay; }
        }

        /// <summary>
        /// 取得目前撥放格的撥放比例1表示要換到下一格
        /// </summary>
        public float GetPlayPercent
        {
            get { return (float)m_BlockPlayTime / (float)m_NowPlayData.PlayLength; }
        }

        public TimePlayControl(int blockAmount)
        {
            if (blockAmount <= 0)
                return;

            m_bPlay = false;
            m_iNowPlay = 0;
            //m_iNextPlay = -1;
            m_PlayBlocks = new SPLAYBLOCK[blockAmount];

            m_bPlayLoop = false;
            m_bPlayBack = false;
        }

        public TimePlayControl()
        {
            m_bPlay = false;
            m_iNowPlay = 0;
            //m_iNextPlay = -1;
            //m_PlayBlocks = new SPLAYBLOCK[blockAmount];

            m_bPlayLoop = false;
            m_bPlayBack = false;
        }

        public void Dispose()
        {
            //for(int i=0 ; i<m_PlayBlocks.Length ; i++)
            //{
            //    m_PlayBlocks[i].Dispose();
            //    m_PlayBlocks[i] = null;
            //}
            m_PlayBlocks = null;
        }

        /// <summary>
        /// 如果停止時，block應該是在最後一個block跑完，處於一個叫最後一格的block上，
        /// 自動+1，外部要自行防止陣列超過問題。。
        /// </summary>
        public int NowPlayBlockID
        {
            get
            {
                if (m_bPlay == false)
                    return (int)m_iNowPlay + 1;//如果停止時，block應該是在一個叫最後一格的block上，所以把他自動+1。
                return (int)m_iNowPlay;
            }
        }

        public int NextPlayBlockID
        {
            get
            {
                if (m_PlayBlocks == null)
                    return -1;
                else
                    return (int)m_PlayBlocks[m_iNowPlay].GoToBlock;
            }
        }

        float m_PlayPercent = 0;
        /// <summary>
        /// 取得時間位在目前撥放格的百分之幾
        /// </summary>
        public float NowPlayBlockPercent
        {
            get { return m_PlayPercent; }
        }

        public float PlaySpeed
        {
            get { return m_PlaySpeed; }
            set { m_PlaySpeed = value; }
        }
        //public long NowPlayBlockLength
        //{
        //    get { return m_NowPlayData.PlayLength; }
        //}

        //public long NowPlayBlockTime
        //{
        //    get { return m_NowPlayData.BlockPlayTime; }
        //}

        public int GetNextBlock()
        {
            int next = -1;
            if (m_bPlayBack)
            {
                if (m_iNowPlay > 0)
                {
                    next = (int)m_iNowPlay - 1;
                }
                else
                {
                    if (m_bPlayLoop)
                        next = m_PlayBlocks.Length - 1;
                }
            }
            else
            {
                next = (int)m_NowPlayData.GoToBlock;
                if (next == -1 && m_bPlayLoop)
                    next = 0;
            }
            return next;
        }

        public void RefreshTime(int iNowTime)
        {
            if (m_PlayBlocks == null)
                return;

            iNowTime = (int)((float)iNowTime * m_PlaySpeed);

            //------------------------------------------------*
            //撥放控制                  *
            //------------------------------------------------*
            if (m_bPlay == false)
                return;

            m_BlockPlayTime += iNowTime;
            while (m_BlockPlayTime > m_NowPlayData.PlayLength)
            {
                m_BlockPlayTime -= m_NowPlayData.PlayLength;

                if (m_bPlayBack)
                {
                    if (m_iNowPlay < 0)
                    {
                        if (m_bPlayLoop)
                            playback();
                        else
                            stop();

                        m_PlayPercent = 0;//倒著撥不知要怎算都給0
                        return;
                    }

                    m_iNowPlay--;
                    m_NowPlayData = m_PlayBlocks[m_iNowPlay];
                }
                else
                {
                    if (m_NowPlayData.GoToBlock == -1)
                    {
                        if (m_bPlayLoop)
                            play();
                        else
                            stop();

                        m_PlayPercent = 0;
                        return;
                    }

                    m_iNowPlay = m_NowPlayData.GoToBlock;
                    m_NowPlayData = m_PlayBlocks[m_iNowPlay];
                    //m_PlayPercent = (float)m_BlockPlayTime / (float)m_NowPlayData.PlayLength;
                }
            }
            m_PlayPercent = (float)m_BlockPlayTime / (float)m_NowPlayData.PlayLength;
        }

        //直接跳至要前往的block
        public float RefreshWithRatio(float ratio)
        {
            //if (m_PlayBlocks == null)
            //    return 0;
            //if (ratio > 1.0f)
            //    ratio = 1.0f;
            //if (ratio < 0)
            //    ratio = 0;

            ////以ratio來計算目前時間 
            //float Decimal = 0;
            //float nowtime = totalTime * ratio;
            //float countTime = 0;
            //int block=-1;
            //for (int a = 0; a < m_PlayBlocks.Length; a++)
            //{
            //    countTime += m_PlayBlocks[a].PlayLength;
            //    if (countTime > nowtime)
            //    {                    
            //        a--;
            //        if (a < 0)
            //            a = 0;
            //        block = a;
            //        int nextB = block +1;
            //        if (nextB >= m_PlayBlocks.Length)
            //            nextB--;

            //        Decimal = (nowtime - (countTime - m_PlayBlocks[block].PlayLength)) / 
            //            m_PlayBlocks[nextB].PlayLength;

            //        break;
            //    }
            //}
            //if (block < 0)
            //    block = 0;
            //m_iNowPlay = block;
            //m_NowPlayData = m_PlayBlocks[m_iNowPlay];
            //return Decimal;

            if (m_PlayBlocks == null)
                return 0;

            if (ratio > 1.0f)
                ratio = 1.0f;
            if (ratio < 0)
                ratio = 0;

            float block = ((float)m_PlayBlocks.Length - 1.0f) * ratio;
            //float block = ((float)m_PlayBlocks.Length) * ratio;
            float Decimal = block - (float)Math.Floor(block);

            m_iNowPlay = (int)block;
            m_NowPlayData = m_PlayBlocks[m_iNowPlay];

            return Decimal;
        }


        public void play()
        {
            if (m_PlayBlocks.Length <= 0)
                return;

            m_bPlay = true;
            m_iNowPlay = 0;  //紀錄現在撥到哪一格
            m_BlockPlayTime = 0;
            m_NowPlayData = m_PlayBlocks[m_iNowPlay];
            m_bPlayBack = false;
        }

        public void playloop(bool bbb)
        {
            m_bPlayLoop = bbb;
        }

        public void playPercent(float percent)
        {
            if (m_PlayBlocks == null)
                return;

            if (percent > 1.0f)
                percent = 1.0f;
            if (percent < 0)
                percent = 0;

            long block = (long)((m_PlayBlocks.Length - 1) * percent);
            if (block >= m_PlayBlocks.Length)
                return;

            m_iNowPlay = block;
            m_NowPlayData = m_PlayBlocks[m_iNowPlay];
            m_bPlay = true;
            m_BlockPlayTime = 0;
            m_bPlayBack = false;
        }

        //public void playPercentWithMaxTime(float percent, int maxTime)
        //{
        //    if (m_PlayBlocks == null)
        //        return;

        //    if (percent > 1.0f)
        //        percent = 1.0f;
        //    if (percent < 0)
        //        percent = 0;

        //    int startBlock =0;
        //    //int endBlock=0;
        //    //int timeLength = 0;
        //    int percentTime = (int)((float)maxTime * percent);
        //    for (int a = 0; a < m_PlayBlocks.Length; a++)
        //    {
        //        if (m_PlayBlocks[a].BlockStart <= percentTime)
        //        {
        //            startBlock = a;
        //            //endBlock = a+1;
        //            //if (endBlock >= m_PlayBlocks.Length)
        //            //    endBlock--;
        //            //timeLength = m_PlayBlocks[endBlock].BlockStart - m_PlayBlocks[startBlock].BlockStart;
        //            break;
        //        }
        //    }

        //    m_iNowPlay = startBlock;
        //    m_NowPlayData = m_PlayBlocks[m_iNowPlay];
        //    m_bPlay = true;
        //    m_BlockPlayTime = 0;
        //    m_bPlayBack = false;
        //}

        public void playback()
        {
            if (m_PlayBlocks == null)
                return;

            m_bPlay = true;
            m_iNowPlay = m_PlayBlocks.Length - 1;
            m_BlockPlayTime = 0;
            m_NowPlayData = m_PlayBlocks[m_iNowPlay];
            m_bPlayBack = true;
        }

        public void stop()
        {
            m_bPlay = false;    //撥放控制
            //m_iStartPlayTime = 0;//開始撥放時間
            //m_iNowPlay = 0;  //紀錄現在撥到哪一格 
            //m_pNowPlayData = null;
            //m_iOffsetStart=0;
        }

        public void SetBlockData(long location, uint BlockStartTime, ushort PlayLengthTime, short gotoBlock)
        {
            m_PlayBlocks[location].BlockStart = BlockStartTime;
            m_PlayBlocks[location].PlayLength = PlayLengthTime;
            m_PlayBlocks[location].GoToBlock = gotoBlock;

            totalTime = 0;
            for (int a = 0; a < m_PlayBlocks.Length; a++)
                totalTime += m_PlayBlocks[a].PlayLength;
        }

        public void SetBlockData(SPLAYBLOCK[] playBlocks)
        {
            m_PlayBlocks = playBlocks;

            totalTime = 0;
            for (int a = 0; a < m_PlayBlocks.Length; a++)
                totalTime += m_PlayBlocks[a].PlayLength;
        }

        float totalTime;
    }
}
