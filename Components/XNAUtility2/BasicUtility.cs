﻿//#define LOAD_FOREVER

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.Collections.ObjectModel;	//For using ‘Clollection’

using System.Runtime.InteropServices;
using System.Threading;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.IO;
using System.Text;

namespace I_XNAUtility
{
    //public class SpriteObject
    //{
    //    Vector2 position;

    //    // a SpriteBatch and SpriteFont, which we will use to draw the objects' names
    //    SpriteBatch spriteBatch = null;

    //    //draw string..................
    //    string text;
    //    SpriteFont spriteFont = null;

    //    //maybe not just one texture, we can use for animation....
    //    int textureAmount = 0;
    //    Texture2D[] textures = null;              //texture

    //    GraphicsDevice graphics = null;
    //    ContentManager content = null;

    //    public SpriteObject(GraphicsDevice g, ContentManager c, string fontName, string[] textureName)
    //    {
    //        graphics = g;
    //        content = c;
    //        // create a spritebatch and load the font, which we'll use to draw the
    //        // models' names.
    //        spriteBatch = new SpriteBatch(graphics);

    //        if (fontName != null)
    //            spriteFont = content.Load<SpriteFont>(fontName);

    //        if (textureName != null)
    //        {
    //            textureAmount = textureName.Length;
    //            textures = new Texture2D[textureAmount];
    //            for (int i = 0; i < textureAmount; i++)
    //            {
    //                textures[i] = Utility.ContentLoad_Texture2D(content, textureName[i]);

    //                //    while (true)
    //                //{
    //                //    try
    //                //    {
    //                //        textures[i] = content.Load<Texture2D>(textureName[i]);
    //                //   //     break;
    //                //    }
    //                //    catch (Exception e)
    //                //    {
    //                //        GC.Collect();
    //                //    }
    //                //}

    //            }
    //        }
    //    }

    //    public string Text
    //    {
    //        get { return text; }
    //        set { text = value; }
    //    }
    //    public Vector2 Position
    //    {
    //        get { return position; }
    //        set { position = value; }
    //    }

    //    public void TextClear()
    //    {
    //        text = "";
    //    }
    //    public void AddText(string t)
    //    {
    //        text = text + " " + t;
    //    }

    //    public void NewLineText(string t)
    //    {
    //        text = text + "\n" + t;
    //    }

    //    public void render(Color FontClor, float fontScale)
    //    {
    //        spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.BackToFront, SaveStateMode.SaveState);

    //        if (textures != null)
    //        {
    //            foreach (Texture2D tex in textures)
    //            {
    //                spriteBatch.Draw(tex, position, Color.White);
    //            }
    //        }

    //        if (spriteFont != null && text != null)
    //        {
    //            spriteBatch.DrawString(spriteFont, text, position, FontClor, 0f, Vector2.Zero, fontScale, SpriteEffects.None, 0f);
    //        }
    //        spriteBatch.End();
    //    }
    //}

    static public class Utility
    {
        static StringBuilder OpenWinformStringBuilder = new StringBuilder(512);
        static public string[] OpenWinformLoadfileDialog(System.Windows.Forms.OpenFileDialog ofile,
            string title, string[] types, string InitialDirectory)
        {
            ofile.Title = title;

            OpenWinformStringBuilder.Remove(0, OpenWinformStringBuilder.Length);

            //"Images(*.xnb*.jpg*.bmp*.png*.tga) |*.xnb;*.jpg;*.bmp;*.png;*.tga|" + "All files (*.*)|*.*";
            OpenWinformStringBuilder.Append("Types(");
            foreach (string tttt in types)
                OpenWinformStringBuilder.AppendFormat("*.{0}", tttt);
            OpenWinformStringBuilder.Append(")|");

            foreach (string tttt in types)
                OpenWinformStringBuilder.AppendFormat("*.{0};", tttt);
            OpenWinformStringBuilder.Append("| All files (*.*)|*.*");

            ofile.Filter = OpenWinformStringBuilder.ToString();
            ofile.InitialDirectory = InitialDirectory;

            if (ofile.ShowDialog() != System.Windows.Forms.DialogResult.OK) return null;

            return ofile.FileNames;
        }

        static public string[] OpenWimformSavefileDialog(System.Windows.Forms.SaveFileDialog sfile,
            string title, string[] types, string InitialDirectory)
        {
            sfile.Title = title;

            OpenWinformStringBuilder.Remove(0, OpenWinformStringBuilder.Length);
            sfile.Filter = null;
            if (types != null)
            {
                OpenWinformStringBuilder.Append("Types(");
                foreach (string tttt in types)
                    OpenWinformStringBuilder.AppendFormat("*.{0}", tttt);
                OpenWinformStringBuilder.Append(")|");

                foreach (string tttt in types)
                    OpenWinformStringBuilder.AppendFormat("*.{0};", tttt);
                OpenWinformStringBuilder.Append("| All files (*.*)|*.*");

                sfile.Filter = OpenWinformStringBuilder.ToString();
            }

            sfile.InitialDirectory = InitialDirectory;

            if (sfile.ShowDialog() != System.Windows.Forms.DialogResult.OK) return null;

            return sfile.FileNames;
        }


        //static StringBuilder m_StringBuilder = new StringBuilder(4096);
        //public static StringBuilder StringBuilder { get { return m_StringBuilder; }}

        //static public void StringAppendLineLine(string text)
        //{
        //    m_StringBuilder.AppendLine(text);
        //}

        //static public void StringGet()
        //{

        //}

        //static public void InitializeData(GraphicsDevice device, ContentManager content)
        //{
        //    Orthographic = Matrix.CreateOrthographic(device.PresentationParameters.BackBufferWidth,
        //        device.PresentationParameters.BackBufferHeight, 0.1f, 100000f); 
        //}


        static public void ShowErrorMessage(Exception e)
        {
            //OutputBox.SwitchOnOff = true;
            string sss = e.Message + "\n\nSource: \n" + e.Source + "\n\nStackTrace: \n" + e.StackTrace;
            OutputBox.ShowMessage(sss);
        }

        /// <summary>
        /// 清除記憶體，跑解構子。p.s. 會造成LAG，所以不能一直CALL。
        /// </summary>
        public static void GCClear()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }


        //static GraphicsDeviceManager Graphics;
        //static public void SetAntialiazing(bool enable)
        //{
        //    Graphics.PreferMultiSampling = enable;
        //}

        //static public RenderTargetUsage RenderTargetUsage = RenderTargetUsage.PlatformContents;
        static public int MultiSampleQuality = 0;
        static public MultiSampleType MultiSampleType = MultiSampleType.None;
        static void graphics_PreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            // Xbox 360 and most PCs support FourSamples/0 
            // (4x) and TwoSamples/0 (2x) antialiasing.
            PresentationParameters pp =
                e.GraphicsDeviceInformation.PresentationParameters;

            int quality = 0;
            GraphicsAdapter adapter = e.GraphicsDeviceInformation.Adapter;
            SurfaceFormat format = adapter.CurrentDisplayMode.Format;
            // Check for 4xAA
            if (adapter.CheckDeviceMultiSampleType(DeviceType.Hardware, format,
                false, MultiSampleType.FourSamples, out quality))
            {
                // even if a greater quality is returned, we only want quality 0
                pp.MultiSampleQuality = 0;
                pp.MultiSampleType =
                    MultiSampleType.FourSamples;
            }
            // Check for 2xAA
            else if (adapter.CheckDeviceMultiSampleType(DeviceType.Hardware,
                format, false, MultiSampleType.TwoSamples, out quality))
            {
                // even if a greater quality is returned, we only want quality 0
                pp.MultiSampleQuality = 0;
                pp.MultiSampleType =
                    MultiSampleType.TwoSamples;
            }

            MultiSampleQuality = pp.MultiSampleQuality;
            MultiSampleType = pp.MultiSampleType;
            return;
        }

        /// <summary>
        /// my game base Initailize setting...
        /// 底層的初始設定資料
        /// </summary>
        public static void GameInitializeSetting(GraphicsDeviceManager graphics, float? TargetElapsedTime, Game game,
            bool bMiusevisible, MultiSampleType sampleType)
        {
            //Graphics = graphics;
            if (game != null)
            {
                game.IsMouseVisible = bMiusevisible;

                //設定為每秒60, 30fps
                SetTargetElapsedTime(TargetElapsedTime, graphics, game);
            }

            //請使用這個設定
            if (graphics != null)
            {
                graphics.PreferredDepthStencilFormat = DepthFormat.Depth24Stencil8;
                graphics.PreferredBackBufferFormat = SurfaceFormat.Color;
            }


            //設定反鋸齒
            if (sampleType != MultiSampleType.None)
            {
                if (graphics != null)
                {
                    graphics.PreferMultiSampling = true;
                    graphics.PreparingDeviceSettings +=
                      new EventHandler<PreparingDeviceSettingsEventArgs>(
                          graphics_PreparingDeviceSettings);
                }
                else
                {
                    MultiSampleQuality = 0;
                    MultiSampleType = sampleType;
                }
            }
            else
            {
                if (graphics != null)
                {
                    graphics.PreferMultiSampling = false;
                }
            }

            if (graphics != null)
            {
                graphics.ApplyChanges();
            }
        }

        public static bool IfAntialiazing
        {
            get { if (MultiSampleType != MultiSampleType.None) { return true; } return false; }
        }

        //public static void GameUpdateSetting(GameTime gameTime)
        //{
        //    //GetTime.SetTime(gameTime.ElapsedGameTime.Milliseconds);//, gameTime.ElapsedGameTime.TotalSeconds);
        //}

        //public static void GameRenderSetting(GameTime gameTime)
        //{
        //    //GetTime.SetTime(gameTime.ElapsedRealTime.Milliseconds);
        //}

        public static void GameDisposeSetting()
        {
        }

        /// <summary>
        /// 設定底層限制fps要多少，傳入60及為每秒限制60fps，
        /// 傳入null及為關閉FixedTimeStep，則會以最快更新率來跑update()。
        /// </summary>
        static void SetTargetElapsedTime(float? targetElapsedTime, GraphicsDeviceManager graphics, Game game)
        {
            if (targetElapsedTime.HasValue)
            {
                //m_TargetElapsedTime = targetElapsedTime.Value;
                //m_ElapsedTimeRatio = 1f / m_TargetElapsedTime;

                //// Most games will want to leave both these values set to true to ensure
                //// smoother updates, but when you are doing performance work it can be
                //// useful to set them to false in order to get more accurate measurements.
                //game.IsFixedTimeStep = true;
                //graphics.SynchronizeWithVerticalRetrace = true;

                //game.TargetElapsedTime = TimeSpan.FromMilliseconds(1000f / (double)m_TargetElapsedTime);

                m_TargetElapsedTime = 60f;
                m_ElapsedTimeRatio = 1f / 60f;
            }
            else
            {
                // Most games will want to leave both these values set to true to ensure
                // smoother updates, but when you are doing performance work it can be
                // useful to set them to false in order to get more accurate measurements.
                game.IsFixedTimeStep = false;
                graphics.SynchronizeWithVerticalRetrace = false;
            }
        }

        /// <summary>
        /// 設定為每秒60 or 30fps
        /// </summary>
        static float m_TargetElapsedTime = 1000f / 60f;
        //public static float TargetElapsedTime { get { return m_TargetElapsedTime; } }

        /// <summary>
        /// 固定的每秒frame比率
        /// </summary>
        static float m_ElapsedTimeRatio = 1f / 60f;
        public static float ElapsedTimeRatio { get { return m_ElapsedTimeRatio; } }

        /// <summary>
        /// 控制讀檔資訊是否從外部讀檔，而非由C#裡面的content。
        /// </summary>
        static public bool IfContentOutSide = true;

   

        //static SpriteBatch m_SpriteBatch = null;

        /// <summary>
        /// 幫助你畫出2D圖
        /// </summary>
        static public void RenderTexture2D(SpriteBatch m_SpriteBatch, Texture2D texture, SpriteBlendMode BlendMode,
            Vector2 pos, Vector2 size)
        {
            //if (m_SpriteBatch == null)
            //    m_SpriteBatch = new SpriteBatch(device);

            m_SpriteBatch.Begin(BlendMode, SpriteSortMode.Immediate, SaveStateMode.None);
            m_SpriteBatch.Draw(texture, new Rectangle((int)pos.X, (int)pos.Y, (int)size.X, (int)size.Y), Color.White);
            m_SpriteBatch.End();
        }

        /// <summary>
        /// 幫助你畫出2D圖
        /// </summary>
        static public void RenderTexture2D(SpriteBatch m_SpriteBatch, Texture2D texture, SpriteBlendMode BlendMode,
            Vector2 pos, Vector2 size, byte alpha255)
        {
            //if (m_SpriteBatch == null)
            //    m_SpriteBatch = new SpriteBatch(device);

            m_SpriteBatch.Begin(BlendMode, SpriteSortMode.Immediate, SaveStateMode.None);
            m_SpriteBatch.Draw(texture, new Rectangle((int)pos.X, (int)pos.Y, (int)size.X, (int)size.Y), new Color(255, 255, 255, alpha255));
            m_SpriteBatch.End();
        }

        /// <summary>
        /// 幫助你畫出2D圖
        /// </summary>
        static public void RenderTexture2D(SpriteBatch m_SpriteBatch, Texture2D texture, SpriteBlendMode BlendMode,
            ref Rectangle destRectangle, ref Rectangle sourceRectangle,
            Vector2 pos, Vector2 size, byte alpha255)
        {
            //if (m_SpriteBatch == null)
            //    m_SpriteBatch = new SpriteBatch(device);

            m_SpriteBatch.Begin(BlendMode, SpriteSortMode.Immediate, SaveStateMode.None);
            m_SpriteBatch.Draw(texture, destRectangle, sourceRectangle, Color.White, 0, Vector2.Zero, SpriteEffects.FlipHorizontally, 0);
            m_SpriteBatch.End();
        }

        /// <summary>
        /// 取得一張內定的環境貼圖，但目前會有當機可能
        /// </summary>
        /// <param name="defaultTextureCube"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static TextureCube GetDefaultTextureCube(ref TextureCube defaultTextureCube, ContentManager content)
        {
            if (defaultTextureCube == null)
            {
                if (Utility.IfContentOutSide)
                {
                    defaultTextureCube = content.Load<TextureCube>(GetFullPath("XNBData\\Content\\hills"));
                }
                else
                {
                    defaultTextureCube = content.Load<TextureCube>("hills");
                }
            }
            return defaultTextureCube;
        }

        public static string WorkingDirectory = Directory.GetCurrentDirectory() + "\\";
        /// <summary>
        /// 取得目前工作目錄下的完整路徑
        /// </summary>
        public static string GetFullPath(string filename)
        {
            return WorkingDirectory + filename;
        }

        /// <summary>
        /// 取得目前工作目錄下的完整路徑
        /// </summary>
        public static void GetFullPath(ref string filename, out string final)
        {
            final = string.Format("{0}{1}", WorkingDirectory, filename);
        }

        public static bool IfFileExist(string filename)
        {
            return System.IO.File.Exists(filename);
        }

        public static void DeleteFile(string filename)
        {
            if (IfFileExist(filename))
            {
                System.IO.File.Delete(filename);
            }
        }

        /// <summary>
        /// 把工作目錄去掉，取得目前檔名下的相對路徑。
        /// </summary>
        public static string GetOppositeFilePath(string filename)
        {
            if (filename.Contains(WorkingDirectory))
            {
                string sss = filename.Substring(WorkingDirectory.Length, filename.Length - WorkingDirectory.Length);
                return sss;
            }
            return filename;
        }

        /// <summary>
        /// 將檔名去掉，回傳路徑資料夾字串。取得資料夾路徑。
        /// </summary>
        public static string GetFileFolderName(string fileName)
        {
            int a = fileName.LastIndexOf('\\');
            int b = fileName.LastIndexOf('/');
            if (b >= 0)
                a = Math.Min(a, b);
            if (a < 0)
                a = b;
            return fileName.Remove(a);
        }

        /// <summary>
        /// 將路徑資料夾字串去掉，回傳檔名。
        /// </summary>
        public static string RemoveFileFolderName(string fileName)
        {
            int a = fileName.LastIndexOf('\\');
            int b = fileName.LastIndexOf('/');
            if (b >= 0)
                a = Math.Min(a, b);
            if (a < 0)
                a = b;

            a = a + 1;
            b = fileName.Length - a;
            return fileName.Substring(a, b);
        }

        /// <summary>
        /// 將附檔名去掉
        /// </summary>
        public static string RemoveSubFileName(string fileName)
        {
            //   return System.IO.Path.ChangeExtension(fileName, string.Empty);
            //  return System.IO.Path.GetFileNameWithoutExtension(fileName);
            int a = fileName.LastIndexOf('.');
            return fileName.Remove(a);
        }

        /// <summary>
        /// 將附檔名去掉
        /// </summary>
        public static void RemoveSubFileName(ref string fileName, out string final)
        {
            //   return System.IO.Path.ChangeExtension(fileName, string.Empty);
            //  return System.IO.Path.GetFileNameWithoutExtension(fileName);
            int a = fileName.LastIndexOf('.');
            final = fileName.Remove(a);
        }

        public static bool SaveXMLFileSOAP(string fileName, object xmlData)
        {
            FileStream FP = File.Open(fileName + ".soap", FileMode.Create);
            if (FP != null)
            {
                SoapFormatter formatter = new SoapFormatter();
                formatter.Serialize(FP, xmlData);
                //XmlSerializer xmlSerializer = new XmlSerializer(xmlData.GetType());
                //xmlSerializer.Serialize(FP, xmlData);
                FP.Close();
                FP.Dispose();
                //xmlSerializer = null;
                formatter = null;
                return true;
            }
            return false;
        }

        public static bool SaveXMLFile(string fileName, object xmlData)
        {
            FileStream FP = File.Open(fileName, FileMode.Create);
            if (FP != null)
            {
                XmlSerializer xmlSerializer = new XmlSerializer(xmlData.GetType());
                xmlSerializer.Serialize(FP, xmlData);
                FP.Close();
                FP.Dispose();
                xmlSerializer = null;
                //formatter = null;
                return true;
            }
            return false;
        }

        public static object LoadXMLFileSOAP(string fileName, Type type)
        {
            return LoadXMLFileSOAP(fileName, type, true);
        }

        public static object LoadXMLFileSOAP(string fileName, Type type, bool warning)
        {
            object xmlData = null;
            if (File.Exists(fileName))
            {
                //讀檔
                // while (true)//同時讀檔時衝突的話讓她先休息0.5秒
                // {
                try
                {
                    FileStream fp = File.Open(fileName + ".soap", FileMode.Open);
                    SoapFormatter formatter = new SoapFormatter();
                    xmlData = formatter.Deserialize(fp);
                    //XmlSerializer xmlSerializer = new XmlSerializer(type);
                    //xmlData = xmlSerializer.Deserialize(fp);
                    fp.Close();
                    fp.Dispose();
                    //xmlSerializer = null;
                    formatter = null;
                    //      break;
                }
                catch (Exception e)
                {
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage(e.Message);

                    // Thread.Sleep(500);
                }
                //}
            }
            else
            {
                if (warning)
                {
                    //   bool bbb = OutputBox.SwitchOnOff;
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage(fileName + " File not found!!!");
                    //OutputBox.SwitchOnOff = bbb;
                }
            }
            return xmlData;
        }

        public static object LoadXMLFile(string fileName, Type type, bool warning)
        {
            object xmlData = null;
            if (File.Exists(fileName))
            {
                //讀檔
                // while (true)//同時讀檔時衝突的話讓她先休息0.5秒
                // {
                try
                {
                    FileStream fp = File.Open(fileName, FileMode.Open);

                    XmlSerializer xmlSerializer = new XmlSerializer(type);
                    xmlData = xmlSerializer.Deserialize(fp);
                    fp.Close();
                    fp.Dispose();
                    xmlSerializer = null;
                    //formatter = null;
                    //      break;
                }
                catch (Exception e)
                {
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage(e.Message);

                    // Thread.Sleep(500);
                }
                //}
            }
            else
            {
                if (warning)
                {
                    //   bool bbb = OutputBox.SwitchOnOff;
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage(fileName + " File not found!!!");
                    //OutputBox.SwitchOnOff = bbb;
                }
            }
            return xmlData;
        }

        public static object LoadXMLFile(string fileName, Type type)
        {
            return LoadXMLFile(fileName, type, false);
        }

        public static bool SaveBinFile(string fileName, object binData)
        {
            FileStream FP = File.Open(fileName, FileMode.Create);
            if (FP != null)
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(FP, binData);
                FP.Close();
                FP.Dispose();
                formatter = null;
                return true;
            }
            return false;
        }

        public static object LoadBinFile(string fileName)
        {
            object binData = null;
            if (File.Exists(fileName))
            {
                try
                {
                    using (FileStream fp = File.Open(fileName, FileMode.Open))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        binData = formatter.Deserialize(fp);
                    }
                }
                catch (Exception e)
                {
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage(e.Message);
                }
            }
            else
            {
                //       bool bbb = OutputBox.SwitchOnOff;
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage(fileName + " File not found!!!");
                //OutputBox.SwitchOnOff = bbb;
            }

            return binData;
        }

        //for FPS
        static int secFPS = 0;
        static int countFPS = 0;
        static long nowTime = 0;
        static long oldTime = 0;
        static public int getFPS()
        {
            nowTime = Environment.TickCount;
            if (nowTime - oldTime > 1000.0f)
            {
                oldTime = nowTime;
                secFPS = countFPS;
                countFPS = 0;
            }
            countFPS++;
            return secFPS;
        }

        //Transform frustum into another local space...
        static public BoundingFrustum TransformFrustum(Matrix transformMatrix, Matrix frustumMatrix)
        {
            //orthogornal projection will invert world matrix, so we invert before doing matrix multiply...
            //InvRotMat x Proj = rotProj
            Matrix tmpInvert = Matrix.Invert(transformMatrix);
            return new BoundingFrustum(tmpInvert * frustumMatrix);
        }

        static public void BoundBoxToBoundingFrustum(ref BoundingBox aabb, out Matrix BoundingFrustumMatrix)
        {
            //bound box's offset factor from origin...
            Vector3 BoundBoxOffsetPos = (aabb.Max + aabb.Min) / 2.0f;

            //create a orthogonal projection...
            BoundingFrustumMatrix = Matrix.CreateOrthographic(
                aabb.Max.X - aabb.Min.X, aabb.Max.Y - aabb.Min.Y, aabb.Min.Z, aabb.Max.Z);

            //transfer matrix into projection...
            BoundingFrustumMatrix = Matrix.Invert(Matrix.CreateTranslation(BoundBoxOffsetPos)) * BoundingFrustumMatrix;
        }

        //safty assign value to Vector, without new ();
        static public void AssignVectorValue(out Vector3 vector, float x, float y, float z)
        {
            vector.X = x; vector.Y = y; vector.Z = z;
        }
        static public void AssignVectorValue(out Vector3 vector, Vector3 vectorSrc)
        {
            vector = vectorSrc;
        }

        //[DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory")]
        //private static extern void CopyMemory(IntPtr Destination, IntPtr Source, int Length);
        //public static unsafe void memcpy(IntPtr Destination, IntPtr Source, int countInBytes)
        //{
        //    CopyMemory(Destination, Source, countInBytes);
        //}



        // The unsafe keyword allows pointers to be used within the following method:
        public static unsafe void MyMemCopy(byte* src, byte* dst, int countInBytes)
        {
            if (src == null ||
                dst == null || countInBytes < 0)
            {
                throw new System.ArgumentException();
            }

            byte* ps = src;
            byte* pd = dst;

            // Loop over the count in blocks of 4 bytes, copying an integer (4 bytes) at a time:
            for (int i = 0; i < countInBytes / 4; i++)
            {
                *((int*)pd) = *((int*)ps);
                pd += 4;
                ps += 4;
            }

            // Complete the copy by moving any bytes that weren't moved in blocks of 4:
            for (int i = 0; i < countInBytes % 4; i++)
            {
                *pd = *ps;
                pd++;
                ps++;
            }
        }

        public static Vector3[] GetVector3Array(int arrayAmount)
        {
            while (true)
            {
                try
                { return new Vector3[arrayAmount]; }
                catch (Exception e)
                {
#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("GetVector3Array :" + e.Message);
                    return null;
#endif
                }
            }
        }
        public static Vector2[] GetVector2Array(int arrayAmount)
        {
            while (true)
            {
                try
                { return new Vector2[arrayAmount]; }
                catch (Exception e)
                {
#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("GetVector2Array :" + e.Message);
                    return null;
#endif
                }
            }
        }

        public static short[] GetShortArray(int arrayAmount)
        {
            while (true)
            {
                try
                { return new short[arrayAmount]; }
                catch (Exception e)
                {
#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("GetShortArray :" + e.Message);
                    return null;
#endif
                }
            }
        }
        public static byte[] GetByteArray(int arrayAmount)
        {
            while (true)
            {
                try
                { return new byte[arrayAmount]; }
                catch (Exception e)
                {
#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("GetByteArray :" + e.Message);
                    return null;
#endif
                }
            }
        }

        public static bool[] GetBoolArray(int arrayAmount)
        {
            while (true)
            {
                try
                { return new bool[arrayAmount]; }
                catch (Exception e)
                {
#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("GetBoolArray :" + e.Message);
                    return null;
#endif
                }
            }
        }

        public static float[] GetFloatArray(int arrayAmount)
        {
            while (true)
            {
                try
                { return new float[arrayAmount]; }
                catch (Exception e)
                {
#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("GetFloatArray :" + e.Message);
                    return null;
#endif
                }
            }
        }

        public static uint[] GetUIntArray(int arrayAmount)
        {
            while (true)
            {
                try
                {
                    return new uint[arrayAmount];
                }
                catch (Exception e)
                {
#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("GetUIntArray :" + e.Message);
                    return null;
#endif
                }
            }
        }

        public static int[] GetIntArray(int arrayAmount)
        {
            while (true)
            {
                try
                {
                    return new int[arrayAmount];
                }
                catch (Exception e)
                {
#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("GetIntArray :" + e.Message);
                    return null;
#endif
                }
            }
        }

        public static Matrix[] GetMatrixArray(int arrayAmount)
        {
            while (true)
            {
                try
                {
                    return new Matrix[arrayAmount];
                }
                catch (Exception e)
                {
#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("GetMatrixArray :" + e.Message);
                    return null;
#endif
                }
            }
        }


        public static Effect ContentLoad_Effect(ContentManager content, string pathName)
        {
            while (true)
            {
                try
                {
                    return content.Load<Effect>(pathName);
                }
                catch (Exception e)
                {

#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage(pathName + "==>Load Effect :" + e.Message);
                    return null;
#endif
                }
            }
            return null;
        }

        public static bool bTextureLoadFromXNB = false;//控制是否要從content中load圖，content會擋重複load圖。
        public static Texture2D Load_Texture2D(GraphicsDevice device, ContentManager content, string pathName, bool bGray)
        {
            return Load_Texture2D(device, content, pathName, bTextureLoadFromXNB, bGray);
        }

        /// <summary>
        /// 傳入相對路徑檔名連附檔名，就會將圖片讀進記憶體中。
        /// </summary>
        static Texture2D Load_Texture2D(GraphicsDevice device, ContentManager content, string pathName, bool bLoadXNB, bool bGray)
        {
            if (pathName == null || pathName.Length <= 0)
                return null;

            if (bLoadXNB == false)
            {
                TextureCreationParameters tcp = TextureCreationParameters.Default;
                if (bGray)
                    tcp.Format = SurfaceFormat.Luminance8;
                try
                {
                    return Texture2D.FromFile(device, Utility.GetFullPath(pathName), tcp);
                }
                catch (Exception e)
                {
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage("Load_Texture2D :" + e.Message);
                    return null;
                }
            }
            else
            {
                pathName = Utility.RemoveSubFileName(pathName);//把附檔名幹掉
                return Utility.ContentLoad_Texture2D(content, Utility.GetFullPath(pathName));
            }
        }

        /// <summary>
        /// 讀取cube map，根據bTextureLoadFromXNB判斷是否要讀取XNB檔，
        /// 若為讀取XNB檔，會自動將副檔名去掉，使用content讀檔。
        /// </summary>
        public static TextureCube Load_TextureCube(GraphicsDevice device, ContentManager content, string pathName)
        {
            if (pathName == null || pathName.Length <= 0)
                return null;

            if (bTextureLoadFromXNB == false)
            {
                return TextureCube.FromFile(device, Utility.GetFullPath(pathName));
            }
            else
            {
                pathName = Utility.RemoveSubFileName(pathName);//把附檔名幹掉
                return content.Load<TextureCube>(Utility.GetFullPath(pathName));
            }
        }

        /// <summary>
        /// 使用Content讀圖，直接根據pathName讀xnb檔
        /// </summary>
        public static Texture2D ContentLoad_Texture2D(ContentManager content, string pathName)
        {
            //while (true)
            {
                try
                {
                    return content.Load<Texture2D>(pathName);
                }
                catch (Exception e)
                {
#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage(pathName + "==>Load Tex2D :" + e.Message);
                    return null;
#endif
                }
            }
            return null;
        }

        /// <summary>
        /// 使用Content讀MODEL，直接根據pathName讀xnb檔
        /// </summary>
        public static Model ContentLoad_Model(ContentManager content, string pathName)
        {
            //while (true)
            {
                try
                {
                    return content.Load<Model>(pathName);
                }
                catch (Exception e)
                {


#if LOAD_FOREVER
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    //Thread.Sleep(1000);
#else
                    //OutputBox.SwitchOnOff = true;
                    OutputBox.ShowMessage(pathName + "==>Load Model :" + e.Message);
                    return null;
#endif
                }
            }
            return null;
        }
    }

    public class MyKeyboard
    {
        KeyboardState previousState;   ////用來存放之前的按鍵狀態
        KeyboardState newState;	   //用來存放目前的按鍵狀態

        Collection<Keys> previousPressedKeys;   //之前按下的按鍵
        Collection<Keys> hittedKeys;            //被按下放開的按鍵

        public MyKeyboard()
        {
            previousState = newState = Keyboard.GetState();
            hittedKeys = new Collection<Keys>();
            previousPressedKeys = new Collection<Keys>();
        }

        public void Dispose()
        {
            previousPressedKeys.Clear();
            hittedKeys.Clear();
        }

        public void Update()
        {
            //把目前的按鍵狀態存成舊的按鍵狀態
            previousState = newState;
            //取出現在的按鍵狀態
            newState = Keyboard.GetState();

            /* 取出被按下的按鍵，放入按下按鍵的集合 */
            hittedKeys.Clear();
            foreach (Keys key in previousPressedKeys)
            {
                if (newState.IsKeyUp(key))
                    hittedKeys.Add(key);
            }

            /* 取出被按下的按鍵，並放入之前被按下的集合中 */
            previousPressedKeys.Clear();
            foreach (Keys key in newState.GetPressedKeys())
                previousPressedKeys.Add(key);
        }

        private bool IsKeyDown(Keys key)
        {
            return newState.IsKeyDown(key);
        }

        private bool IsKeyUp(Keys key)
        {
            return newState.IsKeyUp(key);
        }

        public bool IsKeyHit(Keys key) //案下放開
        {
            return hittedKeys.Contains(key);
        }

        public bool IsKeyPress(Keys key) //按下去
        {
            return previousPressedKeys.Contains(key);
        }
    }

    public class MyMouse
    {
        MouseState previousState;
        MouseState newState;
        int movedX;     //滑鼠移動的相對距離 X
        int movedY;     //滑鼠移動的相對距離 Y
        int oldMouseWheel;
        int MouseWheelOffset;

        public MyMouse()
        {
            previousState = Mouse.GetState();
            newState = Mouse.GetState();
        }

        public void Update()
        {
            // 把之前的狀態保存起來
            previousState = newState;
            newState = Mouse.GetState();

            if (bUserSettingMouseMove)
            {
                bUserSettingMouseMove = false;
            }
            else
            {
                if (IsLeftButtonzPressed() ||
                    IsMiddleButtonPressed() ||
                    IsRightButtonPressed()
                    )
                {
                    if (bFirstPress == false)
                    {
                        previousState = newState;
                        bFirstPress = true;
                    }
                }
                else if (IsLeftButtonReleased() ||
                            IsMiddleButtonReleased() ||
                            IsRightButtonReleased()
                    )
                {
                    if (bFirstPress)
                        bFirstPress = false;
                }

                // 取得X與Y方向的滑鼠相對移動距離
                movedX = newState.X - previousState.X;
                movedY = newState.Y - previousState.Y;
            }
        }

        public Vector2 MouseMoved()
        {
            Vector2 v = new Vector2((float)movedX, (float)movedY);
            return v;
        }

        bool bUserSettingMouseMove = false;
        bool bUserSettingLPress = false;
        bool bUserSettingRPress = false;
        bool bUserSettingMPress = false;
        /// <summary>
        /// 給使用者設定滑鼠移動
        /// </summary>
        public void UserSetMouseMove(int x, int y)
        {
            bUserSettingMouseMove = true;
            movedX = x;
            movedY = y;
        }

        /// <summary>
        /// 給使用者設定滑鼠左鍵按下
        /// </summary>
        public void UserSetLPress()
        {
            bUserSettingLPress = true;
        }
        public void UserSetRPress()
        {
            bUserSettingRPress = true;
        }
        public void UserSetMPress()
        {
            bUserSettingMPress = true;
        }

        public Vector2 MousePosition
        {
            get
            {
                Vector2 v = new Vector2((float)newState.X, (float)newState.Y);
                return v;
            }

            set
            {
                Mouse.SetPosition((int)value.X, (int)value.Y);
            }
        }


        public bool bFirstPress = false;
        public bool IsRightButtonPressed()
        {
            if (bUserSettingRPress)
            {
                bUserSettingRPress = false;
                return true;
            }
            return (newState.RightButton == ButtonState.Pressed);
        }

        public bool IsRightButtonReleased()
        {
            return (newState.RightButton == ButtonState.Released &&
                    previousState.RightButton == ButtonState.Pressed);
        }

        public bool IsMiddleButtonPressed()
        {
            if (bUserSettingMPress)
            {
                bUserSettingMPress = false;
                return true;
            }
            return (newState.MiddleButton == ButtonState.Pressed);
        }

        public bool IsMiddleButtonReleased()
        {
            return (newState.MiddleButton == ButtonState.Released &&
                    previousState.MiddleButton == ButtonState.Pressed);
        }


        public bool IsLeftButtonzPressed()
        {
            if (bUserSettingLPress)
            {
                bUserSettingLPress = false;
                return true;
            }
            bool bbb = (newState.LeftButton == ButtonState.Pressed);
            return bbb;
        }

        public bool IsLeftButtonReleased()
        {
            bool bbb = (newState.LeftButton == ButtonState.Released &&
                    previousState.LeftButton == ButtonState.Pressed);
            return bbb;
        }

        public int IsMouseWheel()
        {
            //MouseWheelOffset = oldMouseWheel - newState.ScrollWheelValue;
            //oldMouseWheel = newState.ScrollWheelValue;
            //return MouseWheelOffset;
            return previousState.ScrollWheelValue - newState.ScrollWheelValue;
        }
    }

    public class ArcBall
    {
        Matrix RotMatrix;
        float angleX, angleY;
        Matrix XRotMatrix;
        Matrix YRotMatrix;

        public ArcBall()
        {
            RotMatrix = Matrix.Identity;
            XRotMatrix = Matrix.Identity;
            YRotMatrix = Matrix.Identity;
        }

        public Matrix GetRotMatrix
        {
            get
            {
                return RotMatrix;
            }
        }

        //public Matrix GetRotMatrix_YUp
        //{
        //    get
        //    {
        //        MyMath.MatrixSetDirection(RotMatrix.Translation, RotMatrix.Forward, Vector3.Up, out RotMatrix);
        //        return RotMatrix;
        //    }
        //}

        public void update(int Milliseconds, Vector2 vecMouseMove)
        {
            if (bLockRot)
            {
                bLockRot = false;
                RotMatrix = Matrix.Identity;
                return;
            }


            angleX = 0;
            angleY = 0;
            float elapsedTime = Milliseconds / 1000.0f;

            if (vecMouseMove.X != 0)
                angleX = vecMouseMove.X * 0.3f * elapsedTime;

            if (vecMouseMove.Y != 0)
                angleY = vecMouseMove.Y * 0.3f * elapsedTime;

            //if (angleY != 0)
            RotY(angleX);
            //if (angleX != 0)
            RotX(angleY);
        }

        public void RotX(float radius)
        {
            XRotMatrix = Matrix.CreateRotationX(radius);
            RotMatrix = YRotMatrix * XRotMatrix;
        }

        public void RotY(float radius)
        {
            YRotMatrix = Matrix.CreateRotationY(radius);
            RotMatrix = XRotMatrix * YRotMatrix;
        }

        bool bLockRot = false;
        public void LockRotationOneFrame()
        {
            bLockRot = true;
        }
    }

    //public class SquareFrameNormal
    //{
    //    GraphicsDevice m_Device;
    //    VertexPositionNormalTexture[] m_FrameVerticesSquare;
    //    short[] m_FrameIndicesSquare;
    //    int m_FramePrimitiveNumberSquare;
    //}

    public class SquareFrame
    {
        GraphicsDevice m_Device;
        VertexPositionTexture[] m_FrameVerticesSquare;

        // protected VertexBuffer frameVertexBuffer;
        short[] m_FrameIndicesSquare;

        //protected IndexBuffer frameIndexBuffer;
        int m_FramePrimitiveNumberSquare;

        VertexDeclaration m_VertexDeclaration;
        BasicEffect m_Effect;
        Texture2D m_Texture;

        Vector3[] m_VertexBuffer; //給外界取得資訊用
        //public Texture2D Texture
        //{
        //    get { return m_Texture; }
        //    set { m_Texture = value; }
        //}



        public void Dispose()
        {
            m_FrameVerticesSquare = null;
            m_FrameIndicesSquare = null;

            if (m_VertexDeclaration != null)
            {
                m_VertexDeclaration.Dispose();
                m_VertexDeclaration = null;
            }

            if (m_Effect != null)
            {
                m_Effect.Dispose();
                m_Effect = null;
            }

            if (m_Texture != null)
            {
                m_Texture.Dispose();
                m_Texture = null;
            }

            m_VertexBuffer = null;
            m_EffectSquareCorner = null;
        }

        public short[] indexbuffer()
        {
            return m_FrameIndicesSquare;
        }

        public Vector3[] vertexbuffer()
        {
            return m_VertexBuffer;
        }

        public SquareFrame(GraphicsDevice D, ContentManager content, string textureName)
        {
            m_Device = D;
            //         aabb = new BoundingBox(new Vector3(-1.0f), new Vector3(1.0f));
            m_Effect = new BasicEffect(m_Device, null);
            m_Effect.DiffuseColor = new Vector3(1.0f, 1.0f, 1.0f);
            m_VertexDeclaration = new VertexDeclaration(m_Device, VertexPositionTexture.VertexElements);

            if (textureName != null)
            {
                //while (true)
                {
                    try
                    {
                        m_Texture = content.Load<Texture2D>(textureName);
                        //       break;
                    }
                    catch (Exception e)
                    {
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                    }
                }

                m_Effect.Texture = m_Texture;
                m_Effect.TextureEnabled = true;
            }

            CreateSquareFrame();
        }

        void CreateSquareFrame()
        {
            m_FrameVerticesSquare = new VertexPositionTexture[4];
            m_FrameIndicesSquare = new short[6];
            m_FramePrimitiveNumberSquare = 2;

            m_FrameVerticesSquare[0] = new VertexPositionTexture(new Vector3(-0.5f, 0.5f, 0), new Vector2(1, 0));
            m_FrameVerticesSquare[1] = new VertexPositionTexture(new Vector3(-0.5f, -0.5f, 0), new Vector2(1, 1));
            m_FrameVerticesSquare[2] = new VertexPositionTexture(new Vector3(0.5f, -0.5f, 0), new Vector2(0, 1));
            m_FrameVerticesSquare[3] = new VertexPositionTexture(new Vector3(0.5f, 0.5f, 0), new Vector2(0, 0));

            m_FrameIndicesSquare[0] = 0;
            m_FrameIndicesSquare[1] = 1;
            m_FrameIndicesSquare[2] = 2;
            m_FrameIndicesSquare[3] = 0;
            m_FrameIndicesSquare[4] = 2;
            m_FrameIndicesSquare[5] = 3;

            m_VertexBuffer = new Vector3[4];
            for (int i = 0; i < m_FrameVerticesSquare.Length; i++)
                m_VertexBuffer[i] = m_FrameVerticesSquare[i].Position;
        }

        //順時鐘方向由左上開始
        public void ResizeSquareFrame(Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3)
        {
            m_FrameVerticesSquare[0].Position = v0;
            m_FrameVerticesSquare[1].Position = v1;
            m_FrameVerticesSquare[2].Position = v2;
            m_FrameVerticesSquare[3].Position = v3;

            for (int i = 0; i < m_FrameVerticesSquare.Length; i++)
                m_VertexBuffer[i] = m_FrameVerticesSquare[i].Position;
        }

        Vector3[] m_EffectSquareCorner = null;
        public void ResizeSquareFrame(float size)
        {
            if (m_EffectSquareCorner == null)
            {
                m_EffectSquareCorner = new Vector3[4];
                m_EffectSquareCorner[0] = GetCorner(0);
                m_EffectSquareCorner[1] = GetCorner(1);
                m_EffectSquareCorner[2] = GetCorner(2);
                m_EffectSquareCorner[3] = GetCorner(3);
            }

            m_FrameVerticesSquare[0].Position = m_EffectSquareCorner[0] * size;
            m_FrameVerticesSquare[1].Position = m_EffectSquareCorner[1] * size;
            m_FrameVerticesSquare[2].Position = m_EffectSquareCorner[2] * size;
            m_FrameVerticesSquare[3].Position = m_EffectSquareCorner[3] * size;

            for (int i = 0; i < m_FrameVerticesSquare.Length; i++)
                m_VertexBuffer[i] = m_FrameVerticesSquare[i].Position;
        }

        public Vector3 GetCorner(int corner)
        {
            return m_FrameVerticesSquare[corner].Position;
        }

        public void DrawSquareFrame(Matrix world, Matrix view, Matrix projection, Texture2D texture)
        {
            m_Device.VertexDeclaration = m_VertexDeclaration;
            m_Effect.View = view;
            m_Effect.Projection = projection;
            m_Effect.World = world;
            m_Effect.DiffuseColor = Color.White.ToVector3();

            m_Effect.LightingEnabled = false;
            m_Effect.TextureEnabled = true;
            if (texture != null)
                m_Effect.Texture = texture;

            m_Effect.Begin();
            m_Effect.CurrentTechnique.Passes[0].Begin();

            DrawSquareFrame();

            m_Effect.CurrentTechnique.Passes[0].End();
            m_Effect.End();

            m_Device.VertexDeclaration = null;
        }
        public void DrawSquareFrame()
        {
            m_Device.DrawUserIndexedPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList,
                m_FrameVerticesSquare, 0, m_FrameVerticesSquare.Length, m_FrameIndicesSquare, 0, m_FramePrimitiveNumberSquare);
        }
    }

    //used for drawing bounding object
    public class BoundingObject
    {
        GraphicsDevice device;
        VertexPositionNormalTexture[] frameVerticesSphere;
        VertexPositionNormalTexture[] frameVerticesBox;
        VertexPositionNormalTexture[] frameVerticesFrustum;
        // protected VertexBuffer frameVertexBuffer;
        short[] frameIndicesSphere;
        short[] frameIndicesBox;
        short[] frameIndicesFrustum;
        //protected IndexBuffer frameIndexBuffer;
        uint framePrimitiveNumberSphere;
        uint framePrimitiveNumberBox;
        uint framePrimitiveNumberFrustum;
        VertexDeclaration vertexDeclaration;
        BasicEffect effect;
        //protected BoundingBox aabb;

        public void Dispose()
        {
            vertexDeclaration.Dispose();
            vertexDeclaration = null;

            effect.Dispose();
            effect = null;

            frameVerticesSphere = null;
            frameVerticesBox = null;
            frameVerticesFrustum = null;
        }

        public BoundingObject(GraphicsDevice D)
        {
            device = D;
            //         aabb = new BoundingBox(new Vector3(-1.0f), new Vector3(1.0f));
            effect = new BasicEffect(device, null);
            effect.DiffuseColor = new Vector3(1.0f, 1.0f, 1.0f);
            vertexDeclaration = new VertexDeclaration(device, VertexPositionNormalTexture.VertexElements);
        }

        //public void RefreshFrustum(BoundingFrustum frustum)
        //{
        //    Vector3[] tmpCorner = frustum.GetCorners();
        //    Utility.AssignVectorValue(frameVerticesFrustum[0].Position, tmpCorner[0]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[1].Position, tmpCorner[1]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[2].Position, tmpCorner[2]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[3].Position, tmpCorner[3]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[4].Position, tmpCorner[4]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[5].Position, tmpCorner[5]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[6].Position, tmpCorner[6]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[7].Position, tmpCorner[7]);
        //}

        public void CreateFrustumFrame(BoundingFrustum frustum)
        {
            frameVerticesFrustum = new VertexPositionNormalTexture[8];
            frameIndicesFrustum = new short[24];       //12 edges of line segments in the line list
            framePrimitiveNumberFrustum = 12;

            Vector3[] tmpCorner = frustum.GetCorners();
            frameVerticesFrustum[0] = new VertexPositionNormalTexture(tmpCorner[0], Vector3.Forward, Vector2.One);
            frameVerticesFrustum[1] = new VertexPositionNormalTexture(tmpCorner[1], Vector3.Forward, Vector2.One);
            frameVerticesFrustum[2] = new VertexPositionNormalTexture(tmpCorner[2], Vector3.Forward, Vector2.One);
            frameVerticesFrustum[3] = new VertexPositionNormalTexture(tmpCorner[3], Vector3.Forward, Vector2.One);
            frameVerticesFrustum[4] = new VertexPositionNormalTexture(tmpCorner[4], Vector3.Forward, Vector2.One);
            frameVerticesFrustum[5] = new VertexPositionNormalTexture(tmpCorner[5], Vector3.Forward, Vector2.One);
            frameVerticesFrustum[6] = new VertexPositionNormalTexture(tmpCorner[6], Vector3.Forward, Vector2.One);
            frameVerticesFrustum[7] = new VertexPositionNormalTexture(tmpCorner[7], Vector3.Forward, Vector2.One);
            frameIndicesFrustum[0] = 0; frameIndicesFrustum[1] = 1;
            frameIndicesFrustum[2] = 1; frameIndicesFrustum[3] = 2;
            frameIndicesFrustum[4] = 2; frameIndicesFrustum[5] = 3;
            frameIndicesFrustum[6] = 3; frameIndicesFrustum[7] = 0;
            frameIndicesFrustum[8] = 4; frameIndicesFrustum[9] = 5;
            frameIndicesFrustum[10] = 5; frameIndicesFrustum[11] = 6;
            frameIndicesFrustum[12] = 6; frameIndicesFrustum[13] = 7;
            frameIndicesFrustum[14] = 7; frameIndicesFrustum[15] = 4;
            frameIndicesFrustum[16] = 0; frameIndicesFrustum[17] = 4;
            frameIndicesFrustum[18] = 1; frameIndicesFrustum[19] = 5;
            frameIndicesFrustum[20] = 2; frameIndicesFrustum[21] = 6;
            frameIndicesFrustum[22] = 3; frameIndicesFrustum[23] = 7;
        }

        //public void RefreshCube(BoundingBox aabb)
        //{
        //    Vector3[] tmpCorner = aabb.GetCorners();
        //    Utility.AssignVectorValue(frameVerticesFrustum[0].Position, tmpCorner[0]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[1].Position, tmpCorner[1]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[2].Position, tmpCorner[2]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[3].Position, tmpCorner[3]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[4].Position, tmpCorner[4]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[5].Position, tmpCorner[5]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[6].Position, tmpCorner[6]);
        //    Utility.AssignVectorValue(frameVerticesFrustum[7].Position, tmpCorner[7]);
        //}

        public void CreateCubeFrame(BoundingBox aabb)
        {
            frameVerticesBox = new VertexPositionNormalTexture[8];
            frameIndicesBox = new short[24];       //12 edges of line segments in the line list
            framePrimitiveNumberBox = 12;

            frameVerticesBox[0] = new VertexPositionNormalTexture(aabb.Min, Vector3.Forward, Vector2.One);
            frameVerticesBox[1] = new VertexPositionNormalTexture(new Vector3(aabb.Max.X, aabb.Min.Y, aabb.Min.Z), Vector3.Forward, Vector2.One);
            frameVerticesBox[2] = new VertexPositionNormalTexture(new Vector3(aabb.Max.X, aabb.Max.Y, aabb.Min.Z), Vector3.Forward, Vector2.One);
            frameVerticesBox[3] = new VertexPositionNormalTexture(new Vector3(aabb.Min.X, aabb.Max.Y, aabb.Min.Z), Vector3.Forward, Vector2.One);
            frameVerticesBox[4] = new VertexPositionNormalTexture(new Vector3(aabb.Min.X, aabb.Max.Y, aabb.Max.Z), Vector3.Forward, Vector2.One);
            frameVerticesBox[5] = new VertexPositionNormalTexture(new Vector3(aabb.Min.X, aabb.Min.Y, aabb.Max.Z), Vector3.Forward, Vector2.One);
            frameVerticesBox[6] = new VertexPositionNormalTexture(new Vector3(aabb.Max.X, aabb.Min.Y, aabb.Max.Z), Vector3.Forward, Vector2.One);
            frameVerticesBox[7] = new VertexPositionNormalTexture(aabb.Max, Vector3.Forward, Vector2.One);
            frameIndicesBox[0] = 0; frameIndicesBox[1] = 1;
            frameIndicesBox[2] = 1; frameIndicesBox[3] = 2;
            frameIndicesBox[4] = 2; frameIndicesBox[5] = 3;
            frameIndicesBox[6] = 3; frameIndicesBox[7] = 0;
            frameIndicesBox[8] = 4; frameIndicesBox[9] = 5;
            frameIndicesBox[10] = 5; frameIndicesBox[11] = 6;
            frameIndicesBox[12] = 6; frameIndicesBox[13] = 7;
            frameIndicesBox[14] = 7; frameIndicesBox[15] = 4;
            frameIndicesBox[16] = 0; frameIndicesBox[17] = 5;
            frameIndicesBox[18] = 1; frameIndicesBox[19] = 6;
            frameIndicesBox[20] = 2; frameIndicesBox[21] = 7;
            frameIndicesBox[22] = 3; frameIndicesBox[23] = 4;
        }

        public void CreateSphereFrame(float radius)
        {
            uint i;
            uint rings = 10;
            uint slices = 10;
            uint vertexNumber = rings * (slices + 1);
            uint indexNumber = (rings - 1) * (slices + 1) * 2;

            frameVerticesSphere = new VertexPositionNormalTexture[vertexNumber];
            frameIndicesSphere = new short[indexNumber];
            framePrimitiveNumberSphere = indexNumber / 2;
            //  float radius = (aabb.Max.X - aabb.Min.X) / 2.0f;

            for (i = 0; i < rings; ++i)
            {
                float phi = ((float)i / (float)(rings - 1) - 0.5f) * MathHelper.Pi;
                for (uint j = 0; j <= slices; ++j)
                {
                    float theta = (float)j / (float)slices * MathHelper.TwoPi;
                    uint n = i * (slices + 1) + j;
                    float x = (float)(Math.Cos(phi) * Math.Cos(theta));
                    float y = (float)Math.Sin(phi);
                    float z = (float)(Math.Cos(phi) * Math.Sin(theta));
                    frameVerticesSphere[n].Position.X = x * radius;
                    frameVerticesSphere[n].Position.Y = y * radius;
                    frameVerticesSphere[n].Position.Z = z * radius;
                    frameVerticesSphere[n].Normal.X = x;
                    frameVerticesSphere[n].Normal.Y = y;
                    frameVerticesSphere[n].Normal.Z = z;
                    frameVerticesSphere[n].TextureCoordinate = Vector2.One;
                }
            }

            for (i = 0; i < rings - 1; ++i)
            {
                for (uint j = 0; j <= slices; ++j)
                {
                    uint n = i * (slices + 1) + j;
                    frameIndicesSphere[n * 2] = (short)(i * (slices + 1) + j);
                    frameIndicesSphere[n * 2 + 1] = (short)((i + 1) * (slices + 1) + j);
                }
            }
        }



        public void DrawCubFrame(Matrix world, Matrix view, Matrix projection, Color color)
        {
            //frameVerticesBox[0].Position = aabb.Min;
            //Utility.AssignVectorValue(out frameVerticesBox[1].Position, aabb.Max.X, aabb.Min.Y, aabb.Min.Z);
            //Utility.AssignVectorValue(out frameVerticesBox[2].Position, aabb.Max.X, aabb.Max.Y, aabb.Min.Z);
            //Utility.AssignVectorValue(out frameVerticesBox[3].Position, aabb.Min.X, aabb.Max.Y, aabb.Min.Z);
            //Utility.AssignVectorValue(out frameVerticesBox[4].Position, aabb.Min.X, aabb.Max.Y, aabb.Max.Z);
            //Utility.AssignVectorValue(out frameVerticesBox[5].Position, aabb.Min.X, aabb.Min.Y, aabb.Max.Z);
            //Utility.AssignVectorValue(out frameVerticesBox[6].Position, aabb.Max.X, aabb.Min.Y, aabb.Max.Z);
            //frameVerticesBox[7].Position = aabb.Max;


            device.VertexDeclaration = vertexDeclaration;
            effect.View = view;
            effect.Projection = projection;
            effect.World = world;
            effect.DiffuseColor = color.ToVector3();
            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(PrimitiveType.LineList,
                    frameVerticesBox, 0, frameVerticesBox.Length, frameIndicesBox, 0, (int)framePrimitiveNumberBox);
                pass.End();
            }
            effect.End();

            device.VertexDeclaration = null;
        }

        public void DrawFrustumFrame(Matrix view, Matrix projection)
        {
            //Vector3[] tmpCorner = frustum.GetCorners();
            //frameVerticesFrustum[0] = new VertexPositionNormalTexture(tmpCorner[0], Vector3.Forward, Vector2.One);
            //frameVerticesFrustum[1] = new VertexPositionNormalTexture(tmpCorner[1], Vector3.Forward, Vector2.One);
            //frameVerticesFrustum[2] = new VertexPositionNormalTexture(tmpCorner[2], Vector3.Forward, Vector2.One);
            //frameVerticesFrustum[3] = new VertexPositionNormalTexture(tmpCorner[3], Vector3.Forward, Vector2.One);
            //frameVerticesFrustum[4] = new VertexPositionNormalTexture(tmpCorner[4], Vector3.Forward, Vector2.One);
            //frameVerticesFrustum[5] = new VertexPositionNormalTexture(tmpCorner[5], Vector3.Forward, Vector2.One);
            //frameVerticesFrustum[6] = new VertexPositionNormalTexture(tmpCorner[6], Vector3.Forward, Vector2.One);
            //frameVerticesFrustum[7] = new VertexPositionNormalTexture(tmpCorner[7], Vector3.Forward, Vector2.One);

            device.VertexDeclaration = vertexDeclaration;
            effect.View = view;
            effect.Projection = projection;
            effect.World = Matrix.Identity;
            effect.DiffuseColor = Color.Yellow.ToVector3();
            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(PrimitiveType.LineList,
                    frameVerticesFrustum, 0, frameVerticesFrustum.Length, frameIndicesFrustum, 0, (int)framePrimitiveNumberFrustum);
                pass.End();
            }
            effect.End();
        }

        public void DrawSphereFrame(Matrix world, Matrix view, Matrix projection)
        {
            DrawSphereFrame(world, view, projection, Color.Red);
        }
        public void DrawSphereFrame(Matrix world, Matrix view, Matrix projection, Color color)
        {
            device.VertexDeclaration = vertexDeclaration;
            effect.View = view;
            effect.Projection = projection;
            effect.World = world;
            effect.DiffuseColor = color.ToVector3();
            //effect.DiffuseColor = Color.Red.ToVector3();
            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(PrimitiveType.LineList,
                    frameVerticesSphere, 0, frameVerticesSphere.Length, frameIndicesSphere, 0, (int)framePrimitiveNumberSphere);
                pass.End();
            }
            effect.End();
        }

    }

    static public class DrawLine
    {
        static GraphicsDevice device = null;
        //static VertexPositionColor[] frameVertexLine = null;
        //  static short[] frameIndicesLine = null;
        //    static uint framePrimitiveNumberLine;
        static VertexDeclaration vertexDeclaration;
        static BasicEffect effect;
        static bool bInit = false;
        static List<VertexPositionColor> m_ObjectContainer;
        //  static MyObjectContainer<VertexPositionColor> m_ObjectContainer;
        //  const int bufferSize = 4096 * 128;
        static void initialize(GraphicsDevice D)
        {
            if (device != null && device != D)
            {
                Unload();
                bInit = false;
            }

            if (bInit)
                return;
            bInit = true;

            device = D;
            effect = new BasicEffect(device, null);
            //effect.DiffuseColor = new Vector3(1.0f, 1.0f, 1.0f);
            vertexDeclaration = new VertexDeclaration(device, VertexPositionColor.VertexElements);

            m_ObjectContainer = new List<VertexPositionColor>(256);
            // m_ObjectContainer = new MyObjectContainer<VertexPositionColor>(bufferSize);
            //frameVertexLine = new VertexPositionColor[2];
            //frameIndicesLine = new short[2];       //12 edges of line segments in the line list
            //framePrimitiveNumberLine = 1;
            //frameVertexLine[0] = new VertexPositionColor(Vector3.Zero, Color.White);
            //frameVertexLine[1] = new VertexPositionColor(Vector3.Zero, Color.White);
            //frameIndicesLine[0] = 0;
            //frameIndicesLine[1] = 1;
        }

        static void Unload()
        {
            effect.Dispose();
            vertexDeclaration.Dispose();
            m_ObjectContainer.Clear();
        }

        static public void AddLineBegin(GraphicsDevice D)
        {
            initialize(D);

            //  m_ObjectContainer.Clear();
        }

        static public void AddLine(Vector3 lineStart, Vector3 lineEnd, Color color)
        {
            //if (m_ObjectContainer.NowUsedAmount >= bufferSize - 1)
            //    return;


            m_ObjectContainer.Add(new VertexPositionColor(lineStart, color));
            m_ObjectContainer.Add(new VertexPositionColor(lineEnd, color));
        }

        //static public void refreshLine(Vector3 lineStart, Vector3 lineEnd)
        //{
        //    frameVertexLine[0].Position = lineStart;
        //    frameVertexLine[1].Position = lineEnd;
        //}

        static Vector3[] frameVerticesBox = new Vector3[8];
        static public void AddBoundingBoxLine(Matrix world, BoundingBox aabb, Color color)
        {
            frameVerticesBox[0] = aabb.Min;
            frameVerticesBox[1] = new Vector3(aabb.Max.X, aabb.Min.Y, aabb.Min.Z);
            frameVerticesBox[2] = new Vector3(aabb.Max.X, aabb.Max.Y, aabb.Min.Z);
            frameVerticesBox[3] = new Vector3(aabb.Min.X, aabb.Max.Y, aabb.Min.Z);
            frameVerticesBox[4] = new Vector3(aabb.Min.X, aabb.Max.Y, aabb.Max.Z);
            frameVerticesBox[5] = new Vector3(aabb.Min.X, aabb.Min.Y, aabb.Max.Z);
            frameVerticesBox[6] = new Vector3(aabb.Max.X, aabb.Min.Y, aabb.Max.Z);
            frameVerticesBox[7] = aabb.Max;

            for (int a = 0; a < frameVerticesBox.Length; a++)
                frameVerticesBox[a] = Vector3.Transform(frameVerticesBox[a], world);

            DrawLine.AddLine(frameVerticesBox[0], frameVerticesBox[1], color);
            DrawLine.AddLine(frameVerticesBox[1], frameVerticesBox[2], color);
            DrawLine.AddLine(frameVerticesBox[2], frameVerticesBox[3], color);
            DrawLine.AddLine(frameVerticesBox[3], frameVerticesBox[0], color);
            DrawLine.AddLine(frameVerticesBox[4], frameVerticesBox[5], color);
            DrawLine.AddLine(frameVerticesBox[5], frameVerticesBox[6], color);
            DrawLine.AddLine(frameVerticesBox[6], frameVerticesBox[7], color);
            DrawLine.AddLine(frameVerticesBox[7], frameVerticesBox[4], color);
            DrawLine.AddLine(frameVerticesBox[0], frameVerticesBox[5], color);
            DrawLine.AddLine(frameVerticesBox[1], frameVerticesBox[6], color);
            DrawLine.AddLine(frameVerticesBox[2], frameVerticesBox[7], color);
            DrawLine.AddLine(frameVerticesBox[3], frameVerticesBox[4], color);
        }

        static public void AddFrustumLine(BoundingFrustum frustum, Color color)
        {
            Vector3[] vertex = frustum.GetCorners();
            DrawLine.AddLine(vertex[0], vertex[1], color);
            DrawLine.AddLine(vertex[1], vertex[2], color);
            DrawLine.AddLine(vertex[2], vertex[3], color);
            DrawLine.AddLine(vertex[3], vertex[0], color);
            DrawLine.AddLine(vertex[4], vertex[5], color);
            DrawLine.AddLine(vertex[5], vertex[6], color);
            DrawLine.AddLine(vertex[6], vertex[7], color);
            DrawLine.AddLine(vertex[7], vertex[4], color);
            DrawLine.AddLine(vertex[0], vertex[4], color);
            DrawLine.AddLine(vertex[1], vertex[5], color);
            DrawLine.AddLine(vertex[2], vertex[6], color);
            DrawLine.AddLine(vertex[3], vertex[7], color);
        }

        /// <summary>
        /// 增加一個Cone，傳入距離強度及角錐的角度
        /// </summary>
        /// <param name="world"></param>
        /// <param name="anglePhiDegree"></param>
        /// <param name="rangeLength"></param>
        /// <param name="color"></param>
        /// <param name="rings"></param>
        /// <param name="slices"></param>
        static public void AddCone(Matrix world, float anglePhiDegree, float rangeLength, Color color, uint rings)
        {
            Vector3 start = world.Translation;
            Vector3 end = world.Translation + world.Forward * rangeLength;
            //DrawLine.AddLine(start, end, Color.Yellow);

            Vector3 eeee;
            for (int a = 0; a < rings; a++)
            {
                float radius = (float)a / rings * MathHelper.TwoPi;
                float lengthRing = (float)Math.Tan(MathHelper.ToRadians(anglePhiDegree)) * rangeLength;

                eeee = Vector3.Transform((Vector3.UnitY * (float)Math.Cos(radius) + Vector3.UnitX * (float)Math.Sin(radius)) * lengthRing, world);
                eeee = eeee + world.Forward * rangeLength;

                DrawLine.AddLine(start, eeee, Color.YellowGreen);
            }
        }

        static Vector3[] vertex = null;
        static public void AddSphereLine(Matrix world, Vector3 centerOffset, float radius, Color color, uint rings, uint slices)
        {
            uint i;
            //     uint rings = 10;
            //      uint slices = 10;
            uint vertexNumber = rings * (slices + 1);
            //uint indexNumber = (rings - 1) * (slices + 1) * 2;

            if (vertex == null || vertex.Length < vertexNumber)
                vertex = new Vector3[vertexNumber];
            //     short []index = new short[indexNumber];
            // int PrimitiveNumber = indexNumber / 2;
            //float radius = (m= .Max.X - aabb.Min.X) / 2.0f;

            for (i = 0; i < rings; ++i)
            {
                float phi = ((float)i / (float)(rings - 1) - 0.5f) * MathHelper.Pi;
                for (uint j = 0; j <= slices; ++j)
                {
                    float theta = (float)j / (float)slices * MathHelper.TwoPi;
                    uint n = i * (slices + 1) + j;
                    float x = (float)(Math.Cos(phi) * Math.Cos(theta));
                    float y = (float)Math.Sin(phi);
                    float z = (float)(Math.Cos(phi) * Math.Sin(theta));
                    vertex[n].X = x * radius;
                    vertex[n].Y = y * radius;
                    vertex[n].Z = z * radius;

                    vertex[n] = Vector3.Transform(centerOffset + vertex[n], world);
                }
            }

            for (i = 0; i < rings - 1; ++i)
            {
                for (uint j = 0; j <= slices; ++j)
                {
                    uint n = i * (slices + 1) + j;
                    //index[n * 2] = (short)(i * (slices + 1) + j);
                    //index[n * 2 + 1] = (short)((i + 1) * (slices + 1) + j);

                    int indexStart = (short)(i * (slices + 1) + j);
                    int indexEnd = (short)((i + 1) * (slices + 1) + j);
                    Vector3 start = vertex[indexStart];
                    Vector3 end = vertex[indexEnd];
                    DrawLine.AddLine(start, end, color);
                    //DrawLine.AddLine(start, end, color);
                }
            }
        }

        ///// <summary>
        ///// deferred render畫出Gbuffer後，處理完最後合併畫面後，傳入儲存depth map的畫線方式。
        ///// </summary>
        ///// <param name="restoreDepthEffect"></param>
        // public static void DeferredDraw(Matrix viewProjection,Texture2D depth, Effect restoreDepthEffect, float depthBias)
        //{         
        //    if (m_ObjectContainer.NowUsedAmount <= 0)
        //        return;

        //    restoreDepthEffect.Parameters["WorldMatrix"].SetValue(Matrix.Identity);
        //    restoreDepthEffect.Parameters["ViewProjection"].SetValue(viewProjection);
        //    restoreDepthEffect.Parameters["DepthTexture"].SetValue(depth);
        //    restoreDepthEffect.Parameters["depthBias"].SetValue(depthBias);

        //    restoreDepthEffect.CommitChanges();

        //    if (frameVertexLine == null || frameVertexLine.Length < m_ObjectContainer.NowUsedAmount)
        //        frameVertexLine = new VertexPositionColor[m_ObjectContainer.NowUsedAmount];

        //    //    static short[] frameIndicesLine = new short[m_ObjectContainer.NowUsedAmount];
        //    for (int a = 0; a < m_ObjectContainer.NowUsedAmount; a++)
        //    {
        //        VertexPositionColor vpc = (VertexPositionColor)m_ObjectContainer.GetObjectArrayValue(a);
        //        frameVertexLine[a].Position = vpc.Position;
        //        frameVertexLine[a].Color =  vpc.Color;      
        //    }

        //    device.VertexDeclaration = vertexDeclaration;
        //    restoreDepthEffect.Begin();
        //    restoreDepthEffect.CurrentTechnique.Passes[0].Begin();
        // //   foreach (EffectPass pass in restoreDepthEffect.CurrentTechnique.Passes)
        // //   {
        ////        pass.Begin();
        //        device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList,
        //            frameVertexLine, 0, frameVertexLine.Length / 2);
        ////        pass.End();
        // //   }

        //    restoreDepthEffect.CurrentTechnique.Passes[0].End();
        //    restoreDepthEffect.End();
        //    device.VertexDeclaration = null;
        //    m_ObjectContainer.Clear();
        //}

        static VertexPositionColor[] frameVertexLine = null;
        static public void Draw(Matrix view, Matrix projection)
        {
            if (m_ObjectContainer.Count <= 0)
                return;

            if (frameVertexLine == null || frameVertexLine.Length < m_ObjectContainer.Count)
                frameVertexLine = new VertexPositionColor[m_ObjectContainer.Count];

            int count = 0;
            List<VertexPositionColor>.Enumerator itr = m_ObjectContainer.GetEnumerator();
            while (itr.MoveNext())
            {
                frameVertexLine[count] = itr.Current;
                count++;
            }
            itr.Dispose();


            //    static short[] frameIndicesLine = new short[m_ObjectContainer.NowUsedAmount];
            //for (int a = 0; a < m_ObjectContainer.NowUsedAmount; a++)
            //{
            //    VertexPositionColor vpc = (VertexPositionColor)m_ObjectContainer.GetObjectArrayValue(a);
            //    frameVertexLine[a] = new VertexPositionColor(vpc.Position, vpc.Color);
            //    //   frameIndicesLine[a] = a;
            //}
            //m_ObjectContainer.Clear();


            //frameVertexLine[0].Color = color;
            //frameVertexLine[1].Color = color;
            //effect.DiffuseColor = Color.White.ToVector3();//color.ToVector3();// new Vector3(color.R, color.G, color.B); 
            //effect.LightingEnabled = false;

            device.VertexDeclaration = vertexDeclaration;
            effect.View = view;
            //effect.LightingEnabled = true;
            //effect.DiffuseColor = Color.Green.ToVector3();
            effect.Projection = projection;
            effect.World = Matrix.Identity;
            effect.VertexColorEnabled = true;
            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList,
                    frameVertexLine, 0, m_ObjectContainer.Count / 2);
                //frameVertexLine.Length / 2);

                //this.Device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, vertices, 0, lines.Length);

                pass.End();
            }
            effect.End();

            device.VertexDeclaration = null;

            Clear();
        }

        /// <summary>
        /// 清除render line buffer
        /// </summary>
        public static void Clear()
        {
            m_ObjectContainer.Clear();
        }
    }



    static public class GetTime
    {
        public static float EllapseTimeRatio;
        //static GameTime m_GameTime;
        static private int CurrentEllapseMillisecond;
        static private float CurrentEllapseSecond;

        static bool m_stop = false;
        static public void StopTime(bool stop)
        {
            m_stop = stop;
        }

        static public void SetTime(int currentEllapseMillisecond)//, double currentEllapseTotalsecond)
        {


            EllapseTimeRatio = (float)currentEllapseMillisecond / 1000f;

            //m_GameTime = gameTime;
            CurrentEllapseMillisecond = currentEllapseMillisecond;
            CurrentEllapseSecond = CurrentEllapseMillisecond / 1000f;
        }

        //public static GameTime GetGameTime()
        //{
        //    //if (m_GameTime == null)
        //    //{
        //    //    //OutputBox.SwitchOnOff = true;
        //    //    OutputBox.ShowMessage("GetGameTime() == nul");
        //    //    Console.WriteLine("GetGameTime() == nul");
        //    //}

        //    return m_GameTime;
        //}

        public static int GetCurrentEllapseMillisecond()
        {
            if (m_stop)
                return 0;
            return CurrentEllapseMillisecond;
        }

        public static float GetCurrentEllapseSecond()
        {
            if (m_stop)
                return 0;
            return CurrentEllapseSecond;
        }
    }


    //public class ZeroOneTimeCounter
    //{
    //    enum PlayStyle
    //    {
    //        Reverse,//播放從頭到尾，尾到頭：會需要2段時間去的時間和回的時間。
    //        Loop,//一直由頭到尾持續撥放：只會需要去的時間。
    //    }

    //    uint m_ForwardTimeLength = 0;//由0~1需要幾秒
    //    uint m_BackwardTimeLength = 0;//由1~0需要幾秒

    //    public ZeroOneTimeCounter()
    //    {
    //    }

    //    public void SetReverseTime(uint forwardTime, uint backwardTime)
    //    {
    //        m_ForwardTimeLength = forwardTime;
    //        m_BackwardTimeLength = backwardTime;
    //    }

    //    public void SetLoopTime(uint forwardTime)
    //    {
    //        m_ForwardTimeLength = forwardTime;            
    //    }

    //    public void UpdateTime()
    //    {
    //    }
    //}
}
