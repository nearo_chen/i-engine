﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using System.Xml.Serialization;
using System.IO;

namespace I_XNAUtility
{
    [Serializable]
    public enum Digit
    {
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
    }

    public class DynamicTexture
    {
        //[Serializable]
        //public struct DynamicTextureVersion
        //{
        //    public float version;
        //}

        public string FileName;
        public int StartNumber;
        public int EndNumber;
        public Digit NumberDigit;
        public int StartPlayTime;
        public int TimeIntervel;
        public int LoopTo;
        public string FileSub;

        public struct DynamicTextureData
        {
            //      public int textureAmount;
            //  public int timeIntervel; //每一張圖的時間間隔
            //    public int startPlayTime;//開始的時間

            public TextureData[] textureData;
        }

        public struct TextureData
        {
            public int startTime;  //millisecond
            public string textureName;
            public int gotoBlock;
        }

        // DynamicTextureVersion m_Version;
        //static DynamicTextureData m_CreateXMLDynamicTextureData;
        //DynamicTextureData m_DynamicTextureData;

        Texture2D[] m_Textures;
        TimePlayControl m_PlayControl;

        long m_CountPlayTime = 0;//記錄從開始撥放到目前為止的時間1000為一秒
        public long GetCountPlayTime() { return m_CountPlayTime; }

        public DynamicTexture()
        {
            m_Textures = null;
            m_PlayControl = null;
        }

        public void Dispose()
        {
            m_PlayControl.Dispose();
            m_PlayControl = null;

            for (int i = 0; i < m_Textures.Length; i++)
            {
                if (m_Textures[i] != null)
                {
                    if (m_ifXNB == false)
                        m_Textures[i].Dispose();
                    m_Textures[i] = null;
                }
            }
            m_Textures = null;
        }

        //public static bool CreateXML_BinData;//設定CreateDynamicTextureXML會存成bin檔，true的話存檔讀檔都會以bin格式存取。
        //     public static bool CreateXMLData = true;//設定不建立XML，直接讀取檔案。
        //     static public void CreateDynamicTextureXML(string name, Digit digit, float startPlayTime,
        //         int numberStart, int numberEnd, uint timeIntervel, int loopTo)
        //     {
        //      //   DynamicTextureVersion version;
        //     //    version.version = 1.0f;

        //         int textureAmount = numberEnd - numberStart + 1;
        //         DynamicTextureData dynamicTexture;
        //         dynamicTexture.textureName = name;
        //         dynamicTexture.textureAmount = textureAmount;
        //         dynamicTexture.timeIntervel = timeIntervel;
        //         dynamicTexture.startPlayTime = startPlayTime;
        //         dynamicTexture.textureData = new TextureData[textureAmount];
        //         for (uint i = 0; i < textureAmount; i++)
        //         {
        //             dynamicTexture.textureData[i].startTime = timeIntervel * i;

        //          //   int a= int.Parse(i, 2);
        //             int number = (int)(i + numberStart);
        //             string D=null;
        //             switch (digit)//01 或1或001看幾位數
        //             {
        //                 case Digit.One: D = "d1"; break;
        //                 case Digit.Two: D = "d2"; break;
        //                 case Digit.Three: D = "d3"; break;
        //                 case Digit.Four: D = "d4"; break;
        //             }
        //             string a = number.ToString(D);
        //             dynamicTexture.textureData[i].textureName = name + a;

        //             dynamicTexture.textureData[i].gotoBlock = (int)i + 1;

        //             if (i == textureAmount - 1)
        //             {
        //                 if (loopTo != -1)
        //                     dynamicTexture.textureData[i].gotoBlock = loopTo - numberStart;//1~7是陣列0~6，11到17也是陣列0~6，所以...要減
        //                 else
        //                     dynamicTexture.textureData[i].gotoBlock = loopTo;
        //             }

        //         }

        //         if (CreateXMLData == false)
        //         {
        //             m_CreateXMLDynamicTextureData = dynamicTexture;
        //             return;
        //         }

        //         string workDirectory = Directory.GetCurrentDirectory();
        //         Directory.CreateDirectory(workDirectory);
        //         FileStream FP = File.Open(workDirectory+"\\"+name+numberStart.ToString()+numberEnd.ToString()+".xml", FileMode.Create);
        //         XmlSerializer xmlSerializer;

        //    //     xmlSerializer = new XmlSerializer(typeof(DynamicTextureVersion));
        ////         xmlSerializer.Serialize(FP, version);

        //         xmlSerializer = new XmlSerializer(typeof(DynamicTextureData));



        //         xmlSerializer.Serialize(FP, dynamicTexture);
        //         FP.Close();

        //         //釋放
        //         xmlSerializer = null;
        //         dynamicTexture.textureData = null;
        //     }

        //public void LoadDynamicTextureXML(string DynamicTextureFileName, ContentManager content)
        //{
        //    FileStream fp=null;
        //    XmlSerializer xmlSerializer=null;
        //    if (CreateXMLData)
        //    {
        //        string workDirectory = Directory.GetCurrentDirectory() + "\\" + DynamicTextureFileName;
        //        if (File.Exists(workDirectory))
        //        {
        //            //讀檔
        //            fp = File.Open(workDirectory, FileMode.Open);
        //            xmlSerializer = new XmlSerializer(typeof(DynamicTextureData));
        //            m_DynamicTextureData = (DynamicTextureData)xmlSerializer.Deserialize(fp);
        //        }
        //        else
        //            OutputBox.ShowMessage(DynamicTextureFileName + " Fille not found !!!");
        //    }
        //    else
        //        m_DynamicTextureData = m_CreateXMLDynamicTextureData;

        //    //建立資料
        //    m_PlayControl = new TimePlayControl(m_DynamicTextureData.textureAmount);
        //    m_Textures = new Texture2D[m_DynamicTextureData.textureAmount];
        //    for (int i = 0; i < m_DynamicTextureData.textureAmount; i++)
        //    {                 
        //        m_Textures[i] = Utility.ContentLoad_Texture2D(content, m_DynamicTextureData.textureData[i].textureName);

        //        int gotoBlock = m_DynamicTextureData.textureData[i].gotoBlock;
        //        //       if (i == m_DynamicTextureData.textureAmount - 1)
        //        //          gotoBlock = -1;

        //        float timeIntervel = m_DynamicTextureData.timeIntervel;
        //        if (i == 0)
        //            timeIntervel += m_DynamicTextureData.startPlayTime;

        //        m_PlayControl.SetBlockData(i,
        //            (uint)m_DynamicTextureData.textureData[i].startTime,
        //            (ushort)timeIntervel, (short)gotoBlock);
        //    }

        //    if (CreateXMLData)
        //    {
        //        fp.Close();
        //        xmlSerializer = null;
        //    }
        //}

        /// <summary>
        /// 記錄是否讀入XNB圖檔，若為XNB連續圖擋，則釋放時不要忽叫Dispose()
        /// </summary>
        bool m_ifXNB = false;

        public void CreateDynamicTexture(string name, Digit digit, int startPlayTime,
            int numberStart, int numberEnd, int timeIntervel, int loopTo, string fileSub, ContentManager content, GraphicsDevice device)
        {
            FileName = name;
            StartNumber = numberStart;
            EndNumber = numberEnd;
            NumberDigit = digit;
            StartPlayTime = startPlayTime;
            TimeIntervel = timeIntervel;
            LoopTo = loopTo;
            FileSub = fileSub;

            m_ifXNB = (FileSub == ".xnb");

            int textureAmount = numberEnd - numberStart + 1;
            DynamicTextureData dynamicTexture;

            //   dynamicTexture.textureAmount = textureAmount;
            //   dynamicTexture.timeIntervel = timeIntervel;
            //   dynamicTexture.startPlayTime = startPlayTime;
            dynamicTexture.textureData = new TextureData[textureAmount];
            for (int i = 0; i < textureAmount; i++)
            {
                dynamicTexture.textureData[i].startTime = timeIntervel * i;

                //   int a= int.Parse(i, 2);
                int number = (int)(i + numberStart);
                string D = null;
                switch (digit)//01 或1或001看幾位數
                {
                    case Digit.One: D = "d1"; break;
                    case Digit.Two: D = "d2"; break;
                    case Digit.Three: D = "d3"; break;
                    case Digit.Four: D = "d4"; break;
                }
                string a = number.ToString(D);

                if (m_ifXNB)
                    dynamicTexture.textureData[i].textureName = name + a;
                else
                    dynamicTexture.textureData[i].textureName = name + a + FileSub;

                dynamicTexture.textureData[i].gotoBlock = (int)i + 1;

                if (i == textureAmount - 1)
                {
                    if (loopTo != -1)
                        dynamicTexture.textureData[i].gotoBlock = loopTo - numberStart;//1~7是陣列0~6，11到17也是陣列0~6，所以...要減
                    else
                        dynamicTexture.textureData[i].gotoBlock = loopTo;
                }
            }



            //建立資料
            m_PlayControl = new TimePlayControl(textureAmount);
            m_Textures = new Texture2D[textureAmount];
            for (int i = 0; i < textureAmount; i++)
            {
                if (m_ifXNB)
                    m_Textures[i] = Utility.ContentLoad_Texture2D(content, Utility.GetFullPath(dynamicTexture.textureData[i].textureName));
                else
                    m_Textures[i] = Utility.Load_Texture2D(device, content, dynamicTexture.textureData[i].textureName, false);

                int gotoBlock = dynamicTexture.textureData[i].gotoBlock;
                //       if (i == m_DynamicTextureData.textureAmount - 1)
                //          gotoBlock = -1;

                float intervel = timeIntervel;
                if (i == 0)
                    intervel += startPlayTime;

                m_PlayControl.SetBlockData(i,
                    (uint)dynamicTexture.textureData[i].startTime,
                    (ushort)intervel, (short)gotoBlock);
            }

            //m_DynamicTextureData = dynamicTexture;
        }

        //public string Name
        //{
        //    get { return m_DynamicTextureData.textureName; }
        //}

        public void Play(bool bLoop, float speed)
        {
            m_CountPlayTime = 0;
            m_PlayControl.play();
            m_PlayControl.playloop(bLoop);
            m_PlayControl.PlaySpeed = speed;
        }

        public void PlayBack(bool bLoop, float speed)
        {
            m_CountPlayTime = 0;
            m_PlayControl.playback();
            m_PlayControl.playloop(bLoop);
            m_PlayControl.PlaySpeed = speed;
        }

        public void Stop()
        {
            m_PlayControl.stop();
        }

        public bool IfPlay()
        {
            return m_PlayControl.IfPlay;
        }

        public void RefreshTime(int ellapseMillisecond)
        {
            m_CountPlayTime += ellapseMillisecond;
            m_PlayControl.RefreshTime(ellapseMillisecond);
        }

        public Texture2D GetTexture()
        {
            int id = (int)MathHelper.Min(m_PlayControl.NowPlayBlockID, m_Textures.Length - 1);
            return m_Textures[id];
        }

        public Texture2D GetNextTexture()
        {
            if (m_PlayControl.GetNextBlock() != -1)
                return m_Textures[m_PlayControl.GetNextBlock()];
            else
                return m_Textures[m_Textures.Length - 1];
            //return null;//給NULL會跟NUU圖座內插
        }

        /// <summary>
        /// 取得目前撥放格的撥放比例1表示要換到下一格
        /// </summary>
        public float GetPlayPercent
        {
            get { return m_PlayControl.GetPlayPercent; }
        }


    }
}
