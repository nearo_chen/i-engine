﻿#region File Description
//-----------------------------------------------------------------------------
// GCLib.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Injoy Motion Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

using Microsoft.Xna.Framework;

#endregion

namespace I_XNAComponent
{
    public static class WINPC32
    {
        [DllImport("MemAPI32.dll")]
        public static extern void _InitializeVRAMXXYAHXZ();

        [DllImport("MemAPI32.dll")]
        public static extern bool _GDEGetBitXXYAKEKPAKXZ(byte type, uint offset, [MarshalAs(UnmanagedType.LPArray)] byte[] outvalue);


        [DllImport("MemAPI32.dll")]
        public static extern void _CloseVRAMXXYAHXZ();

        [DllImport("MemAPI32.dll")]
        public static extern bool _GDESetBitXXYAKEKKXZ(byte type, uint offset, int value);


        static byte[] TempBuffer = new byte[sizeof(uint)];
        public static int GDEGetBit(uint port)
        {
            int i, values;

            WINPC32._GDEGetBitXXYAKEKPAKXZ(0x30, port, TempBuffer);
            values = 0;
            for (i = sizeof(uint) - 1; i >= 0; i--)
            {
                values <<= 8;
                values |= TempBuffer[i];
            }

            return (values);
        }


        public static void GDESetBit(uint port, int value)
        {
            WINPC32._GDESetBitXXYAKEKKXZ(0x30, port, value);
        }
    }

    public static class MyGCLib
    {
        #region Variables
        /// <summary>
        /// Video Open Sucessful...
        /// </summary>
        private static bool is_enable = false;
        /// <summary>
        /// GCLIB ON/OFF
        /// </summary>
        public static bool EnableGCLib
        {
            get
            {
                return is_enable;
            }
            //set
            //{
            //    is_enable = value;
            //}
        }
        private static string myhostname = null;
        public static string MyHostName
        {
            get
            {
                return myhostname;
            }
        }

        private static string myip = null;
        public static string MyIP
        {

            get
            {
                return myip;
            }
        }
        #region To Connect WINPC 32
        public static int GC_x;
        public static int GC_y;
        public static int GC_z;
        public static float[] CostValue = new float[3];
        public static uint[] CostValueF = new uint[3];
        public static uint Q;
        public static int Err;
        public static UInt64 QB;
        #endregion

        const string default_ip = "127.0.0.1";

        #endregion

        #region DLL Functions



        [DllImport("GC_LIB.dll")]
        private static extern int _GC_InitialXXYAHXZ();

        [DllImport("GC_LIB.dll")]
        private static extern void _GC_FreeXXYAXXZ();

        [DllImport("GC_LIB.dll")]
        private static extern int _Randon_FunctionXXYAHXZ();

        [DllImport("GC_LIB.dll")]
        private static extern float _CostFunction1XXYAMHHHXZ(int a, int b, int c);

        [DllImport("GC_LIB.dll")]
        private static extern float _CostFunction2XXYAMHHHXZ(int a, int b, int c);

        [DllImport("GC_LIB.dll")]
        private static extern float _CostFunction3XXYAMHHHXZ(int a, int b, int c);

        [DllImport("GC_LIB.dll")]
        private static extern uint _ProductFunctionXXYAKMHXZ(float a, int b);

        [DllImport("GC_LIB.dll")]
        private static extern uint _GenerateCodeXXYAKKKKXZ(uint a, uint b, uint c);

        [DllImport("GC_LIB.dll")]
        private static extern uint _GetHostNameXXYAKPADXZ([MarshalAs(UnmanagedType.LPArray)] byte[] outvalue);



        #endregion
        


        public static bool InitializeGCLib()
        {
            if (is_enable)
                return false;

            int values = 0;
            values = _GC_InitialXXYAHXZ();

            WINPC32._InitializeVRAMXXYAHXZ();

            is_enable = (values != 0) ? true : false;

            if (EnableGCLib)
            {
                GC_x = Randon_Function();
                GC_y = Randon_Function();
                GC_z = Randon_Function();
                for (int i = 0; i < 3; i++)
                {
                    values = Randon_Function();
                    CostValue[i] = (float)values;
                    CostValue[i] /= 1000.0f;
                    values = Randon_Function();
                    CostValueF[i] = (uint)values;
                }
                values = Randon_Function();
                Q = (uint)values;
                Err = Randon_Function();
                values = Randon_Function();
                QB = (uint)values;
            }
            return EnableGCLib;
        }

        public static void FreeGCLib()
        {

            // _GC_InitialXXYAHXZ();
            WINPC32._CloseVRAMXXYAHXZ();
            _GC_FreeXXYAXXZ();

            is_enable = false;
        }

        public static int Randon_Function()
        {
            int ivalue = 0;
            if (EnableGCLib)
                ivalue = _Randon_FunctionXXYAHXZ();
            return (ivalue);
        }

         public static int GDEGetBit(uint port)
        {
            return WINPC32.GDEGetBit(port);
        }


         public static void GDESetBit(uint port, int value)
         {
             WINPC32.GDESetBit(port, value);
         }

        public static float CostFunction1(int x, int y, int z)
        {
            float fvalue = 0.0f;
            if (EnableGCLib)
                fvalue = _CostFunction1XXYAMHHHXZ(x, y, z);
            return (fvalue);
        }

        public static float CostFunction2(int x, int y, int z)
        {
            float fvalue = 0.0f;
            if (EnableGCLib)
                fvalue = _CostFunction2XXYAMHHHXZ(x, y, z);
            return (fvalue);
        }

        public static float CostFunction3(int x, int y, int z)
        {
            float fvalue = 0.0f;
            if (EnableGCLib)
                fvalue = _CostFunction3XXYAMHHHXZ(x, y, z);
            return (fvalue);
        }

        public static uint ProductFunction(float x, int y)
        {
            uint dwvalue = 0;
            if (EnableGCLib)
                dwvalue = _ProductFunctionXXYAKMHXZ(x, y);
            return (dwvalue);
        }

        public static uint GenerateCode(uint x, uint y, uint z)
        {
            uint dwvalue = 0;
            if (EnableGCLib)
                dwvalue = _GenerateCodeXXYAKKKKXZ(x, y, z);
            return (dwvalue);
        }

        #region GCLIB Check Pass ...
        public static bool GCBLIB_Check()
        {
            float fdiff, my_fcode;

            CostValue[0] = CostFunction1(GDEGetBit(101), GDEGetBit(102), GDEGetBit(103));
            CostValue[1] = CostFunction2(GDEGetBit(101), GDEGetBit(102), GDEGetBit(103));
            CostValue[2] = CostFunction3(GDEGetBit(101), GDEGetBit(102), GDEGetBit(103));
            QB = (UInt64)GDEGetBit(104);

            //Generate Code
            CostValueF[0] = ProductFunction(MyGCLib.CostValue[0], 488629);
            CostValueF[1] = ProductFunction(MyGCLib.CostValue[1], 407076);
            CostValueF[2] = ProductFunction(MyGCLib.CostValue[2], 126576);
            //
            MyGCLib.Q = GenerateCode(MyGCLib.CostValueF[0], MyGCLib.CostValueF[1], MyGCLib.CostValueF[2]);
            //ShareMemory output
            //unsigned long gc.QB
            fdiff = (float)QB;
            my_fcode = (float)Q;
            fdiff = fdiff - my_fcode;
            my_fcode = Math.Abs(fdiff);
            return (my_fcode <= 3.0f);
            //if (my_fcode <= 3.0f)
            //{
            //    test_value++;
            //    //PassCode();
            //}
            //else
            //{
            //    test_value--;
            //    //ErrorCode();
            //}
            //End of Test
        }
        #endregion


        public static uint GetHostInfo()
        {
            uint ret_val = 0;
            int val, no;
            byte[] host_name = new byte[256];

            ret_val = _GetHostNameXXYAKPADXZ(host_name);
            for (int i = 0; host_name[i] != 0; i++)
            {
                char cc = (char)host_name[i];
                myhostname += cc;
            }

            if (ret_val != 0)
            {
                val = (int)ret_val;
                for (int j = 0; j < 4; j++)
                {
                    no = val % 256;
                    val /= 256;
                    if (j == 0)
                        myip += no.ToString();
                    else
                        myip += '.' + no.ToString();
                }
            }
            else
                myip = default_ip;

            return (ret_val);
        }

    }
}
