﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;
using MeshDataDefine;


namespace I_XNAComponent
{
    //模型設定通用基本的元素都可以設定在此CLASS
    abstract public partial class BasicMeshLoader : IDisposable
    {
        //static IDManager BasicMeshLoaderIDManager = null;
        abstract protected void v_Dispose();
        abstract protected void v_Update(int ellapseMillisecond, ContentManager content, GraphicsDevice device);
        abstract public void RenderDebug(SpriteBatch spriteBatch);

        //給外部sceneManager判斷用，此loader的parent資訊
        public FrameNode ParentNode = null;//parent在哪個node上
        public string ParentLoaderName = null;//parent在哪個loader的哪個node中
        public string ParentNodeName = null;

        protected LocatorNode m_RootNode;
        public LocatorNode GetRootNode { get { return m_RootNode; } }

        //int  m_ID = -1;
        //public int ID { get { return m_ID; } }

        #region SpecialObject
        I_ParticleManager m_ParticleManager = null;
        protected Dictionary<LocatorNode, MultiObjectPlayer[]> m_MuliObjectPlayerList = null;
        // Dictionary<LocatorNode, MultiObjectPlayer[]> MuliObjectPlayerList { get { return m_MuliObjectPlayerList; } }
        List<EffectObject> m_EffectObjectList = null;
        #endregion

        #region 整個場景的BoundingVolume
        BoundingSphere m_BoundingSphere;
        BoundingBox m_BoundingBox;
        Matrix m_BoundingFrustumMatrix;
        public BoundingSphere BoundingSphere
        {
            get { return m_BoundingSphere; }
            set { m_BoundingSphere = value; }
        }
        public BoundingBox BoundingBox
        {
            get { return m_BoundingBox; }
            set { m_BoundingBox = value; }
        }
        public Matrix BoundingFrustumMatrix
        {
            get { return Matrix.Invert(m_RootNode.WorldMatrix) * m_BoundingFrustumMatrix; }
            set { m_BoundingFrustumMatrix = value; }
        }
        #endregion

        #region PointLight
        LocatorNode m_StaticPointLightParent;
        LocatorNode m_StaticSpotLightParent;
        //PointLightNode[] m_StaticPointLightNodeList;
        //public PointLightNode[] StaticPointLightList { get { return m_StaticPointLightNodeList; } }
        #endregion

        //#region Depth map
        //DepthMapCreator m_DepthMapCreator;
        //#endregion

        public const string RootNodeName = "Root Node ~ ~";

        //初始化BasicNode基本資訊
        protected void InitBasicMeshLoader(BasicNode childNode, BoundingSphere boundingSphere, BoundingBox boundingBox)
        {
            //if (BasicMeshLoaderIDManager == null)
            //    BasicMeshLoaderIDManager = new IDManager("BasicMeshLoaderIDManager", 1024);

            //m_ID = BasicMeshLoaderIDManager.GetID();

            m_RootNode = LocatorNode.Create();
            m_RootNode.NodeName = RootNodeName;
            m_RootNode.AttachChild(childNode);

            //計算整個模型的bound box之類的         
            m_BoundingSphere = boundingSphere;
            m_BoundingBox = boundingBox;
            Utility.BoundBoxToBoundingFrustum(ref m_BoundingBox, out m_BoundingFrustumMatrix);
        }

        public BasicMeshLoader()
        {
        }

        ~BasicMeshLoader()
        {
            ReleaseCheck.DisposeCheckCount(this);
        }

        public void Dispose()
        {
            ParentNode = null;
            ParentLoaderName = null;
            ParentNodeName = null;

            ReleaseCheck.DisposeCheck(this);



            ReleaseParticleManager();
            ReleaseAllMultiObjectPlayerList();
            EffectObjectList_Release();

            //BasicNode root = m_RootNode as BasicNode;
            BasicNode.DisposeAllTree(m_RootNode);//m_RootNode樹狀結構釋放
            m_RootNode = null;

            //m_GodsRayLightNodeList = null;
            //m_StaticPointLightNodeList = null;

            //if (m_DepthMapCreator != null)
            //{
            //    m_DepthMapCreator.Dispose();
            //    m_DepthMapCreator = null;
            //}

            //if (m_MeshNodeArray != null)
            //    m_MeshNodeArray.Dispose();
            //m_MeshNodeArray = null;

            v_Dispose();
        }

        #region Particle Manager
        /// <summary>
        /// 根據所有mop，統計共需要幾個不同種類的particle system，建立particle system。
        /// </summary>
        void UpdateParticleManager(ContentManager content, GraphicsDevice device)
        {
            #region 初始化particle manager
            if (m_ParticleManager == null && m_MuliObjectPlayerList != null)
            {
                m_ParticleManager = I_ParticleManager.Create();

                //先統計所有的mop看需要幾種system，還有根據定位點數量做初始化。
                //將mop根據particle system分類。
                Dictionary<LocatorNode, MultiObjectPlayer[]>.Enumerator itr = m_MuliObjectPlayerList.GetEnumerator();
                while (itr.MoveNext())
                {
                    MultiObjectPlayer[] moparray = itr.Current.Value as MultiObjectPlayer[];
                    for (int a = 0; a < moparray.Length; a++)
                    {
                        m_ParticleManager.AddMOPParticle(moparray[a]);
                    }
                }
                itr.Dispose();
            }
            #endregion

            if (m_ParticleManager != null)
                m_ParticleManager.UpdateParticleSystem(content, device, GetRootNode.WorldMatrix, GetRootNode.InverseWorlMatrix);
        }

        void ReleaseParticleManager()
        {
            if (m_ParticleManager == null)
                return;
            m_ParticleManager.Dispose();
            m_ParticleManager = null;
        }

        public void RenderParticle(ref Matrix view, ref Matrix proj, ref Vector3 viewPos)
        {
            if (m_ParticleManager == null)
                return;
            m_ParticleManager.Render(ref view, ref proj, ref viewPos);
        }
        #endregion



        #region EffectObject處理
        public void AddOceanInScene(SpecialMeshDataManager.MyOceanData ocean,
            ContentManager content, GraphicsDevice device)
        {
            AddEffectObject(ocean, content, device);
        }

        void AddEffectObject(SpecialMeshDataManager.MyOceanData ocean,
            ContentManager content, GraphicsDevice device)
        {
            EffectObject eo = null;
            if (ocean.Paramaters != null)
            {
                eo = new EffectObject();
                eo.CreateOcean(m_RootNode, ocean, content, device);
            }

            if (m_EffectObjectList == null)
                m_EffectObjectList = new List<EffectObject>();
            m_EffectObjectList.Add(eo);
            m_DepthMap_EffectObjectItr.Dispose();
            m_DepthMap_EffectObjectItr = m_EffectObjectList.GetEnumerator();
            //m_CubeMap_EffectObjectItr.Dispose();
            //m_CubeMap_EffectObjectItr = m_EffectObjectList.GetEnumerator();
        }

        /// <summary>
        /// 回傳false表示EffectObject陣列跑到底了。
        /// </summary>
        public bool EffectObject_SetRT_PreRenderDepthMap(GraphicsDevice device,
            out Matrix? viewMatrix, out Matrix? projectionMatrix, out NodeArray nodeArray)
        {
            nodeArray = null;
            viewMatrix = null;
            projectionMatrix = null;
            if (m_EffectObjectList == null)
                return false;

            if (m_DepthMap_EffectObjectItr.MoveNext())
            {
                m_DepthMap_EffectObjectItr.Current.SetRenderTarget_PreRenderDepthMap(device, out viewMatrix, out projectionMatrix);
                nodeArray = m_DepthMap_EffectObjectItr.Current.GetDepthMapRenderNodeArray();
                return true;
            }

            //m_DepthMap_EffectObjectItr.Dispose();
            //m_DepthMap_EffectObjectItr = m_EffectObjectList.GetEnumerator();//串列重頭開始，準備畫下一個面。
            return false;
        }

        List<EffectObject>.Enumerator m_DepthMap_EffectObjectItr;//紀錄EffectObject List目前跑到哪一個。
        //    List<EffectObject>.Enumerator m_CubeMap_EffectObjectItr;//紀錄EffectObject List目前跑到哪一個。
        //   int m_EffectObjectRenderFace = 0;//紀錄EffectObject List的cube map畫到哪一面了。
        /// <summary>
        /// 給EffectObject設定他們各自cubeMap的render target。
        /// 回傳true表示可以畫出此EffectObject的一個面。
        /// 傳出viewMatrix為null表示此effect不畫cube map。
        /// 每call一次function會設定一個effect object的SetPreRenderCubeMap()，用來畫該EffectObject的一個面。
        /// </summary>
        //public bool EffectObject_SetRT_PreRenderCubeMap1Face(GraphicsDevice device, out Matrix? viewMatrix)
        //{
        //    viewMatrix = null;
        //    if (m_EffectObjectList == null)
        //        return false;

        //    //一次處理畫一個面，並回傳畫該面的view matrix。
        //    if (m_CubeMap_EffectObjectItr.MoveNext())
        //    {
        //        viewMatrix = m_CubeMap_EffectObjectItr.Current.SetRenderTarget_PreRenderCubeMap(device);
        //        return true;
        //    }

        //    m_EffectObjectRenderFace++;//List結束表示畫完cube map的一個面

        //    //如果畫到第7個面就不用在畫下去了，m_EffectObjectItr不重抓，上面MoveNext()就是false，
        //    //就會回傳false，外面收到false就不會畫下去了。
        //    if (m_EffectObjectRenderFace < 6)
        //    {
        //        m_CubeMap_EffectObjectItr.Dispose();
        //        m_CubeMap_EffectObjectItr = m_EffectObjectList.GetEnumerator();//串列重頭開始，準備畫下一個面。
        //    }
        //    else
        //        m_EffectObjectRenderFace = 6;
        //    return false;
        //}

        /// <summary>
        /// 釋放m_EffectObjectList裡面所有的東西。
        /// </summary>
        public void EffectObjectList_Release()
        {
            //    m_EffectObjectRenderFace = 0;
            //  m_CubeMap_EffectObjectItr.Dispose();
            m_DepthMap_EffectObjectItr.Dispose();
            if (m_EffectObjectList != null)
            {
                for (int a = 0; a < m_EffectObjectList.Count; a++)
                    m_EffectObjectList[a].Dispose();
                m_EffectObjectList.Clear();
                m_EffectObjectList = null;
            }
        }

        public void EffectObjectList_Render(ref Matrix view, ref Matrix invView, ref Matrix proj,
            GraphicsDevice device, ContentManager content,
            Vector3 lightDirection, Vector3 LightAmbient, Vector3 lightDiffuse, Vector3 LightSpecular,
            Vector3 fogColor, float fogStart, float fogEnd, CubeMapCreator cubeMap)
        {
            if (m_EffectObjectList == null)
                return;

            List<EffectObject>.Enumerator itr = m_EffectObjectList.GetEnumerator();
            while (itr.MoveNext())
            {
                itr.Current.Render(//GetTime.GetGameTime(),
                    I_GetTime.ellipseUpdateSecond,
                    ref view, ref invView, ref proj, device, content,
                    lightDirection, LightAmbient, lightDiffuse, LightSpecular,
                    fogColor, fogStart, fogEnd, cubeMap);
#if DrawDebug
                itr.Current.DrawDebugLine();
#endif
            }
        }

        public void EffectObjectList_Update()
        {
            if (m_EffectObjectList == null)
                return;

            List<EffectObject>.Enumerator itr = m_EffectObjectList.GetEnumerator();
            while (itr.MoveNext())
            {
                itr.Current.Update();
            }
        }

        //public void EffectObjectList_SaveEnvMapToDDS()
        //{
        //    m_EffectObjectList[0].SaveEnvMapToDDS();
        //}

        public Texture2D EffectObjectList_GetDepthMapTexture()
        {
            if (m_EffectObjectList == null || m_EffectObjectList[0].GetDepthMap() == null)
                return null;
            return m_EffectObjectList[0].GetDepthMap();
        }

        //public void UpdateEffectObjects(ref Matrix view, ref Matrix invView, ref Matrix proj, GraphicsDevice device)
        //{
        //    List<EffectObject>.Enumerator itr = m_EffectObjectList.GetEnumerator();
        //    while (itr.MoveNext())
        //    {
        //        itr.Current.Update();
        //    }
        //}
        #endregion

        //int ellapseMillisecond, , 
        public void Update(int ellapseMillisecond, ContentManager content, GraphicsDevice device,
            MySound sound, ref Matrix invView)
        {
            //if(IfDrawBoundingSphere)
            //    DrawLine.AddSphereLine(Matrix.Identity, BoundingSphere.Center, BoundingSphere.Radius, new Color(Color.Goldenrod, 0.3f), 20, 20);
            //if (IfDrawBoundingBox)
            //    DrawLine.AddBoundingBoxLine(Matrix.Identity, BoundingBox, new Color(Color.AliceBlue, 0.3f));


            //Update MOP
            if (m_MuliObjectPlayerList != null)
            {
                Dictionary<LocatorNode, MultiObjectPlayer[]>.Enumerator itr = m_MuliObjectPlayerList.GetEnumerator();
                while (itr.MoveNext())
                {
                    MultiObjectPlayer[] moparray = itr.Current.Value as MultiObjectPlayer[];
                    for (int a = 0; a < moparray.Length; a++)
                    {
                        moparray[a].Update(ellapseMillisecond, content, device, sound, ref invView);
                    }
                }
                itr.Dispose();
            }

            //Update Particle
            UpdateParticleManager(content, device);

            v_Update(ellapseMillisecond, content, device);
        }

        //public bool IfDrawBoundingBox = false;
        //public bool IfDrawBoundingSphere = false;

        #region PointLight
        /// <summary>
        /// 根據POINTLIGHT資訊新增一個point light node於loader內，
        /// 若是有parent就直接去找他的parent node，attach在他底下。
        /// 若是無parent就直接attach在m_StaticPointLightParent之下。
        /// </summary>
        public void AddPointLight(ref PointLight pl)
        {
            PointLightNode PLNode = PointLightNode.Create();
            PLNode.SetPointLightValu(pl);

            if (string.IsNullOrEmpty(pl.ParentNodeName))
            {
                if (m_StaticPointLightParent == null)
                {
                    m_StaticPointLightParent = LocatorNode.Create();
                    m_StaticPointLightParent.NodeName = "Static_PointLights";
                    GetRootNode.AttachChild(m_StaticPointLightParent);
                }
                m_StaticPointLightParent.AttachChild(PLNode);
            }
            else
            {
                FrameNode parent = GetRootNode.SeekFirstNodeAllName(ref pl.ParentNodeName) as FrameNode;
                parent.AttachChild(PLNode);
            }
        }

        /// <summary>
        /// 根據名稱搜出point light node並將他dispose()
        /// </summary>
        /// <param name="pointLightName"></param>
        public void RemovePointLight(ref string pointLightName)
        {
            PointLightNode PLNode = GetRootNode.SeekFirstNodeAllName(ref pointLightName) as PointLightNode;
            PLNode.DetachFromParent();
            PLNode.Dispose();
        }

        /// <summary>
        /// 更新point light內容
        /// </summary>
        /// <param name="pl"></param>
        public void SetPointLight(ref PointLight pl)
        {
            PointLightNode PLNode = GetRootNode.SeekFirstNodeAllName(ref pl.Name) as PointLightNode;
            PLNode.SetPointLightValu(pl);
        }
        #endregion

        #region SpotLight
        /// <summary>
        /// 根據SPOTLIGHT資訊新增一個spot light node於loader內，
        /// 若是有parent就直接去找他的parent node，attach在他底下。
        /// 若是無parent就直接attach在m_StaticSpotLightParent之下。
        /// </summary>
        public void AddSpotLight(ref SpotLight spotlight)
        {
            SpotLightNode SPLNode = SpotLightNode.Create();
            SPLNode.SetSpotLightValue(spotlight);

            if (string.IsNullOrEmpty(spotlight.ParentNodeName))
            {
                if (m_StaticSpotLightParent == null)
                {
                    m_StaticSpotLightParent = LocatorNode.Create();
                    m_StaticSpotLightParent.NodeName = "Static_SpotLights";
                    GetRootNode.AttachChild(m_StaticSpotLightParent);
                }
                m_StaticSpotLightParent.AttachChild(SPLNode);
            }
            else
            {
                FrameNode parent = GetRootNode.SeekFirstNodeAllName(ref spotlight.ParentNodeName) as FrameNode;
                parent.AttachChild(SPLNode);
            }
        }

        /// <summary>
        /// 根據名稱搜出spot light node並將他dispose()
        /// </summary>
        /// <param name="pointLightName"></param>
        public void RemoveSpotLight(ref string spotLightName)
        {
            SpotLightNode SPLNode = GetRootNode.SeekFirstNodeAllName(ref spotLightName) as SpotLightNode;
            SPLNode.DetachFromParent();
            SPLNode.Dispose();
        }

        /// <summary>
        /// 更新spot light內容
        /// </summary>
        /// <param name="pl"></param>
        public void SetSpotLight(ref SpotLight spl)
        {
            SpotLightNode SPLNode = GetRootNode.SeekFirstNodeAllName(ref spl.Name) as SpotLightNode;
            SPLNode.SetSpotLightValue(spl);
        }
        #endregion

        //#region 此Loader的ShadowMap
        //public void RenderDepthMapBegin(int mapSize, int RTID, GraphicsDevice device, NodeArray renderNodeArray,
        //    ref Vector3 lightDirection, out Matrix? viewMatrix, out Matrix? projMatrix)
        //{
        //    if (m_DepthMapCreator == null)
        //    {
        //        m_DepthMapCreator = DepthMapCreator.Create();// ReleaseCheck.CreateNew(typeof(DepthMapCreator)) as DepthMapCreator;
        //        m_DepthMapCreator.Initialize(mapSize, lightDirection, device);
        //    }
        //    m_DepthMapCreator.SetBoundBoxNodeArray(renderNodeArray);
        //    m_DepthMapCreator.SetRenderTarget(RTID, device, lightDirection, out viewMatrix, out projMatrix);
        //}

        //public void RenderDepthMapEnd()
        //{
        //    m_DepthMapCreator.ReleaseBoundBoxNodeArray();
        //}

        //public bool NotComputeDepthmap = false;
        //public Texture2D GetDepthMap()
        //{
        //    if (m_DepthMapCreator == null)
        //        return null;
        //    if (NotComputeDepthmap)
        //        return null;
        //    return m_DepthMapCreator.GetDepthMap;
        //}

        //public Matrix GetDepthMapFrustum()
        //{
        //    return m_DepthMapCreator.DepthMapFrustum;
        //}
        //#endregion

        //#region Gods Ray Light Data
        //LocatorNode m_GodsRayRootNode;
        //GodsRayLightNode[] m_GodsRayLightNodeList = null;
        //public GodsRayLightNode[] GodsRayLightNodes { get { return m_GodsRayLightNodeList; } }
        //public void ClearGodsRayLightNodeList()
        //{
        //    if (m_GodsRayRootNode == null)
        //        return;
        //    //BasicNode nnn = m_GodsRayRootNode as BasicNode;
        //    BasicNode.DisposeAllTree(m_GodsRayRootNode);
        //    m_GodsRayRootNode = null;
        //    for (int a = 0; a < m_GodsRayLightNodeList.Length; a++)
        //        m_GodsRayLightNodeList[a] = null;
        //    m_GodsRayLightNodeList = null;
        //}

        //public void CreateGodsRayLightNodeList(SpecialMeshDataManager.GodsRayLightData[] GodsRayLightDatas)
        //{
        //    if (GodsRayLightDatas == null || GodsRayLightDatas.Length == 0)
        //        return;

        //    m_GodsRayRootNode = LocatorNode.Create();
        //    m_GodsRayRootNode.NodeName = "GodsRayRootNode";
        //    m_RootNode.AttachChild(m_GodsRayRootNode);

        //    m_GodsRayLightNodeList = new GodsRayLightNode[GodsRayLightDatas.Length];
        //    for (int a = 0; a < m_GodsRayLightNodeList.Length; a++)
        //    {
        //        m_GodsRayLightNodeList[a] = new GodsRayLightNode();
        //        m_GodsRayLightNodeList[a].NodeName = GodsRayLightDatas[a].Name;
        //        m_GodsRayLightNodeList[a].Decay = GodsRayLightDatas[a].Decay;
        //        m_GodsRayLightNodeList[a].Density = GodsRayLightDatas[a].Density;
        //        m_GodsRayLightNodeList[a].Exposition = GodsRayLightDatas[a].Exposition;
        //        m_GodsRayLightNodeList[a].Weight = GodsRayLightDatas[a].Weight;
        //        m_GodsRayLightNodeList[a].LocalPosition = GodsRayLightDatas[a].position;
        //        m_GodsRayLightNodeList[a].samples = GodsRayLightDatas[a].samples;
        //        m_GodsRayRootNode.AttachChild(m_GodsRayLightNodeList[a]);
        //    }
        //}
        //#endregion

        //#region 光和霧之類的屬性
        ////固定要的方向光，另外儲存方向光和霧的設定檔。
        //public Vector3 LightDirection = Vector3.One;
        //public Vector3 LightAmbient = Vector3.One;
        //public Vector3 LightDiffuse = Vector3.One;
        //public Vector3 LightSpecular = Vector3.One;
        //public Vector3 FogColor = Vector3.One;
        //public float FogStart = 0;
        //public float FogEnd = 100000;
        //public float FogMaxRatio = 1;
        //public float BigShadowMapMultiSampleOffset;
        //public float SmallShadowMapMultiSampleOffset;
        //public float BigShadowMapBias;
        //public float SmallShadowMapBias;
        //public float BigShadowMapSampleLength = 10000;
        //public float SmallShadowMapSampleLength = 10000;
        //public Vector3 BigShadowMapRatio;
        //public Vector3 SmallShadowMapRatio;
        //public float MipMapLevel;

        //public void DirectionLight1Set(Vector3 lightDirection, Vector3 lightAmbient, Vector3 lightDiffuse,
        //    Vector3 lightSpecular, float bbbbbbigShadowMapMultiSampleOffset, float ssssssmallShadowMapMultiSampleOffset,
        //    Vector3 bbbbigShadowMapRatio, Vector3 ssssmalligShadowMapRatio,
        //    float bbbbbigShadowMapBias, float ssssssmallShadowMapBias,
        //    float? bbbbbigShadowMapSampleLength, float? ssssssmallShadowMapSampleLength,
        //    float mipmapLevel)
        //{
        //    LightDirection = lightDirection;
        //    LightAmbient = lightAmbient;
        //    LightDiffuse = lightDiffuse;
        //    LightSpecular = lightSpecular;

        //    BigShadowMapMultiSampleOffset = bbbbbbigShadowMapMultiSampleOffset;
        //    SmallShadowMapMultiSampleOffset = ssssssmallShadowMapMultiSampleOffset;
        //    BigShadowMapBias = bbbbbigShadowMapBias;
        //    SmallShadowMapBias = ssssssmallShadowMapBias;

        //    BigShadowMapRatio = bbbbigShadowMapRatio;
        //    SmallShadowMapRatio = ssssmalligShadowMapRatio;

        //    if (bbbbbigShadowMapSampleLength.HasValue)
        //        BigShadowMapSampleLength = bbbbbigShadowMapSampleLength.Value;

        //    if (ssssssmallShadowMapSampleLength.HasValue)
        //        SmallShadowMapSampleLength = ssssssmallShadowMapSampleLength.Value;

        //    MipMapLevel = mipmapLevel;
        //}

        //public void FogSet(Vector3 fogColor, float fogStart, float fogEnd, float fogMaxRatio)
        //{
        //    FogColor = fogColor;
        //    FogStart = fogStart;
        //    FogEnd = fogEnd;
        //    FogMaxRatio = fogMaxRatio;

        //    //System.Drawing.Color color = System.Drawing.Color.FromArgb(
        //    //    (int)(fogColor.X * 255f), (int)(fogColor.Y * 255f), (int)(fogColor.Z * 255f));
        //}
        //#endregion



        #region 給動態模型用的，來判斷Multi Object Player與動作的關係
        /// <summary>
        /// 紀錄animation的ID，以及會與他一同播放的許多MOP node
        /// </summary>
        struct Animation_MOP
        {
            public string MOPAniName;//與動作無關，卻又要控制撥放的mop群組。
            public int animationID;
            public LocatorNode[] MOPNodes;
        }
        Animation_MOP[] m_Animation_MOP;

        static public void AnimationWithMOP_SetData(
            BasicMeshLoader loader, SpecialMeshDataManager.AnimateWithMOP[] animateWithMOPs)
        {
            if (loader.GetType() == typeof(KeyframeMeshLoader))
                loader.AnimationWithMOP_SetData(animateWithMOPs, ((KeyframeMeshLoader)loader).GetAniArray() );
            else if (loader.GetType() == typeof(SkinningMeshLoader))
                loader.AnimationWithMOP_SetData(animateWithMOPs, ((SkinningMeshLoader)loader).GetAniArray());
            else
                loader.AnimationWithMOP_SetData(animateWithMOPs, null);
        }

        /// <summary>
        /// 設定MOP動態撥放的資訊
        /// </summary>
        void AnimationWithMOP_SetData(SpecialMeshDataManager.AnimateWithMOP[] animateWithMOPs,
            AnimationTableDataArray aniArray)
        {
            if (animateWithMOPs == null)
                return;

            if (aniArray == null)//null原本是用來控制沒ani table的動態物件MOP，但後來改為沒aniArray的scene mesh loader控制MOP Group用
            {
                m_Animation_MOP = new Animation_MOP[animateWithMOPs.Length];
                for (int a = 0; a < animateWithMOPs.Length; a++)
                {
                    m_Animation_MOP[a].MOPAniName = animateWithMOPs[a].AnimationName;

                    string[] names = animateWithMOPs[a].MOPLocatorNames;
                    m_Animation_MOP[a].MOPNodes = new LocatorNode[names.Length];
                    for (int b = 0; b < names.Length; b++)
                    {
                        string name = SpecialMeshDataManager.LocatorList.GetLocatorRootNodeName(names[b]);
                        m_Animation_MOP[a].MOPNodes[b] =
                            GetRootNode.SeekFirstNodeAllName(ref name) as LocatorNode;
                    }
                }

                //m_Animation_MOP = new Animation_MOP[1];
                //for (int a = 0; a < animateWithMOPs.Length; a++)
                //{
                //    // int id = 0;
                //    m_Animation_MOP[a].animationID = 0;

                //    string[] names = animateWithMOPs[a].MOPLocatorNames;
                //    m_Animation_MOP[a].MOPNodes = new LocatorNode[names.Length];
                //    for (int b = 0; b < names.Length; b++)
                //    {
                //        m_Animation_MOP[0].MOPNodes[b] =
                //            GetRootNode.SeekFirstNodeAllName(
                //            SpecialMeshDataManager.LocatorList.GetLocatorRootNodeName(names[b])) as LocatorNode;
                //    }
                //}
            }
            else
            {
                m_Animation_MOP = new Animation_MOP[animateWithMOPs.Length];
                for (int a = 0; a < animateWithMOPs.Length; a++)
                {
                    //找出m_AnimationTableDataArray裡的ID
                    int id = -1;
                    for (int b = 0; b < aniArray.AnimationDataList.Length; b++)
                    {
                        if (aniArray.AnimationDataList[b].ActionName ==
                            animateWithMOPs[a].AnimationName)
                        {
                            id = b;
                            break;
                        }
                    }

                    if (id == -1)//如果從動作名稱中找不到ID，可能就是與動作無關的MOPGroup播放
                    {
                        m_Animation_MOP[a].MOPAniName = animateWithMOPs[a].AnimationName;
                        m_Animation_MOP[a].animationID = -1;
                    }
                    else
                    {
                        m_Animation_MOP[a].MOPAniName = null;
                        m_Animation_MOP[a].animationID = id;
                    }

                    string[] names = animateWithMOPs[a].MOPLocatorNames;
                    m_Animation_MOP[a].MOPNodes = new LocatorNode[names.Length];
                    for (int b = 0; b < names.Length; b++)
                    {
                        string name = SpecialMeshDataManager.LocatorList.GetLocatorRootNodeName(names[b]);
                        m_Animation_MOP[a].MOPNodes[b] =
                            GetRootNode.SeekFirstNodeAllName(ref name) as LocatorNode;
                    }
                }
            }
        }

        /// <summary>
        /// 設定MOP Group撥放
        /// </summary>
        public void SetMOPGroupPlay(string GroupName)
        {
            ////先全關掉
            //SetAllMuliObjectPlayerStop();

            for (int a = 0; a < m_Animation_MOP.Length; a++)
            {
                if (m_Animation_MOP[a].MOPAniName == GroupName)
                {
                    //根此動作有關的全都要開
                    for (int b = 0; b < m_Animation_MOP[a].MOPNodes.Length; b++)
                        SetMuliObjectPlayerPlay(m_Animation_MOP[a].MOPNodes[b]);
                }
            }
        }


        public void SetMOPGroupStop(string GroupName)
        {
            ////先全關掉
            //SetAllMuliObjectPlayerStop();

            for (int a = 0; a < m_Animation_MOP.Length; a++)
            {
                if (m_Animation_MOP[a].MOPAniName == GroupName)
                {
                    //根此動作有關的全都要開
                    for (int b = 0; b < m_Animation_MOP[a].MOPNodes.Length; b++)
                        SetMuliObjectPlayerStop(m_Animation_MOP[a].MOPNodes[b]);
                }
            }
        }

        /// <summary>
        /// 根據目前撥放的動作，判斷有哪些MOP要啟動。
        /// </summary>
        protected void SetAnimationWithMOPPlay(int animationID)
        {
            if (m_Animation_MOP == null)
                return;

            if (animationID == -1)
                return;

            //先全關掉
            SetAllMuliObjectPlayerStop();

            for (int a = 0; a < m_Animation_MOP.Length; a++)
            {
                if (m_Animation_MOP[a].animationID == animationID)
                {
                    //根此動作有關的全都要開
                    for (int b = 0; b < m_Animation_MOP[a].MOPNodes.Length; b++)
                        SetMuliObjectPlayerPlay(m_Animation_MOP[a].MOPNodes[b]);
                }
                //else
                //{
                //    //根此動作無關的全都要關
                //    for (int b = 0; b < m_Animation_MOP[a].MOPNodes.Length; b++)
                //        SetMuliObjectPlayerStop(m_Animation_MOP[a].MOPNodes[b]);
                //}
            }
        }
        #endregion

        //protected int m_CountAllNodes = 0;
        //public int CountAllNodes { get { return m_CountAllNodes; } }

        //NodeArray m_MeshNodeArray = null;
        //public NodeArray MeshNodeArray
        //{
        //    get
        //    {
        //        if (m_MeshNodeArray == null)
        //        {
        //            if (m_CountAllNodes == 0)
        //                m_MeshNodeArray = new NodeArray(32);
        //            else
        //                m_MeshNodeArray = new NodeArray(m_CountAllNodes);

        //            SceneNode.AddNodeSameType(GetRootNode, m_MeshNodeArray, typeof(MeshNode));
        //        }
        //        return m_MeshNodeArray;
        //    }
        //}


    }
}
