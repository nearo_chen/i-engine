﻿//using System;
//using System.Collections.Generic;
//using System.IO;

//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Audio;
//using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Net;
//using Microsoft.Xna.Framework.Storage;

//using I_XNAUtility;
//using StillDesign.PhysX;
//using HighMapEditor;


//using System.ComponentModel;


//namespace I_XNAComponent
//{
//    public class MyPhyX : System.IDisposable
//    {
//        public enum FilterMask
//        {
//            None,
//            All,
//            TypeAAll,
//            TypeA1,
//            TypeA2,
//            TypeA3,
//            TypeA4,
//            TypeA5,
//            TypeA6,
//            TypeA7,
//            TypeA8,

//            TypeBAll,
//            TypeB1,
//            TypeB2,
//            TypeB3,
//            TypeB4,
//            TypeB5,
//            TypeB6,
//            TypeB7,
//            TypeB8,

//            TypeCAll,
//            TypeC1,
//            TypeC2,
//            TypeC3,
//            TypeC4,
//            TypeC5,
//            TypeC6,
//            TypeC7,
//            TypeC8,

//            TypeDAll,
//            TypeD1,
//            TypeD2,
//            TypeD3,
//            TypeD4,
//            TypeD5,
//            TypeD6,
//            TypeD7,
//            TypeD8,
//            //TypeViewFrustumCulling,

//            Amount,
//        };

//        static public GroupsMask GetGroupMask(FilterMask filter)
//        {
//            switch (filter)
//            {
//                case FilterMask.TypeAAll: return new GroupsMask(0xffffffff, 0, 0, 0);
//                case FilterMask.TypeA1: return new GroupsMask(1 << 0, 0, 0, 0);
//                case FilterMask.TypeA2: return new GroupsMask(1 << 1, 0, 0, 0);
//                case FilterMask.TypeA3: return new GroupsMask(1 << 2, 0, 0, 0);
//                case FilterMask.TypeA4: return new GroupsMask(1 << 3, 0, 0, 0);
//                case FilterMask.TypeA5: return new GroupsMask(1 << 4, 0, 0, 0);
//                case FilterMask.TypeA6: return new GroupsMask(1 << 5, 0, 0, 0);
//                case FilterMask.TypeA7: return new GroupsMask(1 << 6, 0, 0, 0);
//                case FilterMask.TypeA8: return new GroupsMask(1 << 7, 0, 0, 0);

//                case FilterMask.TypeBAll: return new GroupsMask(0, 0xffffffff, 0, 0);
//                case FilterMask.TypeB1: return new GroupsMask(0, 1 << 0, 0, 0);
//                case FilterMask.TypeB2: return new GroupsMask(0, 1 << 1, 0, 0);
//                case FilterMask.TypeB3: return new GroupsMask(0, 1 << 2, 0, 0);
//                case FilterMask.TypeB4: return new GroupsMask(0, 1 << 3, 0, 0);
//                case FilterMask.TypeB5: return new GroupsMask(0, 1 << 4, 0, 0);
//                case FilterMask.TypeB6: return new GroupsMask(0, 1 << 5, 0, 0);
//                case FilterMask.TypeB7: return new GroupsMask(0, 1 << 6, 0, 0);
//                case FilterMask.TypeB8: return new GroupsMask(0, 1 << 7, 0, 0);

//                case FilterMask.TypeCAll: return new GroupsMask(0, 0, 0xffffffff, 0);
//                case FilterMask.TypeC1: return new GroupsMask(0, 0, 1 << 0, 0);
//                case FilterMask.TypeC2: return new GroupsMask(0, 0, 1 << 1, 0);
//                case FilterMask.TypeC3: return new GroupsMask(0, 0, 1 << 2, 0);
//                case FilterMask.TypeC4: return new GroupsMask(0, 0, 1 << 3, 0);
//                case FilterMask.TypeC5: return new GroupsMask(0, 0, 1 << 4, 0);
//                case FilterMask.TypeC6: return new GroupsMask(0, 0, 1 << 5, 0);
//                case FilterMask.TypeC7: return new GroupsMask(0, 0, 1 << 6, 0);
//                case FilterMask.TypeC8: return new GroupsMask(0, 0, 1 << 7, 0);

//                case FilterMask.TypeDAll: return new GroupsMask(0, 0, 0, 0xffffffff);
//                case FilterMask.TypeD1: return new GroupsMask(0, 0, 0, 1 << 0);
//                case FilterMask.TypeD2: return new GroupsMask(0, 0, 0, 1 << 1);
//                case FilterMask.TypeD3: return new GroupsMask(0, 0, 0, 1 << 2);
//                case FilterMask.TypeD4: return new GroupsMask(0, 0, 0, 1 << 3);
//                case FilterMask.TypeD5: return new GroupsMask(0, 0, 0, 1 << 4);
//                case FilterMask.TypeD6: return new GroupsMask(0, 0, 0, 1 << 5);
//                case FilterMask.TypeD7: return new GroupsMask(0, 0, 0, 1 << 6);
//                //D8當frustum culling用
//                //case FilterMask.TypeViewFrustumCulling: return new GroupsMask(0, 0, 0, 1 << 7);
//                case FilterMask.TypeD8: return new GroupsMask(0, 0, 0, 1 << 7);

//                case FilterMask.All: return new GroupsMask(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
//                case FilterMask.None:
//                default: return new GroupsMask(0, 0, 0, 0);
//            }
//        }

  

//        static public FilterMask GetMyFilter(string filter)
//        {
//            for (int a = 0; a < (int)FilterMask.Amount; a++)
//            {
//                FilterMask mask = (FilterMask)a;
//                if(mask.ToString() == filter)
//                    return mask;
//            }
//            return FilterMask.None;
//        }

//        static public FilterMask GetMyFilterMask(GroupsMask groupsMask)
//        {
//            for (int a = 0; a < (int)FilterMask.Amount; a++)
//            {
//                GroupsMask ggg = GetGroupMask((FilterMask)a);
//                if (groupsMask.Bits0 == ggg.Bits0 &&
//                    groupsMask.Bits1 == ggg.Bits1 &&
//                    groupsMask.Bits2 == ggg.Bits2 &&
//                    groupsMask.Bits3 == ggg.Bits3)
//                    return (FilterMask)a;
//            }

//            return FilterMask.None;
//        }

//        private Engine _engine;
//        private Core _core;
//        private Scene _scene;
//        public Scene PhyXScene { get { return _scene; } }

//        public MyPhyX(GraphicsDevice device)
//        {
//            try
//            {
//                _engine = new Engine(device);
//                _engine.Initalize();

//                // For convenience
//                _core = _engine.Core;
//                _scene = _engine.Scene;

//                //設定collision filter
//                _scene.SetFilterOperations((FilterOperation)1, (FilterOperation)1, (FilterOperation)0);//(or, or, and)
//                _scene.SetFilterBoolean(true);
//                GroupsMask zeroMask = new GroupsMask(0, 0, 0, 0);
//                _scene.SetFilterConstant0(zeroMask);
//                _scene.SetFilterConstant1(zeroMask);

//                PhyX_SetEnableDebugLine(false);
//            }
//            catch (Exception e)
//            {
//                //OutputBox.SwitchOnOff = true;
//                OutputBox.ShowMessage(e.Message);
//            }
//        }

//        ~MyPhyX()
//        {
//        }

//        public void Dispose()
//        {
//            //while (_scene.Materials.Count > 0)
//            //    _scene.Materials[0].Dispose();
//            _scene.Dispose();
//            _core.Dispose();
//            _engine.Dispose();
//            _engine = null;
//            _core = null;
//            _scene = null;
//        }

//        public void Update(float ellapseMillisecond)
//        {
//            //Utility.ElapsedTimeRatio
//            _engine.Update(ellapseMillisecond / 1000f);//Utility.ElapsedTimeRatio);//(float)GetTime.ellipseUpdateMillisecond/1000f);//Utility.TargetElapsedTime);/
//        }

//        public void Render(Matrix ViewMatrix, Matrix ProjectionMatrix)
//        {       
//            _engine.Draw(ref ViewMatrix, ref ProjectionMatrix);         
//        }

//        static public Actor PhyXCreatePlaneGround(Scene _scene, Vector3 normal, float d, FilterMask filterMask)
//        {
//            PlaneShapeDescription planeShapeDesc = new PlaneShapeDescription();
//            planeShapeDesc.Normal = normal;
//            planeShapeDesc.Distance = d;

//            planeShapeDesc.GroupsMask = MyPhyX.GetGroupMask(filterMask);
//            ActorDescription actorDesc = new ActorDescription();
//            //actorDesc.Flags = ActorFlag.ContactModification;
//            actorDesc.Shapes.Add(planeShapeDesc);

          

//            Actor act = CreateActor(_scene, ref actorDesc);

//            planeShapeDesc.Dispose();
//            planeShapeDesc = null;
//            return act;
//        }

//        /// <summary>
//        /// 釋放Actor專用
//        /// </summary>        
//        static public void DisposeActor(ref Actor actor)
//        {
//            ReleaseCheck.DisposeCheck(actor);

//            if (actor != null)
//            {
//                while (actor.Shapes.Count > 0)
//                {                    
//                    actor.Shapes[0].Dispose();
//                }
//                actor.UserData = null;
//                actor.Dispose();
//            }
//            actor = null;
//        }

//        static public Actor CreateActor(Scene scene, ref ActorDescription actorDesc)
//        {            
//            Actor actor = scene.CreateActor(actorDesc);
//            ReleaseCheck.AddOtherNew(actor);

//            actorDesc.Dispose();
//            actorDesc = null;
//            return actor;
//        }

//        /// <summary>
//        /// 針對傳入的bounding box以及world matrix建立物理BOX
//        /// </summary>
//        public static Actor PhyXCreateBoxActor(Scene _scene, BoundingBox boundBox, Vector3 scale,
//             float mass, FilterMask filterMask)
//        {
//            Vector3[] size = new Vector3[1];
//            Vector3[] pos = new Vector3[1];

//            size[0] = (boundBox.Max - boundBox.Min);
//            pos[0] = (boundBox.Max + boundBox.Min) * 0.5f;
            
//            size[0] *= scale;
//            size[0] += Vector3.One * 0.0000001f;//預防有一邊小於0，給他加上一個微小的數。
//            if (size[0].X < 0)
//                size[0].X *= -1;
//            if (size[0].Y < 0)
//                size[0].Y *= -1;
//            if (size[0].Z < 0)
//                size[0].Z *= -1;

//            return MyPhyX.PhyXCreateBoxActor(_scene, size, pos,10, filterMask);
//        }

//        public static Actor PhyXCreateBoxActor(Scene _scene, Vector3 [] size, Vector3 [] localPosOffset,
//             float mass, FilterMask filterMask)
//        {
//            BoxShapeDescription[] boxShapeDesc = new BoxShapeDescription[size.Length];
//            for (int a = 0; a < boxShapeDesc.Length; a++)
//            {
//                boxShapeDesc[a] = new BoxShapeDescription(size[a].X, size[a].Y, size[a].Z);
      
//                if (localPosOffset == null)
//                    boxShapeDesc[a].LocalPosition = Vector3.Zero;
//                else
//                    boxShapeDesc[a].LocalPosition = localPosOffset[a];

//                boxShapeDesc[a].GroupsMask = GetGroupMask(filterMask);
//            }

//            BodyDescription bodyDesc = new BodyDescription(mass);

//            ActorDescription actorDesc = new ActorDescription()
//            {
//                Name = "BoxShape",//String.Format("Box {0}", x),
//                BodyDescription = bodyDesc,
   
//                //GlobalPose = Matrix.CreateTranslation(position),
//                //Shapes = { boxShapeDesc },           
//            };
//            for (int a = 0; a < boxShapeDesc.Length; a++)
//                actorDesc.Shapes.Add(boxShapeDesc[a]);

//            Actor actor = CreateActor(_scene, ref actorDesc);

//            bodyDesc.Dispose();
//            bodyDesc = null;
//            for (int a = 0; a < boxShapeDesc.Length; a++)
//            {
//                boxShapeDesc[a].Dispose();
//                boxShapeDesc[a] = null;
//            }
//            //boxShapeDesc.Dispose();
//            boxShapeDesc = null;
//            //actorDesc.Dispose();
//            //actorDesc = null;

//            //if (node != null)
//            //{
//            //    actor.Name = node.NodeName;
//            //    actor.UserData = node;
//            //}

//            return actor;
//        }

//        static public Actor PhyXCreateSphereActor(Scene _scene, 
//            FrameNode node, float radius, Vector3 position, float mass, FilterMask filterMask)
//        {

//            SphereShapeDescription sphereShapeDesc = new SphereShapeDescription(radius);
//            sphereShapeDesc.GroupsMask = GetGroupMask(filterMask);

//            BodyDescription bodyDesc = new BodyDescription(mass);

//            ActorDescription actorDesc = new ActorDescription()
//            {
//                Name = "SphereShape",//String.Format("Box {0}", x),
//                BodyDescription = bodyDesc,
//                GlobalPose = Matrix.CreateTranslation(position),
//                Shapes = { sphereShapeDesc },
//            };

//            Actor actor = CreateActor(_scene, ref actorDesc);


//            bodyDesc.Dispose();
//            bodyDesc = null;
//            sphereShapeDesc.Dispose();
//            sphereShapeDesc = null;
//            //actorDesc.Dispose();
//            //actorDesc = null;

//            if (node != null)
//            {
//                actor.Name = node.NodeName;
//                actor.UserData = node;
//            }
//            return actor;
//        }

//        public Actor PhyXCreateStaticMesh(string name, Vector3[] vertices, uint[] indices, GroupsMask groupMask)
//        {
//            return PhyXCreateStaticMesh(_scene, _core, name, vertices, indices, groupMask);
//        }

//        static public Actor PhyXCreateStaticMesh(Scene _scene, Core _core,
//            string name, Vector3[] vertices, uint[] indices, GroupsMask groupsMask)
//        {
//            TriangleMeshDescription triangleMeshDesc = new TriangleMeshDescription();
//            triangleMeshDesc.TriangleCount = indices.Length / 3;
//            triangleMeshDesc.VertexCount = vertices.Length;

//            triangleMeshDesc.AllocateTriangles<int>(triangleMeshDesc.TriangleCount);
//            triangleMeshDesc.AllocateVertices<Vector3>(triangleMeshDesc.VertexCount);

//            triangleMeshDesc.TriangleStream.SetData(indices);
//            triangleMeshDesc.VerticesStream.SetData(vertices);

//            MemoryStream s = new MemoryStream();

//            Cooking.InitializeCooking();
//            Cooking.CookTriangleMesh(triangleMeshDesc, s);
//            Cooking.CloseCooking();
       
//            s.Position = 0;
//            TriangleMesh triangleMesh = _core.CreateTriangleMesh(s);

//            MaterialDescription materialDesc = new MaterialDescription()
//            {
//                Restitution = 0.5f,
//                StaticFriction = 0.1f,
//                DynamicFriction = 0.1f,
//                //DynamicFrictionV = 0.8f,
//                //StaticFrictionV = 1f,
//                //DirectionOfAnisotropy = new Vector3(0, 0, 1),
//                //Flags = MaterialFlag.Ansiotropic,
//            };
//            Material material = _scene.CreateMaterial(materialDesc);
       
//            TriangleMeshShapeDescription triangleMeshShapeDesc = new TriangleMeshShapeDescription()
//            {
//                TriangleMesh = triangleMesh,
//                Material = material,
//                GroupsMask = groupsMask,
//            };
//            //triangleMeshShapeDesc.Flags |= ShapeFlag.Visualization;

//            ActorDescription actorDesc = new ActorDescription()
//            {
//                //Name = "StaticMesh",
//                Shapes = { triangleMeshShapeDesc }
//            };
       

//            if (name != null)
//                actorDesc.Name = name;
//            else
//                actorDesc.Name = "StaticMesh";

//            Actor actor = CreateActor(_scene, ref actorDesc);


//            triangleMeshDesc.Dispose();
//            triangleMeshDesc = null;
//            s.Dispose();
//            s = null;
//            triangleMeshShapeDesc.Dispose();
//            triangleMeshShapeDesc = null;
//            //actorDesc.Dispose();
//            //actorDesc = null;

//            if(name!=null)
//                actor.Name = Utility.GetOppositeFilePath(name);

//            return actor;
//        }

//        public static Actor PhyXCreateConvexMesh(Scene _scene, Core _core, Vector3[] vertices, Vector3 position, float mass,
//            FilterMask filterMask)
//        {
//            // Allocate memory for the points and triangles
//            var convexMeshDesc = new ConvexMeshDescription()
//            {
//                PointCount = vertices.Length
//            };
//            convexMeshDesc.Flags |= ConvexFlag.ComputeConvex;
//            convexMeshDesc.AllocatePoints<Vector3>(vertices.Length);

//            convexMeshDesc.PointsStream.SetData(vertices);


//            // Cook to memory or to a file
//            MemoryStream stream = new MemoryStream();
//            //FileStream stream = new FileStream( @"Convex Mesh.cooked", FileMode.CreateNew );

//        //    Cooking.InitializeCooking(new ConsoleOutputStream());
//            Cooking.InitializeCooking();
//            Cooking.CookConvexMesh(convexMeshDesc, stream);
//            Cooking.CloseCooking();

//            stream.Position = 0;

//            ConvexMesh convexMesh = _core.CreateConvexMesh(stream);

//            ConvexShapeDescription convexShapeDesc = new ConvexShapeDescription(convexMesh)
//            {
//                GroupsMask = GetGroupMask(filterMask),
//            };

//            ActorDescription actorDesc = new ActorDescription()
//            {
//                Name = "ConvexMesh",
//                BodyDescription = new BodyDescription(mass),
//                GlobalPose = Matrix.CreateTranslation(position)
//            };
//            actorDesc.Shapes.Add(convexShapeDesc);

//            Actor _torusActor = CreateActor(_scene, ref actorDesc);


//            convexMeshDesc.Dispose();
//            convexMeshDesc = null;
//            stream.Dispose();
//            stream = null;
//            convexShapeDesc.Dispose();
//            convexShapeDesc = null;
//            actorDesc.Dispose();
//            actorDesc = null;

//            //if (node != null)
//            //{
//            //    _torusActor.Name = node.NodeName;
//            //    _torusActor.UserData = node;
//            //}

            
//            return _torusActor;
//        }

//        public Actor PhyXCreateHeightField(string hmFilename, Material material, GroupsMask groupMask)
//        {
//            return PhyXCreateHeightField(_scene, _core, hmFilename, material, groupMask);
//        }

//        static private byte? PhyXHeightFieldHoleMaterialIndex;
//        static public Actor PhyXCreateHeightField(Scene scene,Core core, string hmFilename, Material material,
//            GroupsMask groupMask)
//        {
//            HighMapEditor.HighMap heightMap = new HighMapEditor.HighMap();
//            heightMap.LoadHighMap(hmFilename);

//            //MaterialDescription materialDesc = new MaterialDescription();
//            //materialDesc.Restitution = heightMap.PhyXRestitution;
//            //materialDesc.StaticFriction = heightMap.PhyXStaticFriction;
//            //materialDesc.DynamicFriction = heightMap.PhyXDynamicFriction;
//            //byte materialIndex = (byte)_scene.CreateMaterial(materialDesc).Index;
//            //materialDesc.Dispose();
//            //materialDesc = null;

//            //建立一下洞的material
//            if (PhyXHeightFieldHoleMaterialIndex.HasValue == false)
//            {
//                MaterialDescription materialDesc = new MaterialDescription();
//                PhyXHeightFieldHoleMaterialIndex = (byte)scene.CreateMaterial(materialDesc).Index;
//                materialDesc.Dispose();
//                materialDesc = null;
//            }
//            //建立一下洞的material           

//            HeightFieldSample[] samples = new HeightFieldSample[heightMap.BloclSizeX * heightMap.BloclSizeZ];
//            HighMapEditor.HighMap.HighmapData data;
//            for (int r = 0; r < heightMap.BloclSizeX; r++)
//            {
//                for (int c = 0; c < heightMap.BloclSizeZ; c++)
//                {
//                    //HeightFieldSample sample = new HeightFieldSample();
//                    //sample.Height = (short)h;
//                    //sample.MaterialIndex0 = 0;
//                    //sample.MaterialIndex1 = 1;
//                    //sample.TessellationFlag = 0;

//                    heightMap.GetHimapBlockData(r, c, out data);

//                    if (data.bHasData == false)
//                    {
//                        samples[r * heightMap.BloclSizeZ + c].Height = short.MinValue;
//                        samples[r * heightMap.BloclSizeZ + c].TessellationFlag = 0;
//                        samples[r * heightMap.BloclSizeZ + c].MaterialIndex0 = PhyXHeightFieldHoleMaterialIndex.Value;
//                        samples[r * heightMap.BloclSizeZ + c].MaterialIndex1 = PhyXHeightFieldHoleMaterialIndex.Value;
//                    }
//                    else
//                    {
//                        samples[r * heightMap.BloclSizeZ + c].Height = (short)data.m_HighY;
//                        samples[r * heightMap.BloclSizeZ + c].TessellationFlag = 0;
//                        samples[r * heightMap.BloclSizeZ + c].MaterialIndex0 = (byte)material.Index;//材質一定要設定
//                        samples[r * heightMap.BloclSizeZ + c].MaterialIndex1 = (byte)material.Index;
//                    }
//                }
//            }

//            HeightFieldDescription heightFieldDesc = new HeightFieldDescription()
//            {
//                NumberOfRows = heightMap.BloclSizeX,
//                NumberOfColumns = heightMap.BloclSizeZ,
//                Samples = samples
//            };
//            //heightFieldDesc.SetSamples(samples);

//          //  heightFieldDesc.VerticalExtent = -1000;
//        //    heightFieldDesc.Thickness

//            HeightField heightField = core.CreateHeightField(heightFieldDesc);

//            heightFieldDesc.Dispose();
//            heightFieldDesc = null;

//            HeightFieldShapeDescription heightFieldShapeDesc = new HeightFieldShapeDescription()
//            {
//                HeightField = heightField,
//                HoleMaterial = PhyXHeightFieldHoleMaterialIndex.Value,
//                // The max height of our samples is short.MaxValue and we want it to be 1
//                //HeightScale = 1.0f / (float)short.MaxValue,
//                RowScale = heightMap.BlockWidth,
//                ColumnScale = heightMap.BlockHeight,

//                GroupsMask = groupMask,
//            };
       

//            //phyX的height map是以以左上角算(0,0)，我的height map左上角是MapMinX, Z。
//            heightFieldShapeDesc.LocalPosition = new Vector3(heightMap.MapMinX, 0, heightMap.MapMinZ);
//            //new Vector3(
//            //    -0.5f * heightMap.BloclSizeX * 1 * heightFieldShapeDesc.RowScale, 
//            //    0,
//            //    -0.5f * heightMap.BloclSizeZ * 1 * heightFieldShapeDesc.ColumnScale);

//            ActorDescription actorDesc = new ActorDescription()
//            {
//                GlobalPose = Matrix.CreateTranslation(0, 0, 0),
//                Shapes = { heightFieldShapeDesc }
//            };

//            heightFieldShapeDesc.Dispose();
//            heightFieldShapeDesc = null;

//            Actor actor = CreateActor(scene, ref actorDesc);

//            heightMap.Dispose();
//            heightMap = null;

//            actor.Name = Utility.GetOppositeFilePath(hmFilename);
//            return actor;
//        }

//        //public RaycastHit PhyXRaycastClosestShape(ref Vector3 rayPos, ref Vector3 rayDirection,
//        //    ShapesType shapType, float maxDistance, FilterMask mask)
//        //{
//        //    return PhyXRaycastClosestShape(ref rayPos, ref rayDirection, _scene, ShapesType.Static, maxDistance, mask);
//        //}

//        static public RaycastHit PhyXRaycastClosestShape(ref Vector3 rayPos, ref Vector3 rayDirection, Scene scene,
//           ShapesType shapType, float maxDistance, FilterMask mask)
//        {
//            return PhyXRaycastClosestShape(ref rayPos, ref rayDirection, scene,
//            shapType, maxDistance, GetGroupMask(mask) );
//        }

//        static public RaycastHit PhyXRaycastClosestShape(ref Vector3 rayPos, ref Vector3 rayDirection, Scene scene,
//            ShapesType shapType, float maxDistance, GroupsMask GMask)
//        {
//            if (rayDirection == Vector3.Zero)
//                return null;
//            StillDesign.PhysX.Ray ray = new StillDesign.PhysX.Ray(rayPos, rayDirection);          

//            if (float.IsNaN(ray.Direction.X) || float.IsNaN(ray.Direction.Y) || float.IsNaN(ray.Direction.Z))
//                return null;

//            RaycastHit hit = scene.RaycastClosestShape(ray, shapType, 0xffffffff, maxDistance, 0xffffffff/*RaycastBit.All*/, GMask);
//            return hit;
//        }

//        //static List<Actor> RaycastAllShapesActors = new List<Actor>();
//        //static public Actor [] PhyXRaycastAllShapes(ref Vector3 rayPos, ref Vector3 rayDirection,
//        //    Scene scene, ShapesType shapType, float maxDistance, FilterMask mask)
//        //{
//        //    RaycastAllShapesActors.Clear();

//        //    if (rayDirection == Vector3.Zero)
//        //        return null;
//        //    uint hintflag=0;
            
//        //    StillDesign.PhysX.Ray ray = new StillDesign.PhysX.Ray(rayPos, rayDirection);
//        //    GroupsMask GMask = GetGroupMask(mask);
//        //    scene.RaycastAllShapes(ray, MyRaycastReport, shapType, 0xffffffff, maxDistance,
//        //        (uint)RaycastBit.Distance,// | (uint)RaycastBit.Shape,
//        //        GMask);

//        //    Actor [] aaa = RaycastAllShapesActors.ToArray();
//        //    RaycastAllShapesActors.Clear();
//        //    return aaa;
//        //}

//        //static public class MyRaycastReport : UserRaycastReport
//        //{
//        //    static public override bool OnHit(RaycastHit hits)
//        //    {
//        //        Actor hitActor = hits.Shape.Actor;
//        //        RaycastAllShapesActors.Add(hitActor);
//        //        return true;
//        //    }
//        //}

//        //enum FrustumSide { RIGHT , LEFT, BOTTOM, TOP, FRONT, BACK };
//        static Plane[] FrustumPlane = new Plane[6];

//        public static Plane[]  GetFrustumPlanes(ref Matrix VxP)
//        {
//            //BoundingFrustum bbfff = new BoundingFrustum(VxP);
//            //Vector3[] vv = bbfff.GetCorners();
//            //Plane[] planesss = new Plane[6];
//            //planesss[0] = new Plane(vv[2], vv[1], vv[0]);
//            //planesss[1] = new Plane(vv[4], vv[5], vv[6]);
//            //planesss[2] = new Plane(vv[0], vv[4], vv[7]);
//            //planesss[3] = new Plane(vv[5], vv[1], vv[2]);
//            //planesss[4] = new Plane(vv[7], vv[6], vv[2]);
//            //planesss[5] = new Plane(vv[0], vv[1], vv[5]);

//            //return planesss;

//            int RIGHT = 0;// (int)FrustumSide.RIGHT;
//            int LEFT = 1;// (int)FrustumSide.LEFT;
//            int BOTTOM = 2;// (int)FrustumSide.BOTTOM;
//            int TOP = 3;// (int)FrustumSide.TOP;
//            int FRONT = 4;// (int)FrustumSide.FRONT;
//            int BACK = 5;// (int)FrustumSide.BACK;

//            //FrustumPlane[RIGHT] = new Plane(
//            //    VxP.M14 - VxP.M11, VxP.M24 - VxP.M21, VxP.M34 - VxP.M31, VxP.M44 - VxP.M41) ;
//            //FrustumPlane[LEFT] = new Plane(
//            //    VxP.M14 + VxP.M11, VxP.M24 + VxP.M21, VxP.M34 + VxP.M31, VxP.M44 + VxP.M41);
//            //FrustumPlane[BOTTOM] = new Plane(
//            //    VxP.M14 + VxP.M12, VxP.M24 + VxP.M22, VxP.M34 + VxP.M32, VxP.M44 + VxP.M42);
//            //FrustumPlane[TOP] = new Plane(
//            //    VxP.M14 - VxP.M12, VxP.M24 - VxP.M22, VxP.M34 - VxP.M32, VxP.M44 - VxP.M42);
//            //FrustumPlane[FRONT] = new Plane(
//            //    VxP.M14 + VxP.M13, VxP.M24 + VxP.M23, VxP.M34 + VxP.M33, VxP.M44 + VxP.M43);
//            //FrustumPlane[BACK] = new Plane(
//            //    VxP.M14 - VxP.M13, VxP.M24 - VxP.M23, VxP.M34 - VxP.M33, VxP.M44 - VxP.M43);

//            //FrustumPlane[RIGHT].Normalize();
//            //FrustumPlane[LEFT].Normalize();
//            //FrustumPlane[BOTTOM].Normalize();
//            //FrustumPlane[TOP].Normalize();
//            //FrustumPlane[FRONT].Normalize();
//            //FrustumPlane[BACK].Normalize();

//            //FrustumPlane[RIGHT].Normal *= -1f;
//            //FrustumPlane[LEFT].Normal *= -1f;
//            //FrustumPlane[BOTTOM].Normal *= -1f;
//            //FrustumPlane[TOP].Normal *= -1f;
//            //FrustumPlane[FRONT].Normal *= -1f;
//            //FrustumPlane[BACK].Normal *= -1f;

//            //FrustumPlane[RIGHT].D *= -1f;
//            //FrustumPlane[LEFT].D *= -1f;
//            //FrustumPlane[BOTTOM].D *= -1f;
//            //FrustumPlane[TOP].D *= -1f;
//            //FrustumPlane[FRONT].D *= -1f;
//            //FrustumPlane[BACK].D *= -1f;

        
//            //right clipping plane        
//            FrustumPlane[RIGHT].Normal.X = VxP.M14 - VxP.M11;
//            FrustumPlane[RIGHT].Normal.Y = VxP.M24 - VxP.M21;
//            FrustumPlane[RIGHT].Normal.Z = VxP.M34 - VxP.M31;
//            FrustumPlane[RIGHT].D = VxP.M44 - VxP.M41;
//            FrustumPlane[RIGHT].Normal *= -1f;
//            FrustumPlane[RIGHT].D *= -1f;

//            //normalize
//            FrustumPlane[RIGHT].Normalize();

//            //left clipping plane
//            FrustumPlane[LEFT].Normal.X = VxP.M14 + VxP.M11;
//            FrustumPlane[LEFT].Normal.Y = VxP.M24 + VxP.M21;
//            FrustumPlane[LEFT].Normal.Z = VxP.M34 + VxP.M31;
//            FrustumPlane[LEFT].D = VxP.M44 + VxP.M41;
//            FrustumPlane[LEFT].Normal *= -1f;
//            FrustumPlane[LEFT].D *= -1f;

//            //normalize
//            FrustumPlane[LEFT].Normalize();

//            //bottom clipping plane
//            FrustumPlane[BOTTOM].Normal.X = VxP.M14 + VxP.M12;
//            FrustumPlane[BOTTOM].Normal.Y = VxP.M24 + VxP.M22;
//            FrustumPlane[BOTTOM].Normal.Z = VxP.M34 + VxP.M32;
//            FrustumPlane[BOTTOM].D = VxP.M44 + VxP.M42;
//            FrustumPlane[BOTTOM].Normal *= -1f;
//            FrustumPlane[BOTTOM].D *= -1f;

//            //normalize
//            FrustumPlane[BOTTOM].Normalize();

//            //top clipping plane 
//            FrustumPlane[TOP].Normal.X = VxP.M14 - VxP.M12;
//            FrustumPlane[TOP].Normal.Y = VxP.M24 - VxP.M22;
//            FrustumPlane[TOP].Normal.Z = VxP.M34 - VxP.M32;
//            FrustumPlane[TOP].D = VxP.M44 - VxP.M42;
//            FrustumPlane[TOP].Normal *= -1f;
//            FrustumPlane[TOP].D *= -1f;

//            //normalize
//            FrustumPlane[TOP].Normalize();
//            //near clipping plane
//            FrustumPlane[FRONT].Normal.X = VxP.M14 + VxP.M13;
//            FrustumPlane[FRONT].Normal.Y = VxP.M24 + VxP.M23;
//            FrustumPlane[FRONT].Normal.Z = VxP.M34 + VxP.M33;
//            FrustumPlane[FRONT].D = VxP.M44 + VxP.M43;
//            FrustumPlane[FRONT].Normal *= -1f;
//            FrustumPlane[FRONT].D *= -1f;

//            //normalize
//            FrustumPlane[FRONT].Normalize();

//            //far clipping plane
//            FrustumPlane[BACK].Normal.X = VxP.M14 - VxP.M13;
//            FrustumPlane[BACK].Normal.Y = VxP.M24 - VxP.M23;
//            FrustumPlane[BACK].Normal.Z = VxP.M34 - VxP.M33;
//            FrustumPlane[BACK].D = VxP.M44 - VxP.M43;
//            FrustumPlane[BACK].Normal *= -1f;
//            FrustumPlane[BACK].D *= -1f;

//            //normalize
//            FrustumPlane[BACK].Normalize();

//            return FrustumPlane;
//        }

//        static public Shape [] ConsiderViewFrustumCulling(Scene scene, ShapesType shapType, 
//             FilterMask mask, 
//             ref Matrix VxP)
//        {
//            return scene.CullShapes(GetFrustumPlanes(ref VxP), ShapesType.All, 0xffffffff, GetGroupMask(mask));
//        }

//        /// <summary>
//        /// 設定是否啟動PhyX內部產生debug line的參數控制。
//        /// </summary>
//        //public void PhyX_EnableDebugLine(bool bbb)
//        //{
//        //    _engine.SetDrawDebugLine(bbb);
//        //}

//        public void PhyX_GetDebugLineOnce()
//        {
//            _engine.GetDebugLineOnce();
//        }

//        /// <summary>
//        /// 設定是否啟動內部將phyX的debug line繪出的函式，若為false則會跳過該函式。
//        /// </summary>
//        public void PhyX_SetRenderDebugLine(bool bbb)
//        {
//            _engine.SetRenderDebugLine(bbb);
//        }

//        public void PhyX_SetEnableDebugLine(bool bbb)
//        {
//            _engine.SetEnableDebugLine(bbb);
//        }

//        /// <summary>
//        /// 取得內部phyX產生的debug line，當想要自己畫出phyX除錯線條時可以用。
//        /// </summary>
//        public DebugLine[] PhyX_GetDebugLine()
//        {
//            return _engine.GetDebugLine();
//        }

//        public void PhyX_SetGravity(Vector3 gravity)
//        {
//            _engine.SetGravity(ref gravity);
//        }


        
//    }
//}
