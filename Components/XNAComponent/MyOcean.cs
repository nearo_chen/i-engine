﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;

namespace I_XNAComponent
{
    public class MyOcean : IDisposable
    {
        public struct VertexMultitextured
        {
            public Vector3 Position;
            public Vector3 Normal;
            public Vector2 TextureCoordinate;
            public Vector2 SprayTextureCoord;
            public Vector3 Tangent;
            public Vector3 BiNormal;

            public static int SizeInBytes = (3 + 3 + 2 + 2 + 3 + 3) * 4;
            public static VertexElement[] VertexElements = new VertexElement[]
                 {
                     new VertexElement( 0, 0, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Position, 0 ),
                     new VertexElement( 0, sizeof(float) * 3, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Normal, 0 ),
                     new VertexElement( 0, sizeof(float) * 6, VertexElementFormat.Vector2, VertexElementMethod.Default, VertexElementUsage.TextureCoordinate, 0 ),
                     new VertexElement( 0, sizeof(float) * 8, VertexElementFormat.Vector2, VertexElementMethod.Default, VertexElementUsage.TextureCoordinate, 1 ),
                     new VertexElement( 0, sizeof(float) * 10, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Tangent, 0 ),
                     new VertexElement( 0, sizeof(float) * 13, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Binormal, 0 ),
                 };
        }

        DepthMapCreator m_DepthMapCreator = null;
        //CubeMapCreator m_CubeMapCreator = null;
        //bool m_bCreateCubeMap = false;//紀錄cube map是否使用外部傳入的。
        //public bool IfCreateCubeMap { get { return m_bCreateCubeMap; } }

        Vector3 m_SunLightDirection, m_LightDiffuse, m_LightAmbient, m_LightSpecular;
        Vector3 m_FogColor = Vector3.One;
        float m_FogStart = 0;
        float m_FogEnd = 1000;
        public void SetSunLightDirection(Vector3 L, Vector3 LA, Vector3 LD, Vector3 LS) { m_SunLightDirection = L; m_LightDiffuse = LD; m_LightAmbient = LA; m_LightSpecular = LS; }
        public void SetFog(Vector3 fogColor, float start, float end) { m_FogColor = fogColor; m_FogStart = start; m_FogEnd = end; }

        //public CubeMapCreator CubeMapCreator { get { return m_CubeMapCreator; } }
        public DepthMapCreator DepthMapCreator { get { return m_DepthMapCreator; } }
        VertexBuffer m_VB;
        IndexBuffer m_IB;
        VertexDeclaration m_VertexDeclaration;

        public string OceanName;
        int m_Height = 0;
        int m_Width = 0;
        Effect m_OceanEffect;

        NodeArray m_CoastRenderNodeArray = null;//儲存與海深度計算有關的Node

        //Ocean 的參數
        [Serializable]
        public class OceanParamater
        {
            public const int NUMWAVES = 3;

            [Serializable]
            public struct Wave
            {
                public Wave(float freq, float amp, float phase, Vector2 dir)
                {
                    fFreq = freq; fAmp = amp; fPhase = phase; vDir = dir;
                }
                public float fFreq;	// Frequency (2PI / Wavelength)
                public float fAmp;	// Amplitude
                public float fPhase;	// Phase (Speed * 2PI / Wavelength)
                public Vector2 vDir;	// Direction
            };

            public const int NUMSPRAYS = 2;

            [Serializable]
            public struct SPRAY
            {
                public SPRAY(float freq, float speed, float phase)
                {
                    fFreq = freq; fSpeed = speed; fPhase = phase;
                }
                public float fFreq;
                public float fSpeed;
                public float fPhase;
            };

            public string OceanName;
            public int Height = 0;
            public int Width = 0;
            public float TimeScale = 1;

            public float BumpHeight = 0.5f;
            public Vector2 TextureScale = new Vector2(4, 4);
            public Vector2 BumpSpeed = new Vector2(0, .05f);
            public float FresnelBias = .025f;
            public float FresnelPower = 1.0f;
            public float HDRMultiplier = 1.0f;
            public Vector4 DeepWaterColor = Color.Black.ToVector4();
            public Vector4 ShallowWaterColor = Color.SkyBlue.ToVector4();
            public Vector4 ReflectionColor = Color.White.ToVector4();
            public float ReflectionAmount = 0.5f;
            public float WaterAmount = 0f;
            //public float WaveAmplitude = 0.5f;
            //public float WaveFrequency = 0.1f;

            //因為Depth map有他的限制，取樣問題所以這個無效
            public float CoastSprayRangeScale = 0.1f;//海岸浪花產生的範圍
            public float ShallowWaterAlphaDepthScale = 0;//海水深度岸邊會越ALPHA，到完全不ALPHA的深度。
            public Wave[] Waves = new Wave[NUMWAVES];
            public SPRAY[] Sprays = new SPRAY[2];
            public float SprayRatio = 0.3f;

            public OceanParamater()
            {
                SetDefault();
            }

            public void SetDefault()
            {
                OceanName = "MyOcean" + MyMath.GetNext(1000).ToString();
                Height = 100;
                Width = 100;
                TimeScale = 1f;

                BumpHeight = 0.5f;
                TextureScale = new Vector2(4, 4);
                BumpSpeed = new Vector2(0, .05f);
                FresnelBias = .025f;
                FresnelPower = 1.0f;
                HDRMultiplier = 1.0f;
                DeepWaterColor = Color.Black.ToVector4();
                ShallowWaterColor = Color.SkyBlue.ToVector4();
                ReflectionColor = Color.White.ToVector4();
                ReflectionAmount = 0.5f;
                WaterAmount = 0f;
                //WaveAmplitude = 0.5f;
                //WaveFrequency = 0.1f;

                CoastSprayRangeScale = 0.1f;
                ShallowWaterAlphaDepthScale = 10;

                Waves[0] = new Wave(1.0f, 1.00f, 0.50f, Vector2.Normalize(new Vector2(-1.0f, 0.0f)));
                Waves[1] = new Wave(2.0f, 0.50f, 1.30f, Vector2.Normalize(new Vector2(-0.7f, 0.7f)));
                Waves[2] = new Wave(50f, .50f, 0.250f, Vector2.Normalize(new Vector2(0.2f, 0.1f)));

                SprayRatio = 0.3f;
            }

            /// <summary>
            /// Height of water bump texture.
            /// Min 0.0 Max 2.0 Default = .5
            /// </summary>
            //public float BumpHeight
            //{
            //    get { return bumpHeight; }
            //    set { bumpHeight = value; }
            //}
            /// <summary>
            /// Scale of bump texture.
            /// </summary>
            //public Vector2 TextureScale
            //{
            //    get { return textureScale; }
            //    set { textureScale = value; }
            //}
            /// <summary>
            /// Velocity of water flow
            /// </summary>
            //public Vector2 BumpSpeed
            //{
            //    get { return bumpSpeed; }
            //    set { bumpSpeed = value; }
            //}
            /// <summary>
            /// Min 0.0 Max 1.0 Default = .025
            /// </summary>
            //public float FresnelBias
            //{
            //    get { return fresnelBias; }
            //    set { fresnelBias = value; }
            //}
            ///// <summary>
            ///// Min 0.0 Max 10.0 Default = 1.0;
            ///// </summary>
            //public float FresnelPower
            //{
            //    get { return FresnelPower; }
            //    set { fresnelPower = value; }
            //}
            ///// <summary>
            ///// Min = 0.0 Max = 100 Default = 1.0
            ///// </summary>
            //public float HDRMultiplier
            //{
            //    get { return hdrMultiplier; }
            //    set { hdrMultiplier = value; }
            //}
            ///// <summary>
            ///// Color of deep water Default = Black;
            ///// </summary>
            //public Vector4 DeepWaterColor
            //{
            //    get { return deepWaterColor; }
            //    set { deepWaterColor = value; }
            //}
            ///// <summary>
            ///// Color of shallow water Default = SkyBlue
            ///// </summary>
            //public Vector4 ShallowWaterColor
            //{
            //    get { return shallowWaterColor; }
            //    set { shallowWaterColor = value; }
            //}
            ///// <summary>
            ///// Default = White
            ///// </summary>
            //public Vector4 ReflectionColor
            //{
            //    get { return reflectionColor; }
            //    set { reflectionColor = value; }
            //}
            ///// <summary>
            ///// Min = 0.0 Max = 2.0 Default = .5
            ///// </summary>
            //public float ReflectionAmount
            //{
            //    get { return reflectionAmount; }
            //    set { reflectionAmount = value; }
            //}
            ///// <summary>
            ///// Amount of water color to use.
            ///// Min = 0 Max = 2 Default = 0;
            ///// </summary>
            //public float WaterAmount
            //{
            //    get { return waterAmount; }
            //    set { waterAmount = value; }
            //}
            ///// <summary>
            ///// Min = 0.0 Max = 10 Defatult = 0.5
            ///// </summary>
            //public float WaveAmplitude
            //{
            //    get { return waveAmplitude; }
            //    set { waveAmplitude = value; }
            //}
            ///// <summary>
            ///// Min = 0 Max = 1 Default .1
            ///// </summary>
            //public float WaveFrequency
            //{
            //    get { return waveFrequency; }
            //    set { waveFrequency = value; }
            //}
        }

        OceanParamater m_OceanParamater = null;
        public void SetOceanParamater(OceanParamater par)
        {
            m_OceanParamater = par;

            RefreshEffectPar();
        }

        void RefreshEffectPar()
        {
            m_OceanEffect.Parameters["fBumpHeight"].SetValue(m_OceanParamater.BumpHeight);
            m_OceanEffect.Parameters["vTextureScale"].SetValue(m_OceanParamater.TextureScale);
            m_OceanEffect.Parameters["vBumpSpeed"].SetValue(m_OceanParamater.BumpSpeed);
            m_OceanEffect.Parameters["fFresnelBias"].SetValue(m_OceanParamater.FresnelBias);
            m_OceanEffect.Parameters["fFresnelPower"].SetValue(m_OceanParamater.FresnelPower);
            m_OceanEffect.Parameters["fHDRMultiplier"].SetValue(m_OceanParamater.HDRMultiplier);
            m_OceanEffect.Parameters["vDeepColor"].SetValue(m_OceanParamater.DeepWaterColor);
            m_OceanEffect.Parameters["vShallowColor"].SetValue(m_OceanParamater.ShallowWaterColor);
            m_OceanEffect.Parameters["vReflectionColor"].SetValue(m_OceanParamater.ReflectionColor);
            m_OceanEffect.Parameters["fReflectionAmount"].SetValue(m_OceanParamater.ReflectionAmount);
            m_OceanEffect.Parameters["fWaterAmount"].SetValue(m_OceanParamater.WaterAmount);
            //m_OceanEffect.Parameters["fWaveAmp"].SetValue(m_OceanParamater.WaveAmplitude);
            //m_OceanEffect.Parameters["fWaveFreq"].SetValue(m_OceanParamater.WaveFrequency);

            for (int a = 0; a < OceanParamater.NUMWAVES; a++)
            {
                m_OceanEffect.Parameters["Waves"].Elements[a].StructureMembers["fFreq"].SetValue(m_OceanParamater.Waves[a].fFreq);
                m_OceanEffect.Parameters["Waves"].Elements[a].StructureMembers["fAmp"].SetValue(m_OceanParamater.Waves[a].fAmp);
                m_OceanEffect.Parameters["Waves"].Elements[a].StructureMembers["fPhase"].SetValue(m_OceanParamater.Waves[a].fPhase);

                m_OceanParamater.Waves[a].vDir.Normalize();
                m_OceanEffect.Parameters["Waves"].Elements[a].StructureMembers["vDir"].SetValue(m_OceanParamater.Waves[a].vDir);
            }

            for (int a = 0; a < OceanParamater.NUMSPRAYS; a++)
            {
                m_OceanEffect.Parameters["Sprays"].Elements[a].StructureMembers["fFreq"].SetValue(m_OceanParamater.Sprays[a].fFreq);
                m_OceanEffect.Parameters["Sprays"].Elements[a].StructureMembers["fSpeed"].SetValue(m_OceanParamater.Sprays[a].fSpeed);
                m_OceanEffect.Parameters["Sprays"].Elements[a].StructureMembers["fPhase"].SetValue(m_OceanParamater.Sprays[a].fPhase);
            }

            m_OceanEffect.Parameters["SprayRatio"].SetValue(m_OceanParamater.SprayRatio);


        }

        //如果有要建立海岸的海浪的話....要計算深度資訊
        public void SetCoastData(DepthMapCreator.DepthMapData depthMapData, int size,
            FrameNode rootNode, GraphicsDevice device)
        {
            m_DepthMapCreator = DepthMapCreator.Create();

            m_DepthMapCreator.Initialize(size, depthMapData, rootNode, device);

            //搜尋要需要的Node
            m_CoastRenderNodeArray = new NodeArray(1024);
            for (int a = 0; a < depthMapData.RenderNodeNames.Length; a++)
            {
                SceneNode node = SceneNode.SeekFirstMeshNodeAllName(rootNode, depthMapData.RenderNodeNames[a]);
                m_CoastRenderNodeArray.Add(node);
            }
        }

        public NodeArray CoastRenderNodeArray { get { return m_CoastRenderNodeArray; } }

        /// <summary>
        /// 建立Ocean物件，若傳入CubeMapCreator不為null，表示使用外部傳入的環境圖。
        /// </summary>     
        public MyOcean(OceanParamater oceanPar, ContentManager content, GraphicsDevice device)
        {
            //m_OceanParamater.OceanName = oceanPar.OceanName;
            //m_OceanParamater.Height = oceanPar.Height;
            //m_OceanParamater.Width = oceanPar.Width;


            if (Utility.IfContentOutSide)
            {
                m_OceanEffect = content.Load<Effect>(Utility.GetFullPath("XNBData/Content/MyOcean/Water")).Clone(device);
            }
            else
            {
                m_OceanEffect = content.Load<Effect>("MyOcean/Water").Clone(device);
            }


            SetOceanParamater(oceanPar);

            m_Height = oceanPar.Height;
            m_Width = oceanPar.Width;

            //m_CubeMapCreator = cubeMap;
            //m_bCreateCubeMap = false;
            //if (cubeMap == null)
            //{
            //    m_CubeMapCreator = new CubeMapCreator(device, 512);
            //    m_bCreateCubeMap = true;
            //}

            string normalMapname = "waves2";//"MyOcean/Spray"));WaterTrail5
            string coastSprayName = "wave_03";//sprayT"));//wave_01"));
            if (Utility.IfContentOutSide)
            {
                m_OceanEffect.Parameters["tNormalMap"].SetValue(Utility.ContentLoad_Texture2D(content, Utility.GetFullPath("XNBData/Content/MyOcean/" + normalMapname)));//"MyOcean/Spray"));WaterTrail5
                m_OceanEffect.Parameters["CoastSprayTexture"].SetValue(Utility.ContentLoad_Texture2D(content, Utility.GetFullPath("XNBData/Content/MyOcean/" + coastSprayName)));//sprayT"));//wave_01"));
            }
            else
            {
                m_OceanEffect.Parameters["tNormalMap"].SetValue(Utility.ContentLoad_Texture2D(content, "MyOcean/" + normalMapname));
                m_OceanEffect.Parameters["CoastSprayTexture"].SetValue(Utility.ContentLoad_Texture2D(content, "MyOcean/" + coastSprayName));
            }

            VertexMultitextured[] myVertices = new VertexMultitextured[m_Width * m_Height];

            for (int x = 0; x < m_Width; x++)
                for (int y = 0; y < m_Height; y++)
                {
                    //縮放到-0.5和0.5之間
                    float RangeH = (float)m_Height / 2f;
                    float RangeW = (float)m_Width / 2f;
                    float fY = (float)y - RangeH;
                    float fX = (float)x - RangeW;
                    fY /= RangeH;
                    fX /= RangeW;
                    fY /= 2f;
                    fX /= 2f;
                    myVertices[x + y * m_Width].Position = new Vector3(fY, 0, fX);
                    myVertices[x + y * m_Width].Normal = new Vector3(0, -1, 0);
                    myVertices[x + y * m_Width].TextureCoordinate.X = (float)x / 30.0f;
                    myVertices[x + y * m_Width].TextureCoordinate.Y = (float)y / 30.0f;

                    myVertices[x + y * m_Width].SprayTextureCoord.Y = (float)x % 2f;
                    myVertices[x + y * m_Width].SprayTextureCoord.X = (float)y % 2f;
                }

            // Calc Tangent and Bi Normals.
            for (int x = 0; x < m_Width; x++)
                for (int y = 0; y < m_Width; y++)
                {
                    // Tangent Data.
                    if (x != 0 && x < m_Width - 1)
                        myVertices[x + y * m_Width].Tangent = myVertices[x - 1 + y * m_Width].Position - myVertices[x + 1 + y * m_Width].Position;
                    else
                        if (x == 0)
                            myVertices[x + y * m_Width].Tangent = myVertices[x + y * m_Width].Position - myVertices[x + 1 + y * m_Width].Position;
                        else
                            myVertices[x + y * m_Width].Tangent = myVertices[x - 1 + y * m_Width].Position - myVertices[x + y * m_Width].Position;

                    // Bi Normal Data.
                    if (y != 0 && y < m_Width - 1)
                        myVertices[x + y * m_Width].BiNormal = myVertices[x + (y - 1) * m_Width].Position - myVertices[x + (y + 1) * m_Width].Position;
                    else
                        if (y == 0)
                            myVertices[x + y * m_Width].BiNormal = myVertices[x + y * m_Width].Position - myVertices[x + (y + 1) * m_Width].Position;
                        else
                            myVertices[x + y * m_Width].BiNormal = myVertices[x + (y - 1) * m_Width].Position - myVertices[x + y * m_Width].Position;
                }

            m_VB = new VertexBuffer(device, VertexMultitextured.SizeInBytes * m_Width * m_Height, BufferUsage.WriteOnly);
            m_VB.SetData(myVertices);
            myVertices = null;

            short[] terrainIndices = new short[(m_Width - 1) * (m_Height - 1) * 6];
            for (short x = 0; x < m_Width - 1; x++)
            {
                for (short y = 0; y < m_Height - 1; y++)
                {
                    terrainIndices[(x + y * (m_Width - 1)) * 6] = (short)((x + 1) + (y + 1) * m_Width);
                    terrainIndices[(x + y * (m_Width - 1)) * 6 + 1] = (short)((x + 1) + y * m_Width);
                    terrainIndices[(x + y * (m_Width - 1)) * 6 + 2] = (short)(x + y * m_Width);

                    terrainIndices[(x + y * (m_Width - 1)) * 6 + 3] = (short)((x + 1) + (y + 1) * m_Width);
                    terrainIndices[(x + y * (m_Width - 1)) * 6 + 4] = (short)(x + y * m_Width);
                    terrainIndices[(x + y * (m_Width - 1)) * 6 + 5] = (short)(x + (y + 1) * m_Width);
                }
            }

            m_IB = new IndexBuffer(device, typeof(short), (m_Width - 1) * (m_Height - 1) * 6, BufferUsage.WriteOnly);
            m_IB.SetData(terrainIndices);
            terrainIndices = null;

            m_VertexDeclaration = new VertexDeclaration(device, VertexMultitextured.VertexElements);
        }

        public void Dispose()
        {
            //if(m_DefaultTextureCube!=null)
            //m_DefaultTextureCube.Dispose();
            m_DefaultTextureCube = null;

            if (m_DepthMapCreator != null)
                m_DepthMapCreator.Dispose();
            m_DepthMapCreator = null;

            if (m_CoastRenderNodeArray != null)
                m_CoastRenderNodeArray.Dispose();
            m_CoastRenderNodeArray = null;

            //if (m_bCreateCubeMap)
            //    m_CubeMapCreator.Dispose();
            //m_CubeMapCreator = null;

            m_OceanEffect.Dispose();
            m_OceanEffect = null;

            m_VertexDeclaration.Dispose();
            m_VertexDeclaration = null;
            m_VB.Dispose();
            m_VB = null;
            m_IB.Dispose();
            m_IB = null;
        }

        TextureCube m_DefaultTextureCube= null;
   
        float m_CountEllapseTime = 0;
        public void Draw(//GameTime gameTime, 
            float ellapseMillisecondSeconds,
            ref Matrix world, ref Matrix view, ref Matrix invView, ref Matrix proj,
            GraphicsDevice device, ContentManager content, CubeMapCreator cubeMap)
        {
            m_CountEllapseTime += ellapseMillisecondSeconds;

            if (cubeMap != null)
                m_OceanEffect.Parameters["tEnvMap"].SetValue(cubeMap.TextureCube);
            else
            {
                Utility.GetDefaultTextureCube(ref m_DefaultTextureCube, content);
                m_OceanEffect.Parameters["tEnvMap"].SetValue(m_DefaultTextureCube);
            }

            if (m_DepthMapCreator != null)
            {
                m_OceanEffect.Parameters["tCoastDepthMap"].SetValue(m_DepthMapCreator.GetDepthMap);
                m_OceanEffect.Parameters["CoastDepthMapFrustum"].SetValue(m_DepthMapCreator.DepthMapFrustum);
                m_OceanEffect.Parameters["bCoastDepthMap"].SetValue(true);
                m_OceanEffect.Parameters["CoastSprayRangeScale"].SetValue(m_OceanParamater.CoastSprayRangeScale);
                m_OceanEffect.Parameters["ShallowWaterAlphaDepthScale"].SetValue(m_OceanParamater.ShallowWaterAlphaDepthScale);
            }
            else
                m_OceanEffect.Parameters["bCoastDepthMap"].SetValue(false);

            //Matrix World = Matrix.CreateScale(myScale) *
            //                Matrix.CreateFromQuaternion(myRotation) *
            //                Matrix.CreateTranslation(myPosition);

            Matrix WVP = world * view * proj;
            Matrix WV = world * view;
            //        Matrix viewI = Matrix.Invert(Camera.myView);
            //m_OceanEffect.Parameters["fTime"].SetValue((float)gameTime.TotalRealTime.TotalSeconds * m_OceanParamater.TimeScale);
            m_OceanEffect.Parameters["fTime"].SetValue(m_CountEllapseTime * m_OceanParamater.TimeScale);
            m_OceanEffect.Parameters["matWorldViewProj"].SetValue(WVP);
            m_OceanEffect.Parameters["matWorld"].SetValue(world);
            m_OceanEffect.Parameters["matWorldView"].SetValue(WV);
            m_OceanEffect.Parameters["matViewI"].SetValue(invView);
            m_OceanEffect.Parameters["LightDirection"].SetValue(m_SunLightDirection);
            m_OceanEffect.Parameters["I_a"].SetValue(m_LightAmbient);
            m_OceanEffect.Parameters["I_d"].SetValue(m_LightDiffuse);
            m_OceanEffect.Parameters["I_s"].SetValue(m_LightSpecular);
            m_OceanEffect.Parameters["FogStart"].SetValue(m_FogStart);
            m_OceanEffect.Parameters["FogEnd"].SetValue(m_FogEnd);
            m_OceanEffect.Parameters["FogColor"].SetValue(m_FogColor);

            device.VertexDeclaration = m_VertexDeclaration;
            device.Vertices[0].SetSource(m_VB, 0, VertexMultitextured.SizeInBytes);
            device.Indices = m_IB;

            //Game.GraphicsDevice.RenderState.FillMode = FillMode.WireFrame;       

            m_OceanEffect.Begin(SaveStateMode.SaveState);
            for (int p = 0; p < m_OceanEffect.CurrentTechnique.Passes.Count; p++)
            {
                m_OceanEffect.CurrentTechnique.Passes[p].Begin();

                device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, m_Width * m_Height, 0, (m_Width - 1) * (m_Height - 1) * 2);

                m_OceanEffect.CurrentTechnique.Passes[p].End();
            }
            m_OceanEffect.End();

            //Game.GraphicsDevice.RenderState.FillMode = FillMode.Solid;

            device.VertexDeclaration = null;
            device.Vertices[0].SetSource(null, 0, 0);
            device.Indices = null;
        }
    }
}
