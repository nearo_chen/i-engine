﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;
using MeshDataDefine;


namespace I_XNAComponent
{
  

    //public class KeyFrameMesh2 : IDisposable
    //{
    //    KeyFrameMesh m_KFMesh;
    //    SceneMeshLoader m_ModelLoader;

    //    LocatorNode m_RootNode;

    //    //擴充功能，//也許會有每個BONE對應的NODE，用這可以將它存起來。
    //    FrameNode[] m_RefNodes;
    //    int treeCount;

    //    public KeyFrameMesh2()
    //    {
    //    }

    //    public static KeyFrameMesh2 Create()
    //    {
    //        return ReleaseCheck.CreateNew(typeof(KeyFrameMesh2)) as KeyFrameMesh2;
    //    }

    //    ~KeyFrameMesh2()
    //    {
    //        ReleaseCheck.DisposeCheckCount(this);
    //    }

    //    public void Dispose()
    //    {
    //        ReleaseCheck.DisposeCheck(this);

    //        m_KFMesh.Dispose();
    //        m_KFMesh = null;

    //        m_ModelLoader.Dispose();
    //        m_ModelLoader = null;

    //        //   BasicNode root = m_RootNode as BasicNode;
    //        //   BasicNode.DisposeAllTree(ref root);

    //        //擴充功能，//也許會有每個BONE對應的NODE，用這可以將它存起來。
    //        for (int i = 0; i < m_RefNodes.Length; i++)
    //            m_RefNodes[i] = null;
    //        m_RefNodes = null;
    //    }

    //    //public SceneMeshLoader GetModelLoader()
    //    //{
    //    //    return m_ModelLoader;
    //    //}

    //    public Matrix GetBoundingFrustumMatrix()
    //    {
    //        return m_ModelLoader.BoundingFrustumMatrix;
    //    }

    //    public BoundingSphere GetBoundingSphere()
    //    {
    //        return m_ModelLoader.BoundingSphere;
    //    }

    //    public BoundingBox GetBoundingBox()
    //    {
    //        return m_ModelLoader.BoundingBox;
    //    }

    //    public LocatorNode GetRootNode { get { return m_RootNode; } }

    //    void SetReferenceNodes(FrameNode rootNode)
    //    {
    //        treeCount = 0;
    //        m_RefNodes = new FrameNode[m_ModelLoader.CountAllNodes];
    //        SetReferenceNode(rootNode);
    //    }

    //    void SetReferenceNode(FrameNode parentNode)
    //    {
    //        m_RefNodes[treeCount] = parentNode;
    //        treeCount++;

    //        FrameNode node = (FrameNode)parentNode.FirstChild;
    //        while (node != null)
    //        {
    //            SetReferenceNode(node);
    //            node = (FrameNode)node.Sibling;
    //        }
    //    }

    //    //public void UpdateReferenceNode(int ellapsedMilliseond)
    //    //{
    //    //    m_KFMesh.GetActiveAnimation.AnimatedWithTime(ellapsedMilliseond);

    //    //    for (int i = 0; i < m_RefNodes.Length; i++)
    //    //        m_RefNodes[i].LocalMatrix = m_KFMesh.GetActiveAnimation.AnimatingBones[i];

    //    // //   m_ModelLoader.UpdateSceneElement(GetTime.EllapsedMilliseonds());
    //    //}

    //    /// <summary>
    //    /// 根據時間做key frame判斷，還有更新reference node的matrix。
    //    /// </summary>
    //    public void UpdateKeyFrameTransform(int ellapsedMilliseond)
    //    {
    //        //m_KFMesh.GetActiveAnimation.AnimatedWithTime(ellapsedMilliseond);
    //        m_KFMesh.UpdateAnimateKeyFrameTransform(ellapsedMilliseond);

    //        //m_RefNodes[0].LocalMatrix = Matrix.Multiply( m_KFMesh.GetBoneAnimateLocalMatrix(0), worldTransform);
    //        //m_RefNodes[0].LocalMatrix = m_KFMesh.GetBoneAnimateLocalMatrix(0);
    //        //for (int i = 1; i < m_RefNodes.Length; i++)
    //        //{
    //        //    m_RefNodes[i].LocalMatrix = m_KFMesh.GetBoneAnimateLocalMatrix(i);
    //        //}
    //        //m_ModelLoader.UpdateSceneElement(GetTime.EllapsedMilliseonds());

    //        for (int i = 0; i < m_RefNodes.Length; i++)
    //            m_RefNodes[i].LocalMatrix = m_KFMesh.GetBoneAnimateLocalMatrix(i);
    //    }



    //    public AnimationTableDataArray LoadContent(string modelname, ContentManager content, GraphicsDevice device)
    //    {
    //        Model model = Utility.ContentLoad_Model(content, modelname);

    //        //讀取SceneMeshLoader
    //        if (SceneMeshLoader.LoadSceneMesh(model, out m_ModelLoader, content, device))
    //        {
    //            SetReferenceNodes((FrameNode)m_ModelLoader.GetRootNode.FirstChild);

    //            m_RootNode = LocatorNode.Create();
    //            m_RootNode.AttachChild(m_ModelLoader.GetRootNode);
    //        }

    //        //讀取key frame mesh
    //        m_KFMesh = new KeyFrameMesh();
    //        AnimationTableDataArray aniTableArray = m_KFMesh.LoadContent(model, content, modelname + "_ani.xml");
    //        return aniTableArray;
    //    }

    //    //public void SetAllNodesShadowMap(ShadowMapLight BigShadowMapLight, ShadowMapLight SmallShadowMapLight)
    //    //{
    //    //    m_ModelLoader.SetAllNodesShadowMap(BigShadowMapLight, SmallShadowMapLight);
    //    //}

    //    public bool IfAnimationPlay(int animation)
    //    {
    //        return m_KFMesh.IfAnimationPlay(animation);
    //    }

    //    public void SetAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent)
    //    {
    //        m_KFMesh.SetAnimationPlay(iAnimationID, bLoop, speed, startPercent);

    //        //foreach (DynamicTexture dt in m_CarLoader.DynamicTextureList)
    //        //{
    //        //    dt.Play(bLoop, 1);
    //        //}

    //        //foreach (KeyFrameMesh2 keyframeMesh in m_CarLoader.KeyFrameMeshList)
    //        //{
    //        //    keyframeMesh.SetAnimationPlay(iAnimationID, bLoop, speed, startPercent);
    //        //}
    //    }

    //    public void AllAnimationStop()
    //    {
    //        m_KFMesh.AllAnimationStop();
    //    }

    //    public void SetAnimationStop(int iAnimationID)
    //    {
    //        m_KFMesh.SetAnimationStop(iAnimationID);
    //    }

    //    public void AnimatedWithRatio(int iAnimationID, float ratio)
    //    {
    //        m_KFMesh.AnimatedWithRatio(iAnimationID, ratio);
    //    }

    //    public void SetAnimationPlay_BlendSwitch(int animation, bool bLoop, float speed, float startPercent, int switchTime)
    //    {
    //        m_KFMesh.SetAnimationPlay_BlendSwitch(animation, bLoop, speed, startPercent, switchTime);
    //    }

    //    public void SetBlendAnimationPlay(int animation, bool bLoop, float speed, float startPercent)
    //    {
    //        m_KFMesh.SetBlendAnimationPlay(animation, bLoop, speed, startPercent);
    //    }

    //    public void SetBlendAnimationRatio(float ratio)
    //    {
    //        m_KFMesh.SetBlendAnimationRatio(ratio);
    //    }

    //    //public int ForceRenderBefore(EffectRenderManager effectRenderManager,
    //    //    ref uint triangle, GraphicsDevice device, ref Matrix View, ref Matrix Projection, ref Matrix InverseView)
    //    //{
    //    //    //return m_ModelLoader.RenderSceneBefore(effectRenderManager,
    //    //    //    ref triangle, true, device, ref View, ref Projection, ref InverseView);
    //    //}

    //    //public int RenderBefore(EffectRenderManager effectRenderManager,
    //    //    ref uint triangle, GraphicsDevice device, ref Matrix View, ref Matrix Projection, ref Matrix InverseView)
    //    //{
    //    //    //return m_ModelLoader.RenderSceneBefore(effectRenderManager,
    //    //    //    ref triangle,device, ref View, ref Projection, ref InverseView);
    //    //}

    //    //public int RenderAfter(EffectRenderManager effectRenderManager,
    //    //    ref uint triangle, GraphicsDevice device, ref Matrix View, ref Matrix Projection, ref Matrix InverseView)
    //    //{
    //    //    //return m_ModelLoader.RenderSceneAfter(effectRenderManager,
    //    //    //ref triangle, device, ref View, ref Projection, ref InverseView);
    //    //}

    //    /// <summary>
    //    /// 取得該骨頭時間上所有matrix
    //    /// </summary>      
    //    public Matrix[] GetBoneAllKeyframes(string meshName)
    //    {
    //        int boneID = -1;
    //        for (int a = 0; a < m_RefNodes.Length; a++)
    //        {
    //            if (m_RefNodes[a].NodeName.Contains(meshName))
    //            {
    //                boneID = a;
    //                break;
    //            }
    //        }

    //        return m_KFMesh.GetBoneAllAnimationMatrix(boneID);
    //    }

    //    /// <summary>
    //    /// 取得目前動作播放keyframe block相對於起始撥放block的播放格數，
    //    /// 也就是取得該骨頭，目前撥放到第幾格。
    //    /// </summary>
    //    public int GetNowAnimationPlayKeyframeBlockID(string meshName)
    //    {
    //        int boneID = -1;
    //        for (int a = 0; a < m_RefNodes.Length; a++)
    //        {
    //            if (m_RefNodes[a].NodeName.Contains(meshName))
    //            {
    //                boneID = a;
    //                break;
    //            }
    //        }

    //        return m_KFMesh.GetNowAnimationPlayKeyframeBlockID(boneID);
    //    }

    //    public float GetNowAnimationPlayPercent()
    //    {
    //        return m_KFMesh.GetPlayPercent();
    //    }
    //}
}
