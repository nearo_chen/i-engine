﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using I_XNAUtility;

namespace I_XNAComponent
{
    public class MyGodsRay : IDisposable
    {
        Effect m_ScatterEffect;
        RenderTarget2D m_BlackRT;
        Texture2D m_BlackTexture = null;
        public RenderTarget2D GetGodsRayRT() { return m_BlackRT; }
        public Texture2D GetGodsRayMap()
        {
            if (m_BlackTexture == null) { m_BlackTexture = m_BlackRT.GetTexture(); }
            return m_BlackTexture; 
        }

        public void Dispose()
        {
            if (m_BlackRT != null)
                m_BlackRT.Dispose();
            m_BlackRT = null;

            if (m_ScatterEffect != null)
                m_ScatterEffect.Dispose();
            m_ScatterEffect = null;

            if(m_BlackTexture!=null)
                m_BlackTexture.Dispose();
            m_BlackTexture = null;
        }

        public MyGodsRay(ContentManager Content, GraphicsDevice device, int blackTexW, int blackTexH)
        {
            if (Utility.IfContentOutSide)
            {
                m_ScatterEffect = Content.Load<Effect>(Utility.GetFullPath("XNBData/Content/GodRay/Scatter")).Clone(device);
            }
            else
            {
                m_ScatterEffect = Content.Load<Effect>("GodRay/Scatter").Clone(device);
            }

            m_BlackRT = new RenderTarget2D(device, blackTexW, blackTexH, 1, SurfaceFormat.Color,
                Utility.MultiSampleType, Utility.MultiSampleQuality, RenderTargetUsage.PreserveContents);//, Utility.RenderTargetUsage);            
        }

        public void Draw(ref Matrix View, ref Matrix invView, ref Matrix Projection, SpriteBatch spriteBatch, Texture2D sceneTexture,
            GodsRayLightNode[] GodsRayLights)
        {
            m_ScatterEffect.Parameters["View"].SetValue(View);
            m_ScatterEffect.Parameters["sceneTexture"].SetValue(sceneTexture);
            m_ScatterEffect.Parameters["viewPosition"].SetValue( invView.Translation);
            m_ScatterEffect.Parameters["viewDirection"].SetValue(Vector3.Normalize( invView.Forward));

            m_ScatterEffect.CurrentTechnique = m_ScatterEffect.Techniques["Scatter"];
            m_ScatterEffect.Begin();
            spriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
            EffectPass pass = m_ScatterEffect.CurrentTechnique.Passes[0];
            pass.Begin();

            for (int a = 0; a < GodsRayLights.Length; a++)
            {
                Vector3 LightPosition = GodsRayLights[a].LocalPosition;
                float decay = GodsRayLights[a].Decay;
                float density = GodsRayLights[a].Density;
                float exposition = GodsRayLights[a].Exposition;
                float weight = GodsRayLights[a].Weight;
                float samples = GodsRayLights[a].samples;

                //利用光線處理ray
                m_ScatterEffect.Parameters["Decay"].SetValue(decay);
                m_ScatterEffect.Parameters["Density"].SetValue(density);
                m_ScatterEffect.Parameters["Exposition"].SetValue(exposition);
                m_ScatterEffect.Parameters["Weight"].SetValue(weight);
                m_ScatterEffect.Parameters["numSamples"].SetValue(samples);
                m_ScatterEffect.Parameters["LightPosition"].SetValue(LightPosition);
                m_ScatterEffect.Parameters["WorldViewProjection"].SetValue(
                Matrix.CreateTranslation(LightPosition) *
                View * Projection);

                spriteBatch.Draw(m_BlackRT.GetTexture(), Vector2.Zero, Color.White);
                //spriteBatch.Draw(m_BlackTex, new Rectangle(0, 0, fullScreenTex.Width, fullScreenTex.Height), Color.White);
            }

            pass.End();
            spriteBatch.End();
            m_ScatterEffect.End();
        }
    }


    public class GodsRayLightNode : LocatorNode
    {
        public float Density;
        public float Weight;
        public float Decay;
        public float Exposition;
        public int samples;

        public GodsRayLightNode()
        {
            Density = 0.286f;
            Weight = 0.832f;
            Decay = 1.068102f;
            Exposition = 0.0104f;
            samples = 32;
        }

        protected override void v_Dispose()
        {
            base.v_Dispose();
        }
    }
}
