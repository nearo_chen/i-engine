﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;
using MeshDataDefine;
using I_Component.PhysicsType;

namespace I_XNAComponent
{
    public abstract class ViewCullingMeshNode : FrameNode
    {
        Matrix m_BoundingFrustumMatrix;
        protected BoundingSphere m_BoundingSphere;
        protected BoundingBox m_BoundingBox;
        protected bool m_bBoundingSphere = false;
        protected bool m_bBoundingBox = false;
        protected bool m_bBoundingFrustum = false;

        public bool IfBoundingSphere
        {
            get { return m_bBoundingSphere; }
        }
        public bool IfBoundingBox
        {
            get { return m_bBoundingBox; }
        }
        public bool IfBuildFrustum
        {
            get { return m_bBoundingFrustum; }
        }
        public BoundingSphere BoundingSphere
        {
            get { return m_BoundingSphere; }
            set { m_BoundingSphere = value; }
        }

        public BoundingBox BoundingBox
        {
            get { return m_BoundingBox; }
            set
            {
                m_BoundingBox = value;
            }
        }
        public Matrix BoundingFrustumMatrix
        {
            get { return Matrix.Invert(WorldMatrix) * m_BoundingFrustumMatrix; }
            set { m_BoundingFrustumMatrix = value; }
        }

        void SetBoundingData(BoundingBox box, BoundingSphere sphere)
        {
            m_BoundingBox = box;
            m_BoundingSphere = sphere;
            m_bBoundingBox = true;
            m_bBoundingSphere = true;

            Utility.BoundBoxToBoundingFrustum(ref m_BoundingBox, out m_BoundingFrustumMatrix);
            m_bBoundingFrustum = true;
        }

        internal void AnalyzeMeshBoundingData(Dictionary<string, object> meshTag, out int indexBufferSize)
        {

            if (meshTag != null &&
                meshTag.ContainsKey(MeshKeys.BoundingBoxKey) &&
                meshTag.ContainsKey(MeshKeys.BoundingSphereKey) &&
                meshTag.ContainsKey(MeshKeys.IndexBufferSize))
            {
                SetBoundingData(
                    (BoundingBox)meshTag[MeshKeys.BoundingBoxKey],
                    (BoundingSphere)meshTag[MeshKeys.BoundingSphereKey]
                    );

                indexBufferSize = (int)meshTag[MeshKeys.IndexBufferSize];
            }
            else
            {
                indexBufferSize = 0;

                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage("Do not has Bounding Data.....");
            }
        }


        //protected bool m_bStaticObject;
        bool m_bCulled = true;//做視野內裁切
        System.IDisposable m_ViewCullingObject;//作視野裁切的物件，要再node被幹掉時也執行Dispose，從PhyX中移除。

        /// <summary>
        /// 設定做culling的資訊，傳入此Node的ViewCullingObject，還有他是否為靜態的物件，來做為判斷是否要不斷更新matrix。
        /// </summary>
        public void SetCullingData(System.IDisposable viewCullingObject)//bool bStaticObject)//, System.IDisposable viewCullingObject)
        {
            //m_bStaticObject = bStaticObject;
            m_ViewCullingObject = viewCullingObject;
        }

        public ViewCullingMeshNode()
        {
            m_bCulled = true;
            //m_bStaticObject = true;
        }

        public bool BeCulled
        {
            get { return m_bCulled; }
            set { m_bCulled = value; }
        }

        override public void Dispose()
        {
            if (m_ViewCullingObject != null)
            {
                m_ViewCullingObject.Dispose();
                m_ViewCullingObject = null;
            }

            base.Dispose();
        }

        public object ViewCullingObject
        {
            get { return m_ViewCullingObject; }
        }

        protected static void UpdateViewCullingBoxPose(ViewCullingMeshNode frameNode)
        {
            //if (frameNode.m_bStaticObject)//靜態物件不用更新
            //    return;

            if (frameNode.NodeName == "pPlane3")
            {
                int a = 0;
                a++;
            }

            RTMatrix rtmat;       
            rtmat.m_Quaternion = Quaternion.Identity;
            rtmat.m_Translation = Vector3.Zero;

            ViewCullingMeshNode cullingNode = frameNode as ViewCullingMeshNode;
            if (cullingNode.ViewCullingObject != null)
            {
                if (frameNode.GetType() == typeof(MeshNode))
                {
                    ViewFrustumCullingActorArray viewFrustumCullingActorArray = cullingNode.ViewCullingObject as ViewFrustumCullingActorArray;

                    rtmat.SetData(frameNode.WorldMatrix);
                    //viewFrustumCullingActorArray.SetMatrix(0, ref rtmat.m_Quaternion, ref rtmat.m_Translation);
                    Matrix nmmm = frameNode.WorldMatrix;
                    viewFrustumCullingActorArray.SetMatrix(0, ref nmmm);
                }
                else if (frameNode.GetType() == typeof(InstanceSimpleMeshNode))
                {
                    ViewFrustumCullingActorArray viewFrustumCullingActorArray = cullingNode.ViewCullingObject as ViewFrustumCullingActorArray;
                    InstanceSimpleMeshNode instanceNode = frameNode as InstanceSimpleMeshNode;
                    for (int a = 0; a < instanceNode.ReferenceNodeArray.Length; a++)
                    {
                        rtmat.SetData(instanceNode.ReferenceNodeArray[a].WorldMatrix);
                        viewFrustumCullingActorArray.SetMatrix(a, ref rtmat.m_Quaternion, ref rtmat.m_Translation);
                    }
                }
                else if (frameNode.GetType() == typeof(SkinningMeshNode))
                {
                    ViewFrustumCullingActorArray viewFrustumCullingActorArray = cullingNode.ViewCullingObject as ViewFrustumCullingActorArray;
                    SkinningMeshNode skinningNode = frameNode as SkinningMeshNode;

                    Matrix mat;
                    Vector3 pos, dir;
                    for (int a = 0; a < skinningNode.m_CollBoneNodes.Length; a++)
                    {
                        pos = ((FrameNode)skinningNode.m_CollBoneNodes[a].Parent).WorldPosition;
                        dir = skinningNode.m_CollBoneNodes[a].WorldPosition - pos;
                        dir.Normalize();
                        MyMath.MatrixSetDirection(pos, dir, Vector3.Up, out mat);

                        rtmat.SetData(mat);
                        viewFrustumCullingActorArray.SetMatrix(a, ref rtmat.m_Quaternion, ref rtmat.m_Translation);
                    }
                }
            }
        }


        /// <summary>
        /// 檢測boundbox是否有frustum內，若是不在frustum內，BeCulled會得到false。
        /// 回傳BeCulled
        /// </summary>
        public bool ViewFrustumCullingBoundingBox(ref Matrix ViewProjection)
        {
            m_bCulled = true;
            Matrix worldMat = WorldMatrix;

            if (m_bBoundingBox)
            {
                if (Collision.FrustumContainBox(ref ViewProjection, BoundingBox, ref worldMat) == true)
                {
                    m_bCulled = false;
                }
            }
            return m_bCulled;
        }

        //public static void SetLocalMatrixDone(FrameNode frameNode)
        //{
        //    ViewCullingMeshNode cullingNode = frameNode as ViewCullingMeshNode;
        //    ViewFrustumCullingActorArray viewFrustumCullingActorArray = cullingNode.ViewCullingObject as ViewFrustumCullingActorArray;
        //    viewFrustumCullingActorArray.SetMatrix(0, frameNode.WorldmatrixRT);
        //}
    }


    //public abstract class ViewCullingInstanceLocatorNode : FrameNode
    //{
    //    Actor m_BoxActor = null;
    //    override public void Dispose()
    //    {
    //        m_BoxActor = null;
    //        base.Dispose();
    //    }

    //    public static void SetLocalMatrixDone(FrameNode frameNode)
    //    {
    //        ViewCullingInstanceLocatorNode cullingNode = frameNode as ViewCullingInstanceLocatorNode;
    //        ViewFrustumCullingActorArray viewFrustumCullingActorArray = cullingNode.ViewCullingObject as ViewFrustumCullingActorArray;
    //        viewFrustumCullingActorArray.SetMatrix(0, frameNode.WorldmatrixRT);
    //    }
    //}

    /// <summary>
    /// 將Instance建來做view frustum culling的Box actor存起來的陣列class，
    /// 用於在InstanceMeshNode的dispose時，一併釋放actor。
    /// </summary>
    public class ViewFrustumCullingActorArray : System.IDisposable
    {
        MyPhysics m_myPhysics = null;
        public int Size = 0;
        int count = 0;
        object[] ViewCullingPhysicsObjectArray = null;

        public ViewFrustumCullingActorArray(MyPhysics myPhysics, int size)
        {
            m_myPhysics = myPhysics;
            Size = size;
            count = 0;
            ViewCullingPhysicsObjectArray = new object[size];
        }

        public void Dispose()
        {
            for (int a = 0; a < ViewCullingPhysicsObjectArray.Length; a++)
                m_myPhysics.DisposeActor(ViewCullingPhysicsObjectArray[a]);
            //MyPhyX.DisposeActor(ref ViewCullingObjectArray[a]);
            ViewCullingPhysicsObjectArray = null;
            m_myPhysics = null;
        }

        public void Add(object PhysicsActor)
        {
            ViewCullingPhysicsObjectArray[count] = PhysicsActor;
            count++;
        }

        public void SetMatrix(int id, ref Quaternion quat, ref Vector3 pos)
        {          
            m_myPhysics.SetPose(ViewCullingPhysicsObjectArray[id], ref quat, ref pos);
            m_myPhysics.SetFreeze(ViewCullingPhysicsObjectArray[id]);//這邊一值關掉會有效
        }

        public void SetMatrix(int id, ref Matrix worldT)
        {
            m_myPhysics.SetPose(ViewCullingPhysicsObjectArray[id], ref worldT);
            m_myPhysics.SetFreeze(ViewCullingPhysicsObjectArray[id]);//這邊一值關掉會有效
        }

    }
}
