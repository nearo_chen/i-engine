﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;

namespace I_XNAComponent
{
    public class DepthMapCreator : IDisposable
    {
        NodeArray m_BoundBoxNodeArray;//用來計算boundbox的Node，如有特別需要可以特別設定bound box，計算出特別的view matrix。
        //NodeArray m_RenderNodeArray;//畫出depth map時的node。
        int m_MapWH = 0;
        RenderTarget2D m_RenderTarget = null;

        List<Vector3> m_MergePointList = null;//儲存所有node bound box的點，用來計算總bound box用。
        Vector3[] m_CornerPoints = new Vector3[8];
        //public Matrix viewMat, projMat;

        /// <summary>
        /// 呼叫SetRenderTarget完，可以計算正確的DepthMapFrustum範圍。
        /// </summary>
        public Matrix DepthMapFrustum;

        Vector3 m_LookDirection;

        public Texture2D GetDepthMap { get { return m_RenderTarget.GetTexture(); } }

        public static DepthMapCreator Create()
        {
            DepthMapCreator ooo = new DepthMapCreator();
            ReleaseCheck.AddCheck(ooo);
            return ooo;// ReleaseCheck.CreateNew(typeof(DepthMapCreator)) as DepthMapCreator;
        }

        DepthMapCreator()
        {
        }

        ~DepthMapCreator()
        {
            ReleaseCheck.DisposeCheckCount(this);
        }

        public void Dispose()
        {
            ReleaseCheck.DisposeCheck(this);

            if (m_BoundBoxNodeArray != null)
            {
                m_BoundBoxNodeArray.Dispose();
                m_BoundBoxNodeArray = null;
            }
            //if (m_RenderNodeArray != null)
            //{
            //    m_RenderNodeArray.Dispose();
            //    m_RenderNodeArray = null;
            //}
            if (m_RenderTarget != null)
                m_RenderTarget.Dispose();
            m_RenderTarget = null;

            if (m_MergePointList != null)
                m_MergePointList.Clear();
            m_MergePointList = null;
        }

        /// <summary>
        /// 初始化的第1種方式，需要傳入depthMapData的初始化，初始化一次就好。
        /// </summary>   
        public void Initialize(int mapWH, DepthMapData depthMapData, FrameNode rootNode, GraphicsDevice device)
        {
            m_LookDirection = depthMapData.LookDirection;
            m_MapWH = mapWH;
            CreateRT(device);

            //搜尋要需要的Node
            m_BoundBoxNodeArray = new NodeArray(1024);
            for (int a = 0; a < depthMapData.BoundBoxNodeNames.Length; a++)
            {
                SceneNode node = SceneNode.SeekFirstMeshNodeAllName(rootNode, depthMapData.BoundBoxNodeNames[a]);
                m_BoundBoxNodeArray.Add(node);
            }

            ////搜尋要需要的Node
            //m_RenderNodeArray = new NodeArray(1024);
            //for (int a = 0; a < depthMapData.RenderNodeNames.Length; a++)
            //{
            //    SceneNode node = SceneNode.SeekFirstMeshNodeAllName(rootNode, depthMapData.RenderNodeNames[a]);
            //    m_RenderNodeArray.Add(node);
            //}
        }

        /// <summary>
        /// 初始化的第2種方式，之後render時必須SetRenderNodeArray()傳入NodeArray畫depth map。
        /// </summary>   
        public void Initialize(int mapWH, Vector3 lightDirection, GraphicsDevice device)
        {
            m_LookDirection = lightDirection;
            m_MapWH = mapWH;
            CreateRT(device);
        }

        ///// <summary>
        ///// 初始化的第2種方式，不需傳入node array計算範圍，直接由外部傳入範圍。
        ///// </summary>   
        //public void Initialize_SetRenderData(int mapWH, Vector3 lightDirection, GraphicsDevice device)
        //{
        //    LookDirection = lightDirection;
        //    m_MapWH = mapWH;
        //    CreateRT(device);

        //    //m_BoundBoxNodeArray = allNodeArray;
        //    //m_RenderNodeArray = allNodeArray;
        //    //m_BoundBoxNodeArray = new NodeArray(allNodeArray.nowNodeAmount);
        //    //m_RenderNodeArray = new NodeArray(allNodeArray.nowNodeAmount);
        //    //for (int a = 0; a < allNodeArray.nowNodeAmount; a++)
        //    //{
        //    //    SceneNode node = allNodeArray.nodeArray[a] as SceneNode;
        //    //    if (node.IfRenderShadowMap)
        //    //    {
        //    //        m_BoundBoxNodeArray.Add(node);
        //    //        m_RenderNodeArray.Add(node);
        //    //    }
        //    //}
        //}



        //public void AddBoundingBoxNode(SceneNode node)
        //{
        //    m_BoundBoxNodeArray.Add(node);
        //}

        ////public void AddRenderNode(SceneNode node) { m_RenderNodeArray.Add(node); }
        //public void SetBoundBoxNodeArray(NodeArray array)
        //{
        //    //m_RenderNodeArray = array;
        //    m_BoundBoxNodeArray = array;
        //}

        //public void ReleaseBoundBoxNodeArray()
        //{
        //    //m_RenderNodeArray = null;
        //    m_BoundBoxNodeArray = null;
        //}

        //public NodeArray GetRenderNodeArray() { return m_RenderNodeArray; }

        bool CreateRT(GraphicsDevice device)
        {
            if (m_RenderTarget != null)
                return true;

            SurfaceFormat depthMapFormat = SurfaceFormat.Unknown;
            if (GraphicsAdapter.DefaultAdapter.CheckDeviceFormat(
                               DeviceType.Hardware,
                               GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Format,
                               TextureUsage.Linear, QueryUsages.None,
                               ResourceType.RenderTarget, SurfaceFormat.Single) == true)
            {
                depthMapFormat = SurfaceFormat.Single;
                //depthMapFormat = SurfaceFormat.Color;
            }
            else if (GraphicsAdapter.DefaultAdapter.CheckDeviceFormat(
                               DeviceType.Hardware,
                               GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Format,
                               TextureUsage.Linear, QueryUsages.None,
                               ResourceType.RenderTarget, SurfaceFormat.HalfSingle)
                               == true)
            {
                depthMapFormat = SurfaceFormat.HalfSingle;
            }


            try
            {
                // Create new floating point render target
                m_RenderTarget = new RenderTarget2D(device,
                                                        m_MapWH,
                                                        m_MapWH,
                                                        1, depthMapFormat, RenderTargetUsage.DiscardContents);                
            }
            catch
            {
                return false;
            }

            //device.SetRenderTarget(0, m_RenderTarget);//建完要設定RT一下，不然沒法用。
            //device.SetRenderTarget(0, null);//建完要設定RT一下，不然沒法用。
            return true;
        }

        /// <summary>
        /// 根據BoundBoxNodeArray計算影子範圍(較耗時)，
        /// 呼叫此function完，可以得到計算後的viewMatrix * projMatrix = DepthMapFrustum可直接拿來用
        /// 要是沒有設定m_BoundBoxNodeArray，就沒有viewMatrix和projMatrix，傳null出去表示沒東西好畫。
        /// </summary>  
        public void SetRenderTarget(GraphicsDevice device, Vector3? lookDirection, 
            out Matrix? viewMatrix, out Matrix? projMatrix)
        {
            //使用此function沒有傳入計算frustum範圍，所以拿m_BoundBoxNodeArray來計算範圍，
            //要是沒有設定m_BoundBoxNodeArray，就沒有viewMatrix和projMatrix，傳null出去表示沒東西好畫。
            if (//m_RenderNodeArray == null || 
                m_BoundBoxNodeArray == null || m_BoundBoxNodeArray.nowNodeAmount == 0)
            {
                projMatrix = viewMatrix = null;
                return;
            }

            if (lookDirection.HasValue)
                m_LookDirection = lookDirection.Value;

            //Set our render target to our floating point render target
            device.SetRenderTarget(0, m_RenderTarget);
            device.Clear(Color.White);//設定為1表示Z深度最大

            Matrix viewMat, projMat;
            CreateLightViewProjectionMatrix(m_BoundBoxNodeArray, ref m_LookDirection, out viewMat, out projMat);
            viewMatrix = viewMat;
            projMatrix = projMat;
            Matrix.Multiply(ref viewMat, ref projMat, out DepthMapFrustum);
        }

        /// <summary>
        /// 直接設定RT並做Clear不做其他事情。
        /// </summary>
        public void SetRenderTarget(GraphicsDevice device)
        {
            device.SetRenderTarget(0, m_RenderTarget);
            device.Clear(ClearOptions.DepthBuffer | ClearOptions.Target, Color.White, 1, 0);//設定為1表示Z深度最大
        }

        /// <summary>
        /// 提供MyCornerPoints使用者自行傳入範圍，速度較快。
        /// boxDepthScale是控制影子範圍深度的縮放
        /// 呼叫此function完，可以得到計算後的viewMatrix * projMatrix = DepthMapFrustum可直接拿來用
        /// 因為有直接傳入MyCornerPoints計算frustum範圍，所以確定會有範圍，viewMatrix, projMatrix一定會out個值。
        /// </summary>      
        public void SetRenderTarget(GraphicsDevice device, Vector3? lookDirection, Vector3 upVector,
            Vector3[] MyCornerPoints, float boxDepthScale,
            out Matrix? viewMatrix, out Matrix? projMatrix)
        {
            //因為有直接傳入MyCornerPoints計算frustum範圍，所以確定會有範圍，viewMatrix, projMatrix一定會有個值。
            //if (m_RenderNodeArray == null || m_BoundBoxNodeArray == null)
            //{
            //    projMatrix = viewMatrix = null;
            //    return;
            //}

            if (lookDirection.HasValue)
                m_LookDirection = lookDirection.Value;

            //Set our render target to our floating point render target
            device.SetRenderTarget(0, m_RenderTarget);
            device.Clear(ClearOptions.DepthBuffer | ClearOptions.Target, Color.White, 1, 0);//設定為1表示Z深度最大

            Matrix viewMat, projMat;
            CreateLightViewProjectionMatrix(MyCornerPoints, boxDepthScale, ref m_LookDirection, upVector, out viewMat, out projMat);
            viewMatrix = viewMat;
            projMatrix = projMat;
            Matrix.Multiply(ref viewMat, ref projMat, out DepthMapFrustum);
        }


        /// <summary>
        /// Creates the WorldViewProjection matrix from the perspective of the 
        /// light using the cameras bounding frustum to determine what is visible 
        /// in the scene.     可以考慮使用多執行緒計算範圍
        /// </summary>
        public void CreateLightViewProjectionMatrix(NodeArray nodeArray, ref Vector3 lookDirection,
            out Matrix viewMat, out Matrix projMat)
        {
            // Get the corners of the frustum
            GetShadowNodeMergeBox(nodeArray).GetCorners(m_CornerPoints);
            CreateLightViewProjectionMatrix2(1, ref lookDirection, Vector3.Up, out viewMat, out projMat);
        }

        /// <summary>     
        /// MyCornerPoints是提供給外部傳入自己定義的範圍，不計算GetShadowNodeMergeBox()速度較快。
        /// </summary>
        void CreateLightViewProjectionMatrix(Vector3[] MyCornerPoints, float boxDepthScale, ref Vector3 lookDirection, Vector3 upVector,
            out Matrix viewMat, out Matrix projMat)
        {
            m_CornerPoints = MyCornerPoints;
            CreateLightViewProjectionMatrix2(boxDepthScale, ref lookDirection, upVector,out viewMat, out projMat);
        }

      

        /// <summary>
        /// 跟據m_CornerPoints，以及lookDirection計算m_CornerPoints範圍的viewMat，和projMat。
        /// 投影矩陣為平行投影，constraintBoundingBox為是否要限制投影的長寬高最大範圍，不限制就傳NULL。
        /// </summary>
        void CreateLightViewProjectionMatrix2(float boxDepthScale, ref Vector3 lookDirection, Vector3 upVector, out Matrix viewMat, out Matrix projMat)
        {
            // Matrix with that will rotate in points the direction of the light
            Matrix lightRotation = Matrix.CreateLookAt(Vector3.Zero,
                                                       lookDirection,
                                                       upVector);

            // Transform the positions of the corners into the direction of the light
            Vector3.Transform(m_CornerPoints, ref lightRotation, m_CornerPoints);

            // Find the smallest box around the points
            BoundingBox lightBox = BoundingBox.CreateFromPoints(m_CornerPoints);

            Vector3 boxSize = lightBox.Max - lightBox.Min;
            Vector3 halfBoxSize = boxSize * 0.5f;

            // The position of the light should be in the center of the back
            // pannel of the box. 
            Vector3 lightPosition = lightBox.Min + halfBoxSize;
            lightPosition.Z = lightBox.Min.Z;

            // We need the position back in world coordinates so we transform 
            // the light position by the inverse of the lights rotation
            //    m_LightPosition = Vector3.Transform(m_LightPosition,
            //                                      Matrix.Invert(lightRotation));
            Matrix.Invert(ref lightRotation, out lightRotation);
            Vector3.Transform(ref lightPosition, ref lightRotation, out lightPosition);

            // Create the view matrix for the light
            viewMat = Matrix.CreateLookAt(lightPosition,
                                                   lightPosition + lookDirection,
                                                   Vector3.Up);

            // Create the projection matrix for the light
            // The projection is orthographic since we are using a directional light
            //boxSize.Z *= 4;
            Matrix.CreateOrthographic(boxSize.X, boxSize.Y,
                                                     -boxSize.Z * boxDepthScale, boxSize.Z * boxDepthScale, out projMat);

            //   Matrix.Multiply(ref m_LightView, ref m_LightProjection, out LightViewProjectionMatrix);
        }

        /// <summary>
        /// 計算傳入nodeArray中，每個node的BoundingBox綜合的大AABB
        /// </summary>
        BoundingBox GetShadowNodeMergeBox(NodeArray nodeArray)
        {
            int vecAmount = nodeArray.nowNodeAmount * 8;
            if (m_MergePointList == null)
                m_MergePointList = new List<Vector3>(vecAmount);

            m_MergePointList.Clear();
            //if (m_MergePointList.Length < vecAmount)
            //    m_MergePointList = new Vector3[vecAmount];

            for (int a = 0; a < nodeArray.nowNodeAmount; a++)
            {
                SceneNode node = nodeArray.nodeArray[a] as SceneNode;
                //   if (node == null)
                //      continue;

                Matrix world = node.WorldMatrix;

                node.BoundingBox.GetCorners(m_CornerPoints);
                Vector3.Transform(m_CornerPoints, ref world, m_CornerPoints);
                //m_CornerPoints.CopyTo(m_MergePointList, a * 8);
                m_MergePointList.AddRange(m_CornerPoints);
            }

            return BoundingBox.CreateFromPoints(m_MergePointList);
        }




        /// <summary>
        /// 用來建立depth map必須要的資訊
        /// </summary>         
        [Serializable]
        public class DepthMapData
        {
            public DepthMapData()
            {
                BoundBoxNodeNames = RenderNodeNames = null;
                LookDirection = new Vector3(0, -0.99f, 0.01f);
                LookDirection.Normalize();
            }

            /// <summary>
            /// 用來計算boundbox的Node的名字，之後程式會去scene中搜尋該meshNode。
            /// </summary>
            public string[] BoundBoxNodeNames;

            /// <summary>
            /// 用來畫depth map的Node的名字，之後程式會去scene中搜尋該meshNode。
            /// </summary>
            public string[] RenderNodeNames;

            /// <summary>
            /// 繪製depth map的視野方向。
            /// </summary>
            public Vector3 LookDirection;
        }
    }
}
