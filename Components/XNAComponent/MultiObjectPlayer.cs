﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using I_XNAUtility;


namespace I_XNAComponent
{
    /// <summary>
    /// 控制場景裡的動態模型，聲音，particle..的播放。
    /// </summary>
    public class MultiObjectPlayer
    {
        //public string KeyName;
        //public string LocatorFileName;
        //public string KeyFrameMeshFileName;
        //public int PlayForwardTime { get { return m_ForwardTime; } }
        //public int PlayBackwardTime { get { return m_BackwardTime; } }

        //1是正向播放，-1是反向播放，0是暫停。
        public enum PlayState
        {
            Pause = 0,
            PlayForward = 1,
            PlayBack = -1,
        }

        PlayState m_TimeWay = PlayState.Pause;
        int m_ForwardTime = 0; //正著撥播放的時間範圍。
        int m_BackwardTime = 0;      //反著撥播放的時間範圍。
        int m_ElapsedMilliseconds = 0;
        float m_PlayRatio = 0; //0是撥到頭，1是撥到尾。

        //SkinningMeshNode m_SkinnedMeshNode = null;
        SkinningMeshLoader m_SkinnedMeshLoader = null;
        //KeyFrameMesh2 m_KeyFrameMesh= null;
        KeyframeMeshLoader m_KeyFrameMesh = null;

        public string ParticleFileName;
        FrameNode m_ParticleLocatorNode = null;
        float m_Particle_ForwardStartTime;  //去時什麼時候要開始加入particle
        float m_Particle_ForwardEndTime;
        float m_Particle_BackwardStartTime; //回時什麼時候要開始加入particle
        float m_Particle_BackwardEndTime;
        //Vector4 particle_minColor; //particle顏色內差最小值

        //ushort[] m_ParticleEmmiterNumber;

        string m_CueName = null;
        MySound m_MySound;
        AudioEmitter m_CueEmmiter = null;
        Cue m_SoundCue = null;
        FrameNode m_SoundLocatorNode = null;
        float Sound_ForwardStartRatio;
        float Sound_BackwardStartRatio;
        bool m_bPlayOnce = false; //true是聲音若是要撥就撥他的不用管是否停了，時間往前跑可以撥一次，往回跑時也撥一次。

        //-----------------------------------------------------------------------------------------------------------------------
        public MultiObjectPlayer(float startPlayRatio, int forwardTime, int backwardTime)
        {
            m_PlayRatio = startPlayRatio;
            SetPlayTime(forwardTime, backwardTime);
        }

        public void Dispose()
        {
            //if (m_SkinnedMeshNode != null)
            //{
            ////    m_SkinnedMeshNode.DetachFromParent
            //    m_SkinnedMeshNode.Dispose();
            //    m_SkinnedMeshNode = null;
            //}

            if (m_SkinnedMeshLoader != null)
            {
                m_SkinnedMeshLoader.Dispose();
                m_SkinnedMeshLoader = null;
            }

            if (m_KeyFrameMesh != null)
            {
                m_KeyFrameMesh.Dispose();
                m_KeyFrameMesh = null;
            }
        }

        public void LoadSkinnedMesh(string locatorName, string fileName, LocatorNode parentNode, ContentManager content, GraphicsDevice device)
        {
            SkinningMeshLoader loader;
            SkinningMeshLoader.LoadSkinningMeshLoader(fileName, content, device, out loader);
            LoadSkinnedMesh(locatorName, loader, parentNode);
        }

        public void LoadSkinnedMesh(string locatorName, SkinningMeshLoader loader, LocatorNode parentNode)
        {
            m_SkinnedMeshLoader = loader;
            parentNode.AttachChild(m_SkinnedMeshLoader.GetRootNode);
            m_SkinnedMeshLoader.SetAnimationPlay(0, true, 1, 0, AnimationPlayerManager.AnimationPriority.Low);
       //     m_SkinnedMeshLoader.AllAnimationStop();


            //m_SkinnedMeshNode = SoftwareSkinningMeshNode.CreateNew(typeof(SoftwareSkinningMeshNode)) as SoftwareSkinningMeshNode;
            //m_SkinnedMeshNode.LoadContent(fileName, content, device);

            //m_SkinnedMeshNode.NodeName = locatorName + "_SkinnedMesh";
            //m_SkinnedMeshNode.IfLightEnable = true;
            //parentNode.AttachChild(m_SkinnedMeshNode);

            //m_SkinnedMeshNode.AllAnimationStop();
        }

        public void LoadKeyframeMesh(string locatorName, string fileName, LocatorNode parentNode, ContentManager content, GraphicsDevice device)
        {
            KeyframeMeshLoader.LoadKeyFrameMesh(fileName, content, device, out m_KeyFrameMesh);

            //m_KeyFrameMesh = KeyFrameMesh2.Create();
            //m_KeyFrameMesh.LoadContent(fileName, content, device);

            m_KeyFrameMesh.GetRootNode.NodeName = locatorName + "_KeyFrameMesh";
            parentNode.AttachChild(m_KeyFrameMesh.GetRootNode);

            m_KeyFrameMesh.AllAnimationStop();
        }

        void updateKeyFrameMesh()
        {
            if (m_KeyFrameMesh == null)
                return;

            m_KeyFrameMesh.AnimatedWithRatio(0, m_PlayRatio);
            m_KeyFrameMesh.UpdateKeyFrameTransform(0);//AnimatedWithRatio的話就隨便傳時間
        }

        void updateSkinnedMesh(ContentManager content, GraphicsDevice device, MySound sound, ref Matrix invView)
        {
            if (m_SkinnedMeshLoader == null)
                return;
            m_SkinnedMeshLoader.AnimatedWithRatio(0, m_PlayRatio);
            m_SkinnedMeshLoader.Update(0, content, device, sound, ref invView);//AnimatedWithRatio的話就隨便傳時間

            //if (m_SkinnedMeshNode == null)
            //    return;

            //m_SkinnedMeshNode.AnimatedWithRatio(0, m_PlayRatio);
            //m_SkinnedMeshNode.UpdateSkinningTransform(0);//AnimatedWithRatio的話就隨便傳時間
        }


        //-----------------------------------------------------------------------------------------------------------------------

        public void LoadParticleData(LocatorNode rootNode, string particleFileName, float forwardStart, float forwardEnd,
            float backWardStart, float backWardEnd)//, string searchMeshNodeKey)
        {
            //如果有要抓關鍵字，就抓關鍵字的mesh node，不然就抓child node。
            //if(string.IsNullOrEmpty(searchMeshNodeKey))
            m_ParticleLocatorNode = rootNode;
            //else
            //    m_ParticleLocatorNode = SceneNode.SeekFirstMeshNodeAllName(rootNode, searchMeshNodeKey);

            ParticleFileName = particleFileName;
            m_Particle_ForwardStartTime = forwardStart;
            m_Particle_ForwardEndTime = forwardEnd;
            m_Particle_BackwardStartTime = backWardStart;
            m_Particle_BackwardEndTime = backWardEnd;
        }

        public Matrix GetParticleMatrix
        {
            get { return m_ParticleLocatorNode.WorldMatrix; }
        }

        /// <summary>
        /// 如果particle有要噴發，就return m_ParticleLocatorNodeArray，告訴外面
        /// Particle Manager 要噴發的reference點。
        /// </summary>
        public FrameNode IfParticleNeedPlay()
        {
            bool play = false;
            if (m_TimeWay == PlayState.PlayForward)
            {
                if (m_PlayRatio >= m_Particle_ForwardStartTime && m_PlayRatio <= m_Particle_ForwardEndTime)
                    play = true;
            }
            if (m_TimeWay == PlayState.PlayBack)
            {
                if (m_PlayRatio >= m_Particle_BackwardEndTime && m_PlayRatio <= m_Particle_BackwardStartTime)
                    play = true;
            }
            if (play)
                return m_ParticleLocatorNode;
            else
                return null;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        public void LoadSound(
            LocatorNode rootNode,
            string cueName,
            float forwardStartTime,
            float backwardStartTime,
            bool playOnce)
        {
            m_SoundLocatorNode = rootNode;
            m_CueName = cueName;
            Sound_ForwardStartRatio = forwardStartTime;
            Sound_BackwardStartRatio = backwardStartTime;
            m_CueEmmiter = new AudioEmitter();

            m_bPlayOnce = playOnce;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        float m_Pitch = -1;
        public void SetSoundPitch(float pitch)
        {
            m_Pitch = pitch;
            //m_MySound.SetVariable("DopplerPitchScalar", pitch, ref m_SoundCue);
        }

        void UpdateSound(MySound sound, ref Matrix invCameraMat)
        {
            m_MySound = sound;
            if (string.IsNullOrEmpty(m_CueName))
                return;
            if (sound == null)
                return;

            m_CueEmmiter.Position = m_SoundLocatorNode.WorldPosition;
            m_CueEmmiter.Up = m_SoundLocatorNode.WorldUp;
            m_CueEmmiter.Forward = m_SoundLocatorNode.WorldForward;
            if (m_SoundCue != null && m_SoundCue.IsPlaying)
            {
                //sound.CheckStartPlaySound3D(ref m_SoundCue, m_CueEmmiter);
                sound.UpdateSoundEmmiter(m_SoundCue, m_CueEmmiter);

                if (m_Pitch >= 0)
                {
                    //UpdateSoundEmmiter()裡面的apply 3D會讓setvariable失效，所以放在後面設定。
                    m_MySound.SetVariable("DopplerPitchScalar", m_Pitch, ref m_SoundCue);
                    m_Pitch = -1;
                }
            }
            else
            {
                bool bPlaySound = false;
                if (m_TimeWay == PlayState.PlayForward)
                {
                    if (m_PlayRatio >= Sound_ForwardStartRatio && m_SoundCue == null)
                        bPlaySound = true;
                }
                else if (m_TimeWay == PlayState.PlayBack)
                {
                    if (m_PlayRatio <= Sound_BackwardStartRatio && m_SoundCue == null)
                        bPlaySound = true;
                }

                if (bPlaySound)
                {
                    m_SoundCue = sound.GetCue(m_CueName);
                    sound.CheckStartPlaySound3D(ref m_SoundCue, m_CueEmmiter);
                }
            }
        }

        //-----------------------------------------------------------------------------------------------------------------------
        void SetPlayTime(int forwardTime, int backwardTime)
        {
            m_ForwardTime = forwardTime;
            m_BackwardTime = backwardTime;
            m_PlayRatio = 0;
            m_ElapsedMilliseconds = 0;
            SetPlay();
        }

        PlayState oldTimeway = PlayState.Pause;
        public void SetPause()
        {
            oldTimeway = m_TimeWay;
            m_TimeWay = PlayState.Pause;

            if (m_MySound != null)
                m_MySound.CheckPlayStop(ref m_SoundCue);
        }

        public void SetStop()
        {
            m_TimeWay = PlayState.Pause;
            m_ElapsedMilliseconds = 0;

            if (m_MySound != null)
                m_MySound.CheckPlayStop(ref m_SoundCue);
        }

        public void SetReplay()
        {
            m_TimeWay = PlayState.PlayForward;
            m_ElapsedMilliseconds = 0;

            if (m_MySound != null)
                m_MySound.CheckPlayStop(ref m_SoundCue);
        }

        public void SetPlay()
        {
            if (oldTimeway != 0)
                m_TimeWay = oldTimeway;
            else
                m_TimeWay = PlayState.PlayForward;
        }

        /// <summary>
        /// 取得目前撥放的狀態0是暫停，1是正向撥過去中，-1是反向撥回來中。
        /// </summary>
        public PlayState GetPlayState()
        {
            return m_TimeWay;
        }

        //-----------------------------------------------------------------------------------------------------------------------
        public void Update(int ellapseMillisecond, ContentManager content, GraphicsDevice device, MySound sound, ref Matrix invCam)
        {
            m_ElapsedMilliseconds += ellapseMillisecond * (int)m_TimeWay;

            if (m_ElapsedMilliseconds > m_ForwardTime && m_TimeWay != PlayState.PlayBack)
            {
                m_TimeWay = PlayState.PlayBack;
                m_ElapsedMilliseconds = m_BackwardTime;

                if (m_MySound != null)
                {
                    if (m_bPlayOnce == false)
                        m_MySound.CheckPlayStop(ref m_SoundCue);
                }
            }
            if (m_ElapsedMilliseconds < 0)
            {
                SetReplay();
            }

            m_PlayRatio = 0;
            if (m_TimeWay == PlayState.PlayForward)
                m_PlayRatio = (float)m_ElapsedMilliseconds / m_ForwardTime;
            else
                m_PlayRatio = (float)m_ElapsedMilliseconds / m_BackwardTime;

            //UpdateParticle();
            UpdateSound(sound, ref invCam);
            updateKeyFrameMesh();
            updateSkinnedMesh(content, device, sound, ref invCam);
        }

        //----------------------------------------------------------------------------------
        //public uint DrawBefore(ref Matrix cameraView, ref Matrix cameraProjection)
        //{
        //    uint triangles=0;
        //   // drawKeyFrameMesh(ref triangles);
        //    return triangles;
        //}

        //public void DrawAfter(ref Matrix cameraView, ref Matrix cameraProjection)
        //{
        //    if (m_ParticleSystem != null)
        //    {
        //        m_ParticleSystem.SetCamera(ref cameraView, ref cameraProjection);

        //        //應為所有PARTICLE的EFFECT用同一個所以在RENDER時設定
        //        if (particle_minColor != Vector4.Zero)
        //            m_ParticleSystem.SetMinColor(ref particle_minColor);

        //        m_ParticleSystem.Draw();
        //    }
        //}
    }



}
