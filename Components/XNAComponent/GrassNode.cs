﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using I_XNAUtility;

namespace I_XNAComponent
{
    public class GrassNode : SceneNode
    {
        public static GrassNode Create()
        {
            return ReleaseCheck.CreateNew(typeof(GrassNode)) as GrassNode;
        }

        struct GrassVertex
        {
            public Vector3 position;

            // Add a vertex channel holding normal vectors.
            public Vector3 normal;

            // Add a vertex channel holding texture coordinates.
            public Vector2 texture;

            // Add a second texture coordinate channel, holding a per-billboard
            // random number. This is used to make each billboard come out a
            // slightly different size, and to animate at different speeds.
            public float random;

            // Describe the layout of this vertex structure.
            public static readonly VertexElement[] VertexElements =
            {
                new VertexElement(0, 0, VertexElementFormat.Vector3,
                                        VertexElementMethod.Default,
                                        VertexElementUsage.Position, 0),

                new VertexElement(0, 12, VertexElementFormat.Vector3,
                                         VertexElementMethod.Default,
                                         VertexElementUsage.Normal, 0),

                new VertexElement(0, 24, VertexElementFormat.Vector2,
                                         VertexElementMethod.Default,
                                         VertexElementUsage.TextureCoordinate, 0),

                new VertexElement(0, 32, VertexElementFormat.Single,
                                         VertexElementMethod.Default,
                                         VertexElementUsage.TextureCoordinate, 1),
            };
            // Describe the size of this vertex structure.
            public const int SizeInBytes = 36;
        }

        VertexBuffer m_VertexBuffer = null;
        IndexBuffer m_IndexBuffer = null;
        VertexDeclaration m_VertexDeclaration = null;
        //GrassVertex[] m_GrassVertexBuffer = null;
        //ushort[] m_GrassIndexBuffer = null;

        public GrassNode()
        {
        }

        protected override void v_Dispose()
        {
            if (m_VertexBuffer != null)
                m_VertexBuffer.Dispose();
            m_VertexBuffer = null;

            if (m_IndexBuffer != null)
                m_IndexBuffer.Dispose();
            m_IndexBuffer = null;

            if (m_VertexDeclaration != null)
                m_VertexDeclaration.Dispose();
            m_VertexDeclaration = null;

            //m_GrassVertexBuffer = null;
            //m_GrassIndexBuffer = null;
        }

        //protected override bool v_ConsiderCurrentTechniqueOK(RenderTechnique CurrentRenderTechnique)
        //{
        //    return MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.GrassMesh);
        //}

        protected override void v_Render(EffectRenderManager effectRenderManager, GraphicsDevice device)
        {
            if (m_VertexBuffer == null)
                return;
            effectRenderManager.SetGrassMeshEffectPar(WindData, WorldMatrix);

            device.VertexDeclaration = m_VertexDeclaration;

            // Set vertex buffer and index buffer
            device.Vertices[0].SetSource(m_VertexBuffer, 0, GrassVertex.SizeInBytes);
            device.Indices = m_IndexBuffer;

            // And render all primitives
            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, m_VertexBuffer.SizeInBytes / GrassVertex.SizeInBytes,
                0, m_IndexBuffer.SizeInBytes / 16);

            device.VertexDeclaration = null;
            device.Indices = null;
        }

        public override ModelMesh GetModelMeshPart(out int partID)
        {
            throw new NotImplementedException();
        }

        ushort positionSet; int indexSet;
        public bool CreateGrassData(SpecialMeshDataManager.GrassData grassData, GraphicsDevice device, ContentManager content,
          uint[] indices, Vector3[] vb, Vector3[] nb)
        {
            GrassVertex[] grassVertexBuffer = null;
            ushort[] grassIndexBuffer = null;

            //點數量為，每個三角面的草數量x三角面數量*4。
            int vertexAmount = indices.Length / 3 * grassData.billboardsPerTriangle * 4;
            if (vertexAmount >= 65536)
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage("Create GrassData : vertexAmount >= 65536");
                return false;
            }
            //indexbuffer數量為，m_GrassVertexBuffer數量/ 4 * 6。
            indexSet = 0; positionSet = 0;
            grassVertexBuffer = new GrassVertex[vertexAmount];
            grassIndexBuffer = new ushort[grassVertexBuffer.Length / 4 * 6];

            // Loop over all the triangles in this piece of geometry.
            for (int triangle = 0; triangle < indices.Length; triangle += 3)
            {
                // Look up the three indices for this triangle.
                uint i1 = indices[triangle + 0];
                uint i2 = indices[triangle + 1];
                uint i3 = indices[triangle + 2];

                // Create vegetation billboards to cover this triangle.
                // A more sophisticated implementation would measure the
                // size of the triangle to work out how many to create,
                // but we don't bother since we happen to know that all
                // our triangles are roughly the same size.
                for (int count = 0; count < grassData.billboardsPerTriangle; count++)
                {
                    Vector3 position, normal;

                    // Choose a random location on the triangle.
                    PickRandomPoint(vb[i1], vb[i2], vb[i3], nb[i1], nb[i2], nb[i3],
                                    out position, out normal);

                    //為此面草產生4個點及三角面索引2面 
                    float randomValue = (float)MyMath.GetRand() * 2 - 1;
                    ushort positionIndex = positionSet;
                    for (int a = 0; a < 4; a++)
                    {
                        grassVertexBuffer[positionSet].position = position;
                        grassVertexBuffer[positionSet].normal = normal;
                        grassVertexBuffer[positionSet].random = randomValue;
                        positionSet++;
                    }

                    grassVertexBuffer[positionIndex + 0].texture = new Vector2(0, 0);
                    grassVertexBuffer[positionIndex + 1].texture = new Vector2(1, 0);
                    grassVertexBuffer[positionIndex + 2].texture = new Vector2(1, 1);
                    grassVertexBuffer[positionIndex + 3].texture = new Vector2(0, 1);

                    grassIndexBuffer[indexSet] = (ushort)(positionIndex + 0);
                    indexSet++;
                    grassIndexBuffer[indexSet] = (ushort)(positionIndex + 1);
                    indexSet++;
                    grassIndexBuffer[indexSet] = (ushort)(positionIndex + 2);
                    indexSet++;
                    grassIndexBuffer[indexSet] = (ushort)(positionIndex + 0);
                    indexSet++;
                    grassIndexBuffer[indexSet] = (ushort)(positionIndex + 2);
                    indexSet++;
                    grassIndexBuffer[indexSet] = (ushort)(positionIndex + 3);
                    indexSet++;
                }
            }

            m_VertexBuffer = new VertexBuffer(device, GrassVertex.SizeInBytes * grassVertexBuffer.Length, BufferUsage.WriteOnly);
            m_VertexBuffer.SetData(grassVertexBuffer);

            m_IndexBuffer = new IndexBuffer(device, 4 * grassIndexBuffer.Length, BufferUsage.WriteOnly, IndexElementSize.SixteenBits);
            m_IndexBuffer.SetData(grassIndexBuffer);

            m_VertexDeclaration = new VertexDeclaration(device, GrassVertex.VertexElements);
            return true;
        }


        /// <summary>
        /// Helper function chooses a random location on a triangle.
        /// </summary>
        void PickRandomPoint(Vector3 position1, Vector3 position2, Vector3 position3,
                             Vector3 normal1, Vector3 normal2, Vector3 normal3,
                             out Vector3 randomPosition, out Vector3 randomNormal)
        {
            float a = (float)MyMath.GetRand();
            float b = (float)MyMath.GetRand();

            if (a + b > 1)
            {
                a = 1 - a;
                b = 1 - b;
            }

            randomPosition = Vector3.Barycentric(position1, position2, position3, a, b);
            randomNormal = Vector3.Barycentric(normal1, normal2, normal3, a, b);

            randomNormal.Normalize();
        }
    }
}
