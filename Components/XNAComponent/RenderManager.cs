﻿
//using System;
//using System.Collections.Generic;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Storage;
//using System.Text;
//using System.IO;
//using I_XNAUtility;
//using MeshDataDefine;

//namespace I_XNAComponent
//{
//    public abstract class RenderManager : BasicSceneManager
//    {
  
//        DepthMapCreator m_BigShadowMap, m_SmallShadowMap;

//        //used for post processing..........
//        int m_RenderTargetW;
//        int m_RenderTargetH;
//        RenderTarget2D m_FullScreenRenderTargetA = null;
//        RenderTarget2D m_FullScreenRenderTargetB = null;
//        //RenderTarget2D m_NoMSAARenderTarget = null;
//        //RenderTarget2D m_FullScreenRenderTargetC=null;
//        //RenderTarget2D m_FullScreenRenderTargetD = null;
//        ResolveTexture2D m_ResolveTarget;

//        ShadowVolume m_ShadowVolume = null;

//        DepthStencilBuffer m_OriginalDepthBuffer = null;
//        DepthStencilBuffer m_NoMSAADepthBuffer = null;
//        DepthStencilBuffer m_ShadowMapDepthBuffer = null;

//        //protected RenderTarget2D GetNoMSAARenderTarget() { return m_NoMSAARenderTarget; }
//        protected RenderTarget2D GetRenderTargetA() { return m_FullScreenRenderTargetA; }
//        protected RenderTarget2D GetRenderTargetB() { return m_FullScreenRenderTargetB; }
//        //protected RenderTarget2D GetRenderTargetC() { return m_FullScreenRenderTargetC; }

//        Texture2D m_RTTexA, m_RTTexB;//, m_RTTexC;//, m_RTTexD;
//        protected Texture2D GetRenderTargetATexture() { if (m_RTTexA == null) { m_RTTexA = m_FullScreenRenderTargetA.GetTexture(); } return m_RTTexA; }
//        protected Texture2D GetRenderTargetBTexture() { if (m_RTTexB == null) { m_RTTexB = m_FullScreenRenderTargetB.GetTexture(); } return m_RTTexB; }
//        //protected Texture2D GetRenderTargetCTexture() { if (m_RTTexC == null) { m_RTTexC = m_FullScreenRenderTargetC.GetTexture(); } return m_RTTexC; }
//        //protected Texture2D GetRenderTargetDTexture() { if (m_RTTexD == null) { m_RTTexD = m_FullScreenRenderTargetD.GetTexture(); } return m_RTTexD; }

//        protected SpriteBatch m_SpriteBatch = null;
//        protected SpriteFont m_SpriteFont = null;

//        protected Texture2D ResolveBackBufferTexture()
//        {
//            m_GraphicDevice.ResolveBackBuffer(m_ResolveTarget);
//            return (Texture2D)m_ResolveTarget;
//            //return m_FullScreenRenderTargetB.GetTexture();
//        }

//        #region 弄特效專用的東西
//        //public EnvCubeMap m_EnvMap = null;
//        //public RenderToMap m_RenderMap = null;
//        //public MotionBlur m_MotionBlur = null;
//        MyGodsRay m_GodsRay = null;
//        LensFlareComponent m_LensFlareComponent = null;
//        //public Distortion m_Distortion = null;

//        //public Cloth m_Cloth = null;
//        Effect m_RefractDistort = null;
//        ReflectionMap m_ReflectionMap = null;
//        ReflectData m_ReflectionData = null;

//        MyEmmisive m_Emmisive = null;
//        MyBloom m_MyBloom = null;

//        protected void RenderLensFlare()
//        {
//            if (m_LensFlareComponent == null)
//                m_LensFlareComponent = new LensFlareComponent(m_Content, m_GraphicDevice);

//            m_LensFlareComponent.LightDirection = DirectionLight1;
//            m_LensFlareComponent.View = ViewMatrix;
//            //m_LensFlareComponent.Projection = ProjectionMatrix;
//            m_LensFlareComponent.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(60), m_GraphicDevice.Viewport.AspectRatio, 0.1f, 10000000f);

//            m_LensFlareComponent.Draw(m_GraphicDevice, m_SpriteBatch);
//        }

//        protected void SetRenderTarget_Emmisive(int id)
//        {
//            if (m_Emmisive == null)
//                m_Emmisive = new MyEmmisive(m_Content, m_GraphicDevice, m_RenderTargetW, m_RenderTargetH);
//            SetRenderTarget(id, m_Emmisive.GetEmmisiveRenderTarget());
//        }

//        protected void SetRenderTarget_GodsRay(int id)
//        {
//            if (m_GodsRay == null)
//                m_GodsRay = new MyGodsRay(m_Content, m_GraphicDevice, m_RenderTargetW, m_RenderTargetH);
//            SetRenderTarget(id, m_GodsRay.GetGodsRayRT());
//        }

//        Texture2D m_EmmTexture = null;
//        protected Texture2D GetEmmisiveTexture()
//        {
//            if (m_EmmTexture == null)
//                m_EmmTexture = m_Emmisive.GetEmmisiveRenderTarget().GetTexture();
//            return m_EmmTexture;
//        }

//        protected Texture2D GetGodsRayMap()
//        {
//            if (m_GodsRay == null)
//                return null;
//            return m_GodsRay.GetGodsRayMap();
//        }

//        /// <summary>
//        /// 將texture1根據他的alpha畫到sceneTexture上，輸出在rt。
//        /// </summary>      
//        protected void BlendTextureToScene(Texture2D texture1, Texture2D sceneTexture)
//        {
//            m_Emmisive.RenderEmmisiveBlending(texture1, sceneTexture, m_SpriteBatch, m_GraphicDevice);
//        }

//        /// <summary>
//        /// 丟一張圖進去做bloom處理，然後把bloom的結果畫到傳入的renderTarget上。
//        /// </summary>    
//        protected void RenderBloom(Texture2D texture, RenderTarget2D renderTarget)
//        {
//            if (m_MyBloom == null)
//            {
//                m_MyBloom = new MyBloom();
//                m_MyBloom.LoadContent(m_GraphicDevice, m_Content, m_SpriteBatch);
//            }
//            m_MyBloom.Draw(texture, renderTarget, 7);
//        }

//        /// <summary>
//        /// 丟一張圖進去做bloom處理，然後把bloom的結果畫到傳入的renderTarget上。
//        /// </summary>    
//        protected void RenderBloom2(Texture2D texture, RenderTarget2D renderTarget)
//        {
//            if (m_MyBloom == null)
//            {
//                m_MyBloom = new MyBloom();
//                m_MyBloom.LoadContent(m_GraphicDevice, m_Content, m_SpriteBatch);
//            }
//            m_MyBloom.Draw(texture, renderTarget, 6);
//        }

//        /// <summary>
//        /// 處理將gods ray map與目前畫面做scatter處理。
//        /// </summary>
//        protected void RenderGodsRayScatter(Texture2D sceneTexture, GodsRayLightNode[] godsRayLightNodes)
//        {
//            m_GodsRay.Draw(ref ViewMatrix, ref InverseViewMatrix, ref ProjectionMatrix, m_SpriteBatch, sceneTexture, godsRayLightNodes);
//        }

//        /// <summary>
//        /// 處理將物件畫在reflect map上。
//        /// </summary>        
//        protected ReflectData RenderReflectionMap(int size, ref Vector3 planeNormal, float height,
//            ReflectionMap.RenderReflectionHandler renderHandle)
//        {
//            if (m_ReflectionMap == null)
//            {
//                m_ReflectionMap = new ReflectionMap();
//                m_ReflectionMap.Initialize(m_GraphicDevice, m_Content, size, size);
//                m_ReflectionData = new ReflectData();
//            }

//            m_ReflectionMap.DrawReflectionMap(ref InverseViewMatrix, ref ProjectionMatrix, ref planeNormal, height, renderHandle);
//            m_ReflectionData.ReflectMap = m_ReflectionMap.GetReflectionMap;
//            m_ReflectionData.ReflectView = m_ReflectionMap.ReflectionViewMatrix;
//            return m_ReflectionData;
//        }




//        /// <summary>
//        /// 把傳入secneTexture，根據distortionMap利用RefractDistort effect處理，
//        /// 畫在外面設定的RT上。
//        /// </summary>
//        protected void RenderWithDistortMap(Texture2D distortionMap, Texture2D secneTexture)
//        {
//            if (m_RefractDistort == null)
//            {
//                if (Utility.IfContentOutSide)
//                {
//                    m_RefractDistort = Utility.ContentLoad_Effect(m_Content,
//                        Utility.GetFullPath("XNBData/Content/RefractDistort"));
//                }
//                else
//                {
//                    m_RefractDistort = Utility.ContentLoad_Effect(m_Content, "RefractDistort");
//                }
//                m_RefractDistort.CurrentTechnique = m_RefractDistort.Techniques["Distort"];
//            }

//            //一定要這樣畫post process的map，effect才會有用。
//            m_GraphicDevice.Textures[1] = distortionMap;
//            m_SpriteBatch.Begin(SpriteBlendMode.None,
//                             SpriteSortMode.Immediate,
//                             SaveStateMode.None);

//            // Begin the custom effect, if it is currently enabled. If the user
//            // has selected one of the show intermediate buffer options, we still
//            // draw the quad to make sure the image will end up on the screen,
//            // but might need to skip applying the custom pixel shader.
//            m_RefractDistort.Begin();
//            m_RefractDistort.CurrentTechnique.Passes[0].Begin();

//            // Draw the quad.
//            m_SpriteBatch.Draw(secneTexture, Vector2.Zero, Color.White);
//            m_SpriteBatch.End();

//            // End the custom effect.
//            m_RefractDistort.CurrentTechnique.Passes[0].End();
//            m_RefractDistort.End();
//            m_GraphicDevice.Textures[1] = null;
//        }
//        #endregion

//        //protected void ClearTarget(Color color)
//        //{
//        //    m_GraphicDevice.Clear(color);
//        //}

//        //protected void ClearTarget_Depth(Color color)
//        //{
//        //    m_GraphicDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, color, 1.0f, 0);
//        //}

//        protected void ClearTarget_All(Color color)
//        {
//            try
//            {
//                m_GraphicDevice.Clear(ClearOptions.DepthBuffer | ClearOptions.Stencil | ClearOptions.Target, color, 1.0f, 0);
//            }
//            catch (Exception e)
//            {
//                Utility.ShowErrorMessage(e);
//            }
//        }

//        protected void SetRenderTarget(int id, RenderTarget2D renderTarget)
//        {
//            m_GraphicDevice.SetRenderTarget(id, renderTarget);
//        }

//        new public void Update()
//        {
//            base.Update();
//        }

//        new public void Render()
//        {
//            //DrawLine.AddLineBegin(m_GraphicDevice);
//            DrawLineBegin();

//            base.Render();

//            //RenderLensFlare();

//            //#if DrawDebug
//            //            SetAlphaBlendRenderState(m_GraphicDevice);
//            //            RenderPointLightDebugLine();
//            //#endif

//            //DrawLine.Draw(ViewMatrix, ProjectionMatrix);
//        }

//        void DrawLineBegin()
//        {
//            DrawLine.AddLineBegin(m_GraphicDevice);
//        }

//        protected void DrawLineToBackBuffer()
//        {
//            DrawLine.Draw(ViewMatrix, ProjectionMatrix);
//        }

//        //public void RenderTexture_AlphaBlend(Texture2D texture, Vector2 pos, Vector2 size)
//        //{
//        //    Utility.RenderTexture2D(m_GraphicDevice, texture, SpriteBlendMode.None, pos, size);
//        //}

//        //public void RenderTexture_NoneBlend(Texture2D texture, Vector2 pos, Vector2 size)
//        //{
//        //    Utility.RenderTexture2D(m_GraphicDevice, texture, SpriteBlendMode.None, pos, size);
//        //}

//        public void RenderTextureFullScreen(Texture2D texture)
//        {
//            //m_SpriteBatch.Begin(SpriteBlendMode.None,
//            //                  SpriteSortMode.Immediate,
//            //                  SaveStateMode.None);
//            //m_SpriteBatch.Draw(texture,Vector2.Zero, Color.White);
//            //m_SpriteBatch.End();

//            m_SpriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
//            m_SpriteBatch.Draw(texture,
//                new Rectangle(m_GraphicDevice.Viewport.X, m_GraphicDevice.Viewport.Y,
//                    m_GraphicDevice.Viewport.Width, m_GraphicDevice.Viewport.Height), Color.White);
//            m_SpriteBatch.End();
//        }

//        protected void RenderShadowVolumeScreenSquare()
//        {
//            m_ShadowVolume.ShadowVolumeScreenSquare.Render(m_GraphicDevice);
//        }

//        /// <summary>
//        /// 畫出manager內用來debug的線段圖形
//        /// </summary>        
//        protected void RenderDebugLine(bool bDirectionalLight, BasicMeshLoader RenderDebugLineloader)
//        {

//            if (bDirectionalLight)
//            {
//                Vector3 pos = InverseViewMatrix.Translation + InverseViewMatrix.Forward * 100f;
//                Vector3 end = pos + 100f * DirectionLight1;
//                DrawLine.AddLine(pos, end, Color.Yellow);

//                Vector3 right = Vector3.Cross(Vector3.Up, DirectionLight1);
//                right.Normalize();
//                end = pos + 10000f * right;
//                DrawLine.AddLine(pos, end, Color.Brown);

//                Vector3 up = Vector3.Cross(DirectionLight1, right);
//                up.Normalize();
//                end = pos + 10000f * up;
//                DrawLine.AddLine(pos, end, Color.BurlyWood);

//                Vector3 arrow = pos + 10f * DirectionLight1;
//                DrawLine.AddLine(arrow, pos + 10f * right, Color.Red);
//                DrawLine.AddLine(arrow, pos + -10f * right, Color.Red);

//                pos = arrow;
//                arrow = pos + 8f * DirectionLight1;
//                DrawLine.AddLine(arrow, pos + 5f * up, Color.IndianRed);
//                DrawLine.AddLine(arrow, pos + -5f * up, Color.IndianRed);
//            }

//            if (RenderDebugLineloader != null)
//            {
//                RenderPointLightDebugLine(RenderDebugLineloader.StaticPointLightList);
//            }

//            if (m_BigShadowMap != null)
//            {
//                BoundingFrustum fff = new BoundingFrustum(m_BigShadowMap.DepthMapFrustum);
//                DrawLine.AddFrustumLine(fff, Color.Green);
//            }

//            if (m_SmallShadowMap != null)
//            {
//                BoundingFrustum fff = new BoundingFrustum(m_SmallShadowMap.DepthMapFrustum);
//                DrawLine.AddFrustumLine(fff, Color.Purple);
//            }
//            DrawLineToBackBuffer();
//        }

//        protected SpriteFont GetSpriteFont(string fontName)
//        {
//            if (m_SpriteFont == null)
//            {
//                fontName = Utility.GetFullPath("XNBData\\" + fontName);
//                m_SpriteFont = m_Content.Load<SpriteFont>(fontName);
//            }
//            return m_SpriteFont;
//        }

//        protected void RenderDebugString(Vector2 pos, string fontName, string debugString)
//        {
//            m_GraphicDevice.RenderState.FillMode = FillMode.Solid;
//            m_SpriteBatch.Begin();
//            m_SpriteBatch.DrawString(GetSpriteFont(fontName), debugString, pos, new Color(Color.Black, 128));
//            m_SpriteBatch.DrawString(GetSpriteFont(fontName), debugString, pos - Vector2.One, new Color(Color.Yellow, 128));
//            m_SpriteBatch.End();
//        }

//        EffectRenderManager m_GrassEffectRenderManager;
//        EffectRenderManager m_SimpleEffectRenderManager;
//        EffectRenderManager m_SkinnedMeshEffectRenderManager;
//        EffectRenderManager m_InstanceMeshEffectRenderManager;
//        EffectRenderManager m_NowRenderManager;
//        //       protected EffectRenderManager GetEffectRenderManager { get { return m_NowRenderManager; } }
//        protected void RenderTechniqueBegin(SceneNode.RenderTechnique technique)
//        {
//            if (MyMath.ConsiderBit((uint)technique, (uint)SceneNode.RenderTechnique.SkinnedMesh) == true)
//                m_NowRenderManager = m_SkinnedMeshEffectRenderManager;
//            else if (MyMath.ConsiderBit((uint)technique, (uint)SceneNode.RenderTechnique.SimpleInstanceMesh) == true)
//                m_NowRenderManager = m_InstanceMeshEffectRenderManager;
//            else if (MyMath.ConsiderBit((uint)technique, (uint)SceneNode.RenderTechnique.SimpleMesh) == true)
//                m_NowRenderManager = m_SimpleEffectRenderManager;
//            else if (MyMath.ConsiderBit((uint)technique, (uint)SceneNode.RenderTechnique.GrassMesh) == true)
//                m_NowRenderManager = m_GrassEffectRenderManager;
//            else
//                m_NowRenderManager = null;

//            m_NowRenderManager.SetRenderBegin(technique);
//        }
//        protected void RenderTechniqueEnd()
//        {
//            m_NowRenderManager.SetRenderEnd();
//        }

//        protected void SetBigShadowMap(Matrix BigShadowMapFrustum, Texture2D bigShadowmap,
//            float sampleLength)
//        {
//            m_SimpleEffectRenderManager.SetBigShadowMap(ref BigShadowMapFrustum, bigShadowmap, sampleLength);
//            m_SkinnedMeshEffectRenderManager.SetBigShadowMap(ref BigShadowMapFrustum, bigShadowmap, sampleLength);
//            m_InstanceMeshEffectRenderManager.SetBigShadowMap(ref BigShadowMapFrustum, bigShadowmap, sampleLength);
//            m_GrassEffectRenderManager.SetBigShadowMap(ref BigShadowMapFrustum, bigShadowmap, sampleLength);
//        }

//        protected void SetSmallShadowMap(Matrix BigShadowMapFrustum, Texture2D bigShadowmap)
//        {
//            m_SimpleEffectRenderManager.SetSmallShadowMap(ref BigShadowMapFrustum, bigShadowmap);
//            m_SkinnedMeshEffectRenderManager.SetSmallShadowMap(ref BigShadowMapFrustum, bigShadowmap);
//            m_InstanceMeshEffectRenderManager.SetSmallShadowMap(ref BigShadowMapFrustum, bigShadowmap);
//            m_GrassEffectRenderManager.SetSmallShadowMap(ref BigShadowMapFrustum, bigShadowmap);
//        }

//        protected Vector3 DirectionLight1, DirectionLight1D, DirectionLight1A, DirectionLight1S;
//        protected void SetDirectionalLight1(Vector3? lightAmbient, Vector3? lightDiffuse, Vector3? lightSpecular, Vector3? lightDirection)
//        {
//            DirectionLight1 = lightDirection.Value;
//            DirectionLight1D = lightDiffuse.Value;
//            DirectionLight1A = lightAmbient.Value;
//            DirectionLight1S = lightSpecular.Value;
//            m_SimpleEffectRenderManager.SetDirectionalLight1(lightAmbient, lightDiffuse, lightSpecular, lightDirection);
//            m_SkinnedMeshEffectRenderManager.SetDirectionalLight1(lightAmbient, lightDiffuse, lightSpecular, lightDirection);
//            m_InstanceMeshEffectRenderManager.SetDirectionalLight1(lightAmbient, lightDiffuse, lightSpecular, lightDirection);
//            m_GrassEffectRenderManager.SetDirectionalLight1(lightAmbient, lightDiffuse, lightSpecular, lightDirection);
//        }

//        SpotLight[] m_SpotLightArray = new SpotLight[128];
//        /// <summary>
//        ///  設定整個場景的spot light array
//        /// </summary>
//        protected void SetSpotLightArray(NodeArray spotLightNodeArray)
//        {
//            for (int a = 0; a < spotLightNodeArray.nowNodeAmount; a++)
//            {
//                SceneNode node = spotLightNodeArray.nodeArray.GetValue(a) as SceneNode;
//                m_SpotLightArray[a] = node.GetSpotLight();
//            }
//            m_SimpleEffectRenderManager.SetSpotLight(m_SpotLightArray, spotLightNodeArray.nowNodeAmount);
//            m_SkinnedMeshEffectRenderManager.SetSpotLight(m_SpotLightArray, spotLightNodeArray.nowNodeAmount);
//            m_InstanceMeshEffectRenderManager.SetSpotLight(m_SpotLightArray, spotLightNodeArray.nowNodeAmount);
//            m_GrassEffectRenderManager.SetSpotLight(m_SpotLightArray, spotLightNodeArray.nowNodeAmount);
//        }

//        protected Vector3 FogColor;
//        protected float FogStart, FogEnd;
//        protected void SetFog(float start, float end, float fogMaxRatio, Vector3 fogColor)
//        {
//            FogStart = start;
//            FogEnd = end;
//            FogColor = fogColor;
//            m_SimpleEffectRenderManager.SetFog(start, end, fogMaxRatio, fogColor);
//            m_SkinnedMeshEffectRenderManager.SetFog(start, end, fogMaxRatio, fogColor);
//            m_InstanceMeshEffectRenderManager.SetFog(start, end, fogMaxRatio, fogColor);
//            m_GrassEffectRenderManager.SetFog(start, end, fogMaxRatio, fogColor);
//        }

//        override public void Initialize(IServiceProvider iServiceProvider, GraphicsDevice device, Game game, MySound sound)
//        {
//            base.Initialize(iServiceProvider, device, game, sound);

//            try
//            {
//                m_PointLightManager = new PointLightManager();

//                if (Utility.IfContentOutSide)
//                {
//                    m_SimpleEffectRenderManager = new EffectRenderManager();
//                    m_SimpleEffectRenderManager.LoadEffect(m_Content, Utility.GetFullPath("XNBData\\Content\\fx\\SimpleEffect2"));
//                    m_SkinnedMeshEffectRenderManager = new EffectRenderManager();
//                    m_SkinnedMeshEffectRenderManager.LoadEffect(m_Content, Utility.GetFullPath("XNBData\\Content\\fx\\SkinnedModel2"));
//                    m_InstanceMeshEffectRenderManager = new EffectRenderManager();
//                    m_InstanceMeshEffectRenderManager.LoadEffect(m_Content, Utility.GetFullPath("XNBData\\Content\\fx\\InstancedModel2"));
//                    m_GrassEffectRenderManager = new EffectRenderManager();
//                    m_GrassEffectRenderManager.LoadEffect(m_Content, Utility.GetFullPath("XNBData\\Content\\fx\\GrassMesh"));
//                }
//                else
//                {
//                    m_SimpleEffectRenderManager = new EffectRenderManager();
//                    m_SimpleEffectRenderManager.LoadEffect(m_Content, "fx\\SimpleEffect2");
//                    m_SkinnedMeshEffectRenderManager = new EffectRenderManager();
//                    m_SkinnedMeshEffectRenderManager.LoadEffect(m_Content, "fx\\SkinnedModel2");
//                    m_InstanceMeshEffectRenderManager = new EffectRenderManager();
//                    m_InstanceMeshEffectRenderManager.LoadEffect(m_Content, "fx\\InstancedModel2");
//                    m_GrassEffectRenderManager = new EffectRenderManager();
//                    m_GrassEffectRenderManager.LoadEffect(m_Content, "fx\\GrassMesh");
//                }

//                m_SpriteBatch = new SpriteBatch(device);
//                m_SpriteFont = null;// m_Content.Load<SpriteFont>("Arial 14");

//                m_ShadowVolume = new ShadowVolume(device, m_Content);

//                //used for post processing........
//                m_RenderTargetW = device.PresentationParameters.BackBufferWidth;
//                m_RenderTargetH = device.PresentationParameters.BackBufferHeight;
                
//                m_FullScreenRenderTargetA =
//                    new RenderTarget2D(
//                        device, m_RenderTargetW, m_RenderTargetH,
//                        1, SurfaceFormat.Color,
//                        MultiSampleType.None, 0,
//                    //Utility.MultiSampleType, Utility.MultiSampleQuality,
//                        RenderTargetUsage.PreserveContents);
//                m_FullScreenRenderTargetB =
//                    new RenderTarget2D(
//                        device, m_RenderTargetW, m_RenderTargetH,
//                        1, SurfaceFormat.Color,
//                        MultiSampleType.None, 0,
//                    //Utility.MultiSampleType, Utility.MultiSampleQuality, 
//                        RenderTargetUsage.PreserveContents);

//                //m_NoMSAARenderTarget =
//                //    new RenderTarget2D(
//                //    device, m_RenderTargetW, m_RenderTargetH,
//                //        1, SurfaceFormat.Color,
//                //        MultiSampleType.None, 0,
//                //    //Utility.MultiSampleType, Utility.MultiSampleQuality, 
//                //        RenderTargetUsage.PreserveContents);

//                //,                        Utility.RenderTargetUsage);
//                //m_FullScreenRenderTargetC = new RenderTarget2D(device, m_RenderTargetW, m_RenderTargetH, 1, SurfaceFormat.Color, RenderTargetUsage.PreserveContents);

//                // Create a texture for reading back the backbuffer contents.
//                m_ResolveTarget = new ResolveTexture2D(device, m_RenderTargetW, m_RenderTargetH, 1, device.PresentationParameters.BackBufferFormat);

//                m_OriginalDepthBuffer = device.DepthStencilBuffer;
//                //m_DepthBuffer4096 = new DepthStencilBuffer(device, 4096, 4096, DepthFormat.Depth24Stencil8);

//                OrthographicView = Matrix.CreateLookAt(new Vector3(0, 0, 100), Vector3.Zero, Vector3.Up);
//                InvOrthographicView = Matrix.Invert(OrthographicView);
//                OrthographicProjection = Matrix.CreateOrthographic(device.PresentationParameters.BackBufferWidth,
//                    device.PresentationParameters.BackBufferHeight, 0.1f, 100000f);
//            }
//            catch (Exception e)
//            {
//                Utility.ShowErrorMessage(e);
//            }

//            LoadContent(m_Content, device, m_SpriteBatch);
//        }

//        override public void Dispose()
//        {
//            //Dispose_SelfRender();
//            DisposeRenderData();

//            if (m_LensFlareComponent != null)
//            {
//                m_LensFlareComponent.Dispose();
//                m_LensFlareComponent = null;
//            }

//            if (m_ShadowVolume != null)
//            {
//                m_ShadowVolume.Dispose();
//                m_ShadowVolume = null;
//            }

//            if (m_Emmisive != null)
//            {
//                m_Emmisive.Dispose();
//                m_Emmisive = null;
//            }

//            if (m_MyBloom != null)
//            {
//                m_MyBloom.Dispose();
//                m_MyBloom = null;
//            }

//            if (m_ReflectionMap != null)
//            {
//                m_ReflectionMap.Dispose();
//                m_ReflectionMap = null;
//            }

//            if (m_RefractDistort != null)
//            {
//                m_RefractDistort.Dispose();
//                m_RefractDistort = null;
//            }

//            if (m_GodsRay != null)
//                m_GodsRay.Dispose();
//            m_GodsRay = null;

//            m_PointLightManager.Dispose();
//            m_PointLightManager = null;

//            m_SimpleEffectRenderManager.Dispose();
//            m_SimpleEffectRenderManager = null;

//            m_SkinnedMeshEffectRenderManager.Dispose();
//            m_SkinnedMeshEffectRenderManager = null;

//            m_InstanceMeshEffectRenderManager.Dispose();
//            m_InstanceMeshEffectRenderManager = null;

//            m_GrassEffectRenderManager.Dispose();
//            m_GrassEffectRenderManager = null;

//            m_SpriteBatch.Dispose();
//            m_SpriteBatch = null;
//            m_SpriteFont = null;

//            UnLoadContent();

//            if (m_FullScreenRenderTargetA != null)
//            {
//                //m_NoMSAARenderTarget.Dispose();
//                //m_NoMSAARenderTarget = null;

//                if (m_ShadowMapDepthBuffer != null)
//                {
//                    m_ShadowMapDepthBuffer.Dispose();
//                    m_ShadowMapDepthBuffer = null;
//                }

//                if (m_NoMSAADepthBuffer != null)
//                    m_NoMSAADepthBuffer.Dispose();
//                m_NoMSAADepthBuffer = null;

//                //if (m_OriginalDepthBuffer != null)
//                //    m_OriginalDepthBuffer.Dispose();
//                m_OriginalDepthBuffer = null;

//                m_FullScreenRenderTargetA.Dispose();
//                m_FullScreenRenderTargetA = null;
//                m_FullScreenRenderTargetB.Dispose();
//                m_FullScreenRenderTargetB = null;
//                //m_FullScreenRenderTargetC.Dispose();
//                //m_FullScreenRenderTargetC = null;

//            }

//            m_ResolveTarget.Dispose();
//            m_ResolveTarget = null;

//            Render2DLoaderList_Clear();
//            Render2DLoaderList_ClearReference();

//            if (m_SmallShadowMap != null)
//                m_SmallShadowMap.Dispose();
//            m_SmallShadowMap = null;

//            if (m_BigShadowMap != null)
//                m_BigShadowMap.Dispose();
//            m_BigShadowMap = null;

//            base.Dispose();
//        }

//        protected void SetOriginalDepthBuffer()
//        {
//            m_GraphicDevice.DepthStencilBuffer = m_OriginalDepthBuffer;
//        }

//        protected void SetShadowMapDepthBuffer(int size)
//        {
//            if (m_ShadowMapDepthBuffer == null)
//            {
//                m_ShadowMapDepthBuffer = new DepthStencilBuffer(m_GraphicDevice, size, size, DepthFormat.Depth24Stencil8);
//            }
//            m_GraphicDevice.DepthStencilBuffer = m_ShadowMapDepthBuffer;
//        }

//        protected void SetNoMSAADepthBuffer()
//        {
//            if (m_NoMSAADepthBuffer == null)
//            {
//                m_NoMSAADepthBuffer = new DepthStencilBuffer(m_GraphicDevice, m_OriginalDepthBuffer.Width,
//                    m_OriginalDepthBuffer.Height, m_OriginalDepthBuffer.Format, MultiSampleType.None, 0);
//            }
//            m_GraphicDevice.DepthStencilBuffer = m_NoMSAADepthBuffer;
//        }

//        //public int MotionBlurRTID = -1;
//        //protected void SetRenderMotionBlurStart()
//        //{
//        //    m_MotionBlur.StartRenderSetting();         
//        //}

//        //protected void SetRenderMotionBlurEnd(float PixelBlurConst)
//        //{
//        //    //render blur to B, and keep Z buffer...
//        //    m_GraphicDevice.SetRenderTarget(0, null);
//        //    m_GraphicDevice.SetRenderTarget(1, null);
//        //    m_MotionBlur.MotionBlurDraw(GetRenderTargetATexture(), PixelBlurConst);
//        //}

//        //protected void SetRenderGodsRayStart()
//        //{
//        //    //clear gods ray black texture...
//        //    m_GodsRay.SetStartRenderGodsRaySky();
//        //}

//        //protected void SetRenderGodsRayEnd()
//        //{
//        //    m_GodsRay.SetEndRenderGodsRaySky();
//        //}

//        //protected void SetRenderEmmisiveStart()
//        //{
//        //     //clear Emmisive texture...
//        //    m_Emmisive.SetRenderEmmisiveStart();
//        //}

//        //protected void SetStartRender_MotionBlur_GodsRay_Emmisive()
//        //{
//        //    //繪圖之前一定要做的事
//        //    m_iCountDrawTriangle = 0;
//        //    m_iRenderObjects = 0;

//        //    m_GraphicDevice.SetRenderTarget(0, GetRenderTargetA());
//        //    m_GraphicDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Green, 1.0f, 0);

//        //    //render target0 => scene
//        //    //render target1 => velocity map
//        //    //render target2 => gods ray
//        //    //render target3 => emmisive
//        //    //render scene to A, and velocity map...
//        //    m_GraphicDevice.SetRenderTarget(0, GetRenderTargetA());
//        //    m_GraphicDevice.SetRenderTarget(1, m_MotionBlur.GetCurrentVelocityRenderTarget());
//        //    m_GraphicDevice.SetRenderTarget(2, m_GodsRay.GetBlackRenderTarget());
//        //    m_GraphicDevice.SetRenderTarget(3, m_Emmisive.GetEmmisiveRenderTarget());
//        //}

//        ////-----------------------------------------------------------------------------------------------------
//        //void SetRenderShadowMapRenderState()
//        //{
//        //    m_GraphicDevice.RenderState.CullMode = CullMode.None;

//        //    m_GraphicDevice.RenderState.DepthBufferEnable = true;
//        //    m_GraphicDevice.RenderState.DepthBufferWriteEnable = true;
//        //    m_GraphicDevice.RenderState.AlphaBlendEnable = false;
//        //    m_GraphicDevice.RenderState.AlphaTestEnable = true;
//        //    m_GraphicDevice.RenderState.AlphaFunction = CompareFunction.Greater;
//        //    m_GraphicDevice.RenderState.ReferenceAlpha = 128;
//        //}

//        ////----------------------------------------------------------------------------------------------------------
//        //public void SetCommonRenderState()
//        //{
//        //    SetCommonRenderState(m_GraphicDevice);
//        //}

//        protected void RenderWireFrame(bool bbb)
//        {
//            if (bbb)
//                m_GraphicDevice.RenderState.FillMode = FillMode.WireFrame;
//            else
//                m_GraphicDevice.RenderState.FillMode = FillMode.Solid;
//        }

//        /// <summary>
//        /// 將3D座標轉換，取得2D上的投影點
//        /// </summary>        
//        protected Vector2 Get3DProject2D(ref Vector3 Point3D)
//        {
//            Vector3 pos = m_GraphicDevice.Viewport.Project(Point3D, ProjectionMatrix, ViewMatrix, Matrix.Identity);
//            return new Vector2(pos.X, pos.Y);
//        }

//        protected Microsoft.Xna.Framework.Ray GetCursorRay()
//        {
//            Vector2 Position = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

//            //create 2 positions in screenspace using the cursor position. 0 is as
//            // close as possible to the camera, 1 is as far away as possible.
//            Vector3 nearSource = new Vector3(Position, 0f);
//            Vector3 farSource = new Vector3(Position, 1f);


//            // use Viewport.Unproject to tell what those two screen space positions
//            // would be in world space. we'll need the projection matrix and view
//            // matrix, which we have saved as member variables. We also need a world
//            // matrix, which can just be identity.
//            Vector3 nearPoint = m_GraphicDevice.Viewport.Unproject(nearSource,
//                ProjectionMatrix, ViewMatrix, Matrix.Identity);

//            Vector3 farPoint = m_GraphicDevice.Viewport.Unproject(farSource,
//                ProjectionMatrix, ViewMatrix, Matrix.Identity);

//            // find the direction vector that goes from the nearPoint to the farPoint
//            // and normalize it....
//            Vector3 direction = farPoint - nearPoint;
//            direction.Normalize();

//            // and then create a new ray using nearPoint as the source.
//            return new Ray(nearPoint, direction);
//        }

//        //protected int RenderNode_EmmisiveOnly(NodeArray nodeArray, GraphicsDevice device, bool bAlphaBlending, 
//        //   ref Matrix View, ref Matrix Projection, ref Matrix InverseView, ref uint triangle)
//        //{
//        //    return RenderNode(nodeArray, device, bAlphaBlending, true, ref View, ref Projection, ref InverseView, ref triangle);
//        //}

//        //protected int RenderNode(NodeArray nodeArray, GraphicsDevice device, bool bAlphaBlending, 
//        //   ref Matrix View, ref Matrix Projection, ref Matrix InverseView, ref uint triangle)
//        //{
//        //    return RenderNode(nodeArray, device, bAlphaBlending, false, ref View, ref Projection, ref InverseView, ref triangle);
//        //}

//        protected int RenderNode(NodeArray nodeArray, GraphicsDevice device, bool bAlphaBlending,
//            //  bool bEmmisiveOnly,
//            bool bForceRender,
//           ref Matrix View, ref Matrix Projection, ref Matrix InverseView, ref uint triangle)
//        {
//            if (nodeArray == null)
//                return 0;

//            Matrix? DepthMap = null;
//            Matrix? v = View;
//            Matrix? p = Projection;
//            Matrix? iv = InverseView;

//            bool bIgnoreCullMode = false;
//            return m_NowRenderManager.RenderNode(nodeArray, device, bAlphaBlending/*, bEmmisiveOnly*/,
//                bForceRender, bIgnoreCullMode,
//            DepthMap, v, p, iv, ref triangle);
//        }

//        //protected int RenderNode_AlphaBlending(NodeArray nodeArray, GraphicsDevice device,
//        //    bool bForceRender, bool bEmmisiveOnly,
//        //   ref Matrix View, ref Matrix Projection, ref Matrix InverseView, ref uint triangle)
//        //{
//        //    if (nodeArray == null)
//        //        return 0;

//        //    bool AlphaBlending = true;
//        //    //bool bEmmisiveOnly = false;

//        //    Matrix? DepthMap = null;
//        //    Matrix? v = View;
//        //    Matrix? p = Projection;
//        //    Matrix? iv = InverseView;

//        //    bool bIgnoreCullMode = false;
//        //    return m_NowRenderManager.RenderNode(nodeArray, device, AlphaBlending, bEmmisiveOnly,
//        //      bForceRender, bIgnoreCullMode,
//        //        DepthMap,
//        //   v, p, iv, ref triangle);
//        //}

//        protected int RenderNode_DepthMap(NodeArray nodeArray, GraphicsDevice device,
//            bool bForceRender,
//           ref Matrix View, ref Matrix Projection, ref uint triangle)
//        {
//            if (nodeArray == null)
//                return 0;
//            bool AlphaBlending = false;
//            bool bEmmisiveOnly = false;
//            Matrix? DepthMap = View * Projection;
//            Matrix? v = null;
//            Matrix? p = null;
//            Matrix? iv = null;

//            bool bIgnoreCullMode = true;
//            return m_NowRenderManager.RenderNode(nodeArray, device, AlphaBlending, //bEmmisiveOnly,
//                bForceRender, bIgnoreCullMode,
//                DepthMap,
//           v, p, iv, ref triangle);
//        }

//        #region Point Light Manager
//        PointLightManager m_PointLightManager;

//        /// <summary>
//        /// 計算RenderNodeArray每個與PointLightList的附近點
//        /// 通常靜態場景只需要計算一次就可以了
//        ///車子會動的物件才需要每個frame都計算
//        /// </summary>
//        //void SetPointLightList(NodeArray nodeArray, PointLightNode[] pointLightArray)
//        //{
//        //    //m_PointLightManager.SetPointLightList(nodeArray, pointLightArray);
//        //    m_PointLightManager.RefreshStaticPointLightList(nodeArray, pointLightArray);
//        //}

//        void RenderPointLightDebugLine(PointLightNode[] PointLightList)
//        {
//            if (PointLightList == null)
//                return;

//            for (int a = 0; a < PointLightList.Length; a++)
//            {
//                PointLightNode plnode = PointLightList[a];
//                if (plnode.IfDrawAxis)
//                {
//                    Color ccc = new Color(plnode.LightColor, 128);
//                    DrawLine.AddSphereLine(plnode.WorldMatrix, Vector3.Zero, plnode.RangeValue, ccc, 10, 40);
//                    SceneNode.DrawAxis(plnode.WorldMatrix, plnode.RangeValue);
//                    //DrawLine.AddSphereLine(plnode.WorldMatrix, Vector3.Zero,
//                    //    plnode.RangeValue/2f, plnode.LightColor);
//                }
//                else
//                {
//                    Color ccc = new Color((byte)((float)plnode.LightColor.R * 0.5f),
//                        (byte)((float)plnode.LightColor.G * 0.5f), (byte)((float)plnode.LightColor.B * 0.5f), 64);
//                    DrawLine.AddSphereLine(plnode.WorldMatrix, Vector3.Zero, plnode.RangeValue, ccc, 10, 40);
//                }
//            }
//        }
//        #endregion

//        #region 繪圖node List管理
//        //NodeArray m_RenderSceneNodeArray = new NodeArray(4096);
//        NodeArray m_MeshNodeArray = new NodeArray(4096);
//        NodeArray m_SkinningMeshNodeArray = new NodeArray(2048);
//        NodeArray m_InstanceSimpleMeshNodeArray = new NodeArray(256);
//        NodeArray m_GrassMeshNodeArray = new NodeArray(64);
//        NodeArray m_ShadowVolumeNodeArray = new NodeArray(1024);
//        //NodeArray m_SpotLightNodeArray = new NodeArray(128);
//        CubeMapCreator m_CubeMapCreator = null;

//        //protected TextureCube GetEnvCubeMap()        
//        //{      
//        //    if (m_CubeMapCreator == null) return null;      
//        //    return m_CubeMapCreator.TextureCube;
//        //}
//        protected void SaveEnvCubeMap(string filePath)
//        {
//            if (m_CubeMapCreator == null)
//                return;
//            m_CubeMapCreator.SaveCubeMap(filePath);
//        }

//        //protected NodeArray RenderNodeArray { get { return m_RenderSceneNodeArray; } }
//        //protected NodeArray SpotLightNodeArray { get { return m_SpotLightNodeArray; } }

//        void DisposeRenderData()
//        {
//            //m_RenderSceneNodeArray.Dispose();
//            //m_RenderSceneNodeArray = null;
//            m_ShadowVolumeNodeArray.Dispose();
//            m_ShadowVolumeNodeArray = null;
//            m_MeshNodeArray.Dispose();
//            m_MeshNodeArray = null;
//            m_SkinningMeshNodeArray.Dispose();
//            m_SkinningMeshNodeArray = null;
//            m_InstanceSimpleMeshNodeArray.Dispose();
//            m_InstanceSimpleMeshNodeArray = null;
//            m_GrassMeshNodeArray.Dispose();
//            m_GrassMeshNodeArray = null;
//            //m_SpotLightNodeArray.Dispose();
//            //m_SpotLightNodeArray = null;

//            if (m_CubeMapCreator != null)
//                m_CubeMapCreator.Dispose();
//            m_CubeMapCreator = null;
//        }

//        /// <summary>
//        /// 清除繪出node array
//        /// </summary>
//        protected void ClearRenderNodes()
//        {
//            //m_RenderSceneNodeArray.Reset();
//            m_MeshNodeArray.Reset();
//            m_ShadowVolumeNodeArray.Reset();
//            m_SkinningMeshNodeArray.Reset();
//            m_InstanceSimpleMeshNodeArray.Reset();
//            m_GrassMeshNodeArray.Reset();
//            //m_SpotLightNodeArray.Reset();
//        }

//        protected void SetSpotLight(FrameNode rootNode)
//        {
//            NodeArray spotLightNodeArray = new NodeArray(32);
//            SceneNode.AddSpotLightNode(rootNode, spotLightNodeArray);
//            SetSpotLightArray(spotLightNodeArray);
//        }

//        /// <summary>
//        /// 根據目前的RenderNodeArray，設定meshLoader的點光源資訊給他。
//        /// </summary>
//        /// <param name="meshLoader"></param>
//        //protected void SetPointLight(BasicMeshLoader meshLoader)
//        //{
//        //    //整理所有點光源的資訊
//        //    //PointLightNode[] pointLightArray = new PointLightNode[meshLoader.PointLightList.Length];
//        //    //loadSceneForm.GetScene.PointLightList.CopyTo(pointLightArray, 0);
//        //    //SetPointLightList(m_RenderSceneNodeArray, meshLoader.StaticPointLightList);

//        //    m_PointLightManager.RefreshStaticPointLightList(m_RenderSceneNodeArray, meshLoader.StaticPointLightList);
//        //}

//        /// <summary>
//        /// 根據root  node整理繪圖node資訊。
//        /// </summary>
//        protected void AddRenderNodes(FrameNode rootNode)
//        {
//            //SceneNode.AddMeshNode(rootNode, m_RenderSceneNodeArray);
//            //NodeArray.AddSameType(m_RenderSceneNodeArray, typeof(MeshNode), m_MeshNodeArray);
//            //NodeArray.AddSameType(m_RenderSceneNodeArray, typeof(ShadowVolumeNode), m_ShadowVolumeNodeArray);
//            //NodeArray.AddSameType(m_RenderSceneNodeArray, typeof(SkinningMeshNode), m_SkinningMeshNodeArray);
//            //NodeArray.AddSameType(m_RenderSceneNodeArray, typeof(InstanceSimpleMeshNode), m_InstanceSimpleMeshNodeArray);
//            SceneNode.AddNodeSameType(
//                rootNode, m_MeshNodeArray, typeof(MeshNode));
//            SceneNode.AddNodeSameType(
//                rootNode, m_ShadowVolumeNodeArray, typeof(ShadowVolumeNode));
//            SceneNode.AddNodeSameType(
//                rootNode, m_SkinningMeshNodeArray, typeof(SkinningMeshNode));
//            SceneNode.AddNodeSameType(
//                rootNode, m_InstanceSimpleMeshNodeArray, typeof(InstanceSimpleMeshNode));
//            SceneNode.AddNodeSameType(
//                rootNode, m_GrassMeshNodeArray, typeof(GrassNode));
//        }

//        protected void ClearTargetAB_Emm_GodsRay_SetNull()
//        {
//            SetRenderTarget(0, GetRenderTargetB());
//            ClearTarget_All(Color.TransparentBlack);
//            SetRenderTarget(0, GetRenderTargetA());
//            ClearTarget_All(Color.TransparentBlack);

//            SetRenderTarget_Emmisive(0);
//            ClearTarget_All(Color.TransparentBlack);

//            //SetRenderTarget_GodsRay(0);
//            //ClearTarget_All(Color.Black);

//            SetRenderTarget(0, null);
//            ClearTarget_All(Color.CornflowerBlue);
//        }

//        //protected void ClearTargetAB_Emm_GodsRay_SetA()
//        //{
//        //    SetRenderTarget(0, GetRenderTargetB());
//        //    ClearTarget_All(Color.TransparentBlack);
//        //    //SetRenderTarget(0, GetRenderTargetC());
//        //    //ClearTarget_All(Color.TransparentBlack);
//        //    SetRenderTarget_Emmisive(0);
//        //    ClearTarget_All(Color.TransparentBlack);

//        //    SetRenderTarget_GodsRay(0);
//        //    ClearTarget_All(Color.Black);

//        //    SetRenderTarget(0, GetRenderTargetA());
//        //    ClearTarget_All(Color.CornflowerBlue);
//        //}

//        /// <summary>
//        /// 將mesh loader的particle和特效畫出來
//        /// </summary>      
//        protected void RenderSpecialEffect(BasicMeshLoader meshLoader, ref Matrix view, ref Matrix projection, ref Matrix invView)
//        {
//            if (meshLoader == null)
//                return;
//            SetAlphaBlendRenderState(m_GraphicDevice);
//            meshLoader.EffectObjectList_Render(ref view, ref invView, ref projection, m_GraphicDevice, m_Content,
//                DirectionLight1, DirectionLight1A, DirectionLight1D, DirectionLight1S,
//                FogColor, FogStart, FogEnd, m_CubeMapCreator);

//            SetAlphaBlendRenderState(m_GraphicDevice);

//            Vector3 viewPosition = invView.Translation;
//            meshLoader.RenderParticle(ref view, ref projection, ref viewPosition);
//        }

//        protected void RenderAllMesh_EmmisiveOnly(ref Matrix view, ref Matrix projection, ref Matrix invView,
//            bool bAlphaBlending, bool bForceRender)
//        {
            
//            //bool bEmmisiveOnly = true;
//            RenderAllMesh(ref view, ref projection, ref invView, bAlphaBlending, bForceRender
//                /*, bEmmisiveOnly*/, SceneNode.RenderTechnique.EmmisiveOnly);
//        }

//        //protected void RenderAllMesh(ref Matrix view, ref Matrix projection, ref Matrix invView,
//        //    bool bAlphaBlending, bool bForceRender, SceneNode.RenderTechnique renderTechnoque)
//        //{
//        //    bool bEmmisiveOnly = false;
//        //    RenderAllMesh(ref view, ref projection, ref invView, bAlphaBlending, bForceRender,
//        //        bEmmisiveOnly, renderTechnoque);
//        //}

//        //void RenderAllMesh_SimplePass(ref Matrix view, ref Matrix projection, ref Matrix invView,
//        //    bool bAlphaBlending, bool bForceRender, bool bEmmisiveOnly)
//        //{
//        //    RenderAllMesh(ref view, ref projection, ref invView, bAlphaBlending, bForceRender, bEmmisiveOnly,
//        //        SceneNode.RenderTechnique.None);
//        //        //SceneNode.RenderTechnique.EmmisiveMap | SceneNode.RenderTechnique.GodsRayMap);
//        //}

//        //void RenderAllMesh(ref Matrix view, ref Matrix projection, ref Matrix invView,
//        //    bool bAlphaBlending, bool bForceRender, bool bEmmisiveOnly)
//        //{
//        //    RenderAllMesh(ref view, ref projection, ref invView, bAlphaBlending, bForceRender, bEmmisiveOnly,
//        //        //SceneNode.RenderTechnique.None);
//        //    SceneNode.RenderTechnique.EmmisiveMap | SceneNode.RenderTechnique.GodsRayMap);
//        //}

//        int RenderAllMesh(ref Matrix view, ref Matrix projection, ref Matrix invView,
//            bool bAlphaBlending, bool bForceRender/*, bool bEmmisiveOnly*/, SceneNode.RenderTechnique renderTechnique)
//        {
//            int renderCount = 0;

//            RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleMesh | renderTechnique);
//            renderCount +=
//                RenderNode(m_MeshNodeArray, m_GraphicDevice, bAlphaBlending/*, bEmmisiveOnly*/, bForceRender,
//                 ref view, ref projection, ref invView, ref RenderTriangles);
//            RenderTechniqueEnd();

//            RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleTengentMesh | renderTechnique);
//            renderCount +=
//                RenderNode(m_MeshNodeArray, m_GraphicDevice, bAlphaBlending/*, bEmmisiveOnly*/, bForceRender,
//                 ref view, ref projection, ref invView, ref RenderTriangles);
//            RenderTechniqueEnd();

//            RenderTechniqueBegin(SceneNode.RenderTechnique.SkinnedMesh | renderTechnique);
//            renderCount +=
//                RenderNode(m_SkinningMeshNodeArray, m_GraphicDevice, bAlphaBlending/*, bEmmisiveOnly*/, bForceRender,
//                 ref view, ref projection, ref invView, ref RenderTriangles);
//            RenderTechniqueEnd();

//            RenderTechniqueBegin(SceneNode.RenderTechnique.SkinnedTengentMesh | renderTechnique);
//            renderCount +=
//                RenderNode(m_SkinningMeshNodeArray, m_GraphicDevice, bAlphaBlending/*, bEmmisiveOnly*/, bForceRender,
//                 ref view, ref projection, ref invView, ref RenderTriangles);
//            RenderTechniqueEnd();

//            RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleInstanceMesh | renderTechnique);
//            renderCount +=
//                RenderNode(m_InstanceSimpleMeshNodeArray, m_GraphicDevice, bAlphaBlending/*, bEmmisiveOnly*/, bForceRender,
//                 ref view, ref projection, ref invView, ref RenderTriangles);
//            RenderTechniqueEnd();

//            RenderTechniqueBegin(SceneNode.RenderTechnique.GrassMesh | renderTechnique);
//            renderCount +=
//                RenderNode(m_GrassMeshNodeArray, m_GraphicDevice, bAlphaBlending/*, bEmmisiveOnly*/, bForceRender,
//                 ref view, ref projection, ref invView, ref RenderTriangles);
//            RenderTechniqueEnd();

//            //畫shadow volume--------------------------------------------------------------------------------------------------
//            //RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleMesh | SceneNode.RenderTechnique.ShadowVolume);
//            //RenderNode(m_ShadowVolumeNodeArray, m_GraphicDevice, bAlphaBlending, bEmmisiveOnly, bForceRender,
//            //     ref view, ref projection, ref invView, ref RenderTriangles);
//            //RenderTechniqueEnd();
//            ////RenderShadowVolumeScreenSquare();

//            //RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleTengentMesh | SceneNode.RenderTechnique.ShadowVolume);
//            //RenderNode(m_ShadowVolumeNodeArray, m_GraphicDevice, bAlphaBlending, bEmmisiveOnly, bForceRender,
//            //     ref view, ref projection, ref invView, ref RenderTriangles);
//            //RenderTechniqueEnd();
//            //RenderShadowVolumeScreenSquare();
//            //--------------------------------------------------------------------------------------------------------------------

//            return renderCount;
//        }

//        //protected TextureCube RenderStupidCubeMap()
//        //{
//        //    if (FinalScreenTexture == null)
//        //        return Utility.GetDefaultTextureCube(m_Content.ServiceProvider);
//        //    if (m_CubeMapCreator == null)
//        //        m_CubeMapCreator = new CubeMapCreator(m_GraphicDevice, 512);

//        //    m_CubeMapCreator.SetRenderTarget_1Face(m_GraphicDevice, 0, Vector3.Zero);
//        //    RenderTextureFullScreen(FinalScreenTexture);

//        //    m_CubeMapCreator.SetRenderTarget_1Face(m_GraphicDevice, 0, Vector3.Zero);
//        //    RenderTextureFullScreen(FinalScreenTexture);

//        //    m_CubeMapCreator.SetRenderTarget_1Face(m_GraphicDevice, 0, Vector3.Zero);
//        //    RenderTextureFullScreen(FinalScreenTexture);

//        //    m_CubeMapCreator.SetRenderTarget_1Face(m_GraphicDevice, 0, Vector3.Zero);
//        //    RenderTextureFullScreen(FinalScreenTexture);

//        //    m_CubeMapCreator.SetRenderTarget_1Face(m_GraphicDevice, 0, Vector3.Zero);
//        //    RenderTextureFullScreen(FinalScreenTexture);

//        //    m_CubeMapCreator.SetRenderTarget_1Face(m_GraphicDevice, 0, Vector3.Zero);
//        //    RenderTextureFullScreen(FinalScreenTexture);

//        //    SetRenderTarget(0, null);

//        //    return m_CubeMapCreator.TextureCube;
//        //}


//        protected TextureCube RenderCubeMap()
//        {
//            if (m_CubeMapCreator == null)
//                m_CubeMapCreator = new CubeMapCreator(m_GraphicDevice, 512);

//            Matrix viewEnv =
//              m_CubeMapCreator.SetRenderTarget_1Face(m_GraphicDevice, 0, InverseViewMatrix.Translation);
//            Matrix invViewEnv = Matrix.Invert(viewEnv);
//            float aspectRatio = (float)m_GraphicDevice.Viewport.Width / (float)m_GraphicDevice.Viewport.Height;
//            Matrix projEnv = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(90), aspectRatio, 1f, 5000000f);
//            ClearTarget_All(Color.Black);

//            RenderAllMesh(ref viewEnv, ref projEnv, ref invViewEnv, false, true, SceneNode.RenderTechnique.None);

//            SetRenderTarget(0, null);

//            return m_CubeMapCreator.TextureCube;
//        }

//        protected TextureCube RenderCubeMapEmmisive()
//        {
//            if (m_CubeMapCreator == null)
//                m_CubeMapCreator = new CubeMapCreator(m_GraphicDevice, 512);

//            Matrix viewEnv =
//              m_CubeMapCreator.SetRenderTarget_1Face(m_GraphicDevice, 0, InverseViewMatrix.Translation);
//            Matrix invViewEnv = Matrix.Invert(viewEnv);
//            float aspectRatio = (float)m_GraphicDevice.Viewport.Width / (float)m_GraphicDevice.Viewport.Height;
//            Matrix projEnv = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(90), aspectRatio, 1f, 5000000f);
//            ClearTarget_All(Color.Black);

//            RenderAllMesh_EmmisiveOnly(ref viewEnv, ref projEnv, ref invViewEnv, false, true);

//            SetRenderTarget(0, null);

//            return m_CubeMapCreator.TextureCube;
//        }

//        protected void RenderRefractMap()
//        {
//            RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleMesh | SceneNode.RenderTechnique.NormalRefractMap);
//            RenderNode(m_MeshNodeArray, m_GraphicDevice, false,/*, false*/ true,
//                 ref ViewMatrix, ref ProjectionMatrix, ref InverseViewMatrix, ref RenderTriangles);
//            RenderTechniqueEnd();

//            RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleTengentMesh | SceneNode.RenderTechnique.NormalRefractMap);
//            RenderNode(m_MeshNodeArray, m_GraphicDevice, false, /*, false*/ true,
//                 ref ViewMatrix, ref ProjectionMatrix, ref InverseViewMatrix, ref RenderTriangles);
//            RenderTechniqueEnd();
//        }

//        /// <summary>
//        /// 處理前處理繪圖，計算depth map。
//        /// </summary>
//        void ProcessPreRender(BasicMeshLoader meshLoader, bool bForceRender)
//        {
//            //SetDirectionalLight1(loadSceneForm.LightAmbientGet(),
//            //    loadSceneForm.LightDiffuseGet(),
//            //    loadSceneForm.LightSpecularGet(),
//            //    loadSceneForm.LightDirectionGet());
//            //SetFog(loadSceneForm.FogStartGet(), loadSceneForm.FogEndGet()
//            //    , loadSceneForm.FogColorGet());
//            //SetSpotLightArray(spotLightNodeArray);

//            //SetOriginalDepthBuffer();
//            SetNoMSAADepthBuffer();
//            SetCommonRenderState(m_GraphicDevice);
//            SetCullNone(m_GraphicDevice);

//            Matrix? pViewMat;
//            Matrix viewMat;
//            Matrix? pProjMat;
//            Matrix projMat;
//            NodeArray renderNodeArray;
//            while (meshLoader.EffectObject_SetRT_PreRenderDepthMap(m_GraphicDevice,
//                out pViewMat, out pProjMat, out renderNodeArray))
//            {
//                if (pViewMat.HasValue && pProjMat.HasValue)
//                {
//                    viewMat = pViewMat.Value;
//                    projMat = pProjMat.Value;

//                    RenderAllMeshDepth(ref viewMat, ref projMat, bForceRender, renderNodeArray);
//                }
//            }
//            SetCullCCW(m_GraphicDevice);
//            renderNodeArray = null;

//            SetRenderTarget(0, null);

//            //if (meshLoader != null && meshLoader.GetType() == typeof(SceneMeshLoader))
//            //    renderSceneNodeArray = ((SceneMeshLoader)loadSceneForm.GetScene).GetAllMeshNode;
//            //while (meshLoader.EffectObject_SetRT_PreRenderCubeMap1Face(m_GraphicDevice, out pViewMat))
//            //{
//            //    if (pViewMat != null)
//            //    {
//            //        //ENV                    
//            //        //Matrix viewEnv =
//            //        ClearTarget(Color.Green);
//            //        viewMat = pViewMat.Value;
//            //        float aspectRatio = (float)m_GraphicDevice.Viewport.Width / (float)m_GraphicDevice.Viewport.Height;
//            //        projMat = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(90), aspectRatio, 1f, 5000000f);

//            //        RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleMesh);
//            //        RenderNode(renderSceneNodeArray, m_GraphicDevice, ref viewMat, ref projMat, ref InverseView, ref RenderTriangles);
//            //        RenderTechniqueEnd();
//            //        RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleTengentMesh);
//            //        RenderNode(renderSceneNodeArray, m_GraphicDevice, ref viewMat, ref projMat, ref InverseView, ref RenderTriangles);
//            //        RenderTechniqueEnd();
//            //        //ENV
//            //    }
//            //}
//        }

//        void ProcessPreRenderShadow(BasicMeshLoader meshLoader, bool bForceRender, int shadowMapSize)
//        {
//            SetShadowMapDepthBuffer(shadowMapSize);
//            RenderLoaderShadowMap(meshLoader, shadowMapSize, bForceRender);
//        }

//        protected void RenderAllMeshDepth(ref Matrix view, ref Matrix projection, bool bForceRender, NodeArray renderNodeArray)
//        {
//            RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleMesh | SceneNode.RenderTechnique.DepthMap);
//            RenderNode_DepthMap(renderNodeArray, m_GraphicDevice, bForceRender, ref view, ref projection, ref RenderTriangles);
//            RenderTechniqueEnd();

//            RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleTengentMesh | SceneNode.RenderTechnique.DepthMap);
//            RenderNode_DepthMap(renderNodeArray, m_GraphicDevice, bForceRender, ref view, ref projection, ref RenderTriangles);
//            RenderTechniqueEnd();


//            RenderTechniqueBegin(SceneNode.RenderTechnique.SkinnedMesh | SceneNode.RenderTechnique.DepthMap);
//            RenderNode_DepthMap(renderNodeArray, m_GraphicDevice, bForceRender, ref view, ref projection, ref RenderTriangles);
//            RenderTechniqueEnd();

//            RenderTechniqueBegin(SceneNode.RenderTechnique.SkinnedTengentMesh | SceneNode.RenderTechnique.DepthMap);
//            RenderNode_DepthMap(renderNodeArray, m_GraphicDevice, bForceRender, ref view, ref projection, ref RenderTriangles);
//            RenderTechniqueEnd();

//            RenderTechniqueBegin(SceneNode.RenderTechnique.SimpleInstanceMesh |
//                SceneNode.RenderTechnique.DepthMap);
//            RenderNode_DepthMap(renderNodeArray, m_GraphicDevice, bForceRender, ref view, ref projection, ref RenderTriangles);
//            RenderTechniqueEnd();
//        }

//        StringBuilder m_StringBuilder = new StringBuilder(2048);
//        public string BasicDebugString()
//        {
//#if DrawDebug
//            // string debugString = 
//            //     "ElapsedGameTime:  " + GetTime.GetGameTime().ElapsedGameTime.Milliseconds.ToString() + "    ";
//            //debugString += "ElapsedRealTime:  " + GetTime.GetGameTime().ElapsedRealTime.Milliseconds.ToString() + "\n";
//            //debugString += 

//            m_StringBuilder.Remove(0, m_StringBuilder.Length);
//            m_StringBuilder.AppendFormat("MeshNodeArray : {0}\n", m_MeshNodeArray.nowNodeAmount.ToString());
//            m_StringBuilder.AppendFormat("SkinningMeshNodeArray : {0}\n", m_SkinningMeshNodeArray.nowNodeAmount.ToString());
//            m_StringBuilder.AppendFormat("InstanceSimpleMeshNodeArray : {0}\n", m_InstanceSimpleMeshNodeArray.nowNodeAmount.ToString());
//            m_StringBuilder.AppendFormat("GrassMeshNodeArray : {0}\n", m_GrassMeshNodeArray.nowNodeAmount.ToString());
//            m_StringBuilder.AppendFormat("ShadowVolumeNodeArray : {0}\n", m_ShadowVolumeNodeArray.nowNodeAmount.ToString());
//            m_StringBuilder.AppendLine("\n");
//            m_StringBuilder.AppendLine("\n");
//            m_StringBuilder.AppendLine("----------Object Release Check----------\n");
//            m_StringBuilder.AppendLine(ReleaseCheck.GetInformationString());
//            return m_StringBuilder.ToString();
//            //return debugString;
//#else
//            return null;
//#endif
//        }

//        //public enum SceneSection
//        //{            
//        //    section1 = 1,section2,section3,section4,section5,section6,section7,section8,section9,section10,
//        //    section11,section12,section13,section14,section15,section16,section17,section18,section19,section20,
//        //}

//        MyObjectContainer<BasicMeshLoader>[] RenderSectionListArray = new MyObjectContainer<BasicMeshLoader>[64];
//        MyObjectContainer<BasicMeshLoader> CheckListFromRenderSectionListArray(BasicMeshLoader sectionKey)
//        {
//            for (int a = 0; a < RenderSectionListArray.Length; a++)
//            {
//                if (RenderSectionListArray[a] != null && RenderSectionListArray[a].NowUsedAmount > 0)
//                {
//                    BasicMeshLoader loader = RenderSectionListArray[a].GetObjectArrayValue(0) as BasicMeshLoader;
//                    if (loader == sectionKey)
//                        return RenderSectionListArray[a];
//                }
//            }
//            return null;
//        }

//        //bool AddRenderSectionListArray(MyObjectContainer container)
//        //{
//        //    for (int a = 0; a < RenderSectionListArray.Length; a++)
//        //    {
//        //        if(RenderSectionListArray[a] == null)
//        //        {
//        //            RenderSectionListArray[a] = container;
//        //            return true;
//        //        }
//        //    }
//        //    //OutputBox.SwitchOnOff = true;
//        //    OutputBox.ShowMessage("AddRenderSectionListArray full...");
//        //    return false;
//        //}

//        MyObjectContainer<BasicMeshLoader> SeekNullRenderSectionContainer()
//        {
//            //要一個空的
//            for (int a = 0; a < RenderSectionListArray.Length; a++)
//            {
//                if (RenderSectionListArray[a] != null && RenderSectionListArray[a].NowUsedAmount == 0)
//                    return RenderSectionListArray[a];
//            }

//            //或是建一個新的把他加到陣列裡。
//            MyObjectContainer<BasicMeshLoader> container = new MyObjectContainer<BasicMeshLoader>(2048);
//            for (int a = 0; a < RenderSectionListArray.Length; a++)
//            {
//                if (RenderSectionListArray[a] == null)
//                {
//                    RenderSectionListArray[a] = container;
//                    return container;
//                }
//            }
//            return null;
//        }

//        public void AddRenderSectionListArray(BasicMeshLoader sectionKey, BasicMeshLoader component)
//        {
//            MyObjectContainer<BasicMeshLoader> container = CheckListFromRenderSectionListArray(sectionKey);
//            if (container == null)
//            {
//                container = SeekNullRenderSectionContainer();//要一個空的            
//            }

//            container.AddObject(component);
//        }

//        public void RemoveRenderSection(BasicMeshLoader sectionKey)
//        {
//            RemovePhyXViewFrustumCullingLoader(sectionKey);

//            MyObjectContainer<BasicMeshLoader> container = CheckListFromRenderSectionListArray(sectionKey);
//            if (container != null)
//                container.Clear();
//        }

//        public void EmptyAllRenderSectionListArray()
//        {
//            for (int a = 0; a < RenderSectionListArray.Length; a++)
//            {
//                if (RenderSectionListArray[a] != null)
//                    RenderSectionListArray[a].Clear();
//            }
//        }


//        protected void PreRenderEffectInPipeline()
//        {
//            for (int a = 0; a < RenderSectionListArray.Length; a++)
//            {
//                MyObjectContainer<BasicMeshLoader> container = RenderSectionListArray[a];
//                if (container != null && container.NowUsedAmount > 0)
//                {
//                    BasicMeshLoader loader = container.GetObjectArrayValue(0) as BasicMeshLoader;
//                    ProcessPreRender(loader, true);
//                }
//            }
//        }

//        /// <summary>
//        /// 跟據RenderSectionListArray計算出影子範圍，使用shadowMapSize的MAP來繪製深度圖。
//        /// </summary>
//        /// <param name="shadowMapSize"></param>
//        protected void PreRenderShadowInPipeline(int shadowMapSize)
//        {
//            for (int a = 0; a < RenderSectionListArray.Length; a++)
//            {
//                MyObjectContainer<BasicMeshLoader> container = RenderSectionListArray[a];
//                if (container != null && container.NowUsedAmount > 0)
//                {
//                    BasicMeshLoader loader = container.GetObjectArrayValue(0) as BasicMeshLoader;
//                    ProcessPreRenderShadow(loader, true, shadowMapSize);
//                }
//            }
//        }

//        //protected void RenderInPipeline_NotAlpha_Simple(TextureCube envMap)
//        //{
//        //    bool bForceRender = true;
//        //    if (IfCullingScene)
//        //        bForceRender = false;

//        //    SetOriginalDepthBuffer();
//        //    SetRenderTarget(0, null);
//        //    ClearTarget_All(Color.Lavender);
//        //    SetCommonRenderState(m_GraphicDevice);

//        //    for (int a = 0; a < RenderSectionListArray.Length; a++)
//        //    {
//        //        MyObjectContainer<BasicMeshLoader> container = RenderSectionListArray[a];
//        //        if (container != null && container.NowUsedAmount > 0)
//        //        {
//        //            BasicMeshLoader bigSection = container.GetObjectArrayValue(0) as BasicMeshLoader;
//        //            Array ranerArray = RenderSectionListArray[a].GetObjectArray();

//        //            SetMeshLoaderEffectData(bigSection, ranerArray, envMap);
//        //            RenderAllMesh(ref ViewMatrix, ref ProjectionMatrix, ref InverseViewMatrix, bForceRender);
//        //        }
//        //    }

//        //    //畫鏡面耀斑
//        //    RenderLensFlare();

//        //    SetRenderTarget(0, null);
//        //    SetRenderTarget(1, null);
//        //    SetRenderTarget(2, null);
//        //}

//        protected void RenderInPipline_Clear()
//        {
//            if (Utility.IfAntialiazing)
//            {
//                m_GraphicDevice.RenderState.MultiSampleAntiAlias = true;
//                SetOriginalDepthBuffer();

//                //SetRenderTarget_Emmisive(0);
//                //ClearTarget_All(Color.TransparentBlack);

//                SetRenderTarget(0, null);
//                ClearTarget_All(Color.CornflowerBlue);
//            }
//            else
//            {
//                m_GraphicDevice.RenderState.MultiSampleAntiAlias = false;
//                SetOriginalDepthBuffer();
//                ClearTargetAB_Emm_GodsRay_SetNull();
//            }
//        }

//        SceneNode.RenderTechnique m_NowRenderTechnique;
//        //protected int RenderInPipeline_NotAlpha(TextureCube envMap)
//        //{
//        //    if (Utility.IfAntialiazing)
//        //    {
//        //        m_NowRenderTechnique = SceneNode.RenderTechnique.None;
//        //    }
//        //    else
//        //    {
//        //        m_NowRenderTechnique =
//        //            SceneNode.RenderTechnique.EmmisiveMap |
//        //            SceneNode.RenderTechnique.GodsRayMap;
//        //        SetRenderTarget_Emmisive(1);
//        //        SetRenderTarget_GodsRay(2);
//        //    }

//        //    int renderCount = 0;

//        //    bool bForceRender = true;
//        //    if (IfCullingScene)
//        //        bForceRender = false;

//        //    SetCommonRenderState(m_GraphicDevice);

//        //    for (int a = 0; a < RenderSectionListArray.Length; a++)
//        //    {
//        //        MyObjectContainer<BasicMeshLoader> container = RenderSectionListArray[a];
//        //        if (container != null && container.NowUsedAmount > 0)
//        //        {
//        //            BasicMeshLoader bigSection = container.GetObjectArray()[0];
//        //            SetMeshLoaderEffectData(bigSection, RenderSectionListArray[a], envMap);
//        //            //RenderAllMesh_EmmisiveOnly(
//        //            //    ref ViewMatrix, ref ProjectionMatrix, ref InverseViewMatrix, false, bForceRender);
//        //            renderCount += RenderAllMesh(ref ViewMatrix, ref ProjectionMatrix, ref InverseViewMatrix, false,
//        //                bForceRender, m_NowRenderTechnique);
//        //        }
//        //    }

//        //    //畫鏡面耀斑
//        //    RenderLensFlare();
//        //    return renderCount;
//        //}

//        void RenderInPipeline_EmmisiveOnly()
//        {
//            bool bForceRender = true;
//            if (IfCullingScene)
//                bForceRender = false;

//            //若是反鋸齒情況畫出EMM圖，深度緩衝無法重複使用所以必須全都畫出
//            SetRenderTarget_Emmisive(0);
//            ClearTarget_All(Color.TransparentBlack);

//            SetCommonRenderState(m_GraphicDevice);

//            SetMeshLoaderEmmisiveData();
//            RenderAllMesh_EmmisiveOnly(
//                ref ViewMatrix, ref ProjectionMatrix, ref InverseViewMatrix, false, bForceRender);

//            SetAlphaBlendRenderState(m_GraphicDevice);
//            RenderAllMesh_EmmisiveOnly(
//                ref ViewMatrix, ref ProjectionMatrix, ref InverseViewMatrix, true, bForceRender);
//        }

//        //protected int RenderInPipeline_Alpha(TextureCube envMap)
//        //{
//        //    //if (Utility.IfAntialiazing)
//        //    //{
//        //    //    m_GraphicDevice.RenderState.MultiSampleAntiAlias = true;
//        //    //    SetOriginalDepthBuffer();
//        //    //    SetRenderTarget(0, null);
//        //    //}
//        //    //else
//        //    //{
//        //    //    m_GraphicDevice.RenderState.MultiSampleAntiAlias = false;
//        //    //    SetNoMSAADepthBuffer();
//        //    //    SetRenderTarget(0, GetNoMSAARenderTarget());
//        //    //}

//        //    int renderCount = 0;

//        //    bool bForceRender = true;
//        //    if (IfCullingScene)
//        //        bForceRender = false;

//        //    //畫半透明 & particle
//        //    for (int a = 0; a < RenderSectionListArray.Length; a++)
//        //    {
//        //        MyObjectContainer<BasicMeshLoader> container = RenderSectionListArray[a];
//        //        if (container != null && container.NowUsedAmount > 0)
//        //        {
//        //            SetAlphaBlendRenderState(m_GraphicDevice);
//        //            BasicMeshLoader bigSection = container.GetObjectArray()[0];
//        //            SetMeshLoaderEffectData(bigSection, RenderSectionListArray[a], envMap);
//        //            renderCount +=
//        //                RenderAllMesh(ref ViewMatrix, ref ProjectionMatrix, ref InverseViewMatrix, true, bForceRender,
//        //                m_NowRenderTechnique);

//        //            //個別畫特效
//        //            for (int b = 0; b < RenderSectionListArray[a].NowUsedAmount; b++)
//        //            {
//        //                RenderSpecialEffect(RenderSectionListArray[a].GetObjectArray()[b],
//        //                    ref ViewMatrix, ref ProjectionMatrix, ref InverseViewMatrix);
//        //            }
//        //        }
//        //    }
//        //    return renderCount;
//        //}

//        protected void RenderInPipeline_Simple()
//        {
//            SetRenderTarget(0, null);
//            SetRenderTarget(1, null);
//            SetRenderTarget(2, null);
//            SetRenderTarget(3, null);

//            //RenderTextureFullScreen(FinalScreenTexture);

//            // 利用平行投影畫出2D介面上的物件            
//            //Render2DLoaderList_Draw();
//        }


//        protected Texture2D RenderInPipeline_PostProcessing(bool bEmmisive)
//        {
//            Texture2D FinalScreenTexture = ResolveBackBufferTexture();

//            Texture2D emmMap = null;
//            if (Utility.IfAntialiazing)
//            {
//                if (bEmmisive)
//                {
//                    //畫emmisive圖
//                    SetOriginalDepthBuffer();
//                    RenderInPipeline_EmmisiveOnly();
//                }

//                //畫post processing把反鋸齒關掉
//                m_GraphicDevice.RenderState.MultiSampleAntiAlias = false;
//                SetNoMSAADepthBuffer();

//                if (bEmmisive)
//                {
//                    SetRenderTarget(0, null);
//                    emmMap = GetEmmisiveTexture();
//                    RenderBloom2(emmMap, GetRenderTargetB());

//                    SetRenderTarget(0, GetRenderTargetA());
//                    BlendTextureToScene(GetRenderTargetBTexture(), FinalScreenTexture);//GetRenderTargetATexture());
//                    SetRenderTarget(0, null);
//                    FinalScreenTexture = GetRenderTargetATexture();
//                }

//                //Glow
//                RenderBloom2(FinalScreenTexture, GetRenderTargetA());
//                SetRenderTarget(0, null);
//                FinalScreenTexture = GetRenderTargetATexture();

//                //畫完post processing再把反鋸齒打開
//                m_GraphicDevice.RenderState.MultiSampleAntiAlias = true;
//                SetOriginalDepthBuffer();

//                RenderTextureFullScreen(FinalScreenTexture);

//                // 利用平行投影畫出2D介面上的物件            
//                Render2DLoaderList_Draw();
//                return emmMap;
//            }


//            SetRenderTarget(0, null);
//            SetRenderTarget(1, null);
//            SetRenderTarget(2, null);
//            SetRenderTarget(3, null);

//            ////畫gods ray scatter處理
//            //if (loadSceneForm.GetScene != null && loadSceneForm.GetScene.GodsRayLightNodes != null)
//            //{
//            //    RenderGodsRayScatter(sceneTexture, loadSceneForm.GetScene.GodsRayLightNodes);
//            //    sceneTexture = ResolveBackBufferTexture();
//            //}
//            //foreach (BasicMeshLoader loader in m_OtherScene)
//            //{
//            //    if (loader.GodsRayLightNodes != null)
//            //    {
//            //        RenderGodsRayScatter(sceneTexture, loader.GodsRayLightNodes);
//            //        sceneTexture = ResolveBackBufferTexture();
//            //    }
//            //}

//            //畫折射物件
//            SetRenderTarget(0, GetRenderTargetA());
//            SetRenderTarget(1, null);
//            SetRenderTarget(2, null);

//            RenderRefractMap();

//            SetRenderTarget(0, null);
//            SetRenderTarget(1, null);

//            //畫emmisive圖
//            emmMap = GetEmmisiveTexture();
//            RenderBloom2(emmMap, GetRenderTargetB());

//            SetRenderTarget(0, null);
//            BlendTextureToScene(GetRenderTargetBTexture(), FinalScreenTexture);//GetRenderTargetATexture());
//            FinalScreenTexture = ResolveBackBufferTexture();// GetRenderTargetDTexture();

//            //Glow
//            RenderBloom2(FinalScreenTexture, null);
//            FinalScreenTexture = ResolveBackBufferTexture();

//            //畫折射效果
//            SetRenderTarget(0, GetRenderTargetB());
//            SetRenderTarget(1, null);
//            RenderWithDistortMap(GetRenderTargetATexture(), FinalScreenTexture);

//            SetRenderTarget(0, null);
//            FinalScreenTexture = GetRenderTargetBTexture();
//            RenderTextureFullScreen(FinalScreenTexture);

//            // 利用平行投影畫出2D介面上的物件            
//            Render2DLoaderList_Draw();
//            return emmMap;
//        }

//        /// <summary>
//        /// 設定emmisive map的繪出設定，不需打光與計算霧，也不需設定影子
//        /// emmisive不分section設定全加一起畫了
//        /// </summary>
//        protected void SetMeshLoaderEmmisiveData()
//        {
//            ClearRenderNodes();

//            //emmisive全加一起畫了
//            for (int a = 0; a < RenderSectionListArray.Length; a++)
//            {
//                MyObjectContainer<BasicMeshLoader> sceneComponent = RenderSectionListArray[a];
//                if (sceneComponent != null && sceneComponent.NowUsedAmount > 0)
//                {
//                    for (int b = 0; b < sceneComponent.NowUsedAmount; b++)
//                    {
//                        BasicMeshLoader loader = sceneComponent.GetObjectArray()[b];
//                        AddRenderNodes(loader.GetRootNode);
//                        SceneNode.SetAllEnvCubeMap(loader.GetRootNode, null);
//                    }
//                }
//            }
//        }

//        //protected void SetMeshLoaderEffectData(BasicMeshLoader bigSection,
//        //    MyObjectContainer<BasicMeshLoader> sceneComponent, TextureCube envMap)
//        //{
//        //    ClearRenderNodes();

//        //    for (int a = 0; a < sceneComponent.NowUsedAmount; a++)
//        //    {
//        //        BasicMeshLoader loader = sceneComponent.GetObjectArray()[a];
//        //        AddRenderNodes(loader.GetRootNode);
//        //        SceneNode.SetAllEnvCubeMap(loader.GetRootNode, envMap);
//        //    }

//        //    //光的資訊要自己設定更新
//        //    m_GraphicDevice.SamplerStates[0].MipMapLevelOfDetailBias = bigSection.MipMapLevel;
//        //    SetDirectionalLight1(bigSection.LightAmbient, bigSection.LightDiffuse, bigSection.LightSpecular, bigSection.LightDirection);
//        //    SetFog(bigSection.FogStart, bigSection.FogEnd, bigSection.FogMaxRatio, bigSection.FogColor);

//        //    SetSpotLight(bigSection.GetRootNode);
//        //    //SetPointLight(bigSection);

//        //    Texture2D depthMap = bigSection.GetDepthMap();
//        //    if (depthMap != null)
//        //        SetBigShadowMap(bigSection.GetDepthMapFrustum(), depthMap, bigSection.BigShadowMapSampleLength);
//        //    else
//        //        SetBigShadowMap(Matrix.Identity, null, 0);
//        //}

//        //   protected void SetMeshLoaderEffectData(BasicMeshLoader bigSection, List<BasicMeshLoader> sceneComponent,
//        ////        Vector3 lightAmbient, Vector3 lightDiffuse, Vector3 lightSpecular, Vector3 lightDirection,
//        //  //      float fogStart, float fogEnd, Vector3 fogColor, 
//        //       TextureCube envMap)
//        //    //   ref Matrix BigShadowFrustum, Texture2D BigShadowMap)
//        //   {
//        //       ClearRenderNodes();

//        //       //AddRenderNodes(bigSection.GetRootNode);
//        //       //SceneNode.SetAllEnvCubeMap(bigSection.GetRootNode, envMap);

//        //       List<BasicMeshLoader>.Enumerator itr = sceneComponent.GetEnumerator();
//        //       while (itr.MoveNext())
//        //       {
//        //           AddRenderNodes(itr.Current.GetRootNode);
//        //           SceneNode.SetAllEnvCubeMap(itr.Current.GetRootNode, envMap);
//        //       }

//        //       //光的資訊要自己設定更新
//        //       SetDirectionalLight1(bigSection.LightAmbient, bigSection.LightDiffuse, bigSection.LightSpecular, bigSection.LightDirection);
//        //       SetFog(bigSection.FogStart, bigSection.FogEnd, bigSection.FogColor);

//        //       SetSpotLight(bigSection.GetRootNode);
//        //       SetPointLight(bigSection);

//        //       Texture2D depthMap = bigSection.GetDepthMap();
//        //       if (depthMap != null)
//        //           SetBigShadowMap(bigSection.GetDepthMapFrustum(), depthMap);
//        //       else
//        //           SetBigShadowMap(Matrix.Identity, null);
//        //   }
//        #endregion

//        #region 2D介面投影
//        /// <summary>
//        /// 平行投影矩陣
//        /// </summary>
//        protected Matrix OrthographicProjection;
//        protected Matrix OrthographicView, InvOrthographicView;
//        List<BasicMeshLoader> m_Render2DLoaderList;
//        List<BasicMeshLoader> m_Render2DLoaderListReference;
//        protected List<BasicMeshLoader> Render2DLoaderList { get { return m_Render2DLoaderList; } }

//        ///// <summary>
//        ///// 利用平行投影畫出物件
//        ///// </summary>
//        //protected void RenderOrthographicAllMesh()
//        //{
//        //    RenderAllMesh(ref OrthographicView, ref OrthographicProjection, ref InvOrthographicView);
//        //}

//        ///// <summary>
//        ///// 利用平行投影畫出特效
//        ///// </summary>
//        //protected void RenderOrthographicSpecialEffect(BasicMeshLoader loader)
//        //{
//        //    RenderSpecialEffect(loader, ref OrthographicView, ref OrthographicProjection, ref InvOrthographicView);       
//        //}

//        public void Render2DLoaderList_AddReference(BasicMeshLoader loader, bool LightEnable)
//        {
//            if (m_Render2DLoaderListReference == null)
//                m_Render2DLoaderListReference = new List<BasicMeshLoader>();
//            m_Render2DLoaderListReference.Add(loader);

//            SceneNode.SetAllLightEnable(loader.GetRootNode, LightEnable);
//        }

//        public void Render2DLoaderList_ClearReference()
//        {
//            if (m_Render2DLoaderListReference != null)
//                m_Render2DLoaderListReference.Clear();
//            m_Render2DLoaderListReference = null;
//        }

//        protected BasicMeshLoader Render2DLoaderList_Add(string loaderXML, bool LightEnable)
//        {
//            if (m_Render2DLoaderList == null)
//                m_Render2DLoaderList = new List<BasicMeshLoader>();
//            SpecialMeshDataManager.TatalSpecialMeshData total = null;
//            AnimationTableDataArray aniTable;
//            BasicMeshLoader basicLoader =
//                SpecialMeshDataManager.LoadSceneFromSpecialXML(
//                                        loaderXML, m_Content, m_GraphicDevice, out total, out aniTable);
//            Render2DLoaderList.Add(basicLoader);

//            SceneNode.SetAllLightEnable(basicLoader.GetRootNode, LightEnable);
//            return basicLoader;
//        }

//        /// <summary>
//        /// 使用平行投影畫出Render2DLoaderList
//        /// </summary>
//        protected void Render2DLoaderList_Draw()
//        {
//            ClearRenderNodes();

//            //畫模型資訊
//            if (Render2DLoaderList != null)
//            {
//                List<BasicMeshLoader>.Enumerator itr = Render2DLoaderList.GetEnumerator();
//                while (itr.MoveNext())
//                {
//                    itr.Current.Update(I_GetTime.ellipseUpdateMillisecond, m_Content, m_GraphicDevice, m_MySound, ref InvOrthographicView);
//                    AddRenderNodes(itr.Current.GetRootNode);
//                }
//                itr.Dispose();
//            }

//            if (m_Render2DLoaderListReference != null)
//            {
//                List<BasicMeshLoader>.Enumerator itr = m_Render2DLoaderListReference.GetEnumerator();
//                while (itr.MoveNext())
//                {
//                    itr.Current.Update(I_GetTime.ellipseUpdateMillisecond, m_Content, m_GraphicDevice, m_MySound, ref InvOrthographicView);
//                    AddRenderNodes(itr.Current.GetRootNode);
//                }
//                itr.Dispose();
//            }

//            SetCommonRenderState(m_GraphicDevice);
//            RenderAllMesh(ref OrthographicView, ref OrthographicProjection, ref InvOrthographicView, false, true,
//                SceneNode.RenderTechnique.EmmisiveMap);

//            //畫特效
//            SetAlphaBlendRenderState(m_GraphicDevice);
//            RenderAllMesh(ref OrthographicView, ref OrthographicProjection, ref InvOrthographicView, true, true,
//                SceneNode.RenderTechnique.EmmisiveMap);

//            if (Render2DLoaderList != null)
//            {
//                List<BasicMeshLoader>.Enumerator itr = Render2DLoaderList.GetEnumerator();
//                while (itr.MoveNext())
//                    RenderSpecialEffect(itr.Current, ref OrthographicView, ref OrthographicProjection, ref InvOrthographicView);
//                itr.Dispose();
//            }

//            if (m_Render2DLoaderListReference != null)
//            {
//                List<BasicMeshLoader>.Enumerator itr = m_Render2DLoaderListReference.GetEnumerator();
//                while (itr.MoveNext())
//                    RenderSpecialEffect(itr.Current, ref OrthographicView, ref OrthographicProjection, ref InvOrthographicView);
//                itr.Dispose();
//            }
//        }

//        protected void Render2DLoaderList_Clear()
//        {
//            if (Render2DLoaderList != null)
//            {
//                foreach (BasicMeshLoader loader in Render2DLoaderList)
//                    loader.Dispose();
//                m_Render2DLoaderList.Clear();
//            }
//            m_Render2DLoaderList = null;
//        }
//        #endregion

//        #region Big & Small Shadow Map
//        NodeArray RenderShadowNodeArray = new NodeArray(2048);
//        protected void RenderLoaderShadowMap(BasicMeshLoader loader, int mapSize, bool bForceRender)
//        {
//            RenderShadowNodeArray.Reset();
//            MeshNode.AddHasMeshUVNode(loader.GetRootNode, RenderShadowNodeArray, true);

//            Matrix? depthV, depthP;
//            Matrix v, p;
//            loader.RenderDepthMapBegin(mapSize, 0, m_GraphicDevice, RenderShadowNodeArray, ref loader.LightDirection, out depthV, out depthP);
//            if (depthV.HasValue && depthP.HasValue)
//            {
//                v = depthV.Value;
//                p = depthP.Value;

//                SetCommonRenderState(m_GraphicDevice);
//                SetCullNone(m_GraphicDevice);

//                RenderAllMeshDepth(ref v, ref p, bForceRender, RenderShadowNodeArray);

//                SetCullCCW(m_GraphicDevice);

//                SetRenderTarget(0, null);
//            }
//            loader.RenderDepthMapEnd();
//        }

//        //Vector3[] BigShadowPoint = null;

//        ///// <summary>
//        ///// Big shadow map常用於前處理畫全場景深度資訊用。適合全場景的靜態物件使用，動態物件不適合。
//        ///// </summary>
//        //protected void RenderBigShadowMap(NodeArray nodeArray)
//        //{
//        //    if (nodeArray == null)
//        //    {
//        //        SetBigShadowMap(Matrix.Identity, null);
//        //        return;
//        //    }

//        //    if (m_BigShadowMap == null)
//        //    {
//        //        m_BigShadowMap = DepthMapCreator.Create();
//        //        m_BigShadowMap.Initialize(4096, DirectionLight1, m_GraphicDevice);
//        //        //BigShadowPoint = new Vector3[8];
//        //    }

//        //    m_BigShadowMap.SetBoundBoxNodeArray(nodeArray);
//        //    //RefreshShadowRange(BigShadowPoint, range);

//        //    Matrix? depthV, depthP;
//        //    Matrix v, p;
//        //    //m_BigShadowMap.SetRenderTarget(0, m_GraphicDevice, DirectionLight1, BigShadowPoint, 1, out depthV, out depthP);
//        //    m_BigShadowMap.SetRenderTarget(0, m_GraphicDevice, DirectionLight1, out depthV, out depthP);
//        //    if (depthV.HasValue && depthP.HasValue)
//        //    {
//        //        v = depthV.Value;
//        //        p = depthP.Value;

//        //        SetCommonRenderState(m_GraphicDevice);
//        //        SetCullNone(m_GraphicDevice);

//        //        RenderAllMeshDepth(ref v, ref p, nodeArray);

//        //        SetCullCCW(m_GraphicDevice);

//        //        SetRenderTarget(0, null);
//        //        SetBigShadowMap(m_BigShadowMap.DepthMapFrustum, m_BigShadowMap.GetDepthMap);
//        //    }

//        //    m_BigShadowMap.ReleaseBoundBoxNodeArray();
//        //}

//        //protected Texture2D GetBigShadowMap()
//        //{
//        //    if (m_BigShadowMap != null)
//        //        return m_BigShadowMap.GetDepthMap;
//        //    return null;
//        //}

//        void RefreshViewShadowRange(Vector3[] ShadowPoint, float range)
//        {
//            Vector3 forward = ViewForwardXZ;
//            Vector3 ppp = InverseViewMatrix.Translation + forward * range;
//            Vector3 right = Vector3.Cross(Vector3.Up, forward);
//            right.Normalize();
//            range = range + range * 0.5f;
//            ShadowPoint[0] = new Vector3(ppp.X, ppp.Y, ppp.Z) + right * range + Vector3.Up * range + forward * range;
//            ShadowPoint[1] = new Vector3(ppp.X, ppp.Y, ppp.Z) - right * range + Vector3.Up * range + forward * range;
//            ShadowPoint[2] = new Vector3(ppp.X, ppp.Y, ppp.Z) + right * range - Vector3.Up * range + forward * range;
//            ShadowPoint[3] = new Vector3(ppp.X, ppp.Y, ppp.Z) - right * range - Vector3.Up * range + forward * range;

//            ShadowPoint[4] = new Vector3(ppp.X, ppp.Y, ppp.Z) + right * range + Vector3.Up * range - forward * range;
//            ShadowPoint[5] = new Vector3(ppp.X, ppp.Y, ppp.Z) - right * range + Vector3.Up * range - forward * range;
//            ShadowPoint[6] = new Vector3(ppp.X, ppp.Y, ppp.Z) + right * range - Vector3.Up * range - forward * range;
//            ShadowPoint[7] = new Vector3(ppp.X, ppp.Y, ppp.Z) - right * range - Vector3.Up * range - forward * range;
//        }

//        Vector3[] SmallShadowPoint = null;

//        /// <summary>
//        /// Small shadow map常用於局部區域動態物件使用，隨時做render更新，
//        /// 物件可判斷是否位於影子frustum內再決定是否要繪出(目前未實做判斷frustum culling)。
//        /// </summary>
//        protected void RenderSmallShadowMap(float range, float boxDepthScale, bool bForceRender,
//            NodeArray nodeArray, int size, float shadowSampleLength)
//        {
//            //if (m_ShadowMapDepthBuffer == null)
//            //{
//            //    m_ShadowMapDepthBuffer = new DepthStencilBuffer(m_GraphicDevice, size, size, DepthFormat.Depth24Stencil8);
//            //}

//            //int size = m_ShadowMapDepthBuffer.Width;

//            if (m_SmallShadowMap == null)
//            {
//                m_SmallShadowMap = DepthMapCreator.Create();
//                m_SmallShadowMap.Initialize_SetRenderData(size, DirectionLight1, m_GraphicDevice);
//                SmallShadowPoint = new Vector3[8];
//            }

//            SetShadowMapDepthBuffer(size);

//            RefreshViewShadowRange(SmallShadowPoint, range);

//            Matrix? depthV, depthP;
//            Matrix v, p;
//            m_SmallShadowMap.SetRenderTarget(0, m_GraphicDevice, DirectionLight1, SmallShadowPoint, boxDepthScale,
//                out depthV, out depthP);
//            if (depthV.HasValue && depthP.HasValue)
//            {
//                v = depthV.Value;
//                p = depthP.Value;

//                SetCommonRenderState(m_GraphicDevice);
//                SetCullNone(m_GraphicDevice);

//                RenderAllMeshDepth(ref v, ref p, bForceRender, nodeArray);

//                SetCullCCW(m_GraphicDevice);

//                SetRenderTarget(0, null);
//                SetSmallShadowMap(m_SmallShadowMap.DepthMapFrustum, m_SmallShadowMap.GetDepthMap);
//            }
//        }

//        protected Texture2D GetSmallShadowMap()
//        {
//            if (m_SmallShadowMap != null)
//                return m_SmallShadowMap.GetDepthMap;
//            return null;
//        }
//        #endregion



//        //BasicEffect m_SelfRender_VertexPositionColorEffect = null;
//        //VertexDeclaration m_SelfRender_VertexPositionColorDesc = null;
//        //void Dispose_SelfRender()
//        //{
//        //    if (m_SelfRender_VertexPositionColorEffect != null)
//        //        m_SelfRender_VertexPositionColorEffect.Dispose();
//        //    m_SelfRender_VertexPositionColorEffect = null;

//        //    if (m_SelfRender_VertexPositionColorDesc != null)
//        //        m_SelfRender_VertexPositionColorDesc.Dispose();
//        //    m_SelfRender_VertexPositionColorDesc = null;
//        //}

//        ///// <summary>
//        ///// 設定自己想畫出的線條effect begin。
//        ///// </summary>
//        //protected void SelfRenderLine_VertexPositionColor(Matrix view, Matrix projection, VertexPositionColor[] LineVertices)
//        //{
//        //    //檢查要初始化的
//        //    if (m_SelfRender_VertexPositionColorEffect == null)
//        //    {
//        //        m_SelfRender_VertexPositionColorEffect = new BasicEffect(m_GraphicDevice, null)
//        //        {
//        //            VertexColorEnabled = true
//        //        };

//        //        m_SelfRender_VertexPositionColorDesc =
//        //            new VertexDeclaration(m_GraphicDevice, VertexPositionColor.VertexElements);
//        //    }

//        //    //開始畫
//        //    m_GraphicDevice.VertexDeclaration = m_SelfRender_VertexPositionColorDesc;
//        //    m_SelfRender_VertexPositionColorEffect.World = Matrix.Identity;
//        //    m_SelfRender_VertexPositionColorEffect.View = view;
//        //    m_SelfRender_VertexPositionColorEffect.Projection = projection;

//        //    m_SelfRender_VertexPositionColorEffect.Begin();

//        //    foreach (EffectPass pass in m_SelfRender_VertexPositionColorEffect.CurrentTechnique.Passes)
//        //    {
//        //        pass.Begin();
//        //        m_GraphicDevice.DrawUserPrimitives<VertexPositionColor>(
//        //                PrimitiveType.LineList, LineVertices, 0, LineVertices.Length/2);
//        //        pass.End();
//        //    }

//        //    m_SelfRender_VertexPositionColorEffect.End();

//        //    m_GraphicDevice.VertexDeclaration = null;
//        //}

//        //一個filter可加入許多Loader
//        class CullingLoaderData
//        {
//            public CullingLoaderData()
//            {
//                LoaderList = new List<BasicMeshLoader>();
//                MeshNodeCulling = false;
//                InstanceNodeCulling = false;
//                SkinningNodeCulling = false;
//            }
//            public List<BasicMeshLoader> LoaderList;
//            public bool MeshNodeCulling;//說明此filter已做MeshNodeCulling
//            public bool InstanceNodeCulling;//說明此filter已做InstanceNodeCulling
//            public bool SkinningNodeCulling;//說明此filter已做SkinningNodeCulling
//        }
//        Dictionary<MyPhyX.FilterMask, CullingLoaderData> m_LoaderCullingData = new Dictionary<MyPhyX.FilterMask, CullingLoaderData>(128);

//        bool IfCullingScene
//        {
//            get
//            {
//                if (m_LoaderCullingData == null || m_LoaderCullingData.Count == 0)
//                    return false;
//                return true;
//            }
//        }

//        NodeArray CullingInstanceNodeArray = new NodeArray(64);
//        protected int ProcessViewCrustumCulling(MyPhyX.FilterMask filter, StillDesign.PhysX.Scene scene)
//        {
//            int afterCullingObject = 0;

//            if (IfCullingScene == false)
//                return afterCullingObject;

//            //把所有場景 不管要不要偵測 全都關了
//            List<BasicMeshLoader>.Enumerator loaderItr;
//            Dictionary<MyPhyX.FilterMask, CullingLoaderData>.Enumerator cullingDataItr = m_LoaderCullingData.GetEnumerator();
//            while (cullingDataItr.MoveNext())
//            {
//                bool bMeshNodeCulling = cullingDataItr.Current.Value.MeshNodeCulling;
//                bool bInstanceNodeCulling = cullingDataItr.Current.Value.InstanceNodeCulling;
//                bool bSkinningNodeCulling = cullingDataItr.Current.Value.SkinningNodeCulling;
//                loaderItr = cullingDataItr.Current.Value.LoaderList.GetEnumerator();
//                while (loaderItr.MoveNext())
//                {
//                    if (bMeshNodeCulling)
//                        SceneNode.SetAllBeCulled(loaderItr.Current.GetRootNode, typeof(MeshNode));
//                    if (bInstanceNodeCulling)
//                        SceneNode.SetAllBeCulled(loaderItr.Current.GetRootNode, typeof(InstanceSimpleMeshNode));
//                    if (bSkinningNodeCulling)
//                        SceneNode.SetAllBeCulled(loaderItr.Current.GetRootNode, typeof(SkinningMeshNode));
//                }
//            }

//            //把Instance Node清空
//            CullingInstanceNodeArray.Reset();
//            loaderItr = m_LoaderCullingData[filter].LoaderList.GetEnumerator();
//            while (loaderItr.MoveNext())
//            {
//                SceneNode.AddNodeSameType(
//                    loaderItr.Current.GetRootNode, CullingInstanceNodeArray, typeof(InstanceSimpleMeshNode));
//            }
//            for (int a = 0; a < CullingInstanceNodeArray.nowNodeAmount; a++)
//                ((InstanceSimpleMeshNode)CullingInstanceNodeArray.nodeArray[a]).RefreshInstanceMatrix_Clear();

//            //culling 偵測，只開filter有match的。
//            StillDesign.PhysX.Shape[] shapes = MyPhyX.ConsiderViewFrustumCulling(
//                scene, StillDesign.PhysX.ShapesType.All, filter, ref ViewFrustrum);

//            for (int a = 0; a < shapes.Length; a++)
//            {
//                if (shapes[a].Actor.UserData.GetType() == typeof(MeshNode))
//                {
//                    MeshNode meshNode = shapes[a].Actor.UserData as MeshNode;
//                    meshNode.BeCulled = false;
//                }
//                else if (shapes[a].Actor.UserData.GetType() == typeof(InstanceCullingData))
//                {
//                    InstanceCullingData insdata = shapes[a].Actor.UserData as InstanceCullingData;
//                    insdata.m_InstanceSimpleMeshNode.RefreshInstanceMatrix_Add(
//                        insdata.m_LocatorNode.WorldMatrix);
//                    insdata.m_InstanceSimpleMeshNode.BeCulled = false;
//                }
//                else if (shapes[a].Actor.UserData.GetType() == typeof(SkinningCullingData))
//                {
//                    SkinningCullingData skinData = shapes[a].Actor.UserData as SkinningCullingData;
//                    skinData.m_SkinningMeshNode.BeCulled = false;
//                }
//            }

//            //把Instance Noe設定進去
//            for (int a = 0; a < CullingInstanceNodeArray.nowNodeAmount; a++)
//            {
//                ((InstanceSimpleMeshNode)CullingInstanceNodeArray.nodeArray[a]).RefreshInstanceMatrix_SetRenderArray();
//            }

//            return shapes.Length;
//        }

//        MyObjectContainer<Matrix> ViewFrustumCullingInstanceNode = new MyObjectContainer<Matrix>(2048);
//        NodeArray ViewFrustumCullingNodeArray = new NodeArray(2048);
//        /// <summary>
//        /// 將有建立OBB的loader儲存起來，在render all mesh前把他們全都culled = true，
//        /// 然後再view culling把有範圍內的都設定成culled = false，
//        /// 沒有經過此function設定的，就不存在list內，不做視野裁切。
//        /// </summary>     
//        protected void CreatePhyXViewFrustumCulling_MeshNode(
//            StillDesign.PhysX.Scene scene, BasicMeshLoader loader, MyPhyX.FilterMask filterMask)
//        {
//            CullingLoaderData cullData;
//            if (m_LoaderCullingData.TryGetValue(filterMask, out cullData) == false)
//            {
//                //加入loader 的culling 索引
//                cullData = new CullingLoaderData();
//                m_LoaderCullingData.Add(filterMask, cullData);
//            }

//            if (cullData.LoaderList.IndexOf(loader) >= 0)
//            {
//                ////OutputBox.SwitchOnOff = true;
//                //OutputBox.ShowMessage("CreatePhyXViewFrustumCulling_MeshNode 已經加過loader了");
//            }
//            else
//                cullData.LoaderList.Add(loader);
//            cullData.MeshNodeCulling = true;

//            ViewFrustumCullingNodeArray.Reset();
//            SceneNode.AddNodeSameType(loader.GetRootNode, ViewFrustumCullingNodeArray,
//                typeof(MeshNode), true, true);//沒貼圖不用建

//            RSTMatrix rstMat = RSTMatrix.Init();
//            for (int a = 0; a < ViewFrustumCullingNodeArray.nowNodeAmount; a++)
//            {
//                MeshNode meshNode = ViewFrustumCullingNodeArray.nodeArray[a] as MeshNode;

//                rstMat.SetData(meshNode.WorldMatrix);
//                StillDesign.PhysX.Actor actor =
//                    MyPhyX.PhyXCreateBoxActor(scene, meshNode.BoundingBox, rstMat.m_Scale,
//                    10, filterMask);

//                //初始化要這樣設定才會對，用其他的都沒用。
//                actor.GlobalOrientationQuat = rstMat.m_RTMatrix.m_Quaternion;
//                actor.GlobalPosition = rstMat.m_RTMatrix.m_Translation;

//                actor.BodyFlags.Frozen = true;
//                actor.BodyFlags.DisableGravity = true;
//                actor.BodyFlags.Kinematic = false;
//                actor.ActorFlags.DisableCollision = true;
//                actor.Sleep();

//                actor.UserData = meshNode;

//                ViewFrustumCullingActorArray viewFrustumCullingActorArray = new ViewFrustumCullingActorArray(1);
//                viewFrustumCullingActorArray.Add(actor);

//                meshNode.ViewCullingObject = viewFrustumCullingActorArray;
//                //meshNode.SetLocalMatrixCallback = ViewCullingMeshNode.SetLocalMatrixDone;
//            }
//        }

//        protected void CreatePhyXViewFrustumCulling_SkinningNode(
//            StillDesign.PhysX.Scene scene, BasicMeshLoader loader, MyPhyX.FilterMask filterMask)
//        {
//            CullingLoaderData cullData;
//            if (m_LoaderCullingData.TryGetValue(filterMask, out cullData) == false)
//            {
//                //加入loader 的culling 索引
//                cullData = new CullingLoaderData();
//                m_LoaderCullingData.Add(filterMask, cullData);
//            }

//            if (cullData.LoaderList.IndexOf(loader) >= 0)
//            {
//                ////OutputBox.SwitchOnOff = true;
//                //OutputBox.ShowMessage("CreatePhyXViewFrustumCulling_MeshNode 已經加過loader了");
//            }
//            else
//                cullData.LoaderList.Add(loader);
//            cullData.SkinningNodeCulling = true;//說明此filter已做InstanceNodeCulling

//            ViewFrustumCullingNodeArray.Reset();
//            SceneNode.AddNodeSameType(loader.GetRootNode, ViewFrustumCullingNodeArray,
//                typeof(SkinningMeshNode), true, true);//沒貼圖不用建


//            for (int a = 0; a < ViewFrustumCullingNodeArray.nowNodeAmount; a++)
//            {
//                SkinningMeshNode skinningNode = ViewFrustumCullingNodeArray.nodeArray[a] as SkinningMeshNode;

//                //檢查是否有Bound box資訊設定檔 *_bnd.xml
//                StringBuilder strbuilder = new StringBuilder(128);
//                strbuilder.AppendFormat("{0}_col.xml", skinningNode.ModelFileName);
//                string boundFileName = strbuilder.ToString();

//                if (System.IO.File.Exists(boundFileName))
//                {
//                    //建立骨架碰撞BOX給skinning mesh，找出該有BOX的NODE。
//                    skinningNode.m_CollSetting = (CollisionEditor.CollisionSetting)Utility.LoadXMLFile(boundFileName, typeof(CollisionEditor.CollisionSetting), false);
//                    skinningNode.m_CollBoneNodes = new LocatorNode[skinningNode.m_CollSetting.Box.Length];
//                    foreach (CollisionEditor.CollisionBox box in skinningNode.m_CollSetting.Box)
//                    {
//                        LocatorNode boneNode =
//                            loader.GetRootNode.SeekFirstNodeContainName(box.Name) as LocatorNode;
//                        skinningNode.m_CollBoneNodes[a] = boneNode;
//                    }
//                }
//                else
//                    continue;

//                //針對碰撞box把他建立BOX並綁到骨架上
//                ViewFrustumCullingActorArray viewFrustumCullingActorArray = new
//                    ViewFrustumCullingActorArray(skinningNode.m_CollBoneNodes.Length);
//                RSTMatrix rstMat = RSTMatrix.Init();
//                BoundingBox boundBox;
//                //回圈增加
//                for (int b = 0; b < skinningNode.m_CollBoneNodes.Length; b++)
//                {
//                    LocatorNode boneNode = skinningNode.m_CollBoneNodes[b];

//                    CollisionEditor.CollisionBox colBox = skinningNode.m_CollSetting.Box[b];
//                    rstMat.SetData(boneNode.WorldMatrix);
//                    boundBox.Min = colBox.BoxMin;
//                    boundBox.Max = colBox.BoxMax;

//                    StillDesign.PhysX.Actor actor =
//                        MyPhyX.PhyXCreateBoxActor(scene, boundBox, Vector3.One/* rstMat.m_Scale*/, 10, filterMask);

//                    //初始化要這樣設定才會對，用其他的都沒用。
//                    actor.GlobalOrientationQuat = rstMat.m_RTMatrix.m_Quaternion;
//                    actor.GlobalPosition = rstMat.m_RTMatrix.m_Translation;

//                    actor.BodyFlags.Frozen = true;
//                    actor.BodyFlags.DisableGravity = true;
//                    actor.BodyFlags.Kinematic = false;
//                    actor.ActorFlags.DisableCollision = true;
//                    actor.Sleep();

//                    SkinningCullingData skndata = new SkinningCullingData(skinningNode);
//                    actor.UserData = skndata;
//                    viewFrustumCullingActorArray.Add(actor);
//                }

//                skinningNode.ViewCullingObject = viewFrustumCullingActorArray;
//            }
//        }

//        class InstanceCullingData
//        {
//            public InstanceCullingData(InstanceSimpleMeshNode instanceSimpleMeshNode,
//                FrameNode locatorNode)
//            {
//                m_InstanceSimpleMeshNode = instanceSimpleMeshNode;
//                m_LocatorNode = locatorNode;
//            }
//            public InstanceSimpleMeshNode m_InstanceSimpleMeshNode;
//            public FrameNode m_LocatorNode;
//        }

//        public class SkinningCullingData
//        {
//            public SkinningCullingData(
//                SkinningMeshNode skinningMeshNode)
//            {
//                //m_RecBoxArrayID = boxArrayID;
//                m_SkinningMeshNode = skinningMeshNode;
//                //m_BoneNode = locatorNode;
//                //m_BoxActor = boxActor;
//            }
//            public SkinningMeshNode m_SkinningMeshNode;

//            //public int m_RecBoxArrayID = -1;
//            //public FrameNode m_BoneNode;
//            //public StillDesign.PhysX.Actor m_BoxActor;
//        }

//        protected void CreatePhyXViewFrustumCulling_InstanceNode(
//            StillDesign.PhysX.Scene scene, BasicMeshLoader loader, MyPhyX.FilterMask filterMask)
//        {
//            #region 將loader根據filter加入管理字典-----------------------------------------------------------
//            CullingLoaderData cullData;
//            if (m_LoaderCullingData.TryGetValue(filterMask, out cullData) == false)
//            {
//                //加入loader 的culling 索引
//                cullData = new CullingLoaderData();
//                m_LoaderCullingData.Add(filterMask, cullData);
//            }

//            if (cullData.LoaderList.IndexOf(loader) >= 0)
//            {
//                ////OutputBox.SwitchOnOff = true;
//                //OutputBox.ShowMessage("CreatePhyXViewFrustumCulling_InstanceNode 已經加過loader了");
//            }
//            else
//                cullData.LoaderList.Add(loader);
//            #endregion----------------------------------------------------------------------------------------------------------------------

//            cullData.InstanceNodeCulling = true;//說明此filter已做InstanceNodeCulling

//            ViewFrustumCullingNodeArray.Reset();
//            SceneNode.AddNodeSameType(loader.GetRootNode, ViewFrustumCullingNodeArray,
//                typeof(InstanceSimpleMeshNode), true, true);//沒貼圖不用建，instance動態模型會把keyframe mesh隱藏也不用建。

//            //NodeArray instanceNodeArray = new NodeArray(2048);
//            for (int a = 0; a < ViewFrustumCullingNodeArray.nowNodeAmount; a++)
//            {
//                InstanceSimpleMeshNode instanceNode = ViewFrustumCullingNodeArray.nodeArray[a] as InstanceSimpleMeshNode;

//                //instanceNodeArray.Reset();
//                //instanceNode.AddAllChildNode(instanceNodeArray);
//                FrameNode[] referenceNodeArray = instanceNode.ReferenceNodeArray;

//                //Vector3[] size = new Vector3[1];
//                //Vector3[] pos = new Vector3[1];

//                //回圈增加
//                ViewFrustumCullingActorArray viewFrustumCullingActorArray = new
//                    ViewFrustumCullingActorArray(referenceNodeArray.Length);
//                RSTMatrix rstMat = RSTMatrix.Init();
//                for (int b = 0; b < referenceNodeArray.Length; b++)
//                {
//                    FrameNode locatorNode = referenceNodeArray[b] as FrameNode;
//                    rstMat.SetData(locatorNode.WorldMatrix);
//                    StillDesign.PhysX.Actor actor =
//                        MyPhyX.PhyXCreateBoxActor(scene, instanceNode.BoundingBox, rstMat.m_Scale,
//                        10, filterMask);

//                    //初始化要這樣設定才會對，用其他的都沒用。
//                    actor.GlobalOrientationQuat = rstMat.m_RTMatrix.m_Quaternion;
//                    actor.GlobalPosition = rstMat.m_RTMatrix.m_Translation;

//                    actor.BodyFlags.Frozen = true;
//                    actor.BodyFlags.DisableGravity = true;
//                    actor.BodyFlags.Kinematic = false;
//                    actor.ActorFlags.DisableCollision = true;
//                    actor.Sleep();

//                    InstanceCullingData insdata = new InstanceCullingData(instanceNode, locatorNode);
//                    actor.UserData = insdata;
//                    viewFrustumCullingActorArray.Add(actor);
//                }

//                instanceNode.ViewCullingObject = viewFrustumCullingActorArray;
//            }
//        }



//        void RemovePhyXViewFrustumCullingLoader(BasicMeshLoader loader)
//        {
//            //由所有場景filter中移除
//            Dictionary<MyPhyX.FilterMask, CullingLoaderData>.Enumerator cullingDataItr = m_LoaderCullingData.GetEnumerator();
//            while (cullingDataItr.MoveNext())
//            {
//                if (cullingDataItr.Current.Value.LoaderList.IndexOf(loader) >= 0)
//                    cullingDataItr.Current.Value.LoaderList.Remove(loader);
//            }
//        }
//    }


//    public class ReflectData
//    {
//        public Texture2D ReflectMap;
//        public Matrix ReflectView;
//    }




//}
