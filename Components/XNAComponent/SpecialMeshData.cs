﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.Xml.Serialization;
using System.IO;
using MeshDataDefine;

using I_XNAUtility;


namespace I_XNAComponent
{
    //處理模型檔裡面的mesh，會有的特殊屬性。

    public partial class SpecialMeshDataManager
    {
        public SpecialMeshDataManager() { }

        [Serializable]
        public class MOPData
        {
            /// <summary>
            /// 此function會幫我產生multi object player 的node並attach在其對應的parent node下，
            /// 並回傳multi object player陣列，使用者可視情況去呼叫update()。
            /// </summary>       
            public static bool CreateMultiObjectPlayer(SpecialMeshDataManager.MOPData mopData, LocatorNode sceneRootNode, ContentManager content, GraphicsDevice device,
               out LocatorNode locatorListRootNode, out I_XNAComponent.MultiObjectPlayer[] mopArray)
            {
                locatorListRootNode = null;
                mopArray = null;
                if (mopData.MOPName == null || mopData.KeyName == null || mopData.KeyName.Length == 0)
                {
                    return false;
                }

                #region 依據傳入MutiObjectPlayer資料建立定位點list，並attach在正確的node下。
                int objLength = 0;
                //沒有locator list表示從scene中尋找唯一定位點。
                if (string.IsNullOrEmpty(mopData.LocatorFileName) == true &&
                    mopData.KeyName.Contains("Matrix.Identity") == false)
                {
                    //至少生一個child，好用來連結等等生出來的東西。
                    locatorListRootNode = LocatorList.CreateSimpleLocatorNodeList(mopData.MOPName);
                    objLength = 1;

                    //把他attach在特定點上
                    FrameNode seekNode = SceneNode.SeekFirstMeshNodeAllName(sceneRootNode, mopData.KeyName);

                    //如果mesh node找不到，改不要限制MeshNode看看。
                    if (seekNode == null)
                        seekNode = sceneRootNode.SeekFirstNodeAllName(ref mopData.KeyName) as FrameNode;
                    if (seekNode == null)
                    {
                        //OutputBox.SwitchOnOff = true;
                        OutputBox.ShowMessage("CreateMultiObjectPlayer(...)   ==> seekNode == NULL");
                        return false;
                    }

                    seekNode.AttachChild(locatorListRootNode);
                }
                else if (mopData.KeyName.Contains("Matrix.Identity"))//場景裡唯一定位點為Identity
                {
                    //至少生一個child，好用來連結等等生出來的東西。
                    locatorListRootNode = LocatorList.CreateSimpleLocatorNodeList(mopData.MOPName);
                    locatorListRootNode.LocalMatrix = Matrix.Identity;
                    objLength = 1;
                    sceneRootNode.AttachChild(locatorListRootNode);
                }
                else
                {
                    LocatorList locatorList =
                        Utility.LoadXMLFile(Utility.GetFullPath(mopData.LocatorFileName), typeof(LocatorList), false) as LocatorList;
                    if (locatorList == null)
                        return false;
                    locatorListRootNode = locatorList.CreateLocatorNodeList(mopData.MOPName);
                    objLength = locatorList.GlobalPoses.Length;
                    sceneRootNode.AttachChild(locatorListRootNode);
                }

                mopArray = new MultiObjectPlayer[objLength];
                for (int a = 0; a < objLength; a++)
                {
                    //時間在MOP裡面為分母 最小要是1
                    int forwardTime = (int)MathHelper.Clamp(
                        mopData.playForwardTime + (int)(mopData.playSpeedRandom * (float)MyMath.GetRand()),
                        1, int.MaxValue);

                    int backwardTime = (int)MathHelper.Clamp(
                        mopData.playBackwardTime + (int)(mopData.playSpeedRandom * (float)MyMath.GetRand()),
                        1, int.MaxValue);

                    mopArray[a] = new MultiObjectPlayer(mopData.startPlayRatioRandom,
                         forwardTime, backwardTime);
                }
                #endregion

                //因為Instance會改node結構，還有設定visible，所以先把childnode抓出來。
                NodeArray currectChildNodeArray = new NodeArray(locatorListRootNode.CountAllChildNode());
                locatorListRootNode.GetAllChildNode(currectChildNodeArray);

                #region 建立KeyFrameMesh物件
                if (mopData.KeyFrameMeshFileName != null)
                {
                    //.xnb要把附檔名幹掉.
                    string fileName = mopData.KeyFrameMeshFileName;
                    fileName = fileName.Remove(fileName.Length - 4);

                    int count = 0;
                    LocatorNode sibling = locatorListRootNode.FirstChild as LocatorNode;
                    while (sibling != null)
                    {
                        //為每個sibling產生keyframe mesh，並attach在其下。
                        mopArray[count].LoadKeyframeMesh(sibling.NodeName, Utility.GetFullPath(fileName), sibling, content, device);
                        count++;
                        sibling = sibling.Sibling as LocatorNode;
                    }
                }
                #endregion

                #region 建立instance simple mesh物件，instance做法一定要有Locator list。
                if (mopData.InstanceSimpleMeshFileName != null)
                {
                    if (objLength <= 0)
                    {
                        //OutputBox.SwitchOnOff = true;
                        OutputBox.ShowMessage(mopData.InstanceSimpleMeshFileName + "無法建立 ==> \ninstance做法一定要有Locator list。");
                        goto INSTANCE_END;
                    }

                    //搜尋定位點底下所有mesh node。
                    int amount = 0;
                    SceneNode.CountTreeAllNode(locatorListRootNode, ref amount, typeof(MeshNode));
                    NodeArray instanceNodeArray = new NodeArray(amount);
                    //SceneNode.AddMeshNode(locatorListRootNode, instanceNodeArray);
                    SceneNode.AddNodeSameType(locatorListRootNode, instanceNodeArray, typeof(MeshNode));

                    //若是有mesh node表示底下有simple keyframe動態模型，
                    //索引就抓有mesh的node。
                    //都抓不到meshnode就使用AllChildNode
                    bool bStaticInstance = false;
                    if (instanceNodeArray.nowNodeAmount == 0)
                    {
                        instanceNodeArray.Dispose();
                        instanceNodeArray = new NodeArray(locatorListRootNode.CountAllChildNode());

                        locatorListRootNode.GetAllChildNode(instanceNodeArray);
                        bStaticInstance = true;
                    }

                    SceneNode.SetAllVisible(locatorListRootNode, false);//有instance畫出，底下就不需要畫原本的模型的。

                    //存到陣列裡傳入instance model
                    FrameNode[] referenceNodes = new FrameNode[instanceNodeArray.nowNodeAmount];
                    for (int a = 0; a < instanceNodeArray.nowNodeAmount; a++)
                        referenceNodes[a] = instanceNodeArray.nodeArray[a] as FrameNode;
                    instanceNodeArray.Dispose();
                    instanceNodeArray = null;

                    //.xnb要把附檔名幹掉
                    string fileName = Utility.RemoveSubFileName(mopData.InstanceSimpleMeshFileName);
                    Model model = Utility.ContentLoad_Model(content, Utility.GetFullPath(fileName));
                    InstanceSimpleMeshNode instanceSimpleMeshNode = InstanceSimpleMeshNode.Create();
                    instanceSimpleMeshNode.SetInstanceMesh(model.Meshes[0], mopData.KeyName, device, content,
                        referenceNodes, bStaticInstance);

                    //把instance接在底下
                    if (instanceNodeArray != null)
                        instanceNodeArray.Dispose();

                    instanceNodeArray = new NodeArray(locatorListRootNode.CountAllChildNode());
                    locatorListRootNode.GetAllChildNode(instanceNodeArray);

                    for (int a = 0; a < instanceNodeArray.nowNodeAmount; a++)
                    {
                        instanceNodeArray.nodeArray[a].DetachFromParent();
                        instanceSimpleMeshNode.AttachChild(instanceNodeArray.nodeArray[a]);
                    }
                    locatorListRootNode.AttachChild(instanceSimpleMeshNode);
                    instanceNodeArray.Dispose();
                    instanceNodeArray = null;
                }
            INSTANCE_END:
                #endregion

                #region 建立SkinnedMesh物件
                if (mopData.SkinnedMeshFileName != null)
                {
                    if (mopData.SkinnedMeshFileName.Contains(".xml"))
                    {
                        string fileName = Utility.GetFullPath(mopData.SkinnedMeshFileName);
                        for (int count = 0; count < currectChildNodeArray.nowNodeAmount; count++)
                        {
                            //為每個sibling產生skinned mesh，並attach在其下。
                            LocatorNode sibling = currectChildNodeArray.nodeArray.GetValue(count) as LocatorNode;

                            SpecialMeshDataManager.TatalSpecialMeshData total;
                            AnimationTableDataArray aniTable;
                            SkinningMeshLoader loader = LoadSceneFromSpecialXML(fileName,
                                content, device, out total, out aniTable) as SkinningMeshLoader;
                            mopArray[count].LoadSkinnedMesh(sibling.NodeName, loader, sibling);
                        }
                    }
                    else
                    {
                        //.xnb要把附檔名幹掉
                        string fileName = Utility.GetFullPath(Utility.RemoveSubFileName(mopData.SkinnedMeshFileName));
                        for (int count = 0; count < currectChildNodeArray.nowNodeAmount; count++)
                        {
                            //為每個sibling產生skinned mesh，並attach在其下。
                            LocatorNode sibling = currectChildNodeArray.nodeArray.GetValue(count) as LocatorNode;
                            mopArray[count].LoadSkinnedMesh(sibling.NodeName, fileName, sibling, content, device);
                        }
                    }
                }
                #endregion

                #region 抓取particle的reference node
                for (int particle_count = 0; particle_count < currectChildNodeArray.nowNodeAmount; particle_count++)
                {
                    LocatorNode particle_sibling = currectChildNodeArray.nodeArray.GetValue(particle_count) as LocatorNode;
                    mopArray[particle_count].LoadParticleData(particle_sibling, mopData.ParticleFileName,
                        mopData.ParticleForwardStartRatio, mopData.ParticleForwardEndRatio,
                        mopData.ParticleBackwardStartRatio, mopData.ParticleBackwardEndRatio);//,
                    //mopData.ParticleSearchMeshNode);
                }
                #endregion

                #region 建立Sound
                for (int cue_count = 0; cue_count < currectChildNodeArray.nowNodeAmount; cue_count++)
                {
                    LocatorNode cue_sibling = currectChildNodeArray.nodeArray.GetValue(cue_count) as LocatorNode;
                    mopArray[cue_count].LoadSound(cue_sibling, mopData.CueName, mopData.CueForwardStartRatio,
                        mopData.CueBackwardStartRatio, mopData.SoundPlayOnce);
                }
                #endregion

                currectChildNodeArray.Dispose();
                currectChildNodeArray = null;

                return true;
            }

            public MOPData()
            {
            }

            /// <summary>
            /// MOP的名稱，一個Node下面可以接很多MOP，但名稱不能一樣。
            /// </summary>
            public string MOPName;

            /// <summary>
            /// 根據KeyName找出Node，update時MOP會計算attach在下面。
            /// </summary>
            public string KeyName;

            public int playForwardTime;
            public int playBackwardTime;
            public float startPlayRatioRandom;
            public float playSpeedRandom;

            public string LocatorFileName;
            public string KeyFrameMeshFileName;
            public string SkinnedMeshFileName;
            public string InstanceSimpleMeshFileName;

            public string ParticleFileName;
            //public string ParticleSearchMeshNode;
            public float ParticleForwardStartRatio;
            public float ParticleForwardEndRatio;
            public float ParticleBackwardStartRatio;
            public float ParticleBackwardEndRatio;

            public string CueName;
            public float CueForwardStartRatio;
            public float CueBackwardStartRatio;
            public bool SoundPlayOnce;
        }

        [Serializable]
        public class DynamicTextureMesh
        {
            public DynamicTextureMesh()
            {
                textureName = FileSubName = null;
                fileNumberDigit = Digit.One;
                startPlayTime = startFileNumber = endFileNumber = timeIntervel = loopTo = 0;
            }

            public string textureName;
            public string FileSubName;
            public Digit fileNumberDigit;
            public int startPlayTime;
            public int startFileNumber;
            public int endFileNumber;
            public int timeIntervel;
            public int loopTo;
        }

        [Serializable]
        public class LocatorList
        {
            public struct SimpleMat
            {
                //public string Name;
                public Quaternion quaternion;
                public Vector3 translation;
            }
            public LocatorList() { }
            public LocatorList(string keyName, int size)
            {
                GlobalPoses = new SimpleMat[size];
            }
            public void SetData(int index, Matrix mat)
            {
                Vector3 s, t;
                Quaternion q;
                mat.Decompose(out s, out q, out t);
                GlobalPoses[index].quaternion = q;
                GlobalPoses[index].translation = t;
            }
            //public void SetDataName(int index, string name)
            //{
            //    GlobalPoses[index].Name = name;
            //}
            public Matrix GetData(int index)
            {
                Matrix mmm = Matrix.CreateFromQuaternion(GlobalPoses[index].quaternion);
                mmm.Translation = GlobalPoses[index].translation;
                return mmm;
            }
            //public string GetDataName(int index)
            //{            
            //    return GlobalPoses[index].Name;
            //}

            /// <summary>
            /// 將傳入名稱處理為，Locator名稱
            /// 建立一個parent和只有一個child的locator tree結構，用來對應CreateLocatorNodeList()
            /// 建立一個parent和很多child的locator tree結構。
            /// </summary>
            public static string GetLocatorRootNodeName(string keyname)
            {
                return keyname + "_LocatorRoot";
            }

            /// <summary>
            /// 建立一個parent只有一個child的簡單的Locator架構
            /// </summary>       
            static public LocatorNode CreateSimpleLocatorNodeList(string keyName)
            {
                LocatorNode ParentNode = LocatorNode.Create();
                LocatorNode childNode = LocatorNode.Create();

                ParentNode.NodeName = GetLocatorRootNodeName(keyName);
                childNode.NodeName = keyName + "_LocatorChild";

                ParentNode.AttachChild(childNode);
                return ParentNode;
            }

            /// <summary>
            /// 將list資料形式，建立成locator node 樹狀結構。
            /// </summary>   
            public LocatorNode CreateLocatorNodeList(string keyName)
            {
                LocatorNode ParentNode = LocatorNode.Create();
                ParentNode.NodeName = GetLocatorRootNodeName(keyName);
                for (int a = 0; a < GlobalPoses.Length; a++)
                {
                    LocatorNode ChildNode = LocatorNode.Create();
                    ChildNode.NodeName = keyName + a.ToString();
                    ChildNode.WorldMatrix = GetData(a);
                    ParentNode.AttachChild(ChildNode);
                }
                return ParentNode;
            }

            public SimpleMat[] GlobalPoses;
        }

        [Serializable]
        public class TranslateUVMesh
        {
            public TranslateUVMesh()
            {
            }

            public static void SetToNode(SceneNode node, TranslateUVMesh translateUV)
            {
                node.TranslateUVSinLoopSpeed.X = translateUV.TranslateUSinLoopSpeed;
                node.TranslateUVSinLoopSpeed.Y = translateUV.TranslateVSinLoopSpeed;
                node.TranslateUVSpeed.X = translateUV.TranslateUSpeed;
                node.TranslateUVSpeed.Y = translateUV.TranslateVSpeed;
                node.centerScaleUV.X = translateUV.CenterScaleX;
                node.centerScaleUV.Y = translateUV.CenterScaleY;
                node.centerScaleUV.Z = translateUV.CenterScaleSpeed;
            }

            public float TranslateUSinLoopSpeed;
            public float TranslateVSinLoopSpeed;

            public float TranslateUSpeed;
            public float TranslateVSpeed;

            public float CenterScaleX;
            public float CenterScaleY;
            public float CenterScaleSpeed;
        }

        [Serializable]
        public class RenderFeature
        {
            public RenderFeature()
            {
                HasTexture = true;
                LightEnable = true;
                DrawAlphaBlend = false;
                DrawTwoSide = false;
                FogEnable = true;
                RenderDepth = true;
                MipmapLevel = 0;
                // AlphaTestEnable = true;
                AlphaTestReference = 128;

                //DrawWithRelectMap = false;
                ObjectAlpha = 255;
                RefractMapScale = 0;
                ReflectIntensity = 0;
                FresnelPower = 1;

                //spotLightData = null;

                IfRenderShadowMap = true;
                //BigShadowMapRatio = new Vector3(0.98f);
                IfRenderGodsRay = false;

                BlendColor = null;
            }

            public static void SetToNode(SceneNode node, RenderFeature renderFeature, ContentManager content, GraphicsDevice device)
            {
                node.IfTextured = renderFeature.HasTexture;
                node.IfLightEnable = renderFeature.LightEnable;
                node.IfAlphaBlenbing = renderFeature.DrawAlphaBlend;
                node.IfTwoSideDraw = renderFeature.DrawTwoSide;
                node.IfRenderDepth = renderFeature.RenderDepth;
                node.IfFogEnable = renderFeature.FogEnable;
                //node.IfMipmap = renderFeature.MipmapEnable;
                node.MipmapLevel = renderFeature.MipmapLevel;
                //    node.IfAlphaTestEnable = renderFeature.AlphaTestEnable;
                node.AlphaTestReference = renderFeature.AlphaTestReference;

                //node.IfDrawWithReflectMap = renderFeature.DrawWithRelectMap;
                if (renderFeature.BlendColor != null)
                    node.BlendColor = (Color)renderFeature.BlendColor;
                node.ObjectAlpha = renderFeature.ObjectAlpha;
                node.RefractScale = renderFeature.RefractMapScale;
                node.ReflectIntensity = renderFeature.ReflectIntensity;
                node.FresnelPower = renderFeature.FresnelPower;

                node.IfRenderShadowMap = renderFeature.IfRenderShadowMap;
                //node.BigShadowMapRatio = renderFeature.BigShadowMapRatio;
                node.IfRenderGodsRay = renderFeature.IfRenderGodsRay;

                //if (renderFeature.spotLightData != null)
                //{
                //    node.SetSpotLight(renderFeature.spotLightData.angleTheta,
                //        renderFeature.spotLightData.anglePhi, renderFeature.spotLightData.strength,
                //        renderFeature.spotLightData.decay, renderFeature.spotLightData.range,
                //        renderFeature.spotLightData.colorValue);
                //}

                //if (renderFeature.bCreateShadowVolume)
                //{
                //    int part;
                //    ModelMesh mesh = node.GetModelMeshPart(out part);
                //    ShadowVolumeNode.CreateShadowVolumeNode(node, content, device, mesh, part);
                //}
                //else
                //    ShadowVolumeNode.CreateShadowVolumeNode(node, content, device, null, 0);
            }

            public bool HasTexture;
            public bool LightEnable;
            public bool DrawAlphaBlend;
            public bool DrawTwoSide;

            public bool FogEnable;
            public bool RenderDepth;
            //  public bool AlphaTestEnable;
            public byte AlphaTestReference;
            public float MipmapLevel;

            //public bool DrawWithRelectMap;
            public byte ObjectAlpha;
            public float RefractMapScale;
            public float ReflectIntensity;
            public float FresnelPower;

            //public SpotLightData spotLightData;
            //public bool bCreateShadowVolume;

            public bool IfRenderShadowMap;
            //public Vector3 BigShadowMapRatio;
            public bool IfRenderGodsRay;

            public Color? BlendColor;
        }

        [Serializable]
        public class MeshMaterial
        {
            public static void SetToNode(SceneNode node, MeshMaterial materialData, ContentManager content, GraphicsDevice device)
            {
                //SceneNode.SetMaterialDiffuseAmbientSpecularEmmisive(
                //    materialData.Ambient,
                //    materialData.Diffuse,
                //    materialData.Specular,
                //    materialData.Power,
                //    node, false);

                if (materialData.SpecularMapName == string.Empty && node.SpecularMap != null)//有時Effect設定就有specular map資訊，就不要設定這個了。
                { }
                else
                    node.SetSpecularMap(materialData.SpecularMapName, device, content);

                if (materialData.EmmisiveMapName == string.Empty && node.EmmisiveMap != null)//有時Effect設定就有emmisive map資訊，就不要設定這個了。
                { }
                else
                    node.SetEmmisiveMap(materialData.EmmisiveMapName, device, content);

                if (materialData.ReflectPowerMapName == string.Empty && node.ReflectIntensityMap != null)//有時Effect設定就有emmisive map資訊，就不要設定這個了。
                { }
                else
                    node.SetReflectIntensityMap(materialData.ReflectPowerMapName, device, content);

                node.SetEnvCubeMap(materialData.EnvMapName, device, content);
                node.EmmisiveAlpha = materialData.EmmisiveAlpha;
            }

            public MeshMaterial() { }

            //  public Vector3 Ambient;
            //   public Vector3 Diffuse;
            //  public Vector3 Specular;
            //   public ushort Power;

            public string SpecularMapName = null;
            public string ReflectPowerMapName = null;
            public string EmmisiveMapName = null;
            public string EnvMapName = null;
            public float EmmisiveAlpha; //控制emmisive的濃度,0~1之間...
        }

        [Serializable]
        public class RSTData
        {
            public string nodeName;
            public RSTMatrix rst;
            public RSTMatrix origRST;

            public void SetData(string name, ref Matrix mat, Matrix? origMat)
            {
                nodeName = name;
                rst.SetData(mat);

                if(origMat!=null)
                origRST.SetData(origMat.Value);
                //Matrix matR;
                //rstData.rst.GetMatrix(out matR);
                //node.LocalMatrix = matR;
            }

            public Matrix GetOrigMat() { return origRST.mat; }
        }

        [Serializable]
        public class SceneLightFogData
        {
            public SceneLightFogData()
            {
                Light1Direction = Light1Ambient = Light1Diffuse = Light1Specular = FogColor = Vector3.One;
                FogStart = FogEnd = 0;
                AllBigShadowMapMultiSampleOffset = 20;
                AllSmallShadowMapMultiSampleOffset = 10;
                AllBigShadowMapRatio = new Vector3(0.98f);
                AllSmallShadowMapRatio = new Vector3(0.95f);
                AllBigShadowMapBias = 0.001f;
                AllSmallShadowMapBias = 0.001f;

                MipmapLevel = 0;
                FogMaxRatio = 1;

                AllBigShadowSampleLength = 1;
                AllSmallShadowSampleLength = 1;
            }
            public Vector3 Light1Direction;
            public Vector3 Light1Ambient;
            public Vector3 Light1Diffuse;
            public Vector3 Light1Specular;
            public Vector3 FogColor;
            public float FogStart;
            public float FogEnd;
            public float FogMaxRatio;

            public float AllBigShadowMapMultiSampleOffset;
            public float AllSmallShadowMapMultiSampleOffset;
            public Vector3 AllBigShadowMapRatio;
            public Vector3 AllSmallShadowMapRatio;
            public float AllBigShadowMapBias, AllSmallShadowMapBias;
            public float? AllBigShadowSampleLength, AllSmallShadowSampleLength;

            public float MipmapLevel;
        }

        [Serializable]
        public class TatalSpecialMeshData
        {
            public enum MODELTYPE
            {
                None = 0,
                SceneMesh = 1,
                SkinnedMesh = 2,
                KeyframeMesh = 3,
            }
            public int ModelType;
            public string ModelFileName;//相對應的場景檔案名稱

            public string ParentLoaderName = null;//此loader是否會parent於哪個loader的哪個node中
            public string ParentNodeName = null;

            //public RSTMatrix RootRST = RSTMatrix.Init();//RootNode的RST資訊

            public TatalSpecialMeshData()
            {
                ParentLoaderName = null;
                ParentNodeName = null;

                ModelType = (int)MODELTYPE.None;
                ModelFileName = null;
                NodesData = null;
                pointLights = null;
                spotLights = null;
                MutiObjectPlayers = null;
                //LightFogData = null;
                //OceanDatas = null;
                //GodsRayLightDatas = null;
                //AnimateWithMOPs = null;

                NodesRSTData = null;
            }


            public NodeSpecialMeshData[] NodesData;
            public PointLights pointLights;//此loader上的點光源
            public SpotLights spotLights;//此loader上的spot光源

            // //       public SceneLightFogData LightFogData;

            //        // 其他與場景有關的特殊物件設定。
            public MOPData[] MutiObjectPlayers;

            //public string[] XNAParticleFileName;
            //public string 
            //        public MyOceanData[] OceanDatas;

            ////        public GodsRayLightData[] GodsRayLightDatas;

            //紀錄動態與他相關MOP的播放，動態模型才會有的資訊。
            public AnimateWithMOP[] AnimateWithMOPs;

            //記錄node要設定的RST資訊
            public RSTData[] NodesRSTData;
        }

        /// <summary>
        /// 與node有關的基本屬性設定。
        /// </summary>
        [Serializable]
        public class NodeSpecialMeshData
        {
            public NodeSpecialMeshData()
            {
                NodeName = null;
                MaterialBankID = null;
                MeshesMaterials = null;
                TranslateUVMeshes = null;
                RenderFeatureMeshes = null;
                //RSTDataMeshes = null;
                windData = null;
            }

            public string NodeName;
            public string MaterialBankID;
            public MeshMaterial MeshesMaterials;
            public TranslateUVMesh TranslateUVMeshes;
            public RenderFeature RenderFeatureMeshes;
            public DynamicTextureMesh DynamicTextureMeshes;
            public DynamicTextureMesh DynamicNormalTextureMeshes;
            //public RSTData RSTDataMeshes;
            public string NormalTextureFileName;
            public WindData windData;
        }

        /// <summary>
        /// 建立MyOcean專用資料結構
        /// </summary>
        [Serializable]
        public struct MyOceanData
        {
            public static MyOceanData GetDefault()
            {
                MyOceanData ddd;
                //ddd.OceanPlaneCenter = Vector3.Zero;
                ddd.ReferenceOceanPlaneName = null;
                ddd.Paramaters = new MyOcean.OceanParamater();
                ddd.DepthMapData = null;
                ddd.CoastDepthMapSize = 128;
                return ddd;
            }

            public string ReferenceOceanPlaneName;
            public MyOcean.OceanParamater Paramaters;
            public DepthMapCreator.DepthMapData DepthMapData;
            public int CoastDepthMapSize;
        }

        [Serializable]
        public struct GodsRayLightData
        {
            public static GodsRayLightData GetDefault()
            {
                GodsRayLightData ddd;
                ddd.Density = 0.286f;
                ddd.Weight = 0.832f;
                ddd.Decay = 1.0261f;
                ddd.Exposition = 0.0104f;
                ddd.position = ddd.direction = Vector3.Zero;
                ddd.Name = "GodsRayLight_" + MyMath.GetNext(999).ToString();
                ddd.samples = 32;
                return ddd;
            }

            public string Name;
            public int samples;
            public float Density, Decay, Weight, Exposition;
            public Vector3 position, direction;
        }

        [Serializable]
        public class WindData
        {
            public static WindData WindDataClone(WindData from)
            {
                WindData to = new WindData();
                to.WindAmount = from.WindAmount;
                to.WindSpeed = from.WindSpeed;
                //to.RootHeight=                                    from.RootHeight;
                to.WorldWindDirection = from.WorldWindDirection;
                to.WindWaveSize = from.WindWaveSize;
                to.TreeHeight = from.TreeHeight;
                to.grassData = null;
                if (from.grassData != null)
                {
                    to.grassData = new GrassData();
                    to.grassData.textureFilename = from.grassData.textureFilename;
                    to.grassData.width = from.grassData.width;
                    //to.grassData.height = from.grassData.height;
                    to.grassData.billboardsPerTriangle = from.grassData.billboardsPerTriangle;
                    to.grassData.windRandomness = from.grassData.windRandomness;
                }
                return to;
            }

            public WindData()
            {
                WindAmount = 0;
                WindSpeed = 0;
                //RootHeight = 0;
                WorldWindDirection = Vector3.One;
                WindWaveSize = 0;
                grassData = null;
            }

            public float WindAmount;
            public float WindSpeed;
            //public float RootHeight;
            public float TreeHeight;
            public Vector3 WorldWindDirection;
            public float WindWaveSize;
            public GrassData grassData;
        }

        [Serializable]
        public class GrassData
        {
            public GrassData()
            {
                textureFilename = null;
                width = billboardsPerTriangle = 0;
                //windSpeed =
                windRandomness = 0;
            }
            public string textureFilename;
            public float width;
            //public float height;     
            public int billboardsPerTriangle;
            public float windRandomness;
            //public float windSpeed;
        }

        /// <summary>
        /// 記錄動作名稱還有會一起觸發的MOP
        /// </summary>
        [Serializable]
        public class AnimateWithMOP
        {
            public string AnimationName;
            public string[] MOPLocatorNames;
        }
    }

}
