﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Media;

using I_XNAUtility;
using MeshDataDefine;

namespace I_XNAComponent
{
    public enum BillBoardAxis
    {
        None,
        X,
        Y,
        Z,
    }

    public class LocatorNode : FrameNode
    {
        public static LocatorNode Create()
        {
            return ReleaseCheck.CreateNew(typeof(LocatorNode)) as LocatorNode;
        }
        public LocatorNode()
        {
        }

        ~LocatorNode()
        {
            ReleaseCheck.DisposeCheckCount(this);
        }

        protected override void v_Dispose()
        {
            ReleaseCheck.DisposeCheck(this);
        }
    }

    public abstract class SceneNode : ViewCullingMeshNode
    {
        public object[] tags = new object[8];


        public enum RenderTechnique : uint
        {
            None = 0,
            //post processing........................
            //ScreenVelocityMap = 1,
            //GodsRayMap = 1 << 1,//2
            //EmmisiveMap = 1 << 2,//4
            //PointLightMap = 1 << 3,//8
            //NormalRefractMap = 1 << 4,
            //DepthMap = 1 << 5,
            //ShadowVolume = 1 << 6,
            //EmmisiveOnly = 1 << 7,
            //post processing........................

            SimpleMesh = 1 << 0,
            TengentMesh = 1 << 1,//16
            //  DeferredSimpleMesh = SimpleMesh | DeferredShading,
            //  SimpleTengentMesh = SimpleMesh | TengentMesh,

            SkinnedMesh = 1 << 3,
            //   DeferredSkinnedMesh = SkinnedMesh | DeferredShading,
            //  SkinnedTengentMesh = SkinnedMesh | TengentMesh,

            SimpleInstanceMesh = 1 << 19,
            //         InstanceTengentMesh = SimpleInstanceMesh | TengentMesh,

            GrassMesh = 1 << 20,

            DeferredShading = 1 << 21,
        }
        //uint m_CurrentTechnique = (uint)RenderTechnique.None;

        abstract protected void v_Render(EffectRenderManager effectRenderManager, GraphicsDevice device); //override and do render anything.....
        //abstract protected bool v_ConsiderCurrentTechniqueOK(RenderTechnique CurrentRenderTechnique);
        abstract public ModelMesh GetModelMeshPart(out int partID);

        //abstract protected void v_LoadContent(string modelName, ContentManager content, GraphicsDevice device);
        //abstract void ConsiderViewFrustumCulling();

        public enum RenderFeature : uint
        {
            None = 0,
            HasTexture = 1,
            LightEnable = 1 << 1,
            DrawAlphaBlend = 1 << 2,
            DrawTwoSide = 1 << 3,
            FogEnable = 1 << 4,
            RenderDepth = 1 << 5,
            //MipmapEnable = 1 << 6,
            RenderShadowMap = 1 << 7,
            CreateShadowVolume = 1 << 8,
            RenderGodsRay = 1 << 9,
            //AlphaTestEnable = 1 << 10,
            Default = None |
                //    RenderFeature.AlphaTestEnable |
                RenderFeature.LightEnable |
                RenderFeature.HasTexture |
                RenderFeature.FogEnable |
                RenderFeature.RenderDepth |
                //RenderFeature.MipmapEnable |
                RenderFeature.RenderShadowMap,
        }

        public SceneNode()
        {
            m_IndexBuffer = null;
            m_VertexBuffer = null;

        }

        ~SceneNode()
        {
            ReleaseCheck.DisposeCheckCount(this);
        }

        override public void Dispose()
        {
            ReleaseCheck.DisposeCheck(this);

            XNBTextureFileName = null;
            VideoTextureFileName = null;
            if (m_VideoPlayer != null)
                m_VideoPlayer.Dispose();
            m_VideoPlayer = null;
            m_Video = null;

            m_IndexBuffer = null;
            m_VertexBuffer = null;

            if (SpecularMap != null)
            {
                SpecularMap.Dispose();
                SpecularMap = null;
            }
            if (ReflectIntensityMap != null)
            {
                ReflectIntensityMap.Dispose();
                ReflectIntensityMap = null;
            }
            if (EmmisiveMap != null)
            {
                EmmisiveMap.Dispose();
                EmmisiveMap = null;
            }

            if (m_DynamicTexture != null)
            {
                m_DynamicTexture.Dispose();
                m_DynamicTexture = null;
            }

            if (m_DynamicNormalTexture != null)
            {
                m_DynamicNormalTexture.Dispose();
                m_DynamicNormalTexture = null;
            }

            SoundEffectPlay(null, null, 0);
            //if (m_SoundEffectInstance != null)
            //    m_SoundEffectInstance.Dispose();
            //m_SoundEffectInstance = null;
            //m_SoundEffectInstance = null;
            //SoundEffectFileName = null;

            ReleaseEnvCubeMap();

            base.Dispose();
        }

        #region 解析模型參數
        bool m_bNormalMap = false;//判斷模型資訊理是否有normal圖
        bool m_bTangentFrame = false;
        public bool IfTangentFrame { get { return m_bTangentFrame; } }
        virtual public void AnalyzeMeshTexture(Dictionary<string, object> meshTag, ModelMesh mesh, int partID)
        {
            if (meshTag == null)
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage("SetMesh() ==> 'meshTag == null'");
                return;
            }

            m_bNormalMap = false;//判斷模型資訊理是否有normal圖
            m_bTangentFrame = false;//判斷模型資訊哩，點資訊是否有可以貼normal map的tangent資訊...

            if (meshTag.ContainsKey(MeshKeys.NormalMapKey))
                m_bNormalMap = (bool)meshTag[MeshKeys.NormalMapKey];

            if (meshTag.ContainsKey(MeshKeys.TangentFrameKey))
                m_bTangentFrame = (bool)meshTag[MeshKeys.TangentFrameKey];

            if (m_bNormalMap)
            {
                EffectParameter nnnEP = mesh.MeshParts[0].Effect.Parameters["NormalMap"];
                m_NormalTexture = nnnEP.GetValueTexture2D();
            }
            else
                m_NormalTexture = null;
            m_RecNormalTexture = m_NormalTexture;

            //檢查是否有貼圖
            SetBaseTexture(mesh.MeshParts[partID].Effect.Parameters["Texture"].GetValueTexture2D());

            EffectParameter effpar = mesh.MeshParts[partID].Effect.Parameters["SpecularMap"];
            if (effpar != null)
                SetSpecularMap(effpar.GetValueTexture2D());

            effpar = mesh.MeshParts[partID].Effect.Parameters["EmmisiveMap"];
            if (effpar != null)
                SetEmmisiveMap(effpar.GetValueTexture2D());

            effpar = mesh.MeshParts[partID].Effect.Parameters["ReflectIntensityMap"];
            if (effpar != null)
                SetReflectIntensityMap(effpar.GetValueTexture2D());
        }


        public string XNBTextureFileName = null;
        /// <summary>
        /// 設定XNB檔替換掉貼圖
        /// </summary>
        public void SetBaseTexture(string xnbTextureFileName, GraphicsDevice device, ContentManager content)
        {
            if (m_VideoPlayer != null)
                m_VideoPlayer.Dispose();
            m_VideoPlayer = null;
            m_Video = null;
            VideoTextureFileName = null;

            if (XNBTextureFileName != xnbTextureFileName)
            {
                XNBTextureFileName = xnbTextureFileName;
                m_BaseTexture = Utility.Load_Texture2D(device, content, XNBTextureFileName, false);
            }
        }



        public string VideoTextureFileName;
        public VideoPlayer m_VideoPlayer = null;
        public Video m_Video = null;
        public void SetBaseTexture(string videoFileName, ContentManager content)
        {
            if (VideoTextureFileName != videoFileName)
            {
                VideoTextureFileName = videoFileName;
                if (m_VideoPlayer != null)
                    m_VideoPlayer.Dispose();
                //if (m_VideoPlayer == null)
                m_VideoPlayer = new VideoPlayer();
                m_Video = content.Load<Video>(Utility.GetFullPath(videoFileName));
            }

            m_VideoPlayer.IsLooped = false;
            m_VideoPlayer.Play(m_Video);
            XNBTextureFileName = null;
        }

        public MediaState GetVideoTextureState()
        {
            return m_VideoPlayer.State;
        }

        public void SetBaseTexture(Texture2D tex)
        {
            m_BaseTexture = tex;
            m_RecBaseTexture = m_BaseTexture;

            if (m_BaseTexture == null)
            {
                //parentNode.Visible = false;
                IfTextured = false;//沒貼圖就讓他畫白色的
                //Visible = true;
            }
            else
            {
                IfTextured = true;
                //Visible = true;
            }

            if (m_VideoPlayer != null)
                m_VideoPlayer.Dispose();
            m_VideoPlayer = null;
            m_Video = null;
            VideoTextureFileName = null;

            XNBTextureFileName = null;
        }

        //virtual public bool ConsiderCurrentTechniqueOK(RenderTechnique currentTechnique)
        //{
        //    //有tangent但目前不是畫normal map狀態就回傳FALSE
        //    if (IfTangentFrame)
        //    {
        //        //如果有normal Map但卻沒用normal map的technique就不畫了                
        //        if (((uint)currentTechnique & (uint)RenderTechnique.TengentMesh) == 0)
        //            return false;
        //    }

        //    //沒tangent但目前是畫normal map狀態就回傳FALSE
        //    else// (IfTangentFrame == false)
        //    {
        //        if (((uint)currentTechnique & (uint)RenderTechnique.TengentMesh) > 0)
        //            return false;
        //    }

        //    return v_ConsiderCurrentTechniqueOK(currentTechnique);
        //}

        //protected ModelMesh GetBasicModelMesh { get { return m_BasicModelMesh; } }

        //protected ModelMeshPart GetMeshPart() { return m_Mesh.MeshParts[0]; }
        //protected IndexBuffer GetMeshIndexBuffer() { return m_Mesh.IndexBuffer; }
        //protected VertexBuffer GetMeshVertexBuffer() { return m_Mesh.VertexBuffer; }
        #endregion

        #region 與貼圖，normal map，及動態圖有關的
        public float ReflectIntensity = 0;    //與環境貼圖與反射貼圖計算的濃度參數
        public float FresnelPower = 0;
        public Vector3 BigShadowMapRatio = new Vector3(0.95f);//全場景大shadow map的soft比例
        public Vector3 SmallShadowMapRatio = new Vector3(0.97f);//全場景大shadow map的soft比例
        public float BigShadowMapBias = 0.0001f;
        public float SmallShadowMapBias = 0.0001f;
        public float SmallShadowSampleOffset = 50f;
        public float BigShadowSampleOffset = 10f;
        public float SmallShadowSampleLength = 1;
        public float BigShadowSampleLength = 1;
        public static void SetAllShadowMapData(FrameNode parentNode, Vector3? bigRatio, Vector3? smallRatio,
            float? bigBias, float? smallBias, float? bigOffset, float? smallOffset, float? bigLength, float? smallLength)
        {
            SceneNode node = parentNode as SceneNode;
            if (node != null)
            {
                if (bigRatio.HasValue)
                    node.BigShadowMapRatio = bigRatio.Value;
                if (smallRatio.HasValue)
                    node.SmallShadowMapRatio = smallRatio.Value;
                if (bigBias.HasValue)
                    node.BigShadowMapBias = bigBias.Value;
                if (smallBias.HasValue)
                    node.SmallShadowMapBias = smallBias.Value;

                if (bigOffset.HasValue)
                    node.BigShadowSampleOffset = bigOffset.Value;
                if (smallOffset.HasValue)
                    node.SmallShadowSampleOffset = smallOffset.Value;

                if (bigLength.HasValue)
                    node.BigShadowSampleLength = bigLength.Value;
                if (smallLength.HasValue)
                    node.SmallShadowSampleLength = smallLength.Value;
            }

            FrameNode childnode = parentNode.FirstChild as FrameNode;
            while (childnode != null)
            {
                SetAllShadowMapData(childnode, bigRatio, smallRatio, bigBias, smallBias, bigOffset, smallOffset, bigLength, smallLength);
                childnode = childnode.Sibling as FrameNode;
            }
        }

        //ReflectData m_ReflectData;//外部傳入的反射圖資訊
        //public void SetReflectData(ReflectData reflectData)
        //{
        //    m_ReflectData = null;
        //    if (ReflectIntensity > 0)
        //        m_ReflectData = reflectData;
        //}

        //public static void SetAllReflectData(FrameNode parentNode, ReflectData reflectData)
        //{
        //    SceneNode node = parentNode as SceneNode;
        //    if (node != null)
        //        node.SetReflectData(reflectData);

        //    FrameNode childnode = parentNode.FirstChild as FrameNode;
        //    while (childnode != null)
        //    {
        //        SetAllReflectData(childnode, reflectData);
        //        childnode = childnode.Sibling as FrameNode;
        //    }
        //}

        //public Texture2D ReflectMap { get { if (m_ReflectData == null)return null; return m_ReflectData.ReflectMap; } }
        //public Matrix ReflectView { get { return m_ReflectData.ReflectView; } }

        public Color BlendColor = Color.TransparentWhite;// new Color(255, 255, 255, 0);//new Vector4(255, 255, 255, 0);
        public byte ObjectAlpha = 255;

        /// <summary>
        /// 若此模型是折射物體值為>0，並且要畫refract map。
        /// </summary>
        public float RefractScale = 0;

        Texture2D m_BaseTexture, m_RecBaseTexture;
        Texture2D m_NormalTexture, m_RecNormalTexture;
        public Texture2D BaseTexture { get { return m_BaseTexture; } }
        public string NormalMapLoadFileName = null;//由外部讀入的normal map就把它檔名記下來
        public Texture2D NormalTexture { get { return m_NormalTexture; } }
        public DynamicTexture m_DynamicTexture;
        public DynamicTexture m_DynamicNormalTexture;
        public DynamicTexture DynamicTexture { get { return m_DynamicTexture; } }
        public DynamicTexture DynamicNormalTexture { get { return m_DynamicNormalTexture; } }
        public string GrassTextureFileName = null;//由外部讀入的Grass Texture就把它檔名記下來
        Texture2D m_GrassTexture;

        //移動貼圖軸屬性ˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇ
        public Vector2 TranslateUVSinLoopSpeed = Vector2.Zero;
        public Vector2 TranslateUVSpeed = Vector2.Zero;
        public Vector3 centerScaleUV = Vector3.Zero;

        //動態圖ˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇ
        public void SetDynamicTexture(string name, Digit digit, int startPlayTime,
            int numberStart, int numberEnd, int timeIntervel, int loopTo, string fileSub, ContentManager content, GraphicsDevice device)
        {
            RemoveDynamicTexture();

            m_DynamicTexture = new DynamicTexture();
            m_DynamicTexture.CreateDynamicTexture(name, digit, startPlayTime,
               numberStart, numberEnd, timeIntervel, loopTo, fileSub, content, device);

            m_DynamicTexture.Play(false, 1);
        }
        public void RemoveDynamicTexture()
        {
            m_BaseTexture = m_RecBaseTexture;
            if (m_DynamicTexture != null)
                m_DynamicTexture.Dispose();
            m_DynamicTexture = null;
        }

        public void SetDynamicTexturePlay()
        {
            m_DynamicTexture.Play(false, 1);
        }

        public void SetDynamicTexturePlayBack()
        {
            m_DynamicTexture.PlayBack(false, 1);
        }

        public void SetDynamicNormalTexture(string name, Digit digit, int startPlayTime,
            int numberStart, int numberEnd, int timeIntervel, int loopTo, string fileSub, ContentManager content, GraphicsDevice device)
        {
            RemoveDynamicNormalTexture();

            m_DynamicNormalTexture = new DynamicTexture();
            m_DynamicNormalTexture.CreateDynamicTexture(name, digit, startPlayTime,
               numberStart, numberEnd, timeIntervel, loopTo, fileSub, content, device);

            m_DynamicNormalTexture.Play(false, 1);
        }

        public void RemoveDynamicNormalTexture()
        {
            m_NormalTexture = m_RecNormalTexture;
            if (m_DynamicNormalTexture != null)
                m_DynamicNormalTexture.Dispose();
            m_DynamicNormalTexture = null;
        }

        /// <summary>
        /// Normal Texture只能讀processor處理完的XNB檔，不用傳入副檔名.xnb。
        /// </summary>     
        public void SetNormalTexture(string normalTextureFileName, ContentManager content)
        {
            NormalMapLoadFileName = normalTextureFileName;
            m_NormalTexture = Utility.ContentLoad_Texture2D(content, Utility.GetFullPath(Utility.RemoveSubFileName(
                normalTextureFileName)));
            //Utility.Load_Texture2D(device, content, normalTextureFileName, false);
        }

        public void RemoveNormalTexture()
        {
            if (m_NormalTexture != m_RecNormalTexture)
            {
                if (NormalMapLoadFileName.Contains(".xnb") == false)//不是使用content load XND才釋放，XND的給content去釋放
                    m_NormalTexture.Dispose();

                NormalMapLoadFileName = null;
                m_NormalTexture = m_RecNormalTexture;
            }
        }

        //public void SetGrassTexture(string grassFileName, GraphicsDevice device, ContentManager content)
        //{
        //    GrassTextureFileName = grassFileName;
        //    m_GrassTexture = Utility.Load_Texture2D(device, content, Utility.GetFullPath(grassFileName), false);
        //}

        //public void RemoveGrassTexture()
        //{
        //    //if (m_GrassTexture == null)
        //    //    return;

        //    if (GrassTextureFileName.Contains(".xnb") == false)//不是使用content load XND才釋放，XND的給content去釋放
        //        m_GrassTexture.Dispose();

        //    GrassTextureFileName = null;
        //}
        #endregion

        #region 判斷繪圖switch & IB VB資訊

        //Do fruntum culling here........
        //bool m_bDrawBoundingVolume;


        protected int m_IndexBufferSize = 0; //儲存index buffer的數量，小於等於0表示沒有模型。

        /// <summary>
        /// 針對做碰撞測試才設定IndexBuffer和VertexBuffer資訊。
        /// </summary>
        uint[] m_IndexBuffer;
        Vector3[] m_VertexBuffer;

        public bool IfMeshData
        {
            get { if (m_IndexBufferSize > 0) { return true; } return false; }
        }

        public uint[] IndexBuffer
        {
            get { return m_IndexBuffer; }
            set { IndexBuffer = value; }
        }
        public Vector3[] VertexBuffer
        {
            get { return m_VertexBuffer; }
            set { VertexBuffer = value; }
        }



        //public void SetAllVisible(bool visible, string name)
        //{
        //    if (NodeName != null && NodeName.Contains(name))
        //        this.Visible = visible;

        //    SceneNode children = (SceneNode)this.FirstChild;
        //    while (children != null)
        //    {
        //        children.SetAllVisible(visible, name);
        //        children = (SceneNode)children.Sibling;
        //    }
        //}

        //set vertex buffer , index buffer for create bounding volume...
        protected void SetIBVB(uint[] IB, Vector3[] VB)
        {
            m_IndexBuffer = IB;
            m_VertexBuffer = VB;
        }

        public void ReleaseAllIBVB()
        {
            m_IndexBuffer = null;
            m_VertexBuffer = null;

            SceneNode children = (SceneNode)this.FirstChild;
            while (children != null)
            {
                children.ReleaseAllIBVB();
                children = (SceneNode)children.Sibling;
            }
        }


        #endregion

        #region 與bounding volumn有關


#if DEBUG
        bool m_bDrawBoundingBox;
        bool m_bDrawBoundingSphere;
        Color BoundBoxColor;
        Color BoundSphereColor;


        public void SetDrawBoundingSphere(bool bbb, Color? color)
        {
            m_bDrawBoundingSphere = bbb;
            if (color != null)
                BoundSphereColor = color.Value;
        }


        static public void SetAllDrawBoundingSphere(FrameNode frameNode, bool bbb, Color? color)
        {
            SceneNode sceneNode = frameNode as SceneNode;
            if (sceneNode != null)
                sceneNode.SetDrawBoundingSphere(bbb, color);

            FrameNode node = frameNode.FirstChild as FrameNode;
            while (node != null)
            {
                SceneNode.SetAllDrawBoundingSphere(node, bbb, color);
                node = node.Sibling as FrameNode;
            }
        }
        public void SetDrawBoundingBox(bool bbb, Color? color)
        {
            m_bDrawBoundingBox = bbb;
            if (color != null)
                BoundBoxColor = color.Value;
        }
        static public void SetAllDrawBoundingBox(FrameNode frameNode, bool bbb, Color? color)
        {
            SceneNode sceneNode = frameNode as SceneNode;
            if (sceneNode != null)
                sceneNode.SetDrawBoundingBox(bbb, color);

            FrameNode node = frameNode.FirstChild as FrameNode;
            while (node != null)
            {
                SceneNode.SetAllDrawBoundingBox(node, bbb, color);
                node = node.Sibling as FrameNode;
            }
        }
#endif




        //culling object with view frustum....    
        public void MyViewFrustumCulling(ref Matrix ViewProjection)
        {
            BeCulled = true;
            Matrix worldMat = WorldMatrix;
            if (m_bBoundingSphere)
            {
                if (Collision.FrustumContainSphere(ref ViewProjection, BoundingSphere, ref worldMat) == true)
                {
                    if (m_bBoundingBox)
                    {
                        if (Collision.FrustumContainBox(ref ViewProjection, BoundingBox, ref worldMat) == true)
                        {
                            BeCulled = false;
                        }
                    }
                }
            }
        }

        public void UpdateTree_FrustumCulling(ref Matrix ViewProjection)
        {
            if (Visible)
                MyViewFrustumCulling(ref ViewProjection);

            //render...
            //Render(ref renderTriangleAmount);

            SceneNode c = (SceneNode)FirstChild;
            while (c != null)
            {
                c.UpdateTree_FrustumCulling(ref ViewProjection);
                c = (SceneNode)c.Sibling;
            }
        }


        #endregion

        #region 各種搜尋node方法

        /// <summary>
        /// 計算nowNode底下有多少，相同nodeType的Node數量
        /// </summary>  
        public static void CountNodeSameType(FrameNode nowNode, Type nodeType, ref int Count)
        {
            SceneNode node = nowNode as SceneNode;
            if (node != null && node.GetType() == nodeType)
            {
                Count++;
            }

            FrameNode c = nowNode.FirstChild as FrameNode;
            while (c != null)
            {
                CountNodeSameType(c, nodeType, ref Count);
                c = c.Sibling as FrameNode;
            }
        }

        /// <summary>
        /// 由parentNode開始將同nodeType的Node抓出來放在nodeList裡。
        /// </summary>     
        public static void AddNodeSameType(FrameNode parentNode, List<BasicNode> nodeList, Type nodeType)
        {
            if (parentNode.GetType() == nodeType)
            {
                nodeList.Add(parentNode);
            }

            FrameNode c = parentNode.FirstChild as FrameNode;
            while (c != null)
            {
                AddNodeSameType(c, nodeList, nodeType);
                c = c.Sibling as FrameNode;
            }
        }

        /// <summary>
        /// 由parentNode開始將同nodeType的Node抓出來放在nodeList裡。
        /// </summary>     
        public static void AddNodeSameType(FrameNode nowNode, NodeArray nodeArray, Type nodeType)
        {
            bool MustTextured = false;
            bool MustVisible = false;
            AddNodeSameType(nowNode, nodeArray, nodeType, MustTextured, MustVisible);
        }

        /// <summary>
        /// 由parentNode開始將同nodeType的Node抓出來放在nodeList裡。
        /// </summary>     
        public static void AddNodeSameType(FrameNode nowNode, NodeArray nodeArray,
            Type nodeType, bool bMustTextured, bool bMustVisible)//BasicNode[] nodeArray, ref int nowAmount)
        {
            SceneNode node = nowNode as SceneNode;
            if (node != null && node.GetType() == nodeType)
            {
                if (bMustVisible && node.Visible)
                {
                    if (bMustTextured && node.IfTextured)
                        nodeArray.Add(node);
                    else if (bMustTextured == false)
                        nodeArray.Add(node);
                }
                else if (bMustVisible == false)
                {
                    if (bMustTextured && node.IfTextured)
                        nodeArray.Add(node);
                    else if (bMustTextured == false)
                        nodeArray.Add(node);
                }
            }

            FrameNode c = nowNode.FirstChild as FrameNode;
            while (c != null)
            {
                AddNodeSameType(c, nodeArray, nodeType, bMustTextured, bMustVisible);
                c = c.Sibling as FrameNode;
            }
        }

        public static void AddHasMeshUVNode(FrameNode nowNode, NodeArray nodeArray)
        {
            AddHasMeshUVNode(nowNode, nodeArray, false);
        }

        /// <summary>
        ///抓出有mesh資料的node(IfMeshData == true)，並非抓出型態為MeshNode的node。
        ///而且還要有UV貼圖軸
        /// </summary>
        public static void AddHasMeshUVNode(FrameNode nowNode, NodeArray nodeArray, bool ConsiderAddCreateShadowMap)//BasicNode[] nodeArray, ref int nowAmount)
        {
            SceneNode node = nowNode as SceneNode;
            if (node != null && node.IfMeshData && node.IfTextured)
            {
                if (ConsiderAddCreateShadowMap == true)
                {
                    if (node.IfRenderShadowMap == true)
                        nodeArray.Add(node);
                }
                else
                    nodeArray.Add(node);

                //nodeArray.nodeArray[nodeArray.nowNodeAmount] = node;
                //nodeArray.nowNodeAmount++;
            }

            FrameNode c = nowNode.FirstChild as FrameNode;
            while (c != null)
            {
                AddHasMeshUVNode(c, nodeArray, ConsiderAddCreateShadowMap);
                c = c.Sibling as FrameNode;
            }
        }

        /// <summary>
        ///抓出有mesh資料的node(IfMeshData == true)，並非抓出型態為MeshNode的node。
        /// </summary>
        public static void AddMeshNodeContainName(FrameNode nowNode, string nodeName, NodeArray nodeArray)//BasicNode[] nodeArray, ref int nowAmount)
        {
            SceneNode node = nowNode as SceneNode;
            if (node != null && node.IfMeshData && string.IsNullOrEmpty(node.NodeName) == false &&
                 node.NodeName.Contains(nodeName))
            {
                nodeArray.Add(node);
                //nodeArray.nodeArray[nodeArray.nowNodeAmount] = node;
                //nodeArray.nowNodeAmount++;
            }

            FrameNode c = nowNode.FirstChild as FrameNode;
            while (c != null)
            {
                AddMeshNodeContainName(c, nodeName, nodeArray);
                c = c.Sibling as FrameNode;
            }
        }

        /// <summary>
        /// 抓出有mesh資料的node(IfMeshData == true)，並非抓出型態為MeshNode的node。
        /// </summary>
        static public SceneNode SeekFirstMeshNodeAllName(FrameNode frameNode, string nodeName)
        {
            SceneNode sceneNode = frameNode as SceneNode;

            if (sceneNode != null && sceneNode.NodeName != null &&
                sceneNode.NodeName.Equals(nodeName) && sceneNode.IfMeshData)
            // (this.IndexBuffer != null && this.VertexBuffer != null))
            {
                return sceneNode;
            }
            else
            {
                FrameNode children = (FrameNode)frameNode.FirstChild;
                while (children != null)
                {
                    SceneNode seekNode = SeekFirstMeshNodeAllName(children, nodeName);

                    if (seekNode != null)
                        return seekNode;
                    else
                        children = (FrameNode)children.Sibling;
                }
                return null;
            }
        }

        /// <summary>
        /// 抓出有mesh資料的node(IfMeshData == true)，並非抓出型態為MeshNode的node。
        /// </summary>
        static public SceneNode SeekFirstMeshNodeContainName(FrameNode frameNode, string nodeName)
        {
            SceneNode sceneNode = frameNode as SceneNode;

            if (sceneNode != null && sceneNode.NodeName != null &&
                sceneNode.NodeName.Contains(nodeName) && sceneNode.IfMeshData)
            // (this.IndexBuffer != null && this.VertexBuffer != null))
            {
                return sceneNode;
            }
            else
            {
                FrameNode children = (FrameNode)frameNode.FirstChild;
                while (children != null)
                {
                    SceneNode seekNode = SeekFirstMeshNodeContainName(children, nodeName);

                    if (seekNode != null)
                        return seekNode;
                    else
                        children = (FrameNode)children.Sibling;
                }
                return null;
            }
        }

        /// <summary>
        /// 抓出有mesh資料的node(IfMeshData == true)，並非抓出型態為MeshNode的node。
        /// 抓樹葉末端節點的最後一個有MESH的NODE。
        /// </summary>
        static public void SeekLastLeafMeshNode(FrameNode frameNode, ref SceneNode leafNode)
        {
            SceneNode sceneNode = frameNode as SceneNode;

            if (sceneNode != null && sceneNode.m_IndexBufferSize > 0)
                leafNode = sceneNode;

            FrameNode children = frameNode.FirstChild as FrameNode;
            while (children != null)
            {
                SeekLastLeafMeshNode(children, ref leafNode);
                children = children.Sibling as FrameNode;
            }
        }

        /// <summary>
        /// 抓出有mesh資料的node(IfMeshData == true)，並非抓出型態為MeshNode的node。
        /// </summary>
        static public SceneNode SeekFirstMeshNode(FrameNode frameNode)
        {
            SceneNode sceneNode = frameNode as SceneNode;
            if (sceneNode != null && sceneNode.m_IndexBufferSize > 0)
            {
                return sceneNode;
            }
            else
            {
                FrameNode children = frameNode.FirstChild as FrameNode;
                while (children != null)
                {
                    SceneNode seekNode = SeekFirstMeshNode(children);

                    if (seekNode != null)
                        return seekNode;
                    else
                        children = children.Sibling as FrameNode;
                }
                return null;
            }
        }

        #endregion

        #region Render
        virtual public void Render(EffectRenderManager effectRenderManager,
            //bool bForce, bool bIgnoreCullMode,
            ref UInt32 renderTriangleAmount, GraphicsDevice device)
        {
            if (BeCulled == true)
                return;
            if (Visible == false)
                return;
            if (m_IndexBufferSize <= 0)
                return;

            renderTriangleAmount += (uint)m_IndexBufferSize / 3;
#if DEBUG
            if (m_bDrawBoundingBox)
                DrawBoundBox();
            if (m_bDrawBoundingSphere)
                DrawBoundingSphere(m_BoundingSphere.Radius);
#endif
            if (IfDrawAxis)
            {
                if (m_BoundingSphere.Radius > 0)
                    DrawAxis(m_BoundingSphere.Radius);
                else
                    DrawAxis(200f);
            }
            if (DynamicTexture != null)
            {
                DynamicTexture.RefreshTime(I_GetTime.ellipseUpdateMillisecond);
                m_BaseTexture = DynamicTexture.GetTexture();
            }
            else if (m_VideoPlayer != null)
            {
                m_BaseTexture = m_VideoPlayer.GetTexture();
            }

            if (DynamicNormalTexture != null)
            {
                DynamicNormalTexture.RefreshTime(I_GetTime.ellipseUpdateMillisecond);
                m_NormalTexture = DynamicNormalTexture.GetTexture();
            }

            SetNodeRenderState_Before(device);
            v_Render(effectRenderManager, device);
            SetNodeRenderState_After(device);
        }

        static bool bChangeDepthBufferWriteEnable = false;
        static float oldMipMapLevel = 0;
        //    static bool recAlphatest;
        static float RecMipMapLevel;
        void SetNodeRenderState_Before(GraphicsDevice device)
        {
            /*  recAlphatest = true;
              if (IfAlphaTestEnable == false)
              {
                  recAlphatest = device.RenderState.AlphaTestEnable;
                  device.RenderState.AlphaTestEnable = false;
              }*/

            RecMipMapLevel = device.SamplerStates[0].MipMapLevelOfDetailBias;
            if (MipmapLevel != 0 && MipmapLevel != oldMipMapLevel)//不為0的才吃他的設定值
            {
                oldMipMapLevel = MipmapLevel;
                device.SamplerStates[0].MipMapLevelOfDetailBias = oldMipMapLevel;
            }

            if (IfRenderDepth == false && device.RenderState.DepthBufferWriteEnable == true)
            {
                bChangeDepthBufferWriteEnable = true;
                device.RenderState.DepthBufferWriteEnable = false;
            }

            if (IfTwoSideDraw)
                device.RenderState.CullMode = CullMode.None;
        }

        void SetNodeRenderState_After(GraphicsDevice device)
        {
            //還原
            if (IfTwoSideDraw)
                device.RenderState.CullMode = CullMode.CullCounterClockwiseFace;

            if (IfRenderDepth == false && bChangeDepthBufferWriteEnable)
            {
                bChangeDepthBufferWriteEnable = false;
                device.RenderState.DepthBufferWriteEnable = true;
            }

            /*    if (IfAlphaTestEnable == false)
                    device.RenderState.AlphaTestEnable = recAlphatest;
                */
            if (oldMipMapLevel != RecMipMapLevel)
            {
                device.SamplerStates[0].MipMapLevelOfDetailBias = RecMipMapLevel;
                oldMipMapLevel = RecMipMapLevel;
            }
        }

#if DEBUG
        void DrawBoundBox()
        {
            Matrix world = WorldMatrix;
            Vector3[] corners = BoundingBox.GetCorners();
            for (int a = 0; a < corners.Length; a++)
                Vector3.Transform(ref corners[a], ref world, out corners[a]);

            DrawLine.AddLine(corners[0], corners[1], BoundBoxColor);
            DrawLine.AddLine(corners[1], corners[2], BoundBoxColor);
            DrawLine.AddLine(corners[2], corners[3], BoundBoxColor);
            DrawLine.AddLine(corners[3], corners[0], BoundBoxColor);

            DrawLine.AddLine(corners[4], corners[5], BoundBoxColor);
            DrawLine.AddLine(corners[5], corners[6], BoundBoxColor);
            DrawLine.AddLine(corners[6], corners[7], BoundBoxColor);
            DrawLine.AddLine(corners[7], corners[4], BoundBoxColor);

            DrawLine.AddLine(corners[0], corners[4], BoundBoxColor);
            DrawLine.AddLine(corners[1], corners[5], BoundBoxColor);
            DrawLine.AddLine(corners[2], corners[6], BoundBoxColor);
            DrawLine.AddLine(corners[3], corners[7], BoundBoxColor);
        }

        void DrawBoundingSphere(float radius)
        {
            DrawLine.AddSphereLine(WorldMatrix, BoundingSphere.Center, BoundingSphere.Radius, BoundSphereColor, 10, 10);
        }
#endif
        public static void DrawAxis(Matrix world, float fLength)
        {
            Vector3 tmpV;

            //right
            Vector3 start = world.Translation;
            tmpV = world.Right;
            //MyMath.GetMatrixRightElement(ref world, out tmpV);
            Vector3 end = world.Translation + tmpV * fLength;
            DrawLine.AddLine(start, end, Color.Red);

            //DrawLine.refreshLine(start, end);
            //DrawLine.Draw(view, projection, Color.Red);

            //up
            start = world.Translation;
            tmpV = world.Up;
            //MyMath.GetMatrixUpElement(ref world, out tmpV);
            end = world.Translation + tmpV * fLength;
            DrawLine.AddLine(start, end, Color.Green);

            //forward
            start = world.Translation;
            tmpV = world.Forward;
            //MyMath.GetMatrixForwardElement(ref world, out tmpV);
            end = world.Translation + tmpV * fLength;
            DrawLine.AddLine(start, end, Color.Blue);
        }
        void DrawAxis(float fLength)
        {
            DrawAxis(WorldMatrix, fLength);
        }
        #endregion

        #region 物件材質有關

        /// <summary>
        /// 此物件的材質庫編號，預設為default : 0，null表示default。
        /// </summary>        
        public string MaterialBankID = null;
        public int iMaterialBankID = 0;

        //Shader 參數----------------------------------------------------------------------------------------------------------
        //public Vector3 LightDirection = Vector3.Normalize(new Vector3(-0.3333333f, -0.6666667f, 0.6666667f));
        //public Vector3 LightAmbient = new Vector3(1.0f, 1.0f, 1.0f);
        //public Vector3 LightDiffuse = new Vector3(1.0f, 1.0f, 1.0f);
        //public Vector3 LightSpecular = new Vector3(1.0f, 1.0f, 1.0f);

        //public bool LightEnable = false;//是否開啟光計算
        public byte AlphaTestReference = 128;//colorA - AlphaTestReference <0 會於shader中clip( )。
        public float MipmapLevel = 0;
        //public Vector3 MaterialAmbient = new Vector3(0.7f, 0.7f, 0.7f);
        //public Vector3 MaterialDiffuse = new Vector3(0.7f, 0.7f, 0.7f);
        //public Vector3 MaterialSpecular = new Vector3(0.0f, 0.0f, 0.0f);
        //public int MaterialPower = 128;

        public float EmmisiveAlpha = 0;//自發光的強度，若為0表示無自發光，(目前皆為自體顏色發光無法設定其他顏色)

        //static public void SetMaterialDiffuseAmbientSpecularEmmisive(
        //    Vector3? Ambient, Vector3? Diffuse, Vector3? Specular, int Power,
        //    /*Vector3? Emmisive, */SceneNode node, bool setAllTree)
        //{
        //    if (Ambient != null)
        //        node.MaterialAmbient = (Vector3)Ambient;
        //    if (Diffuse != null)
        //        node.MaterialDiffuse = (Vector3)Diffuse;
        //    if (Specular != null)
        //    {
        //        node.MaterialSpecular = (Vector3)Specular;
        //        node.MaterialPower = Power;
        //    }
        //    //if (Emmisive != null)
        //    //{
        //    //    node.MaterialEmmisive = (Vector3)Emmisive;
        //    //}

        //    if (setAllTree)
        //    {
        //        SceneNode childrenNode = (SceneNode)node.FirstChild;
        //        while (childrenNode != null)
        //        {
        //            SetMaterialDiffuseAmbientSpecularEmmisive(Ambient, Diffuse, Specular, Power,
        //                /*Emmisive, */childrenNode, setAllTree);
        //            childrenNode = (SceneNode)childrenNode.Sibling;
        //        }
        //    }
        //}
        //Shader 參數----------------------------------------------------------------------------------------------------------

        #endregion

        #region 繪圖控制參數^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        uint m_RenderFeature = (uint)RenderFeature.Default;

        /// <summary>
        /// 是否此node要畫在shadow map上
        /// </summary>
        public bool IfRenderShadowMap
        {
            set
            {
                if (value)
                    m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.RenderShadowMap);
                else
                    m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.RenderShadowMap);
            }
            get
            {
                return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.RenderShadowMap);
            }
        }

        /// <summary>
        /// 是否有要貼圖
        /// </summary>
        public bool IfTextured
        {
            set
            {
                if (value)
                    m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.HasTexture);
                else
                    m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.HasTexture);
            }
            get
            {
                return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.HasTexture);
            }
        }

        /// <summary>
        /// 是否要畫2面
        /// </summary>
        public bool IfTwoSideDraw
        {
            set
            {
                if (value)
                    m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.DrawTwoSide);
                else
                    m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.DrawTwoSide);
            }
            get
            {
                return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.DrawTwoSide);
            }
        }

        /// <summary>
        /// 是否要畫在深度緩衝上
        /// </summary>
        public bool IfRenderDepth
        {
            set
            {
                if (value)
                    m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.RenderDepth);
                else
                    m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.RenderDepth);
            }
            get
            {
                return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.RenderDepth);
            }
        }

        /// <summary>
        /// 是否要畫半透明
        /// </summary>
        public bool IfAlphaBlenbing
        {
            set
            {
                if (value)
                    m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.DrawAlphaBlend);
                else
                    m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.DrawAlphaBlend);
            }
            get
            {
                return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.DrawAlphaBlend);
            }
        }

        /// <summary>
        /// 是否要受霧影響
        /// </summary>
        public bool IfFogEnable
        {
            set
            {
                if (value)
                    m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.FogEnable);
                else
                    m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.FogEnable);
            }
            get
            {
                return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.FogEnable);
            }
        }

        /*   public bool IfAlphaTestEnable
           {
               set
               {
                   if (value)
                       m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.AlphaTestEnable);
                   else
                       m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.AlphaTestEnable);
               }
               get
               {
                   return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.AlphaTestEnable);
               }
           }*/

        /// <summary>
        /// 是否要畫ShadowVolume，目前沒做這個
        /// </summary>
        public bool IfCreateShadowVolume
        {
            set
            {
                if (value)
                {
                    m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.CreateShadowVolume);
                }
                else
                {
                    m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.CreateShadowVolume);
                }
            }
            get
            {
                return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.CreateShadowVolume);
            }
        }


        public bool IfRenderGodsRay
        {
            set
            {
                if (value)
                    m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.RenderGodsRay);
                else
                    m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.RenderGodsRay);
            }
            get
            {
                return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.RenderGodsRay);
            }
        }

        /// <summary>
        /// 是否與光計算
        /// </summary>
        public bool IfLightEnable
        {
            set
            {
                if (value)
                    m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.LightEnable);
                else
                    m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.LightEnable);
            }
            get
            {
                return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.LightEnable);
            }
        }

        //public bool IfMipmap
        //{
        //    set
        //    {
        //        if (value)
        //            m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.MipmapEnable);
        //        else
        //            m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.MipmapEnable);
        //    }
        //    get
        //    {
        //        return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.MipmapEnable);
        //    }
        //}

        //public bool IfDrawWithReflectMap
        //{
        //    set
        //    {
        //        if (value)
        //            m_RenderFeature = MyMath.BitInsert(m_RenderFeature, (uint)RenderFeature.DrawWithReflectMap);
        //        else
        //            m_RenderFeature = MyMath.BitRemove(m_RenderFeature, (uint)RenderFeature.DrawWithReflectMap);
        //    }
        //    get
        //    {
        //        return MyMath.ConsiderBit(m_RenderFeature, (uint)RenderFeature.DrawWithReflectMap);
        //    }
        //}

        /// <summary>
        /// 設定node及底下tree node是否要light enable
        /// </summary>    
        static public void SetAllRenderShadowmap(FrameNode frameNode, bool enable)
        {
            SceneNode sceneNode = frameNode as SceneNode;
            if (sceneNode != null)
                sceneNode.IfRenderShadowMap = enable;

            FrameNode children = frameNode.FirstChild as FrameNode;
            while (children != null)
            {
                SetAllRenderShadowmap(children, enable);
                children = children.Sibling as FrameNode;
            }
        }

        /// <summary>
        /// 設定node及底下tree node是否要light enable
        /// </summary>    
        static public void SetAllLightEnable(FrameNode frameNode, bool enable)
        {
            SceneNode sceneNode = frameNode as SceneNode;
            if (sceneNode != null)
                sceneNode.IfLightEnable = enable;

            FrameNode children = frameNode.FirstChild as FrameNode;
            while (children != null)
            {
                SetAllLightEnable(children, enable);
                children = children.Sibling as FrameNode;
            }
        }

        static public void SetAllBlendColor(FrameNode frameNode, ref Color color)
        {
            SceneNode sceneNode = frameNode as SceneNode;
            if (sceneNode != null)
                sceneNode.BlendColor = color;

            FrameNode children = frameNode.FirstChild as FrameNode;
            while (children != null)
            {
                SetAllBlendColor(children, ref color);
                children = children.Sibling as FrameNode;
            }
        }

        /// <summary>
        /// 設定node及底下tree node是否要RenderDepth
        /// </summary>    
        public void SetAllRenderDepth(bool enable)
        {
            this.IfRenderDepth = enable;

            SceneNode children = this.FirstChild as SceneNode;
            while (children != null)
            {
                children.SetAllRenderDepth(enable);
                children = children.Sibling as SceneNode;
            }
        }

        public static void SetAllBeCulled(FrameNode parentNode, Type type)
        {
            if (parentNode.GetType() == type)
            {
                SceneNode sceneNode = parentNode as SceneNode;
                if (sceneNode != null)
                    sceneNode.BeCulled = true;
            }

            FrameNode c = parentNode.FirstChild as FrameNode;
            while (c != null)
            {
                SetAllBeCulled(c, type);

                c = c.Sibling as FrameNode;
            }
        }

        ///// <summary>
        ///// 設定node及底下tree node是否要AlphaBlend畫出
        ///// </summary>    
        //public void SetAllDrawAlphaBlend(bool alphaBlend)
        //{
        //    IfAlphaBlenbing = alphaBlend;

        //    SceneNode children = this.FirstChild as SceneNode;
        //    while (children != null)
        //    {
        //        children.SetAllDrawAlphaBlend(alphaBlend);
        //        children = children.Sibling as SceneNode;
        //    }
        //}

        //public void SetAllAlphaTestDisable(bool disable)
        //{
        //    this.AlphaTestDisable = disable;

        //    MeshNode children = this.FirstChild as MeshNode;
        //    while (children != null)
        //    {
        //        children.SetAllAlphaTestDisable(disable);
        //        children = children.Sibling as MeshNode;
        //    }
        //}

        /// <summary>
        /// 設定node及底下tree node是否要取消貼圖畫出
        /// </summary> 
        //public void SetAllNoTexture()
        //{
        //    IfTextured = false;

        //    SceneNode children = this.FirstChild as SceneNode;
        //    while (children != null)
        //    {
        //        children.SetAllNoTexture();
        //        children = children.Sibling as SceneNode;
        //    }
        //}

        static public void SetAllDrawTwoSide(FrameNode parentNode, bool bbb)
        {
            SceneNode sceneNode = parentNode as SceneNode;
            if (sceneNode != null)
                sceneNode.IfTwoSideDraw = bbb;

            FrameNode children = parentNode.FirstChild as FrameNode;
            while (children != null)
            {
                SetAllDrawTwoSide(children, bbb);
                children = children.Sibling as FrameNode;
            }
        }

        /// <summary>
        /// 設定node及底下tree node是否要取消貼圖畫出
        /// </summary> 
        static public void SetAllEmmisive(FrameNode frameNode, bool bEnable)
        {
            SceneNode sceneNode = frameNode as SceneNode;
            if (sceneNode != null)
            {
                sceneNode.EmmisiveAlpha = 0;
                if (bEnable)
                    sceneNode.EmmisiveAlpha = 1;
            }

            FrameNode children = frameNode.FirstChild as FrameNode;
            while (children != null)
            {
                SetAllEmmisive(children, bEnable);
                children = children.Sibling as FrameNode;
            }
        }

        /// <summary>
        /// 設定node及底下是否沒有貼圖就invisible(visible=false)畫出
        /// </summary> 
        static public void SetAllNoTextureVisible(FrameNode frameNode, bool bVisible)
        {
            SceneNode sceneNode = frameNode as SceneNode;
            if (sceneNode != null)
            {
                if (sceneNode.IfTextured == false)
                    sceneNode.Visible = bVisible;
            }

            FrameNode children = frameNode.FirstChild as FrameNode;
            while (children != null)
            {
                SetAllNoTextureVisible(children, bVisible);
                children = children.Sibling as FrameNode;
            }
        }
        #endregion

        #region 貼圖設定屬性^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        string m_SpecularMapFileName = null;
        string m_ReflectIntensityMapFileName = null;
        string m_EmmisiveMapFileName = null;
        string m_EnvCubeMapFileName = null;
        public Texture2D SpecularMap = null;
        public Texture2D ReflectIntensityMap = null;
        public Texture2D EmmisiveMap = null;
        public TextureCube EnvCubeMap = null;

        public string SpecularMapFileName { get { return m_SpecularMapFileName; } }
        public string ReflectIntensityMapFileName { get { return m_ReflectIntensityMapFileName; } }
        public string EmmisiveMapFileName { get { return m_EmmisiveMapFileName; } }
        public string EnvCubeMapFileName { get { return m_EnvCubeMapFileName; } }

        const bool bLoadFormXNB = false;//控制load圖檔模型檔的形式。
        public void SetSpecularMap(Texture2D specularMap)
        {
            if (SpecularMap != null)
            {
                SpecularMap.Dispose();
                SpecularMap = null;
            }
            SpecularMap = specularMap;
            m_SpecularMapFileName = null;
        }
        public void SetSpecularMap(string fileName, GraphicsDevice device, ContentManager content)
        {
            Texture2D tmpS = Utility.Load_Texture2D(device, content, fileName, true);
            SetSpecularMap(tmpS);
            m_SpecularMapFileName = fileName;
        }
        public void SetReflectIntensityMap(string fileName, GraphicsDevice device, ContentManager content)
        {
            Texture2D map = Utility.Load_Texture2D(device, content, fileName, true);
            SetReflectIntensityMap(map);
            m_ReflectIntensityMapFileName = fileName;
        }

        public void SetReflectIntensityMap(Texture2D reflectIntensityMap)
        {
            if (ReflectIntensityMap != null)
            {
                ReflectIntensityMap.Dispose();
                ReflectIntensityMap = null;
            }
            ReflectIntensityMap = reflectIntensityMap;
            m_ReflectIntensityMapFileName = null;

            if (ReflectIntensityMap != null)
                ReflectIntensity = 0.5f;//設定為1才會開啟計算功能
        }

        /// <summary>
        /// 設定自發光貼圖，會將m_EmmisiveMapFileName = null
        /// </summary>
        public void SetEmmisiveMap(Texture2D emmm)
        {
            if (EmmisiveMap != null)
            {
                EmmisiveMap.Dispose();
                EmmisiveMap = null;
            }
            EmmisiveMap = emmm;
            m_EmmisiveMapFileName = null;

            if (EmmisiveMap != null)
                EmmisiveAlpha = 1;
        }

        public void SetEmmisiveMap(string fileName, GraphicsDevice device, ContentManager content)
        {
            Texture2D map = Utility.Load_Texture2D(device, content, fileName, true);
            SetEmmisiveMap(map);
            m_EmmisiveMapFileName = fileName;
        }
        void ReleaseEnvCubeMap()
        {
            //ENV CUBE MAP有可能大家都會用同一張
            if (m_EnvCubeMapFileName != null && EnvCubeMap != null)//若是讀檔讀入，就要dispose。
                EnvCubeMap.Dispose();

            m_EnvCubeMapFileName = null;
            EnvCubeMap = null;
        }
        public void SetEnvCubeMap(string fileName, GraphicsDevice device, ContentManager content)
        {
            ReleaseEnvCubeMap();

            m_EnvCubeMapFileName = fileName;

            EnvCubeMap = Utility.Load_TextureCube(device, content, fileName);
        }
        public void SetEnvCubeMap(TextureCube envMap)
        {
            if (m_EnvCubeMapFileName != null && EnvCubeMap != null)//已經有Load檔設定就不要設定他ㄌ
                return;

            ReleaseEnvCubeMap();

            if (ReflectIntensity > 0)
                EnvCubeMap = envMap;
        }

        public static void SetAllEnvCubeMap(FrameNode parentNode, TextureCube envMap)
        {
            SceneNode node = parentNode as SceneNode;
            if (node != null)
                node.SetEnvCubeMap(envMap);

            FrameNode childnode = parentNode.FirstChild as FrameNode;
            while (childnode != null)
            {
                SetAllEnvCubeMap(childnode, envMap);
                childnode = childnode.Sibling as FrameNode;
            }
        }
        #endregion


        //#region Point Light參數^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //public const int ShaderMaxPointLight = 8;//在fx裡定義的PointLight陣列維度。
        //int m_PointLightAmount = 0;
        //PointLightNode[] m_PointLightList = null;
        //public int PointLightAmount
        //{
        //    set { m_PointLightAmount = value; }
        //    get { return m_PointLightAmount; }
        //}
        //public PointLightNode[] PointLightList
        //{
        //    get
        //    {
        //        if (m_PointLightList == null)
        //            m_PointLightList = new PointLightNode[ShaderMaxPointLight];
        //        return m_PointLightList;
        //    }
        //}
        //#endregion

        //#region spot Light參數
        //SpotLight m_SpotLight = null;
        //public void SetSpotLight(float theta, float phi, float strength, float decay, float range, Vector3 color)
        //{
        //    m_SpotLight = new SpotLight();
        //    m_SpotLight.angleTheta = 1f - theta / 180f;//把角度轉乘dot的值。內圈的dot一定會比外圈大
        //    m_SpotLight.anglePhi = 1f - phi / 180f;
        //    m_SpotLight.strength = strength;
        //    m_SpotLight.decay = decay;
        //    m_SpotLight.range = range;
        //    m_SpotLight.colorValue = color;
        //}
        //public SpotLight GetSpotLight()
        //{
        //    if (m_SpotLight == null)
        //        return null;
        //    m_SpotLight.position = WorldPosition;
        //    m_SpotLight.direction = WorldForward;
        //    return m_SpotLight;
        //}

        ///// <summary>
        /////  將有SpotLight屬性(GetSpotLight() != null)的scene node抓進nodeArray來。
        ///// </summary>  
        //static public void AddSpotLightNode(FrameNode parentNode, NodeArray nodeArray)
        //{
        //    SceneNode nowNode = parentNode as SceneNode;
        //    if (nowNode != null && nowNode.GetSpotLight() != null)
        //        nodeArray.Add(nowNode);

        //    FrameNode node = parentNode.FirstChild as FrameNode;
        //    while (node != null)
        //    {
        //        AddSpotLightNode(node, nodeArray);
        //        node = node.Sibling as FrameNode;
        //    }
        //}
        //#endregion

        #region 判斷並設定SceneNode的屬性設定
        /// <summary>
        /// 判斷並設定SceneNode的material
        /// </summary> 
        static public void NodeMaterialChange(SpecialMeshDataManager.MeshMaterial material, SceneNode node, ContentManager content, GraphicsDevice device)
        {
            if (material == null) return;
            SpecialMeshDataManager.MeshMaterial.SetToNode(node, material, content, device);
        }

        /// <summary>
        /// 判斷並設定SceneNode的translateUV
        /// </summary> 
        static public void NodeTranslateUVChange(SpecialMeshDataManager.TranslateUVMesh translateUV, SceneNode node)
        {
            if (translateUV == null) return;
            SpecialMeshDataManager.TranslateUVMesh.SetToNode(node, translateUV);
        }

        /// <summary>
        /// 判斷並設定SceneNode的renderFeature
        /// </summary> 
        static public void NodeRenderFeatureChange(SpecialMeshDataManager.RenderFeature renderFeature, SceneNode node, ContentManager content, GraphicsDevice device)
        {
            if (renderFeature == null) return;
            SpecialMeshDataManager.RenderFeature.SetToNode(node, renderFeature, content, device);
        }

        /// <summary>
        /// 判斷並設定SceneNode的dynamicTexture
        /// </summary> 
        static public void NodeDynamicTextureChange(SpecialMeshDataManager.DynamicTextureMesh dynamicTexture, SceneNode node, ContentManager content, GraphicsDevice device)
        {
            if (dynamicTexture == null)
                node.RemoveDynamicTexture();
            else
            {
                node.SetDynamicTexture(dynamicTexture.textureName, dynamicTexture.fileNumberDigit,
                    dynamicTexture.startPlayTime, dynamicTexture.startFileNumber, dynamicTexture.endFileNumber,
                    dynamicTexture.timeIntervel, dynamicTexture.loopTo, dynamicTexture.FileSubName, content, device);
            }
        }

        /// <summary>
        /// 判斷並設定SceneNode的dynamicNormalTexture
        /// </summary>
        static public void NodeDynamicNormalTextureChange(SpecialMeshDataManager.DynamicTextureMesh dynamicNormalTexture, SceneNode node, ContentManager content, GraphicsDevice device)
        {
            if (dynamicNormalTexture == null)
                node.RemoveDynamicNormalTexture();
            else
            {
                node.SetDynamicNormalTexture(dynamicNormalTexture.textureName, dynamicNormalTexture.fileNumberDigit,
                    dynamicNormalTexture.startPlayTime, dynamicNormalTexture.startFileNumber, dynamicNormalTexture.endFileNumber,
                    dynamicNormalTexture.timeIntervel, dynamicNormalTexture.loopTo, dynamicNormalTexture.FileSubName, content, device);
            }
        }

        static public void NodeRSTDataChange(SpecialMeshDataManager.RSTData rstData, SceneNode node)
        {
            if (rstData != null)
                node.LocalMatrix = rstData.rst.mat;
        }

        static public void NodeNormalMapChange(string normalMapFileName, SceneNode node, ContentManager content)
        {
            if (string.IsNullOrEmpty(normalMapFileName))
            {
                node.RemoveNormalTexture();
            }
            else
            {
                node.SetNormalTexture(normalMapFileName, content);
            }
        }
        #endregion

        SpecialMeshDataManager.WindData m_WindData;
        public SpecialMeshDataManager.WindData WindData
        {
            get { return m_WindData; }
        }


        static List<uint> ib = new List<uint>(2048);
        static List<Vector3> vb = new List<Vector3>(2048);
        static List<Vector3> nb = new List<Vector3>(2048);

        /// <summary>
        /// 風的和草屬性設定
        /// </summary>
        static public void NodeWindDataChange(
            SpecialMeshDataManager.WindData windData, SceneNode node,
           
            GraphicsDevice device, ContentManager content)
        {
            node.m_WindData = windData;
            node.Visible = true;

            //先抓出底下的grass node將他幹掉
            FrameNode sibling = node.FirstChild as FrameNode;
            while (sibling != null)
            {
                GrassNode gnode = sibling as GrassNode;
                if (gnode != null)
                {
                    gnode.DetachFromParent();
                    gnode.Dispose();
                    gnode = null;
                    break;
                }
                sibling = sibling.Sibling as FrameNode;
            }

            //只有風的搖動資訊就直接設定給node
            if (windData != null && windData.grassData == null)
            {
                node.m_WindData = windData;
            }
            //有grass data就將模型隱藏(隱藏的原因是可以另外製作有規律的格子型三角面範圍)，在他下面生grass mesh。
            else if (windData != null && windData.grassData != null)
            {
                GrassNode grassNode = GrassNode.Create();
                grassNode.m_WindData = windData;

                ib.Clear();
                vb.Clear();
                nb.Clear();

                int partID;
                ModelMesh mesh = node.GetModelMeshPart(out partID);
                Collision.GetMeshVBIB(mesh, ib, vb, nb, null, null, null);
                //Matrix nnnnn= node.WorldMatrix;
                //Vector3[] vvvbbb = new Vector3[vb.Count];
                Vector3[] vvvbbb = vb.ToArray();
                //Vector3.Transform(vb.ToArray(), ref nnnnn, vvvbbb);
                if (grassNode.CreateGrassData(windData.grassData, device, content, ib.ToArray(), vvvbbb, nb.ToArray()))
                {
                    //node.m_WindData = null;
                    node.Visible = false;

                    node.AttachChild(grassNode);

                    grassNode.NodeName = node.NodeName + "_GrassMesh";
                    grassNode.LocalMatrix = Matrix.Identity;
                    //shadowVolumeNode.IfAlphaBlenbing = true;
                

                    grassNode.SetBaseTexture(Utility.Load_Texture2D(device, content, windData.grassData.textureFilename, false));
                    grassNode.AnalyzeMeshBoundingData((Dictionary<string, object>)mesh.Tag, out grassNode.m_IndexBufferSize);
                }
                else
                {
                    grassNode.Dispose();
                    grassNode = null;
                }

                ib.Clear();
                vb.Clear();
                nb.Clear();
                //node.SetGrassTexture(windData.grassData.textureFilename, device, content);
            }
        }

        //Set the sound effects to use
        public string SoundEffectFileName = null;
        SoundEffect m_SoundEffect = null;
        SoundEffectInstance m_SoundEffectInstance = null;

        /// <summary>
        /// filename傳入null表示停止，
        /// </summary>   
        public void SoundEffectPlay(ContentManager content, string filename, float Volume0_1)
        {
            //FILE NAME不一樣就作處理，先釋放，傳入檔名NULL就不再作撥放了。
            if (SoundEffectFileName != filename ||
               ((SoundEffectFileName == filename) && (m_SoundEffectInstance != null) && (m_SoundEffectInstance.State == SoundState.Stopped))//如果同樣檔名且，有聲音且，已經聲音停了就重建
               )
            {
                SoundEffectFileName = filename;
                if (m_SoundEffectInstance != null)
                {
                    //m_SoundEffectInstance.Stop();
                    m_SoundEffectInstance.Dispose();
                    m_SoundEffectInstance = null;
                }

                if (string.IsNullOrEmpty(SoundEffectFileName))
                    return;

                try
                {
                    m_SoundEffect = content.Load<SoundEffect>(Utility.GetFullPath(filename));
                }
                catch (Exception e)
                {
                    Utility.ShowErrorMessage(e);
                }
                m_SoundEffectInstance = m_SoundEffect.CreateInstance();
            }
            if (string.IsNullOrEmpty(SoundEffectFileName))
                return;

            if (m_SoundEffectInstance.State == SoundState.Stopped)
            {
                m_SoundEffectInstance.Volume = Volume0_1;
                m_SoundEffectInstance.IsLooped = false;
                m_SoundEffectInstance.Play();
            }
            return;
        }

        public bool IfSoundEffectPlay()
        {
            if (m_SoundEffectInstance != null && m_SoundEffectInstance.State == SoundState.Playing)
                return true;
            return false;
        }

        //public void SetSoundEffectStop()
        //{
        //    m_SoundEngineInstance.Stop();
        //}

        //public void SetSoundEffectPause()
        //{
        //    m_SoundEngineInstance.Pause();
        //}

        public static void AddVisibleNode(FrameNode nowNode, NodeArray nodeArray)
        {
            SceneNode snode = nowNode as SceneNode;
            if (snode != null && snode.Visible == true)
            {
                nodeArray.nodeArray[nodeArray.nowNodeAmount] = snode;
                nodeArray.nowNodeAmount++;
            }

            FrameNode c = (FrameNode)nowNode.FirstChild;
            while (c != null)
            {
                AddVisibleNode(c, nodeArray);
                c = (FrameNode)c.Sibling;
            }
        }

        bool m_bVisible = true;//設定是否為可視物件
        public bool Visible { get { return m_bVisible; } set { m_bVisible = value; } }

        public static void SetAllVisible(FrameNode node, bool visible)
        {
            SceneNode snode = node as SceneNode;
            if (snode != null)
            {
                snode.Visible = visible;
            }

            FrameNode c = node.FirstChild as FrameNode;
            while (c != null)
            {
                SetAllVisible(c, visible);
                c = c.Sibling as FrameNode;
            }
        }
    }
}
