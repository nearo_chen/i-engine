﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using System.IO;
using I_XNAUtility;

namespace I_XNAComponent
{
    /// <summary>
    /// 控制Effect參數，以及繪圖。
    /// </summary>
    public partial class EffectRenderManager : IDisposable
    {

        /// <summary>
        /// 設定CurrentTechnique，Effect.Begin()還有Effect的CurrentTechnique.Passes[0].Begin()
        /// </summary>
        public void SetRenderBegin_Simple(SceneNode.RenderTechnique currentTechnique)
        {
            //m_CurrentRenderTechnique = currentTechnique;               
            if (currentTechnique ==
                MeshNode.RenderTechnique.SimpleMesh)
            {
                m_RenderEffect.CurrentTechnique = m_RenderEffect.Techniques["SimpleMesh"];
            }
            else if (currentTechnique ==
                (MeshNode.RenderTechnique.SimpleMesh |
                SceneNode.RenderTechnique.TengentMesh))
            {
                m_RenderEffect.CurrentTechnique = m_RenderEffect.Techniques["SimpleMesh_TengentMesh"];
            }
            else if (currentTechnique == MeshNode.RenderTechnique.SkinnedMesh)
            {
                m_RenderEffect.CurrentTechnique = m_RenderEffect.Techniques["SkinningMesh"];
            }
            else if (currentTechnique ==
                (MeshNode.RenderTechnique.SkinnedMesh |
                SceneNode.RenderTechnique.TengentMesh))
            {
                m_RenderEffect.CurrentTechnique = m_RenderEffect.Techniques["SkinningMesh_TengentMesh"];
            }
            else if (currentTechnique == SceneNode.RenderTechnique.GrassMesh)
            {
                m_RenderEffect.CurrentTechnique = m_RenderEffect.Techniques["GrassMesh"];            
            }

            m_RenderEffect.Begin();
            m_RenderEffect.CurrentTechnique.Passes[0].Begin();
        }


        /// <summary>
        /// Deffer shading於畫出MeshNode專用的設定參數，及一些相關設定，
        /// 並呼叫MeshNode的Render畫出。
        /// </summary>
        public void Simple_RenderMeshNode(MeshNode meshNode, ref uint triangle, GraphicsDevice device, bool bSetNormalMap)
        {
          //  Simple_SetCommonNodeEffectPar(meshNode);
            UpdateTime(meshNode);
            UpdateUVAnimation(meshNode);

            SetTextureEffectPar(meshNode, bSetNormalMap);
            SetEmmisiveEffectPar(meshNode);

            SetBlendColorEffectPar(meshNode);
            SetObjectAlphaEffectPar(meshNode);

            if (oldLightEnable != meshNode.IfLightEnable)
            {
                oldLightEnable = meshNode.IfLightEnable;
                parLightEnable.SetValue(meshNode.IfLightEnable);
            }

            meshNode.Render(this, ref triangle, device);
        }

        /// <summary>
        /// Deffer shading於畫出SkinningMeshNode專用的設定參數，及一些相關設定，
        /// 並呼叫SkinningMeshNode的Render畫出。
        /// </summary>
        public void Simple_RenderSkinningMeshNode(SkinningMeshNode skinNode, ref uint triangle, GraphicsDevice device, bool bSetNormalMap)
        {
            SetTextureEffectPar(skinNode, bSetNormalMap);
            SetEmmisiveEffectPar(skinNode);
            
            skinNode.Render(this, ref triangle, device);
        }

        /// <summary>
        /// Grass Mesh畫出專用的設定參數，及一些相關設定，
        /// 並呼叫GrassNode的Render畫出。
        /// </summary>
        public void Simple_RenderGrassMeshNode(GrassNode grassNode, ref uint triangle, GraphicsDevice device)
        {
            UpdateTime(grassNode);
            UpdateUVAnimation(grassNode);
            SetTextureEffectPar(grassNode, false);

            SetWindWaveEffectpar(grassNode);

            grassNode.Render(this, ref triangle, device);
        }

        /// <summary>
        /// 於繪製所有Node之前，設定通用的Effect參數，像是Projection，Fog...平行光之類。
        /// </summary>
        public void Simple_SetCommonEffectPar(ref Matrix projection, ref Matrix view, ref Vector3 cameraPosition,
            ref Vector3 fogColor, float FogStart, float FogEnd, float FogMaxRatio,
            ref Vector3 DLightDirection, ref Vector3 DLight_a, ref Vector3 DLight_d, ref Vector3 DLight_s)
        {
            parProjection.SetValue(projection);
            parView.SetValue(view);
            parViewPosition.SetValue(cameraPosition);

            SetFog(FogStart, FogEnd, FogMaxRatio, ref fogColor);
            SetDirectionalLight1(ref DLight_a, ref DLight_d, ref DLight_s, ref DLightDirection);
        }

     

    
    }
}
