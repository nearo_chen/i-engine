﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

using I_XNAUtility;

namespace I_XNAComponent
{
    public class CubeMapCreator : IDisposable
    {
        //public bool IfRenderOK = false;
        int targetIndex = 0;
        RenderTargetCube m_RTCube = null;
        public TextureCube TextureCube { get { return m_RTCube.GetTexture(); } }
        public CubeMapCreator(GraphicsDevice device, int size)
        {
            //IfRenderOK = false;
            m_RTCube = new RenderTargetCube(device, size, 1, SurfaceFormat.Bgr32, RenderTargetUsage.PreserveContents);
        }
        ~CubeMapCreator()
        {
            Dispose();
        }
        public void Dispose()
        {
            if (m_RTCube != null)
            {
                m_RTCube.Dispose();
                m_RTCube = null;
            }
        }

        /// <summary>
        /// 傳出cameraView給外面畫物件
        /// </summary>
        void GetViewMatrix(CubeMapFace cubeFace, Vector3 cubePosition, out Matrix cameraView)
        {
            //X 和Z面都對掉，在shader裡使用zyx
            Vector3 lookDir, cameraUp;
            //Matrix fixMat;
            switch (cubeFace)
            {
                //case CubeMapFace.PositiveX:
                case CubeMapFace.PositiveZ:
                    lookDir = Vector3.UnitX * 0.999f;
                    cameraUp = Vector3.UnitY;
                    break;
                //case CubeMapFace.NegativeX:
                case CubeMapFace.NegativeZ:
                    lookDir = Vector3.UnitX * -0.999f;
                    cameraUp = Vector3.UnitY;
                    break;
                case CubeMapFace.PositiveY:
                    lookDir = Vector3.UnitY;
                    //cameraUp = Vector3.UnitZ * -0.99f;
                    cameraUp = Vector3.UnitX * -0.999f;
                    break;
                case CubeMapFace.NegativeY:
                    lookDir = Vector3.UnitY * -0.999f;
                    //cameraUp = Vector3.UnitZ;
                    cameraUp = Vector3.UnitX;
                    break;

                //case CubeMapFace.PositiveZ:
                case CubeMapFace.PositiveX:
                    lookDir = Vector3.UnitZ * 0.999f;
                    cameraUp = Vector3.UnitY;
                    break;

                default:
                    //case CubeMapFace.NegativeZ:
                    lookDir = Vector3.UnitZ * -0.999f;
                    cameraUp = Vector3.UnitY;
                    break;
            }
            //cameraView = Matrix.CreateLookAt(cubePosition, cubePosition + lookDir * 100, cameraUp);
            //cameraView = Matrix.Identity;
            MyMath.MatrixSetDirection(cubePosition, lookDir, cameraUp, out cameraView);  
            Matrix.Invert(ref cameraView, out cameraView);
           
            //Vector3 lookDir, cameraUp;
            //Matrix fixMat;
            //switch (cubeFace)
            //{
            //    case CubeMapFace.PositiveX:
            //        lookDir = Vector3.UnitX;
            //        cameraUp = Vector3.UnitY;
            //        Matrix.CreateRotationY((float)(Math.PI * -.5), out fixMat);
            //        break;
            //    case CubeMapFace.NegativeX:
            //        lookDir = Vector3.UnitX * -1f;
            //        cameraUp = Vector3.UnitY;
            //        Matrix.CreateRotationY((float)(Math.PI * -.5), out fixMat);
            //        break;
            //    case CubeMapFace.PositiveY:
            //        lookDir = Vector3.UnitY;
            //        cameraUp = Vector3.UnitZ * -1f ;
            //        Matrix.CreateRotationZ((float)(Math.PI * -.5), out fixMat);
            //        break;
            //    case CubeMapFace.NegativeY:
            //        lookDir = Vector3.UnitY * -1f;
            //        cameraUp = Vector3.UnitZ;
            //        Matrix.CreateRotationZ((float)(Math.PI * -.5), out fixMat);
            //        break;
                
            //    case CubeMapFace.PositiveZ:
            //        lookDir = Vector3.UnitZ;
            //        cameraUp = Vector3.UnitY;
            //        Matrix.CreateRotationY((float)(Math.PI * .5), out fixMat);
            //        break;

            //    default:
            //    //case CubeMapFace.NegativeZ:
            //        lookDir = Vector3.UnitZ * -1f;
            //        cameraUp = Vector3.UnitY;
            //        Matrix.CreateRotationY((float)(Math.PI * .5), out fixMat);
            //        break;
            //}

            //cameraView = Matrix.Identity;            
            //MyMath.MatrixSetDirection(cubePosition, lookDir, cameraUp, out cameraView);      
            //Matrix.Multiply(ref fixMat, ref cameraView, out cameraView);
            //Matrix.Invert(ref cameraView, out cameraView);
        }

        /// <summary>
        /// 設定畫一個面，回傳camera的view matrix給外面設定。
        /// </summary> 
        public Matrix SetRenderTarget_1Face(GraphicsDevice device, int RTID, Vector3 cameraPosition)
        {
            Matrix viewEnv;
            GetViewMatrix((CubeMapFace)targetIndex, cameraPosition, out viewEnv);
            //SetRenderTarget(device, 0, (CubeMapFace)targetIndex);
            device.SetRenderTarget(RTID, m_RTCube, (CubeMapFace)targetIndex);
         
            targetIndex++;
            if (targetIndex >= 6)
            {
                //IfRenderOK = true;
                targetIndex = 0;
            }
            return viewEnv;

        }



        //public void SetRenderTarget(GraphicsDevice device, int RTID, CubeMapFace cubeFace)
        //{
        //    device.SetRenderTarget(RTID, m_RTCube, cubeFace);
        //}

        public void SaveCubeMap(string filePath)
        {
            TextureCube.Save(filePath, ImageFileFormat.Dds);         
        }
    }
}
