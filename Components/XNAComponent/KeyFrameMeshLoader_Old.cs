﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Audio;
//using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Net;
//using Microsoft.Xna.Framework.Storage;
//using I_XNAUtility;
//using MeshDataDefine;

//namespace I_XNAComponent
//{
//    /// <summary>
//    /// KeyframeMeshLoader不是很需要的class，因為KeyFrameMesh2裡面已經有SceneMeshLoader了，
//    /// KeyframeMeshLoader存粹是為了統一底層而寫的，如此和SceneMeshLoader，SkinningMeshLoader，
//    /// 就都有統一的底層BasicMeshLoader可供界面繪圖存取操控，使得KeyframeMeshLoader在
//    /// 界面上可提供單讀讀檔。
//    /// 之後可以藉由BasicMeshLoader統一讀取SpecialMeshData，但因SceneMeshLoader也是繼承BasicMeshLoader
//    /// KeyFrameMesh2裡面就已經有SceneMeshLoader了，所以KeyframeMeshLoader更顯得沒存在意義。
//    /// </summary>
//    public class KeyframeMeshLoader : BasicMeshLoader, IAnimationPlay
//    {
//        public static KeyframeMeshLoader Create()
//        {
//            return ReleaseCheck.CreateNew(typeof(KeyframeMeshLoader)) as KeyframeMeshLoader;
//        }

//        AnimationTableDataArray m_AnimationTableDataArray;
//        public AnimationTableDataArray AniArray { get { return m_AnimationTableDataArray; } }
//        KeyFrameMesh2 m_KeyFrameMesh;

//        public KeyframeMeshLoader()
//        {
//            m_KeyFrameMesh = null;
//        }

//        ~KeyframeMeshLoader()
//        {

//        }

//        protected override void v_Dispose()
//        {
//            if (m_KeyFrameMesh != null)
//                m_KeyFrameMesh.Dispose();
//            m_KeyFrameMesh = null;
//            m_AnimationTableDataArray = null;
//        }


//        /// <summary>
//        /// 讀取骨架模型並建立bone tree結構。
//        /// </summary>
//        AnimationTableDataArray LoadFile(string modelFile, ContentManager content, GraphicsDevice device)
//        {
//            m_KeyFrameMesh = KeyFrameMesh2.Create();
//            m_AnimationTableDataArray = m_KeyFrameMesh.LoadContent(modelFile, content, device);

//            InitBasicMeshLoader(m_KeyFrameMesh.GetRootNode,
//                m_KeyFrameMesh.GetBoundingSphere()
//                , m_KeyFrameMesh.GetBoundingBox());

//            m_KeyFrameMesh.GetRootNode.NodeName = "KeyFrameMeshLoader";
//            m_KeyFrameMesh.SetAnimationPlay(0, true, 1, 0);

//            GetRootNode.AttachChild(m_KeyFrameMesh.GetRootNode);
//            return m_AnimationTableDataArray;
//        }

//        protected override void v_Update(int ellapseMillisecond, ContentManager content, GraphicsDevice device)
//        {
//            m_KeyFrameMesh.UpdateKeyFrameTransform(ellapseMillisecond);
//        }

//        public override void RenderDebug(GraphicsDevice device, Vector2 Pos2D, Vector2 range2D)
//        {
//            //m_SkinnedNode.RenderDebug();
//        }

//        /// <summary>
//        /// 單純讀取動態keyframe檔，傳入檔案路徑，回傳讀取完成的keyframe mesh loader。
//        /// </summary>
//        static public AnimationTableDataArray LoadKeyframeMeshLoader(string fileName, ContentManager content, GraphicsDevice device,
//            out KeyframeMeshLoader keyframeMeshLoader)
//        {
//            keyframeMeshLoader = null;
//            if (string.IsNullOrEmpty(fileName))
//                return null;

//            keyframeMeshLoader = KeyframeMeshLoader.Create();
//            return keyframeMeshLoader.LoadFile(fileName, content, device);
//        }


//        public void AllAnimationStop()
//        {
//            m_KeyFrameMesh.AllAnimationStop();
//            SetAllMuliObjectPlayerStop();
//        }

//        public void AnimatedWithRatio(int id, float ratio)
//        {
//            m_KeyFrameMesh.AnimatedWithRatio(id, ratio);
//        }

//        public void SetAnimationPlay(int animation, bool bLoop, float speed, float startPercent)
//        {
//            m_KeyFrameMesh.SetAnimationPlay(animation, bLoop, speed, startPercent);

//            SetAnimationWithMOPPlay(animation);
//        }

//        public void SetAnimationStop(int animation)
//        {
//            m_KeyFrameMesh.SetAnimationStop(animation);
//        }

//        public void SetAnimationPlay_BlendSwitch(int animation, bool bLoop, float speed, float startPercent, int switchTime)
//        {
//            m_KeyFrameMesh.SetAnimationPlay_BlendSwitch(animation, bLoop, speed, startPercent, switchTime);

//            SetAnimationWithMOPPlay(animation);
//        }

//        public void SetBlendAnimationPlay(int animation, bool bLoop, float speed, float startPercent)
//        {
//            m_KeyFrameMesh.SetBlendAnimationPlay(animation, bLoop, speed, startPercent);
//        }

//        public void SetBlendAnimationRatio(float blendRatio)
//        {
//            m_KeyFrameMesh.SetBlendAnimationRatio(blendRatio);
//        }

//        public bool IfAnimationPlay(int id)
//        {
//            return m_KeyFrameMesh.IfAnimationPlay(id);
//        }

//        public Matrix[] GetBoneAllKeyframes(string meshName)
//        {
//            return m_KeyFrameMesh.GetBoneAllKeyframes(meshName);
//        }

//        public int GetNowAnimationPlayKeyframeBlockID(string meshName)
//        {
//            return m_KeyFrameMesh.GetNowAnimationPlayKeyframeBlockID(meshName);
//        }

//        public float GetNowAnimationPlayPercent()
//        {
//            return m_KeyFrameMesh.GetNowAnimationPlayPercent();
//        }

//        //#region 給動態模型用的，來判斷Multi Object Player與動作的關係
//        ///// <summary>
//        ///// 紀錄animation的ID，以及會與他一同播放的許多MOP node
//        ///// </summary>
//        //struct Animation_MOP
//        //{
//        //    public int animationID;
//        //    public LocatorNode[] MOPNodes;
//        //}
//        //Animation_MOP[] m_Animation_MOP;


//        //public void AnimationWithMOP_SetData(SpecialMeshDataManager.AnimateWithMOP[] animateWithMOPs)
//        //{
//        //    if (animateWithMOPs == null)
//        //        return;

//        //    if (m_AnimationTableDataArray == null)
//        //    {
//        //        m_Animation_MOP = new Animation_MOP[1];
//        //        for (int a = 0; a < animateWithMOPs.Length; a++)
//        //        {
//        //           // int id = 0;
//        //            m_Animation_MOP[a].animationID = 0;

//        //            string[] names = animateWithMOPs[a].MOPLocatorNames;
//        //            m_Animation_MOP[a].MOPNodes = new LocatorNode[names.Length];
//        //            for (int b = 0; b < names.Length; b++)
//        //            {
//        //                m_Animation_MOP[0].MOPNodes[b] =
//        //                    GetRootNode.SeekFirstNodeAllName(
//        //                    SpecialMeshDataManager.LocatorList.GetLocatorRootNodeName(names[b])) as LocatorNode;
//        //            }
//        //        }
//        //    }
//        //    else
//        //    {
//        //        m_Animation_MOP = new Animation_MOP[animateWithMOPs.Length];
//        //        for (int a = 0; a < animateWithMOPs.Length; a++)
//        //        {
//        //            //找出m_AnimationTableDataArray裡的ID
//        //            int id = -1;
//        //            for (int b = 0; b < m_AnimationTableDataArray.AnimationDataList.Length; b++)
//        //            {
//        //                if (m_AnimationTableDataArray.AnimationDataList[b].ActionName ==
//        //                    animateWithMOPs[a].AnimationName)
//        //                {
//        //                    id = b;
//        //                    break;
//        //                }
//        //            }

//        //            m_Animation_MOP[a].animationID = id;

//        //            string[] names = animateWithMOPs[a].MOPLocatorNames;
//        //            m_Animation_MOP[a].MOPNodes = new LocatorNode[names.Length];
//        //            for (int b = 0; b < names.Length; b++)
//        //            {
//        //                m_Animation_MOP[a].MOPNodes[b] =
//        //                    GetRootNode.SeekFirstNodeAllName(SpecialMeshDataManager.LocatorList.GetLocatorRootNodeName(names[b]))
//        //                    as LocatorNode;
//        //            }
//        //        }
//        //    }
//        //}

//        ///// <summary>
//        ///// 根據目前撥放的動作，判斷有哪些MOP要啟動，傳入ID -1表示要全部停止就好。
//        ///// </summary>
//        //void AnimationWithMOP_CheckPlay(int animationID)
//        //{
//        //    if (m_Animation_MOP == null)
//        //        return;

//        //    //先全關掉
//        //    SetAllMuliObjectPlayerStop();

//        //    if (animationID == -1)
//        //        return;

//        //    for (int a = 0; a < m_Animation_MOP.Length; a++)
//        //    {
//        //        if (m_Animation_MOP[a].animationID == animationID)
//        //        {
//        //            //根此動作有關的全都要開
//        //            for (int b = 0; b < m_Animation_MOP[a].MOPNodes.Length; b++)
//        //                SetMuliObjectPlayerPlay(m_Animation_MOP[a].MOPNodes[b]);
//        //        }
//        //        //else
//        //        //{
//        //        //    //根此動作無關的全都要關
//        //        //    for (int b = 0; b < m_Animation_MOP[a].MOPNodes.Length; b++)
//        //        //        SetMuliObjectPlayerStop(m_Animation_MOP[a].MOPNodes[b]);
//        //        //}
//        //    }
//        //}
//        //#endregion
//    }
//}
