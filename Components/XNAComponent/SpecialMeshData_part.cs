﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using System.Xml.Serialization;
using System.IO;


using I_XNAUtility;


namespace I_XNAComponent
{
    //處理模型檔裡面的mesh，會有的特殊屬性。

    public partial class SpecialMeshDataManager
    {

        //固定要的方向光，另外儲存方向光和霧的設定檔。
        //public static Vector3 LightDirection = Vector3.One;
        //public static Vector3 LightAmbient = Vector3.One;
        //public static Vector3 LightDiffuse = Vector3.One;
        //public static Vector3 LightSpecular = Vector3.One;
        //public static Vector3 FogColor = Vector3.One;
        //public static float FogStart = 0;
        //public static float FogEnd = 100000;

        //public static float BigShadowMapMultiSampleOffset = 10;
        //public static float SmallShadowMapMultiSampleOffset = 5;

        //public static float BigShadowMapRatio = 10;
        //public static float SmallShadowMapRatio = 5;

        //public static float BigShadowMapBias = 0.001f;
        //public static float SmallShadowMapBias = 0.001f;

        ///// <summary>
        ///// 單純讀取場景檔，傳入檔案路徑，回傳讀取完成的scene loader。
        ///// </summary>
        //public static SceneMeshLoader LoadScene_XNB(string fileName, ContentManager content, GraphicsDevice device)
        //{
        //    if (string.IsNullOrEmpty(fileName))
        //        return null;

        //    fileName = Utility.RemoveSubFileName(fileName);
        //    SceneMeshLoader sceneLoader = null;
        //    SceneMeshLoader.LoadSceneMesh(Utility.GetFullPath(fileName), out sceneLoader, content, device);

        //    SceneNode.SetAllLightEnable(sceneLoader.GetRootNode, true);
        //    return sceneLoader;
        //}

        /// <summary>
        /// 根據特殊設定檔讀取場景並設定它，傳出場景模型檔名，和檔案型態(普通場景或骨架動畫)，
        /// 還有傳出場景物件(BasicMeshLoader)。
        /// </summary>
        public static BasicMeshLoader LoadSceneFromSpecialXML(string XMLfilename, ContentManager content, GraphicsDevice device,
            out SpecialMeshDataManager.TatalSpecialMeshData total,
            out AnimationTableDataArray animationTableDataArray)
        {
            animationTableDataArray = null;
            object loadObj = Utility.LoadXMLFile(XMLfilename, typeof(SpecialMeshDataManager.TatalSpecialMeshData), false);
            total = loadObj as SpecialMeshDataManager.TatalSpecialMeshData;

            if (total == null)
                return null;

            //讀模型檔
            BasicMeshLoader loadScene = null;
            if (string.IsNullOrEmpty(total.ModelFileName) == false)
            {
                switch ((SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE)total.ModelType)
                {
                    case SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.SceneMesh:
                        {
                            string fileName = Utility.RemoveSubFileName(total.ModelFileName);
                            SceneMeshLoader sceneLoader = null;
                            if (SceneMeshLoader.LoadSceneMesh(Utility.GetFullPath(fileName), out sceneLoader, content, device))
                            {
                                SceneNode.SetAllLightEnable(sceneLoader.GetRootNode, true);
                                loadScene = sceneLoader;
                            }
                            //loadScene = SpecialMeshDataManager.LoadScene_XNB(total.ModelFileName, content, device);
                        }
                        break;
                    case SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.KeyframeMesh:
                        {
                            string fileName = Utility.RemoveSubFileName(total.ModelFileName);
                            KeyframeMeshLoader keyframeMeshLoader;
                            animationTableDataArray = KeyframeMeshLoader.LoadKeyFrameMesh(Utility.GetFullPath(fileName), content, device, out keyframeMeshLoader);
                            loadScene = keyframeMeshLoader;
                        }
                        break;
                    case SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.SkinnedMesh:
                        {
                            string fileName = Utility.RemoveSubFileName(total.ModelFileName);
                            SkinningMeshLoader skinLoader;
                            animationTableDataArray = SkinningMeshLoader.LoadSkinningMeshLoader(Utility.GetFullPath(fileName), content, device, out skinLoader);
                            loadScene = skinLoader;
                        }
                        break;
                    default:
                        loadScene = null;
                        break;
                }
            }
            loadScene.GetRootNode.NodeName = total.ModelFileName;

            if (loadScene != null)
                SpecialMeshDataManager.CreateAllSceneData_SceneObject(loadScene, total, content, device);

            return loadScene;
        }


        //static void DirectionLight1Set(Vector3 lightDirection, Vector3 lightAmbient, Vector3 lightDiffuse, Vector3 lightSpecular,
        //    float bbbbigShadowMapMultiSampleOffset, float ssssmalligShadowMapMultiSampleOffset,
        //    float bbbbigShadowMapRatio, float ssssmalligShadowMapRatio, 
        //    float bbbbigShadowMapBias, float ssssmalligShadowMapBias)
        //{
        //    LightDirection = lightDirection;
        //    LightAmbient = lightAmbient;
        //    LightDiffuse = lightDiffuse;
        //    LightSpecular = lightSpecular;
        //    BigShadowMapMultiSampleOffset = bbbbigShadowMapMultiSampleOffset;
        //    BigShadowMapRatio = bbbbigShadowMapRatio;
        //    SmallShadowMapMultiSampleOffset = ssssmalligShadowMapMultiSampleOffset;
        //    SmallShadowMapRatio = ssssmalligShadowMapRatio;

        //    BigShadowMapBias = bbbbigShadowMapBias;
        //    SmallShadowMapBias = ssssmalligShadowMapBias;
        //}

        //static void FogSet(Vector3 fogColor, float fogStart, float fogEnd)
        //{
        //    FogColor = fogColor;
        //    FogStart = fogStart;
        //    FogEnd = fogEnd;
        //    //System.Drawing.Color color = System.Drawing.Color.FromArgb(
        //    //    (int)(fogColor.X * 255f), (int)(fogColor.Y * 255f), (int)(fogColor.Z * 255f));
        //}

        /// <summary>
        /// 根據TatalSpecialMeshData資料建立整個場景的物件。
        /// </summary>
        public static void CreateAllSceneData_SceneObject(BasicMeshLoader sceneLoader, SpecialMeshDataManager.TatalSpecialMeshData total,
            ContentManager content, GraphicsDevice device)
        {
            try
            {
                sceneLoader.ParentLoaderName = total.ParentLoaderName;
                sceneLoader.ParentNodeName = total.ParentNodeName;

                if (total.pointLights != null)
                {
                    for(int a=0 ; a<total.pointLights.PointLight.Length ; a++)
                    {
                        sceneLoader.AddPointLight(ref total.pointLights.PointLight[a]);
                    }
                }

                if (total.spotLights != null)
                {
                    for (int a = 0; a < total.spotLights.SpotLight.Length; a++)
                    {
                        sceneLoader.AddSpotLight(ref total.spotLights.SpotLight[a]);
                    }
                }

                //sceneLoader.GetRootNode.LocalMatrix = total.RootRST.mat;

                //if (total.LightFogData != null)
                //{
                //    //DirectionLight1Set(total.LightFogData.Light1Direction, total.LightFogData.Light1Ambient,
                //    //    total.LightFogData.Light1Diffuse, total.LightFogData.Light1Specular,
                //    //    total.LightFogData.BigShadowMapMultiSampleOffset, total.LightFogData.SmallShadowMapMultiSampleOffset,
                //    //    total.LightFogData.AllBigShadowMapRatio, total.LightFogData.AllSmallShadowMapRatio,
                //    //    total.LightFogData.AllBigShadowMapBias, total.LightFogData.AllSmallShadowMapBias);
                //    //FogSet(total.LightFogData.FogColor, total.LightFogData.FogStart, total.LightFogData.FogEnd);

                //    //將光霧設定給此場景
                //    //sceneLoader.DirectionLight1Set(total.LightFogData.Light1Direction, total.LightFogData.Light1Ambient,
                //    //    total.LightFogData.Light1Diffuse, total.LightFogData.Light1Specular,
                //    //    total.LightFogData.AllBigShadowMapMultiSampleOffset, total.LightFogData.AllSmallShadowMapMultiSampleOffset,
                //    //    total.LightFogData.AllBigShadowMapRatio, total.LightFogData.AllSmallShadowMapRatio,
                //    //    total.LightFogData.AllBigShadowMapBias, total.LightFogData.AllSmallShadowMapBias,
                //    //    total.LightFogData.AllBigShadowSampleLength, total.LightFogData.AllSmallShadowSampleLength,
                //    //    total.LightFogData.MipmapLevel);
                //    //sceneLoader.FogSet(total.LightFogData.FogColor,
                //    //    total.LightFogData.FogStart, total.LightFogData.FogEnd, total.LightFogData.FogMaxRatio);

                //    //設定全場景設定的參數
                //    SceneNode.SetAllShadowMapData(sceneLoader.GetRootNode, total.LightFogData.AllBigShadowMapRatio,
                //        total.LightFogData.AllSmallShadowMapRatio,
                //        total.LightFogData.AllBigShadowMapBias, total.LightFogData.AllSmallShadowMapBias,
                //        total.LightFogData.AllBigShadowMapMultiSampleOffset, total.LightFogData.AllSmallShadowMapMultiSampleOffset,
                //        total.LightFogData.AllBigShadowSampleLength, total.LightFogData.AllSmallShadowSampleLength);
                //}

            

                //先設定群體設定的物件，有*號先設定
                if (total.NodesData != null)
                {
                //    for (int a = 0; a < total.NodesData.Length; a++)
                //    {
                //        string nodeName = total.NodesData[a].NodeName;
                //        if (nodeName.Contains("*"))
                //        {
                //            NodeArray nodeArray = new NodeArray(512);
                //            SceneNode.AddMeshNodeContainName(sceneLoader.GetRootNode, nodeName.Replace("*", string.Empty), nodeArray);
                //            for (int b = 0; b < nodeArray.nowNodeAmount; b++)
                //            {
                //                SceneNode node = nodeArray.nodeArray.GetValue(b) as SceneNode;

                //                SceneNode.NodeMaterialChange(total.NodesData[a].MeshesMaterials, node, content, device);
                //                SceneNode.NodeTranslateUVChange(total.NodesData[a].TranslateUVMeshes, node);
                //                SceneNode.NodeRenderFeatureChange(total.NodesData[a].RenderFeatureMeshes, node, content, device);
                //                SceneNode.NodeDynamicTextureChange(total.NodesData[a].DynamicTextureMeshes, node, content, device);
                //                SceneNode.NodeDynamicNormalTextureChange(total.NodesData[a].DynamicNormalTextureMeshes, node, content, device);
                //                SceneNode.NodeRSTDataChange(total.NodesData[a].RSTDataMeshes, node);
                //                SceneNode.NodeNormalMapChange(total.NodesData[a].NormalTextureFileName, node, content);
                //                SceneNode.NodeWindDataChange(total.NodesData[a].windData, node, device, content);
                //            }
                //        }
                //    }



                    //在設定個別物件，沒有*號的
                    for (int a = 0; a < total.NodesData.Length; a++)
                    {
                        string nodeName = total.NodesData[a].NodeName;
                        if (nodeName.Contains("*"))
                        {
                        }
                        else
                        {
                            SceneNode node = SceneNode.SeekFirstMeshNodeAllName(sceneLoader.GetRootNode, nodeName) as SceneNode;
                            if (node != null)
                            {
                                node.MaterialBankID = total.NodesData[a].MaterialBankID;
                                SceneNode.NodeMaterialChange(total.NodesData[a].MeshesMaterials, node, content, device);
                                SceneNode.NodeTranslateUVChange(total.NodesData[a].TranslateUVMeshes, node);
                                SceneNode.NodeRenderFeatureChange(total.NodesData[a].RenderFeatureMeshes, node, content, device);
                                SceneNode.NodeDynamicTextureChange(total.NodesData[a].DynamicTextureMeshes, node, content, device);
                                SceneNode.NodeDynamicNormalTextureChange(total.NodesData[a].DynamicNormalTextureMeshes, node, content, device);
                                //SceneNode.NodeRSTDataChange(total.NodesData[a].RSTDataMeshes, node);
                                SceneNode.NodeNormalMapChange(total.NodesData[a].NormalTextureFileName, node, content);
                                SceneNode.NodeWindDataChange(total.NodesData[a].windData, node, device, content);
                            }
                            else
                            {
                                ////OutputBox.SwitchOnOff = true;
                                //OutputBox.ShowMessage(total.NodesData[a].NodeName + "  ==> Node Not Exist!!!!!");
                            }
                        }
                    }
                }

                //增加MOP物件
                if (total.MutiObjectPlayers != null)
                {
                    for (int a = 0; a < total.MutiObjectPlayers.Length; a++)
                    {
                        sceneLoader.AddMultiObjectPlayerInScene(total.MutiObjectPlayers[a], content, device);
                    }
                }

                if (total.AnimateWithMOPs != null)
                {
                    BasicMeshLoader.AnimationWithMOP_SetData(sceneLoader, total.AnimateWithMOPs);           
                }

                if (total.NodesRSTData != null)
                {
                    for(int a=0 ; a<total.NodesRSTData.Length ; a++)
                    {
                        FrameNode node = sceneLoader.GetRootNode.SeekFirstNodeAllName(total.NodesRSTData[a].nodeName) as FrameNode;
                        node.LocalMatrix = total.NodesRSTData[a].rst.mat;
                    }
                }

                //if (total.OceanDatas != null)
                //    RecreateEffectObject(sceneLoader, content, device, total.OceanDatas);

                //sceneLoader.ClearGodsRayLightNodeList();
                //sceneLoader.CreateGodsRayLightNodeList(total.GodsRayLightDatas);
            }
            catch (Exception e)
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage(e.Message);
            }
        }



        //public static void AddMultiObjectPlayerInScene(SpecialMeshDataManager.MOPData mop, BasicMeshLoader loadScene,
        //ContentManager content, GraphicsDevice device)
        //{
        //    MultiObjectPlayer[] mopList;
        //    LocatorNode locatorListRootNode;
        //    if (SpecialMeshDataManager.MOPData.CreateMultiObjectPlayer(mop, loadScene.GetRootNode, content, device,
        //        out locatorListRootNode, out mopList))
        //    {
        //        loadScene.SetMuliObjectPlayerList(locatorListRootNode, mopList);
        //    }

        //    ////這邊release掉所有的particle，之後update時會自動重加。
        //    //loadScene.ReleaseParticleManager();
        //}


        //根據EDITOR的清單重建所有的Effect object
        public static void RecreateEffectObject(BasicMeshLoader sceneLoader, ContentManager content, GraphicsDevice device,
            SpecialMeshDataManager.MyOceanData[] oceanDataArray)
        {
            sceneLoader.EffectObjectList_Release();
            //foreach (string kkk in oceanDataList.Keys)
            //{
            //    sceneLoader.AddOceanInScene(oceanDataList[kkk], content, device);
            //}
            for (int a = 0; a < oceanDataArray.Length; a++)
            {
                sceneLoader.AddOceanInScene(oceanDataArray[a], content, device);
            }
        }
    }

}
