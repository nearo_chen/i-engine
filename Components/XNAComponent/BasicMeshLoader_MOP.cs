﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;
using MeshDataDefine;


namespace I_XNAComponent
{
    //模型設定通用基本的元素都可以設定在此CLASS
    abstract public partial class BasicMeshLoader : IDisposable
    {
        /// <summary>
        /// 由LocatorListRootNode控制MOP的撥放
        /// </summary>
        /// <param name="LocatorListRootNode"></param>
        public void SetMuliObjectPlayerPause(LocatorNode LocatorListRootNode)
        {
            MultiObjectPlayer[] mopList;
            if (m_MuliObjectPlayerList != null && m_MuliObjectPlayerList.TryGetValue(LocatorListRootNode, out  mopList))
            {
                SetMOPListPause(mopList);
            }
        }

        /// <summary>
        /// 由LocatorListRootNode控制MOP的撥放
        /// </summary>
        /// <param name="LocatorListRootNode"></param>
        public void SetMuliObjectPlayerPlay(LocatorNode LocatorListRootNode)
        {
            if (LocatorListRootNode == null)
                return;
            MultiObjectPlayer[] mopList;
            if (m_MuliObjectPlayerList != null && m_MuliObjectPlayerList.TryGetValue(LocatorListRootNode, out  mopList))
            {
                SetMOPListRePlay(mopList);
            }
        }

        /// <summary>
        /// 由LocatorListRootNode控制MOP的撥放
        /// </summary>
        /// <param name="LocatorListRootNode"></param>
        public void SetMuliObjectPlayerRePlay(LocatorNode LocatorListRootNode)
        {
            MultiObjectPlayer[] mopList;
            if (m_MuliObjectPlayerList != null && LocatorListRootNode != null &&
                m_MuliObjectPlayerList.TryGetValue(LocatorListRootNode, out  mopList))
            {
                SetMOPListPlay(mopList);
            }
        }

        /// <summary>
        /// 由LocatorListRootNode控制MOP的撥放
        /// </summary>
        /// <param name="LocatorListRootNode"></param>
        public void SetMuliObjectPlayerStop(LocatorNode LocatorListRootNode)
        {
            MultiObjectPlayer[] mopList;
            if (m_MuliObjectPlayerList != null && LocatorListRootNode != null &&
                m_MuliObjectPlayerList.TryGetValue(LocatorListRootNode, out  mopList))
            {
                SetMOPListStop(mopList);
            }
        }

        public static void SetMOPListPlay(MultiObjectPlayer[] mopList)
        {
            for (int a = 0; a < mopList.Length; a++)
                mopList[a].SetPlay();
        }

        public static void SetMOPListRePlay(MultiObjectPlayer[] mopList)
        {
            for (int a = 0; a < mopList.Length; a++)
                mopList[a].SetReplay();
        }

        public static void SetMOPListStop(MultiObjectPlayer[] mopList)
        {
            for (int a = 0; a < mopList.Length; a++)
                mopList[a].SetStop();
        }

        public static void SetMOPListPause(MultiObjectPlayer[] mopList)
        {
            for (int a = 0; a < mopList.Length; a++)
                mopList[a].SetPause();
        }

        public static void SetMOPList_SoundPitch(MultiObjectPlayer[] mopList, float pitch)
        {
            for (int a = 0; a < mopList.Length; a++)
                mopList[a].SetSoundPitch(pitch);
        }

        public MultiObjectPlayer[] GetMuliObjectPlayerList(string containLocatorName)
        {
            LocatorNode lll = GetRootNode.SeekFirstNodeContainName(containLocatorName) as LocatorNode;
            MultiObjectPlayer[] mopList;
            if (m_MuliObjectPlayerList != null && lll != null &&
                m_MuliObjectPlayerList.TryGetValue(lll, out  mopList))
            {
                return mopList;
            }
            return null;
        }

        /// <summary>
        /// 停止撥放MOP
        /// </summary>
        public void SetAllMuliObjectPlayerStop()
        {
            if (m_MuliObjectPlayerList == null)
                return;
            Dictionary<LocatorNode, MultiObjectPlayer[]>.Enumerator mopItr = m_MuliObjectPlayerList.GetEnumerator();
            while (mopItr.MoveNext())
            {
                MultiObjectPlayer[] mopList = mopItr.Current.Value;
                for (int a = 0; a < mopList.Length; a++)
                {
                    mopList[a].SetStop();
                }
            }
            mopItr.Dispose();
        }

        /// <summary>
        /// 傳入一組 multi object player進入場景設定中。
        /// 如果在表單上設定一次mop，可能會產生許多mop在一個node下。
        /// 因此我將parent node設定為索引，底下為他生出來的mop list。
        /// 只建議於編輯或設定]狀態中呼叫，不然會LAG
        /// </summary>   
        public void SetMuliObjectPlayerList(LocatorNode LocatorListRootNode, MultiObjectPlayer[] mopList)
        {
            if (m_MuliObjectPlayerList == null)
                m_MuliObjectPlayerList = new Dictionary<LocatorNode, MultiObjectPlayer[]>();
            m_MuliObjectPlayerList.Add(LocatorListRootNode, mopList);
        }

        /// <summary>
        /// 釋放掉某MOP，只建議於編輯或設定]狀態中呼叫，不然會LAG
        /// </summary>
        /// <param name="LocatorListRootNode"></param>
        public void ReleaseMuliObjectPlayerList(LocatorNode LocatorListRootNode)
        {
            MultiObjectPlayer[] moparray = m_MuliObjectPlayerList[LocatorListRootNode];
            for (int a = 0; a < moparray.Length; a++)
            {
                moparray[a].Dispose();
                moparray[a] = null;
            }
            moparray = null;

            //BasicNode nnn = LocatorListRootNode as BasicNode;
            BasicNode.DisposeAllTree(LocatorListRootNode);
            m_MuliObjectPlayerList.Remove(LocatorListRootNode);
        }

        /// <summary>
        /// m_MuliObjectPlayerList所有的MOP呼叫Dispose()
        /// </summary>
        public void ReleaseAllMultiObjectPlayerList()
        {
            if (m_MuliObjectPlayerList != null)
            {
                Dictionary<LocatorNode, MultiObjectPlayer[]>.Enumerator itr = m_MuliObjectPlayerList.GetEnumerator();
                while (itr.MoveNext())
                {
                    MultiObjectPlayer[] moparray = itr.Current.Value as MultiObjectPlayer[];
                    for (int a = 0; a < moparray.Length; a++)
                    {
                        moparray[a].Dispose();
                        moparray[a] = null;
                    }
                    moparray = null;
                }
                itr.Dispose();

                m_MuliObjectPlayerList.Clear();
                m_MuliObjectPlayerList = null;
            }
        }

        /// <summary>
        /// 根據mop建立MOP物件，並呼叫SetMuliObjectPlayerList設定物件給Loader
        /// </summary>
        public LocatorNode AddMultiObjectPlayerInScene(SpecialMeshDataManager.MOPData mop, ContentManager content, GraphicsDevice device)
        {
            MultiObjectPlayer[] mopList;
            LocatorNode locatorListRootNode;
            if (SpecialMeshDataManager.MOPData.CreateMultiObjectPlayer(mop, GetRootNode, content, device,
                out locatorListRootNode, out mopList))
            {
                SetMuliObjectPlayerList(locatorListRootNode, mopList);
            }

            //這邊release掉所有的particle，之後update時會自動重加。
            ReleaseParticleManager();

            return locatorListRootNode;
        }

        /// <summary>
        /// 根據名稱取得MOP的parent locator root node
        /// </summary>
        /// <returns></returns>
        public LocatorNode GetMOPLocator(string parentNodeName)
        {
            Dictionary<LocatorNode, MultiObjectPlayer[]>.KeyCollection.Enumerator key = m_MuliObjectPlayerList.Keys.GetEnumerator();
            while (key.MoveNext())
            {
                if (key.Current.NodeName == parentNodeName)
                    return key.Current;
            }
            return null;
        }
    }
}
