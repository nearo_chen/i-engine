﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;


namespace I_XNAComponent
{
    public static class SceneCollision
    {
        public const  int CollisionBufferSize = 256;

        //Collision with scene tree...
        //cout tree level in all tree node collision test...use for check tree root.....
        static int countCollisionTreeLevel = 0;

        //add all collision Object in this list
        //static List<SceneNode> ColList;
        static MyObjectContainer<SceneNode> OutPutColList = null;
        static MyObjectContainer<SceneNode> ColList = null;

        /// <summary>
        /// 清光跟別人有關的資料讓他沒instance卡在這。
        /// </summary>
        static public void ClearCollisionData()
        {
            if (OutPutColList != null)
            {
                OutPutColList.Dispose();
                OutPutColList = null;
            }

            if (ColList != null)
            {
                ColList.Dispose();
                ColList = null;
            }
        }

        static MyObjectContainer<SceneNode> SetOutPutColList()
        {
            if (OutPutColList == null)
                OutPutColList = new MyObjectContainer<SceneNode>(CollisionBufferSize);
            MyObjectContainer<SceneNode>.AsignAtoB(ColList, OutPutColList);
            ColList.Clear();
            return OutPutColList;

            //MyObjectContainer OutPutColList = MyObjectContainer.CloneFetchNewContainer(ColList);
            //ColList.Clear();
            //return OutPutColList;
        }

        //use for record the closest distance....
        static float nearD = 0;

        //record the nearest node
        static SceneNode nearestNode;

        static public SceneNode NearestNode
        {
            get { return nearestNode; }
        }
        static public float NearestDistance
        {
            get { return nearD; }
        }

        //record the collision triangle's normal...
        static Vector3 m_NearestTriangleNormal;
        static public Vector3 NearestTriangleNormal
        {
            get { return m_NearestTriangleNormal; }
        }
        static Vector3[] m_ColTriangle = new Vector3[3];
        static public Vector3 CollideTrianglePointA
        { get { return m_ColTriangle[0]; } }
        static public Vector3 CollideTrianglePointB
        { get { return m_ColTriangle[1]; } }
        static public Vector3 CollideTrianglePointC
        { get { return m_ColTriangle[2]; } }
        static void initializeCollisionData()
        {
            nearestNode = null;
            nearD = float.MaxValue; 
      
            if(ColList == null)
                ColList = new MyObjectContainer<SceneNode>(CollisionBufferSize);
            ColList.Clear();
        }

        static public MyObjectContainer<SceneNode> RayIntersectBoundSphere_Tree(SceneNode node, Ray ray)
        {
            //count tree level, if first enter tree, give a large distence to nearD....
            countCollisionTreeLevel++;
            if (countCollisionTreeLevel == 1)//if tree level = 1,this node is tree root...
            {
                initializeCollisionData();
            }

            //collision test....
            if (node.IfBoundingSphere)
            {
                float? distance = Collision.RayIntersectBoundSphere(node.WorldMatrix, ray,
                    node.BoundingSphere);
                if (distance != null)
                {
                    //ColList.Add(node);
                    ColList.AddObject(node);

                    if (nearD > distance)
                    {
                        nearD = (float)distance;
                        nearestNode = node;
                    }
                }
            }

            //children continue...
            SceneNode c = (SceneNode)node.FirstChild;
            while (c != null)
            {
                RayIntersectBoundSphere_Tree(c, ray);
                c = (SceneNode)c.Sibling;
            }

            //leave root node copy collision object to array...return.
            if (countCollisionTreeLevel == 1)
            {
                countCollisionTreeLevel--;
                return SetOutPutColList();
            }
            countCollisionTreeLevel--;
            return null;
        }

        static public MyObjectContainer<SceneNode> RayIntersectBoundSphere_Array(MyObjectContainer<SceneNode> nodearray, Ray ray)
        {
            initializeCollisionData();
            //foreach (SceneNode node in nodearray)
            for(int a=0 ; a<nodearray.NowUsedAmount ; a++)
            {
                SceneNode node = (SceneNode)nodearray.GetObjectArrayValue(a);
                if (node.IfBoundingSphere)
                {
                    float? distance = Collision.RayIntersectBoundSphere(
                        node.WorldMatrix, ray, node.BoundingSphere);
                    if (distance != null)
                    {
                        //ColList.Add(node);
                        ColList.AddObject(node);
                        if (nearD > distance)
                        {
                            nearD = (float)distance;
                            nearestNode = node;
                        }
                    }
                }
            }

            return SetOutPutColList();
        }

        static public MyObjectContainer<SceneNode> RayIntersectBoundBox_Tree(FrameNode frameNode, Ray ray)
        {
            //count tree level, if first enter tree, give a large distence to nearD....
            countCollisionTreeLevel++;
            if (countCollisionTreeLevel == 1)//if tree level = 1,this node is tree root...
            {
                initializeCollisionData();
            }

            SceneNode sceneNode = frameNode as SceneNode;
            //collision test....
            if (sceneNode != null && sceneNode.IfBoundingBox)
            {
                float? distance = Collision.RayIntersectBoundBox(sceneNode.WorldMatrix, ray, sceneNode.BoundingBox);
                if (distance != null)
                {
                    ColList.AddObject(sceneNode);

                    if (nearD > distance)
                    {
                        nearD = (float)distance;
                        nearestNode = sceneNode;
                    }
                }
            }

            //children continue...
            FrameNode c = frameNode.FirstChild as FrameNode;
            while (c != null)
            {
                RayIntersectBoundBox_Tree(c, ray);
                c = c.Sibling as FrameNode;
            }

            //leave root node copy collision object to array...return.
            if (countCollisionTreeLevel == 1)
            {
                countCollisionTreeLevel--;
                return SetOutPutColList();
            }
            countCollisionTreeLevel--;
            return null;
        }

        static public MyObjectContainer<SceneNode> RayIntersectBoundBox_Array(MyObjectContainer<SceneNode> nodearray, Ray ray)
        {
            initializeCollisionData();
            //foreach (SceneNode node in nodearray)

            if (nodearray != null)
            {
                for (int a = 0; a < nodearray.NowUsedAmount; a++)
                {
                    SceneNode node = (SceneNode)nodearray.GetObjectArrayValue(a);

                    if (node.IfBoundingBox)
                    {
                        float? distance = Collision.RayIntersectBoundBox(node.WorldMatrix, ray, node.BoundingBox);
                        if (distance != null)
                        {
                            ColList.AddObject(node);
                            if (nearD > distance)
                            {
                                nearD = (float)distance;
                                nearestNode = node;
                            }
                        }
                    }
                }
            }
            return SetOutPutColList();
        }

        static public MyObjectContainer<SceneNode> RayIntersectModel_Array(MyObjectContainer<SceneNode> nodearray, Ray ray)
        {
            initializeCollisionData();

            if (nodearray != null)
            {

                // foreach (SceneNode node in nodearray)
                for (int a = 0; a < nodearray.NowUsedAmount; a++)
                {
                    SceneNode node = (SceneNode)nodearray.GetObjectArrayValue(a);

                    if (node.IndexBuffer != null)
                    {
                        float? distance = Collision.RayIntersectModel(node.WorldMatrix, ray, node.IndexBuffer, node.VertexBuffer);
                        if (distance != null)
                        {
                            ColList.AddObject(node);
                            if (nearD > distance)
                            {
                                //float dot = Vector3.Dot(ray.Direction, Collision.CollideTriangleNormal);
                                //dot = Math.Cos(dot);
                                //if()
                                {
                                    nearD = (float)distance;
                                    nearestNode = node;
                                    m_NearestTriangleNormal = Collision.CollideTriangleNormal;

                                    m_ColTriangle[0] = Collision.CollideTrianglePointA;
                                    m_ColTriangle[1] = Collision.CollideTrianglePointB;
                                    m_ColTriangle[2] = Collision.CollideTrianglePointC;
                                }
                            }
                        }
                    }
                }
            }
            return SetOutPutColList();
        }


        static public MyObjectContainer<SceneNode> FrustumContainNodeSphere_Tree(ref Matrix frustumMatrix, SceneNode node)
        {
            //count tree level, if first enter tree, give a large distence to nearD....
            countCollisionTreeLevel++;
            if (countCollisionTreeLevel == 1)//if tree level = 1,this node is tree root...
            {
                initializeCollisionData();
            }

            //Collision Test...
            if (node.IfBoundingSphere)
            {
                Matrix nodeWorld = node.WorldMatrix;
                if (Collision.FrustumContainSphere(ref frustumMatrix, node.BoundingSphere, ref nodeWorld))
                {
                    //ColList.Add(node);
                    ColList.AddObject(node);
                }
            }

            SceneNode c = (SceneNode)node.FirstChild;
            while (c != null)
            {
                FrustumContainNodeSphere_Tree(ref frustumMatrix, c);
                c = (SceneNode)c.Sibling;
            }

            //leave root node copy collision object to array...return.
            if (countCollisionTreeLevel == 1)
            {
                countCollisionTreeLevel--;
                return SetOutPutColList();
            }
            countCollisionTreeLevel--;
            return null;
        }

        static public MyObjectContainer<SceneNode> FrustumContainNodeBox_Tree(ref Matrix frustumMatrix, SceneNode node)
        {
            //count tree level, if first enter tree, give a large distence to nearD....
            countCollisionTreeLevel++;
            if (countCollisionTreeLevel == 1)//if tree level = 1,this node is tree root...
            {
                initializeCollisionData();
            }

            //Collision Test...
            if (node.IfBoundingBox)
            {
                Matrix nodeWorld = node.WorldMatrix;
                if (Collision.FrustumContainBox(ref frustumMatrix, node.BoundingBox, ref nodeWorld))
                {
                    ColList.AddObject(node);
                }
            }

            SceneNode c = (SceneNode)node.FirstChild;
            while (c != null)
            {
                FrustumContainNodeBox_Tree(ref frustumMatrix, c);
                c = (SceneNode)c.Sibling;
            }

            //leave root node copy collision object to array...return.
            if (countCollisionTreeLevel == 1)
            {
                countCollisionTreeLevel--;
                return SetOutPutColList();
            }
            countCollisionTreeLevel--;
            return null;
        }

        static public MyObjectContainer<SceneNode> FrustumContainNodeBox_Array(ref Matrix frustumMatrix, MyObjectContainer<SceneNode> nodeArray)
        {
            initializeCollisionData();

            if (nodeArray != null)
            {
                //Collision Test...
                //foreach (SceneNode node in nodeArray)
                for (int a = 0; a < nodeArray.NowUsedAmount; a++)
                {
                    SceneNode node = (SceneNode)nodeArray.GetObjectArrayValue(a);
                    if (node.IfBoundingBox)
                    {
                        Matrix nodeW = node.WorldMatrix;
                        if (Collision.FrustumContainBox(ref frustumMatrix, node.BoundingBox, ref nodeW))
                        {
                            ColList.AddObject(node);
                        }
                    }
                }
            }
            return SetOutPutColList();
        }

        //給車子用的回傳碰撞點
        //一個物件某個bomb bar對周圍的牆作碰撞測試
        static public Vector3? WallCollision(ref Matrix carMatrix, ref Vector3 bombPoint, ref Vector3 bombDir,
            MyObjectContainer<SceneNode> wallsNode, out Ray collisionRay)
        {
            Vector3? collisionPoint = null;
            //SceneNode[] collArray;
          

            collisionRay.Position = carMatrix.Translation;
            //collisionRay.Direction = Vector3.TransformNormal(bombPoint - rayPosPoint);
            Vector3 dir;
            Vector3.TransformNormal(ref bombDir, ref carMatrix, out dir);
            collisionRay.Direction = dir;
          //  collisionRay.Position = collisionRay.Position - collisionRay.Direction * 4;

            MyObjectContainer<SceneNode> RecMoc = SceneCollision.RayIntersectBoundSphere_Array(wallsNode, collisionRay);
            RecMoc = SceneCollision.RayIntersectBoundBox_Array(RecMoc, collisionRay);
            RecMoc = SceneCollision.RayIntersectModel_Array(RecMoc, collisionRay);
            if (SceneCollision.NearestNode != null)
            {
                collisionPoint = collisionRay.Position + collisionRay.Direction * SceneCollision.NearestDistance;
            }
            return collisionPoint;
        }

        ////回傳位置修正向量.
        //static public Vector3 WallCollisionFix(Matrix carMatrix, SceneNode[] walls, Vector3 bombPoint,
        //                                           out Ray collisionRay, out Vector3 collisionPoint)
        //{
        //    return WallCollisionFix(carMatrix, walls, Vector3.Zero, bombPoint, out collisionRay, out collisionPoint);
        //}

        static public Vector3 WallCollisionFix(Matrix carMatrix, MyObjectContainer<SceneNode> walls, 
                                                    ref Vector3 bombPoint, ref Vector3 bombDir,
                                                   out Ray collisionRay, out Vector3 collisionPoint, out float offsetDistance, out Vector3 CollideTriangleNormal)
        {
           // bombPoint.Y = 0;
            offsetDistance = 0;
            CollideTriangleNormal = Vector3.Zero;

            Vector3 fixedOffsetVector = Vector3.Zero;
            collisionPoint = Vector3.Zero;
            float SafeDistance = bombPoint.Length();//Vector3.Distance(Vector3.Zero, bombPoint)+0;

            //檢查bomb bar有無撞進牆壁裡
            Vector3? collPoint = SceneCollision.WallCollision(ref carMatrix, ref bombPoint, ref bombDir, walls, out collisionRay);
            if (collPoint != null)
            {
                collisionPoint = (Vector3)collPoint;
                //如果撞進牆壁裡了把它喬出來
                if (SceneCollision.NearestDistance <= SafeDistance)
                {
                    SceneNode nearestNode = SceneCollision.NearestNode;

                    //近一步對模型測試，得到牆壁法向量...
                    Collision.RayIntersectModel(nearestNode.WorldMatrix, collisionRay,
                                                 nearestNode.IndexBuffer, nearestNode.VertexBuffer);

                    //粗略的修正
                    //Vector3 newPosition = carNode.WorldPosition;
                    offsetDistance = SafeDistance - SceneCollision.NearestDistance;
                    CollideTriangleNormal = Collision.CollideTriangleNormal;
                    fixedOffsetVector = Collision.CollideTriangleNormal * offsetDistance;

                    ////精確的修正，但BUG多
                    //Vector3 worldBombPoint = collisionRay.Position + collisionRay.Direction * SafeDistance;
                    //Ray rayBombWall;
                    //rayBombWall.Position = worldBombPoint;
                    //rayBombWall.Direction = Collision.CollideTriangleNormal;
                    ////由穿透的bomb point往牆的法向量偵測碰撞點，求得修正距離...
                    //float? distance = Collision.RayIntersectModel(nearestNode.WorldMatrix, rayBombWall,
                    //                                                                    nearestNode.IndexBuffer, nearestNode.VertexBuffer);
                    //if (distance!=null)
                    //    fixedOffsetVector = Collision.CollideTriangleNormal * (float)distance*2.0f;
                }
            }

        
            return fixedOffsetVector;
        }

        static public bool RayCollideSquare(Matrix carMatrix, Ray ray, float safeDis, uint[] indexbuffer, Vector3[] vertexbuffer,
            out Vector3 offsetDir, out float offsetDistence)
        {
            float? dis;
            dis = Collision.RayIntersectModel(carMatrix, ray, indexbuffer, vertexbuffer);
            if (dis != null && (float)dis < safeDis)
            {
                nearD = (float)dis;
                offsetDistence = safeDis - (float)dis;
                offsetDir = Collision.CollideTriangleNormal;
               // Vector3 fixedOffsetVector = Collision.CollideTriangleNormal * offsetDistence;
                return true;
            }

            offsetDir = Vector3.Zero;
            offsetDistence = 0;
            return false;
        }

        //偵測輪子點，往車子的UP反方向與地板碰撞的結果，回傳是否有輪子有在地面
        static public bool FloorCollision(SceneNode carNode, Vector3 wheelPoint, SceneNode floorNodes,
                                                     out Ray collisionRay, out Vector3 collisionPoint, out Vector3 floorNormal)
        {
            //輪子新的位置，是利用往地板的碰撞點與地板的法向量，與輪子距離地面的高度計算出
            floorNormal = Vector3.Zero;
            collisionPoint = Vector3.Zero;
            bool bWheelOnFloor=false;

            //wheelPoint.Y += 5; //增高一點，但太高有可能會被山洞上的路影響。
            collisionRay.Position = Vector3.Transform(wheelPoint, carNode.WorldMatrix);
            collisionRay.Direction = carNode.WorldUp * -1.0f;
            //collisionRay.Direction = Vector3.Up * -1;

       
            //Sphere偵測到有SCALE的東西會有錯...
            MyObjectContainer<SceneNode> obc = SceneCollision.RayIntersectBoundSphere_Tree(floorNodes, collisionRay);
            //collarray = SceneCollision.RayIntersectBoundBox_Tree(floorNodes, collisionRay);
            obc = SceneCollision.RayIntersectBoundBox_Array(obc, collisionRay);
            obc = SceneCollision.RayIntersectModel_Array(obc, collisionRay);

            //wheelNewPoint = collisionRay.Position;
            if (SceneCollision.NearestNode != null)
            {
                collisionPoint = collisionRay.Position + collisionRay.Direction * SceneCollision.NearestDistance;
                floorNormal = SceneCollision.NearestTriangleNormal;

                bWheelOnFloor = true;
                ////檢查碰撞面是否角度太大，大於45度=>dot >0.5，就當沒碰撞點
                //float dot = Vector3.Dot(carNode.WorldUp, floorNormal);
                //if (dot >= 0.9f && dot <= 1.0f)
                //    wheelNewPoint = collisionPoint + floorNormal * wheelOffsetFloor;//由碰撞點反算出輪子位置
            }

            return bWheelOnFloor;
        }

        //傳入點座標，與node tree做偵測，是否點座標位於某NODE的BOUND BOX內。
        public static SceneNode PointInBoundBoxTree(ref Vector3 point, SceneNode parentNode)
        {
            if ( parentNode.IfBoundingBox)
            {
                Matrix invwww = parentNode.InverseWorlMatrix;
                Vector3 ppp;
                Vector3.Transform(ref point, ref invwww, out ppp);

                if (parentNode.BoundingBox.Contains(ppp) != ContainmentType.Disjoint)
                    return parentNode;
            }

            //children continue...
            SceneNode c = (SceneNode)parentNode.FirstChild;
            while (c != null)
            {
                SceneNode InNode =  PointInBoundBoxTree(ref point, c);
                if (InNode != null)
                    return InNode;

                c = (SceneNode)c.Sibling;
            }

            return null;
        }
    }
}
