﻿#define UseRSTMatrix
//#define ANI_Ratio

using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;
using MeshDataDefine;

namespace I_XNAComponent
{
    public class AnimationPlayer : IDisposable
    {
        //refresh animating matrix for each bone.....
        AnimationData m_Animation;
#if UseRSTMatrix
        RSTMatrix[] NowBoneAnimatingMatrix; //計算並儲存目前每一bone的matrix在上面
#else
        Matrix[] NowBoneAnimatingMatrix; //計算並儲存目前每一bone的matrix在上面
#endif
        TimePlayControl[] BoneTimeControl = null; //用來計算時間過多少bone應該是撥放到哪一格
        //bool m_bUseTimeControl = false;

        //不使用BoneTimeControl自己算
        //bool m_bSetPlay = false;
        //uint m_PlaytimeCount = 0;

        //key frame range...取得固定的keyframe範圍
        int[] m_BonesStartKey;
        int[] m_BonesEndKey;
        string m_AnimationName;

        int m_iStratTime;
        int m_iEndTime;
        float m_iTotalTime;
        int m_iCountElapseTime;

        //public AnimationPlayer(AnimationData animationData, int startKey, int endKey, string animationName, bool bUseTimeControl)
        //{
        //    AnimationPlayer(animationData, startKey, endKey, animationName, false);
        //}

        public void Dispose()
        {
            m_Animation = null;
            NowBoneAnimatingMatrix = null;
            for (int a = 0; a < BoneTimeControl.Length; a++)
            {
                BoneTimeControl[a].Dispose();
                BoneTimeControl[a] = null;
            }
            BoneTimeControl = null;

            m_BonesStartKey = null;
            m_BonesEndKey = null;
            m_AnimationName = null;
        }

        //public AnimationPlayer(AnimationData animationData, uint totalMillisecondTime, string animationName, bool bUseTimeControl)
        //{
        //    uint iStratTime = 0;
        //    uint iEndTime = (uint)totalMillisecondTime;

        //    InitAnimationPlayer(animationData, iStratTime, iEndTime, animationName, bUseTimeControl);
        //}

        /// <summary>
        /// keyPerSecond 為每秒幾格撥放數。
        /// </summary>    
        public AnimationPlayer(AnimationData animationData, int startKey, int endKey, string animationName, //bool bUseTimeControl,
            int keyPerSecond)
        {
            int iStratTime = (int)((float)startKey / (float)keyPerSecond * 1000f);
            int iEndTime = (int)((float)endKey / (float)keyPerSecond * 1000f);

            //if (iStratTime == iEndTime)
            //{
            //    int a = 0;
            //}

            InitAnimationPlayer(animationData, iStratTime, iEndTime, animationName);//, bUseTimeControl);
        }

        /// <summary>
        /// bool bUseTimeControl控制是否要使用TimePlayControl，來加速取得keyframe，但較耗記憶體。
        /// </summary> 
        void InitAnimationPlayer(AnimationData animationData, int iStratTime, int iEndTime, string animationName)//, bool bUseTimeControl)
        {
            m_iStratTime = iStratTime;
            m_iEndTime = iEndTime;
            m_iTotalTime = iEndTime - iStratTime;

            m_AnimationName = animationName;
            m_Animation = animationData;

#if UseRSTMatrix
            NowBoneAnimatingMatrix = RSTMatrix.GetArray(animationData.BoneKeyframes.Length);
#else
            NowBoneAnimatingMatrix = Utility.GetMatrixArray(animationData.BoneKeyframes.Length);//new Matrix[animationData.BoneKeyframes.Length];
#endif

            m_BonesStartKey = Utility.GetIntArray(animationData.BoneKeyframes.Length);// new int[animationData.BoneKeyframes.Length];
            m_BonesEndKey = Utility.GetIntArray(animationData.BoneKeyframes.Length); //new int[animationData.BoneKeyframes.Length];

            //uint m_iStratTime = (uint)(startKey / 24f * 1000);
            //uint m_iEndTime = (uint)(endKey / 24f * 1000);

            //m_bUseTimeControl = bUseTimeControl;
            //if (bUseTimeControl)
            {
                BoneTimeControl = TimePlayControl.GetTimePlayControlArray(animationData.BoneKeyframes.Length);// new TimePlayControl[animationData.BoneKeyframes.Length];
                //int countBone= 0;
                //foreach (BoneKeyframe boneKeyFrame in animationData.BoneKeyframes)
                for (int countBone = 0; countBone < animationData.BoneKeyframes.Length; countBone++)
                {
                    BoneKeyframe boneKeyFrame = animationData.BoneKeyframes[countBone];
                    //if (boneKeyFrame == null)
                    //    continue;

                    int StartKey = 0;
                    int EndKey = 0;
                    Keyframe[] keyframes = boneKeyFrame.Keyframes;

                    for (int i = 0; i < keyframes.Length; i++)
                    {
                        if (keyframes[i].Time <= iStratTime)
                            StartKey = i;

                        if (keyframes[i].Time <= iEndTime)
                            EndKey = i;
                    }

                    if (iStratTime - keyframes[StartKey].Time > 10)//如果格數差太多就中間補一格
                    {
                        int nextStart = StartKey+1;
                        if (nextStart >= keyframes.Length)
                            nextStart--;
                        int remainKeys = boneKeyFrame.Keyframes.Length - 1 - StartKey;

                        Keyframe[] NewKeyframes = new Keyframe[boneKeyFrame.Keyframes.Length + 1];
                        Array.Copy(boneKeyFrame.Keyframes, NewKeyframes, StartKey+1);
                        Array.Copy(boneKeyFrame.Keyframes, StartKey+1, NewKeyframes, StartKey + 2, remainKeys);
                        float localTimelength = keyframes[nextStart].Time - keyframes[StartKey].Time;
                        float ratio = 0;
                        if(localTimelength>0)
                         ratio = (float)(iStratTime - keyframes[StartKey].Time) / localTimelength;

                        RSTMatrix rrr;
                        RSTMatrix.RSTMatrixLerp(ref keyframes[StartKey].m_RSTMatrix, ref keyframes[nextStart].m_RSTMatrix, ratio, out rrr);
                        Keyframe kkk = new Keyframe((uint)iStratTime, rrr.m_Scale, rrr.m_RTMatrix.m_Quaternion, rrr.m_RTMatrix.m_Translation);
                        NewKeyframes[StartKey+1] = kkk;
                        StartKey++;
                        EndKey++;

                        boneKeyFrame.Keyframes = NewKeyframes;
                        keyframes = NewKeyframes;
                        //boneKeyFrame.Keyframes.to
                    }

                    if (iEndTime - keyframes[EndKey].Time > 10)//如果格數差太多就中間補一格
                    {
                        int nextEnd = EndKey+1;
                        if (nextEnd >= keyframes.Length)
                            nextEnd--;
                        int remainKeys = boneKeyFrame.Keyframes.Length - 1 - EndKey;

                        Keyframe[] NewKeyframes = new Keyframe[boneKeyFrame.Keyframes.Length + 1];
                        Array.Copy(boneKeyFrame.Keyframes, NewKeyframes, EndKey + 1);
                        Array.Copy(boneKeyFrame.Keyframes, EndKey + 1, NewKeyframes, EndKey + 2, remainKeys);
                        float localTimelength = keyframes[nextEnd].Time - keyframes[EndKey].Time;
                        float ratio = 0;
                        if (localTimelength > 0)
                            ratio = (float)(iEndTime - keyframes[EndKey].Time) / localTimelength;

                        RSTMatrix rrr;
                        RSTMatrix.RSTMatrixLerp(ref keyframes[EndKey].m_RSTMatrix, ref keyframes[nextEnd].m_RSTMatrix, ratio, out rrr);
                        Keyframe kkk = new Keyframe((uint)iEndTime, rrr.m_Scale, rrr.m_RTMatrix.m_Quaternion, rrr.m_RTMatrix.m_Translation);
                        NewKeyframes[EndKey + 1] = kkk;
                        //StartKey++;
                        EndKey++;

                        boneKeyFrame.Keyframes = NewKeyframes;
                        keyframes = NewKeyframes;
                    }

                    //set data...
                    int keyAmount = EndKey - StartKey + 1;
                    int blockAmount = keyAmount - 1;
                    if (EndKey == StartKey)//給相同key就給他那一格
                    {
                        blockAmount = 1;
                        BoneTimeControl[countBone] = new TimePlayControl(blockAmount);//有n格就有n-1個block
                        TimePlayControl playControl = BoneTimeControl[countBone];

                        uint iStartTime = keyframes[StartKey].Time;
                        playControl.SetBlockData(0, iStartTime, 0, -1);//last block will loop to head...
                    }
                    else
                    {
                        BoneTimeControl[countBone] = new TimePlayControl(blockAmount);//有n格就有n-1個block
                        TimePlayControl playControl = BoneTimeControl[countBone];

                        for (int i = 0; i < blockAmount; i++)
                        {
                            uint iStartTime = keyframes[StartKey + i].Time;
                            uint iTimeLength = (uint)keyframes[StartKey + i + 1].Time - iStartTime;


                            if (i == blockAmount - 1)
                                playControl.SetBlockData(i, iStartTime, (ushort)iTimeLength, -1);//last block will loop to head...
                            else
                                playControl.SetBlockData(i, iStartTime, (ushort)iTimeLength, (short)(i + 1));
                        }
                    }

                    m_BonesStartKey[countBone] = StartKey;
                    m_BonesEndKey[countBone] = EndKey;
                    //countBone++;
                }
            }

            //float ratio = (float)animationData.BoneKeyframes[0].Keyframes.Length / (float)totalKey;
            //m_StartKey = (int)(startKey * ratio);
            //m_EndKey = (int)(endKey * ratio);

            //int keyAmount = m_EndKey - m_StartKey + 1;

            //Animation = animationData;


            //int countBone = 0;
            //foreach (BoneKeyframe boneKeyFrame in animationData.BoneKeyframes)
            //{
            //    //if (boneKeyFrame == null)
            //    //{
            //    //    BoneTimeControl[countBone] = new TimePlayControl(1);
            //    //    BoneTimeControl[countBone].SetBlockData(0, 0, 0, false);                    
            //    //    continue;
            //    //}

            //    Keyframe[] keyframes = boneKeyFrame.Keyframes;
            //    BoneTimeControl[countBone] = new TimePlayControl(keyAmount - 1);//有n格就有n-1個block
            //    TimePlayControl playControl = BoneTimeControl[countBone];

            //    //create time play key frame....
            //    //for (int i = 0; i < keyframes.Length; i++)
            //    for (int i = 0; i < keyAmount-1; i++)
            //    {
            //        int iStartTime;
            //        int iTimeLength;

            //        if (i == keyAmount - 1)
            //        {
            //            //最後keyframes的算長度進去block了，可以不要了...
            //        }
            //        else
            //        {
            //            if (m_StartKey + i + 1 >= keyframes.Length)
            //            {
            //                playControl.SetBlockData(i, 0, 0, -1);//last block will loop to head...
            //                break;
            //            }

            //            iStartTime = (int)keyframes[m_StartKey + i].Time;
            //            iTimeLength = (int)keyframes[m_StartKey + i + 1].Time - (int)keyframes[m_StartKey + i].Time;

            //            if (i == keyAmount - 2)
            //                playControl.SetBlockData(i, iStartTime, iTimeLength, -1);//last block will loop to head...
            //            else
            //                playControl.SetBlockData(i, iStartTime, iTimeLength, i + 1);
            //        }
            //    }
            //    countBone++;
            //}
        }

        //bool ifLoop = false;
        //float playSpeed = 0;
        public void AnimationPlay(bool bLoop, float speed, float startPercent)
        {
            //ifLoop = bLoop;
            //playSpeed = speed;

            m_iCountElapseTime = (int)(startPercent * (float)(m_iEndTime - m_iStratTime));

            //if (m_bUseTimeControl)
            {
                //foreach (TimePlayControl tpl in BoneTimeControl)
                for (int a = 0; a < BoneTimeControl.Length; a++)
                {
                    TimePlayControl tpl = BoneTimeControl[a];

                    //tpl.play();
                    tpl.playPercent(startPercent);
                    tpl.playloop(bLoop);
                    tpl.PlaySpeed = speed;
                }
            }
            //else
            //{
            //    m_bSetPlay = true;
            //    m_PlaytimeCount = 0;
            //}
        }

        public void AnimationStop()
        {
            //if (m_bUseTimeControl)
            {
                //foreach (TimePlayControl tpl in BoneTimeControl)
                for (int a = 0; a < BoneTimeControl.Length; a++)
                    BoneTimeControl[a].stop();
            }
            //else
            //{
            //    m_bSetPlay = false;
            //    m_PlaytimeCount = 0;
            //}
        }

        public bool IfAnimationPlay()
        {

            //if (m_bUseTimeControl)
            {
                //全部骨頭都停了才算動作停
                //foreach (TimePlayControl tpl in BoneTimeControl)
                for (int a = 0; a < BoneTimeControl.Length; a++)
                {
                    if (BoneTimeControl[a].IfPlay == true)
                        return true;
                }
                return false;
            }


            //else
            //{
            //    return m_bSetPlay;
            //}
        }

        public void AnimatedWithTime(int EllapseMillisecond)
        {
            AnimatedWithTime(EllapseMillisecond, true);
        }

        public void AnimatedWithTime(int EllapseMillisecond, bool bLerp)
        {
            if (IfAnimationPlay() == false)//有在PLAY中就繼續直到都停
                return;

            m_iCountElapseTime += (int)((float)EllapseMillisecond);

#if ANI_Ratio
            if (ifLoop)
            {
                if (m_iCountElapseTime > m_iTotalTime)
                {
                    AnimationStop();
                    return;
                }
            }

            float r =0;
            if (m_iTotalTime > 0)
            {
                r = ((float)m_iCountElapseTime / m_iTotalTime);// % 1f是讓他LOOP時也是0~1
                if (bLerp)
                    r = r % 1;
                else
                {
                    if (r > 1) { r = 1; }
                }
            }

            AnimatedWithRatio(r, bLerp);//改用百分比來計算，消除一段時間內沒插key的問題
            for (int a = 0; a < BoneTimeControl.Length; a++)
            {
                TimePlayControl tpl = BoneTimeControl[a];
                tpl.RefreshTime(EllapseMillisecond);
            }
#else

            //if (m_bUseTimeControl)
            {
                int bonID = 0;
                //foreach (TimePlayControl tpl in BoneTimeControl)
                for (int a = 0; a < BoneTimeControl.Length; a++)
                {
                    TimePlayControl tpl = BoneTimeControl[a];

                    tpl.RefreshTime(EllapseMillisecond);

#if UseRSTMatrix
                    unsafe
                    {
                        int keyID = (int)MathHelper.Min(
                            tpl.NowPlayBlockID + m_BonesStartKey[bonID],
                            m_Animation.BoneKeyframes[bonID].Keyframes.Length - 1);
                        fixed (RSTMatrix* pNowMatrix = &m_Animation.BoneKeyframes[bonID].Keyframes[keyID].m_RSTMatrix)
                        {
                            if (bLerp && tpl.NextPlayBlockID >= 0)
                            {
                                fixed (RSTMatrix* pNowBoneAnimatingMatrix = &NowBoneAnimatingMatrix[bonID])
                                {
                                    keyID = (int)MathHelper.Min(
                                        tpl.NextPlayBlockID + m_BonesStartKey[bonID],
                                        m_Animation.BoneKeyframes[bonID].Keyframes.Length - 1);
                                    fixed (
                                    RSTMatrix* pNextMatrix = &m_Animation.BoneKeyframes[bonID].Keyframes[keyID].m_RSTMatrix)
                                    {
                                        Vector3.Lerp(ref pNowMatrix->m_RTMatrix.m_Translation,
                                            ref pNextMatrix->m_RTMatrix.m_Translation, tpl.NowPlayBlockPercent,
                                            out pNowBoneAnimatingMatrix->m_RTMatrix.m_Translation);

                                        Vector3.Lerp(ref pNowMatrix->m_Scale,
                                            ref pNextMatrix->m_Scale, tpl.NowPlayBlockPercent,
                                            out pNowBoneAnimatingMatrix->m_Scale);

                                        Quaternion.Slerp(ref pNowMatrix->m_RTMatrix.m_Quaternion,
                                            ref pNextMatrix->m_RTMatrix.m_Quaternion, tpl.NowPlayBlockPercent,
                                            out pNowBoneAnimatingMatrix->m_RTMatrix.m_Quaternion);
                                    }
                                }
                            }
                            else
                            {
                                NowBoneAnimatingMatrix[bonID] = *pNowMatrix;
                            }
                        }
                    }
#else
                    Matrix nowMatrix = m_Animation.BoneKeyframes[bonID].Keyframes
                                            [tpl.NowPlayBlockID + m_BonesStartKey[bonID]].BoneOrientation;

                    if (bLerp && tpl.NextPlayBlockID >= 0)
                    {
                        Matrix nextMatrix = m_Animation.BoneKeyframes[bonID].Keyframes
                                                [tpl.NextPlayBlockID + m_BonesStartKey[bonID]].BoneOrientation;
                        Matrix.Lerp(ref nowMatrix, ref nextMatrix, tpl.NowPlayBlockPercent,
                            out NowBoneAnimatingMatrix[bonID]);
                    }
                    else
                    {
                        NowBoneAnimatingMatrix[bonID] = nowMatrix;
                    }
#endif
                    bonID++;
                }
            }
            //            else
            //            {
            //                m_PlaytimeCount += (uint)EllapseMillisecond;
            //                //int NowPlayBlockID = 0;
            //                for (int bonID = 0; bonID < m_Animation.BoneKeyframes.Length; bonID++)
            //                {
            //                    BoneKeyframe nowBone = m_Animation.BoneKeyframes[bonID];

            //                    //if (nowBone.Keyframes.Length > 2)
            //                    //{
            //                    //    int a = 0;
            //                    //        a=1;
            //                    //}

            //#if UseRSTMatrix
            //                    Keyframe keyframe = m_Animation.BoneKeyframes[bonID].Keyframes[
            //                                                                                        GetNowPlayBlockID(nowBone, m_PlaytimeCount)];

            //                    NowBoneAnimatingMatrix[bonID] = keyframe.m_RSTMatrix;
            //#else
            //                    Matrix nowMatrix = m_Animation.BoneKeyframes[bonID].Keyframes
            //                        [GetNowPlayID(nowBone, m_PlaytimeCount)].BoneOrientation;

            //                    NowBoneAnimatingMatrix[bonID] = nowMatrix;
            //#endif
            //                }
            //            }

#endif


        }

        /// <summary>
        /// 取得根據目前時間位移的播放格數為第幾格
        /// </summary>
        int GetNowPlayBlockID(BoneKeyframe boneKeyFrame, uint nowTimeCount)
        {
            uint realTime = nowTimeCount % (uint)m_Animation.Duration.TotalMilliseconds;
            for (int a = 0; a < boneKeyFrame.Keyframes.Length; a++)
            {
                //uint realTime = nowTimeCount % (uint)m_Animation.Duration.TotalMilliseconds;
                if (boneKeyFrame.Keyframes[a].Time < realTime)
                {
                    if (a + 1 < boneKeyFrame.Keyframes.Length &&
                        boneKeyFrame.Keyframes[a + 1].Time > realTime)
                        return a;
                }
            }
            return 0;
        }

        /// <summary>
        /// 利用比例控制動作大小，但必須先讓撥放中的動作停止。
        /// </summary>
        public void AnimatedWithRatio(float ratio, bool bLerp)
        {
            //if (IfAnimationPlay() == true)//都沒PLAY就才可以任意控制BLOCK
            //    return;

            //if (m_bUseTimeControl)
            {
                int bonID = 0;
                //foreach (TimePlayControl tpl in BoneTimeControl)
                for (int a = 0; a < BoneTimeControl.Length; a++)
                {
                    TimePlayControl tpl = BoneTimeControl[a];

                    float percent;
                    if (bLerp)
                    {
                        percent = tpl.RefreshWithRatio(ratio);
                    }
                    else
                    {
                        tpl.RefreshWithRatio(ratio);
                        percent = 1;
                    }

#if UseRSTMatrix

                    int keyID = (int)MathHelper.Min(
                            tpl.NowPlayBlockID + m_BonesStartKey[bonID],
                            m_Animation.BoneKeyframes[bonID].Keyframes.Length - 1);

                    Keyframe nowKeyFrame = m_Animation.BoneKeyframes[bonID].Keyframes[keyID];

                    int nextFrame = tpl.NowPlayBlockID + m_BonesStartKey[bonID] + 1;
                    keyID = (int)MathHelper.Min(
                            nextFrame,
                            m_Animation.BoneKeyframes[bonID].Keyframes.Length - 1);

                    //if (nextFrame >= m_Animation.BoneKeyframes[bonID].Keyframes.Length)
                    //    nextFrame--;

                    Keyframe nextKeyFrame = m_Animation.BoneKeyframes[bonID].Keyframes[keyID];

                    Vector3.Lerp(ref nowKeyFrame.m_RSTMatrix.m_Scale,
                        ref nextKeyFrame.m_RSTMatrix.m_Scale, percent,
                        out NowBoneAnimatingMatrix[bonID].m_Scale);

                    Vector3.Lerp(ref nowKeyFrame.m_RSTMatrix.m_RTMatrix.m_Translation,
                        ref nextKeyFrame.m_RSTMatrix.m_RTMatrix.m_Translation, percent,
                   out NowBoneAnimatingMatrix[bonID].m_RTMatrix.m_Translation);

                    Quaternion.Slerp(ref nowKeyFrame.m_RSTMatrix.m_RTMatrix.m_Quaternion,
                        ref nextKeyFrame.m_RSTMatrix.m_RTMatrix.m_Quaternion, percent,
                        out NowBoneAnimatingMatrix[bonID].m_RTMatrix.m_Quaternion);
#else
                    Matrix combineMat;
                    combineMat = m_Animation.BoneKeyframes[bonID].Keyframes
                                                                   [tpl.NowPlayBlockID + m_BonesStartKey[bonID]].BoneOrientation * percent;

                    if (bLerp)
                    {
                        int nextFrame = tpl.NowPlayBlockID + m_BonesStartKey[bonID] + 1;
                        if (nextFrame >= m_Animation.BoneKeyframes[bonID].Keyframes.Length)
                            nextFrame--;
                        combineMat += m_Animation.BoneKeyframes[bonID].Keyframes
                                                                   [nextFrame].BoneOrientation * (1 - percent);
                    }

                    NowBoneAnimatingMatrix[bonID] = combineMat;
#endif

                    bonID++;
                }
            }
            //else
            //{
            //}
        }

#if UseRSTMatrix
        public RSTMatrix[] AnimatingBones
        {
            get { return NowBoneAnimatingMatrix; }
        }

        //RSTMatrix[] InitMatrixBoneArray;
        ///// <summary>
        ///// 取得整個骨架動作第一格資訊
        ///// </summary>
        //public RSTMatrix[] InitMatrixBones
        //{
        //    get
        //    {
        //        if (InitMatrixBoneArray == null)
        //            InitMatrixBoneArray = RSTMatrix.GetArray(m_Animation.BoneKeyframes.Length);

        //        for (int bonID = 0; bonID < InitMatrixBoneArray.Length; bonID++)
        //            InitMatrixBoneArray[bonID] = m_Animation.BoneKeyframes[bonID].Keyframes[m_BonesStartKey[bonID]].m_RSTMatrix;
        //        return InitMatrixBoneArray;
        //    }

        //}
#else
        public Matrix[] AnimatingBones
        {
            get { return NowBoneAnimatingMatrix; }
        }
#endif

        /// <summary>
        /// 根據boneID，取得該骨頭所有keyframe資訊
        /// </summary>
        public Matrix[] GetBoneAnimationKeyframes(int boneID)
        {
            Keyframe[] Keyframes = m_Animation.BoneKeyframes[boneID].Keyframes;
            Matrix[] Matrixsss = new Matrix[Keyframes.Length];
            for (int a = 0; a < Matrixsss.Length; a++)
                Matrixsss[a] = Keyframes[a].BoneOrientation;

            return Matrixsss;
        }

        /// <summary>
        /// 根據boneID，取得該骨頭目前撥放格數為目前動作的相對第幾格。
        /// </summary>
        public int GetBoneAnimationNowOffsetKeyframeBlockID(int boneID)
        {
            return BoneTimeControl[boneID].NowPlayBlockID;

            //BoneKeyframe nowBone = m_Animation.BoneKeyframes[boneID];
            //return GetNowPlayBlockID(nowBone, m_PlaytimeCount);
        }

        /// <summary>
        ///  取得目前撥放動態的撥放百分比0~1
        /// </summary>
        public float GetPlayPercent()
        {
            if (m_iTotalTime == 0)
                return 0;

            float r = ((float)m_iCountElapseTime / m_iTotalTime) % 1f;// % 1f是讓他LOOP時也是0~1
            //if (r > 1)
            //    r = 1;
            return r;
        }
    }



    /// <summary>
    /// Animation Table List
    /// </summary>
    public class AnimationTableDataArray
    {
        public int? FramesPerSecond = null;//null表示每秒24格
        public AnimationTableData[] AnimationDataList;
    }

    public struct AnimationTableData
    {
        public string ActionName;
        public int StartKey;
        public int EndKey;
    }

    /*
    public class AnimationTable
    {
        public List<AnimationTableData> AnimationList;
        public AnimationTable()
        {
            AnimationList = new List<AnimationTableData>();
            initialize();
        }

        ~AnimationTable()
        {
            AnimationList.Clear();
        }

        public void initialize()
        {
            //AnimationTableData data;

            //data.ActionName = "idle";
            //data.StartKey = 366;
            //data.EndKey = 384;
            //AnimationList.Add(data);

            //data.ActionName = "GoLeft";
            //data.StartKey = 354;
            //data.EndKey = 366;
            //AnimationList.Add(data);

            //data.ActionName = "ReturnLeft";
            //data.StartKey = 192;
            //data.EndKey = 205;
            //AnimationList.Add(data);
        }

        /// <summary>
        /// 檔名不需要加.XML
        /// </summary>
        /// <param name="tableFileName"></param>
        /// <returns></returns>
        public bool LoadAnimationTable(string tableFileName)
        {
            string workDirectory = Directory.GetCurrentDirectory() + "\\" + tableFileName + ".xml";
            if (File.Exists(workDirectory))
            {
                //讀檔
                FileStream fp = File.Open(workDirectory, FileMode.Open);
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(AnimationTableDataArray));
                AnimationTableDataArray animationTableDataArray = (AnimationTableDataArray)xmlSerializer.Deserialize(fp);
                fp.Close();

                for (int a = 0; a < animationTableDataArray.AnimationDataList.Length; a++)
                    AnimationList.Add(animationTableDataArray.AnimationDataList[a]);
                //foreach (AnimationTableData data in animationTableDataArray.AnimationDataList)
                //    AnimationList.Add(data);

                return true;
            }
            else
                OutputBox.ShowMessage(workDirectory + " File not found!!!");
            
            return false;
        }
    }

*/



}
