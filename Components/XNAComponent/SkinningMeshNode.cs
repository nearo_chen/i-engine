﻿//#define OpenMP
//#define UseThreadPool

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using I_XNAUtility;
using MeshDataDefine;


namespace I_XNAComponent
{
    public class SkinningMeshNode : SceneNode
    {
        public CollisionEditor.CollisionSetting m_CollSetting;//綁在骨架上的box們
        public LocatorNode[] m_CollBoneNodes = null;//對應m_CollSetting下Box的數量
        public string ModelFileName = null;

        public List<ModelMesh> m_NotDrawModelMesh = null;

        /// <summary>
        /// 骨架動畫下面分裂的一堆模型，看哪個不要畫就把他加進來
        /// </summary>
        /// <param name="meshName"></param>
        public void AddNotDrawModelMesh(string meshName)
        {
            if (m_NotDrawModelMesh == null)
                m_NotDrawModelMesh = new List<ModelMesh>(32);

            ModelMesh modelMesh = m_SkinningMesh.GetModelMesh(meshName);
            m_NotDrawModelMesh.Add(modelMesh);
        }

        /// <summary>
        /// 清除不要畫的骨架模型parts
        /// </summary>
        public void ClearNotDrawModelMesh()
        {
            if (m_NotDrawModelMesh == null)
                return;
            m_NotDrawModelMesh.Clear();
        }

        public static SkinningMeshNode Create()
        {
            return ReleaseCheck.CreateNew(typeof(SkinningMeshNode)) as SkinningMeshNode;
        }

        public SkinningMeshNode()
        {
        }

        override protected void v_Dispose()
        {
            if (m_SkinningMesh != null)
            {
                m_SkinningMesh.Dispose();
                m_SkinningMesh = null;
            }

            if (m_NotDrawModelMesh != null)
                m_NotDrawModelMesh.Clear();
            m_NotDrawModelMesh = null;

            m_CollSetting.Box = null;//綁在骨架上的box們
            m_CollBoneNodes = null;//對應m_CollSetting下Box的數量
            ModelFileName = null;

            if (m_MeshPartDatas != null)
            {
                foreach (MeshNode.RenderMeshPartData[] ppppp in m_MeshPartDatas)
                    foreach (MeshNode.RenderMeshPartData ddd in ppppp)
                        ddd.Dispose();
            }
            m_MeshPartDatas = null;
        }

        //public bool LoadAnimationTable=true;
        protected SkinningMesh m_SkinningMesh;

        public override ModelMesh GetModelMeshPart(out int partID)
        {
            throw new NotImplementedException();
        }

        public float GetNowAnimationPlayPercent()
        {
            return m_SkinningMesh.GetPlayPercent();
        }

    
        protected override void v_Render(EffectRenderManager effectRenderManager, GraphicsDevice device)
        {
            UpdateViewCullingBoxPose(this);

            for (int meshID = 0; meshID < m_SkinningMesh.ModelSplitAmount(); meshID++)
            {
                ModelMesh modelMesh = m_SkinningMesh.GetModelMesh(meshID);

                if (m_NotDrawModelMesh != null && m_NotDrawModelMesh.IndexOf(modelMesh) >= 0)
                    continue;

                effectRenderManager.SetSkinnedMeshEffectPar(
                    m_SkinningMesh.GetSkinningBoneArray(meshID), m_SkinningMesh.GetParentAbsolute(meshID));

                for (int partID = 0; partID < modelMesh.MeshParts.Count; partID++)
                {
                    Texture2D texture = m_SkinningMesh.GetModelMeshPartTexture(meshID, partID);
                    effectRenderManager.SetSkinnedMeshPartEffectPar(texture, this);
                 //   m_SkinningMesh.RenderMeshPart(modelMesh.MeshParts[partID], modelMesh.IndexBuffer, modelMesh.VertexBuffer, device);
                    m_SkinningMesh.RenderMeshPart(m_MeshPartDatas[meshID][partID], device);
                }
            }
        }

        MeshNode.RenderMeshPartData [][] m_MeshPartDatas;
        virtual public AnimationTableDataArray LoadContent(string modelName, ContentManager content, GraphicsDevice device)
        {
            ModelFileName = modelName;

            if (m_SkinningMesh != null)
                OutputBox.ShowMessage(modelName + " already loaded!");

            //獨跟該模型相對應的動作key frame表格....
            m_SkinningMesh = new SkinningMesh();

            //object ooo = Utility.LoadXMLFile(modelName + "_ani.xml", typeof(AnimationTableDataArray), false);
            //if (ooo != null)
            //    m_SkinningMesh.LoadContent(modelName, content, ((AnimationTableDataArray)ooo).AnimationDataList);
            //else
            //    m_SkinningMesh.LoadContent(modelName, content, null);

            object ooo = m_SkinningMesh.LoadContent(modelName, content, modelName + "_ani.xml");

            AnalyzeMeshBoundingData((Dictionary<string, object>)m_SkinningMesh.GetModelMesh(0).Tag, out m_IndexBufferSize);//m_SkinningMesh.GetModelTag);
            AnalyzeMeshTexture((Dictionary<string, object>)m_SkinningMesh.GetModelMesh(0).Tag,//m_SkinningMesh.GetModelTag, 
                m_SkinningMesh.GetModelMesh(0), 0);

            m_MeshPartDatas = new MeshNode.RenderMeshPartData[m_SkinningMesh.ModelSplitAmount()][];
            for (int meshID = 0; meshID < m_MeshPartDatas.Length; meshID++)
            {
                ModelMesh modelMesh = m_SkinningMesh.GetModelMesh(meshID);
                m_MeshPartDatas[meshID] = new MeshNode.RenderMeshPartData[modelMesh.MeshParts.Count];
                for (int b = 0; b < m_MeshPartDatas[meshID].Length; b++)
                    m_MeshPartDatas[meshID][b] = new MeshNode.RenderMeshPartData(modelMesh, modelMesh.MeshParts[b]);
            }

            if (ooo == null)
                return null;
            return ooo as AnimationTableDataArray;
        }

        public void AllAnimationStop()
        {
            m_SkinningMesh.AllAnimationStop();
        }

        public void SetAnimationStop(int iAnimationID)
        {
            m_SkinningMesh.SetAnimationStop(iAnimationID);
        }

        public void SetBlendAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent)
        {
            m_SkinningMesh.SetBlendAnimationPlay(iAnimationID, bLoop, speed, startPercent);
        }

        public void SetBlendAnimationRatio(float BlendRatio)
        {
            m_SkinningMesh.SetBlendAnimationRatio(BlendRatio);
        }

        public void SetAnimationPlay(int animation, bool bLoop, float speed, float startPercent, AnimationPlayerManager.AnimationPriority priority)
        {
            m_SkinningMesh.SetAnimationPlay(animation, bLoop, speed, startPercent, priority);
        }

        public void SetAnimationPlay_BlendSwitch(int animation, bool bLoop, float speed, float startPercent, int switchTime, AnimationPlayerManager.AnimationPriority priority)
        {
            m_SkinningMesh.SetAnimationPlay_BlendSwitch(animation, bLoop, speed, startPercent, switchTime, priority);
        }

        public void AnimatedWithRatio(int iAnimationID, float ratio)
        {
            m_SkinningMesh.AnimatedWithRatio(iAnimationID, ratio);
        }

        public bool IfAnimationPlay(int animation)
        {
            return m_SkinningMesh.IfAnimationPlay(animation);
        }

        public int GetNowPlayingAnimationID()
        {
            return m_SkinningMesh.GetNowPlayingAnimationID();
        }

        public int GetLastPlayAnimationID()
        {
            return m_SkinningMesh.GetLastPlayAnimationID();
        }

        virtual public void UpdateSkinningTransform(int EllapseMillisecond)
        {
            m_SkinningMesh.UpdateSkinningTransform(EllapseMillisecond, WorldMatrix);
        }

        //protected override bool v_ConsiderCurrentTechniqueOK(RenderTechnique CurrentRenderTechnique)
        //{
        //    return MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.SkinnedMesh);
        //}

        public LocatorNode GreateBoneTree()
        {
            LocatorNode locator_node = null;
            m_SkinningMesh.CreateBoneTree(out locator_node);
            return locator_node;
        }

        public void RenderDebug()
        {
            m_SkinningMesh.RenderDebug(BoundingSphere.Radius * 0.009f);
        }
    }



    /// <summary>
    /// 注意：SoftwareSkinningMeshNode若是需要做shadoe volume，就不能因為骨架太多做模型分裂，編譯模型檔時需注意。
    /// </summary>
    public class SoftwareSkinningMeshNode : SkinningMeshNode
    {
        object m_Lock = new object();
        VertexDeclaration m_VertexDeclaration;                  //頂點格式
        VertexPositionNormalTexture[][] m_SkinVertexList;//記錄每個model mesh的vertex buffer。
        byte[][][] m_VertexIndexList;                                     //記錄每個model mesh的vertex 的blend index。
        Vector4[][] m_BoneWeightList;                             //記錄每個model mesh的vertex 的bone weight。
        DynamicVertexBuffer[] m_DynVertBuffer;               //提供給mesh node畫出用
        int m_VertexStride = 4 * 3 + 4 * 3 + 4 * 2; //Position , Normal, Texture
        //IndexBuffer[] m_IndexBuffer;                                 //提供給mesh node畫出用
        MeshNode[] m_SkinMeshNodeList;//建立在底下的child mesh node。
        int[] m_SkinMeshNodeID;//紀錄mesh node的meshID，做為取出骨架的索引。

        VertexPositionNormalTexture[][] m_TmpRender;//畫圖時暫存用。 //這個暫存的可以一維就好存玩買上dynamicVB.setData<>...


        public SoftwareSkinningMeshNode() { }

        public static SoftwareSkinningMeshNode Create()
        {
            return ReleaseCheck.CreateNew(typeof(SoftwareSkinningMeshNode)) as SoftwareSkinningMeshNode;
        }

        protected override void v_Dispose()
        {
            ThreadManager.ReleaseThreadID(ref threadID);


            base.v_Dispose();

            if (m_VertexDeclaration != null)
                m_VertexDeclaration.Dispose();
            m_VertexDeclaration = null;

            lock (m_Lock)
            {
                if (m_SkinMeshNodeList != null)
                {
                    for (int a = 0; a < m_SkinMeshNodeList.Length; a++)
                        m_SkinMeshNodeList[a] = null;
                    m_SkinMeshNodeList = null;
                }
                m_SkinMeshNodeID = null;

                if (m_TmpRender != null)
                {
                    for (int a = 0; a < m_TmpRender.Length; a++)
                        m_TmpRender[a] = null;
                    m_TmpRender = null;
                }

                if (m_SkinVertexList != null)
                {
                    for (int a = 0; a < m_SkinVertexList.Length; a++)
                        m_SkinVertexList[a] = null;
                    m_SkinVertexList = null;
                }

                if (m_DynVertBuffer != null)
                {
                    for (int a = 0; a < m_DynVertBuffer.Length; a++)
                    {
                        m_DynVertBuffer[a].Dispose();
                        m_DynVertBuffer[a] = null;
                    }
                    m_DynVertBuffer = null;
                }

                m_VertexIndexList = null;
            }
        }

        //struct PNTI
        //{
        //    public Vector3 pos, normal;
        //    public Vector2 tex;
        //    public byte b1, b2, b3, b4;
        //    public Vector4 www;
        //}

        override public AnimationTableDataArray LoadContent(string modelName, ContentManager content, GraphicsDevice device)
        {
            AnimationTableDataArray aaa = base.LoadContent(modelName, content, device);

            m_VertexDeclaration = new VertexDeclaration(device, VertexPositionNormalTexture.VertexElements);

            //if (m_SkinningMesh.ModelSplitAmount() > 1)
            //{

            //}

            //將Model底下的ModelMesh底下的MeshPart抓出來建立MeshNode。       
            int nodeCount = 0;
            for (int meshID = 0; meshID < m_SkinningMesh.ModelSplitAmount(); meshID++)
            {
                ModelMesh modelMesh = m_SkinningMesh.GetModelMesh(meshID);
                for (int partID = 0; partID < modelMesh.MeshParts.Count; partID++)
                {
                    MeshNode meshNode = MeshNode.Create();
                    meshNode.IfLightEnable = true;

                    MeshNode.ProcessMeshNodeInfo(modelMesh.Name + "_" + meshID.ToString() + "_" + partID.ToString(), Matrix.Identity, modelMesh, partID, meshNode);
                    AttachChild(meshNode);
                    nodeCount++;
                }
            }

            //建立陣列資訊方便以後繪圖用
            m_SkinMeshNodeList = new MeshNode[nodeCount];
            m_SkinMeshNodeID = new int[nodeCount];
            nodeCount = 0;
            MeshNode tmpNode = FirstChild as MeshNode;
            for (int meshID = 0; meshID < m_SkinningMesh.ModelSplitAmount(); meshID++)
            {
                ModelMesh modelMesh = m_SkinningMesh.GetModelMesh(meshID);
                for (int partID = 0; partID < modelMesh.MeshParts.Count; partID++)
                {
                    m_SkinMeshNodeList[nodeCount] = tmpNode;
                    m_SkinMeshNodeID[nodeCount] = meshID;

                    tmpNode = tmpNode.Sibling as MeshNode;
                    nodeCount++;
                }
            }

            //建立vertex buffer資訊
            m_SkinVertexList = new VertexPositionNormalTexture[m_SkinningMesh.ModelSplitAmount()][];
            m_DynVertBuffer = new DynamicVertexBuffer[m_SkinningMesh.ModelSplitAmount()];
            //m_IndexBuffer = new IndexBuffer[m_SkinningMesh.ModelSplitAmount()];
            m_VertexIndexList = new byte[m_SkinningMesh.ModelSplitAmount()][][];
            m_BoneWeightList = new Vector4[m_SkinningMesh.ModelSplitAmount()][];
            m_TmpRender = new VertexPositionNormalTexture[m_SkinningMesh.ModelSplitAmount()][];
            int maxMeshVertex = 0;
            for (int meshID = 0; meshID < m_SkinningMesh.ModelSplitAmount(); meshID++)
            {
                ModelMesh modelMesh = m_SkinningMesh.GetModelMesh(meshID);

                List<uint> indexList = null;
                List<Vector3> positionList = new List<Vector3>();
                List<Vector3> normalList = new List<Vector3>();
                List<Vector2> texCoordList = new List<Vector2>();
                List<Collision.byte4> boneIndexList = new List<Collision.byte4>();
                List<Vector4> boneWeightList = new List<Vector4>();
                Collision.GetMeshVBIB(modelMesh, indexList, positionList, normalList, texCoordList, boneIndexList, boneWeightList);

                //PNTI[] vbData = new PNTI[positionList.Count];
                //modelMesh.VertexBuffer.GetData<PNTI>(vbData);

                m_SkinVertexList[meshID] = new VertexPositionNormalTexture[positionList.Count];
                m_TmpRender[meshID] = new VertexPositionNormalTexture[positionList.Count];
                m_BoneWeightList[meshID] = new Vector4[positionList.Count];
                m_VertexIndexList[meshID] = new byte[positionList.Count][];

                List<Vector3>.Enumerator positionItr = positionList.GetEnumerator();
                List<Vector3>.Enumerator normalItr = normalList.GetEnumerator();
                List<Vector2>.Enumerator texCoordItr = texCoordList.GetEnumerator();
                List<Collision.byte4>.Enumerator boneIndexItr = boneIndexList.GetEnumerator();
                List<Vector4>.Enumerator boneWeightItr = boneWeightList.GetEnumerator();
                int countVertex = 0;
                while (positionItr.MoveNext() && normalItr.MoveNext() && texCoordItr.MoveNext() &&
                          boneIndexItr.MoveNext() && boneWeightItr.MoveNext())
                {
                    m_SkinVertexList[meshID][countVertex].Position = positionItr.Current;
                    m_SkinVertexList[meshID][countVertex].Normal = Vector3.Normalize(normalItr.Current);
                    m_SkinVertexList[meshID][countVertex].TextureCoordinate = texCoordItr.Current;

                    m_VertexIndexList[meshID][countVertex] = new byte[4];
                    m_VertexIndexList[meshID][countVertex][0] = boneIndexItr.Current.b1;
                    m_VertexIndexList[meshID][countVertex][1] = boneIndexItr.Current.b2;
                    m_VertexIndexList[meshID][countVertex][2] = boneIndexItr.Current.b3;
                    m_VertexIndexList[meshID][countVertex][3] = boneIndexItr.Current.b4;

                    m_BoneWeightList[meshID][countVertex] = boneWeightItr.Current;
                    countVertex++;
                }

                m_DynVertBuffer[meshID] =
                    new DynamicVertexBuffer(device, VertexPositionNormalTexture.SizeInBytes * positionList.Count, BufferUsage.WriteOnly);
                m_DynVertBuffer[meshID].ContentLost += DynVertBuffer_ContentLost;

                if (maxMeshVertex < positionList.Count)
                    maxMeshVertex = positionList.Count;

                positionList.Clear(); positionList = null;
                normalList.Clear(); normalList = null;
                texCoordList.Clear(); texCoordList = null;
                boneIndexList.Clear(); boneIndexList = null;
                boneWeightList.Clear(); boneWeightList = null;
            }
            //  m_TmpRender = new VertexPositionNormalTexture[maxMeshVertex];//建一個最大的buffer用來暫存繪圖資料用

            return aaa;
        }

        private void DynVertBuffer_ContentLost(object sender, EventArgs e)
        {
            DynamicVertexBuffer dvb = sender as DynamicVertexBuffer;
            int a = 0;
            for (a = 0; a < m_DynVertBuffer.Length; a++)
                if (m_DynVertBuffer[a] == dvb)
                    break;
            dvb.SetData(m_TmpRender[a], 0, m_TmpRender[a].Length, SetDataOptions.NoOverwrite);
        }

        void UpdateVertex()
        {
            lock (m_Lock)
            {
                if (m_SkinVertexList == null)
                    return;

                base.UpdateSkinningTransform(m_EllapseMillisecond);
                m_EllapseMillisecond = 0;

                //只根據ModelMesh的數量做頂點更新計算，因為ModelMesh底下的MeshPart使用的都是同一個ib, vb。
                //所以這邊先計算與骨架相乘後的點位置。
                for (int meshSplitID = 0; meshSplitID < m_SkinVertexList.Length; meshSplitID++)
                {
                    Matrix[] boneArray = m_SkinningMesh.GetSkinningBoneArray(meshSplitID);
                    Matrix parentAbsolute = m_SkinningMesh.GetParentAbsolute(meshSplitID);

#if OpenMP
//                    int IDDDD = meshSplitID;
//                // with threading, we'll loop between 0..dataSize, as before, but this time we will use 4 threads- avForThread will divide up the loop into 4 thread tasks
//                // for other examples, we'll need to keep roughly the same format as the for loop has here
//                // here, "using" simply forces the runtime to call the dispose method of the class, which in turn will wait for the threads to finish. 
//                // We could omit the using statement, carry on working, and then call the avForThread object's WaitToFinish() method explicitly
//                using (new avForThread(0, m_SkinVertexList[meshSplitID].Length, 4,
//                    delegate(int start, int end)
//                    {
//                        for (int i = start; i < end; i++)
//                        {
//                            CalculateVertexWithBone(boneArray, parentAbsolute, IDDDD, i);
//                        }
//                    })
//                ) ;
#elif UseThreadPool

                    CalculateData data;
                    data.boneArray = boneArray;
                    data.parentAbsolute = parentAbsolute;
                    data.splitID = meshSplitID;
                    //using (new MyAVThread(0, m_SkinVertexList[meshSplitID].Length, 4, CalculateInThreadPool, data));
                    new MyAVThread(0, m_SkinVertexList[meshSplitID].Length, 4, CalculateInThreadPool, data);
#else

                    int a = 0;
                    for (a = 0; a < m_SkinVertexList[meshSplitID].Length; a++)
                    {
                        CalculateVertexWithBone(boneArray, parentAbsolute, meshSplitID, a);
                    }

#endif
                }

                //之後再為每個MeshNode設定vb資訊。         
                MeshNode meshNode = FirstChild as MeshNode;
                int meshCount = 0;
                while (meshNode != null)
                {
                    int meshID = m_SkinMeshNodeID[meshCount];
                    if (meshNode.FirstChild == null)
                    {
                        meshNode.SetOutSideVertexBuffer(m_DynVertBuffer[meshID], m_VertexStride, m_VertexDeclaration, m_TmpRender[meshID]);
                    }
                    else
                    {
                        meshNode.Visible = false;
                        ShadowVolumeNode shadowNode = meshNode.FirstChild as ShadowVolumeNode;
                        shadowNode.RefreshFaceList(m_TmpRender[meshID]);//若是做骨架shadow volume就不能模型分裂，所以只會有一個mesh，所以直接拿m_TmpRender來用就可以了。
                    }
                    meshCount++;
                    meshNode = meshNode.Sibling as MeshNode;
                }
            }
        }

        /// <summary>
        /// 計算模型頂點陣列vertex buffer 與骨架的變形
        /// </summary>  
        void CalculateVertexWithBone(Matrix[] boneArray, Matrix parentAbsolute, int meshSplitID, int a)
        {
            Matrix finalMat, finalMat2;
            //int id = m_VertexIndexList[meshSplitID][a][0];
            //finalMat = boneArray[id];
            Matrix.Multiply(ref boneArray[m_VertexIndexList[meshSplitID][a][0]], m_BoneWeightList[meshSplitID][a].X, out finalMat);
            Matrix.Multiply(ref boneArray[m_VertexIndexList[meshSplitID][a][1]], m_BoneWeightList[meshSplitID][a].Y, out finalMat2);
            Matrix.Add(ref finalMat, ref finalMat2, out finalMat);
            Matrix.Multiply(ref boneArray[m_VertexIndexList[meshSplitID][a][2]], m_BoneWeightList[meshSplitID][a].Z, out finalMat2);
            Matrix.Add(ref finalMat, ref finalMat2, out finalMat);
            Matrix.Multiply(ref boneArray[m_VertexIndexList[meshSplitID][a][3]], m_BoneWeightList[meshSplitID][a].W, out finalMat2);
            Matrix.Add(ref finalMat, ref finalMat2, out finalMat);
            Matrix.Multiply(ref parentAbsolute, ref finalMat, out finalMat);

            Vector3.Transform(ref m_SkinVertexList[meshSplitID][a].Position, ref finalMat, out m_TmpRender[meshSplitID][a].Position);
            Vector3.TransformNormal(ref m_SkinVertexList[meshSplitID][a].Normal, ref finalMat, out m_TmpRender[meshSplitID][a].Normal);//這裡一定要用TransformNormal

            m_TmpRender[meshSplitID][a].TextureCoordinate = m_SkinVertexList[meshSplitID][a].TextureCoordinate;
        }

#if UseThreadPool
        struct CalculateData
        {
            public Matrix[] boneArray;
            public Matrix parentAbsolute;
            public int splitID;
        }

        void CalculateInThreadPool(object myObj, int arrayID)
        {
            CalculateData ddd = (CalculateData)myObj;
            CalculateVertexWithBone(ddd.boneArray, ddd.parentAbsolute, ddd.splitID, arrayID);
        }
#endif

        void UpdateThreadVertex(object o)
        {
            while (threadID != -1)//ThreadManager.ReleaseThread 被摧毀時會變成-1
            {
                UpdateVertex();

                ThreadManager.Sleep(33);
                //  ThreadManager.SuspendThread(threadID);
            }
        }
        int threadID = -1;//ThreadManager.ReleaseThread 被摧毀時會變成-1

        int m_EllapseMillisecond;
        public override void UpdateSkinningTransform(int EllapseMillisecond)//點太多計算每個點的transform會lag
        {
            m_EllapseMillisecond += EllapseMillisecond;


            //MyThreadPool.QueueUserWorkItem(UpdateThreadVertex, null);
            //UpdateVertex();

            if (threadID == -1)
            {
                threadID = ThreadManager.CreateThread(UpdateThreadVertex, null, "UpdateVertex", 0);
            }


            //UpdateVertex();
        }

        //把他override掉讓他不要用原本shader的skinning mesh node的方法畫ㄌ
        protected override void v_Render(EffectRenderManager effectRenderManager, GraphicsDevice device)
        {
            //base.v_Render(effectRenderManager, device);
        }


    }
}
