﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;

namespace I_XNAComponent
{
    /// <summary>
    /// 此class處理各種需要Effect特別畫出的物件。
    /// 為了管理方便，將各種物件都加在裡面統一管理如Ocean，和Bubble...之類
    /// 但每個EffectObject只能產生一個物件，如Ocean，和Bubble不能共存。
    /// </summary>
    public class EffectObject : IDisposable
    {
        LocatorNode m_RefNode;

        MyOcean m_Ocean;
        MyBubble m_Bubble;

        public EffectObject()
        {
        }

        ~EffectObject()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (m_RefNode != null)
            {
                m_RefNode.DetachFromParent();
                m_RefNode = null;
            }
            if (m_Ocean != null)
            {
                m_Ocean.Dispose();
                m_Ocean = null;
            }
            if (m_Bubble != null)
            {
                m_Bubble.Dispose();
                m_Bubble = null;
            }       
        }

        public void Update()
        {          
        }
        
        public void Render(//GameTime gameTime,
            float ellapsemillisecond,
            ref Matrix view, ref Matrix invView, ref Matrix proj,
            GraphicsDevice device, ContentManager content,
            Vector3 LightDirection, Vector3 LightAmbient, Vector3 lightDiffuse, Vector3 LightSpecular,
            Vector3 fogColor, float fogStart, float fogEnd, CubeMapCreator cubeMap)
        {
            if (m_RefNode == null)
                return;
            Matrix world = Matrix.CreateScale(100) * m_RefNode.WorldMatrix;
            //Vector3 scale, translate;
            //Quaternion quat;
            //world.Decompose(out scale, out quat, out translate);
            //scale.Y = 0;
            //world = Matrix.CreateScale(scale * 100) * Matrix.CreateFromQuaternion(quat) * Matrix.CreateTranslation(translate);
            if (m_Ocean != null)
            {
                m_Ocean.SetSunLightDirection(LightDirection, LightAmbient, lightDiffuse, LightSpecular);
                m_Ocean.SetFog(fogColor, fogStart, fogEnd);
                m_Ocean.Draw(ellapsemillisecond, ref world, ref view, ref invView, ref proj, device, content, cubeMap);
            }
        }

        public void CreateOcean( 
            FrameNode sceneRootNode,
            SpecialMeshDataManager.MyOceanData ocean,
        ContentManager content, GraphicsDevice device)
        {
            if (string.IsNullOrEmpty(ocean.ReferenceOceanPlaneName) == true)
            {
                OutputBox.ShowMessage("ocean.ReferenceOceanPlaneName == null");
                return;
            }

            SceneNode OceanPlaneNode =
                SceneNode.SeekFirstMeshNodeAllName(sceneRootNode, ocean.ReferenceOceanPlaneName) as SceneNode;            
            if (OceanPlaneNode == null)
            {
                OutputBox.ShowMessage("ocean.ReferenceOceanPlaneName == Not Found");
                return;
            }
            OceanPlaneNode.Visible = false;

            m_RefNode = LocatorNode.Create();
            OceanPlaneNode.AttachChild(m_RefNode);
            //rootNode.AttachChild(m_RefNode);

            m_RefNode.NodeName = "RefNode_" + ocean.Paramaters.OceanName;
            m_Ocean = new MyOcean(ocean.Paramaters, content, device);

            //如果有要建立海岸的海浪的話....
            if (ocean.DepthMapData != null && ocean.CoastDepthMapSize != 0)
            {
                m_Ocean.SetCoastData(ocean.DepthMapData, ocean.CoastDepthMapSize, sceneRootNode, device);
            }
        }

        /// <summary>
        /// 傳出viewMatrix為null表示此effect不畫cube map。
        /// </summary>
        //public Matrix? SetRenderTarget_PreRenderCubeMap(GraphicsDevice device)
        //{
        //    Matrix? viewMat = null;
        //    if (m_Ocean != null)// && m_Ocean.IfCreateCubeMap)
        //        viewMat = m_Ocean.CubeMapCreator.SetRenderTarget_1Face(device, 0, m_RefNode.WorldPosition);
        //    return viewMat;
        //}

        /// <summary>
        /// 傳出viewMatrix為null表示此effect不畫depth map。
        /// </summary>
        public void SetRenderTarget_PreRenderDepthMap(GraphicsDevice device, out Matrix? view, out Matrix? projection)
        {
            view = null;
            projection = null;
            if (m_Ocean != null && m_Ocean.DepthMapCreator !=null)
            {
                m_Ocean.DepthMapCreator.SetRenderTarget(device, null, out view, out projection);
            }
        }

        public NodeArray GetDepthMapRenderNodeArray()
        {
            if (m_Ocean != null && m_Ocean.DepthMapCreator != null)
                return m_Ocean.CoastRenderNodeArray;//DepthMapCreator.GetRenderNodeArray();
            return null;
        }

        //Debug 用
        //public void SaveEnvMapToDDS()
        //{
        //    if (m_Ocean != null)
        //        m_Ocean.CubeMapCreator.SaveCubeMap();
        //}

        public Texture2D GetDepthMap()
        {
            if (m_Ocean != null && m_Ocean.DepthMapCreator != null)
                return m_Ocean.DepthMapCreator.GetDepthMap;
            return null;
        }

        public void DrawDebugLine()
        {
            if (m_Ocean != null && m_Ocean.DepthMapCreator != null)
            {
                BoundingFrustum frustum = new BoundingFrustum(m_Ocean.DepthMapCreator.DepthMapFrustum);
                DrawLine.AddFrustumLine(frustum, Color.Yellow);
            }
        }
    }
}
