﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;
using MeshDataDefine;

namespace I_XNAComponent
{
    public class KeyframeMeshLoader : SceneMeshLoader, IAnimationPlay
    {
        KeyFrameMesh m_KFMesh = null;
        //擴充功能，//也許會有每個BONE對應的NODE，用這可以將它存起來。
        FrameNode[] m_RefNodes;
        int treeCount;

        AnimationTableDataArray m_AnimationTableDataArray;
        public AnimationTableDataArray GetAniArray() {  return m_AnimationTableDataArray;  }

        new public static KeyframeMeshLoader Create()
        {
            return ReleaseCheck.CreateNew(typeof(KeyframeMeshLoader)) as KeyframeMeshLoader;
        }

        protected override void v_Dispose()
        {
            m_KFMesh.Dispose();
            m_KFMesh = null;

            for (int a = 0; a < m_RefNodes.Length; a++)
                m_RefNodes[a] = null;
            m_RefNodes = null;

            base.v_Dispose();
        }


        public static AnimationTableDataArray LoadKeyFrameMesh(string fileName,
         ContentManager content, GraphicsDevice device,
         out KeyframeMeshLoader keyframeMeshLoader)
        {
            Model model = content.Load<Model>(fileName);

            KeyframeMeshLoader loader = KeyframeMeshLoader.Create();
            if (loader.InitScene(model, content, device) == false)
            {
                loader.Dispose();
                loader = null;
                keyframeMeshLoader = loader;
                return null;
            }

            loader.SetReferenceNodes();

            //讀取key frame mesh
            loader.m_KFMesh = new KeyFrameMesh();
            AnimationTableDataArray aniTableArray = loader.m_KFMesh.LoadContent(model, content, fileName + "_ani.xml");

            loader.m_AnimationTableDataArray = aniTableArray;
            keyframeMeshLoader = loader;
            return aniTableArray;
        }

        void SetReferenceNodes()
        {
            treeCount = 0;
            m_RefNodes = new FrameNode[m_CountAllNodes];
            SetReferenceNode((FrameNode)GetRootNode.FirstChild);
        }

        void SetReferenceNode(FrameNode parentNode)
        {
            m_RefNodes[treeCount] = parentNode;
            treeCount++;

            FrameNode node = (FrameNode)parentNode.FirstChild;
            while (node != null)
            {
                SetReferenceNode(node);
                node = (FrameNode)node.Sibling;
            }
        }

        protected override void v_Update(int ellapseMillisecond, ContentManager content, GraphicsDevice device)
        {
            UpdateKeyFrameTransform(ellapseMillisecond);
        }

        /// <summary>
        /// 根據時間做key frame判斷，還有更新reference node的matrix。
        /// </summary>
        public void UpdateKeyFrameTransform(int ellapsedMilliseond)
        {
            m_KFMesh.UpdateAnimateKeyFrameTransform(ellapsedMilliseond);

            for (int i = 0; i < m_RefNodes.Length; i++)
                m_RefNodes[i].LocalMatrix = m_KFMesh.GetBoneAnimateLocalMatrix(i);
        }

        public bool IfAnimationPlay(int animation)
        {
            return m_KFMesh.IfAnimationPlay(animation);
        }

        public void SetAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent, AnimationPlayerManager.AnimationPriority priority)
        {
            m_KFMesh.SetAnimationPlay(iAnimationID, bLoop, speed, startPercent, priority);
            SetAnimationWithMOPPlay(iAnimationID);
        }

        public void SetAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent)
        {
            SetAnimationPlay(iAnimationID, bLoop, speed, startPercent, AnimationPlayerManager.AnimationPriority.Low);
        }

        public void AllAnimationStop()
        {
            m_KFMesh.AllAnimationStop();
            SetAllMuliObjectPlayerStop();            
        }

        public void SetAnimationStop(int iAnimationID)
        {
            m_KFMesh.SetAnimationStop(iAnimationID);
        
        }

        public void AnimatedWithRatio(int iAnimationID, float ratio)
        {
            m_KFMesh.AnimatedWithRatio(iAnimationID, ratio);
        }

        public void SetAnimationPlay_BlendSwitch(int animation, bool bLoop, float speed, float startPercent, int switchTime,
            AnimationPlayerManager.AnimationPriority priority)
        {
            m_KFMesh.SetAnimationPlay_BlendSwitch(animation, bLoop, speed, startPercent, switchTime, priority);
            SetAnimationWithMOPPlay(animation);
        }

        public void SetAnimationPlay_BlendSwitch(int animation, bool bLoop, float speed, float startPercent, int switchTime)
        {
            SetAnimationPlay_BlendSwitch(animation, bLoop, speed, startPercent, switchTime, AnimationPlayerManager.AnimationPriority.Low);
        }

        public void SetBlendAnimationPlay(int animation, bool bLoop, float speed, float startPercent)
        {
            m_KFMesh.SetBlendAnimationPlay(animation, bLoop, speed, startPercent);
        }

        public void SetBlendAnimationRatio(float ratio)
        {
            m_KFMesh.SetBlendAnimationRatio(ratio);
        }

        /// <summary>
        /// 取得目前動作播放keyframe block相對於起始撥放block的播放格數，
        /// 也就是取得該骨頭，目前撥放到第幾格。
        /// </summary>
        public int GetNowAnimationPlayKeyframeBlockID(string meshName)
        {
            int boneID = -1;
            for (int a = 0; a < m_RefNodes.Length; a++)
            {
                if (m_RefNodes[a].NodeName.Contains(meshName))
                {
                    boneID = a;
                    break;
                }
            }

            return m_KFMesh.GetNowAnimationPlayKeyframeBlockID(boneID);
        }

        public float GetNowAnimationPlayPercent()
        {
            return m_KFMesh.GetPlayPercent();
        }

        /// <summary>
        /// 取得該骨頭時間上所有matrix
        /// </summary>      
        public Matrix[] GetBoneAllKeyframes(string meshName)
        {
            int boneID = -1;
            for (int a = 0; a < m_RefNodes.Length; a++)
            {
                if (m_RefNodes[a].NodeName.Contains(meshName))
                {
                    boneID = a;
                    break;
                }
            }

            return m_KFMesh.GetBoneAllAnimationMatrix(boneID);
        }


        public int GetNowPlayingAnimationID()
        {
            return m_KFMesh.GetNowPlayingAnimationID();
        }

        public int GetLastPlayAnimationID()
        {
            return m_KFMesh.GetLastPlayAnimationID();
        }
    }
}
