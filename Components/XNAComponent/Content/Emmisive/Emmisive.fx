
sampler SceneTexture : register(s0);
sampler EmmisiveMap : register(s1);
//float maxAlpha = 1.0f;

float4 Emmisive_PixelShader(float2 TexCoord : TEXCOORD0) : COLOR0
{
	float4 finalColor = tex2D(SceneTexture, TexCoord);
	float4 emmColor = tex2D(EmmisiveMap, TexCoord);

	//float alpha = clamp(emmColor.a, 0, maxAlpha);

	finalColor.rgb = finalColor.rgb + emmColor.rgb * emmColor.a;
	//finalColor.rgb = lerp(finalColor.rgb , emmColor.rgb , emmColor.a);
	
	return finalColor;
}

technique Distort
{
    pass
    {
        PixelShader = compile ps_3_0 Emmisive_PixelShader();
    }
}
