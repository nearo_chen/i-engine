// Effect uses a scrolling displacement texture to offset the position of the main
// texture. Depending on the contents of the displacement texture, this can give a
// wide range of refraction, rippling, warping, and swirling type effects.

float2 DisplacementScroll; //介於-1到1之間，
float range=20; //控制取樣的範圍，數字越小對waterfall的取樣範圍越大

sampler DisplacementSampler : register(s0) = sampler_state 
{
   	MinFilter = Linear;
   	MagFilter = Linear;
   	MipFilter = Linear;   
   	AddressU  = wrap;
   	AddressV  = wrap;
};

bool bAlphaTexture=false;
sampler AlphaSampler : register(s1) ;


float4 main(float4 color : COLOR0, float2 texCoord : TEXCOORD0) : COLOR0
{
    // Look up the displacement amount.
    //waterfall是無接縫貼圖，所以不用管貼圖座標超過。
    float2 displacement = tex2D(DisplacementSampler, DisplacementScroll + texCoord / range).rg;
    
    float alpha = 1;
    if(bAlphaTexture)
		alpha = tex2D(AlphaSampler, texCoord).a;
		
//   	displacement = displacement*alpha;
    
    //由waterfall中取得0~1之間的displacement...
    return float4(displacement, alpha ,1);
}


technique Refraction
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 main();
    }
}



