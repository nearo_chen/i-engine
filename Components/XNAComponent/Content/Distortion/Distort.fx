//-----------------------------------------------------------------------------
// Distort.fx
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

sampler SceneTexture : register(s0);
sampler DistortionMap : register(s1);
float scale=0.5f;

float4 Distort_PixelShader(float2 TexCoord : TEXCOORD0) : COLOR0
{
    // Look up the displacement
    float4 displacement = tex2D(DistortionMap, TexCoord);
    
	displacement.rg -= 0.5f;
	displacement.rg *= scale;
    TexCoord += displacement.rg * displacement.b;
	
	float4 finalColor = tex2D(SceneTexture, TexCoord);


	// Look up the displaced color, without multisampling
	return   finalColor;
}

technique Distort
{
    pass
    {
        PixelShader = compile ps_1_4 Distort_PixelShader();
    }
}
