// Effect uses a scrolling displacement texture to offset the position of the main
// texture. Depending on the contents of the displacement texture, this can give a
// wide range of refraction, rippling, warping, and swirling type effects.

float2 DisplacementScroll; //介於-1到1之間，
float range=20; //控制取樣的範圍，數字越小對waterfall的取樣範圍越大



float4x4 WorldViewProjection;
texture2D displaceMap;
sampler BoardDisplacementSampler = sampler_state 
{
	Texture = <displaceMap>;
   	MinFilter = Linear;
   	MagFilter = Linear;
   	MipFilter = Linear;   
   	AddressU  = wrap;
   	AddressV  = wrap;
};


bool bAlphaTexture=false;
texture alphaBlendMap;
sampler AlphaMapSampler = sampler_state
{
    Texture = <alphaBlendMap>;
    MinFilter = linear;
    MagFilter = linear;
    MipFilter = NONE;
    AddressU = border;
    AddressV = border;
};

struct Simple_VSOut
{
	float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;
	float4 origPos : TEXCOORD1; //要用per pexil取樣才會對...
};

void vertexShader(float4 Position : POSITION0,	float2 TexCoord : TEXCOORD0,
							 out Simple_VSOut simpleOut)
{
	simpleOut.Position = mul(Position, WorldViewProjection);
	simpleOut.TexCoord = TexCoord;
	simpleOut.origPos = Position;//要用per pexil取樣才會對...
}

float4 main3DBoard(float2 texCoord : TEXCOORD0, float4 origPos : TEXCOORD1) : COLOR0
{
	//+-0.5是因為我的貼圖板子xy座標是+-0.5之間，把它弄成0~1的貼圖做標
    float2 coord = float2(origPos.x + 0.5f , origPos.y + 0.5f );//要用per pexil取樣才會對...

    // Look up the displacement amount.
    //waterfall是無接縫貼圖，所以不用管貼圖座標超過。
    float2 displacement = tex2D(BoardDisplacementSampler, DisplacementScroll + coord/range).rg;
    
    float alpha = 1;
    if(bAlphaTexture)
    {
		alpha = tex2D(AlphaMapSampler, coord).a;
		//float alpha = abs(0.5f - texCoord.x)*2;
		//float alpha = texCoord.x;
    }
    
    //由waterfall中取得0~1之間的displacement...
    return float4(displacement, alpha , 1);
}

technique Refraction3DBoard
{
    pass P0
    {
		VertexShader = compile vs_2_0 vertexShader();
        PixelShader = compile ps_2_0 main3DBoard();
    }
}