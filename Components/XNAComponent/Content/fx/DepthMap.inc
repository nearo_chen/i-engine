//static shared bool bInShadowFrustum;

float4x4 CreateDepthMapFrustum;
struct DepthMap_VSOut
{
    float4 Position : POSITION;
    float2 TexCoord : TEXCOORD0;
    float Depth     : TEXCOORD1;
};

DepthMap_VSOut CreateDepthMapVS(Simple_VSIn input, float4x4 world)
{
	DepthMap_VSOut Out;
    Out.Position =  mul(input.Position, world);
    Out.Position = mul(Out.Position, CreateDepthMapFrustum); 
   
    Out.Depth = Out.Position.z/Out.Position.w;
    Out.TexCoord =  input.TexCoord;
    return Out; 
}



//判斷傳入的點是否位於frustum裡面，回傳深度值，-1為不受影子影響。
float ConsiderInFrustum(float3 worldPosition, float4x4 ViewProjection, sampler2D depthMapSampler, float depthBias)
{
	// Find the position of this pixel in light space
    float4 lightingPosition = mul( float4(worldPosition, 1), ViewProjection);
    
    // Find the position in the shadow map for this pixel
    //float2 ShadowTexCoord = (lightingPosition.xy / lightingPosition.w + 1)/2;
    float2 ShadowTexCoord = lightingPosition.xy * 0.5f / lightingPosition.w + float2( 0.5, 0.5 );
    ShadowTexCoord.y = 1.0f - ShadowTexCoord.y;
    
    float depth=-1;
    //bInShadowFrustum = false;
   // if(max(ShadowTexCoord.x, ShadowTexCoord.y)<1 &&  min(ShadowTexCoord.x, ShadowTexCoord.y)>0) 
   if(ShadowTexCoord.x>0 && ShadowTexCoord.x <1 && ShadowTexCoord.y>0 && ShadowTexCoord.y <1)
	{		
		// Get the current depth stored in the shadow map
		float shadowdepth = tex2D(depthMapSampler, ShadowTexCoord).r;
		
		// Calculate the current pixel depth
		// The bias is used to prevent folating point errors that occur when
		// the pixel of the occluder is being drawn
		depth = lightingPosition.z/lightingPosition.w - shadowdepth - depthBias;
		//bInShadowFrustum = true;
    }
    
    return depth;  
}

