


// NOTE: even though the tangentToWorld matrix is only marked 
// with TEXCOORD3, it will actually take TEXCOORD3, 4, and 5.
struct NormalMap_VSOut
{
	float4 WVPPosition : POSITION0;
	float2 TexCoord : TEXCOORD0;
    float3 WorldViewPos : TEXCOORD1; 

    float3x3 tangentToWorld    : TEXCOORD2;

    float3 WorldPos : TEXCOORD5;
};


texture2D NormalMap;
sampler2D NormalMapSampler = sampler_state
{
    Texture = <NormalMap>;
    MinFilter = linear;
    MagFilter = linear;
    MipFilter = linear;
   AddressU  = wrap;
	AddressV  = wrap; 
};