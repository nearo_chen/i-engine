#include "SkinnedModel2Fuc.inc"

// Vertex shader program.
Simple_VSOut SkinningTransform(  Simple_VSIn input ,
														float4 BoneIndices : BLENDINDICES0,
														float4 BoneWeights : BLENDWEIGHT0)
{
    Simple_VSOut output;
    // Blend between the weighted bone matrices.
    float4x4 skinTransform = GetBoneTransform(WorldMatrix, BoneIndices, BoneWeights);
    output = SimpleMeshVS(input, skinTransform, ViewMatrix);
	return output;
}

NormalMap_VSOut NormalMapSkinningTransform(  Simple_VSIn input ,
														float4 BoneIndices : BLENDINDICES0,
														float4 BoneWeights : BLENDWEIGHT0,
														float3 binormal : BINORMAL0,
														float3 tangent   : TANGENT0)
{
	// Blend between the weighted bone matrices.
    float4x4 skinTransform = GetBoneTransform(WorldMatrix, BoneIndices, BoneWeights);
    
    Simple_VSOut simpleOut = SimpleMeshVS(input, skinTransform, ViewMatrix);

   	NormalMap_VSOut output;
   	
   	// calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors.  the pixel shader will normalize these
    // in case the world matrix has scaling.
    output.tangentToWorld[0] = mul(tangent,    skinTransform);
    output.tangentToWorld[1] = mul(binormal,    skinTransform);
    output.tangentToWorld[2] = simpleOut.WorldNormal;

	output.WVPPosition = simpleOut.WVPPosition;
	output.TexCoord = simpleOut.TexCoord;
	output.WorldViewPos = simpleOut.WorldViewPos;

	// Save the vertices postion in world space
	output.WorldPos = simpleOut.WorldPos;  //mul(input.Position, World);

	return output;
}

// Transforms the model into light space an renders out the depth of the object
DepthMap_VSOut SkinningCreateShadowMapVS(	 Simple_VSIn input,
																				  float4 BoneIndices : BLENDINDICES0,
																				  float4 BoneWeights : BLENDWEIGHT0)
{   
    // Blend between the weighted bone matrices.
    float4x4 skinTransform= GetBoneTransform(WorldMatrix, BoneIndices, BoneWeights);
	return CreateDepthMapVS(input, skinTransform);  
}



// Technique for creating the shadow map
technique SkinnedMesh_DepthMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 SkinningCreateShadowMapVS();
        PixelShader = compile ps_3_0 CreateDepthMap_PixelShader();
    }
}

technique SkinnedMesh_TengentMesh_DepthMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 SkinningCreateShadowMapVS();
        PixelShader = compile ps_3_0 CreateDepthMap_PixelShader();
    }
}

technique SkinnedMesh
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 SkinningTransform();
        PixelShader = compile ps_3_0 SimpleMeshPS();
    }
}

technique SkinnedMesh_EmmisiveMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 SkinningTransform();
        PixelShader = compile ps_3_0 SimpleMesh_EmmisivePS();
    }
}

technique SkinnedMesh_EmmisiveMap_GodsRayMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 SkinningTransform();
        PixelShader = compile ps_3_0 SimpleMesh_Emmisive_GodsRayMapPS();
    }
}


technique SkinnedMesh_TengentMesh_EmmisiveMap_GodsRayMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 NormalMapSkinningTransform();
        PixelShader = compile ps_3_0 NormalMap_Emmisive_GodsRayMapPS();
    }
}


technique SkinnedMesh_TengentMesh
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 NormalMapSkinningTransform();
        PixelShader = compile ps_3_0 NormalMapPS();
    }
}

technique SkinnedMesh_TengentMesh_EmmisiveMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 NormalMapSkinningTransform();
        PixelShader = compile ps_3_0 NormalMap_EmmisivePS();
    }
}
/*
technique SkinnedMesh_TengentMesh_EmmisiveOnly
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 SkinningTransform();
        PixelShader = compile ps_3_0 SimpleMesh_EmmisiveOnlyPS();
    }
}

technique SkinnedMesh_EmmisiveOnly
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 SkinningTransform();
        PixelShader = compile ps_3_0 SimpleMesh_EmmisiveOnlyPS();
    }
}*/













DeferredVSOut DeferredSkinningTransform(  Simple_VSIn input ,
														float4 BoneIndices : BLENDINDICES0,
														float4 BoneWeights : BLENDWEIGHT0)
{
    DeferredVSOut output;
    // Blend between the weighted bone matrices.
    float4x4 skinTransform = GetBoneTransform(WorldMatrix, BoneIndices, BoneWeights);
    output = DeferredVS(input, skinTransform);
	return output;
}



NormalMap_DeferredVSOut NormalMap_DeferredSkinningTransform(  Simple_VSIn input ,
														float4 BoneIndices : BLENDINDICES0,
														float4 BoneWeights : BLENDWEIGHT0,
														float3 binormal : BINORMAL0,
														float3 tangent   : TANGENT0)
{
	// Blend between the weighted bone matrices.
    float4x4 skinTransform = GetBoneTransform(WorldMatrix, BoneIndices, BoneWeights);
    
    DeferredVSOut simpleOut = DeferredVS(input, skinTransform);

   	NormalMap_DeferredVSOut output;
   	
   	// calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors.  the pixel shader will normalize these
    // in case the world matrix has scaling.
    output.tangentToWorld[0] = mul(tangent,    skinTransform);
    output.tangentToWorld[1] = mul(binormal,    skinTransform);
    output.tangentToWorld[2] = simpleOut.Normal;

	output.Position = simpleOut.Position;
    output.TexCoord = simpleOut.TexCoord;
    output.Depth = simpleOut.Depth;
	return output;
}

technique SkinningMesh_Deferred
{
    pass P0
    {
        vertexShader = compile vs_2_0 DeferredSkinningTransform();
        pixelShader  = compile ps_2_0 DeferredPS();
    }
}

technique SkinningMesh_TengentMesh_Deferred
{
    pass P0
    {
        vertexShader = compile vs_2_0 NormalMap_DeferredSkinningTransform();
        pixelShader  = compile ps_2_0 NormalMap_DeferredPS();
    }
}