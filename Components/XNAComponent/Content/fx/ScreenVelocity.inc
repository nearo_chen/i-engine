//used for calculate motion blur... 
float4x4 mWorldViewProjectionLast;


//在pixel shader中計算velocity，只計算view projection的變動。
float2 CalculateScreenWorldViewProjVelocityPS(float4 localPos, float4x4 WorldViewProjection)
{
	float4 vPosProjSpaceLast = mul(localPos, mWorldViewProjectionLast);
	vPosProjSpaceLast /= vPosProjSpaceLast.w;

//	float4 vPosProjSpaceCurrent = mul(localPos, world);
//	vPosProjSpaceCurrent = mul(vPosProjSpaceCurrent, view);
//	vPosProjSpaceCurrent = mul(vPosProjSpaceCurrent, Projection);
	float4 vPosProjSpaceCurrent = mul(localPos, WorldViewProjection);
	vPosProjSpaceCurrent /= vPosProjSpaceCurrent.w;

	float2 velocity = vPosProjSpaceCurrent - vPosProjSpaceLast;    
	velocity /= 2.0f;
	
	//抓只有Z位移最大的糊就好
	float zRatio = abs(vPosProjSpaceCurrent.z - vPosProjSpaceLast.z);
	if(zRatio < 0.001f)
		velocity *= zRatio / 0.001f;

	return  velocity;
}
