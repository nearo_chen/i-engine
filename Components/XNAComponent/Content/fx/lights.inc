float MaterialBankID = 0; //deferred shading中使用的材質庫ID ，0為預設值 0~1之間做ID縮放


bool bFogEnable = true;
float FogEnd = 100000;
float FogStart = 0;
float FogMaxRatio = 1;
float3 FogColor = 1;

bool LightEnable=false;
float3 LightDirection;

// light intensity
float3 I_a = { 1.0f, 1.0f, 1.0f};    // ambient
float3 I_d = { 1.0f, 1.0f, 1.0f};    // diffuse
float3 I_s = { 1.0f, 1.0f, 1.0f};     // specular

bool bSpecularMap=false;
texture2D SpecularMap;
sampler2D SpecularMapSampler = sampler_state
{
	Texture = (SpecularMap);
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = wrap;
	AddressV  = wrap;
};




// material reflectivity
float3 k_a = { 0.8f, 0.8f, 0.8f };    // ambient
float3 k_d = { 0.5f, 0.5f, 0.5f};    // diffuse
float3 k_s = {0.0f, 0.0f, 0.0f};    // specular
int m_SpecularPower = 16;
//float specularIntensity=1;

float ConsiderObjectSpecularMapColor(float2 texcoord)
{	
	if(bSpecularMap)
		return tex2D(SpecularMapSampler, texcoord).r;
	else
		return 1;
}

struct PointLight 
{
    float3 color;
    float3 position;
    float decay;    //的衰減強度
    float range;
    float strength; //計算後光的強度在加強的倍數
    float specularIntensity;//控制光的specular強度，控制地上反射的小光點強度。
};

struct SpotLight
{
	float3 position;
	float3 direction;     
	float angleTheta;  //SPOT的內圈
	float anglePhi;		//SPOT的外圈
	float strength;	//計算後光的強度在加強的倍數
	float decay;       //SPOT的衰減強度
	float3 color;      

	float range; //SPOT最遠照多遠
};


float3 CalculateDirectionLight(float3 worldN, float3 WorldPos,
											 float3 TextureColor,
											  float3 ObjectSpecularColor,
											   float specularPower,
											 float3 RV,
										//	  float3 R,
											   float3  LightDir,
											  float3 ObjectD, 
											  float3 LightD,
											   float3 ObjectA,
											    float3 LightA,
											   float3 LightS)
{
	float dotDiffuse = dot(worldN, -LightDir);
	float3 diffuse = max(0 , dotDiffuse) * LightD * ObjectD + LightA*ObjectA; 
	
	float3 S = pow( saturate( dot( RV, -LightDir) ) , specularPower);            // specular
	return (TextureColor.rgb *  diffuse) + (LightS * ObjectSpecularColor * S);
}

//This function calculates the diffuse and specular effect of a single light
//on a pixel given the world position, normal, and material properties
float3 CalculateSingleLight(PointLight light, float3 worldPosition, float3 worldNormal, 
										float3 TextureColor,
										float3 ObjectDiffuse, 
										float3 ObjectSpecularColor ,
										float specularPower,
										float3 ViewPosition)
{
     float3 PLightDirection = worldPosition - light.position;
     float lightDist = length(PLightDirection);
     PLightDirection = PLightDirection / lightDist;
     
     //calculate the intensity of the light with exponential falloff
     float baseIntensity = pow
									(
										  saturate
										  (
												(light.range - lightDist) / light.range
										  )
										  , light.decay
                                    );

     float diffuseIntensity = saturate( dot(-PLightDirection, worldNormal));
     float3 diffuse = diffuseIntensity * light.color * ObjectDiffuse * TextureColor.rgb;

     //calculate Phong components per-pixel
     float3 reflectionVector = normalize(reflect(PLightDirection, worldNormal));
     float3 directionToCamera = normalize(ViewPosition - worldPosition);

     //calculate specular component
     float3 specular = 
                        saturate
                        (
                             light.color * light.specularIntensity *
                             ObjectSpecularColor * 
                             pow
                             (
                                 saturate(dot(reflectionVector, directionToCamera)), 
                                 specularPower
                             )
                        );

     return   baseIntensity * (diffuse  *light.strength  + specular);             
}

float3 CalculateSingleSpotlight(SpotLight light, float3 worldPosition, float3 worldNormal, 
                            float3 TextureColor, 
                            float3 ObjectDiffuse,
                            float3 ObjectSpecularColor ,
                            float specularPower,
                             float3 ViewPosition)
{
	float3 directionToLight = light.position - worldPosition;
	float lightDist = length(directionToLight);
	directionToLight = directionToLight / lightDist;
	
	float coneDot = dot(-directionToLight, light.direction);
	
	float coneAttenuation=0;
    
     if(coneDot > light.anglePhi)//可以把if 弄掉，內圈的dot一定會比外圈大
	{
	    if(coneDot > light.angleTheta)
			coneAttenuation = 1;
		else
			coneAttenuation = pow( (coneDot-light.anglePhi) / (light.angleTheta-light.anglePhi) , light.decay);
		
		coneAttenuation *= pow
									  ( 
											saturate
											(
											    (light.range - lightDist) / light.range
											)
										   , light.decay
                                      );

		TextureColor.rgb = ObjectDiffuse * light.color * TextureColor.rgb * dot(worldNormal, directionToLight);
	
	
		float3 reflectionVector = normalize(reflect(-directionToLight, worldNormal));
		float3 directionToCamera = normalize(ViewPosition - worldPosition);
		//calculate specular component
		ObjectSpecularColor =// saturate(
											   light.color * ObjectSpecularColor * //specularIntensity * 
											   pow(   saturate(
																	dot(reflectionVector, directionToCamera)
																	)
														  , specularPower
													  );
									//		  );									
	
	}
	
	//return coneAttenuation * light.strength * (diffuseColor.rgb + specularColor);
	return coneAttenuation * light.strength * TextureColor.rgb +  ObjectSpecularColor;
}


PointLight PointLightArray[8];
int PointLightAmount = 0;
float3 CalculatePointLights(float3 worldN, float3 WorldPos, float3 TextureColor,
											float3 ObjectDiffuse,
										 float3 specularColor, float specularPower,
											float3 ViewPosition)
{
	//Calculate point Light...
	float3 PointLightColor = 0;
	for(int i=0; i<PointLightAmount ; i++)
	{
		PointLightColor +=
					  CalculateSingleLight(
					  PointLightArray[i],
					  WorldPos, worldN,
					  TextureColor, 
					  ObjectDiffuse,
					  specularColor,
					  specularPower,
					  ViewPosition);
	}
	return PointLightColor;
}

SpotLight SPlightArray[8]; //陣列不能給1的樣子
int SpotLightAmount = 0;
float3 CalculateSpotlights(float3 worldN, float3 worldPos, float3 TextureColor, 
										float3 ObjectDiffuse,
										float3 ObjectSpecular,
										 float specularPower,
										float3 viewPosition)
{
	float3 Color = 0;
	for(int i=0; i<SpotLightAmount ; i++)
	{
		Color += 
					  CalculateSingleSpotlight(
					  SPlightArray[i], 
					  worldPos, worldN,
					  TextureColor, 
					  ObjectDiffuse,
					  ObjectSpecular, 
					  specularPower,
					  viewPosition);
	}
	return Color;
}



//-----------------------------------------------------------------------------
// Compute fog factor
//-----------------------------------------------------------------------------
float ComputeFogFactor(float d)
{
    return 
    clamp((d - FogStart) / (FogEnd - FogStart), 0, 1)  *  bFogEnable * FogMaxRatio;
    
    
}



//V = ViewPosition - WorldPos
//L = LightDirection
//R = normalize(reflect(L, worldN));
float3 ConsiderLights(float3 TextureColor, float3 worldN, float3 WorldPos, float3 ObjectSpecularColor, float specularPower,
								 float3 ViewPosition
									,float3 RV, float3 LightDir)
{
	TextureColor = 
		CalculateDirectionLight(worldN, WorldPos, TextureColor, ObjectSpecularColor, specularPower, RV, LightDir, k_d, I_d, k_a, I_a, I_s) 
		+			CalculatePointLights(worldN, WorldPos, TextureColor, k_d, ObjectSpecularColor, specularPower, ViewPosition)
		+			CalculateSpotlights(worldN, WorldPos, TextureColor, k_d, ObjectSpecularColor, specularPower, ViewPosition);
	 return TextureColor;
}