
float RefractMapScale = 0;
float2 RefractScreenUV(float3 WorldViewNormal)
{
   WorldViewNormal.y = -WorldViewNormal.y;
   
   float amount = dot(WorldViewNormal, float3(0,0,1)) * RefractMapScale;
   return float2(.5,.5) + float2(amount * WorldViewNormal.xy);
}

