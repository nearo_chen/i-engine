
float FresnelPower;

//********Environment Map***********************************************
bool bEnvCubeMap;
textureCUBE EnvCubeMap				: ENVIRONMENT;
samplerCUBE CubeMapSampler		= sampler_state
{
	Texture = <EnvCubeMap>;
   	MinFilter = Linear;
   	MagFilter = Linear;
   	MipFilter = Linear;
   	AddressU  = clamp;
   	AddressV  = clamp;
};
//********Environment Map***********************************************

float3 CalculateEnvCubeMap(float fresnelTerm, float3 diffuseColor, float3 ReflectView, float reflectIntensity)
{
	if( bEnvCubeMap )
	{
		//float3 incident = normalize(WorldPos - ViewPosition);
		//float3 EnvR = incident - 2.0f * N * dot(incident, worldN);
		//cubeMap = texCUBE(CubeMapSampler, EnvR);
		float4 cubeMap = texCUBE(CubeMapSampler, ReflectView.zyx);

		// Approximate a Fresnel coefficient for the environment map.
		// This makes the surface less reflective when you are looking
		// straight at it, and more reflective when it is viewed edge-on.
	    //float Fresnel = saturate(1 + dot(ReflectView, worldN));	

		// Use the Fresnel coefficient to interpolate between texture and environment map.
		//if(reflectIntensity>=0)
		//	diffuseColor = lerp(diffuseColor, cubeMap, reflectIntensity*(1-fresnelTerm)*0.6f );//PColor.g*0.3f);
		//else
		//	diffuseColor = lerp(diffuseColor, cubeMap, 1-fresnelTerm);//0.25f);

		//	diffuseColor = lerp(diffuseColor, cubeMap, reflectIntensity) * fresnelTerm ;

		diffuseColor = diffuseColor + reflectIntensity * cubeMap * fresnelTerm;
	}
	return diffuseColor;
}