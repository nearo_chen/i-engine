//#include "includes2.inc"
#include "Simpleeffect2.fx"

// On Windows shader 3.0 cards, we can use hardware instancing, reading	
// the per-instance world transform directly from a secondary vertex stream.
Simple_VSOut HardwareInstancingVertexShader(Simple_VSIn input, float4x4 instanceTransform : TEXCOORD1)
{
	return SimpleMeshVS(input, transpose(instanceTransform) , ViewMatrix);
}


DepthMap_VSOut HardwareInstancing_CreateDepthMapVertexShader(Simple_VSIn input, float4x4 instanceTransform : TEXCOORD1)
{
    return CreateDepthMapVS(input, transpose(instanceTransform));
}


// Windows instancing technique for shader 3.0 cards.
technique InstanceSimpleMesh
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 HardwareInstancingVertexShader();
        PixelShader = compile ps_3_0 SimpleMeshPS();
    }
}
/*
technique InstanceSimpleMesh_EmmisiveMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 HardwareInstancingVertexShader();
        PixelShader = compile ps_3_0 SimpleMesh_EmmisivePS();
    }
}

technique InstanceSimpleMesh_EmmisiveMap_GodsRayMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 HardwareInstancingVertexShader();
        PixelShader = compile ps_3_0 SimpleMesh_Emmisive_GodsRayMapPS();
    }
}

technique InstanceSimpleMesh_TengentMesh_EmmisiveMap_GodsRayMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 HardwareInstancingVertexShader();
        PixelShader = compile ps_3_0 NormalMap_Emmisive_GodsRayMapPS();
    }
}


technique InstanceSimpleMesh_DepthMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 HardwareInstancing_CreateDepthMapVertexShader();
        PixelShader = compile ps_3_0 CreateDepthMap_PixelShader();
    }
}


technique InstanceSimpleMesh_EmmisiveOnly
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 HardwareInstancingVertexShader();
        PixelShader = compile ps_3_0 SimpleMesh_EmmisiveOnlyPS();
    }
}*/