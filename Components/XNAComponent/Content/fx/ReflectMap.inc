float4x4 ReflectionView;
bool bReflectionMap = false;
//float ReflectIntensity = 0;
texture2D ReflectionMap;
float3 ReflectPlaneNormal = float3(0, 1, 0);

sampler2D ReflectionMapSampler = sampler_state
{
	Texture = (ReflectionMap);
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = clamp;
	AddressV  = clamp;
};

float3 CalculateReflectColor(float4x4 projection, float3 diffuseColor, float3 WorldPos,
											 float3 WorldNormal, 
										  	 float fresnelTerm, float reflectIntensity)
{
	if( bReflectionMap )
	{
		projection = mul (ReflectionView, projection);
		float4 WorldReflectionViewProjection = mul (float4(WorldPos,1), projection);

		float2 ProjectedTexCoords;
		ProjectedTexCoords.x = WorldReflectionViewProjection.x/WorldReflectionViewProjection.w/2.0f + 0.5f;
		ProjectedTexCoords.y = -WorldReflectionViewProjection.y/WorldReflectionViewProjection.w/2.0f + 0.5f;

		if(max(ProjectedTexCoords.x, ProjectedTexCoords.y)<1 &&  min(ProjectedTexCoords.x, ProjectedTexCoords.y)>0) 
		{
			float4 reflectiveColor = tex2D(ReflectionMapSampler, ProjectedTexCoords);

			
			diffuseColor.rgb =
			   lerp(diffuseColor.rgb, reflectiveColor.rgb, 
			   
			    //乘上與反射平面角度差很大就不要用反射圖
				clamp( pow(dot(WorldNormal, ReflectPlaneNormal), 64) , 0 ,1)
											* (1-fresnelTerm) * reflectiveColor.a * reflectIntensity  );//0.55f);
		}
	}
	return diffuseColor;
}