#include "DepthMap.inc"


float BigShadowMapMultiSampleOffset = 20;
float3 BigShadowMapRatio = 0.95;
float BigShadowMapDepthBias = 0.002f;
bool bBigShadowMap;
float4x4 BigShadowMapFrustum;
texture2D BigShadowMap;

sampler2D BigShadowMapSampler = sampler_state
{
    Texture = <BigShadowMap>;
    MinFilter = POINT;
    MagFilter = POINT;
    MipFilter = NONE;
    AddressU = Clamp;
    AddressV = Clamp;
};

float SmallShadowMapMultiSampleOffset = 10;
float3 SmallShadowMapRatio = 0.95;
float SmallShadowMapDepthBias = 0.001f;
bool bSmallShadowMap;
float4x4 SmallShadowMapFrustum;
texture2D SmallShadowMap;

float BigShadowSampleLength = 0;
float SmallShadowSampleLength = 0;

sampler2D SmallShadowMapSampler = sampler_state
{
    Texture = <SmallShadowMap>;
    MinFilter = POINT;
    MagFilter = POINT;
    MipFilter = NONE;
    AddressU = Clamp;
    AddressV = Clamp;
};

float CalculateShadow(float3 WorldPosition, float4x4 shadowFrustum, sampler2D shadowMapSampler, float ShadowRatio, float DepthBias)
{
    //depth小於0表示水在陸地上方
    float depth = ConsiderInFrustum(WorldPosition, shadowFrustum, shadowMapSampler, DepthBias);

	float I = 1;
    if(depth >=0)
	{		
		//finalColor *= 0.5f;

		// Shadow the pixel by lowering the intensity
		I =  1 - depth * ShadowRatio;
		I = clamp(I , 0.3, 1);
	}
	
	return I;
}

float CalculateShadowSample1(float3 WorldPosition, float4x4 shadowFrustum, sampler2D shadowMapSampler,
												//	 float3 ShadowRatio,
													  float DepthBias, float sampleOffset)
{	
	//depth小於0表示水在陸地上方
    float Count = 0;
    float depth = ConsiderInFrustum(WorldPosition, shadowFrustum, shadowMapSampler, DepthBias);
    if(depth >=0)
	{
		// Shadow the pixel by lowering the intensity
		//I =  ShadowRatio;
		Count++;
	}
	return Count;
}
 
float CalculateShadowSample4XY(float3 WorldPosition, float4x4 shadowFrustum, sampler2D shadowMapSampler,
													// float3 ShadowRatio,
													  float DepthBias, float sampleOffset)
 {
	float Count = 0;

	float depth = ConsiderInFrustum(float3(WorldPosition.x+sampleOffset, WorldPosition.y, WorldPosition.z), 
														shadowFrustum, shadowMapSampler, DepthBias);
	if(depth >=0)
		Count++;

	depth = ConsiderInFrustum(float3(WorldPosition.x-sampleOffset, WorldPosition.y, WorldPosition.z), 
														shadowFrustum, shadowMapSampler, DepthBias);
	if(depth >=0)
		Count++;

	depth = ConsiderInFrustum(float3(WorldPosition.x, WorldPosition.y+sampleOffset, WorldPosition.z), 
														shadowFrustum, shadowMapSampler, DepthBias);
	if(depth >=0)
		Count++;

	depth = ConsiderInFrustum(float3(WorldPosition.x, WorldPosition.y-sampleOffset, WorldPosition.z), 
														shadowFrustum, shadowMapSampler, DepthBias);
	if(depth >=0)
		Count++;
		
		return Count++;;
 }
 
float CalculateShadowSample4XXYY(float3 WorldPosition, float4x4 shadowFrustum, sampler2D shadowMapSampler,
													// float3 ShadowRatio, 
													 float DepthBias, float sampleOffset)
{  
	float Count = 0;

	float depth = ConsiderInFrustum(float3(WorldPosition.x+sampleOffset, WorldPosition.y+sampleOffset, WorldPosition.z), 
														shadowFrustum, shadowMapSampler, DepthBias);
    if(depth >=0)
		Count++;

	depth = ConsiderInFrustum(float3(WorldPosition.x+sampleOffset, WorldPosition.y-sampleOffset, WorldPosition.z), 
														shadowFrustum, shadowMapSampler, DepthBias);
    if(depth >=0)
		Count++;

	depth = ConsiderInFrustum(float3(WorldPosition.x-sampleOffset, WorldPosition.y+sampleOffset, WorldPosition.z), 
														shadowFrustum, shadowMapSampler, DepthBias);
    if(depth >=0)
		Count++;

	depth = ConsiderInFrustum(float3(WorldPosition.x-sampleOffset, WorldPosition.y-sampleOffset, WorldPosition.z), 
														shadowFrustum, shadowMapSampler, DepthBias);
    if(depth >=0)
		Count++;

	return Count;
}

//處理大大小小shadow map的計算
float4 DiffuseWithShadowMap(float3 WorldPosition, float4 diffuse, float3 viewPos)
{
	float3 ratio = 1;
	
	//small dynamic shadow
	if(bSmallShadowMap)
	{
		float R1 = CalculateShadowSample1(WorldPosition, SmallShadowMapFrustum, SmallShadowMapSampler,
		 SmallShadowMapDepthBias, SmallShadowMapMultiSampleOffset);
		 
		 R1 += CalculateShadowSample4XXYY(WorldPosition, SmallShadowMapFrustum, SmallShadowMapSampler,
		  SmallShadowMapDepthBias, SmallShadowMapMultiSampleOffset);
		  
		  R1 += CalculateShadowSample4XY(WorldPosition, SmallShadowMapFrustum, SmallShadowMapSampler,
		  SmallShadowMapDepthBias, SmallShadowMapMultiSampleOffset);
			  
		ratio *=  pow(SmallShadowMapRatio, R1);
	}

    //big static shadow
	if(bBigShadowMap)// && bInShadowFrustum == false)
	{

		
		 float R1 = CalculateShadowSample1(WorldPosition, BigShadowMapFrustum, BigShadowMapSampler,
		  BigShadowMapDepthBias, BigShadowMapMultiSampleOffset); 
		  
		  float R2=8;
		  		  
		 float3 viewLength = viewPos - WorldPosition;
		 viewLength = viewLength * viewLength;
		 float lengthsquare = viewLength.x + viewLength.y + viewLength.z;
		 if(lengthsquare < BigShadowSampleLength * BigShadowSampleLength * 1)
		 {
			  R1 += CalculateShadowSample4XXYY(WorldPosition, BigShadowMapFrustum, BigShadowMapSampler,
			  BigShadowMapDepthBias, BigShadowMapMultiSampleOffset); 
			  
			  R1 += CalculateShadowSample4XY(WorldPosition, BigShadowMapFrustum, BigShadowMapSampler,
			  BigShadowMapDepthBias, BigShadowMapMultiSampleOffset); 
			
			  R2 = 0;
		 }		 
		 else if(lengthsquare < BigShadowSampleLength * BigShadowSampleLength * 2)
		 {
			  R1 += CalculateShadowSample4XY(WorldPosition, BigShadowMapFrustum, BigShadowMapSampler,
			  BigShadowMapDepthBias, BigShadowMapMultiSampleOffset); 
	
			  R2 = 4;			  
		 }
		 else
		 {
			 R2 = 0;
		 }
				
		 ratio *= pow(BigShadowMapRatio, R1 + R2);
    }
    
    diffuse.rgb *= ratio;
	return diffuse; 
}