//#include "includes2.inc"
#include "simpleEffect2.fx"

float WindRandomness = 0.3;//對sin波的取樣擾動亂數值的縮放值，草的擺動亂數會較縮放

// Parameters describing the billboard itself.
float BillboardWidth;
//float BillboardHeight;

struct GrassVS_IN
{
    float3 Position : POSITION0;
    float3 Normal : NORMAL0;
    float2 TexCoord : TEXCOORD0;
    float Random : TEXCOORD1;
};

Simple_VSOut VertexShader(GrassVS_IN input)
{
    // Apply a scaling factor to make some of the billboards
    // shorter and fatter while others are taller and thinner.
    float squishFactor = 0.75 + abs(input.Random) / 2;

    float width = BillboardWidth * squishFactor;
    //float height = BillboardHeight / squishFactor;
    float height = TreeHeight / squishFactor;

    // Flip half of the billboards from left to right. This gives visual variety
    // even though we are actually just repeating the same texture over and over.
    if (input.Random < 0)
        width = -width;

    // Work out what direction we are viewing the billboard from.
    float3 viewDirection = ViewMatrix._m02_m12_m22;

    float3 rightVector = normalize(cross(viewDirection, input.Normal));

    // Calculate the position of this billboard vertex.
    float3 position = input.Position;

    // Offset to the left or right.
    position += rightVector * (input.TexCoord.x - 0.5) * width;
    
    // Offset upward if we are one of the top two vertices.
    position += input.Normal * (1 - input.TexCoord.y) * height;

	//風搖動**********************************************************************
	position = WindWaveCoordV( mul(position, WorldMatrix),
													input.Random * WindRandomness,
													input.TexCoord.y,
													WindSpeed);
	//風搖動**********************************************************************

    // Apply the camera transform.
    float4 viewPosition = mul(float4(position, 1), ViewMatrix);

	Simple_VSOut output;
    output.WVPPosition = mul(viewPosition, ProjectionMatrix);
   
    //計算貼圖軸位移
	//output.TexCoord= TexCoordinateMove(input.TexCoord);
	output.TexCoord= input.TexCoord;

    output.WorldNormal =  cross(input.Normal, rightVector);
    output.WorldPos = position;
    output.WorldViewPos = viewPosition;
    
    return output;
}


//float4 PixelShader(float2 texCoord : TEXCOORD0, float4 color : COLOR0) : COLOR0
//{
//    return tex2D(TextureSampler, texCoord) * color;
//}

//MULTIPS_OUTPUT3 SimpleMesh_Emmisive_GodsRayMapPS(Simple_PSIn input)
//{
	//MULTIPS_OUTPUT3 Output;
	//float4 TextureColor = GetNowTextureColor(input.TexCoord);
	//Output.RGBColor1 = SimpleMeshPS(input, TextureColor);
    //Output.RGBColor2 = EmmisiveColor;
    //Output.RGBColor3 = GodsRayColor();
    //return Output;
//}


technique  GrassMesh_EmmisiveMap_GodsRayMap
{    
    pass RenderOpaquePixels
    {
        VertexShader = compile vs_3_0 VertexShader();
        PixelShader = compile ps_3_0 SimpleMesh_Emmisive_GodsRayMapPS();

/*
        AlphaBlendEnable = false;
                
        AlphaTestEnable = true;
        AlphaFunc = Greater;
        AlphaRef = 245;
        
        ZEnable = true;
        ZWriteEnable = true;

        CullMode = NONE;        
        */
    }
}


technique  GrassMesh
{    
    pass RenderOpaquePixels
    {
        VertexShader = compile vs_3_0 VertexShader();
        PixelShader = compile ps_3_0 SimpleMeshPS();
    }
}

technique  GrassMesh_EmmisiveMap
{    
    pass RenderOpaquePixels
    {
        VertexShader = compile vs_3_0 VertexShader();
        PixelShader = compile ps_3_0 SimpleMesh_EmmisivePS();
    }
}
/*
technique  GrassMesh_EmmisiveOnly
{    
    pass RenderOpaquePixels
    {
        VertexShader = compile vs_3_0 VertexShader();
        PixelShader = compile ps_3_0 SimpleMesh_EmmisiveOnlyPS();
    }
}


*/



