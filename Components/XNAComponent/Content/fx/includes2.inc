
#include "define.inc"

//#include "Maps.inc" //maps裡面有texture sampler要放在上面，不然程式裡設定m_GraphicDevice.SamplerStates[0].MipMapLevelOfDetailBias = -10; 都沒有用
#include "WindWave.inc"
#include "lights.inc"

#include "Emmisive.inc"
#include "GodsRay.inc"
#include "ReflectMap.inc"
#include "EnvCubeMaps.inc"
#include "ShadowMap.inc"
#include "NormalMap.inc"
#include "ScreenVelocity.inc"
#include "RefractMap.inc"
#include "ShadowVolume.inc"



//通用的

float4x4 WorldMatrix : WORLD;
float4x4 ViewMatrix : VIEW;
float4x4 ProjectionMatrix : PROJECTION;
float3 ViewPosition;


Simple_VSOut SimpleMeshVS(Simple_VSIn input, float4x4 world, float4x4 view)
{
	Simple_VSOut Out;

	// Transform the models verticies and normal
    float4 PWorld = mul(input.Position, world);
    
    PWorld.xyz = WindWaveHeight(input.Position.y, PWorld, 1, WindSpeed);
    
    float4 PWorldView =  mul(PWorld, view);

    //final data....
    Out.WVPPosition = mul(PWorldView, ProjectionMatrix);

	//計算貼圖軸位移
	Out.TexCoord= TexCoordinateMove(input.TexCoord);

    Out.WorldNormal =  normalize(mul(input.Normal, world));
    Out.WorldPos = PWorld;
    Out.WorldViewPos = PWorldView;

	return Out; 
}

Simple_VSOut SimpleMeshVS(Simple_VSIn input)
{
    Simple_VSOut Output;
	// Transform the models verticies and normal
    Output = SimpleMeshVS(input, WorldMatrix, ViewMatrix);
    return Output;
}

NormalMap_VSOut NormalMapVS(Simple_VSIn input,
															float3 binormal : BINORMAL0,
															float3 tangent   : TANGENT0)
{
    Simple_VSOut simpleOut = SimpleMeshVS(input, WorldMatrix, ViewMatrix);

   	NormalMap_VSOut output;
   	
    // calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors.  the pixel shader will normalize these
    // in case the world matrix has scaling.
    output.tangentToWorld[0] = mul(tangent,    WorldMatrix);
    output.tangentToWorld[1] = mul(binormal,    WorldMatrix);
    output.tangentToWorld[2] = simpleOut.WorldNormal;

	output.WVPPosition = simpleOut.WVPPosition;
	output.TexCoord = simpleOut.TexCoord;
	output.WorldViewPos = simpleOut.WorldViewPos;

	// Save the vertices postion in world space
	output.WorldPos = simpleOut.WorldPos;  //mul(input.Position, World);

	return output;
}

float4 SimplePS2(float3 worldN, 
							float3 WorldPos,
							float4 TextureColor,
							float reflectIntensity, 
							float3 ObjectSpecularColor, 
							float specularPower,
							float3 LightDir,   //L是light direction
							float2 TexCoord)
{
	//TextureColor = lerp(BlendColor, TextureColor, TextureColor.a * ObjectAlpha);//先改顏色
	//TextureColor.rgb = BlendColor.rgb * BlendColor.a + TextureColor.rgb * TextureColor.a;//先改顏色

	float3 rV = normalize(ViewPosition - WorldPos);
	float3 RV = reflect(-rV, worldN);//改用視野觀點去算折射
	
	// Approximate a Fresnel coefficient for the environment map.
	// This makes the surface less reflective when you are looking
	// straight at it, and more reflective when it is viewed edge-on.
	float fFacing  = 1.0 - max( dot( rV, worldN ), 0 );
	float fresnelTerm =// fFresnelBias + ( 1.0 - fFresnelBias ) * 
																							  pow( fFacing, FresnelPower);
    //float   fresnelTerm = dot(RV, worldN);

	//float3 L = LightDirection;//normalize(LightDirection);
	//float3 R = normalize(L - 2 * dot(N, L) * worldN );          // reflection vector
	//float3 R  = reflect(L, worldN);
	//float3 R = L*-1;//光的折射
	
	TextureColor.rgb = CalculateEnvCubeMap(fresnelTerm, TextureColor.rgb, RV, reflectIntensity);
	TextureColor.rgb = CalculateReflectColor(ProjectionMatrix, TextureColor.rgb, WorldPos, worldN,  fresnelTerm, reflectIntensity);

	float emmPower = 0;//CalculateEmmisive(TextureColor.rgb, TexCoord);

	float3 origColor = TextureColor.rgb;
   
	TextureColor.rgb = ConsiderLights(TextureColor.rgb, worldN, WorldPos, ObjectSpecularColor, specularPower, 
	ViewPosition,
	RV, LightDir);

	//emmRatio越1表示越沒自發光，越相信與光計算後的顏色。
	TextureColor.rgb = lerp(origColor, TextureColor.rgb, emmPower);
	
    return TextureColor;
} 

float4 NormalMapPS(NormalMap_VSOut input, float4 TextureColor)
{
	float fogFactor = ComputeFogFactor(abs(input.WorldViewPos.z));
	if(LightEnable)
	{
		// look up the normal from the normal map, and transform from tangent space
		// into world space using the matrix created above.  normalize the result
		// in case the matrix contains scaling.
		float3 normalFromMap = tex2D(NormalMapSampler, input.TexCoord);
		normalFromMap = mul(normalFromMap, input.tangentToWorld);
		normalFromMap = normalize(normalFromMap);

		float reflectPower = m_ReflectIntensity;	
		if(bReflectPowerMap)
			reflectPower *= tex2D(ReflectIntensityMapSampler, input.TexCoord).r;		

		//TextureColor = SimplePS2(normalFromMap, input.WorldPos, TextureColor, reflectPower, 
		//							ConsiderObjectSpecularColor(input.TexCoord), m_SpecularPower, LightDirection, input.TexCoord);
									
									
		TextureColor = DiffuseWithShadowMap(input.WorldPos, TextureColor, ViewPosition );
	}
	TextureColor.rgb = lerp( TextureColor.rgb, FogColor, fogFactor);
	TextureColor.a *= ObjectAlpha;
    return TextureColor;
}


// Determines the depth of the pixel for the model and checks to see 
// if it is in shadow or not
float4 SimpleMeshPS(Simple_PSIn input, float4 TextureColor)
{
    float fogFactor = ComputeFogFactor(abs(input.WorldViewPos.z));
	if(LightEnable)
	{
		//float reflectIntensity = m_ReflectIntensity;
		//if(bReflectPowerMap)
		//	reflectIntensity *= tex2D(ReflectIntensityMapSampler, input.TexCoord).r;		
	//	float reflectIntensity = GetReflectIntensity(input.TexCoord);
		
	//	TextureColor = SimplePS2(input.WorldNormal, input.WorldPos, TextureColor, reflectIntensity, 
	//							ConsiderObjectSpecularColor(input.TexCoord), m_SpecularPower, LightDirection, input.TexCoord);
		TextureColor = DiffuseWithShadowMap(input.WorldPos, TextureColor, ViewPosition);
	}
	TextureColor.rgb = lerp( TextureColor.rgb, FogColor, fogFactor);
	TextureColor.a *=  ObjectAlpha;
	return TextureColor;
}

float4 SimpleMeshPS(Simple_PSIn input): COLOR
{
	float4 objectDiffuseColor = GetNowTextureColor(input.TexCoord);
	return SimpleMeshPS(input, objectDiffuseColor);
}

// Transforms the model into light space an renders out the depth of the object
DepthMap_VSOut CreateDepthMap_VertexShader(Simple_VSIn input)
{
    return CreateDepthMapVS(input, WorldMatrix);
}

// Saves the depth value out to the 32bit floating point texture
float4 CreateDepthMap_PixelShader(DepthMap_VSOut input) : COLOR
{ 
    float4 diffuseColor = GetNowTextureColor(input.TexCoord);
    return float4(input.Depth, 0, 0, diffuseColor.a);
}

float4 ShadowVolumeVS_LightVector (float3 Pos : POSITION,  float3 Normal : NORMAL) : POSITION
{
    float4 Out = 0;

    // light to vertex ray
    float3 dir = -ShadowVolumeLightDirection;
    float3 normal = mul(Normal, WorldMatrix);
    float4 pos = mul(float4(Pos, 1), WorldMatrix);
    
    // dot ray and normal
    float LN = dot(dir, normal);

    // scale is 0 for light facing or 1 for non-light facing
    float scale = (LN >= 0) ? 0.0f : 10000.0f;

    // reduced position - scale * light direction
    pos.xyz -= scale * dir;
    Out = mul(mul(pos, ViewMatrix), ProjectionMatrix);

    return Out;
}


