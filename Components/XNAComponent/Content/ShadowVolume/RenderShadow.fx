float4x4 World;
float4x4 View;
float4x4 Projection;

struct VS_OUTPUT
{
	float4 Pos :		POSITION;
};

VS_OUTPUT VS_RenderShadow(float4 Pos : Position)
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	float4x4 WorldViewProj = mul(mul(World, View), Projection);
	Out.Pos = mul(Pos, WorldViewProj);
	return Out;  
}

float4 PS_RenderShadow() : COLOR
{
	float4 color = { 0.2f, 0.2f, 0.2f, 0.6f };
	return color;
}

technique RenderShadow
{
    pass P0
    {
        VertexShader = compile vs_1_1 VS_RenderShadow();
        PixelShader  = compile ps_1_1 PS_RenderShadow();
        
        ShadeMode = Gouraud;
        ColorWriteEnable = 15;			//15 = 0x0f for enabling color write，還原frame buffer顏色的更新
    }
}

