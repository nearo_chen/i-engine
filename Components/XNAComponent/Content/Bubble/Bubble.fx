
float4x4 WorldViewProjection;
float4x4 World;

float time;


float3 cameraPos;
float4 waveHeight = {0.35, 0.35, 0.055, 0.055};
float4 waveSpeed = {0.6, 0.7, 1.2, 1.4};
float4 waveDirX  = {0.0, 2.0, 0.0, 4.0};
float4 waveDirY  = {2.0, 0.0, 4.0, 0.0};

texture BaseTexture;

sampler2D baseMap = sampler_state
{
	Texture = <BaseTexture>;
    ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
	MIPFILTER = LINEAR;
};

texture glowTex; 

samplerCUBE cubeMap = sampler_state
{
	Texture = <glowTex>;
	ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MipFilter = LINEAR;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
};


struct VS_INPUT 
{
   float4 Position	: POSITION0;
   float4 Normal	: NORMAL0;
   float2 TexCoord	: TEXCOORD;
   float4 Tangent	: TANGENT0;
   float4 Binormal	: BINORMAL0;
};

struct VS_OUTPUT 
{
   float4 Position		: POSITION;
   float2 TexCoord		: TEXCOORD0;
   float3 ReflectionVec	: TEXCOORD1;
   float4 NdotV			: TEXCOORD2;
};

VS_OUTPUT VertexShaderFunction(VS_INPUT input)
{
	VS_OUTPUT Out;
	
	float4 waveVec = waveDirX * input.TexCoord.x + waveDirY * input.TexCoord.y;

	waveVec += time * waveSpeed;
	waveVec = frac(waveVec) - 0.5;
	waveVec *= 6.2831853071795864769252867665590;

	float3 deformedPos = input.Normal * dot(sin(waveVec), waveHeight) * 15.0;
	deformedPos = input.Position + deformedPos;

	Out.Position = mul(float4(deformedPos, 1), WorldViewProjection);
	Out.TexCoord = input.TexCoord;
	
	float4 cosWaveHeight = cos(waveVec) * waveHeight * 0.2;
	float warpedBinormal = dot(cosWaveHeight, waveDirX);
	float warpedTangent  = dot(cosWaveHeight, waveDirY);
	float3 normalWarp = input.Binormal * warpedBinormal + input.Tangent * warpedTangent;
	float3 deformedNormal = input.Normal + normalWarp;
	deformedNormal = normalize(mul(deformedNormal, World));

	float3 viewVec = normalize(cameraPos - mul(float4(deformedPos, 1), World));

	Out.NdotV = dot(deformedNormal, viewVec);
	Out.ReflectionVec = 2 * Out.NdotV * deformedNormal - viewVec;  
	
	return Out;
}


float4 PixelShaderFunction(VS_OUTPUT input ) : COLOR
{
	float4 modulatedColor;
	float4 finalColor;

	float4 baseColor = tex2D(baseMap, input.TexCoord);
	float4 cubeMapColor = texCUBE(cubeMap, input.ReflectionVec);
	
	modulatedColor.rgb = saturate(2 * baseColor * cubeMapColor);
	modulatedColor.a = (1-input.NdotV)*0.5 - 0.01;		
	float opacity = saturate(4*(cubeMapColor.a*cubeMapColor.a - 0.75));

	finalColor.rgb = lerp(modulatedColor, cubeMapColor, opacity);
	finalColor.a = modulatedColor.a + opacity;
	
	return finalColor;
}


technique Bubble
{
    pass Pass1
    {
		//AlphaBlendEnable = true;
        VertexShader = compile vs_1_1 VertexShaderFunction();
        PixelShader = compile ps_1_1 PixelShaderFunction();
        //AlphaBlendEnable = false;
    }
}
