// Effect uses a scrolling displacement texture to offset the position of the main
// texture. Depending on the contents of the displacement texture, this can give a
// wide range of refraction, rippling, warping, and swirling type effects.

float DistortMapRange=0.1f;
float m_xScale=0.1f;
float m_yScale=0.1f;
float2 DisplacementScroll;

sampler TextureSampler : register(s0);
sampler DisplacementSampler : register(s1)  = sampler_state 
{
   	MinFilter = Linear;
   	MagFilter = Linear;
   	MipFilter = Linear;   
   	AddressU  = wrap;
   	AddressV  = wrap;
};



float4 main(float4 color : COLOR0, float2 texCoord : TEXCOORD0) : COLOR0
{
    // Look up the displacement amount.
    float2 displacement = tex2D(DisplacementSampler, DisplacementScroll + texCoord * DistortMapRange);
    
    // Offset the main texture coordinates.
    texCoord += (displacement-0.5f) * float2(m_xScale, m_yScale);
    
  
    // Look up into the main texture.
    return tex2D(TextureSampler, texCoord) * color;
}


technique Refraction
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 main();
    }
}
