
sampler baseTexture : register(s0);

Texture sceneTexture;

sampler2D SceneTextureSampler = sampler_state
{
	Texture = <sceneTexture>;
    ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
	MIPFILTER = LINEAR;
};

float4x4 View;
float4x4 WorldViewProjection;

half Density = 0.8f;
half Weight = 0.9f;
half Decay = 0.5f;
half Exposition = 0.5f;

half3 viewPosition;
half3 viewDirection;
half3 LightPosition;
//half3 LightDirection = {1.0f,1.0f,1.0f};

int numSamples;

half4 PixelShaderFunction(float2 texCoord : TEXCOORD0) : COLOR0
{
    half3 LightDirection = normalize( viewPosition    - LightPosition);
    half4 screenPos = mul(LightPosition, WorldViewProjection); 
    half2 ssPos = screenPos.xy / screenPos.w 
			* float2(0.5,-0.5) + 0.5;
			
	half2 oriTexCoord = texCoord;
			
	half2 deltaTexCoord = (texCoord - ssPos);
    
    deltaTexCoord *= 1.0f / numSamples * Density;
    
    half3 color = tex2D(baseTexture, texCoord);
    
    half illuminationDecay = 1.0f;
    
	for (int i=0; i < numSamples; i++)
	{
		texCoord -= deltaTexCoord;
			
		half3 sample = tex2D(baseTexture, texCoord);
		sample *= illuminationDecay * Weight;
		color += sample;
			
		illuminationDecay *= Decay;
	}
	
	half amount = dot(viewDirection, LightDirection * -1);
	//half amount = dot(mul(LightDirection,View), half3(0.0f,0.0f,1.f)); //越直視陽光越亮
	half4 sampleFrame = tex2D(SceneTextureSampler, oriTexCoord);
    
    return saturate(amount) * half4(color * Exposition, 1) + sampleFrame;
    //return saturate(amount-0.5) * half4(color * Exposition, 1) + sampleFrame;
    //saturate(amount-0.5)是讓他在45度範圍內直視陽光才變亮，不然90度一接近就開始亮很怪。
}


technique Scatter
{
    pass Pass1
    {
		PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}