﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;
using MeshDataDefine;


namespace I_XNAComponent
{
    public class KeyFrameMesh : AnimationPlayerManager
    {
        //AnimationPlayerManager m_AnimationPlayerManager;

        Model m_Model;

        //int m_iBoneAmount;
        KeyframeMeshData m_KeyFrameMeshData = null;
        //AnimationPlayer[] m_AnimationControllers = null; //animation controller for each animation...
        //AnimationPlayer m_ActiveAnimationColtroller = null; //now actived animation....
        //Matrix[] m_KeyFrameAnimatingMatrixss = null;//used for animating bone transform calculate, on time....
        //int m_iAnimationAmount;
     //   Matrix[] absolute=null;

        public Model GetModel { get { return m_Model; } }

        public KeyFrameMesh()
        {
        }

        //public AnimationPlayer GetActiveAnimation
        //{
        //    get { return m_ActiveAnimationColtroller; }
        //}

        override public void Dispose()
        {
            base.Dispose();

            m_Model = null;
            m_KeyFrameMeshData = null;

            
            //m_AnimationPlayerManager.Dispose();
            //m_AnimationPlayerManager = null;
            //m_KeyFrameAnimatingMatrixss = null;
            //m_ActiveAnimationColtroller = null;

            //for(int i=0 ; i<m_AnimationControllers.Length ; i++)
            //    m_AnimationControllers[i] = null;
            //m_AnimationControllers = null;
        }

        public void LoadContent(string modelname, ContentManager content, string animationXML)
        {
            LoadContent(Utility.ContentLoad_Model(content, modelname), content, animationXML);
        }

        public AnimationTableDataArray LoadContent(Model model, ContentManager content, string animationXML)
        {
            m_Model = model;
            if (m_Model == null)
                return null;

            // Get the dictionary
            Dictionary<string, object> modelTag = (Dictionary<string, object>)m_Model.Tag;
            if (modelTag == null)
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage("This is not a valid animated keyframe model. ==> " + animationXML);
                return null;
            }

            // Get the KeyFrameMesh from the dictionary
            if (modelTag.ContainsKey(MeshKeys.KeyFrameMeshKey))
                m_KeyFrameMeshData = (KeyframeMeshData)modelTag[MeshKeys.KeyFrameMeshKey];
            else
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage("This is not a valid keyframe model. ==> " + animationXML);
                return null;
            }

            //m_AnimationPlayerManager = new AnimationPlayerManager();
            //AnimationTableDataArray ooo = m_AnimationPlayerManager.LoadAnimationXML(m_KeyFrameMeshData, animationXML);
            AnimationTableDataArray ooo = LoadAnimationXML(m_KeyFrameMeshData, animationXML);
            return ooo;
            ////Create Amimation controller for each animation....
            //if (AnimationDataList == null)
            //{
            //    m_iAnimationAmount = m_KeyFrameMeshData.Animations.Length;

            //    //int endKey = 0;
            //    //foreach (BoneKeyframe bk in m_KeyFrameMeshData.Animations[0].BoneKeyframes)
            //    //{
            //    //    if (endKey < bk.Keyframes.Length)
            //    //        endKey = bk.Keyframes.Length;
            //    //}

            //    m_AnimationControllers = new AnimationPlayer[m_iAnimationAmount];
            //    for (int i = 0; i < m_AnimationControllers.Length; i++)
            //    {
            //        m_AnimationControllers[i] = new AnimationPlayer(
            //            m_KeyFrameMeshData.Animations[0],
            //            (uint)m_KeyFrameMeshData.Animations[0].Duration.TotalMilliseconds,
            //            //0,
            //            //endKey,
            //            m_KeyFrameMeshData.Animations[i].Name, true);
            //    }
            //}
            //else
            //{
            //    m_iAnimationAmount = AnimationDataList.Length;
            //    m_AnimationControllers = new AnimationPlayer[m_iAnimationAmount];
            //    for (int i = 0; i < m_AnimationControllers.Length; i++)
            //    {
            //        m_AnimationControllers[i] = new AnimationPlayer(
            //            m_KeyFrameMeshData.Animations[0], //都固定是Animations[0]，因為沒法很多Animations在一個檔案裡，幻動作只能用跳ket frame的方法。
            //            AnimationDataList[i].StartKey,
            //            AnimationDataList[i].EndKey,
            //            AnimationDataList[i].ActionName, true);
            //    }
            //}

            ////m_KeyFrameAnimatingMatrixss = new Matrix[m_Model.Bones.Count];
            //m_KeyFrameAnimatingMatrixss = Utility.GetMatrixArray(m_Model.Bones.Count);
            //m_ActiveAnimationColtroller = m_AnimationControllers[0];
            //SetAnimationPlay(0, false, 1, 0);
        }

        //public bool IfAnimationPlay(int animation)
        //{
        //    return m_AnimationPlayerManager.IfAnimationPlay(animation);
        //}

        //public void SetAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent)
        //{
        //    m_AnimationPlayerManager.SetAnimationPlay(iAnimationID, bLoop, speed, startPercent);
        //    //AllAnimationStop();

        //    //m_ActiveAnimationColtroller = m_AnimationControllers[iAnimationID];
        //    //m_ActiveAnimationColtroller.AnimationPlay(bLoop, speed, startPercent); 
        //}

        //public void SetAnimationStop(int iAnimationID)
        //{
        //    m_AnimationPlayerManager.SetAnimationStop(iAnimationID);
        //    //m_AnimationControllers[iAnimationID].AnimationStop();
        //}

        //public void AnimatedWithRatio(int iAnimationID, float ratio)
        //{
        //    m_AnimationPlayerManager.AnimatedWithRatio(iAnimationID, ratio);
        //    //m_ActiveAnimationColtroller = m_AnimationControllers[iAnimationID];
        //    //m_ActiveAnimationColtroller.AnimatedWithRatio(ratio, true);
        //}

        //public void AllAnimationStop()
        //{
        //    m_AnimationPlayerManager.AllAnimationStop();
        //    //m_ActiveAnimationColtroller = null;
        //    //foreach (AnimationPlayer ap in m_AnimationControllers)
        //    //    ap.AnimationStop();
        //}

        //public void UpdateKeyFrameTransform(int ellapsedMilliseond, ref Matrix worldTransform)
        //{
        //    m_AnimationPlayerManager.UpdateAnimateKeyFrameTransform(ellapsedMilliseond);
        //    //m_AnimationPlayerManager.UpdateKeyFrameTransform(ellapsedMilliseond, ref worldTransform);

        //    //m_ActiveAnimationColtroller.AnimatedWithTime(ellapsedMilliseond);

        //    //m_KeyFrameAnimatingMatrixss[0] = m_ActiveAnimationColtroller.AnimatingBones[0] * worldTransform;
        //    //for (int boneID = 1; boneID < m_KeyFrameAnimatingMatrixss.Length; boneID++)
        //    //{
        //    //    Matrix.Multiply(ref m_ActiveAnimationColtroller.AnimatingBones[boneID], 
        //    //        ref m_KeyFrameAnimatingMatrixss[m_KeyFrameMeshData.BonesParent[boneID]], 
        //    //        out m_KeyFrameAnimatingMatrixss[boneID]);
        //    //}
        //}

        //public void Render(Matrix world, Matrix view, Matrix projection, SceneNode node)
        //{
        //    if (m_Model == null)
        //        return;

        //    UpdateKeyFrameTransform(GetTime.EllapsedMilliseonds(), world);

        //    foreach (ModelMesh mesh in m_Model.Meshes)
        //    {
        //        foreach (Effect effect in mesh.Effects)
        //        {
        //            effect.CurrentTechnique = effect.Techniques["SimpleTransformAndTexture"];
        //            node.SetCommonEffectPar2(effect, ref m_KeyFrameAnimatingMatrixss[mesh.ParentBone.Index], null);
        //        }
        //        mesh.Draw();
        //    }
        //}


        //public void RenderWithShadow(Matrix world, Matrix view, Matrix projection, ShadowNode node)
        //{
        //    if (m_Model == null)
        //        return;

        //    UpdateKeyFrameTransform(GetTime.EllapsedMilliseonds(), world);

        //    // Render the skinned mesh.
        //    foreach (ModelMesh mesh in m_Model.Meshes)
        //    {
        //        //CopyAbsoluteBoneTransformsTo
        //        //這個是bone的parent world transform，一定要加在world裡面乘上去，
        //        //跟bind pose不一樣，bind pose也是pose，是key frame的一格，
        //        //通常是沒啥作用，bind pose可以不儲存
        //        foreach (Effect effect in mesh.Effects)
        //        {
        //            // Set the currest values for the effect
        //            effect.CurrentTechnique = effect.Techniques["SimpleDrawWithShadowMap"];
        //            node.SetCommonEffectPar2(effect, ref m_KeyFrameAnimatingMatrixss[mesh.ParentBone.Index], null);
        //            node.SetShadowEffectPar(effect, null);
        //        }
        //        mesh.Draw();
        //    }
        //}

        //public void CreateShadowMap(Matrix world, Matrix view, Matrix projection, Matrix lightViewProjection, ShadowNode node)
        //{
        //    if (m_Model == null)
        //        return;

        //    UpdateKeyFrameTransform(GetTime.EllapsedMilliseonds(), world);

        //    // Loop over meshs in the model
        //    foreach (ModelMesh mesh in m_Model.Meshes)
        //    {
        //        // Loop over effects in the mesh
        //        foreach (Effect effect in mesh.Effects)
        //        {
        //            // Set the currest values for the effect
        //            effect.CurrentTechnique = effect.Techniques["SimpleCreateShadowMap"];
        //            node.SetCreateShadowPar(effect, ref m_KeyFrameAnimatingMatrixss[mesh.ParentBone.Index], ref lightViewProjection);        
        //        }
        //        // Draw the mesh
        //        mesh.Draw();
        //    }
        //}

        //public Matrix GetBoneAnimateMatrix(int boneID)
        //{
        //    return m_AnimationPlayerManager.GetBoneAnimateLocalMatrix(boneID);
        //}
    }
    





}
