﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using ParticleEngine;
using I_XNAUtility;

namespace I_XNAComponent
{
    public class I_ParticleManager : IDisposable
    {
        protected class MOPParticle
        {
            public MOPParticle(){}
            public List<MultiObjectPlayer> MOPList; //每個mop各有一個定位點
            public ParticleSystemWithEmitter ParticleSystem; //MOPList共用的particle system
        }

        //以同一種性質的particle，分類所有的multi object player。
        Dictionary<string, MOPParticle> m_MOPParticleData = new Dictionary<string, MOPParticle>(32);

        const int TmpParticleNumber = 1024;
        ParticleLocationData[] m_TempLocationData = new ParticleLocationData[TmpParticleNumber];

        //Dictionary<string, ParticleSystemWithEmitter>m_ParticleSystemList = new Dictionary<string,ParticleSystemWithEmitter>();

        public static I_ParticleManager Create()
        {
            I_ParticleManager iii = new I_ParticleManager();
            ReleaseCheck.AddCheck(iii);
            return iii;
        }

       private I_ParticleManager()
        {
        }

        ~I_ParticleManager()
        {
            ReleaseCheck.DisposeCheckCount(this);
        }

        public void Dispose()
        {
            ReleaseCheck.DisposeCheck(this);

            Dictionary<string, MOPParticle>.Enumerator itr = m_MOPParticleData.GetEnumerator();
            while (itr.MoveNext())
            {
                itr.Current.Value.MOPList.Clear();
                itr.Current.Value.ParticleSystem.Dispose();
            }
            itr.Dispose();

            m_MOPParticleData.Clear();
            m_MOPParticleData = null;

            //Dictionary<string, ParticleSystemWithEmitter>.Enumerator itr = m_ParticleSystemList.GetEnumerator();
            //while (itr.MoveNext())
            //    itr.Current.Value.Dispose();

            //m_ParticleSystemList.Clear();
            //m_ParticleSystemList = null;
        }

        /// <summary>
        /// 將mop加入manager分類。
        /// </summary>      
        public void AddMOPParticle(MultiObjectPlayer mop)
        {
            if (string.IsNullOrEmpty(mop.ParticleFileName))
                return;

            MOPParticle mopparticle = null;
            if (m_MOPParticleData.TryGetValue(mop.ParticleFileName, out mopparticle) == false)
            {
                mopparticle = new MOPParticle();
                mopparticle.MOPList = new List<MultiObjectPlayer>();
                m_MOPParticleData.Add(mop.ParticleFileName, mopparticle);
            }
            mopparticle.MOPList.Add(mop);
        }

        public void UpdateParticleSystem(ContentManager content, GraphicsDevice device, Matrix rootNodeMat, Matrix invRootNodeMat)
        {
            //每個particle system跑一次
            Dictionary<string, MOPParticle>.Enumerator itr = m_MOPParticleData.GetEnumerator();
            while (itr.MoveNext())
            {
                MOPParticle mopParticle = itr.Current.Value;

                #region 擷取目前要產生的particle位置，填入m_TempLocationData。
                int index = 0;
                int LocationCount = 0;
                List<MultiObjectPlayer>.Enumerator mopitr = mopParticle.MOPList.GetEnumerator();
                while (mopitr.MoveNext())
                {
                    //檢查此mop是否要噴發particle
                    FrameNode node = mopitr.Current.IfParticleNeedPlay();
                    if (node != null && mopParticle.ParticleSystem != null)
                    {
                        m_TempLocationData[LocationCount].emitterIndex = index;

                        if (mopParticle.ParticleSystem.m_Settings.particleSettings.UseLocalWorld)
                            m_TempLocationData[LocationCount].WorldMatrix = mopitr.Current.GetParticleMatrix * invRootNodeMat;
                        else
                            m_TempLocationData[LocationCount].WorldMatrix = mopitr.Current.GetParticleMatrix;

                        LocationCount++;

                        if (LocationCount >= TmpParticleNumber)
                            OutputBox.ShowMessage("Particle Manager m_TempLocationData : LocationCount >= " + TmpParticleNumber.ToString());
                    }
                    index++;
                }
                mopitr.Dispose();
                #endregion

                #region 檢查particle system是否有初始化
                if (mopParticle.ParticleSystem == null)
                {
                    //擷取初始資訊
                    ParticleLocationData [] locationData = new ParticleLocationData[mopParticle.MOPList.Count];
                    List<MultiObjectPlayer>.Enumerator tmpitr = mopParticle.MOPList.GetEnumerator();
                    int tmpcount = 0;
                    while (tmpitr.MoveNext())
                    {
                        locationData[tmpcount].WorldMatrix = tmpitr.Current.GetParticleMatrix;
                        tmpcount++;
                    }
                    tmpitr.Dispose();

                    mopParticle.ParticleSystem = new ParticleSystemWithEmitter(content, device, Utility.GetFullPath(itr.Current.Key), locationData);
                }
                #endregion

                #region 更新m_TempLocationData給system
                if(mopParticle.ParticleSystem.m_Settings.particleSettings.UseLocalWorld)
                    mopParticle.ParticleSystem.UpdateLocalLocationData(m_TempLocationData, rootNodeMat, LocationCount);
                else
                    mopParticle.ParticleSystem.UpdateWorldLocationData(m_TempLocationData, LocationCount);
                mopParticle.ParticleSystem.Update(I_GetTime.ellipseUpdateSecond);
                #endregion
            }
            itr.Dispose();
        }

        public void Render(ref Matrix view, ref Matrix proj, ref Vector3 viewPosition)
        { 
            Dictionary<string, MOPParticle>.Enumerator itr = m_MOPParticleData.GetEnumerator();
            while (itr.MoveNext())
            {
                MOPParticle mopParticle = itr.Current.Value;

                if(mopParticle.ParticleSystem != null)
                    mopParticle.ParticleSystem.Draw(ref view, ref proj, ref viewPosition);
            }
            itr.Dispose();
        }
    }
}
