﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace I_XNAComponent
{
    /// <summary>
    /// 此class會產生一個renderTarget，專門用來呼叫XNA底層基本的繪圖方式，如spriteBatch、
    /// mesh.Draw()...等，將畫出的結果都產生在RT上，並提供GetTexture()屬性回傳RT的圖。
    /// </summary>
    public class Render2DToTarget : IDisposable
    {
        public string Name;
        static SpriteBatch StaticSpriteBatch;
        static int referenceCount;

        public delegate void RenderThings(SpriteBatch spriteBatch, string OPString, string[] splitOPString);
        RenderThings m_RenderThings;
        RenderTarget2D m_RT;

        public Render2DToTarget(string Name, int sizeW, int sizeH, GraphicsDevice device, RenderThings renderThings)
        {
            m_RenderThings = renderThings;
            if (System.Threading.Interlocked.Increment(ref referenceCount) == 1)
            {
                StaticSpriteBatch = new SpriteBatch(device);
            }

            m_RT = new RenderTarget2D(device, sizeW, sizeH, 1, SurfaceFormat.Dxt5,
                MultiSampleType.None, 0,
                RenderTargetUsage.PreserveContents);
        }

        public void Render(GraphicsDevice device, string OPString)
        {
            device.SetRenderTarget(0, m_RT);
            device.Clear(Color.TransparentBlack);

            string[] splitOPString = OPString.Split(' ');
            m_RenderThings(StaticSpriteBatch, OPString, splitOPString);          
        }

        public void Dispose()
        {
            if (System.Threading.Interlocked.Decrement(ref referenceCount) == 0)
            {
                StaticSpriteBatch.Dispose();
                StaticSpriteBatch = null;
            }

            m_RT.Dispose();
            m_RT = null;
        }

        public Texture2D GetTexture()
        {
            return m_RT.GetTexture();
        }
    }
}
