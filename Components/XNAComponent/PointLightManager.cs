﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

using I_XNAUtility;
using System.Threading;

namespace I_XNAComponent
{
    public class PointLightNode : LocatorNode
    {      
        public Color LightColor;
        public float Strength;
        public float Decay;         //衰簡直
        public float RangeValue;//光的範圍
        public float SpecularIntensity;//光的specular強度直接乘上的倍率

        public static PointLightNode Create()
        {
            PointLightNode nnn = new PointLightNode();
            ReleaseCheck.AddCheck(nnn);
            return nnn;
        }

        PointLightNode()
        {
        }

        public PointLight GetPointLight()
        {
            PointLight PL;
            PL.ParentNodeName = null;
            if (Parent != null)
                PL.ParentNodeName = Parent.NodeName;
            PL.Name = NodeName;
            PL.postionX = WorldPosition.X;
            PL.postionY = WorldPosition.Y;
            PL.postionZ = WorldPosition.Z;

            PL.colorR = LightColor.R;
            PL.colorG = LightColor.G;
            PL.colorB = LightColor.B;

            PL.Decay = Decay;
            PL.Strength = Strength;
            PL.rangeValue = RangeValue;
            PL.SpecularIntensity = SpecularIntensity;
            return PL;
        }

        public void SetPointLightValu(PointLight pointLight)
        {
            NodeName = pointLight.Name;

            Matrix mmm = Matrix.Identity;
            mmm.Translation = new Vector3(
                pointLight.postionX,
                pointLight.postionY,
                pointLight.postionZ);
            WorldMatrix = mmm;

            LightColor = new Color((float)pointLight.colorR / 255f,
         (float)pointLight.colorG / 255f,
          (float)pointLight.colorB / 255f);

            Strength = pointLight.Strength;
            Decay = pointLight.Decay;
            RangeValue = pointLight.rangeValue;
            SpecularIntensity = pointLight.SpecularIntensity;
        }
    }

    public class SpotLightNode : LocatorNode
    {
        public SpotLightData Data;
        //public float angleTheta;//SPOT的內圈
        //public float anglePhi;//SPOT的外圈
        //public float strength;//計算後光的強度在加強的倍數
        //public float decay;//SPOT的衰減強度，DECAY不能為0，不然在pow()時，0次方會變成1數字會錯
        //public Color colorValue;
        //public float range;

        public static SpotLightNode Create()
        {
            SpotLightNode nnn = new SpotLightNode();
            ReleaseCheck.AddCheck(nnn);
            return nnn;
        }

        SpotLightNode()
        {            
        }

        public SpotLight GetSpotLight()
        {            
            SpotLight SPL;
            SPL.ParentNodeName = null;
            if (Parent != null)
                SPL.ParentNodeName = Parent.NodeName;
            SPL.Name = NodeName;
            SPL.position = WorldPosition;
            SPL.direction = WorldForward;
            SPL.Data = Data;
            return SPL;
        }

        public void SetSpotLightValue(SpotLight spotLight)
        {
            NodeName = spotLight.Name;

            Matrix mmm;
            MyMath.MatrixSetDirection(spotLight.position, spotLight.direction, WorldUp, out mmm);
            WorldMatrix = mmm;

            Data = spotLight.Data;
        }

        /// <summary>
        /// 取得spot light轉換為Vector3的值
        /// </summary>
        /// <returns></returns>
        public Vector3 ColorVector3()
        {
            return new Vector3((float)Data.colorR / 255f,
                (float)Data.colorG / 255f,
                (float)Data.colorB / 255f);
        }
    }

    //public class PointLightManager : IDisposable
    //{
    //    ///// <summary>
    //    ///// 存檔
    //    ///// </summary>
    //    //public static void SavePointLightListToXML(string fileName, PointLightNode [] pointLightList)
    //    //{
    //    //    PointLights pointLightArray = new PointLights();
    //    //    pointLightArray.Amount = pointLightList.Length;
    //    //    pointLightArray.PointLight = new PointLight[pointLightArray.Amount];
    //    //    for(int a=0 ; a<pointLightArray.Amount ; a++)
    //    //    {
    //    //        pointLightArray.PointLight[a] = pointLightList[a].GetPointLight();           
    //    //    }

    //    //    Utility.SaveXMLFile(fileName, pointLightArray);            
    //    //}

    //    ///// <summary>
    //    ///// 由locatorlist中轉換成point light list node
    //    ///// </summary>    
    //    //public static LocatorNode LoadPointLightListFromXML(string fileName)
    //    //{
    //    //    PointLights pointLightArray = Utility.LoadXMLFile(fileName, typeof(PointLights), false) as PointLights;
    //    //    if (pointLightArray == null)
    //    //        return null;
    //    //    LocatorNode PointLightRootNode = LocatorNode.Create();
    //    //    PointLightRootNode.NodeName = "PointLight_RootNode";
    //    //    for (int a = 0; a < pointLightArray.Amount; a++)
    //    //    {
    //    //        PointLightNode pointLightNode = new PointLightNode();
    //    //        pointLightNode.SetPointLightValu(pointLightArray.PointLight[a]);

    //    //        PointLightRootNode.AttachChild(pointLightNode);
    //    //        pointLightNode.NodeName = "PointLight_" + a.ToString();
    //    //    }
    //    //    return PointLightRootNode;
    //    //}

    //    /// <summary>
    //   /// 讀取point light list node檔
    //   /// </summary>    
    //    public static LocatorNode LoadPointLightListFromLocatorList(SpecialMeshDataManager.LocatorList locatorList)
    //    {
    //        LocatorNode PointLightRootNode = LocatorNode.Create();
    //        PointLightRootNode.NodeName = "PointLight_RootNode";
    //        for (int a = 0; a < locatorList.GlobalPoses.Length; a++)
    //        {
    //            Vector3 vvv = locatorList.GlobalPoses[a].translation;
    //            PointLight plight = PointLight.Default(vvv);//從locator就給default值
    //            PointLightNode pointLightNode = new PointLightNode();
    //            pointLightNode.SetPointLightValu(plight);

    //            PointLightRootNode.AttachChild(pointLightNode);
    //            //pointLightNode.NodeName = "PointLight_" + a.ToString();
    //        }
    //        return PointLightRootNode;
    //    }

    //    NodeArray m_NodeArray = null;
    //    PointLightNode[] m_PointLightArray = null;
    //    public PointLightNode[] GetPointLightList { get { return m_PointLightArray; } }

    //    //Thread m_RefreshPointLightThread = null;
    //    public PointLightManager()
    //    {
    //        // Start a new physics simulation.
    //        //ThreadStart threadStarter = delegate { RefreshNearestPLight_InThread(); };
    //        //m_RefreshPointLightThread = new Thread(threadStarter)
    //        //{
    //        //    Name = "Refresh Nearest Point Light Thread",
    //        //    Priority = ThreadPriority.Lowest,
    //        //};
    //        //m_RefreshPointLightThread.Start();
    //    }

    //    //更新最近的點光源給node array裡面所有的node。
    //    //void RefreshNearestPLight_InThread()
    //    //{
    //    //    while (true)
    //    //    {
    //    //        lock (this)
    //    //        {
    //    //            Cauculate(m_NodeArray, m_PointLightArray);
    //    //        }

    //    //        Thread.Sleep(30);
    //    //    }
    //    //}

    //    ///// <summary>
    //    ///// 計算靜態的點光源與node的關係，會馬上將結果計算出來，適合放在初始化位置就好。
    //    ///// </summary>
    //    //public static void Cauculate(NodeArray nodeArray, PointLightNode[] pointLightArray)
    //    //{
    //    //    if (nodeArray != null && pointLightArray != null)
    //    //    {
    //    //        //這邊計算每個node所屬的point light
    //    //        for (int a = 0; a < nodeArray.nowNodeAmount; a++)
    //    //        {
    //    //            SceneNode frameNode = nodeArray.nodeArray[a] as SceneNode;
    //    //            Vector3 pos = frameNode.WorldPosition;
    //    //            int lightAmount;

    //    //            //RefreshNeighborPointLight_InRange(pointLightArray, ref pos,
    //    //            //    frameNode.PointLightList, out lightAmount);//填入frameNode.PointLightList資料

    //    //            RefreshNeighborLight(pointLightArray, ref pos,
    //    //                frameNode.PointLightList, out lightAmount);//填入frameNode.PointLightList資料

    //    //            frameNode.PointLightAmount = lightAmount;//告知frameNode.PointLightList有效資料大小
    //    //        }
    //    //    }
    //    //}

    //    public void Dispose()
    //    {
    //        //m_RefreshPointLightThread.Abort();
    //        //m_RefreshPointLightThread = null;
    //        //SetStaticPointLightList(null, null);
    //    }

    //    ///// <summary>
    //    ///// 計算靜態的點光源與node的關係，會馬上將結果計算出來，適合放在初始化位置就好。
    //    ///// </summary>
    //    //public void RefreshStaticPointLightList(NodeArray nodeArray, PointLightNode[] pointLightArray)
    //    //{
    //    //    m_NodeArray = nodeArray;
    //    //    m_PointLightArray = pointLightArray;
    //    //    Cauculate(nodeArray, pointLightArray); //馬上計算
    //    //}

    //    /// <summary>
    //    /// 傳入要計算點和他最近的光源，要注意node和光源的數量，
    //    /// 應為複雜度 = node數量 x 點光數量。 
    //    /// </summary>      
    //    //public void SetPointLightList(NodeArray nodeArray, PointLightNode[] pointLightArray)
    //    //{
    //    //    m_NodeArray = nodeArray;
    //    //    m_PointLightArray = pointLightArray;          
    //    //}


    ////    #region 計算最接近point light 的list 
    ////    static float[] PointLightDistance = new float[2048];//暫存計算用
    ////    static bool[] bPointLightSorted = new bool[2048];//暫存計算用

    ////    /// <summary>
    ////    /// 針對物件的中心座標找出，在點光源範圍內，
    ////    /// 最接近光源中心的4個點。
    ////    /// 應為SortPointLight是固定大小陣列，
    ////    /// 所以直接將資料填入SortPointLight中，並回傳有效的資料數量。
    ////    /// </summary>   
    ////    static public void RefreshNeighborPointLight_InRange(PointLightNode[] lights, ref Vector3 worldPos,
    ////        PointLightNode[] SortPointLight, out int dataAmount)
    ////    {
    ////        //int lightAmount = lights.Length;
    ////        //if (lights.Length > pointLightBufferSize)
    ////        //    lightAmount = pointLightBufferSize;//Buffer給超大不會有大魚的情況

    ////        //先算此node距離每個點的距離
    ////        int i;
    ////        for (i = 0; i < lights.Length; i++)
    ////        {
    ////            if (lights[i] != null)
    ////            {
    ////                float length = (worldPos - lights[i].WorldPosition).LengthSquared();
    ////                float length2 = (lights[i].RangeValue * lights[i].RangeValue) - length;
    ////                //距離儲存為距離點光源邊界的距離，在圓外的過濾掉
    ////                //之後取得距離最大的點光源，代表距離元的中心較近。
    ////                PointLightDistance[i] = length2;
    ////            }
    ////        }

    ////        Array.Clear(bPointLightSorted, 0, bPointLightSorted.Length);

    ////        //找出可以傳入shader內的ShaderMaxPointLight個點。
    ////        dataAmount = 0;
    ////        for (i = 0; i < SceneNode.ShaderMaxPointLight; i++)
    ////        {
    ////            if (i >= lights.Length)
    ////                break;

    ////            //從每次的bPointLightSorted 為false中找出最大值。
    ////            float maxLength = float.MinValue;
    ////            int maxLindex = -1;
    ////            for (int a = 0; a < lights.Length; a++)
    ////            {
    ////                if (bPointLightSorted[a] || PointLightDistance[a] <0)//在圓外的過濾掉
    ////                    continue;

    ////                if (PointLightDistance[a] > maxLength)
    ////                {
    ////                    maxLength = PointLightDistance[a];
    ////                    maxLindex = a;
    ////                }
    ////            }

    ////            if (maxLindex >= 0)
    ////            {
    ////                bPointLightSorted[maxLindex] = true;

    ////                //填入資料               
    ////                SortPointLight[i] = lights[maxLindex];
    ////                dataAmount++;
    ////            }
    ////        }

    ////        return;
    ////    }

    ////    /// <summary>
    ////    /// 針對物件的座標找出最接近的4個點
    ////    /// </summary>
    ////    /// <param name="lights"></param>
    ////    static public void RefreshNeighborLight(PointLightNode[] lights, ref Vector3 worldPos,
    ////        PointLightNode[] SortPointLight, out int dataAmount)
    ////    {
    ////        //先算此node距離每個點的距離
    ////        int i;
    ////        for (i = 0; i < lights.Length; i++)
    ////        {
    ////            float length = (worldPos - lights[i].WorldPosition).LengthSquared();            
    ////            PointLightDistance[i] = length;
    ////        }

    ////        Array.Clear(bPointLightSorted, 0, bPointLightSorted.Length);

    ////        //找出可以傳入shader內的ShaderMaxPointLight個點。
    ////        dataAmount = 0;
    ////        for (i = 0; i < SceneNode.ShaderMaxPointLight; i++)
    ////        {
    ////            if (i >= lights.Length)
    ////                break;

    ////            //從每次的bPointLightSorted 為false中找出最大值。
    ////            float minLength = float.MaxValue;
    ////            int minLindex = -1;
    ////            for (int a = 0; a < lights.Length; a++)
    ////            {
    ////                if (bPointLightSorted[a])
    ////                    continue;

    ////                if (PointLightDistance[a] < minLength)
    ////                {
    ////                    minLength = PointLightDistance[a];
    ////                    minLindex = a;
    ////                }
    ////            }

    ////            bPointLightSorted[minLindex] = true;
    ////            SortPointLight[i] = lights[minLindex];
    ////            dataAmount++;             
    ////        }
    ////        return;
    ////    }
    ////    #endregion
    //}
}
