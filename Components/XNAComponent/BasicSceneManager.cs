﻿
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Audio;

using System.IO;
using I_XNAUtility;

namespace I_XNAComponent
{
    public abstract class BasicSceneManager : IDisposable
    {
        protected object LockObject = new object();

        protected abstract void LoadContent(ContentManager content, GraphicsDevice device, SpriteBatch spriteBatch);
        protected abstract void UnLoadContent();


        protected abstract void KeyboardMouseUpdate(MyKeyboard MyKB, MyMouse MyMS, ArcBall ArcB);
        protected abstract void UpdateScene(MySound mySound);
        bool m_bRunPreRender = true;
        protected abstract void RenderScene(ref uint RenderTriangles);

        //區分render前段用的前處理
        protected abstract void PreRenderScene(ref uint RenderTriangles);
        //protected abstract void RenderSceneAfter(ref uint RenderTriangles);

        //protected Game m_Game;
        protected ContentManager m_Content = null;
        //static protected ContentManager m_NotReleaseContent; //遊戲啟動到結束一直不會清空的content
        protected GraphicsDevice m_GraphicDevice;

        protected MySound m_MySound = null;
        MyKeyboard m_MyKB = null;
        MyMouse m_MyMouse;
        ArcBall m_Arcball;

        //bool m_bChaseCameraLock = false;//避免網路連線時將chase camera黏在車上，但車子沒更新座標，而camera持續計算造成畫面跳動，所以在車子沒更新座標時把他lock起來。

        ChaseCamera m_ChaseCamera = null;
        FrameNode m_ChaseCameraNode = null;
        Camera m_Camera;
        FrameNode m_ControlNode;
        bool m_bCameraCoord;
        float m_MouseWheelSpeed;

        //protected Matrix InverseViewMatrix; //3D世界中的view
        //protected Matrix ViewMatrix; //要傳入顯卡中的view
        //protected Matrix ProjectionMatrix;

        protected Matrix ViewFrustrum;

        protected Vector3 ViewForwardXZ;
        //Matrix OldViewMatrix;
        //Matrix OldProjectionMatrix;
        //Matrix? EffectProjection = null;
        //Matrix? EffectView = null;
        //Matrix? EffectInverseView = null;

        
        bool m_bLockUpdate = false;
        public void LockUpdate(bool bbb) { m_bLockUpdate = bbb; }
        virtual public void Update()
        {
            I_Utility.GameUpdateSetting();

            lock (LockObject)
            {
                //Keyboard--------------------------
                m_MyKB.Update();
                m_MyMouse.Update();

                //if (m_MyKB.IsKeyPress(Keys.Escape))
                //{
                //    if(m_Game != null)
                //        m_Game.Exit();
                //}

                KeyboardMouseUpdate(m_MyKB, m_MyMouse, m_Arcball);

                if (m_bLockUpdate == false)
                    UpdateScene(m_MySound);

                //if (m_MySound != null)
                //    m_MySound.UpdateCamera(ref InverseViewMatrix);
            }
        }

        //protected void SetViewProjection(ref Matrix view, ref Matrix projection)
        //{
        //    //OldViewMatrix = ViewMatrix;
        //    //OldProjectionMatrix = ProjectionMatrix;

        //    //ViewMatrix = view;
        //    //ProjectionMatrix = projection;
        //    //Matrix.Multiply(ref view, ref projection, out ViewProjectionMatrix);
        //    //Matrix.Invert(ref ViewMatrix, out InverseViewMatrix);

        //    //ViewForwardXZ = new Vector3(InverseViewMatrix.Forward.X, 0, InverseViewMatrix.Forward.Z);
        //    //ViewForwardXZ.Normalize();

        //    InverseViewMatrix = view;
        //    ProjectionMatrix = projection;

        //    Matrix.Invert(ref view, out ViewMatrix);
        //    Matrix.Multiply(ref ViewMatrix, ref ProjectionMatrix, out ViewFrustrum);

        //    ViewForwardXZ = new Vector3(view.Forward.X, 0, view.Forward.Z);
        //    ViewForwardXZ.Normalize();
        //}

      

        bool m_bLockRender = false;
        protected void LockRender(bool bbb) { m_bLockRender = bbb; }
        public uint RenderTriangles;
        public uint RenderNodesDeferred;
        public uint RenderNodesOld;
        public uint RenderRefractNodes;
        virtual public void Render()
        {
            I_Utility.GameUpdateRenderSetting();

            lock (LockObject)
            {
                RenderTriangles = 0;
                RenderNodesDeferred = 0;
                RenderNodesOld = 0;
                RenderRefractNodes = 0;
                if (m_bRunPreRender)
                {
                    PreRenderScene(ref RenderTriangles);
                }

                if (m_bLockRender == false)
                {
                    RenderScene(ref RenderTriangles);
                }
                m_bRunPreRender = true;
            }
        }

        virtual public void Initialize(IServiceProvider iServiceProvider, GraphicsDevice device, MySound sound)
        {
            m_MySound = sound;
            m_GraphicDevice = device;
            //m_Game = game;
            m_Content = new ContentManager(iServiceProvider);
            m_Camera = new Camera();

            if (Utility.IfContentOutSide)
                m_Content.RootDirectory = "";//Utility.WorkingDirectory + "XNBData\\Content";
            else
                m_Content.RootDirectory = "Content";

            m_MyKB = new MyKeyboard();
            m_MyMouse = new MyMouse();
            m_Arcball = new ArcBall();
        }

        virtual public void Dispose()
        {
            m_MyKB.Dispose();
            m_MyKB = null;

            m_Content.Unload();
            m_Content.Dispose();
            m_Content = null;

            m_Camera.Dispose();
            m_Camera = null;
            m_ChaseCamera = null;
            m_ChaseCameraNode = null;

            m_ControlNode = null;
        }

        static public void SetCullNone(GraphicsDevice device)
        {
            device.RenderState.CullMode = CullMode.None;
        }
        static public void SetCullCCW(GraphicsDevice device)
        {
            device.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
        }

        //------------------------------------------------------------------------------------------------------------
        static public void SetCommonRenderState(GraphicsDevice device)
        {
            device.RenderState.CullMode = CullMode.CullCounterClockwiseFace;

            device.RenderState.DepthBufferEnable = true;
            device.RenderState.DepthBufferWriteEnable = true;
            device.RenderState.AlphaBlendEnable = false;
            device.RenderState.AlphaTestEnable = false;//使用Alphareference所以不用alpha test了    
            //device.RenderState.AlphaFunction = CompareFunction.Greater;
            //device.RenderState.ReferenceAlpha = 128;
        }
        //--------------------------------------------------------------------------------------------------------------
        static public void SetAlphaBlendRenderState(GraphicsDevice device)
        {
            device.RenderState.CullMode = CullMode.CullCounterClockwiseFace;

            device.RenderState.DepthBufferEnable = true;
            device.RenderState.DepthBufferWriteEnable = false;
            device.RenderState.AlphaBlendEnable = true;
            device.RenderState.BlendFunction = BlendFunction.Add;
            device.RenderState.SourceBlend = Blend.SourceAlpha;
            device.RenderState.DestinationBlend = Blend.InverseSourceAlpha;

            device.RenderState.AlphaTestEnable = false;
            //m_GraphicDevice.RenderState.AlphaFunction = CompareFunction.Greater;
            //m_GraphicDevice.RenderState.ReferenceAlpha = 128;
        }

        static public void SetDepthBufferWriteOff(GraphicsDevice device)
        {
            device.RenderState.DepthBufferWriteEnable = false;
        }

        static public void SetDepthBufferWriteOn(GraphicsDevice device)
        {
            device.RenderState.DepthBufferWriteEnable = true;
        }

        #region Camera及其他基本操作
        protected void ChaseCameraInit(Vector3 desiredPositionOffset, Vector3 lookAtOffset)
        {
            if (m_ChaseCamera == null)
            {
                m_ChaseCamera = new ChaseCamera();
                // Set the camera offsets
                m_ChaseCamera.DesiredPositionOffset = desiredPositionOffset;
                m_ChaseCamera.LookAtOffset = lookAtOffset;
                m_ChaseCamera.Reset();
            }
        }

        protected void ChaseCameraDesiredPosition(Vector3 desiredPositionOffset)
        {
            m_ChaseCamera.DesiredPositionOffset = desiredPositionOffset;
        }

        protected void ChaseCameraLookAtOffset(Vector3 lookAtOffset)
        {
            m_ChaseCamera.LookAtOffset = lookAtOffset;
        }

        Vector3? chaseUp;
        protected void ChaseCameraSetNode(FrameNode node, float? stiffness, float? maxLength, Vector3? up)
        {
            m_ChaseCameraNode = node;

            if (stiffness.HasValue)
                m_ChaseCamera.Stiffness = stiffness.Value;

            if (maxLength.HasValue)
                m_ChaseCamera.MaxLength = maxLength.Value;

            if (up.HasValue)
                chaseUp = up.Value;
        }

        //protected void ChaseCameraLock(bool bLock)
        //{
        //    m_bChaseCameraLock = bLock;
        //}

        void UpdateChaseCamera(double ElapsedTotalSeconds)
        {
            //if (m_bChaseCameraLock)
            //    return;

            Matrix carMat = m_ChaseCameraNode.WorldMatrix;
            m_ChaseCamera.ChasePosition = carMat.Translation;
            m_ChaseCamera.ChaseDirection = carMat.Forward;

            if (chaseUp.HasValue)
                m_ChaseCamera.Up = chaseUp.Value;
            else
                m_ChaseCamera.Up = Vector3.Up; // carMat.Up; //Vector3.Up;

            m_ChaseCamera.Update(ElapsedTotalSeconds);
            //viewMat = m_ChaseCamera.GetViewMatrix();
        }

        protected void BasicCameraSet(Vector3 cameraPos, Vector3 cameraLookAt)
        {
            Matrix mmm = Matrix.Identity;
            //MyMath.SetMatrixPositionElement(ref mmm, ref cameraPos);
            MyMath.MatrixSetDirection(cameraPos, cameraLookAt - cameraPos, Vector3.Up, out mmm);

            m_Camera.LocalMatrix = mmm;
        }

        /// <summary>
        /// 設定camera姿態的matrix，沒inverse過的。
        /// </summary>
        public void BasicCameraMatrixSet(Matrix mat)
        {
            m_Camera.LocalMatrix = mat;
        }

        public bool LockCamera = false;
        Matrix cameraMat = Matrix.Identity;
        protected Matrix BasicCameraMatrixGet(double ElapsedTotalSeconds, float mouseWheelSpeed, ref Matrix invView)
        {
            m_MouseWheelSpeed = mouseWheelSpeed;

            //if (m_UserSetMouseWheelSpeed.HasValue)//一frame內，使用自訂wheel speed
            //{
            //    m_MouseWheelSpeed = m_UserSetMouseWheelSpeed.Value;
            //    m_UserSetMouseWheelSpeed = null;
            //}


            if (LockCamera)
                return cameraMat;

            if (m_ChaseCameraNode != null)
            {
                UpdateChaseCamera(ElapsedTotalSeconds);
                cameraMat = m_ChaseCamera.GetViewMatrix();
            }
            else if (m_CameraRotateCentorNode != null)
            {
                UpdateControlMatrixCentorNode();
                cameraMat = m_Camera.GetViewMatrix();
            }
            else
            {
                UpdateControlMatrix(ref invView);
                cameraMat = m_Camera.GetViewMatrix();
            }
            return cameraMat;
        }

        /// <summary>
        /// 給定wheel speed數字，MouseWheelSpeed != 0才能對物件做操作
        /// bCameraCoord 控制是否要以camera的使用者方位做物件操作。
        /// </summary>  
        //protected void BasicSetPickNode(FrameNode pNodeControl, float MouseWheelSpeed, bool bCameraCoord)
        //{
        //    m_ControlNode = pNodeControl;
        //    m_bCameraCoord = bCameraCoord;
        //    m_MouseWheelSpeed = MouseWheelSpeed;
        //}

        ///// <summary>
        ///// 給定wheel speed才能對物件做操作
        ///// </summary>  
        //void BasicSetMouseWheelSpeed(float speed)
        //{
        //    m_MouseWheelSpeed = speed;
        //}
        protected bool LRChange = false;

        void UpdateControlMatrix(ref Matrix InverseViewMatrix)
        {
            float wheel = m_MyMouse.IsMouseWheel();
            if (wheel != 0)
            {
                m_Camera.TranslateZ(m_MouseWheelSpeed * wheel / 300f);
            }

            if ((LRChange == true && m_MyMouse.IsLeftButtonzPressed()) ||
                (LRChange == false && m_MyMouse.IsRightButtonPressed()) ||
                m_MyMouse.IsMiddleButtonPressed()
                )
            {
                Vector2 vecMouseMove = m_MyMouse.MouseMoved();
                float elapsedTime = I_GetTime.ellipseUpdateSecond;
                float tranX = 0;
                float tranY = 0;
                if (vecMouseMove.X != 0)
                    tranX = vecMouseMove.X * m_MouseWheelSpeed * elapsedTime;
                if (vecMouseMove.Y != 0)
                    tranY = vecMouseMove.Y * m_MouseWheelSpeed * elapsedTime;

                if (m_MyMouse.IsMiddleButtonPressed())
                {
                    if (m_ControlNode == null)
                        m_Camera.LocalMatrix = Matrix.CreateTranslation(0, 0, tranY * 0.1f) * m_Camera.LocalMatrix;
                    else
                    {
                        if (m_bCameraCoord)
                        {
                            Vector3 pos = m_ControlNode.WorldPosition;
                            m_ControlNode.WorldPosition = pos + InverseViewMatrix.Forward * tranY;
                        }
                        else
                            m_ControlNode.WorldMatrix = Matrix.CreateTranslation(0, 0, tranY) * m_ControlNode.WorldMatrix;
                    }
                }
                else
                {
                    if (m_ControlNode == null)
                        m_Camera.LocalMatrix = Matrix.CreateTranslation(tranX * -1, tranY, 0) * m_Camera.LocalMatrix;
                    else
                    {
                        if (m_bCameraCoord)
                        {
                            Vector3 pos = m_ControlNode.WorldPosition;
                            m_ControlNode.WorldPosition = pos + InverseViewMatrix.Right * tranX + InverseViewMatrix.Up * -tranY;
                        }
                        else
                            m_ControlNode.WorldMatrix = Matrix.CreateTranslation(tranX, tranY, 0) * m_ControlNode.WorldMatrix;
                    }

                    //controlMat = Matrix.CreateTranslation(tranX, 0, 0) * controlMat;
                    //controlMat = Matrix.CreateTranslation(0, tranY, 0) * controlMat;
                }
            }


            if (
                (
                    (LRChange == true && m_MyMouse.IsRightButtonPressed()) ||
                    (LRChange == false && m_MyMouse.IsLeftButtonzPressed())
                )
                && m_MouseWheelSpeed != 0)
            {
                Vector2 vecMouseMove = m_MyMouse.MouseMoved();

                m_Arcball.update((int)I_GetTime.ellipseUpdateMillisecond, vecMouseMove);
                Matrix mat = m_Arcball.GetRotMatrix;

                if (m_ControlNode == null)
                {
                    mat = mat * m_Camera.LocalMatrix;
                    MyMath.MatrixSetDirection(mat.Translation, mat.Forward, Vector3.Up, out mat);
                    m_Camera.LocalMatrix = mat;
                }
                else
                {
                    m_ControlNode.WorldMatrix = mat * m_ControlNode.WorldMatrix;
                }
            }
            else
            {
                m_Arcball.update((int)I_GetTime.ellipseUpdateMillisecond, Vector2.Zero);
            }

            if (m_MyMouse.IsMiddleButtonReleased())
            {
                m_ControlNode = null;
            }
            if (m_MyMouse.IsLeftButtonReleased())
            {
                m_ControlNode = null;
            }
            if (m_MyMouse.IsRightButtonReleased())
            {
                m_ControlNode = null;
            }
        }
        #endregion

        #region CAMERA根據物件旋轉的計算
        //Vector2 m_CameraRotateCentorNewMouse, m_CameraRotateCentorOldMouse;
        FrameNode m_CameraRotateCentorNode = null;
        //  Vector3 m_CameraRotateCentorDirection;
        float m_CameraRotateCentorLength;
        float m_CameraRotateCentorTranX = 0;
        float m_CameraRotateCentorTranY = 0;
        //  float m_CameraRotateCentorHeight;
        //  float m_CameraRotateCentorRadius;
        /// <summary>
        /// 給定一個Node做為Camera旋轉的中心
        /// </summary>
        protected void BasicSetCameraRotateCentorNode(FrameNode node, float centerLength, bool bReset)
        {
            m_CameraRotateCentorNode = node;
            if (node == null)
                return;

            if (bReset)
            {
                m_CameraRotateCentorTranX = 0;
                m_CameraRotateCentorTranY = 0;
                m_CameraRotateCentorLength = 0;
            }

            m_CameraRotateCentorLength = centerLength;
            // m_CameraRotateCentorDirection = m_CameraRotateCentorDirection / m_CameraRotateCentorLength;
        }

        protected void BasicSetCameraRotateCentorNode(FrameNode node)
        {
            m_CameraRotateCentorNode = node;
            if (node == null)
                return;

            Vector3 length = m_Camera.WorldPosition - node.WorldPosition;
            BasicSetCameraRotateCentorNode(node, length.Length(), false);
        }


        void UpdateControlMatrixCentorNode()
        {
            float wheel = m_MyMouse.IsMouseWheel();
            if (wheel != 0)
            {
                m_CameraRotateCentorLength += m_MouseWheelSpeed * 100f / wheel;
            }

            if (
                m_MyMouse.IsMiddleButtonPressed()
                )
            {
                Vector2 vecMouseMove = m_MyMouse.MouseMoved();
                float elapsedTime = I_GetTime.ellipseUpdateMillisecond / 1000.0f;
                //float tranX = 0;
                //float tranY = 0;
                //if (vecMouseMove.X != 0)
                //    tranX = vecMouseMove.X * m_MouseWheelSpeed * elapsedTime;
                if (vecMouseMove.Y != 0)
                    m_CameraRotateCentorLength += vecMouseMove.Y * m_MouseWheelSpeed * elapsedTime;
            }
            else if (
                (LRChange == true && m_MyMouse.IsLeftButtonzPressed()) ||
                (LRChange == false && m_MyMouse.IsRightButtonPressed())
                )
            {
                Vector2 vecMouseMove = m_MyMouse.MouseMoved();
                float elapsedTime = I_GetTime.ellipseUpdateMillisecond / 1000.0f;

                if (vecMouseMove.X != 0)
                    m_CameraRotateCentorTranX += vecMouseMove.X * m_MouseWheelSpeed * elapsedTime;
                if (vecMouseMove.Y != 0)
                    m_CameraRotateCentorTranY += vecMouseMove.Y * m_MouseWheelSpeed * elapsedTime;

                //m_Camera.LocalMatrix = Matrix.CreateTranslation(tranX * -1, tranY, 0) * m_Camera.LocalMatrix;
            }else  if (
                (
                    (LRChange == true && m_MyMouse.IsRightButtonPressed() ) ||
                    (LRChange == false && m_MyMouse.IsLeftButtonzPressed() )
                )
                && m_MouseWheelSpeed != 0)
            {
                Vector2 vecMouseMove = m_MyMouse.MouseMoved();
                m_Arcball.update((int)I_GetTime.ellipseUpdateMillisecond, vecMouseMove);
            }
            else
            {
                m_Arcball.update((int)I_GetTime.ellipseUpdateMillisecond, Vector2.Zero);
            }

            Matrix cameraWorld = Matrix.CreateTranslation(Vector3.UnitZ * m_CameraRotateCentorLength) * m_Arcball.GetRotMatrix;
            cameraWorld = cameraWorld * m_Camera.WorldmatrixR;
            MyMath.MatrixSetDirection(cameraWorld.Translation, cameraWorld.Forward, Vector3.Up, out cameraWorld);

            m_Camera.LocalMatrix =
                Matrix.CreateTranslation(m_CameraRotateCentorTranX * -1, m_CameraRotateCentorTranY, 0) * cameraWorld;
        }


        public void LoclRotationOneFrame()
        {
            m_Arcball.LockRotationOneFrame();
        }
        public void UserSetMouseMove(int x, int y)
        {
            m_MyMouse.UserSetMouseMove(x, y);
        }
        public void UserSetLPress()
        {
            m_MyMouse.UserSetLPress();
        }
        public void UserSetRPress()
        {
            m_MyMouse.UserSetRPress();
        }
        public void UserSetMPress()
        {
            m_MyMouse.UserSetMPress();
        }
        #endregion

        //#region 設定撥放沒3D的普通的聲音
        //List<SoundEffect> m_SoundEffectList = new List<SoundEffect>(32) ;
        //List<SoundEffectInstance> m_SoundEffectInstance = new List<SoundEffectInstance>(32);
        //#endregion
    }

}
