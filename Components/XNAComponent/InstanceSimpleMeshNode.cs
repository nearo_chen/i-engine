﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using I_XNAUtility;

namespace I_XNAComponent
{
    public class InstanceSimpleMeshNode : MeshNode
    {
        new static public InstanceSimpleMeshNode Create()
        {
            return ReleaseCheck.CreateNew(typeof(InstanceSimpleMeshNode)) as InstanceSimpleMeshNode;
        }

        MyObjectContainer<Matrix> m_MatrixContainer = null;
        InstanceSimpleMesh m_InstanceSimpleMesh = null;
        FrameNode[] m_ReferenceNodeArray = null;//記錄是否有參考node
        bool m_bStaticInstance;//是否為靜態物件，動態物件的話再render隻錢要從新取得matrix一次。

        public FrameNode[] ReferenceNodeArray { get { return m_ReferenceNodeArray; } }

        /// <summary>
        /// 建立此InstanceSimpleMeshNode的 Instance索引
        /// </summary>
        /// <param name="instanceMesh">要Instance的模型資料</param>
        /// <param name="nodeName">此Node的名稱</param>
        /// <param name="device"></param>
        /// <param name="content"></param>
        /// <param name="instanceNodes">Instance索引參考點，一個SceneNode代表一個instance位置。
        /// 可以先不丟之後再呼叫SetReferenceArray設定</param>
        public void SetInstanceMesh(ModelMesh instanceMesh,//, Matrix absoluteWorld, 
            string nodeName, GraphicsDevice device, ContentManager content,
           FrameNode[] instanceNodes, bool bStaticInstance)//bStaticInstance是否為靜態物件
        {
            m_InstanceSimpleMesh = new InstanceSimpleMesh(instanceMesh, device, content);
            MeshNode.ProcessMeshNodeInfo(nodeName + "_InstanceMesh", Matrix.Identity, instanceMesh, 0, this);

            m_ReferenceNodeArray = instanceNodes;
            m_bStaticInstance = bStaticInstance;

            SetReferenceNodeArray(instanceNodes);
        }

        /// <summary>
        /// 更新ReferenceArray
        /// </summary>
        void SetReferenceNodeArray(FrameNode[] instanceNodes)
        {
            if (m_ReferenceNodeArray == null)
                return;

            if (m_MatrixContainer == null)
                m_MatrixContainer = new MyObjectContainer<Matrix>((uint)instanceNodes.Length);

            if (m_MatrixContainer.GetObjectArray().Length < instanceNodes.Length)
            {
                m_MatrixContainer.Dispose();
                m_MatrixContainer = new MyObjectContainer<Matrix>((uint)instanceNodes.Length);
            }

            RefreshReferenceMatrix();
        }

        void RefreshReferenceMatrix()
        {
            if (m_ReferenceNodeArray == null)
                return;

            m_MatrixContainer.Clear();
            for (int a = 0; a < m_ReferenceNodeArray.Length; a++)
                m_MatrixContainer.AddObject(m_ReferenceNodeArray[a].WorldMatrix);
            m_InstanceSimpleMesh.SetInstanceMatrix(m_MatrixContainer);
        }

        /// <summary>
        /// 更新ReferenceArray，直接傳入外面做好的MyObjectContainer，Matrix陣列
        /// </summary>
        public void SetReferenceMatrixArray(MyObjectContainer<Matrix> matrixArray)
        {
            //Matrix[] arr = (Matrix[])matrixArray.GetObjectArray();
            //Matrix world = WorldMatrix;
            //for (int a = 0; a < matrixArray.NowUsedAmount; a++)
            //    Matrix.Multiply(ref arr[a], ref world, out arr[a]);
            m_InstanceSimpleMesh.SetInstanceMatrix(matrixArray);
        }

        protected override void v_Dispose()
        {
            m_ReferenceNodeArray = null;

            if (m_InstanceSimpleMesh != null)
                m_InstanceSimpleMesh.Dispose();
            m_InstanceSimpleMesh = null;

            if (m_MatrixContainer != null)
                m_MatrixContainer.Dispose();
            m_MatrixContainer = null;
        }

        protected override void v_Render( EffectRenderManager effectRenderManager, GraphicsDevice device)
        {
            if (m_bStaticInstance == false)
            {
                UpdateViewCullingBoxPose(this);
                RefreshReferenceMatrix();
            }

            //effectRenderManager.SetInstanceMeshEffectPar(WorldMatrix);

            m_InstanceSimpleMesh.DrawInstances(device, m_bStaticInstance);

            //#if DrawDebug
            //            //Draw Debug
            //            for (int a = 0; a < m_InstanceSimpleMesh.GetMatrixArray.Length; a++)
            //            {
            //                DrawLine.AddSphereLine(m_InstanceSimpleMesh.GetMatrixArray[a],
            //                    BoundingSphere.Center,
            //                    BoundingSphere.Radius, Color.Aqua, 10, 10);
            //            }
            //#endif
        }

        //protected override bool v_ConsiderCurrentTechniqueOK(RenderTechnique CurrentRenderTechnique)
        //{
        //    return MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.SimpleInstanceMesh);
        //}


        public static void AddInstanceNode(FrameNode nowNode, NodeArray nodeArray)//BasicNode[] nodeArray, ref int nowAmount)
        {
            InstanceSimpleMeshNode node = nowNode as InstanceSimpleMeshNode;
            if (node != null && node.GetType() == typeof(InstanceSimpleMeshNode))
            {
                nodeArray.nodeArray[nodeArray.nowNodeAmount] = node;
                nodeArray.nowNodeAmount++;
            }

            FrameNode c = nowNode.FirstChild as FrameNode;
            while (c != null)
            {
                AddInstanceNode(c, nodeArray);
                c = c.Sibling as FrameNode;
            }
        }

        public Matrix[] GetInstanceMatrixArray()
        {
            return m_InstanceSimpleMesh.GetMatrixArray;
        }




        //給即時想要控制繪出數量用
        MyObjectContainer<Matrix> m_RefreshInstanceMatrix = new MyObjectContainer<Matrix>(256);
        public void RefreshInstanceMatrix_Clear()
        {
            m_RefreshInstanceMatrix.Clear();
        }

        public void RefreshInstanceMatrix_Add(Matrix mat)
        {
            if (m_RefreshInstanceMatrix.NowUsedAmount >= m_RefreshInstanceMatrix.MaxObjectAmount)
            {
                MyObjectContainer<Matrix> tmpMatarray =
                    new MyObjectContainer<Matrix>(m_RefreshInstanceMatrix.MaxObjectAmount + 256);

                Matrix[] destArray = tmpMatarray.GetObjectArray() as Matrix[];
                Matrix[] srcArray = m_RefreshInstanceMatrix.GetObjectArray() as Matrix[];
                Array.Copy(srcArray, destArray, srcArray.Length);
                m_RefreshInstanceMatrix.Dispose();
                m_RefreshInstanceMatrix = tmpMatarray;
            }

            m_RefreshInstanceMatrix.AddObject(mat);
        }

        public void RefreshInstanceMatrix_SetRenderArray()
        {
            SetReferenceMatrixArray(m_RefreshInstanceMatrix);
        }
    }
}
