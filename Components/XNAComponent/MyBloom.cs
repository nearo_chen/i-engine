﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using I_XNAUtility;

namespace I_XNAComponent
{
    public class MyBloom
    {
        Effect bloomExtractEffect;
        Effect bloomCombineEffect;
        Effect gaussianBlurEffect;

        ///// <summary>
        ///// 我新增於此用來利用bloom做高絲模糊計算用
        ///// </summary>
        //Effect bloomCombineDOF;

        RenderTarget2D renderTarget1;
        RenderTarget2D renderTarget2;

        GraphicsDevice m_Device;
        SpriteBatch m_SpriteBatch;

        // Optionally displays one of the intermediate buffers used
        // by the bloom postprocess, so you can see exactly what is
        // being drawn into each rendertarget.
        public enum IntermediateBuffer
        {
            PreBloom,
            BlurredHorizontally,
            BlurredBothWays,
            FinalResult,
        }

        IntermediateBuffer showBuffer = IntermediateBuffer.FinalResult;

        public MyBloom()
        {
        }

        bool bInitialize = false;
        public void Dispose()
        {
            if (bInitialize)
            {
                bloomExtractEffect.Dispose();
                bloomCombineEffect.Dispose();
                gaussianBlurEffect.Dispose();
                renderTarget1.Dispose();
                renderTarget2.Dispose();

                bloomExtractEffect = null;
                bloomCombineEffect = null;
                gaussianBlurEffect = null;
                renderTarget1 = null;
                renderTarget2 = null;

                //bloomCombineDOF.Dispose();
            }
        }

        // Choose what display settings the bloom should use.
        public BloomSettings Settings
        {
            get { return settings; }
            set { settings = value; }
        }

        BloomSettings settings = BloomSettings.PresetSettings[0];
        EffectParameter weightsParameter, offsetsParameter;
        int sampleCount;
        float[] sampleWeights;
        Vector2[] sampleOffsets;

        public void LoadContent(GraphicsDevice device, ContentManager content, SpriteBatch spriteBatch)
        {
            m_Device = device;
            m_SpriteBatch = spriteBatch;

            bInitialize = true;

            if (Utility.IfContentOutSide)
            {
                bloomExtractEffect = Utility.ContentLoad_Effect(content,
                    Utility.GetFullPath("XNBData/Content/Bloom\\BloomExtract"));
                bloomCombineEffect = Utility.ContentLoad_Effect(content,
                    Utility.GetFullPath("XNBData/Content/Bloom\\BloomCombine"));
                gaussianBlurEffect = Utility.ContentLoad_Effect(content,
                    Utility.GetFullPath("XNBData/Content/Bloom\\GaussianBlur"));

                //bloomCombineDOF = Utility.ContentLoad_Effect(content,
                //    Utility.GetFullPath("XNBData/Content/Bloom\\BloomCombineDOF"));
            }
            else
            {
                bloomExtractEffect = Utility.ContentLoad_Effect(content, "Bloom\\BloomExtract");
                bloomCombineEffect = Utility.ContentLoad_Effect(content, "Bloom\\BloomCombine");
                gaussianBlurEffect = Utility.ContentLoad_Effect(content, "Bloom\\GaussianBlur");

                //bloomCombineDOF = Utility.ContentLoad_Effect(content, "Bloom\\BloomCombineDOF");
            }

            // Look up the sample weight and offset effect parameters.
            weightsParameter = gaussianBlurEffect.Parameters["SampleWeights"];
            offsetsParameter = gaussianBlurEffect.Parameters["SampleOffsets"];

            sampleCount = weightsParameter.Elements.Count;

            // Create temporary arrays for computing our filter settings.
            sampleWeights = new float[sampleCount];
            sampleOffsets = new Vector2[sampleCount];

            // Look up the resolution and format of our main backbuffer.
            PresentationParameters pp = device.PresentationParameters;

            int width = pp.BackBufferWidth;
            int height = pp.BackBufferHeight;

            SurfaceFormat format = pp.BackBufferFormat;

            // Create two rendertargets for the bloom processing. These are half the
            // size of the backbuffer, in order to minimize fillrate costs. Reducing
            // the resolution in this way doesn't hurt quality, because we are going
            // to be blurring the bloom images in any case.
            width /= 2;
            height /= 2;

            renderTarget1 = new RenderTarget2D(
                device, width, height, 1, format);// pp.MultiSampleType, pp.MultiSampleQuality, RenderTargetUsage.DiscardContents);
            renderTarget2 = new RenderTarget2D(
                device, width, height, 1, format);//, pp.MultiSampleType, pp.MultiSampleQuality, RenderTargetUsage.DiscardContents);
        }

        /// <summary>
        /// 提供外部座高絲模糊使用，第一步為將要模糊的材質填入renderTarget1，第二步再呼叫RenderBothCaussianBlur()，
        /// 看要將renderTarget1模糊幾次，就呼叫幾次，最後呼叫GetTexture_RenderTarget1取回高絲模糊後的材質。
        /// </summary>
        /// <param name="texture"></param>
        public void ImportTextureToRenderTarget1(Texture2D texture)
        {
            m_Device.SetRenderTarget(0, renderTarget1);
            m_SpriteBatch.Begin(SpriteBlendMode.None, SpriteSortMode.Immediate, SaveStateMode.None);
            m_SpriteBatch.Draw(texture, new Rectangle(0, 0, renderTarget1.Width, renderTarget1.Height), Color.White);
            m_SpriteBatch.End();
            m_Device.SetRenderTarget(0, null);
        }

        /// <summary>
        /// 取回高絲模糊後的材質。
        /// </summary>
        /// <returns></returns>
        public Texture2D GetTexture_RenderTarget1()
        {
            m_Device.SetRenderTarget(0, null);
            return renderTarget1.GetTexture();
        }

        /// <summary>
        /// 畫水平與垂直gaussian blur，將renderTarget1畫到renderTarget2水平模糊，
        /// 再由renderTarget2畫回renderTarget1垂直模糊。
        /// </summary>
        public void RenderBothCaussianBlur()
        {
            // Pass 2: draw from rendertarget 1 into rendertarget 2,
            // using a shader to apply a horizontal gaussian blur filter.
            SetBlurEffectParameters(1.0f / (float)renderTarget1.Width, 0);

            DrawFullscreenQuad(renderTarget1.GetTexture(), renderTarget2,
                               gaussianBlurEffect,
                               IntermediateBuffer.BlurredHorizontally);

            // Pass 3: draw from rendertarget 2 back into rendertarget 1,
            // using a shader to apply a vertical gaussian blur filter.
            SetBlurEffectParameters(0, 1.0f / (float)renderTarget1.Height);

            DrawFullscreenQuad(renderTarget2.GetTexture(), renderTarget1,
                               gaussianBlurEffect,
                               IntermediateBuffer.BlurredBothWays);
        }

        /// <summary>
        /// This is where it all happens. Grabs a scene that has already been rendered,
        /// and uses postprocess magic to add a glowing bloom effect over the top of it.
        /// </summary>
        public void Draw(Texture2D SceneTexture, RenderTarget2D m_DestRenderTarget, int bloomType)
        {
            settings = BloomSettings.PresetSettings[bloomType];

            // Resolve the scene into a texture, so we can
            // use it as input data for the bloom processing.
            //GraphicsDevice.ResolveBackBuffer(resolveTarget);

            // Pass 1: draw the scene into rendertarget 1, using a
            // shader that extracts only the brightest parts of the image.
            bloomExtractEffect.Parameters["BloomThreshold"].SetValue(
                Settings.BloomThreshold);

            DrawFullscreenQuad(SceneTexture, renderTarget1,
                               bloomExtractEffect,
                               IntermediateBuffer.PreBloom);

            //來個幾次湖一點
            //   for (int hahaha = 0; hahaha < blurTimes; hahaha++)
            {
                RenderBothCaussianBlur();
                //// Pass 2: draw from rendertarget 1 into rendertarget 2,
                //// using a shader to apply a horizontal gaussian blur filter.
                //SetBlurEffectParameters(1.0f / (float)renderTarget1.Width, 0);

                //DrawFullscreenQuad(renderTarget1.GetTexture(), renderTarget2,
                //                   gaussianBlurEffect,
                //                   IntermediateBuffer.BlurredHorizontally);

                //// Pass 3: draw from rendertarget 2 back into rendertarget 1,
                //// using a shader to apply a vertical gaussian blur filter.
                //SetBlurEffectParameters(0, 1.0f / (float)renderTarget1.Height);

                //DrawFullscreenQuad(renderTarget2.GetTexture(), renderTarget1,
                //                   gaussianBlurEffect,
                //                   IntermediateBuffer.BlurredBothWays);
            }

            // Pass 4: draw both rendertarget 1 and the original scene
            // image back into the main backbuffer, using a shader that
            // combines them to produce the final bloomed result.
            //m_Device.SetRenderTarget(0, null);
            m_Device.SetRenderTarget(0, m_DestRenderTarget);


            EffectParameterCollection parameters = bloomCombineEffect.Parameters;

            parameters["BloomIntensity"].SetValue(Settings.BloomIntensity);
            parameters["BaseIntensity"].SetValue(Settings.BaseIntensity);
            parameters["BloomSaturation"].SetValue(Settings.BloomSaturation);
            parameters["BaseSaturation"].SetValue(Settings.BaseSaturation);

            //parameters["DepthMap"].SetValue(m_DepthMap);

            m_Device.Textures[1] = SceneTexture;

            Viewport viewport = m_Device.Viewport;

            //DrawFullscreenQuad(renderTarget1.GetTexture(),
            //                   viewport.Width, viewport.Height,
            //                   bloomCombineEffect,
            //                   IntermediateBuffer.FinalResult);

            DrawFullscreenQuad(renderTarget1.GetTexture(), m_DestRenderTarget,
                                   bloomCombineEffect,
                                   IntermediateBuffer.FinalResult);

            m_Device.Textures[0] = null;
            m_Device.Textures[1] = null;
        }

        ///// <summary>
        ///// 利用bloom，將SceneTexture做高斯模糊，並根據參數畫於m_DestRenderTarget
        ///// </summary>
        ///// <param name="destRT"></param>
        //public void DrawDOFWithScene(Texture2D SceneTexture, RenderTarget2D m_DestRenderTarget)
        //{
        //    ImportTextureToRenderTarget1(SceneTexture);
        //    RenderBothCaussianBlur();

        //    m_Device.SetRenderTarget(0, m_DestRenderTarget);

        //    bloomCombineDOF.Parameters["BloomIntensity"].SetValue(Settings.BloomIntensity);
        //    bloomCombineDOF.Parameters["BaseIntensity"].SetValue(Settings.BaseIntensity);
        //    bloomCombineDOF.Parameters["BloomSaturation"].SetValue(Settings.BloomSaturation);
        //    bloomCombineDOF.Parameters["BaseSaturation"].SetValue(Settings.BaseSaturation);

        //    //parameters["DepthMap"].SetValue(m_DepthMap);

        //    m_Device.Textures[1] = SceneTexture;

        //    Viewport viewport = m_Device.Viewport;

        //    //DrawFullscreenQuad(renderTarget1.GetTexture(),
        //    //                   viewport.Width, viewport.Height,
        //    //                   bloomCombineEffect,
        //    //                   IntermediateBuffer.FinalResult);

        //    DrawFullscreenQuad(renderTarget1.GetTexture(), m_DestRenderTarget,
        //                           bloomCombineEffect,
        //                           IntermediateBuffer.FinalResult);

        //    m_Device.Textures[0] = null;
        //    m_Device.Textures[1] = null;
        //}

        /// <summary>
        /// Helper for drawing a texture into a rendertarget, using
        /// a custom shader to apply postprocessing effects.
        /// </summary>
        void DrawFullscreenQuad(Texture2D texture, RenderTarget2D renderTarget,
                                Effect effect, IntermediateBuffer currentBuffer)
        {
            int width, height;
            if (renderTarget != null)
            {
                width = renderTarget.Width;
                height = renderTarget.Height;
            }
            else
            {
                width = m_Device.PresentationParameters.BackBufferWidth;
                height = m_Device.PresentationParameters.BackBufferHeight;
            }
            m_Device.SetRenderTarget(0, renderTarget);
            DrawFullscreenQuad(texture,
                               width, height,
                               effect, currentBuffer);
            m_Device.SetRenderTarget(0, null);
        }


        /// <summary>
        /// Helper for drawing a texture into the current rendertarget,
        /// using a custom shader to apply postprocessing effects.
        /// </summary>
        void DrawFullscreenQuad(Texture2D texture, int width, int height,
                                Effect effect, IntermediateBuffer currentBuffer)
        {
            m_SpriteBatch.Begin(SpriteBlendMode.None,
                              SpriteSortMode.Immediate,
                              SaveStateMode.None);

            // Begin the custom effect, if it is currently enabled. If the user
            // has selected one of the show intermediate buffer options, we still
            // draw the quad to make sure the image will end up on the screen,
            // but might need to skip applying the custom pixel shader.
            if (showBuffer >= currentBuffer)
            {
                effect.Begin();
                effect.CurrentTechnique.Passes[0].Begin();
            }

            // Draw the quad.
            m_SpriteBatch.Draw(texture, new Rectangle(0, 0, width, height), Color.White);
            m_SpriteBatch.End();

            // End the custom effect.
            if (showBuffer >= currentBuffer)
            {
                effect.CurrentTechnique.Passes[0].End();
                effect.End();
            }
        }

        /// <summary>
        /// Computes sample weightings and texture coordinate offsets
        /// for one pass of a separable gaussian blur filter.
        /// </summary>
        void SetBlurEffectParameters(float dx, float dy)
        {
            //// Look up the sample weight and offset effect parameters.
            //EffectParameter weightsParameter, offsetsParameter;

            //weightsParameter = gaussianBlurEffect.Parameters["SampleWeights"];
            //offsetsParameter = gaussianBlurEffect.Parameters["SampleOffsets"];

            // Look up how many samples our gaussian blur effect supports.
            //int sampleCount = weightsParameter.Elements.Count;

            //// Create temporary arrays for computing our filter settings.
            //float[] sampleWeights = new float[sampleCount];
            //Vector2[] sampleOffsets = new Vector2[sampleCount];

            // The first sample always has a zero offset.
            sampleWeights[0] = ComputeGaussian(0);
            sampleOffsets[0] = Vector2.Zero;

            // Maintain a sum of all the weighting values.
            float totalWeights = sampleWeights[0];

            // Add pairs of additional sample taps, positioned
            // along a line in both directions from the center.
            for (int i = 0; i < sampleCount / 2; i++)
            {
                // Store weights for the positive and negative taps.
                float weight = ComputeGaussian(i + 1);

                sampleWeights[i * 2 + 1] = weight;
                sampleWeights[i * 2 + 2] = weight;

                totalWeights += weight * 2;

                // To get the maximum amount of blurring from a limited number of
                // pixel shader samples, we take advantage of the bilinear filtering
                // hardware inside the texture fetch unit. If we position our texture
                // coordinates exactly halfway between two texels, the filtering unit
                // will average them for us, giving two samples for the price of one.
                // This allows us to step in units of two texels per sample, rather
                // than just one at a time. The 1.5 offset kicks things off by
                // positioning us nicely in between two texels.
                float sampleOffset = i * 2 + 1.5f;

                /*
                Vector2 delta = new Vector2(dx, dy) * sampleOffset;

                // Store texture coordinate offsets for the positive and negative taps.
                sampleOffsets[i * 2 + 1] = delta;
                sampleOffsets[i * 2 + 2] = -delta;
                 * */

                sampleOffsets[i * 2 + 1].X = dx * sampleOffset;
                sampleOffsets[i * 2 + 1].Y = dy * sampleOffset;

                sampleOffsets[i * 2 + 2].X = dx * sampleOffset * -1;
                sampleOffsets[i * 2 + 2].Y = dy * sampleOffset * -1;
            }

            // Normalize the list of sample weightings, so they will always sum to one.
            for (int i = 0; i < sampleWeights.Length; i++)
            {
                sampleWeights[i] /= totalWeights;
            }

            // Tell the effect about our new filter settings.
            weightsParameter.SetValue(sampleWeights);
            offsetsParameter.SetValue(sampleOffsets);
        }


        /// <summary>
        /// Evaluates a single point on the gaussian falloff curve.
        /// Used for setting up the blur filter weightings.
        /// </summary>
        float ComputeGaussian(float n)
        {
            float theta = Settings.BlurAmount;

            return (float)((1.0 / Math.Sqrt(2 * Math.PI * theta)) *
                           Math.Exp(-(n * n) / (2 * theta * theta)));
        }
    }






    /// <summary>
    /// Class holds all the settings used to tweak the bloom effect.
    /// </summary>
    public class BloomSettings
    {
        #region Fields


        // Name of a preset bloom setting, for display to the user.
        public readonly string Name;


        // Controls how bright a pixel needs to be before it will bloom.
        // Zero makes everything bloom equally, while higher values select
        // only brighter colors. Somewhere between 0.25 and 0.5 is good.
        public readonly float BloomThreshold;


        // Controls how much blurring is applied to the bloom image.
        // The typical range is from 1 up to 10 or so.
        public readonly float BlurAmount;


        // Controls the amount of the bloom and base images that
        // will be mixed into the final scene. Range 0 to 1.
        public readonly float BloomIntensity;
        public readonly float BaseIntensity;


        // Independently control the color saturation of the bloom and
        // base images. Zero is totally desaturated, 1.0 leaves saturation
        // unchanged, while higher values increase the saturation level.
        public readonly float BloomSaturation;
        public readonly float BaseSaturation;


        #endregion


        /// <summary>
        /// Constructs a new bloom settings descriptor.
        /// </summary>
        public BloomSettings(string name, float bloomThreshold, float blurAmount,
                             float bloomIntensity, float baseIntensity,
                             float bloomSaturation, float baseSaturation)
        {
            Name = name;
            BloomThreshold = bloomThreshold; //限制要bloom顏色的門檻值
            BlurAmount = blurAmount;

            BloomSaturation = bloomSaturation; //Gaussian blur後的色彩 0最接近灰階～1最接近bloom後的顏色
            BaseSaturation = baseSaturation;        //原圖色彩 0最接近灰階～1最接近bloom後的顏色

            BloomIntensity = bloomIntensity; //上面顏色saturate後，再乘上濃度
            BaseIntensity = baseIntensity;
        }


        /// <summary>
        /// Table of preset bloom settings, used by the sample program.
        /// </summary>
        public static BloomSettings[] PresetSettings =
        {
            //                                  Name           Thresh  Blur Bloom  Base  BloomSat BaseSat
            new BloomSettings("Default",             0.25f,   4,    1.25f,     1,        1,           1),
            new BloomSettings("Soft",                       0,   3,    1,            1,       1,           1),
            new BloomSettings("Desaturated",       0.5f,   8,    2,            1,       0,           1),
            new BloomSettings("Saturated",         0.25f,   4,    2,            1,       2,           0),
            new BloomSettings("Blurry",                    0,   2,    1,        0.1f,       1,           1),
            new BloomSettings("Subtle",                0.5f,   2,    1,            1,       1,           1),
            new BloomSettings("mybloom",           0.5f,    2,    1.0f,        1,       1,           1),

            new BloomSettings("nnnnnooooo",       0.0f,    4,    2.0f,       1,       1,           1),
        };
    }
}
