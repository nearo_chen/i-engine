﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace I_XNAComponent
{
    public class MyBubble : IDisposable
    {
        Model sphere;
        Effect bubbleEffect;
        Texture2D rainbow;
        TextureCube cube;
        double wave = 0;
        GraphicsDevice m_Device;

        public MyBubble(ContentManager content, GraphicsDevice device)
        {
            m_Device = device;

            bubbleEffect = content.Load<Effect>("Bubble/Bubble");
            sphere = content.Load<Model>("Bubble/sphere");
            cube = content.Load<TextureCube>("Bubble/uffizi_cross");
            rainbow = content.Load<Texture2D>("Bubble/rainbow");
            wave = 0;

            RemapModel(sphere, bubbleEffect);
        }

        public void Dispose()
        {
            if (bubbleEffect != null)
            {
                bubbleEffect.Dispose();
                bubbleEffect = null;
            }

            sphere = null;
            
            if(rainbow!=null)
                rainbow.Dispose();
            rainbow = null;

            if(cube!=null)
                cube.Dispose();
            cube = null;
        }

        public void Update(double ElapsedTotalSeconds)
        {
            wave += ElapsedTotalSeconds;
        }

        public void Draw(ref Matrix world, ref Matrix view, ref Matrix projection, ref Vector3 cameraPos)
        {
            //m_Device.RenderState.AlphaBlendEnable = true;
            //m_Device.RenderState.AlphaBlendOperation = BlendFunction.Add;
            //m_Device.RenderState.SourceBlend = Blend.SourceAlpha;
            //m_Device.RenderState.DestinationBlend = Blend.InverseSourceAlpha;

            foreach (ModelMesh mesh in sphere.Meshes)
            {
                foreach (Effect effect in mesh.Effects)
                {
                    effect.CurrentTechnique = effect.Techniques["Bubble"];
                    effect.Parameters["cameraPos"].SetValue(cameraPos);
                    effect.Parameters["World"].SetValue(world);
                    effect.Parameters["WorldViewProjection"].SetValue(
                        world * view * projection);
                    effect.Parameters["time"].SetValue((float)wave);
                }
                mesh.Draw();
            }

            //m_Device.RenderState.AlphaBlendEnable = false;
        }

        void RemapModel(Model model, Effect effect)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    Effect i_effect = effect.Clone(m_Device);
                    i_effect.Parameters["BaseTexture"].SetValue(rainbow);
                    i_effect.Parameters["glowTex"].SetValue(cube);
                    part.Effect = i_effect;
                }
            }
        }
    }
}
