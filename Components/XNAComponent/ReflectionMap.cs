﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;


namespace I_XNAComponent
{
    public class ReflectionMap : IDisposable
    {
        GraphicsDevice m_Device;
        RenderTarget2D m_ReflectionRT;
        public Texture2D GetReflectionMap { get { return m_ReflectionRT.GetTexture(); } }
    //    public Texture2D GetReflectionMap{};
        public delegate void RenderReflectionHandler(ref Matrix view, ref Matrix projection, ref Matrix invView);

        public ReflectionMap()
        {
        }

        ~ ReflectionMap()
        {
        }

        public void Dispose()
        {
            if (m_ReflectionRT != null)
                m_ReflectionRT.Dispose();
            m_ReflectionRT = null;
        }

        public void Initialize(GraphicsDevice device, ContentManager content, int sizeW, int sizeH)
        {
            m_Device = device;
            m_ReflectionRT = new RenderTarget2D(
                device, sizeW, sizeH, 1, SurfaceFormat.Color, RenderTargetUsage.PreserveContents);
        }

        Matrix m_ReflectionViewMatrix;
        public Matrix ReflectionViewMatrix        {            get { return m_ReflectionViewMatrix; }        }

        Matrix camMatrix, invCamMatrix, invReflectionView;
        public void DrawReflectionMap(ref Matrix cameraInvView, ref Matrix cameraProj, ref Vector3 planeNormal,
            float waterHeight, RenderReflectionHandler render)
        {
            Plane reflectionPlane = new Plane(planeNormal * -1, waterHeight * -1); ;
            //CreatePlane(waterHeight - 0.5f, ref planeNormal, true, out reflectionPlane);

            Matrix.CreateReflection(ref reflectionPlane, out invReflectionView);
            Matrix.Multiply(ref cameraInvView, ref invReflectionView, out invReflectionView);
            invReflectionView.Up *= -1;
            Matrix.Invert(ref invReflectionView, out m_ReflectionViewMatrix);

            Matrix.Multiply(ref m_ReflectionViewMatrix, ref cameraProj, out camMatrix);       
            Matrix.Invert(ref camMatrix, out invCamMatrix);
            Matrix.Transpose(ref invCamMatrix, out invCamMatrix);

            Vector4 planeCoefficients = new Vector4(reflectionPlane.Normal, reflectionPlane.D);
            Vector4.Transform(ref planeCoefficients, ref invCamMatrix, out planeCoefficients);
            Plane refractionClipPlane = new Plane(planeCoefficients);

            m_Device.ClipPlanes[0].Plane = refractionClipPlane;
            m_Device.ClipPlanes[0].IsEnabled = true;

            m_Device.SetRenderTarget(0, m_ReflectionRT);
            m_Device.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.TransparentBlack, 1.0f, 0);

            //RenderScene
            render(ref m_ReflectionViewMatrix, ref cameraProj, ref invReflectionView);

            m_Device.SetRenderTarget(0, null);
         //   reflectionMap = m_ReflectionRT.GetTexture();

            m_Device.ClipPlanes[0].IsEnabled = false;
        }

        //void CreatePlane(float height, ref Vector3 planeNormalDirection, bool clipSide, out Plane newPlane)
        //{
        //    //planeNormalDirection.Normalize();
        //    Vector4 planeCoeffs = new Vector4(planeNormalDirection, height);
        //    if (clipSide)
        //        planeCoeffs *= -1;

        //    newPlane = new Plane(planeCoeffs);
        //}
    }
}
