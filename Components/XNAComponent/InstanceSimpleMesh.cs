﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using I_XNAUtility;

namespace I_XNAComponent
{
    public class InstanceSimpleMesh : IDisposable
    {
        //int oldMatrixLength = 0;
        const int sizeofMatrix = sizeof(float) * 16;
        //Model m_Model = null;
        InstancedModelPart m_InstancedModelPart = null;
        Matrix[] m_MatrixArray = null;
        //FrameNode[] m_LocatorList = null;
        DynamicVertexBuffer m_InstanceDataStream = null;

        public Matrix[] GetMatrixArray { get { return m_MatrixArray; } }

        public InstanceSimpleMesh(ModelMesh mesh0, GraphicsDevice device, ContentManager content)
        {
            m_InstancedModelPart =
                new InstancedModelPart(device, mesh0.MeshParts[0], mesh0.VertexBuffer, mesh0.IndexBuffer);
        }

        public void Dispose()
        {
            //m_Model = null;
            m_InstancedModelPart.Dispose();
            m_InstancedModelPart = null;
            m_MatrixArray = null;

            if (m_InstanceDataStream != null)
                m_InstanceDataStream.Dispose();
            m_InstanceDataStream = null;
        }


        /// <summary>
        /// 使用m_LocatorList設定的方法
        /// </summary>
        //public void SetInstanceLocatorNode(GraphicsDevice device, FrameNode[] nodeArray)
        //{
        //    m_MatrixArray = new Matrix[nodeArray.Length];
        //    m_LocatorList = nodeArray;
        //}

        int renderAmount = 0;
        /// <summary>
        /// 使用matrixArray設定的方法
        /// </summary>
        public void SetInstanceMatrix(MyObjectContainer<Matrix> matrixArray)
        {
            renderAmount = (int)matrixArray.NowUsedAmount;
            if (renderAmount == 0)
                return;

            if (m_MatrixArray == null)
                m_MatrixArray = new Matrix[matrixArray.NowUsedAmount];

            if (m_MatrixArray.Length != matrixArray.NowUsedAmount)
                Array.Resize(ref m_MatrixArray, (int)matrixArray.NowUsedAmount);
                //m_MatrixArray = new Matrix[(int)matrixArray.NowUsedAmount];
                //
                //m_MatrixArray = new Matrix[(int)matrixArray.NowUsedAmount];
                //

            Matrix[] array = matrixArray.GetObjectArray() as Matrix[];
            Array.Copy(array, m_MatrixArray, m_MatrixArray.Length);
        }

        //void RefreshInstanceMatrix()
        //{
        //    if (m_LocatorList == null)
        //        return;
        //    for (int a = 0; a < m_LocatorList.Length; a++)
        //        m_MatrixArray[a] = m_LocatorList[a].WorldMatrix;
        //}

        public void DrawInstances(GraphicsDevice device, bool bStaticInstance)
        {
            if (renderAmount == 0)
                return;
            //if (m_MatrixArray == null || m_MatrixArray.Length == 0)
            //    return;

            //簡查是否要重建m_InstanceDataStream
            int instanceDataSize = sizeofMatrix * m_MatrixArray.Length;
            if (m_InstanceDataStream == null ||
                m_InstanceDataStream.SizeInBytes != instanceDataSize)
            {
                //oldMatrixLength = m_MatrixArray.Length;
                if (m_InstanceDataStream != null)
                    m_InstanceDataStream.Dispose();
            
                m_InstanceDataStream = new DynamicVertexBuffer(device,
                                                                                 instanceDataSize,
                                                                                 BufferUsage.WriteOnly);
            }

            device.VertexDeclaration = m_InstancedModelPart.VertexDeclaration;
            device.Vertices[0].SetSource(m_InstancedModelPart.VertexBuffer, 0, m_InstancedModelPart.VertexStride);
            device.Indices = m_InstancedModelPart.IndexBuffer;

            //RefreshInstanceMatrix();
            DrawHardwareInstancing(
                device, m_InstancedModelPart.VertexCount, m_InstancedModelPart.PrimitiveCount, bStaticInstance);
        }

        private void DrawHardwareInstancing(GraphicsDevice device, int vertexCount, int primitiveCount,
            bool bStaticInstance)//控制是否要使用SetDataOptions.Discard方式畫出，重新生instance記憶體
        {
            if (bStaticInstance)
            {
                m_InstanceDataStream.SetData(m_MatrixArray, 0, m_MatrixArray.Length,
                    SetDataOptions.NoOverwrite);//靜態物件選用NoOverwrite重複使用此記憶體，並且較快。
            }
            else
            {
                m_InstanceDataStream.SetData(m_MatrixArray, 0, m_MatrixArray.Length, SetDataOptions.Discard);//動態物件必須，選用Discard重新生instance記憶體，不然會閃。
            }

            device.Vertices[0].SetFrequencyOfIndexData(m_MatrixArray.Length);

            device.Vertices[1].SetSource(m_InstanceDataStream, 0, sizeofMatrix);
            device.Vertices[1].SetFrequencyOfInstanceData(1);

            device.DrawIndexedPrimitives(PrimitiveType.TriangleList,
                                                       0, 0, vertexCount, 0, primitiveCount);

            // Reset the instancing streams.
            device.Vertices[0].SetSource(null, 0, 0);
            device.Vertices[1].SetSource(null, 0, 0);

            //device.Vertices[0].SetFrequencyOfIndexData(1);
        }
    }


    public class InstancedModelPart : IDisposable
    {
        VertexDeclaration m_NewVertexDeclaration;
        public VertexDeclaration VertexDeclaration { get { return m_NewVertexDeclaration; } }

        private int m_primitiveCount;
        public int PrimitiveCount { get { return m_primitiveCount; } }

        private int m_vertexCount;
        public int VertexCount { get { return m_vertexCount; } }

        private int m_vertexStride;
        public int VertexStride { get { return m_vertexStride; } }

        private VertexBuffer m_vertexBuffer;
        public VertexBuffer VertexBuffer { get { return m_vertexBuffer; } }

        private IndexBuffer m_indexBuffer;
        public IndexBuffer IndexBuffer { get { return m_indexBuffer; } }

        public InstancedModelPart(GraphicsDevice device, ModelMeshPart part, VertexBuffer vertexBuffer, IndexBuffer indexBuffer)
        {
            m_primitiveCount = part.PrimitiveCount;
            m_vertexCount = part.NumVertices;
            m_vertexStride = part.VertexStride;
            m_vertexBuffer = vertexBuffer;
            m_indexBuffer = indexBuffer;

            InitializeHardwareInstancing(device, part);
        }

        private void InitializeHardwareInstancing(GraphicsDevice device, ModelMeshPart part)
        {
            VertexElement[] extraElements = new VertexElement[4];

            short offset = 0;
            byte usageIndex = 1;
            short stream = 1;

            short sizeOfVector4 = sizeof(float) * 4;

            for (int i = 0; i < extraElements.Length; i++)
            {
                extraElements[i] = new VertexElement(stream, offset,
                                                VertexElementFormat.Vector4,
                                                VertexElementMethod.Default,
                                                VertexElementUsage.TextureCoordinate,
                                                usageIndex);
                offset += sizeOfVector4;
                usageIndex++;
            }

            ExtendVertexDeclaration(device, extraElements, part);
        }

        private void ExtendVertexDeclaration(GraphicsDevice device, VertexElement[] extraElements, ModelMeshPart part)
        {
            VertexElement[] originalVertexDeclaration = part.VertexDeclaration.GetVertexElements();

            int length = originalVertexDeclaration.Length + extraElements.Length;

            VertexElement[] elements = new VertexElement[length];

            originalVertexDeclaration.CopyTo(elements, 0);
            extraElements.CopyTo(elements, originalVertexDeclaration.Length);
            m_NewVertexDeclaration = new VertexDeclaration(device, elements);

            //part.VertexDeclaration.Dispose();//dispose掉此模型的VertexDecla就毀了，下次從同一個content讀進來就事dispose掉的
            //part.VertexDeclaration = m_NewVertexDeclaration;
        }

        public void Dispose()
        {
            m_NewVertexDeclaration.Dispose();
        }
    }
}
