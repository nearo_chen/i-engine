﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;
using MeshDataDefine;

namespace I_XNAComponent
{
    /// <summary>
    /// construct a SceneNode tree from a mesh, according to mesh's tree struct...
    /// </summary>
    public class SceneMeshLoader : BasicMeshLoader
    {
        /// <summary>
        /// loader下面的模型數量
        /// </summary>
        protected int m_CountAllNodes = 0;

        public static SceneMeshLoader Create()
        {
           return ReleaseCheck.CreateNew(typeof(SceneMeshLoader)) as SceneMeshLoader;
        }

        protected override void v_Update(int ellapseMillisecond, ContentManager content, GraphicsDevice device)
        {
        }
        

        #region 讀取檔案和設定檔
        Model m_Model;
        public Model GetModel { get { return m_Model; } }
        //NodeArray m_AllMeshNode = null;
        //public NodeArray GetAllMeshNode { get { return m_AllMeshNode; } }
        
        
        static public bool LoadSceneMesh(string modelFile, out SceneMeshLoader meshLoader, ContentManager content, GraphicsDevice device)
        {
            return LoadSceneMesh(Utility.ContentLoad_Model(content, modelFile), out meshLoader, content, device);
        }

        static public bool LoadSceneMesh(Model model, out SceneMeshLoader meshLoader, ContentManager content, GraphicsDevice device)
        {
            //if (meshLoader != null)
            //    meshLoader.Dispose();
            //meshLoader = null;

            SceneMeshLoader tmpMeshLoader = SceneMeshLoader.Create();
            if (tmpMeshLoader.InitScene(model, content, device) == false)
            {
                tmpMeshLoader.Dispose();
                tmpMeshLoader = null;
                meshLoader = tmpMeshLoader;
                return false;
            }

            meshLoader = tmpMeshLoader;
            return true;
        }
        //bool LoadScene(string modelName, ContentManager content, GraphicsDevice device)
        //{
        //    return LoadScene(Utility.ContentLoad_Model(content, modelName), content, device);
        //}
        protected bool InitScene(Model model, ContentManager content, GraphicsDevice device)
        {        
            if (model == null)
                return false;

            m_Model = model;

            //計算整個模型的bound box之類的
            BasicNode childNode = (BasicNode)GetModelBoneData(m_Model, m_Model.Root, device, ref m_CountAllNodes);
            InitBasicMeshLoader(childNode,
                (BoundingSphere)((Dictionary<string, object>)m_Model.Tag)[MeshKeys.BoundingSphereKey],
                (BoundingBox)((Dictionary<string, object>)m_Model.Tag)[MeshKeys.BoundingBoxKey]);

            //抓出所有的mesh Node
            //NodeArray nodeArray = new NodeArray(4096);
            //SceneNode.AddMeshNode(GetRootNode, nodeArray);
            //m_AllMeshNode = new NodeArray(nodeArray.nowNodeAmount);
            //for (int a = 0; a < nodeArray.nowNodeAmount; a++)
            //    m_AllMeshNode.Add(nodeArray.nodeArray[a]);
            //nodeArray.Dispose();
            //nodeArray = null;
            return true;
        }
        #endregion

        public SceneMeshLoader()
        {
            m_Model = null;
            m_RootNode = null;
            //m_AlphaNodeList = new List<SceneNode>();
        }

        ~SceneMeshLoader()
        {
        }

        protected override void v_Dispose()
        {            
            m_Model = null;            
        }

        /// <summary>
        /// Extract hierarchy from root parentBone...and construct scene node tree...
        /// </summary>
        static private object GetModelBoneData(Model model, ModelBone parentBone,
                                                                       GraphicsDevice device, ref int iCountAllNodes)
        {
            BasicNode parentNode = null;
            //照簡單的場景來講，一個BONE會對應一個mesh，也許會在不同階層，但一定會有對應的。
            iCountAllNodes++;

            ModelMesh mesh = null;
            bool bMesh = false;
            if(parentBone.Name !=null )
                bMesh = model.Meshes.TryGetValue(parentBone.Name, out mesh);

            if (bMesh)
            {
                MeshNode meshNode = MeshNode.Create();
                parentNode = meshNode as BasicNode;
                MeshNode.ProcessMeshNodeInfo(parentBone.Name, parentBone.Transform, mesh, 0, meshNode);
            }
            else
            {
                LocatorNode locatorNode = LocatorNode.Create();
                parentNode = locatorNode as BasicNode;
                locatorNode.NodeName = parentBone.Name;
                locatorNode.LocalMatrix = parentBone.Transform;
            }
            
            //return parentNode;

            ModelBoneCollection.Enumerator itr = parentBone.Children.GetEnumerator();
            while(itr.MoveNext())
            {
                object ckildrenNode = 
                    GetModelBoneData(model, itr.Current, device, ref iCountAllNodes);
                parentNode.AttachChild((BasicNode)ckildrenNode);
            }
            itr.Dispose();

            return parentNode;
        }

        public override void RenderDebug(SpriteBatch spriteBatch)
        {
            if (I_Utility.IfDrawDebug == false)
                return;
            
            if (EffectObjectList_GetDepthMapTexture() != null)
            {
                spriteBatch.Begin(SpriteBlendMode.None);
                spriteBatch.Draw(EffectObjectList_GetDepthMapTexture(), new Rectangle(0, 0, 200, 200), Color.White);
                spriteBatch.End();
            }

                //Utility.RenderTexture2D(device, , SpriteBlendMode.AlphaBlend,
                //    Pos2D, range2D);
        }



    }
}
