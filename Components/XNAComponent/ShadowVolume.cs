using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using I_XNAUtility;

namespace I_XNAComponent
{
    public class ShadowVolumeScreenSquare : IDisposable
    {
        VertexPositionColor[] vertices = new VertexPositionColor[4];
        Effect effect;

        VertexDeclaration m_VertexDeclaration = null;

        public ShadowVolumeScreenSquare(GraphicsDevice device, ContentManager content)
        {
           // if (isFullScreen)
                Create(device.DisplayMode.Width, device.DisplayMode.Height, new Color(0, 0, 0, 255));
           // else
          //      Create(device.Viewport.Width, device.Viewport.Height, new Color(0, 0, 0, 255));

            if (Utility.IfContentOutSide)
            {
                effect = content.Load<Effect>(Utility.GetFullPath("XNBData/Content/ShadowVolume/RenderShadow"));
            }
            else
            {
                effect = content.Load<Effect>("ShadowVolume/RenderShadow");
            }
            effect.CurrentTechnique = effect.Techniques["RenderShadow"];
            effect.Parameters["World"].SetValue(Matrix.Identity);  
            effect.Parameters["View"].SetValue(Matrix.CreateLookAt(new Vector3(0.0f, 0.0f, 5.0f), Vector3.Zero, Vector3.Up));
            effect.Parameters["Projection"].SetValue(Matrix.CreateOrthographic(device.Viewport.Width, device.Viewport.Height, 0.0f, 10.0f));

            m_VertexDeclaration = new VertexDeclaration(device, VertexPositionColor.VertexElements);
        }

        public void Dispose()
        {
            m_VertexDeclaration.Dispose();
            m_VertexDeclaration = null;
            vertices = null;
            effect.Dispose();
            effect = null;
        }

        public void Create(int width, int height, Color color)
        {
            float x = (float)width / 2.0f;
            float y = (float)height / 2.0f;

            vertices[0].Position = new Vector3(-x, y, 0.0f);
            vertices[1].Position = new Vector3(x, y, 0.0f);
            vertices[2].Position = new Vector3(-x, -y, 0.0f);
            vertices[3].Position = new Vector3(x, -y, 0.0f);
            vertices[0].Color = vertices[1].Color = vertices[2].Color = vertices[3].Color = color;
        }

        public void Render(GraphicsDevice device)
        {
            device.VertexDeclaration = m_VertexDeclaration;

            //Save current render state
            bool enableZ = device.RenderState.DepthBufferEnable;
            bool enableAlphaBlend = device.RenderState.AlphaBlendEnable;
            Blend sourceBlend = device.RenderState.AlphaSourceBlend;
            Blend destBlend = device.RenderState.AlphaDestinationBlend;
            int stencilRef = device.RenderState.ReferenceStencil;
            CompareFunction stencilFunc = device.RenderState.StencilFunction;
            StencilOperation stencilOp = device.RenderState.StencilPass;

            //Set render state for rendering shadow
            device.RenderState.DepthBufferEnable = false;
            device.RenderState.AlphaBlendEnable = true;
            device.RenderState.AlphaSourceBlend = Blend.SourceAlpha;
            device.RenderState.AlphaDestinationBlend = Blend.InverseSourceAlpha;
            device.RenderState.StencilEnable = true;
            device.RenderState.ReferenceStencil = 1;
            device.RenderState.StencilFunction = CompareFunction.LessEqual;
            device.RenderState.StencilPass = StencilOperation.Keep;
 
            //Render shadow
            effect.Begin(); 
            foreach(EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin(); 
                device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.TriangleStrip, vertices, 0, 2);
                pass.End();
            }
            effect.End(); 

            //Restore render state
            device.RenderState.DepthBufferEnable = enableZ;
            device.RenderState.AlphaBlendEnable = enableAlphaBlend;
            device.RenderState.AlphaSourceBlend = sourceBlend;
            device.RenderState.AlphaDestinationBlend = destBlend;
            device.RenderState.ReferenceStencil = stencilRef;
            device.RenderState.StencilFunction = stencilFunc;
            device.RenderState.StencilPass =stencilOp;

            device.VertexDeclaration = null;
        }
    }

    public struct VertexPositionNormal
    {
        public Vector3 position;
        public Vector3 normal;

        public static int SizeInBytes;
        public static VertexElement[] VertexElements;
    }

    public struct DataPositionNormal
    {
        public Vector3 position;
        public Vector3 normal;
    }
    
    public class ShadowVolume : IDisposable
    { 
        //Effect effect;
        ShadowVolumeScreenSquare m_screenSquare=null;
        public ShadowVolumeScreenSquare ShadowVolumeScreenSquare { get { return m_screenSquare; } }
#if DrawDebug
        static BasicEffect basicEffect;
#endif

        public enum LightInfoType { LightPosition, LightVector };

        static VertexDeclaration m_FaceListVertexDec = null;

        public ShadowVolume(GraphicsDevice device, ContentManager content)
        {
            VertexPositionNormal.SizeInBytes = 6 * 4;
            VertexPositionNormal.VertexElements = new VertexElement[2];
            VertexPositionNormal.VertexElements[0] = new VertexElement(0, 0, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Position, 0);
            VertexPositionNormal.VertexElements[1] = new VertexElement(0, sizeof(float) * 3, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Normal, 0);

            m_screenSquare = new ShadowVolumeScreenSquare(device, content);

            if(m_FaceListVertexDec==null)
                m_FaceListVertexDec = new VertexDeclaration(device, VertexPositionNormal.VertexElements);
        }

        ~ShadowVolume()
        {
            Dispose();
        }
        public void Dispose()
        {
#if DrawDebug
            if (basicEffect != null)
            {
                basicEffect.Dispose();
                basicEffect = null;
            }
#endif
            if (m_FaceListVertexDec != null)
            {
                m_FaceListVertexDec.Dispose();
                m_FaceListVertexDec = null;
            }
            //if (effect != null)
            //{
            //    effect.Dispose();
            //    effect = null;
            //}
            if (m_screenSquare != null)
            {
                m_screenSquare.Dispose();
                m_screenSquare = null;
            }
        }

        static bool CheckFormat(ModelMesh mesh, int partID)
        {
            if (mesh.MeshParts.Count != 1)      //Only a sigle part mesh is valid   
                return false;

            ModelMeshPart part = mesh.MeshParts[partID];
            if (part.PrimitiveCount < 4)
                return false;                   //Can not form a close mesh

            VertexBuffer vb = mesh.VertexBuffer;
            IndexBuffer ib = mesh.IndexBuffer;
            VertexElement[] ve = part.VertexDeclaration.GetVertexElements();


            if (ib.IndexElementSize != IndexElementSize.SixteenBits)
                return false;                                   //Only 16-bit index buffer is supported
            if (ib.SizeInBytes / 2 != part.PrimitiveCount * 3)
                return false;                                   //Only triangle list is supported

            //Check if the vertex format is correct
            //if (ve.Length != 3)
            //    return false;

            if (!(
                ve[0].VertexElementUsage == VertexElementUsage.Position// && 
                //ve[1].VertexElementUsage == VertexElementUsage.Normal           
                ))
                return false;

            return true;
        }

        /// <summary>
        /// 建立shadow volume的face list。
        /// </summary>  
        /// <param name="faceList">最大的faceList陣列，之後根據faceCount使用量將陣列畫出</param>
        /// <param name="faceCount">faceList實際用的到的面數量</param>
        /// <param name="VBReference">faceList對於原本mesh裡的vertex buffer的索引。</param>
        /// <returns></returns>
        public static bool Create(ContentManager content, GraphicsDevice device, ModelMesh mesh, int partID,
            out VertexPositionNormal[] faceList, out int faceCount, bool bVBReference, out  int []  VBReference)
        {      
            faceList = null;
            faceCount = 0;
            VBReference = null;

            if (CheckFormat(mesh, partID) == false)
                return false;

            ModelMeshPart part = mesh.MeshParts[partID];
            VertexBuffer vb = mesh.VertexBuffer;
            IndexBuffer ib = mesh.IndexBuffer;

#if DrawDebug
            basicEffect = new BasicEffect(device, null);
            basicEffect.EnableDefaultLighting();
#endif

            DataPositionNormal[] vbData = new DataPositionNormal[part.NumVertices];
            short[] ibData = new short[ib.SizeInBytes / 2];

            //effect = content.Load<Effect>("ShadowVolume/ShadowVolume");

            //Retrieve vertex position data and compute vertex and face normals 
            faceCount = part.PrimitiveCount * 4; //原本的MESH加上一個三角形可能有3個相鄰邊的list所以最多會4倍
            int vertexCount = faceCount * 3;
            faceList = new VertexPositionNormal[vertexCount];
            Vector3[] faceNormal = new Vector3[part.PrimitiveCount];

            if(bVBReference)
                VBReference = new int[vertexCount];

            List<Vector3> positionList = new List<Vector3>();
         //   List<Vector3> normalList = null;// new List<Vector3>();
            Collision.GetMeshVBIB(mesh, null, positionList, null, null, null, null);
            List<Vector3>.Enumerator posItr = positionList.GetEnumerator();
            //List<Vector3>.Enumerator normalItr = normalList.GetEnumerator();
            for (int a = 0; a < vbData.Length; a++)
            {
                posItr.MoveNext(); 
                //normalItr.MoveNext();
                vbData[a].position = posItr.Current;
                //vbData[a].normal = normalItr.Current;
            }
            posItr.Dispose();
            //normalItr.Dispose();
       //  indexList.Clear(); indexList = null;
            positionList.Clear(); positionList = null;
            //normalList.Clear(); normalList = null;

            //vb.GetData<DataPositionNormal>(vbData);
            ib.GetData<short>(ibData, part.StartIndex, part.PrimitiveCount * 3);

            //把三角形每個面的點抓出來，並計算每個面的normal。
            Vector3 v1, v2;
            for (int i = 0; i < part.PrimitiveCount; ++i)
            {
                int idx = i * 3;
                int ibidx;
                for (int j = 0; j < 3; ++j)
                {
                    ibidx = ibData[idx + j];
                    faceList[idx + j].position = vbData[ibidx].position;
                    VBReference[idx + j] = ibidx;
                }
 
                v1 = faceList[idx + 1].position - faceList[idx].position;
                v2 = faceList[idx + 2].position - faceList[idx].position;
                faceNormal[i] = Vector3.Normalize(Vector3.Cross(v2, v1));
                faceList[idx].normal = faceList[idx + 1].normal = faceList[idx + 2].normal = faceNormal[i];

                
            }
            vbData = null;
            ibData = null;

            //Extrude faces by step throught the three edges of each face 

            int[,] id = new int[2, 2];                          //checklist
            int face = part.PrimitiveCount;

            for (int i = 0; i < part.PrimitiveCount; ++i)//A三角形
            {
                for (int j = i + 1; j < part.PrimitiveCount; ++j)//拿A三角形去跟所有三角形(B)比對，找出有共用邊的。
                {
                    int cnt = 0;
                    for (int k = 0; k < 3 && cnt < 2; ++k)//A三角形的一個點
                    {
                        for (int l = 0; l < 3 && cnt < 2; ++l)//拿A三角形的一個點跟三角形B的所有點比對
                        {
                            //如果點重疊或靠近就當作共用點...找出是否有2共用點就跳出迴圈
                            //把對印的點index存在id[][]裡，

                            if (faceList[3 * i + k].position == faceList[3 * j + l].position)
                            {
                                id[cnt, 0] = 3 * i + k;
                                id[cnt, 1] = 3 * j + l;
                                ++cnt;
                            }
                            else
                            {
                                v1 = faceList[3 * i + k].position - faceList[3 * j + l].position;
                                if (v1.LengthSquared() < 0.001f)
                                {
                                    id[cnt, 0] = 3 * i + k;
                                    id[cnt, 1] = 3 * j + l;
                                    ++cnt;
                                }
                            }
                        }
                    }

                    //只針對有共用邊的2三角面做處理
                    if (cnt == 2)       //two vertices from two faces share one edge
                    {
                        if (id[1, 0] - id[0, 0] != 1)
                            //face 1 vertex 0 is on the shared edge, switch the order to ensure new faces to be clockwise
                        {
                            int tmp = id[0, 0];
                            id[0, 0] = id[1, 0];
                            id[1, 0] = tmp;
                            tmp = id[0, 1];
                            id[0, 1] = id[1, 1];
                            id[1, 1] = tmp;
                        }

                        // insert degenerated quadrilateral the face normals are used for the vertex normals
                        faceList[3 * face + 0].position = faceList[id[1, 0]].position;
                        faceList[3 * face + 1].position = faceList[id[0, 0]].position;
                        faceList[3 * face + 2].position = faceList[id[0, 1]].position;
                        faceList[3 * face + 0].normal = faceNormal[i];
                        faceList[3 * face + 1].normal = faceNormal[i];
                        faceList[3 * face + 2].normal = faceNormal[j];

                        if (VBReference != null)
                        {
                            VBReference[3 * face + 0] = VBReference[id[1, 0]];
                            VBReference[3 * face + 1] = VBReference[id[0, 0]];
                            VBReference[3 * face + 2] = VBReference[id[0, 1]];
                        }
                        ++ face;

                        

                        faceList[3 * face + 0].position = faceList[id[1, 0]].position;
                        faceList[3 * face + 1].position = faceList[id[0, 1]].position;
                        faceList[3 * face + 2].position = faceList[id[1, 1]].position;
                        faceList[3 * face + 0].normal = faceNormal[i];
                        faceList[3 * face + 1].normal = faceNormal[j];
                        faceList[3 * face + 2].normal = faceNormal[j];

                        if (VBReference != null)
                        {
                            VBReference[3 * face + 0] = VBReference[id[1, 0]];
                            VBReference[3 * face + 1] = VBReference[id[0, 1]];
                            VBReference[3 * face + 2] = VBReference[id[1, 1]];
                        }
                        ++ face;
                    }
                }
            }

            faceCount = face;//實際不會用那個多，最後還要在更新一下正確數量。
            faceNormal = null;
            return true;
        }

        static public void RenderDebugFaceList(GraphicsDevice device, Matrix world, Matrix view, Matrix projection, VertexPositionNormal[] faceList, int faceCount)
        {
#if DrawDebug
            device.VertexDeclaration = m_FaceListVertexDec;
            basicEffect.World = world;
            basicEffect.View = view;
            basicEffect.Projection = projection;
            basicEffect.LightingEnabled = true;
            basicEffect.DirectionalLight0.Direction = new Vector3(1.0f, -1.0f, 0.0f);
            basicEffect.DirectionalLight0.Enabled = true;

            basicEffect.Begin();
            foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
            {
                pass.Begin();
                device.DrawUserPrimitives<VertexPositionNormal>(PrimitiveType.TriangleList, faceList, 0, faceCount);
                pass.End();
            }
            basicEffect.End();
            device.VertexDeclaration = null;
#endif
        }

        static public void Render(VertexPositionNormal[] faceList, int faceCount, GraphicsDevice device)
        {
            //Save render states
            SaveRenderState(device);

            //Set render states
            device.RenderState.DepthBufferWriteEnable = false;
            
            device.RenderState.StencilEnable = true;
            //順時針方向的Stencil設定
            device.RenderState.StencilFunction = CompareFunction.Always;
            device.RenderState.StencilDepthBufferFail = StencilOperation.Increment;
            
            device.RenderState.ReferenceStencil = 1;
            device.RenderState.StencilMask = Int32.MaxValue;
            device.RenderState.StencilWriteMask = 0x7fffffff;// Int32.MaxValue;       //0x7fffffff
            device.RenderState.StencilPass = StencilOperation.Keep;
            device.RenderState.StencilFail = StencilOperation.Keep;

            device.VertexDeclaration = m_FaceListVertexDec;
            device.Indices = null;
            device.Vertices[0].SetSource(null, 0, 0);
            if (device.GraphicsDeviceCapabilities.StencilCapabilities.SupportsTwoSided)
            {
                //啟動TwoSidedStencilMode
                device.RenderState.TwoSidedStencilMode = true;
      
                //逆時針方向的Stencil設定
                device.RenderState.CounterClockwiseStencilFunction = CompareFunction.Always;
                device.RenderState.CounterClockwiseStencilDepthBufferFail = StencilOperation.Decrement;

                //畫出shadow Volume
                device.RenderState.CullMode = CullMode.None;
                device.DrawUserPrimitives<VertexPositionNormal>(PrimitiveType.TriangleList, faceList, 0, faceCount);
                device.RenderState.TwoSidedStencilMode = false;
            }
            else
            {
                //Render front faces
                device.RenderState.StencilDepthBufferFail = StencilOperation.Increment;
                device.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
                device.DrawUserPrimitives<VertexPositionNormal>(PrimitiveType.TriangleList, faceList, 0, faceCount);
                //Render back faces
                device.RenderState.StencilDepthBufferFail = StencilOperation.Decrement;
                device.RenderState.CullMode = CullMode.CullClockwiseFace;
                device.DrawUserPrimitives<VertexPositionNormal>(PrimitiveType.TriangleList, faceList, 0, faceCount);
            }
            device.VertexDeclaration = null;

            RestoreRenderState(device);            
        }


        static bool enableZWrite;
        static bool enableStencil;
        static CompareFunction stencilFunc;
        static int stencilRef;
        static int stencilMask;
        static int stencilWriteMask;
        static StencilOperation stencilPassOp;
        static StencilOperation stencilFailOp;
        static StencilOperation stencilZFailOp;
        static CompareFunction ccwStencilFunc;
        static StencilOperation ccwStencilZFail;
        static CullMode cullMode;
        static protected void SaveRenderState(GraphicsDevice device)
        {
            enableZWrite = device.RenderState.DepthBufferWriteEnable;
            enableStencil = device.RenderState.StencilEnable;
            stencilFunc = device.RenderState.StencilFunction;
            stencilRef = device.RenderState.ReferenceStencil;
            stencilMask = device.RenderState.StencilMask;
            stencilWriteMask = device.RenderState.StencilWriteMask;
            stencilPassOp = device.RenderState.StencilPass;
            stencilFailOp = device.RenderState.StencilFail;
            stencilZFailOp = device.RenderState.StencilDepthBufferFail;
            ccwStencilFunc = device.RenderState.CounterClockwiseStencilFunction;
            ccwStencilZFail = device.RenderState.CounterClockwiseStencilDepthBufferFail;
            cullMode = device.RenderState.CullMode;
        }

        static protected void RestoreRenderState(GraphicsDevice device)
        {
            device.RenderState.DepthBufferWriteEnable = enableZWrite;
            device.RenderState.StencilEnable = enableStencil;
            device.RenderState.StencilFunction = stencilFunc;
            device.RenderState.ReferenceStencil = stencilRef;
            device.RenderState.StencilMask = stencilMask;
            device.RenderState.StencilWriteMask = stencilWriteMask;
            device.RenderState.StencilPass = stencilPassOp;
            device.RenderState.StencilFail = stencilFailOp;
            device.RenderState.StencilDepthBufferFail = stencilZFailOp;
            device.RenderState.CounterClockwiseStencilDepthBufferFail = ccwStencilZFail;
            device.RenderState.CounterClockwiseStencilFunction = ccwStencilFunc;
            device.RenderState.CullMode = cullMode; 
        }
    }

    //public class ShadowVolumeEffectRenderManager: IDisposable
    //{
    //    Effect m_ShadowVolumeEffect;

        

    //    public ShadowVolumeEffectRenderManager(ContentManager content)
    //    {
    //        m_ShadowVolumeEffect = null;
    //        m_ShadowVolumeEffect =
    //            Utility.ContentLoad_Effect(content, "ShadowVolume/ShadowVolume");
    //        GetEffectPar();
    //    }

    //    ~ShadowVolumeEffectRenderManager()
    //    {
    //        Dispose();
    //    }

    //    public void Dispose()
    //    {
    //        m_ShadowVolumeEffect.Dispose();
    //        m_ShadowVolumeEffect = null;
    //    }

    //    //Effect傳入參數ˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇ
    //    EffectParameter parWorld = null;
    //    EffectParameter parView = null;
    //    EffectParameter parProjection = null;
    //    EffectParameter parLightDirection = null;
    //    Matrix oldWorld, oldView, oldProjection;
    //    Vector3 oldLightDirection;

    //    void GetEffectPar()
    //    {
    //        Effect effect = m_ShadowVolumeEffect;

    //        parWorld = effect.Parameters["World"];
    //        oldWorld = parWorld.GetValueMatrix();

    //        parView = effect.Parameters["View"];
    //        oldView = parView.GetValueMatrix();

    //        parProjection = effect.Parameters["Projection"];
    //        oldProjection = parProjection.GetValueMatrix();

    //        
    //    }

    //    void SetEffectPar(Matrix view, Matrix projection, Vector3 lightDir, SceneNode node)
    //    {
    //        Matrix TempMat = node.WorldMatrix;
    //        if (Matrix.Equals(oldWorld, TempMat) == false)
    //        {
    //            oldWorld = TempMat;
    //            parWorld.SetValue(TempMat);
    //        }

    //        if (Matrix.Equals(oldProjection, projection) == false)
    //        {
    //            oldProjection = projection;
    //            parProjection.SetValue(projection);
    //        }

    //        if (Matrix.Equals(oldView, view) == false)
    //        {
    //            oldView = view;
    //            parView.SetValue(view);               
    //        }

    //        if (Matrix.Equals(oldLightDirection, lightDir) == false)
    //        {
    //            oldLightDirection = lightDir;
    //            parView.SetValue(lightDir);
    //        }

    //        m_ShadowVolumeEffect.CommitChanges();
    //    }
    //}

    public class ShadowVolumeNode : SceneNode
    {
        static public ShadowVolumeNode Create()
        {
            return ReleaseCheck.CreateNew(typeof(ShadowVolumeNode)) as ShadowVolumeNode;
        }

        int m_FaceCount = 0;
        VertexPositionNormal[] m_FaceList = null;
        int[] m_VBReference = null;

        public override ModelMesh GetModelMeshPart(out int partID)
        {
            throw new NotImplementedException();
        }

        //protected override bool v_ConsiderCurrentTechniqueOK(RenderTechnique CurrentRenderTechnique)
        //{
        //    return MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.ShadowVolume);
        //}

        protected override void v_Dispose()
        {
            m_FaceList = null;
        }

        protected override void v_Render(EffectRenderManager effectRenderManager, GraphicsDevice device)
        {
            //effectRenderManager.SetShadowVolumeNodeEffectPar(WorldMatrix);
            ShadowVolume.Render(m_FaceList, m_FaceCount, device);
        }

        public void CreateFaceList(ContentManager content, GraphicsDevice device, ModelMesh mesh, int partID)
        {
            ShadowVolume.Create(content, device, mesh, partID, out m_FaceList, out m_FaceCount, true, out m_VBReference);
        }

        /// <summary>
        /// 若是骨架模型的shadow volume，
        /// 傳入外部經過骨架計算的VB進來，此FUNCTION會根據外部傳入的資訊，
        /// 將FaceList根據索引表跟新最新的頂點及法向量資訊。
        /// </summary>
        public void RefreshFaceList(VertexPositionNormalTexture [] softwareSkinnedVB)
        {
            int refID;
            for (int a = 0; a < m_FaceCount; a++)
            {
                refID = m_VBReference[a];
                m_FaceList[a].position = softwareSkinnedVB[refID].Position;
                m_FaceList[a].normal = softwareSkinnedVB[refID].Normal;         
            }
        }

        public void RenderDebugFaceListMesh(GraphicsDevice device, Matrix view, Matrix projection)
        {
            ShadowVolume.RenderDebugFaceList(device, WorldMatrix, view, projection, m_FaceList, m_FaceCount);
        }

        /// <summary>
        /// 根據此node的mesh建立shadow volume face list。
        /// mesh傳入null表是要取消shadow volume
        /// 傳入null代表取消shadow volume原因是因為，在編輯狀態下方便開關shadow volume用。
        /// 1.先都幹掉 2.外部會判斷是否要建立shadow volume傳入mesh，才建立。
        /// </summary>     
        public static void CreateShadowVolumeNode(SceneNode node,
            ContentManager content, GraphicsDevice device, ModelMesh mesh, int partID)
        {
            //1.有shadow volume要在node下attach一個shadow volume node
            //把此node下的shadow volume node都幹掉。
            FrameNode childNode = node.FirstChild as FrameNode;
            if (childNode != null)
            {
                ShadowVolumeNode SVNode = childNode as ShadowVolumeNode;
                childNode = childNode.Sibling as FrameNode;
                if (SVNode != null)
                {
                    SVNode.DetachFromParent();
                    SVNode.Dispose();
                }
            }

            node.IfCreateShadowVolume = false;
            if (mesh != null)
            {
                //2.在ATTACH上唯一一個新的
                ShadowVolumeNode shadowVolumeNode = ShadowVolumeNode.Create();
                shadowVolumeNode.CreateFaceList(content, device, mesh, partID);
                shadowVolumeNode.LocalMatrix = Matrix.Identity;
                //shadowVolumeNode.IfAlphaBlenbing = true;
                shadowVolumeNode.NodeName = node.NodeName + "_ShadowVolume";
                shadowVolumeNode.AnalyzeMeshBoundingData((Dictionary<string, object>)mesh.Tag, out shadowVolumeNode.m_IndexBufferSize);
                node.AttachChild(shadowVolumeNode);
                node.IfCreateShadowVolume = true;
            }
        }
   
    }
}