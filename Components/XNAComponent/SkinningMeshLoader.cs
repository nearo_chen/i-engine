﻿//#define UseSoftware
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;

namespace I_XNAComponent
{
    /// <summary>
    /// SkinningMeshLoader會建立bone tree結構，並有讀取特殊設定檔special mesh data的功能。
    /// </summary>
    public class SkinningMeshLoader : BasicMeshLoader, IAnimationPlay
    {
        AnimationTableDataArray m_AnimationTableDataArray;
        public AnimationTableDataArray GetAniArray() { return m_AnimationTableDataArray; } 

        public static SkinningMeshLoader Create()
        {
            return ReleaseCheck.CreateNew(typeof(SkinningMeshLoader)) as SkinningMeshLoader;
        }

#if UseSoftware
        SoftwareSkinningMeshNode m_SkinnedNode;
#else
        SkinningMeshNode m_SkinnedNode;
#endif

        public SkinningMeshLoader()
        {
            m_SkinnedNode = null;
        }

        ~SkinningMeshLoader()
        {
        }

        protected override void v_Dispose()
        {
            if (m_SkinnedNode != null)
                m_SkinnedNode.Dispose();
            m_SkinnedNode = null;
        }

        /// <summary>
        /// 讀取骨架模型並建立bone tree結構。
        /// </summary>
        AnimationTableDataArray LoadFile(string modelFile, ContentManager content, GraphicsDevice device)
        {

#if UseSoftware
            SoftwareSkinningMeshNode skinnedNode = SoftwareSkinningMeshNode.Create();
#else
            SkinningMeshNode skinnedNode = SkinningMeshNode.Create();
#endif
            //SoftwareSkinningMeshNode skinnedNode = SoftwareSkinningMeshNode.CreateNew(typeof(SoftwareSkinningMeshNode)) as SoftwareSkinningMeshNode;
            m_AnimationTableDataArray = skinnedNode.LoadContent(modelFile, content, device);
            InitBasicMeshLoader(skinnedNode, skinnedNode.BoundingSphere, skinnedNode.BoundingBox);

            skinnedNode.NodeName = "SkinningMeshNode";
            skinnedNode.IfLightEnable = true;
            skinnedNode.SetAnimationPlay(0, true, 1, 0, AnimationPlayerManager.AnimationPriority.Low);
            m_SkinnedNode = skinnedNode;

            GetRootNode.AttachChild(skinnedNode.GreateBoneTree());

            return m_AnimationTableDataArray;
        }

        protected override void v_Update(int ellapseMillisecond, ContentManager content, GraphicsDevice device)
        {
            m_SkinnedNode.UpdateSkinningTransform(ellapseMillisecond);
        }

        public override void RenderDebug(SpriteBatch spriteBatch)
        {
            if (I_Utility.IfDrawDebug == false)
                return;

            m_SkinnedNode.RenderDebug();
        }

        /// <summary>
        /// 單純讀取骨架檔，傳入檔案路徑，回傳讀取完成的skinning mesh loader。
        /// </summary>
        static public AnimationTableDataArray LoadSkinningMeshLoader(string fileName,
            ContentManager content, GraphicsDevice device,
            out SkinningMeshLoader skinningMeshLoader)
        {
            skinningMeshLoader = null;
            if (string.IsNullOrEmpty(fileName))
                return null;

            //fileName = Utility.RemoveSubFileName(fileName);
            skinningMeshLoader = SkinningMeshLoader.Create();
            return skinningMeshLoader.LoadFile(fileName, content, device);
        }

        public void AllAnimationStop()
        {
            m_SkinnedNode.AllAnimationStop();
            SetAllMuliObjectPlayerStop();
        }

        public void AnimatedWithRatio(int id, float ratio)
        {
            m_SkinnedNode.AnimatedWithRatio(id, ratio);
        }

        public void SetAnimationPlay(int animation, bool bLoop, float speed, float startPercent, AnimationPlayerManager.AnimationPriority priority)
        {
            m_SkinnedNode.SetAnimationPlay(animation, bLoop, speed, startPercent, priority);
            SetAnimationWithMOPPlay(animation);
        }

        public void SetAnimationPlay(int animation, bool bLoop, float speed, float startPercent)
        {
            SetAnimationPlay(animation, bLoop, speed, startPercent, AnimationPlayerManager.AnimationPriority.Low);
        }

        public void SetAnimationStop(int animation)
        {
            m_SkinnedNode.SetAnimationStop(animation);
        }

        public void SetAnimationPlay_BlendSwitch(int animation, bool bLoop, float speed, float startPercent, int switchTime, AnimationPlayerManager.AnimationPriority priority)
        {
            m_SkinnedNode.SetAnimationPlay_BlendSwitch(animation, bLoop, speed, startPercent, switchTime, priority);
            SetAnimationWithMOPPlay(animation);
        }

        public void SetAnimationPlay_BlendSwitch(int animation, bool bLoop, float speed, float startPercent, int switchTime)
        {
            SetAnimationPlay_BlendSwitch(animation, bLoop, speed, startPercent, switchTime, AnimationPlayerManager.AnimationPriority.Low);
        }

        //public void SetAnimationPlay_BlendSwitch(int animation, bool bLoop, float speed, float startPercent, int switchTime,
        //    AnimationPlayerManager.AnimationPriority priority)
        //{
        //    m_SkinnedNode.SetAnimationPlay_BlendSwitch(animation, bLoop, speed, startPercent, switchTime, priority);
        //    SetAnimationWithMOPPlay(animation);
        //}


        public void SetBlendAnimationPlay(int animation, bool bLoop, float speed, float startPercent)
        {
            m_SkinnedNode.SetBlendAnimationPlay(animation, bLoop, speed, startPercent);
        }

        public void SetBlendAnimationRatio(float blendRatio)
        {
            m_SkinnedNode.SetBlendAnimationRatio(blendRatio);
        }

        //public void AllAnimationStop()
        //{
        //    m_SkinnedNode.AllAnimationStop();
        //}

        public bool IfAnimationPlay(int id)
        {
            return m_SkinnedNode.IfAnimationPlay(id);
        }

        public float GetNowAnimationPlayPercent()
        {
            return m_SkinnedNode.GetNowAnimationPlayPercent();
        }

        public void AddNotDrawModelMesh(string meshName)
        {
            m_SkinnedNode.AddNotDrawModelMesh(meshName);
        }

        public void ClearNotDrawModelMesh()
        {
            m_SkinnedNode.ClearNotDrawModelMesh();
        }

        public int GetNowPlayingAnimationID()
        {
            return m_SkinnedNode.GetNowPlayingAnimationID();
        }

        public int GetLastPlayAnimationID()
        {
            return m_SkinnedNode.GetLastPlayAnimationID();
        }
    }
}
