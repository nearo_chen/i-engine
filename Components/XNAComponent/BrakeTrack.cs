﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using I_XNAUtility;

namespace I_XNAComponent
{
    public struct BrakeTrackData
    {
        public Vector3 topLeftPoint;
        public Vector3 topRightPoint;
        public Vector3 bottomLeftPoint;
        public Vector3 bottomRightPoint;
        public Color color;
    }

    public class BrakeTrack
    {
        public BrakeTrackData[] TrackData;
        public Vector3 LastAdded;
        VertexDeclaration m_VertexDeclaration;
        VertexPositionColorTexture[] m_TrackPlane;
        DynamicVertexBuffer m_DynVertBuffer;
        BasicEffect m_Effect;
        Texture2D m_Texture;
        GraphicsDevice m_Device;

        public int TrackIndex;
        int m_PlaneCount;
        public float Width, Length;
        float m_height;
        const int TrackNum = 10;
        private int m_Alpha = 100;
        private int m_MaxAlpha = 255;

        Vector2 texCo11;
        Vector2 texCo10;
        Vector2 texCo00;
        Vector2 texCo01;

        public int MaxAlpha { get { return m_MaxAlpha; } set { m_MaxAlpha = value; } }
        public int MaxTrack { get { return TrackNum; } }

        public void Dispose()
        {
            m_VertexDeclaration.Dispose();
            m_DynVertBuffer.Dispose();
      
             m_Effect.Dispose();
             m_Texture.Dispose();
             m_Texture = null;    
        }

        //-------------------------------------------------------------------------------------------------
        public BrakeTrack(GraphicsDevice device, ContentManager content, float width, float length, float height)
        {
            m_Device = device;
            TrackIndex = 0;
            TrackData = new BrakeTrackData[TrackNum];
            m_Effect = new BasicEffect(m_Device, null);
            m_VertexDeclaration = new VertexDeclaration(m_Device, VertexPositionColorTexture.VertexElements);

            //while (true)
            {
                try
                {
                    m_Texture = content.Load<Texture2D>("Pic/wheel_01");
                    //break;
                }
                catch (Exception e)
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
            }
            
            m_Effect.Texture = m_Texture;
            m_Effect.TextureEnabled = true;
            Width = width;
            Length = length;
            m_height = height;

            m_TrackPlane = new VertexPositionColorTexture[TrackNum * 6];
            m_DynVertBuffer = new DynamicVertexBuffer(m_Device, VertexPositionColorTexture.SizeInBytes * TrackNum * 6, BufferUsage.WriteOnly);
            m_DynVertBuffer.ContentLost += new EventHandler(DynVertBuffer_ContentLost);

            texCo11 = new Vector2(1, 1);
            texCo10 = new Vector2(1, 0);
            texCo00 = new Vector2(0, 0);
            texCo01 = new Vector2(0, 1);
        }


        //-------------------------------------------------------------------------------------------------
        public void ChangeTexture(ref Texture2D texture)
        {
            m_Effect.Texture = texture;
        }


        //-------------------------------------------------------------------------------------------------
        public void AddBrakeTrack(Vector3 position, ref Vector3 direction, ref Vector3 right, float tireFriction)
        {
            float distance = Vector3.DistanceSquared(position, LastAdded);

            //******************************************************
            //如果新加入的胎痕與上一個的胎痕距離小於某一個值，就不加入
            //*****************************************************************************
            if (distance < 100f)
                return;

            m_Alpha = (int)(tireFriction * m_MaxAlpha);

            TrackData[TrackIndex].color = new Color(255, 255, 255, (byte)m_Alpha);

            //******************************************************
            //如果新加入的與上一個的距離大於一個胎痕的貼圖長*500，Bottom的位置就重算，
            //反之，就讓Bottom的位置與上一個胎痕的Top相接
            //***********************************************************************************************************

            //TrackData[TrackIndex].bottomLeftPoint = position - right * Width / 2 - direction * Length / 2;
            //TrackData[TrackIndex].bottomRightPoint = position + right * Width / 2 - direction * Length / 2;
            //TrackData[TrackIndex].topLeftPoint = position - right * Width / 2 + direction * Length / 2;
            //TrackData[TrackIndex].topRightPoint = position + right * Width / 2 + direction * Length / 2;


            if (distance > Length * 200 || TrackData.Length == 0)
            {
                TrackData[TrackIndex].bottomLeftPoint = position - right * Width / 2 - direction * Length / 2;
                TrackData[TrackIndex].bottomRightPoint = position + right * Width / 2 - direction * Length / 2;
            }
            else
            {
                if (TrackIndex == 0)
                {
                    TrackData[TrackIndex].bottomLeftPoint = TrackData[TrackData.Length - 1].topLeftPoint;
                    TrackData[TrackIndex].bottomRightPoint = TrackData[TrackData.Length - 1].topRightPoint;
                }
                else
                {
                    TrackData[TrackIndex].bottomLeftPoint = TrackData[TrackIndex - 1].topLeftPoint;
                    TrackData[TrackIndex].bottomRightPoint = TrackData[TrackIndex - 1].topRightPoint;
                }
            }
            TrackData[TrackIndex].topLeftPoint = position - right * Width / 2 + direction * Length / 2;
            TrackData[TrackIndex].topRightPoint = position + right * Width / 2 + direction * Length / 2;

            TrackIndex++;

            if (TrackIndex >= TrackData.Length)
                TrackIndex = 0;

            LastAdded = position;
        }


        //-------------------------------------------------------------------------------------------------
        public void RenderBrakeTrack(ref Matrix ViewMat, ref Matrix ProjMat)
        {
            CheckSkimarkCount();

            if (m_PlaneCount <= 0)
                return;

            //save renderstate
            bool alphaBlendEnable = m_Device.RenderState.AlphaBlendEnable;
            //BlendFunction blendFunction = m_Device.RenderState.AlphaBlendOperation;
            Blend alphaSourceBlend = m_Device.RenderState.AlphaSourceBlend;
            Blend alphaDestinationBlend = m_Device.RenderState.AlphaDestinationBlend;

            bool alphaTestEnable = m_Device.RenderState.AlphaTestEnable;
            //CompareFunction alphaFunction = m_Device.RenderState.AlphaFunction;
            int referenceAlpha = m_Device.RenderState.ReferenceAlpha;

            SetRenderState();

            CreateTrackPlane();



            m_Effect.View = ViewMat;
            m_Effect.Projection = ProjMat;
            m_Effect.World = Matrix.CreateTranslation(new Vector3(0, m_height, 0));
            //m_Effect.DiffuseColor = Color.White.ToVector3();
            m_Effect.VertexColorEnabled = true;

            m_Device.VertexDeclaration = m_VertexDeclaration;
            m_Device.Vertices[0].SetSource(m_DynVertBuffer, 0, VertexPositionColorTexture.SizeInBytes);

            m_Effect.Begin();
            //foreach (EffectPass pass in m_Effect.CurrentTechnique.Passes)
            EffectPass pass = m_Effect.CurrentTechnique.Passes[0];
            {
                pass.Begin();

                m_Device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2 * TrackData.Length);

                pass.End();
            }
            m_Effect.End();

            m_Device.VertexDeclaration = null;
            m_Device.Vertices[0].SetSource(null, 0, 0);

            m_Device.RenderState.AlphaBlendEnable = alphaBlendEnable;
            //m_Device.RenderState.AlphaBlendOperation = blendFunction;
            m_Device.RenderState.AlphaSourceBlend = alphaSourceBlend;
            m_Device.RenderState.AlphaDestinationBlend = alphaDestinationBlend;

            m_Device.RenderState.AlphaTestEnable = alphaTestEnable;
            //m_Device.RenderState.AlphaFunction = alphaFunction;
            m_Device.RenderState.ReferenceAlpha = referenceAlpha;
        }


        void SetRenderState()
        {
            m_Device.RenderState.AlphaBlendEnable = true;
            m_Device.RenderState.SourceBlend = Blend.SourceAlpha;
            m_Device.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
            m_Device.RenderState.ReferenceAlpha = 0;
        }

        //-------------------------------------------------------------------------------------------------
        void CheckSkimarkCount()
        {
            int i;
            m_PlaneCount = 0;

            for (i = 0; i < TrackData.Length; i++)
            {
                if (TrackData[i].color.A >= 1)
                    m_PlaneCount++;
            }
        }

        //-------------------------------------------------------------------------------------------------
        public void CreateTrackPlane()
        {
            int i;
            //byte alpha;

            for (i = 0; i < TrackData.Length; i++)
            {
                m_TrackPlane[i * 6 + 0].Position = TrackData[i].bottomLeftPoint;
                m_TrackPlane[i * 6 + 0].Color = TrackData[i].color;
                m_TrackPlane[i * 6 + 0].TextureCoordinate = texCo11;

                m_TrackPlane[i * 6 + 1].Position = TrackData[i].topLeftPoint;
                m_TrackPlane[i * 6 + 1].Color = TrackData[i].color;
                m_TrackPlane[i * 6 + 1].TextureCoordinate = texCo10;

                m_TrackPlane[i * 6 + 2].Position = TrackData[i].topRightPoint;
                m_TrackPlane[i * 6 + 2].Color = TrackData[i].color;
                m_TrackPlane[i * 6 + 2].TextureCoordinate = texCo00;

                m_TrackPlane[i * 6 + 3].Position = TrackData[i].topRightPoint;
                m_TrackPlane[i * 6 + 3].Color = TrackData[i].color;
                m_TrackPlane[i * 6 + 3].TextureCoordinate = texCo00;

                m_TrackPlane[i * 6 + 4].Position = TrackData[i].bottomRightPoint;
                m_TrackPlane[i * 6 + 4].Color = TrackData[i].color;
                m_TrackPlane[i * 6 + 4].TextureCoordinate = texCo01;

                m_TrackPlane[i * 6 + 5].Position = TrackData[i].bottomLeftPoint;
                m_TrackPlane[i * 6 + 5].Color = TrackData[i].color;
                m_TrackPlane[i * 6 + 5].TextureCoordinate = texCo11;

                //m_TrackPlane[i * 6 + 0] = new VertexPositionColorTexture(TrackData[i].bottomLeftPoint, TrackData[i].color, new Vector2(1, 1));
                //m_TrackPlane[i * 6 + 1] = new VertexPositionColorTexture(TrackData[i].topLeftPoint, TrackData[i].color, new Vector2(1, 0));
                //m_TrackPlane[i * 6 + 2] = new VertexPositionColorTexture(TrackData[i].topRightPoint, TrackData[i].color, new Vector2(0, 0));
                //m_TrackPlane[i * 6 + 3] = new VertexPositionColorTexture(TrackData[i].topRightPoint, TrackData[i].color, new Vector2(0, 0));
                //m_TrackPlane[i * 6 + 4] = new VertexPositionColorTexture(TrackData[i].bottomRightPoint, TrackData[i].color, new Vector2(0, 1));
                //m_TrackPlane[i * 6 + 5] = new VertexPositionColorTexture(TrackData[i].bottomLeftPoint, TrackData[i].color, new Vector2(1, 1));

                //if (TrackData[i].color.A >= 1)
                //{
                //    alpha = TrackData[i].color.A;
                //    alpha -= 1;
                //    TrackData[i].color = new Color(255, 255, 255, alpha);
                //}
            }

            if (m_PlaneCount > 0)
                m_DynVertBuffer.SetData(m_TrackPlane, 0, m_TrackPlane.Length, SetDataOptions.NoOverwrite);
        }

        //-------------------------------------------------------------------------------------------------
        private void DynVertBuffer_ContentLost(object sender, EventArgs e)
        {
            if (m_PlaneCount > 0)
                m_DynVertBuffer.SetData(m_TrackPlane, 0, m_TrackPlane.Length, SetDataOptions.NoOverwrite);
        }
    }
}
