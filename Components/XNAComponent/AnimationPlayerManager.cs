﻿//#define UseRSTMatrix

using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;
using MeshDataDefine;

namespace I_XNAComponent
{
    public interface IAnimationPlay
    {
        void SetAnimationPlay_BlendSwitch(int iAnimationID, bool bLoop, float speed, float startPercent, int switchTime, AnimationPlayerManager.AnimationPriority priority);
        void SetAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent, AnimationPlayerManager.AnimationPriority priority);
        void SetAnimationStop(int iAnimationID);
        void AllAnimationStop();
        void AnimatedWithRatio(int iAnimationID, float ratio);
        void SetBlendAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent);
        void SetBlendAnimationRatio(float BlendRatio);
        bool IfAnimationPlay(int iAnimationID);
        int GetNowPlayingAnimationID();
        int GetLastPlayAnimationID();

        AnimationTableDataArray GetAniArray();
    }

    /// <summary>
    /// 控制讀動態檔的matrix key frame播放管理，每個動態檔有很多動作，
    /// 每個動作由一個AnimationPlayer管理，很多動作需要切換就需要
    /// 很多AnimationPlayer來集中管理，所以有此class
    /// </summary>
    public class AnimationPlayerManager : IDisposable, IAnimationPlay
    {
     public   AnimationTableDataArray GetAniArray() { return null; }

        #region 控制blending 切換動作
        const bool bBlendSwitchMode = true;
        bool m_bSwitchingAction = false;
        float m_fTotalSwitchActionTime, m_fNowSwitchActionTime;
        #endregion

        int m_iAnimationAmount;
        KeyframeMeshData m_AnimatedMeshData;
        AnimationPlayer[] m_AnimationControllers; //animation controller for each animation...
        AnimationPlayer m_ActiveAnimationColtroller; //now actived animation...

        //輸出動做的矩陣，目前動畫需要用到的矩陣都存放在此，根據不同的動畫形態內容會不一致。
        Matrix[] m_AnimatingLocalMatrixss;

        //動作blending用
        AnimationPlayer m_AnimationPlayerBlend = null;
        float m_BlendRatio;

        #region 骨架動化才需要 new
        bool bSkinningAnimate = false;
        //骨架動化要傳入shader才會用到的matrix
        Matrix[] m_SkinningAnimatingMatrixss;//used for animating bone transform calculate, on time...
        Matrix[][] m_SkinningAnimatingMatrixss2 = null;

        //Matrix[] []m_MeshSplitAnimateArray = null;//紀錄模型骨架太多分裂的mesh及它們的bone array站存計算用


        //Matrix[][] m_MeshSplitBindInverseWorldBones = null;//站存計算用
        #endregion

        public AnimationPlayerManager()
        {
        }

        virtual public void Dispose()
        {
            m_AnimatedMeshData = null;
            m_ActiveAnimationColtroller = null; //now actived animation....
            m_SkinningAnimatingMatrixss = null;//used for animating bone transform calculate, on time....
            m_AnimatingLocalMatrixss = null;

            if (m_AnimationControllers != null)
            {
                for (int i = 0; i < m_AnimationControllers.Length; i++)
                {
                    m_AnimationControllers[i].Dispose();
                    m_AnimationControllers[i] = null;
                }
                m_AnimationControllers = null;
            }

            //if (m_MeshSplitAnimateArray != null)
            //{
            //    for (int i = 0; i < m_MeshSplitAnimateArray.Length; i++)
            //        m_MeshSplitAnimateArray[i] = null;
            //    m_MeshSplitAnimateArray = null;
            //}

            if (m_SkinningAnimatingMatrixss2 != null)
            {
                for (int i = 0; i < m_SkinningAnimatingMatrixss2.Length; i++)
                    m_SkinningAnimatingMatrixss2[i] = null;
                m_SkinningAnimatingMatrixss2 = null;
            }
        }

        /// <summary>
        /// 讀取動作設定檔XML資料，與整合骨架keyframe資訊。
        /// </summary>
        public AnimationTableDataArray LoadAnimationXML(KeyframeMeshData animatedMeshData, string animationXML)
        {
            if (animatedMeshData.GetType() == typeof(SkinningMeshData))
                bSkinningAnimate = true;

            object ooo = null;
            if (animationXML != null)
                ooo = Utility.LoadXMLFile(animationXML, typeof(AnimationTableDataArray), false);

            m_AnimatedMeshData = animatedMeshData;

            if (ooo == null)//沒有animationTable就全部從頭到尾播放
            {
                m_iAnimationAmount = 1;// m_AnimatedMeshData.Animations.Length;

                m_AnimationControllers = new AnimationPlayer[m_iAnimationAmount];
                //  for (int i = 0; i < m_AnimationControllers.Length; i++)
                {
                    //m_AnimationControllers[0] = new AnimationPlayer(
                    //m_AnimatedMeshData.Animations[0],
                    //    (uint)m_AnimatedMeshData.Animations[0].Duration.TotalMilliseconds,
                    //    m_AnimatedMeshData.Animations[0].Name, true);

                    //搜尋最大撥放格數
                    int maxFrame = 0;
                    for (int bone = 0; bone < m_AnimatedMeshData.Animations[0].BoneKeyframes.Length; bone++)
                    {
                        //if (m_AnimatedMeshData.Animations[0].BoneKeyframes[bone] == null)
                        //    continue;
                        int now = m_AnimatedMeshData.Animations[0].BoneKeyframes[bone].Keyframes.Length;
                        if (maxFrame < now)
                            maxFrame = now;
                    }

                    m_AnimationControllers[0] = new AnimationPlayer(
                        m_AnimatedMeshData.Animations[0],
                        0,
                        maxFrame,
                        m_AnimatedMeshData.Animations[0].Name, //true,
                        24);
                }
            }
            else
            {
                AnimationTableData[] AnimationDataList = ((AnimationTableDataArray)ooo).AnimationDataList;

                m_iAnimationAmount = AnimationDataList.Length;
                m_AnimationControllers = new AnimationPlayer[m_iAnimationAmount];
                for (int i = 0; i < m_AnimationControllers.Length; i++)
                {
                    int fps = 24;
                    if (((AnimationTableDataArray)ooo).FramesPerSecond.HasValue)
                        fps = ((AnimationTableDataArray)ooo).FramesPerSecond.Value;

                    int s=AnimationDataList[i].StartKey;
                    int e=AnimationDataList[i].EndKey;
                    string nnn = AnimationDataList[i].ActionName;
                    AnimationData ddd = m_AnimatedMeshData.Animations[0];
                    m_AnimationControllers[i] = new AnimationPlayer(
                       ddd, //都固定是Animations[0]，因為沒法很多Animations在一個檔案裡，幻動作只能用跳ket frame的方法。
                        s,
                       e,
                        nnn,// true,
                        fps);
                }
            }

            int boneAmount = m_AnimatedMeshData.BonesParent.Length;

            m_AnimatingLocalMatrixss = Utility.GetMatrixArray(boneAmount);

            m_ActiveAnimationColtroller = m_AnimationControllers[0];
            SetAnimationPlay(0, true, 1, 0, AnimationPriority.Low);

            if (bSkinningAnimate)
            {
                m_SkinningAnimatingMatrixss = Utility.GetMatrixArray(boneAmount);

                //骨架動畫的話檢查他有沒有模型分裂
                if (animatedMeshData.GetType() == typeof(SkinningMeshData))
                {
                    SkinningMeshData skinningMeshData = animatedMeshData as SkinningMeshData;
                    //設定模型分裂要怎麼畫的資料
                    if (skinningMeshData.SkinningMeshSplitTable != null)
                    {
                        if (m_SkinningAnimatingMatrixss2 == null)
                        {
                            m_SkinningAnimatingMatrixss2 = new Matrix[skinningMeshData.SkinningMeshSplitTable.Length][];
                            for (int meshID = 0; meshID < m_SkinningAnimatingMatrixss2.Length; meshID++)
                                m_SkinningAnimatingMatrixss2[meshID] = new Matrix[skinningMeshData.SkinningMeshSplitTable[meshID].Length];
                        }

                        int splitAmount = skinningMeshData.SkinningMeshSplitTable.Length;

                        //m_MeshSplitAnimateArray = new Matrix[splitAmount][];
                        //m_MeshSplitBindInverseWorldBones = new Matrix[splitAmount][];
                        //for (int i = 0; i < m_MeshSplitAnimateArray.Length; i++)
                        //{
                        //    //m_MeshSplitAnimateArray[i] = Utility.GetMatrixArray(skinningMeshData.SkinningMeshSplitTable[i].Length);
                        //    //m_MeshSplitBindInverseWorldBones[i] = Utility.GetMatrixArray(skinningMeshData.SkinningMeshSplitTable[i].Length);
                        //}
                    }
                }
            }

            if (ooo == null)
                return null;
            return ooo as AnimationTableDataArray;
        }

        public enum AnimationPriority : byte
        {
            High = 0, Normal, Low,
        }
        public enum PlayType
        {
            Forward, Backward,
        }
        public enum LoopType
        {
            Once, Loop, PinPonLoop,
        }

        class PlayPriorityData
        {
            public void SetData(int iAnimationID, LoopType looptype, float speed, float startPercent, int switchTime)
            {
                AniID = iAnimationID;
                LoopType = looptype;
                Speed = speed;
                StartPercent = startPercent;
                BlendSwitchTime = switchTime;
                PlayType = PlayType.Forward;
            }
            public int AniID = -1;
            public PlayType PlayType = PlayType.Forward;
            public LoopType LoopType = LoopType.Once;
            public int BlendSwitchTime;//動作切換的內插時間
            public float Speed = 1;
            public float StartPercent = 0;
            public void ClearData() { AniID = -1; }
            public bool? IfPlayOnce() { if (AniID < 0) { return null; } if (LoopType == LoopType.Once) { return true; } return false; }
        }
        PlayPriorityData[] m_AnimationPriorityList = new PlayPriorityData[]
        {
            new PlayPriorityData(), new PlayPriorityData(), new PlayPriorityData(),
        };//[0]為高優先

        //public void SetAnimationPlay_BlendSwitch(int iAnimationID, bool bLoop, float speed, float startPercent, int switchTime)
        //{
        //    SetAnimationPlay_BlendSwitch(iAnimationID, bLoop, speed, startPercent, switchTime, AnimationPriority.High);
        //}

        public void SetAnimationPlay_BlendSwitch(int iAnimationID, bool bLoop, float speed, float startPercent,
                                                                           int switchTime, AnimationPriority priority)
        {
            LoopType lt = LoopType.Once;
            if(bLoop)
                lt = LoopType.Loop;
            m_AnimationPriorityList[(int)priority].SetData(iAnimationID, lt, speed, startPercent, switchTime);
          
            SetSwitchActionPlay(iAnimationID, bLoop, speed, startPercent, switchTime);

            playID = iAnimationID;
        }

        void SetSwitchActionPlay(int iAnimationID, bool bLoop, float speed, float startPercent, int switchTime)
        {
            m_fTotalSwitchActionTime = switchTime;
            m_fNowSwitchActionTime = switchTime;
            SetBlendAnimationPlay(iAnimationID, bLoop, speed, startPercent);
            m_bSwitchingAction = true;
        }

        int playID = -1;
        public void SetAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent,
            AnimationPlayerManager.AnimationPriority priority)
        {
            playID = iAnimationID;
            //AllAnimationStop();


            LoopType lt = LoopType.Once;
            if (bLoop)
                lt = LoopType.Loop;
            m_AnimationPriorityList[(int)priority].SetData(iAnimationID, lt, speed, startPercent, 0);

            m_ActiveAnimationColtroller = m_AnimationControllers[iAnimationID];
            m_ActiveAnimationColtroller.AnimationPlay(bLoop, speed, startPercent);

            //用play就把blending弄掉
            //m_AnimationPlayerBlend = null;
        }

        /// <summary>
        /// 檢查所有animation controller看哪一個在playing中，並回傳他的id，都沒有就回傳-1。
        /// </summary>
        public int GetNowPlayingAnimationID()
        {
            for (int a = 0; a < m_AnimationControllers.Length; a++)
            {
                if (m_AnimationControllers[a].IfAnimationPlay())
                    return a;
            }
            return -1;
        }

        /// <summary>
        /// 回船上次設定PLAY的animation ID
        /// </summary>
        public int GetLastPlayAnimationID()
        {
            return playID;
        }

        public void SetAnimationStop(int iAnimationID)
        {
            m_AnimationControllers[iAnimationID].AnimationStop();
        }

        public void AllAnimationStop()
        {
            // m_ActiveAnimationColtroller = null;
            foreach (AnimationPlayer ac in m_AnimationControllers)
                ac.AnimationStop();
        }

        public void AnimatedWithRatio(int iAnimationID, float ratio)
        {
            m_ActiveAnimationColtroller = m_AnimationControllers[iAnimationID];
            m_ActiveAnimationColtroller.AnimatedWithRatio(ratio, true);
        }

        RSTMatrix[] m_RecNowBones;
        public void SetBlendAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent)
        {
            //if(iAnimationID >= m_AnimationControllers.Length)
            //return;
            m_AnimationPlayerBlend = m_AnimationControllers[iAnimationID];

            //紀錄目前骨頭姿態
            if (m_RecNowBones == null)
                m_RecNowBones = new RSTMatrix[m_AnimatedMeshData.Animations[0].BoneKeyframes.Length];
            //for (int boneID = 0; boneID < m_RecNowBones.Length; boneID++)
            //    m_RecNowBones[boneID] = m_ActiveAnimationColtroller.AnimatingBones[boneID];
            m_ActiveAnimationColtroller.AnimatingBones.CopyTo(m_RecNowBones, 0);

            //設定播放，但還未blend完成實時間先傳0進去。
            m_AnimationPlayerBlend.AnimationPlay(bLoop, speed, startPercent);
        }

        //public void SetBlendAnimationNull()
        //{
        //    m_AnimationPlayerBlend.AnimationStop();
        //    m_AnimationPlayerBlend = null;
        //}

        public void SetBlendAnimationRatio(float BlendRatio)
        {
            m_BlendRatio = BlendRatio;
        }


        /// <summary>
        /// 計算每個動作KeyFrame的Local Matrix給輸出動做矩陣
        /// </summary>
        public void UpdateAnimateKeyFrameTransform(int EllapseMillisecond)
        {
    
            for (int aaa = 0; aaa < m_AnimationPriorityList.Length; aaa++)
            {
                bool? bbb = m_AnimationPriorityList[aaa].IfPlayOnce();
                if (bbb.HasValue)
                {
                    if (bbb.Value)//如果只撥放一次的，他停了就把他資料清空
                    {
                        if (IfAnimationPlay(m_AnimationPriorityList[aaa].AniID) == false)
                            m_AnimationPriorityList[aaa].ClearData();
                        break;
                    }
                }
            }

            //判斷最高撥放優先的是否有再撥放，沒撥放就給他跑
            for (int aaa = 0; aaa < m_AnimationPriorityList.Length; aaa++)
            {
                bool? bloop = m_AnimationPriorityList[aaa].IfPlayOnce();
                if (bloop != null)
                {
                    int id = m_AnimationPriorityList[aaa].AniID;
                    bool ifloop = !bloop.Value;
                    if (IfAnimationPlay(id) == false)
                    {
                        SetSwitchActionPlay(id, ifloop,
                            m_AnimationPriorityList[aaa].Speed,
                            m_AnimationPriorityList[aaa].StartPercent,
                            m_AnimationPriorityList[aaa].BlendSwitchTime);
                    }
                    break;
                }
            }


            if (m_bSwitchingAction)
            {
                if (m_fNowSwitchActionTime > 0)
                {
                    SetBlendAnimationRatio(1f - (m_fNowSwitchActionTime / m_fTotalSwitchActionTime));

                    m_fNowSwitchActionTime -= EllapseMillisecond;
                    m_AnimationPlayerBlend.AnimatedWithTime(0);
                }
                else
                {
                    m_bSwitchingAction = false;
                    if (m_ActiveAnimationColtroller != m_AnimationPlayerBlend)//預防自己和自己blending時被stop了
                        m_ActiveAnimationColtroller.AnimationStop();
                    //m_AnimationPlayerBlend.AnimationStop();
                    m_ActiveAnimationColtroller = m_AnimationPlayerBlend;
                    m_AnimationPlayerBlend = null;
                }
            }
            else
            {
                //正常更新時間
                m_ActiveAnimationColtroller.AnimatedWithTime(EllapseMillisecond);
                if (m_AnimationPlayerBlend != null)
                    m_AnimationPlayerBlend.AnimatedWithTime(EllapseMillisecond);
            }

            //Matrix[] localMatrixss = m_AnimatingLocalMatrixss;

            //判斷是否有blending
            if (m_AnimationPlayerBlend != null)
            {
                unsafe
                {
                    Vector3 translation, scale;
                    Quaternion quaternion;
                    Matrix scaleMat;
                    for (int boneID = 0; boneID < m_AnimatingLocalMatrixss.Length; boneID++)
                    {
                        //fixed(RSTMatrix* activeBoneMat = &m_ActiveAnimationColtroller.AnimatingBones[boneID])
                        {
                            //if (m_ActiveAnimationColtroller != m_AnimationPlayerBlend)
                            {
                                fixed (RSTMatrix* blendBoneMat = &m_AnimationPlayerBlend.AnimatingBones[boneID])
                                {
                                    Vector3.Lerp(ref m_RecNowBones[boneID].m_RTMatrix.m_Translation,
                                        //ref activeBoneMat->m_RTMatrix.m_Translation,
                                       ref blendBoneMat->m_RTMatrix.m_Translation, m_BlendRatio,
                                       out translation);

                                    Vector3.Lerp(ref m_RecNowBones[boneID].m_Scale,
                                        //ref activeBoneMat->m_Scale,
                                        ref blendBoneMat->m_Scale, m_BlendRatio,
                                        out scale);

                                    Quaternion.Slerp(ref m_RecNowBones[boneID].m_RTMatrix.m_Quaternion,
                                        //ref activeBoneMat->m_RTMatrix.m_Quaternion,
                                        ref blendBoneMat->m_RTMatrix.m_Quaternion, m_BlendRatio,
                                        out quaternion);

                                    Matrix.CreateFromQuaternion(ref quaternion, out m_AnimatingLocalMatrixss[boneID]);
                                    Matrix.CreateScale(ref scale, out scaleMat);
                                    Matrix.Multiply(ref scaleMat, ref m_AnimatingLocalMatrixss[boneID],
                                        out m_AnimatingLocalMatrixss[boneID]);
                                    m_AnimatingLocalMatrixss[boneID].M41 = translation.X;
                                    m_AnimatingLocalMatrixss[boneID].M42 = translation.Y;
                                    m_AnimatingLocalMatrixss[boneID].M43 = translation.Z;
                                }
                            }
                            //else
                            //{//如果同 動作內插就拿第一格去給他做計算
                            //    fixed (RSTMatrix* blendBoneMat = &m_AnimationPlayerBlend.InitMatrixBones[boneID])
                            //    {
                            //        Vector3.Lerp(ref activeBoneMat->m_RTMatrix.m_Translation,
                            //           ref blendBoneMat->m_RTMatrix.m_Translation, m_BlendRatio,
                            //           out translation);

                            //        Vector3.Lerp(ref activeBoneMat->m_Scale,
                            //            ref blendBoneMat->m_Scale, m_BlendRatio,
                            //            out scale);

                            //        Quaternion.Slerp(ref activeBoneMat->m_RTMatrix.m_Quaternion,
                            //            ref blendBoneMat->m_RTMatrix.m_Quaternion, m_BlendRatio,
                            //            out quaternion);

                            //        Matrix.CreateFromQuaternion(ref quaternion, out localMatrixss[boneID]);
                            //        Matrix.CreateScale(ref scale, out scaleMat);
                            //        Matrix.Multiply(ref scaleMat, ref localMatrixss[boneID], out localMatrixss[boneID]);
                            //        localMatrixss[boneID].M41 = translation.X;
                            //        localMatrixss[boneID].M42 = translation.Y;
                            //        localMatrixss[boneID].M43 = translation.Z;
                            //    }
                            //}
                        }
                    }
                }
            }
            else
            {
                //取得Active的動做資訊
                for (int boneID = 0; boneID < m_AnimatingLocalMatrixss.Length; boneID++)
                    m_ActiveAnimationColtroller.AnimatingBones[boneID].GetMatrix(out m_AnimatingLocalMatrixss[boneID]);
            }


           
            
        }

        /// <summary>
        ///  骨架動畫才需要幫他計算階層matrix array，
        ///  非骨架動畫連m_AnimatingMatrixss都可以不用new了，記得修改。
        /// </summary>
        public void UpdateSkinningKeyFrameTransform(ref Matrix worldTransform)
        {
            Matrix.Multiply
                (ref m_AnimatingLocalMatrixss[0], ref worldTransform, out m_SkinningAnimatingMatrixss[0]);

            //updateMatrix[0] = animationPlayer.AnimatingBones[0] * worldTransform;
            for (int boneID = 1; boneID < m_SkinningAnimatingMatrixss.Length; boneID++)
            {
                //get world matrix...
                Matrix.Multiply(ref m_AnimatingLocalMatrixss[boneID],
                    ref m_SkinningAnimatingMatrixss[m_AnimatedMeshData.BonesParent[boneID]],
                    out m_SkinningAnimatingMatrixss[boneID]);
            }

            SkinningMeshData skinningMeshData = m_AnimatedMeshData as SkinningMeshData;

            // 設定最後要送進shader裡的matrix 陣列資料要乘上invers資訊以便shader內部計算
            //Multiply AnimateWorldBones with Inverse worldBone, ane set into shader.......
            for (int boneID = 0; boneID < m_SkinningAnimatingMatrixss.Length; boneID++)
            {
                Matrix.Multiply(ref skinningMeshData.BindInverseWorldBones[boneID],
                    ref m_SkinningAnimatingMatrixss[boneID], out m_SkinningAnimatingMatrixss[boneID]);
            }

            if (m_SkinningAnimatingMatrixss2 != null)
            {
                for (int meshID = 0; meshID < m_SkinningAnimatingMatrixss2.Length; meshID++)
                {
                    for (int i = 0; i < m_SkinningAnimatingMatrixss2[meshID].Length; i++)
                    {
                        // 設定最後要送進shader裡的matrix 陣列資料要乘上invers資訊以便shader內部計算
                        int boneID = skinningMeshData.SkinningMeshSplitTable[meshID][i];

                        if (boneID >= 0)
                            m_SkinningAnimatingMatrixss2[meshID][i] = m_SkinningAnimatingMatrixss[boneID];
                    }
                }
            }

        }

        /// <summary>
        /// 取得目前骨架動畫Model底下，第幾個mesh對應的bone array，
        /// 因為骨架太多，會把模型細分為幾個mesh。
        /// 若是模型分裂，那他們bone array也會分開。
        /// </summary>
        public Matrix[] GetSkinningBoneArray(int meshID)
        {
            SkinningMeshData skinningMeshData = m_AnimatedMeshData as SkinningMeshData;

            Matrix[] animateMatrixArray = null;

            //取得目前骨架正確資料填入animateMatrixArray
            if (m_SkinningAnimatingMatrixss2 != null)
            {
                //for (int i = 0; i < m_MeshSplitAnimateArray[meshID].Length; i++)
                //{
                //    m_MeshSplitAnimateArray[meshID][i] =
                //        m_SkinningAnimatingMatrixss[skinningMeshData.SkinningMeshSplitTable[meshID][i]];

                //}
                //animateMatrixArray = m_MeshSplitAnimateArray[meshID];
                animateMatrixArray = m_SkinningAnimatingMatrixss2[meshID];
            }
            else
                animateMatrixArray = m_SkinningAnimatingMatrixss;

            return animateMatrixArray;
        }

        public bool IfAnimationPlay(int animation)
        {
            //if (m_bSwitchingAction)//正在blending動作切換中，都回傳true。
            //{
            //    return true;
            //}

            return m_AnimationControllers[animation].IfAnimationPlay();
        }

        /// <summary>
        /// 取得目前骨架骨頭的local matrix
        /// </summary>      
        public Matrix GetBoneAnimateLocalMatrix(int boneID)
        {
            return m_AnimatingLocalMatrixss[boneID];
            //return m_ActiveAnimationColtroller.AnimatingBones[boneID].mat;
        }


        /// <summary>
        /// 取得該骨頭時間上所有matrix
        /// </summary>      
        public Matrix[] GetBoneAllAnimationMatrix(int boneID)
        {
            Matrix[] boneMatrixss = m_ActiveAnimationColtroller.GetBoneAnimationKeyframes(boneID);

            int parentID = m_AnimatedMeshData.BonesParent[boneID];//抓parent來算世界座標
            if (parentID != -1)
            {
                Matrix[] boneParentMatrixss = m_ActiveAnimationColtroller.GetBoneAnimationKeyframes(parentID);

                if (boneParentMatrixss.Length == boneMatrixss.Length)//有時parent會只有3個keyframe這時就不行用
                {
                    for (int a = 0; a < boneMatrixss.Length; a++)
                    {
                        Matrix.Multiply(ref boneMatrixss[a], ref boneParentMatrixss[a], out boneMatrixss[a]);
                    }
                }
            }
            return boneMatrixss;
        }

        /// <summary>
        /// 取得目前動作播放keyframe block相對於起始撥放block的播放格數，
        /// 也就是取得該骨頭，目前撥放到第幾格。
        /// </summary>
        public int GetNowAnimationPlayKeyframeBlockID(int boneID)
        {
            return m_ActiveAnimationColtroller.GetBoneAnimationNowOffsetKeyframeBlockID(boneID);
        }

        public float GetPlayPercent()
        {
            return m_ActiveAnimationColtroller.GetPlayPercent();
        }
    }

}
