﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAUtility;
#endregion

namespace I_XNAComponent
{
    public class MyEmmisive
    {
        RenderTarget2D m_EmmisiveRT = null;   
        Effect m_BlendEffect = null;

        public RenderTarget2D GetEmmisiveRenderTarget() { return m_EmmisiveRT; } 

        public MyEmmisive(ContentManager Content, GraphicsDevice device, int TexW, int TexH)
        {
            m_EmmisiveRT = new RenderTarget2D(device, TexW, TexH, 1, SurfaceFormat.Color,
                  //MultiSampleType.None, 0,
                Utility.MultiSampleType, Utility.MultiSampleQuality, 
            RenderTargetUsage.PreserveContents);

            if (Utility.IfContentOutSide)
            {
                m_BlendEffect = Utility.ContentLoad_Effect(Content, Utility.GetFullPath("XNBData/Content/Emmisive/Emmisive"));
            }
            else
            {
                m_BlendEffect = Utility.ContentLoad_Effect(Content, "Emmisive\\Emmisive");
            }

        }

        public void Dispose()
        {
            if (m_BlendEffect != null)
            {
                m_BlendEffect.Dispose();
                m_BlendEffect = null;
            }

            if (m_EmmisiveRT != null)
                m_EmmisiveRT.Dispose();
            m_EmmisiveRT = null;
        }

        public void RenderEmmisiveBlending(Texture2D EmmTex, Texture2D SceneTexture, SpriteBatch spritebatch, GraphicsDevice device)
        {
            device.Textures[1] = EmmTex;

            spritebatch.Begin(SpriteBlendMode.None,
                              SpriteSortMode.Immediate,
                              SaveStateMode.None);

            m_BlendEffect.Begin();
            m_BlendEffect.CurrentTechnique.Passes[0].Begin();

            // Draw the quad.
            spritebatch.Draw(SceneTexture, Vector2.Zero, Color.White);
            spritebatch.End();

            // End the custom effect.
            m_BlendEffect.CurrentTechnique.Passes[0].End();
            m_BlendEffect.End();

            device.Textures[0] = null;
            device.Textures[1] = null;
        }
    }
}
