﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using System.IO;
using I_XNAUtility;

namespace I_XNAComponent
{
    /// <summary>
    /// 控制Effect參數，以及繪圖。
    /// </summary>
    public partial class EffectRenderManager : IDisposable
    {
        /// <summary>
        /// 設定CurrentTechnique，Effect.Begin()還有Effect的CurrentTechnique.Passes[0].Begin()
        /// </summary>
        public void SetRenderBegin_DeferredShading(SceneNode.RenderTechnique currentTechnique)
        {
            //m_CurrentRenderTechnique = currentTechnique;               
            if (currentTechnique ==
                (MeshNode.RenderTechnique.SimpleMesh |
                SceneNode.RenderTechnique.DeferredShading))
            {
                m_RenderEffect.CurrentTechnique = m_RenderEffect.Techniques["SimpleMesh_Deferred"];
            }
            else if (currentTechnique ==
                (MeshNode.RenderTechnique.SimpleMesh |
                SceneNode.RenderTechnique.DeferredShading |
                SceneNode.RenderTechnique.TengentMesh))
            {
                m_RenderEffect.CurrentTechnique = m_RenderEffect.Techniques["SimpleMesh_TengentMesh_Deferred"];
            }
            else if (currentTechnique ==
                (MeshNode.RenderTechnique.SkinnedMesh |
                SceneNode.RenderTechnique.DeferredShading))
            {
                m_RenderEffect.CurrentTechnique = m_RenderEffect.Techniques["SkinningMesh_Deferred"];
            }
            else if (currentTechnique ==
                (MeshNode.RenderTechnique.SkinnedMesh |
                SceneNode.RenderTechnique.DeferredShading |
                SceneNode.RenderTechnique.TengentMesh))
            {
                m_RenderEffect.CurrentTechnique = m_RenderEffect.Techniques["SkinningMesh_TengentMesh_Deferred"];
            }

            m_RenderEffect.Begin();
            m_RenderEffect.CurrentTechnique.Passes[0].Begin();
        }


        /// <summary>
        /// Deffer shading於畫出MeshNode專用的設定參數，及一些相關設定，
        /// 並呼叫MeshNode的Render畫出。
        /// </summary>
        public void DeferredShading_RenderMeshNode(MeshNode meshNode, ref uint triangle, GraphicsDevice device, bool bSetNormalMap, float MaxMaterialBankSize)
        {
            //meshNode.EmmisiveAlpha = 1.0f;
            //meshNode.iMaterialBankID = 4;
            //meshNode.SetDrawBoundingBox(true, new Color(Color.Yellow, 80));
            //meshNode.SetDrawBoundingSphere(true, new Color(Color.Green, 80));

            //   Deferred這邊的UV位移好像不吃有BUG，應該是SHADER有問題
            UpdateTime(meshNode);
            UpdateUVAnimation(meshNode);

            SetTextureEffectPar(meshNode, bSetNormalMap);
            SetEmmisiveEffectPar(meshNode);

            SetBlendColorEffectPar(meshNode);
            SetObjectAlphaEffectPar(meshNode);
            deferred_SetMaterialID(meshNode, MaxMaterialBankSize);

            SetWindWaveEffectpar(meshNode);

            meshNode.Render(this, ref triangle, device);
        }

        /// <summary>
        /// Deffer shading於畫出SkinningMeshNode專用的設定參數，及一些相關設定，
        /// 並呼叫SkinningMeshNode的Render畫出。
        /// </summary>
        public void DeferredShading_RenderSkinningMeshNode(SkinningMeshNode skinNode, ref uint triangle, GraphicsDevice device)
        {


            SetTextureEffectPar(skinNode, true);
            SetEmmisiveEffectPar(skinNode);

            //SetBlendColorEffectPar(skinNode);
            //SetObjectAlphaEffectPar(skinNode);
            //  deferred_SetMaterialID(skinNode, MaxMaterialBankSize);

            skinNode.Render(this, ref triangle, device);
        }

        /// <summary>
        /// 於繪製所有Node之前，設定通用的Effect參數，像是Projection，Fog 平行光...之類。
        /// </summary>
        public void DeferredShading_SetCommonEffects(ref Matrix projection, ref Matrix view)
        {
            parProjection.SetValue(projection);
            parView.SetValue(view);
        }

        ///// <summary>
        ///// 一直更新時間進去shader
        ///// </summary>
        //void deferred_UpdateTime()
        //{
        //    float totalEllapseMillisecond = (float)Environment.TickCount / 1000f;//1代表1秒
        //    if (oldTotalEllapseMillisecond != totalEllapseMillisecond)
        //    {
        //        oldTotalEllapseMillisecond = totalEllapseMillisecond;
        //        parTotalEllapsedMilliseonds.SetValue(totalEllapseMillisecond);
        //    }
        //}

        ///// <summary>
        ///// 控制貼圖軸的動態
        ///// </summary>
        //void deferred_UVControl(SceneNode node)
        //{
        //    if (oldTranslateUVSinLoopSpeed != node.TranslateUVSinLoopSpeed)
        //    {
        //        oldTranslateUVSinLoopSpeed = node.TranslateUVSinLoopSpeed;
        //        parTranslateUVSinLoopSpeed.SetValue(node.TranslateUVSinLoopSpeed);
        //    }

        //    if (oldTranslateUVSpeed != node.TranslateUVSpeed)
        //    {
        //        oldTranslateUVSpeed = node.TranslateUVSpeed;
        //        parTranslateUVSpeed.SetValue(node.TranslateUVSpeed);
        //    }

        //    if (oldCenterScaleUV != node.centerScaleUV)
        //    {
        //        oldCenterScaleUV = node.centerScaleUV;
        //        parCenterScaleUV.SetValue(node.centerScaleUV);
        //    }
        //}

        //void deferred_RenderFeature()
        //{
        //}

        /// <summary>
        /// 設定此node material 的編號
        /// </summary>
        /// <param name="node"></param>
        /// <param name="MaxMaterialBankSize"></param>
        void deferred_SetMaterialID(SceneNode node, float MaxMaterialBankSize)
        {
            float shaderID = (float)node.iMaterialBankID / MaxMaterialBankSize;
            if (oldMaterialBankID != shaderID)
            {
                oldMaterialBankID = shaderID;
                parMaterialBankID.SetValue(oldMaterialBankID);
            }
        }
    }
}
