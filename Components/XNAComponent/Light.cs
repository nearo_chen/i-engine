﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace I_XNAComponent
{
    ////just simple light data...
    //public class DirectionalLight
    //{
    //    Vector3 m_Direction;
    //    Vector3 m_Position;
    //    Vector3 m_LightColor;

    //    public DirectionalLight()
    //    {
    //    }

    //    //public void initialize(Vector3 Direction, Vector3 Position, Vector3 LightColor)
    //    //{
    //    //    m_Direction = Direction;
    //    //    m_Position = Position;
    //    //    m_LightColor = LightColor;
    //    //}

    //    public Vector3 Direction
    //    {
    //        set { 
    //            m_Direction = Vector3.Normalize(value); 
    //        }
    //        get { return m_Direction; }
    //    }

    //    public Vector3 Position
    //    {
    //        set { m_Position = value; }
    //        get { return m_Position; }
    //    }

    //    public Vector3 LightColor
    //    {
    //        set { m_LightColor = value; }
    //        get { return m_LightColor; }
    //    }
    //}


    [Serializable]
    public struct PointLight
    {
        static public PointLight Default(Vector3? pos)
        {
            Vector3 position = Vector3.Zero;
            if (pos.HasValue)
                position = pos.Value;
            PointLight lll;
            lll.ParentNodeName = null;
            lll.Name = "_PLight" + (int)(I_XNAUtility.MyMath.GetRand() * 99999f);
            lll.postionX = position.X;
            lll.postionY = position.Y;
            lll.postionZ = position.Z;
            lll.colorR = lll.colorG = lll.colorB = 255;
            lll.Strength = 1f;
            lll.Decay = 2f;
            lll.rangeValue = 100f;
            lll.SpecularIntensity = 2;
            return lll;
        }
        public string ParentNodeName;//可當名稱使用(有個 _name)，或是點光源是否要parent在某個Node模型物件上，null表示parent在root node上
        public string Name;
        public float postionX;
        public float postionY;
        public float postionZ;

        public byte colorR;
        public byte colorG;
        public byte colorB;

        public float Strength;
        public float Decay;         //衰簡直
        public float rangeValue;//光的範圍
        public float SpecularIntensity;//光的specular強度直接乘上的倍率，反射地上光圈大小。
    };

    [Serializable]
    public class PointLights
    {
        PointLights() { }
        public PointLights(int amount)
        {
            Amount = amount;
            PointLight = new PointLight[Amount];
        }

        public int Amount; //存給人看的，不然不需要。
        public PointLight[] PointLight;
    };

    [Serializable]
    public struct SpotLightData
    {
        public float angleTheta;//SPOT的內圈
        public float anglePhi;//SPOT的外圈
        public float strength;//計算後光的強度在加強的倍數
        public float decay;//SPOT的衰減強度，DECAY不能為0，不然在pow()時，0次方會變成1數字會錯
        public byte colorR;
        public byte colorG;
        public byte colorB;
        public float range;//最遠照射範圍
    }

    [Serializable]
    public class SpotLights
    {
        public SpotLights() { }
        public SpotLights(int amount)
        {
            Amount = amount; 
            SpotLight = new SpotLight[amount];
        }
        public int Amount; //存給人看的，不然不需要。
        public SpotLight[] SpotLight;
    }

    [Serializable]
    public struct SpotLight
    {
        static public SpotLight Default(Vector3? pos, Vector3? dir)
        {
            Vector3 position = Vector3.Zero;
            if (pos.HasValue)
                position = pos.Value;

            Vector3 direction = Vector3.UnitX;
            if (dir.HasValue)
                direction = dir.Value;

            SpotLight lll;
            lll.ParentNodeName = null;
            lll.Name = "_SPLight" + (int)(I_XNAUtility.MyMath.GetRand() * 99999f);
            lll.position = position;
            lll.direction = direction;

            lll.Data.colorR = lll.Data.colorG = lll.Data.colorB = 255;
            lll.Data.strength = 1f;
            lll.Data.decay = 2f;
            lll.Data.range = 100f;

            lll.Data.angleTheta = 30;
            lll.Data.anglePhi = 45;
            return lll;
        }

        public string ParentNodeName;//可當名稱使用(有個 _name)，或是點光源是否要parent在某個Node模型物件上，null表示parent在root node上
        public string Name;

        public Vector3 position;
        public Vector3 direction;
        public SpotLightData Data;
        //      public string parentNodeName;//光源是否要parent在某個物件上

        //public SpotLight() { spotLightData = new SpotLightData(); }
        //public SpotLight(ref Vector3 initialPosition, ref Vector3 initialdirection,
        //    float initTheta, float initialPhi, float initialStrength, float InitialDecay, ref Vector3 InitialColor, float initRange)
        ////   ,            string parentName)
        //{
        //    spotLightData = new SpotLightData();
        //    position = initialPosition;
        //    direction = initialdirection;
        //    //   parentNodeName = 
        //    spotLightData.angleTheta = initTheta;
        //    spotLightData.anglePhi = initialPhi;
        //    spotLightData.strength = initialStrength;
        //    spotLightData.decay = InitialDecay;
        //    spotLightData.colorR = InitialColor;
        //    spotLightData.range = initRange;

        //}
        //public float angleTheta
        //{
        //    get { return spotLightData.angleTheta; }
        //    set { spotLightData.angleTheta = value; }
        //}
        //public float anglePhi
        //{
        //    get { return spotLightData.anglePhi; }
        //    set { spotLightData.anglePhi = value; }
        //}
        //public float strength
        //{
        //    get { return spotLightData.strength; }
        //    set { spotLightData.strength = value; }
        //}
        //public float decay
        //{
        //    get { return spotLightData.decay; }
        //    set { spotLightData.decay = value; }
        //}
        //public byte colorR
        //{
        //    get { return spotLightData.colorR; }
        //    set { spotLightData.colorR = value; }
        //}
        //public byte colorG
        //{
        //    get { return spotLightData.colorG; }
        //    set { spotLightData.colorR = value; }
        //}
        //public byte colorB
        //{
        //    get { return spotLightData.colorR; }
        //    set { spotLightData.colorR = value; }
        //}
        //public float range
        //{
        //    get { return spotLightData.range; }
        //    set { spotLightData.range = value; }
        //}
    }
}
