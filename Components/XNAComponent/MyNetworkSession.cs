﻿

//#region Using Statements
//using System;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.GamerServices;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Net;
//using Microsoft.Xna.Framework.Content;
//using System.Collections.Generic;

//using System.Threading;
//#endregion

//namespace I_XNAComponent
//{
//    abstract public class MyNetworkSession
//    { 
//        //收到封包...
//        abstract protected void v_DataReceive(NetworkGamer sender, PacketReader reader);
//        abstract protected void v_PlayerJoin(NetworkGamer sender);
//        abstract protected void v_PlayerLeft(NetworkGamer sender);

//        const int maxGamers = 4;
//        const int maxLocalGamers = 1;
//        string errorMessage;

     
//        NetworkSession networkSession = null;
//        protected PacketWriter m_PacketWriter = new PacketWriter();
//        protected PacketReader m_PacketReader = new PacketReader();

//        Thread simulationThread = null;



//        public bool bHasSession()
//        {
//            if (networkSession == null)
//                return false;
//            return true;
//        }

//        public MyNetworkSession() { }

//        protected void initialize()
//        {
       
//        }

//        int m_MyID = -1;
//        public int GetMyID()
//        {
//            if (networkSession == null)
//                return -1;
//            if (m_MyID == -1)
//                m_MyID = networkSession.AllGamers.IndexOf(networkSession.LocalGamers[0]);
//            return m_MyID;
//        }

//        public int GetGamerMyGameID(NetworkGamer ggg)
//        {
//            return networkSession.AllGamers.IndexOf(ggg);
//        }

//        public  GamerCollection< NetworkGamer> GetAllGamer()
//        {
//            return networkSession.AllGamers;
//        }

//        public void Dispose()
//        {
//            //先停止thread再把networkSession  null
//            //simulationThread.Abort();
//            //simulationThread = null;
//            EndUpdateThread();


//            if (networkSession != null)
//            {
//                networkSession.Dispose();
//                networkSession = null;
//            }

        
            
//        }

//        //bool bInThread = false;
//        public void SetUpdateThread()
//        {
//            // Start a new physics simulation.
//            ThreadStart threadStarter = delegate { UpdateInThread(); };
//            simulationThread = new Thread(threadStarter)
//            {
//                Name = "UpdateNetwork Thread",
//                Priority = ThreadPriority.Highest,
//            };
//            simulationThread.Start();
//            //timeCount = Environment.TickCount;
//            //bInThread = true;
//        }
//     //   int timeCount, nowTime;

//        /// <summary>
//        /// 設定thread跑update
//        /// </summary>
//        void UpdateInThread()
//        {
//            while (true)
//            {
//                //if (bInThread == false)
//                //    break;
//              //  nowTime = Environment.TickCount;
//             //   if (nowTime - timeCount > 500)
//              //  {
//            //        timeCount = nowTime;
//                    if(networkSession != null)
//                    {
//                        // If we are in a network session, update it.
//                        UpdateNetworkSession();
//                    }
//          //      }

//                    Thread.Sleep(1);//春哥說他給5就差不多了看情況再調整
//            }
//        }

//        /// <summary>
//        /// 結束多值行序update
//        /// </summary>
//        public void EndUpdateThread()
//        {
//            //bInThread = false;
//            simulationThread.Abort();
//            simulationThread = null;
//            //simulationThread = null;
//        }

//        /// <summary>
//        /// 若設定了UpdateInThread(),那Update()會無效
//        /// </summary>
//        public void Update()
//        {
//            //thread還活著就不用這個
//            if (simulationThread != null)
//                return;

//            //if (networkSession == null)
//            //{
//            //    // If we are not in a network session, update the
//            //    // menu screen that will let us create or join one.
//            //    //UpdateMenuScreen();
//            //}
//            //else

//            if(networkSession != null)
//            {        
//                // If we are in a network session, update it.
//                UpdateNetworkSession();             
//            }
//        }

//        /// <summary>
//        /// Menu screen provides options to create or join network sessions.
//        /// </summary>
//        static public bool UpdateCreatAccountMenu()
//        {
//            if (Gamer.SignedInGamers.Count == 0)
//            {
//                // If there are no profiles signed in, we cannot proceed.
//                // Show the Guide so the user can sign in.
//                //Guide.ShowSignIn(maxLocalGamers, false);
//                return false;
//            }
//            return true; 
//        }

//        enum SessionState : byte
//        {
//            CanJoin = 2,
//            CannotJoin = 4,
//        };

//        IAsyncResult m_AsyncResult = null;

//        /// <summary>
//        /// Starts hosting a new network session.
//        /// </summary>
//         public void CreateSession(bool bLocal)
//        {
//            try
//            {
//                if(bLocal)
//                {
//                    //networkSession = NetworkSession.Create(NetworkSessionType.Local,
//                    //                                       maxLocalGamers, maxGamers);
//                    m_AsyncResult = NetworkSession.BeginCreate(
//                                                        NetworkSessionType.Local, maxLocalGamers,
//                                                        maxGamers,
//                                                        0, null, null, null);
//                }
//                else
//                {
//                    //networkSession = NetworkSession.Create(NetworkSessionType.SystemLink,
//                    //                                       maxLocalGamers, maxGamers);


//                    NetworkSessionProperties nsp = new NetworkSessionProperties();
//                    nsp[0] = (byte)SessionState.CannotJoin;
//                    // Begin an asynchronous create network session operation.
//                    m_AsyncResult = NetworkSession.BeginCreate(
//                                                        NetworkSessionType.SystemLink, maxLocalGamers,
//                                                        maxGamers,
//                                                        0, nsp, null, null);

//                }

//                errorMessage = "Create Session Begin...";
//                //給host加上tag
//                //networkSession..Host.Tag = tag;
//            }
//            catch (Exception e)
//            {
//                errorMessage = e.Message;
//            }
//        }
        
//         public bool CreateSessionOperationCompleted()
//         {
//             if (m_AsyncResult == null || m_AsyncResult.IsCompleted == false)
//                 return false;

//             try
//             {
//                 // End the asynchronous create network session operation.
//                 networkSession = NetworkSession.EndCreate(m_AsyncResult);
                 
//                 networkSession.SessionProperties[0] = (byte)SessionState.CanJoin;
//                 HookSessionEvents();
//                 return true;
//             }
//             catch (Exception exception)
//             {
//                 errorMessage = exception.Message;           
//             }
//             return false;
//         }

//        public bool JoinSessions()
//        {
//            NetworkSessionProperties nsp = new NetworkSessionProperties();
//            nsp[0] = (byte)SessionState.CanJoin;

//            try
//            {
//                // Search for sessions.
//                using (AvailableNetworkSessionCollection availableSessions =
//                            NetworkSession.Find(NetworkSessionType.SystemLink,
//                                                maxLocalGamers, nsp))
//                {
//                    if (availableSessions.Count > 0)
//                    {
//                        foreach(AvailableNetworkSession avs in availableSessions)
//                        {
//                            //加入session
//                            networkSession = NetworkSession.Join(avs);                      
//                            HookSessionEvents();
//                            errorMessage = "Join Session";
//                            return true;
                         

//                            ////加入session看看是否還可以加入
//                            //networkSession = NetworkSession.Join(avs);
                
//                            ////if(networkSession.SessionState == NetworkSessionState.Playing)
//                            //if (networkSession.Host.IsReady)
//                            //{
//                            //    //不能加入了就離開
//                            //    networkSession.Dispose();
//                            //    networkSession = null;
//                            //    continue;
//                            //}
//                            //else
//                            //{
//                            //    HookSessionEvents();
//                            //    errorMessage = "Join Session";
//                            //    return true;
//                            //}
//                        }                         
//                    }                 
//                }
//            }
//            catch (Exception e)
//            {
//                errorMessage = e.Message;
//            }
//            return false;
//        }

//        public void SetSessionAllReady()
//        {
//            networkSession.SessionProperties[0] = (byte)SessionState.CannotJoin;
//            //foreach (NetworkGamer NG in networkSession.AllGamers)
//            //{
//            //    NG.IsReady = true;
//            //}
//        }
   

//        /// <summary>
//        /// After creating or joining a network session, we must subscribe to
//        /// some events so we will be notified when the session changes state.
//        /// </summary>
//        void HookSessionEvents()
//        {
//            networkSession.GamerJoined += GamerJoinedEventHandler;
//            networkSession.SessionEnded += SessionEndedEventHandler;
//            networkSession.GamerLeft += GamerLeftEventHandler;
//        }

//        public struct GamerTag
//        {
//            public int id;
//        }

//        /// <summary>
//        /// This event handler will be called whenever a new gamer joins the session.
//        /// We use it to allocate a Tank object, and associate it with the new gamer.
//        /// </summary>
//        void GamerJoinedEventHandler(object sender, GamerJoinedEventArgs e)
//        {
//            errorMessage = "'"+e.Gamer.Gamertag+"'   " + "Enter session...";

//            //refresh id，ID是算進了就沒有出的...所以不會變...
//            int gamerIndex = networkSession.AllGamers.IndexOf(e.Gamer);
//            GamerTag tag;
//            tag.id = gamerIndex;
//            e.Gamer.Tag = tag;

//            v_PlayerJoin(e.Gamer);

//            //e.Gamer.Tag 可以利用
//          //  e.Gamer.Tag = new Tank(gamerIndex, Content, screenWidth, screenHeight);

//            //進入Session後會將房間內每個人，都用這function一個一個傳過來....
//            //所以一連上host，裡面已經有很多人的話，這裡會跑很多次，
//            //所以client的這裡無法用來確定玩家是否連上線，
//            //可以確定的地方是host的這邊，只會跑一次，
//            //所以如果是host，而且有人連上來，在這邊就送一個確定連上的訊息給他。
//            //if (bCreateSession)
//            //    v_Host_GamerEnter(e.Gamer);
//        }

//        void GamerLeftEventHandler(object sender, GamerLeftEventArgs e)
//        {
//            //如果是開房的結束了，這裡的e.Gamer是自己，這樣沒意義...
//            if (networkSession == null)
//                return;

//            errorMessage = "'" + e.Gamer.Gamertag + "'   " + "Left session...";

//            v_PlayerLeft(e.Gamer);
//        }


//        /// <summary>
//        /// Event handler notifies us when the network session has ended.
//        /// </summary>
//        void SessionEndedEventHandler(object sender, NetworkSessionEndedEventArgs e)
//        {
//            errorMessage = e.EndReason.ToString();

//            networkSession.Dispose();
//            networkSession = null;
//        }


//        /// <summary>
//        /// Updates the state of the network session, moving the tanks
//        /// around and synchronizing their state over the network.
//        /// </summary>
//        void UpdateNetworkSession()
//        {
//            //TimeSpan sss = TimeSpan.FromSeconds(2);
//            //networkSession.SimulatedLatency = sss;

//            // Update our locally controlled tanks, and send their
//            // latest position data to everyone in the session.
//            //foreach (LocalNetworkGamer gamer in networkSession.LocalGamers)
//            //{
//            //    UpdateLocalGamer(gamer);
//            //}

//            // Pump the underlying session object.
//            networkSession.Update();
           

//            // Make sure the session has not ended.
//            if (networkSession == null)
//                return;

//            // Read any packets telling us the positions of remotely controlled tanks.
//            foreach (LocalNetworkGamer gamer in networkSession.LocalGamers)
//            {
//                ReadIncomingPackets(gamer);
//            }
//        }

//        //送封包可參考這邊
//        ///// <summary>
//        ///// Helper for updating a locally controlled gamer.
//        ///// </summary>
//        //void UpdateLocalGamer(LocalNetworkGamer gamer)
//        //{
//        //    //// Look up what tank is associated with this local player.
//        //    //Tank localTank = gamer.Tag as Tank;

//        //    //// Update the tank.
//        //    //ReadTankInputs(localTank, gamer.SignedInGamer.PlayerIndex);

//        //    //localTank.Update();

//        //    //// Write the tank state into a network packet.
//        //    //packetWriter.Write(localTank.Position);
//        //    //packetWriter.Write(localTank.TankRotation);
//        //    //packetWriter.Write(localTank.TurretRotation);

//        //    //// Send the data to everyone in the session.
//        //    //gamer.SendData(packetWriter, SendDataOptions.InOrder);
//        //}

//        public void SendToHost(PacketWriter packetWriter)
//        {
//            networkSession.LocalGamers[0].SendData(packetWriter, SendDataOptions.InOrder, networkSession.Host);
//        }

//        public void SendToGamer(PacketWriter packetWriter, NetworkGamer gamer)
//        {
//            networkSession.LocalGamers[0].SendData(packetWriter, SendDataOptions.InOrder, gamer);
//        }

//        public void SendToEveryOne(PacketWriter packetWriter)
//        {
//            networkSession.LocalGamers[0].SendData(packetWriter, SendDataOptions.InOrder);
//        }


//        //收封包...
//        /// <summary>
//        /// Helper for reading incoming network packets.
//        /// </summary>
//        void ReadIncomingPackets(LocalNetworkGamer gamer)
//        {
//            // Keep reading as long as incoming packets are available.
//            while (gamer.IsDataAvailable)
//            {
//                NetworkGamer sender;

//                // Read a single packet from the network.
//                gamer.ReceiveData(m_PacketReader, out sender);

//                v_DataReceive(sender, m_PacketReader);

//                // Discard packets sent by local gamers: we already know their state!
//               // if (sender.IsLocal)
//              //      continue;

//                //把收到的資訊更新
//                //// Look up the tank associated with whoever sent this packet.
//                //Tank remoteTank = sender.Tag as Tank;

//                //// Read the state of this tank from the network packet.
//                //remoteTank.Position = packetReader.ReadVector2();
//                //remoteTank.TankRotation = packetReader.ReadSingle();
//                //remoteTank.TurretRotation = packetReader.ReadSingle();
//            }
//        }

//        /// <summary>
//        /// Draws the startup screen used to create and join network sessions.
//        /// </summary>
//        //public void DrawConnectStatus()
//        //{
//        //    string message = string.Empty;

//        //    if (!string.IsNullOrEmpty(errorMessage))
//        //        message += "Error:\n" + errorMessage.Replace(". ", ".\n") + "\n\n";

//        //    //message += "A = create session\n" +
//        //    //           "B = join session";

//        //    if (networkSession != null)
//        //    {
//        //        foreach (NetworkGamer gamer in networkSession.AllGamers)
//        //        {
//        //            message += "'"+gamer.Gamertag + "'\n";
//        //        }
//        //    }
            

        
//        //}
//    }
//}
