﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using I_XNAUtility;

namespace I_XNAComponent
{
    //public class Distortion
    //{
    //    //Distortion產生distortion map...，深度緩衝去外面找別人共用...
    //    public RenderTarget2D DistortionMap;
        

    //    // Enum describes all the rendering techniques used in this demo.
    //    public enum DemoDistortion
    //    {
    //        Refract2DBoard,
    //        Refract3DBoard,
    //    }

    //    Effect m_DistortMapEffect2D, m_DistortMapEffect3D;
    //    Texture2D waterfallTexture;
    //    Texture2D alphaTexture;
    //    GraphicsDevice m_Device;
    //    //DemoDistortion nowDemoDistortion = DemoDistortion.Refract2DBoard;
    //    Effect m_RenderDistortEffect;

    //    public void Dispose()
    //    {
    //        m_DistortMapEffect2D.Dispose();
    //        m_DistortMapEffect2D = null;

    //        m_DistortMapEffect3D.Dispose();
    //        m_DistortMapEffect3D = null;

    //        m_RenderDistortEffect.Dispose();
    //        m_RenderDistortEffect = null;

    //        DistortionMap.Dispose();
    //        DistortionMap = null;

    //        waterfallTexture.Dispose();
    //        waterfallTexture = null;

    //        alphaTexture.Dispose();
    //        alphaTexture = null;
    //    }

    //    public void Initialize(
    //         GraphicsDevice device, ContentManager content, int targetW, int targetH)
    //    {
    //        m_Device = device;
    //        //nowDemoDistortion = demoDistortion;

    //        SurfaceFormat MapFormat = SurfaceFormat.Color;
    //        DistortionMap = new RenderTarget2D(device, targetW, targetH, 1, MapFormat);

    //        m_RenderDistortEffect = Utility.ContentLoad_Effect(content, "Distortion\\Distort");
    //        m_RenderDistortEffect.CurrentTechnique = m_RenderDistortEffect.Techniques["Distort"];
    //        //m_RenderDistortEffect.Parameters["scale"].SetValue(0.3f);//控制折射程度

    //        waterfallTexture = Utility.ContentLoad_Texture2D(content, "Distortion\\waterfall");
    //        alphaTexture = Utility.ContentLoad_Texture2D(content, "Distortion\\fire");

    //        //if (nowDemoDistortion == DemoDistortion.Refract2DBoard)
    //        {
    //            m_DistortMapEffect2D = Utility.ContentLoad_Effect(content, "Distortion\\refraction2DBoard");

    //            //waterfallTexture = Utility.ContentLoad_Texture2D(content, "Distortion\\waterfall");
    //            //alphaTexture = Utility.ContentLoad_Texture2D(content, "Pic\\fire");
    //            m_DistortMapEffect2D.Parameters["range"].SetValue(1.0f);
    //            m_DistortMapEffect2D.CurrentTechnique = m_DistortMapEffect2D.Techniques["Refraction"];
    //        }
    //        //else if(nowDemoDistortion == DemoDistortion.Refract3DBoard)
    //        {
    //            m_DistortMapEffect3D = Utility.ContentLoad_Effect(content, "Distortion\\refraction3DBoard");

    //            //waterfallTexture = Utility.ContentLoad_Texture2D(content, "Distortion\\waterfall");
    //            //alphaTexture = Utility.ContentLoad_Texture2D(content, "Pic\\fire");


    //            m_DistortMapEffect3D.Parameters["range"].SetValue(5.0f);
    //            m_DistortMapEffect3D.CurrentTechnique = m_DistortMapEffect3D.Techniques["Refraction3DBoard"];
    //        }
    //    }

    //    //public void ClearMap(DepthStencilBuffer depthBuffer)
    //    //{
    //    //    m_Device.SetRenderTarget(0, DistortionMap);
    //    //    m_Device.DepthStencilBuffer = depthBuffer;
    //    //    m_Device.Clear(ClearOptions.Target, Color.TransparentBlack, 1.0f, 0);
    //    //}

    //    public void RenderDistortionMap2DBoard(GameTime gameTime, SpriteBatch spritebatch, int posX, int posY, int w, int h, float range, float speed)
    //    {
    //        //if (nowDemoDistortion == DemoDistortion.Refract2DBoard)
    //        {
    //            Effect m_DistortMapEffect = m_DistortMapEffect2D;

    //            // Begin the sprite batch.
    //            spritebatch.Begin(SpriteBlendMode.AlphaBlend,
    //                              SpriteSortMode.Immediate,
    //                              SaveStateMode.None);

    //            // Set an effect parameter to make the
    //            // displacement texture scroll in a giant circle.
    //            m_DistortMapEffect.Parameters["DisplacementScroll"].SetValue(
    //                                                        MoveInCircle(gameTime, speed));

    //            m_DistortMapEffect.Parameters["range"].SetValue(range);

    //            bool bAlphaMap = false;
    //            m_DistortMapEffect.Parameters["bAlphaTexture"].SetValue(bAlphaMap);
    //            if (bAlphaMap)
    //                m_Device.Textures[1] = alphaTexture;

    //            // Begin the custom effect.
    //            m_DistortMapEffect.Begin();
    //            m_DistortMapEffect.CurrentTechnique.Passes[0].Begin();

    //            // Draw the sprite.
    //            spritebatch.Draw(waterfallTexture, // Set the displacement texture.
    //                new Rectangle(posX - w/2, posY - h/2, w, h),
    //                    // MoveInCircle(gameTime, catTexture, 1),
    //                             Color.White);

    //            // End the sprite batch, then end our custom effect.
    //            spritebatch.End();

    //            m_DistortMapEffect.CurrentTechnique.Passes[0].End();
    //            m_DistortMapEffect.End();

    //            m_Device.Textures[0] = null;
    //            m_Device.Textures[1] = null;
    //        }
    //    }

    //    public void RenderDistortionMap3DBoard(GameTime gameTime, SquareFrame squareFrame, 
    //        ref Matrix mat, ref Matrix view, ref Matrix proj)
    //    {
    //        //if (nowDemoDistortion == DemoDistortion.Refract3DBoard)
    //        {
    //            Effect m_DistortMapEffect = m_DistortMapEffect3D;

    //            m_DistortMapEffect.CurrentTechnique = m_DistortMapEffect.Techniques["Refraction3DBoard"];
    //            // Set an effect parameter to make the
    //            // displacement texture scroll in a giant circle.
    //            m_DistortMapEffect.Parameters["DisplacementScroll"].SetValue(
    //                                                        MoveInCircle(gameTime, 15f));

    //            m_DistortMapEffect.Parameters["WorldViewProjection"].SetValue(mat*view*proj);
    //            m_DistortMapEffect.Parameters["displaceMap"].SetValue(waterfallTexture);

    //            bool bAlphaMap = true;
    //            m_DistortMapEffect.Parameters["bAlphaTexture"].SetValue(bAlphaMap);
    //            if(bAlphaMap)                
    //                m_DistortMapEffect.Parameters["alphaBlendMap"].SetValue(alphaTexture);

    //            //m_DistortMapEffect.CommitChanges();
    //            m_DistortMapEffect.Begin();
    //            m_DistortMapEffect.CurrentTechnique.Passes[0].Begin();

    //            squareFrame.DrawSquareFrame();

    //            m_DistortMapEffect.CurrentTechnique.Passes[0].End();
    //            m_DistortMapEffect.End();

    //        }
    //    }
       

    //    public void RenderWithScene(SpriteBatch spritebatch, Texture2D sceneMap, float distortScale)
    //    {
    //        m_RenderDistortEffect.Parameters["scale"].SetValue(distortScale);//控制折射程度

    //        // draw the scene image again, distorting it with the distortion map
    //        //m_Device.SetRenderTarget(0, m_RecDistortRT);
    //        m_Device.Textures[1] = DistortionMap.GetTexture();

    //        //Viewport viewport = m_Device.Viewport;

    //        DrawFullscreenQuad(sceneMap, sceneMap.Width, sceneMap.Height,
    //            m_RenderDistortEffect, spritebatch);


    //        m_Device.Textures[0] = null;
    //        m_Device.Textures[1] = null;
    //    }

    //    /// <summary>
    //    /// Helper for drawing a texture into the current rendertarget,
    //    /// using a custom shader to apply postprocessing effects.
    //    /// </summary>
    //    void DrawFullscreenQuad(Texture2D texture, int width, int height,
    //                            Effect effect, SpriteBatch spritebatch)
    //    {
    //        spritebatch.Begin(SpriteBlendMode.None,
    //                          SpriteSortMode.Immediate,
    //                          SaveStateMode.None);

    //        // Begin the custom effect, if it is currently enabled. If the user
    //        // has selected one of the show intermediate buffer options, we still
    //        // draw the quad to make sure the image will end up on the screen,
    //        // but might need to skip applying the custom pixel shader.
    //        effect.Begin();
    //        effect.CurrentTechnique.Passes[0].Begin();

    //        // Draw the quad.
    //        spritebatch.Draw(texture, new Rectangle(0, 0, width, height), Color.White);
    //        spritebatch.End();

    //        // End the custom effect.
    //        effect.CurrentTechnique.Passes[0].End();
    //        effect.End();
    //    }
    //    /// <summary>
    //    /// Helper for moving a value around in a circle.
    //    /// </summary>
    //    static Vector2 MoveInCircle(GameTime gameTime, float speed)
    //    {
    //        double time = gameTime.TotalGameTime.TotalSeconds * speed;

    //        float x = (float)Math.Cos(time);
    //        float y = (float)Math.Sin(time);

    //        return new Vector2(x, y);
    //    }

    //}
}