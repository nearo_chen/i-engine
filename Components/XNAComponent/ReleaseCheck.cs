﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using I_XNAUtility;

namespace I_XNAComponent
{
    public static class ReleaseCheck
    {
        public static bool Enable = true;

        static bool bCheckInit = false;
        static Dictionary<Type, CheckList> m_CheckDictionary = null;//new Dictionary<Type, CheckList>();
        static Dictionary<Type, int> m_CheckFinalizeDictionary = null;//new Dictionary<Type, int>();
        static StringBuilder m_StringBuilder = null;//new StringBuilder(1024);
        static  void CheckInit()
        {
            if (bCheckInit)
                return;
            bCheckInit = true;
            m_CheckDictionary = new Dictionary<Type, CheckList>();
            m_CheckFinalizeDictionary = new Dictionary<Type, int>();
            m_StringBuilder = new StringBuilder(1024);
        }

        #region 管理生出來的node

        /// <summary>
        /// 加入別人創造出的物件，監控他是否最後有呼叫Dispose。
        /// </summary>
        static public void AddOtherNew(object ooo)
        {
            if (Enable == false)
                return;
            CheckInit();

            Type type = ooo.GetType();
            CheckList checkList;
            if (m_CheckDictionary.TryGetValue(type, out checkList) == false)
            {
                checkList = new CheckList();
                m_CheckDictionary.Add(type, checkList);
            }
            checkList.AddCheckNode(ooo);
        }

        /// <summary>
        /// 建立Scene Node都要用這個管理，
        /// 幫助建立物件，但其實不需要這樣生，只要於自己class將建構子private，並且寫一個state function於class內，
        /// 專門Create()，再將建立物件加入ReleaseCheck.AddCheck()也是可。
        /// </summary>  
        static public object CreateNew(Type type)
        {
            object ooo = null;
            //if (type == typeof(MeshNode))
            //    ooo = new MeshNode();
            if (type == typeof(GrassNode))
                ooo = new GrassNode();
            else if (type == typeof(LocatorNode))
                ooo = new LocatorNode();

            else if (type == typeof(SkinningMeshNode))
                ooo = new SkinningMeshNode();
            else if (type == typeof(SoftwareSkinningMeshNode))
                ooo = new SoftwareSkinningMeshNode();
            else if (type == typeof(ShadowVolumeNode))
                ooo = new ShadowVolumeNode();
            else if (type == typeof(InstanceSimpleMeshNode))
                ooo = new InstanceSimpleMeshNode();



            else if (type == typeof(SceneMeshLoader))
                ooo = new SceneMeshLoader();
            else if (type == typeof(SkinningMeshLoader))
                ooo = new SkinningMeshLoader();
            //else if (type == typeof(KeyframeMeshLoader))
            //    ooo = new KeyframeMeshLoader();
            else if (type == typeof(KeyframeMeshLoader))
                ooo = new KeyframeMeshLoader();


            //else if (type == typeof(KeyFrameMesh2))
            //    ooo = new KeyFrameMesh2();

            //else if (type == typeof(DepthMapCreator))
            //    ooo = new DepthMapCreator();
            
            if (Enable)
               AddCheck(ooo);
            //if (Enable == false)
            //    return ooo;
            //CheckInit();

            ////#if DrawDebug
            //CheckList checkList;
            //if (m_CheckDictionary.TryGetValue(type, out checkList) == false)
            //{
            //    checkList = new CheckList();
            //    m_CheckDictionary.Add(type, checkList);
            //}
            //checkList.AddCheckNode(ooo);

            //int count = 0;
            //if (m_CheckFinalizeDictionary.TryGetValue(type, out count) == false)
            //{
            //    count = 0;
            //    m_CheckFinalizeDictionary.Add(type, count);
            //}
            ////#endif
            return ooo;
        }

        /// <summary>
        /// 加入有解構子的物件，於釋放scene之後，可監控自己寫的class，是否有跑Dispose()，和解構子。
        /// </summary>
        /// <param name="ooo"></param>
        public static void AddCheck(object ooo)
        {
            CheckInit();

            //#if DrawDebug
            CheckList checkList;
            if (m_CheckDictionary.TryGetValue(ooo.GetType(), out checkList) == false)
            {
                checkList = new CheckList();
                m_CheckDictionary.Add(ooo.GetType(), checkList);
            }
            checkList.AddCheckNode(ooo);

            int count = 0;
            if (m_CheckFinalizeDictionary.TryGetValue(ooo.GetType(), out count) == false)
            {
                count = 0;
                m_CheckFinalizeDictionary.Add(ooo.GetType(), count);
            }
            //#endif
        }

        //#if DrawDebug

        //public static int MeshNodeCount, SkinningMeshNodeCount, SoftwareSkinningMeshNodeCount, InstanceSimpleMeshNodeCount,
        //    ShadowVolumeNodeCount, SceneMeshLoaderCount, SkinningMeshLoaderCount,
        //    KeyFrameMesh2Count;


        //static public CheckList GetNodeList(Type type)
        //{
        //    CheckList checkList = null;
        //    m_CheckDictionary.TryGetValue(type, out checkList);
        //    return checkList;
        //}

        //static public int GetFinalizeTypeCount(Type type)
        //{
        //    int count = 99999;
        //    m_CheckFinalizeDictionary.TryGetValue(type, out count);
        //    return count;
        //}

        /// <summary>
        /// NODE資源檢查歸0初始化
        /// </summary>
        static public void ClearFinalizeCheck()
        {
            if (Enable == false)
                return;
            CheckInit();

            m_CheckFinalizeDictionary.Clear();

            //MeshNodeCount = SkinningMeshNodeCount = SoftwareSkinningMeshNodeCount =
            //InstanceSimpleMeshNodeCount = ShadowVolumeNodeCount = SceneMeshLoaderCount =
            //SkinningMeshLoaderCount = KeyFrameMesh2Count = 0;
        }



        /// <summary>
        /// 放在Dispose裡面將指標釋放
        /// </summary>
        static public void DisposeCheck(object obj)
        {
            if (Enable == false)
                return;
            CheckInit();

            CheckList checkList = m_CheckDictionary[obj.GetType()];
            checkList.ReleaseCheckNode(obj);
        }

        /// <summary>
        /// 放在解構子裡面計算該型態的NODE是否釋放了
        /// </summary>
        static public void DisposeCheckCount(object obj)
        {
            if (Enable == false)
                return;
            CheckInit();

            Type type = obj.GetType();
            //if (type == typeof(MeshNode))
            //    MeshNodeCount++;
            //else if (type == typeof(SkinningMeshNode))
            //    SkinningMeshNodeCount++;
            //else if (type == typeof(SoftwareSkinningMeshNode))
            //    SoftwareSkinningMeshNodeCount++;
            //else if (type == typeof(ShadowVolumeNode))
            //    ShadowVolumeNodeCount++;
            //else if (type == typeof(InstanceSimpleMeshNode))
            //    InstanceSimpleMeshNodeCount++;
            //else if (type == typeof(SceneMeshLoader))
            //    SceneMeshLoaderCount++;
            //else if (type == typeof(SkinningMeshLoader))
            //    SkinningMeshLoaderCount++;
            //else if (type == typeof(KeyFrameMesh2))
            //    KeyFrameMesh2Count++;                                                                                                                                                                                                                                                                                                                                                                   

            //#if DrawDebug
            m_CheckFinalizeDictionary[type]++;
            //#endif
        }


        /// <summary>
        /// 取得ReleaseCheck管理的資源數量資訊
        /// </summary>   
        static public string GetInformationString()
        {
            CheckInit();

            m_StringBuilder.Remove(0, m_StringBuilder.Length);

            //string debugString=null;
            foreach (Type type in m_CheckDictionary.Keys)
            {
                CheckList checkList = m_CheckDictionary[type];
                int surplus;
                if (m_CheckFinalizeDictionary.TryGetValue(type, out surplus))
                {
                    //m_CheckFinalizeDictionary[type];
                    //debugString += type.ToString() + ": " + checkList.GetListAmount().ToString() +
                    //    "   destructor count: " + surplus.ToString() + "\n"; //秀出node剩幾個

                    m_StringBuilder.AppendFormat("{0} : {1} destructor :{2} \n",
                        type.ToString(), checkList.GetListAmount().ToString(), surplus.ToString());
                }
                else
                {
                    //debugString += type.ToString() + ": " + checkList.GetListAmount().ToString() +
                    //    "   destructor count: No Finalize Info~ \n"; //秀出node剩幾個

                    m_StringBuilder.AppendFormat("{0} : {1}   destructor count: No Finalize Info~ \n",
                        type.ToString(), checkList.GetListAmount().ToString());
                }
            }


            return m_StringBuilder.ToString();// debugString;
        }
        //#endif
        #endregion

        //static public void DisposeAnything(ref System.IDisposable dispose)
        //{
        //    ReleaseCheck.DisposeCheckCount(dispose);

        //    if (dispose != null)
        //        dispose.Dispose();
        //    dispose = null;
        //}

        //static public void DisposeAnything(ref StillDesign.PhysX.IDisposable dispose)
        //{
        //    ReleaseCheck.DisposeCheckCount(dispose);

        //    if (dispose != null)
        //        dispose.Dispose();
        //    dispose = null;
        //}
    }
}
