﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using I_XNAUtility;
using MeshDataDefine;


namespace I_XNAComponent
{
    public class SkinningMesh : AnimationPlayerManager
    {
        //AnimationPlayerManager m_AnimationPlayerManager;
        // const int iMaxBones = 25;

        Model m_Model;
        Matrix[] absolute;

        //int m_iAnimationAmount;
        int m_iBoneAmount;
        SkinningMeshData m_SkinningMeshData;
        //AnimationPlayer[] m_AnimationControllers; //animation controller for each animation...
        //AnimationPlayer m_ActiveAnimationColtroller; //now actived animation...
        //Matrix[] m_SkinningAnimatingMatrixss;//used for animating bone transform calculate, on time...

        //Matrix[][] m_MeshSplitAnimateArray = null;//紀錄模型骨架太多分裂的mesh及它們的bone array
        //Matrix [] animateMatrixArray;//用來暫存回傳用
        Texture2D[][] m_MeshPartTextureArray = null;
        LocatorNode[] m_BoneLocatorNodeTreeArray = null;//儲存建立骨架樹狀結構的node陣列，他們在陣列裡是有父子attach關係的。

        public SkinningMesh()
        {
            //m_ActiveAnimationColtroller = null;
        }
        ~SkinningMesh()
        {
            Dispose();
        }

        //public Dictionary<string, object> GetModelTag
        //{
        //    get { return m_Model.Tag as Dictionary<string, object>; }
        //}

        public ModelMesh GetModelMesh(int meshID)
        {
            return m_Model.Meshes[meshID];
        }

        public ModelMesh GetModelMesh(string meshName)
        {
            foreach (ModelMesh modelMesh in m_Model.Meshes)
            {
                if (modelMesh.Name.Contains(meshName))
                    return modelMesh;
            }
            return null;
        }

        /// <summary>
        /// 建立並取得模型裡的texture
        /// </summary>      
        public Texture2D GetModelMeshPartTexture(int meshID, int meshPartID)
        {
            if (m_MeshPartTextureArray == null)
            {
                m_MeshPartTextureArray = new Texture2D[m_Model.Meshes.Count][];
                for (int a = 0; a < m_MeshPartTextureArray.Length; a++)
                {
                    m_MeshPartTextureArray[a] = new Texture2D[m_Model.Meshes[a].MeshParts.Count];
                    for (int b = 0; b < m_MeshPartTextureArray[a].Length; b++)
                        m_MeshPartTextureArray[a][b] = m_Model.Meshes[a].MeshParts[b].Effect.Parameters["Texture"].GetValueTexture2D();
                }
            }
            return m_MeshPartTextureArray[meshID][meshPartID];
        }

        override public void Dispose()
        {
            base.Dispose();

            m_Model = null;
            absolute = null;

            m_BoneLocatorNodeTreeArray = null;

            if (m_MeshPartTextureArray != null)
            {
                for (int a = 0; a < m_MeshPartTextureArray.Length; a++)
                {
                    for (int b = 0; b < m_MeshPartTextureArray[a].Length; b++)
                    {
                        //m_MeshPartTextureArray[a][b].Dispose();
                        m_MeshPartTextureArray[a][b] = null;
                    }
                    m_MeshPartTextureArray[a] = null;
                }
                m_MeshPartTextureArray = null;
            }

            //if (m_AnimationPlayerManager != null)
            //{
            //    m_AnimationPlayerManager.Dispose();
            //    m_AnimationPlayerManager = null;
            //}
        }

        public AnimationTableDataArray LoadContent(string modelname, ContentManager content, string animationXML)
        {
            m_Model = Utility.ContentLoad_Model(content, modelname);
            if (m_Model == null)
                return null;
            absolute = Utility.GetMatrixArray(m_Model.Bones.Count);
            m_Model.CopyAbsoluteBoneTransformsTo(absolute);

            // Get the dictionary
            Dictionary<string, object> modelTag = (Dictionary<string, object>)m_Model.Tag;
            if (modelTag == null)
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage("This is not a valid animated skinning model. ==>" + animationXML);
            }

            // Get the AnimatedModelData from the dictionary
            if (modelTag.ContainsKey(MeshKeys.SkinningMeshKey))
                m_SkinningMeshData = (SkinningMeshData)modelTag[MeshKeys.SkinningMeshKey];
            else
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage("This is not a valid animated model. ==> " + animationXML);
            }

            //m_AnimationPlayerManager = new AnimationPlayerManager();
            //AnimationTableDataArray ooo = m_AnimationPlayerManager.LoadAnimationXML(m_SkinningMeshData, animationXML);
            AnimationTableDataArray ooo = LoadAnimationXML(m_SkinningMeshData, animationXML);

            m_iBoneAmount = m_SkinningMeshData.BonesParent.Length;

            return ooo;

            //Create Amimation controller for each animation....
            //if (AnimationDataList == null)//沒有animationTable就全部從頭到尾播放
            //{
            //    m_iAnimationAmount = 1;// m_AnimatedMeshData.Animations.Length;

            //    m_AnimationControllers = new AnimationPlayer[m_iAnimationAmount];
            //  //  for (int i = 0; i < m_AnimationControllers.Length; i++)
            //    {                    
            //        m_AnimationControllers[0] = new AnimationPlayer(m_AnimatedMeshData.Animations[0],
            //            (uint)m_AnimatedMeshData.Animations[0].Duration.TotalMilliseconds,                
            //            m_AnimatedMeshData.Animations[0].Name, true);
            //    }
            //}
            //else
            //{
            //    m_iAnimationAmount = AnimationDataList.Length;
            //    m_AnimationControllers = new AnimationPlayer[m_iAnimationAmount];
            //    for (int i = 0; i < m_AnimationControllers.Length; i++)
            //    {
            //        m_AnimationControllers[i] = new AnimationPlayer(
            //            m_AnimatedMeshData.Animations[0], //都固定是Animations[0]，因為沒法很多Animations在一個檔案裡，幻動作只能用跳ket frame的方法。
            //            AnimationDataList[i].StartKey,
            //            AnimationDataList[i].EndKey,
            //            AnimationDataList[i].ActionName, true);
            //    }
            //}


            ////m_SkinningAnimatingMatrixss = new Matrix[m_iBoneAmount];
            //m_SkinningAnimatingMatrixss = Utility.GetMatrixArray(m_iBoneAmount);
            //m_ActiveAnimationColtroller = m_AnimationControllers[0];
            //SetAnimationPlay(0, false, 1, 0);


            ////設定模型分裂要怎麼畫的資料
            //if (m_AnimatedMeshData.SkinningMeshSplitTable != null)
            //{
            //    m_MeshSplitAnimateArray = new Matrix[ModelSplitAmount()][];
            //    for (int i = 0; i < m_MeshSplitAnimateArray.Length; i++)
            //        m_MeshSplitAnimateArray[i] = Utility.GetMatrixArray(m_AnimatedMeshData.SkinningMeshSplitTable[i].Length);
            //        //m_MeshSplitAnimateArray[i] = new Matrix[m_AnimatedMeshData.SkinningMeshSplitTable[i].Length];
            //}
        }

        //public bool IfAnimationPlay(int animation)
        //{
        //    return m_AnimationPlayerManager.IfAnimationPlay(animation);
        //}

        //public void SetAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent)
        //{
        //    m_AnimationPlayerManager.SetAnimationPlay(iAnimationID, bLoop, speed, startPercent);
        //}

        //public void SetAnimationStop(int iAnimationID)
        //{
        //    m_AnimationPlayerManager.SetAnimationStop(iAnimationID);
        //}

        //public void AllAnimationStop()
        //{
        //    m_AnimationPlayerManager.AllAnimationStop();
        //}

        //public void AnimatedWithRatio(int iAnimationID, float ratio)
        //{
        //    m_AnimationPlayerManager.AnimatedWithRatio(iAnimationID, ratio);
        //}


        //public void SetBlendAnimationPlay(int iAnimationID, bool bLoop, float speed, float startPercent)
        //{
        //    m_AnimationPlayerManager.SetBlendAnimationPlay(iAnimationID, bLoop, speed, startPercent);
        //}

        //public void SetBlendAnimationPlay_BlendSwitch(int iAnimationID, bool bLoop, float speed, float startPercent,
        //    int switchTime)
        //{
        //    m_AnimationPlayerManager.SetAnimationPlay_BlendSwitch(iAnimationID, bLoop, speed, startPercent, switchTime);
        //}

        //public void SetBlendAnimationRatio(float BlendRatio)
        //{
        //    m_AnimationPlayerManager.SetBlendAnimationRatio(BlendRatio);
        //}


        //public bool IfBlendAnimationPlay()
        //{
        //    if (m_AnimationPlayerBlend == null)
        //        return false;
        //    return m_AnimationPlayerBlend.IfAnimationPlay();
        //}

        public void UpdateSkinningTransform(int EllapseMillisecond, Matrix worldTransform)
        {
            UpdateAnimateKeyFrameTransform(EllapseMillisecond);
            UpdateSkinningKeyFrameTransform(ref worldTransform);

            //更新樹狀結構的matrix
            if (m_BoneLocatorNodeTreeArray != null)
            {
                m_BoneLocatorNodeTreeArray[0].LocalMatrix = //worldTransform * 
                    GetBoneAnimateLocalMatrix(0);
                for (int boneID = 1; boneID < m_BoneLocatorNodeTreeArray.Length; boneID++)
                    m_BoneLocatorNodeTreeArray[boneID].LocalMatrix = GetBoneAnimateLocalMatrix(boneID);
            }
        }

        /// <summary>
        /// 取得目前骨架動畫Model底下，第幾個mesh對應的bone array，
        /// 因為骨架太多，會把模型細分為幾個mesh。
        /// 若是模型分裂，那他們bone array也會分開。
        /// </summary>
        //public Matrix[] GetSkinningBoneArray(int meshID)
        //{
        //    return m_AnimationPlayerManager.GetSkinningBoneArray(meshID);
        //}

        /// <summary>
        /// 將傳入mesh根據bone array畫出骨架模型。
        /// </summary>
        public void RenderMeshPart(MeshNode.RenderMeshPartData renderMeshPart, GraphicsDevice device)
        {
            device.Indices = renderMeshPart.m_IndexBuffer;
            device.VertexDeclaration = renderMeshPart.m_VDec;

            // Set vertex buffer and index buffer
            device.Vertices[0].SetSource(
               renderMeshPart.m_VB, renderMeshPart.m_StreamOffset,
                renderMeshPart.m_VertexStride);

            // And render all primitives
            device.DrawIndexedPrimitives(
                PrimitiveType.TriangleList,
                renderMeshPart.m_BaseVertex, 0, renderMeshPart.m_NumVertices,
                renderMeshPart.m_StartIndex, renderMeshPart.m_PrimitiveCount);

            device.Vertices[0].SetSource(null, 0, 0);
            device.VertexDeclaration = null;
            device.Indices = null;
        }

        /// <summary>
        /// 取得模型因為骨架太多分裂的數量
        /// </summary>
        /// <returns></returns>
        public int ModelSplitAmount()
        {
            if (m_SkinningMeshData.SkinningMeshSplitTable == null)
                return 1;
            return m_SkinningMeshData.SkinningMeshSplitTable.Length;
        }

        //public Matrix GetSkinningAnimatingBone(int iiid)
        //{
        //    return absolute[iiid] * m_SkinningAnimatingMatrixss[iiid];
        //}

        public Matrix GetParentAbsolute(int meshID)
        {
            int bondID = m_Model.Meshes[meshID].ParentBone.Index;
            //int bondID = m_SkinningMeshData.SkinningMeshSplitTable[meshID][0];
            return absolute[bondID];
        }

        /// <summary>
        /// 由骨架動態資料陣列取得該骨架MATRIX索引位址。
        /// </summary>
        public int GetBoneMatrixID(string boneName)
        {
            for (int i = 0; i < m_SkinningMeshData.Animations[0].BoneKeyframes.Length; i++)
            {
                if (m_SkinningMeshData.Animations[0].BoneKeyframes[i].BoneName == boneName)
                    return i;
            }
            return -1;
        }

        /// <summary>
        /// 根據骨架結構建立樹狀LOCATOR NODE資訊。
        /// </summary>
        public void CreateBoneTree(out LocatorNode parent)
        {
            if (m_BoneLocatorNodeTreeArray == null)
                m_BoneLocatorNodeTreeArray = new LocatorNode[m_iBoneAmount];

            for (int boneID = 0; boneID < m_iBoneAmount; boneID++)
            {
                m_BoneLocatorNodeTreeArray[boneID] = LocatorNode.Create();
                m_BoneLocatorNodeTreeArray[boneID].NodeName = m_SkinningMeshData.Animations[0].BoneKeyframes[boneID].BoneName;

                int parentID = m_SkinningMeshData.BonesParent[boneID];
                if (parentID != -1)
                {
                    m_BoneLocatorNodeTreeArray[m_SkinningMeshData.BonesParent[boneID]].AttachChild(
                        m_BoneLocatorNodeTreeArray[boneID]);
                }
            }
            parent = m_BoneLocatorNodeTreeArray[0];
        }

        /// <summary>
        /// 用線條畫出骨架
        /// </summary>
        /// <param name="radius"></param>
        public void RenderDebug(float radius)
        {
            for (int boneID = 0; boneID < m_iBoneAmount; boneID++)
            {
                DrawLine.AddSphereLine(m_BoneLocatorNodeTreeArray[boneID].WorldMatrix, Vector3.Zero, radius, Color.Red, 10, 10);
                int parentID = m_SkinningMeshData.BonesParent[boneID];
                if (parentID != -1)
                    DrawLine.AddLine(m_BoneLocatorNodeTreeArray[boneID].WorldMatrix.Translation,
                        m_BoneLocatorNodeTreeArray[parentID].WorldMatrix.Translation, Color.White);
            }
        }
    }

}