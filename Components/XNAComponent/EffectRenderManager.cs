﻿
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using System.IO;
using I_XNAUtility;

namespace I_XNAComponent
{
    /// <summary>
    /// 控制Effect參數，以及繪圖。
    /// </summary>
    public partial class EffectRenderManager : IDisposable
    {
        public const int ShaderMaxSpotLight = 1;
        Effect m_RenderEffect;
        SceneNode.RenderTechnique m_CurrentRenderTechnique;
        public SceneNode.RenderTechnique CurrentRenderTechnique { get { return m_CurrentRenderTechnique; } }

        public EffectRenderManager()
        {
        }

        public void Dispose()
        {
            m_RenderEffect.Dispose();
            m_RenderEffect = null;
        }

        public void LoadEffect(ContentManager content, string pathName)
        {
            m_RenderEffect = Utility.ContentLoad_Effect(content, pathName);
            GetEffectPar();
        }




        //public void SetRenderBegin(SceneNode.RenderTechnique currentTechnique)
        //{
        //    m_CurrentRenderTechnique = currentTechnique;
        //    SetCurrentTechnique(currentTechnique);

        //    m_RenderEffect.Begin();
        //    m_RenderEffect.CurrentTechnique.Passes[0].Begin();
        //}

        /// <summary>
        /// 設定CurrentTechnique.Passes[0].End()還有Effect.End();
        /// </summary>
        public void SetRenderEnd()
        {
            m_RenderEffect.CurrentTechnique.Passes[0].End();
            m_RenderEffect.End();
        }



        //void SetCurrentTechnique(SceneNode.RenderTechnique NowTechniques)
        //{
        //    string EffectString = string.Empty;
        //    if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.SimpleMesh))
        //    {
        //        EffectString += "SimpleMesh";
        //    }
        //    else if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.SkinnedMesh))
        //    {
        //        EffectString += "SkinnedMesh";
        //    }
        //    else if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.SimpleInstanceMesh))
        //    {
        //        EffectString += "InstanceSimpleMesh";
        //    }
        //    else if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.GrassMesh))
        //    {
        //        EffectString += "GrassMesh";
        //    }

        //    if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.TengentMesh))
        //    {
        //        EffectString += "_TengentMesh";
        //    }

        //    //if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.ShadowMap))
        //    //{
        //    //    EffectString += "_ShadowMap";
        //    //}

        //    if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.EmmisiveMap))
        //    {
        //        EffectString += "_EmmisiveMap";
        //    }

        //    if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.GodsRayMap))
        //    {
        //        EffectString += "_GodsRayMap";
        //    }

        //    if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.NormalRefractMap))
        //    {
        //        EffectString += "_NormalRefractMap";
        //    }

        //    if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.DepthMap))
        //    {
        //        EffectString += "_DepthMap";
        //    }

        //    if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.ShadowVolume))
        //    {
        //        EffectString += "_ShadowVolume";
        //    }

        //    if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.ScreenVelocityMap))
        //    {
        //        EffectString += "_ScreenVelocityMap";
        //    }

        //    if (MyMath.ConsiderBit((uint)NowTechniques, (uint)MeshNode.RenderTechnique.EmmisiveOnly))
        //    {
        //        EffectString += "_EmmisiveOnly";
        //    }

        //    if (string.IsNullOrEmpty(EffectString))
        //        m_RenderEffect.CurrentTechnique = null;
        //    else
        //        m_RenderEffect.CurrentTechnique = m_RenderEffect.Techniques[EffectString];
        //}

        //Effect傳入參數ˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇ
        EffectParameter parWorld = null;
        EffectParameter parView = null;
        EffectParameter parProjection = null;
        EffectParameter parViewPosition = null;
        EffectParameter parNormalMap = null;
        EffectParameter parBaseTexture = null;
        EffectParameter parAlphaTestReference = null;
        EffectParameter parbTexture = null;
        EffectParameter parEnvCubeMap = null;
        EffectParameter parbEnvCubeMap = null;
        EffectParameter parbEmmisiveMap = null;
        EffectParameter parEmmisiveMap = null;
        EffectParameter parEmmisiveAlpha = null;
        EffectParameter parObjectAlpha = null;
        EffectParameter parBlendColor = null;

        EffectParameter parFogStart = null;
        EffectParameter parFogEnd = null;
        EffectParameter parFogMaxRatio = null;
        EffectParameter parFogColor = null;
        EffectParameter parbFogEnable = null;
        EffectParameter parLightEnable = null;
        //EffectParameter parMipmapEnable = null;
        EffectParameter parLightDirection = null;
        EffectParameter parMaterial_a = null;
        EffectParameter parMaterial_d = null;
        EffectParameter parMaterial_s = null;
        EffectParameter parDLight_a = null;
        EffectParameter parDLight_d = null;
        EffectParameter parDLight_s = null;
        EffectParameter parMaterialSPower = null;
        EffectParameter parTranslateUVSinLoopSpeed = null;
        EffectParameter parTranslateUVSpeed = null;
        EffectParameter parCenterScaleUV = null;
        EffectParameter parTotalEllapsedMilliseonds = null;

        EffectParameter parMaterialBankID = null;

        EffectParameter parbReflectPowerMap = null;
        EffectParameter parReflectPowerMap = null;
        EffectParameter parFresnelPower = null;
        EffectParameter parbSpecularMap = null;
        EffectParameter parSpecularMap = null;

        EffectParameter parDepthMapFrustum = null;

        EffectParameter parBigShadowMapRatio = null;
        EffectParameter parBigShadowMapBias = null;
        //EffectParameter parBigShadowMapDepthBias = null;
        EffectParameter parbBigShadowMap = null;
        EffectParameter parBigShadowMapFrustum = null;
        EffectParameter parBigShadowMap = null;
        EffectParameter parBigShadowMapMultiSampleOffset = null;

        EffectParameter parSmallShadowMapRatio = null;
        EffectParameter parSmallShadowMapBias = null;
        EffectParameter parbSmallShadowMap = null;
        EffectParameter parSmallShadowMapFrustum = null;
        EffectParameter parSmallShadowMap = null;
        EffectParameter parSmallShadowMapMultiSampleOffset = null;

        EffectParameter parBigShadowSampleLength = null;
        EffectParameter parSmallShadowSampleLength = null;

        EffectParameter parIfRenderGodsRay = null;


        //EffectParameter parmWorldViewProjectionLast = null;
        //EffectParameter parbPowerTexture = null;
        //EffectParameter parPowerTexture = null;

        EffectParameter parbReflectMap = null;
        EffectParameter parReflectIntensity = null;
        EffectParameter parReflectionView = null;
        EffectParameter parReflectionMap = null;
        EffectParameter parRefractMapScale = null;

        //EffectParameter parPointLightArray = null;
        //struct PointLightPar
        //{
        //    public EffectParameter position;
        //    public EffectParameter color;
        //    public EffectParameter decay;
        //    public EffectParameter range;
        //    public EffectParameter strength;
        //    public EffectParameter specularIntensity;
        //    public Vector3 oldPos, oldColor;
        //    public float oldDecay, oldRange, oldStrength, oldSpecularIntensity;
        //}
        //PointLightPar[] parPointLightParArray = new PointLightPar[SceneNode.ShaderMaxPointLight];
        //EffectParameter parPointLightAmount = null;

        //EffectParameter parSpotLightArray = null;
        //struct SpotLightPar
        //{
        //    public EffectParameter position;
        //    public EffectParameter direction;
        //    public EffectParameter angleTheta;
        //    public EffectParameter anglePhi;
        //    public EffectParameter strength;
        //    public EffectParameter decay;
        //    public EffectParameter color;
        //    public EffectParameter range;
        //    public Vector3 oldPos, oldColor, oldDir;
        //    public float oldDecay, oldRange, oldStrength, oldAngleTheta, oldAnglePhi;
        //}
        //SpotLightPar[] parSpotLightParArray = new SpotLightPar[ShaderMaxSpotLight];
        //EffectParameter parSpotLightAmount = null;

        EffectParameter parNextTexturePercent = null;
        EffectParameter parNextTexture = null;

        EffectParameter parBoneArray = null;

        EffectParameter parShadowVolumeLightDirection = null;

        EffectParameter parWindDirection = null;
        EffectParameter parWindWaveSize = null;
        EffectParameter parWindAmount = null;
        //EffectParameter parRootHeight = null;
        EffectParameter parTreeHeight = null;
        EffectParameter parWindSpeed = null;

        EffectParameter parGrassWindRandomness = null;
        EffectParameter parGrassWindSpeed = null;
        EffectParameter parGrassBillboardWidth = null;
        //EffectParameter parGrassBillboardHeight = null;    

        EffectTechnique //m_SimpleTec,
                        m_NormalMapTec,
                        m_ScreenV_NormalMapTec,
                        m_ScreenV_GodsRay_NormalMapTec,
                        m_ScreenV_GodsRay_Emmisive_NormalMapTec,
                        m_Emmisive_NormalMapTec,

                        m_DrawWithShadowMapTec,
                        m_ScreenV_DrawWithShadowMapTec,
                        m_ScreenV_GodsRay_DrawWithShadowMapTec,
                        m_ScreenV_GodsRay_Emmisive_DrawWithShadowMapTec,
                        m_Emmisive_DrawWithShadowMapTec,

                        m_ScreenV_GodsRay_Emmisive_NormalMapSkinned,
                        m_ScreenV_GodsRay_SkinnedDrawWithShadowMap,
                        m_Emmisive_NormalMapSkinned,
                        m_Emmisive_SkinnedDrawWithShadowMap,
                        m_NormalMapSkinned,
                        m_SkinnedDrawWithShadowMap,

                        m_HardwareInstancing;

        void GetEffectPar()
        {
            if (parWorld != null)
                return;
            if (m_RenderEffect == null)
                return;
            Effect effect = m_RenderEffect;

            parWorld = effect.Parameters["WorldMatrix"];
            oldWorld = parWorld.GetValueMatrix();

            parView = effect.Parameters["ViewMatrix"];
            oldView = parView.GetValueMatrix();

            parProjection = effect.Parameters["ProjectionMatrix"];
            if (parProjection != null)
                oldProjection = parProjection.GetValueMatrix();

            parViewPosition = effect.Parameters["ViewPosition"];

            parNormalMap = effect.Parameters["NormalMap"];
            if (parNormalMap != null)
                oldNormalTexture = parNormalMap.GetValueTexture2D();

            parbTexture = effect.Parameters["bTexture"];
            oldHasTexture = parbTexture.GetValueBoolean();

            parBaseTexture = effect.Parameters["Texture"];
            if (parBaseTexture != null)
                oldBaseTexture = parBaseTexture.GetValueTexture2D();

            parAlphaTestReference = effect.Parameters["AlphaTestReference"];
            if (parAlphaTestReference != null)
                oldAlphaTestReference = parAlphaTestReference.GetValueSingle();

            parNextTexturePercent = effect.Parameters["NextTexturePercent"];
            if (parNextTexturePercent != null)
                oldNextTexturePercent = parNextTexturePercent.GetValueSingle();

            parNextTexture = effect.Parameters["NextTexture"];
            if (parNextTexture != null)
                oldNormalTexture = parNextTexture.GetValueTexture2D();


            parFogStart = effect.Parameters["FogStart"];
            parFogEnd = effect.Parameters["FogEnd"];
            parFogMaxRatio = effect.Parameters["FogMaxRatio"];
            parFogColor = effect.Parameters["FogColor"];

            parbFogEnable = effect.Parameters["bFogEnable"];
            if (parbFogEnable != null)
                oldbFogEnable = parbFogEnable.GetValueBoolean();

            //parMipmapEnable = effect.Parameters["bMipmapEnable"];
            //oldMipMapEnable = parMipmapEnable.GetValueBoolean();

            parLightEnable = effect.Parameters["bLightEnable"];
            if (parLightEnable != null)
                oldLightEnable = parLightEnable.GetValueBoolean();

            parLightDirection = effect.Parameters["DLightDirection"];
            if (parLightDirection != null)
                oldLightDirection = parLightDirection.GetValueVector3();

            parMaterial_a = effect.Parameters["Material_a"];
            if (parMaterial_a != null)
                oldLightAmbient = parMaterial_a.GetValueVector3();

            parMaterial_d = effect.Parameters["I_Material_d"];
            if (parMaterial_d != null)
                oldLightDiffuse = parMaterial_d.GetValueVector3();

            parMaterial_s = effect.Parameters["I_Material_s"];
            if (parMaterial_s != null)
                oldLightSpecular = parMaterial_s.GetValueVector3();

            parDLight_a = effect.Parameters["DLight_a"];
            if (parDLight_a != null)
                oldMaterialAmbient = parDLight_a.GetValueVector3();

            parDLight_d = effect.Parameters["DLight_d"];
            if (parDLight_d != null)
                oldMaterialDiffuse = parDLight_d.GetValueVector3();

            parDLight_s = effect.Parameters["DLight_s"];
            if (parDLight_s != null)
                oldMaterialSpecular = parDLight_s.GetValueVector3();

            parMaterialSPower = effect.Parameters["MaterialSPower"];
            if (parMaterialSPower != null)
                oldMaterialPower = parMaterialSPower.GetValueInt32();

            parTranslateUVSinLoopSpeed = effect.Parameters["TranslateUVSinLoopSpeed"];
            if (parTranslateUVSinLoopSpeed != null)
                oldTranslateUVSinLoopSpeed = parTranslateUVSinLoopSpeed.GetValueVector2();

            parTranslateUVSpeed = effect.Parameters["TranslateUVSpeed"];
            if (parTranslateUVSpeed != null)
                oldTranslateUVSpeed = parTranslateUVSpeed.GetValueVector2();

            parCenterScaleUV = effect.Parameters["centerScaleUV"];
            if (parCenterScaleUV != null)
                oldCenterScaleUV = parCenterScaleUV.GetValueVector3();

            parTotalEllapsedMilliseonds = effect.Parameters["TotalEllapsedMilliseonds"];
            if (parTotalEllapsedMilliseonds != null)
                oldTotalEllapseMillisecond = parTotalEllapsedMilliseonds.GetValueSingle();

            parMaterialBankID = effect.Parameters["MaterialBankID"];
            if (parMaterialBankID != null)
                oldMaterialBankID = parMaterialBankID.GetValueSingle();


            parReflectIntensity = effect.Parameters["m_ReflectIntensity"];
            if (parReflectIntensity != null)
                oldReflectIntensity = parReflectIntensity.GetValueSingle();

            parbReflectPowerMap = effect.Parameters["bReflectPowerMap"];
            if (parbReflectPowerMap != null)
                oldbReflectPowerMap = parbReflectPowerMap.GetValueBoolean();

            parReflectPowerMap = effect.Parameters["ReflectIntensityMap"];
            if (parReflectPowerMap != null)
                oldReflectPowerMap = parReflectPowerMap.GetValueTexture2D();


            parbSpecularMap = effect.Parameters["bSpecularMap"];
            if (parbSpecularMap != null)
                oldbSpecularMap = parbSpecularMap.GetValueBoolean();

            parSpecularMap = effect.Parameters["SpecularMap"];
            if (parSpecularMap != null)
                oldSpecularMap = parSpecularMap.GetValueTexture2D();

            parDepthMapFrustum = effect.Parameters["CreateDepthMapFrustum"];
            if (parDepthMapFrustum != null)
                oldDepthMapFrustum = parDepthMapFrustum.GetValueMatrix();

            parbEnvCubeMap = effect.Parameters["bEnvCubeMap"];
            if (parbEnvCubeMap != null)
                oldbEnvCubeMap = parbEnvCubeMap.GetValueBoolean();

            parEnvCubeMap = effect.Parameters["EnvCubeMap"];
            if (parEnvCubeMap != null)
                oldEnvCubeMap = parEnvCubeMap.GetValueTextureCube();

            parFresnelPower = effect.Parameters["FresnelPower"];
            if (parFresnelPower != null)
                oldFresnelPower = parFresnelPower.GetValueSingle();

            parbEmmisiveMap = effect.Parameters["bEmmisiveMap"];
            if (parbEmmisiveMap != null)
                oldbEmmisiveMap = parbEmmisiveMap.GetValueBoolean();

            parEmmisiveMap = effect.Parameters["EmmisiveMap"];
            if (parEmmisiveMap != null)
                oldEmmisiveMap = parEmmisiveMap.GetValueTexture2D();

            parEmmisiveAlpha = effect.Parameters["EmmisiveAlpha"];
            if (parEmmisiveAlpha != null)
                oldEmmisiveAlpha = parEmmisiveAlpha.GetValueSingle();

            parObjectAlpha = effect.Parameters["ObjectAlpha"];
            if (parObjectAlpha != null)
                oldObjectAlpha = parObjectAlpha.GetValueSingle();

            parBlendColor = effect.Parameters["BlendColor"];
            if (parBlendColor != null)
                oldBlendColor = parBlendColor.GetValueVector4();

            parRefractMapScale = effect.Parameters["RefractMapScale"];
            if (parRefractMapScale != null)
                oldRefractMapScale = parRefractMapScale.GetValueSingle();

            //parPointLightArray = effect.Parameters["PointLightArray"];
            ////  oldPointLightArray = null;

            //for (int a = 0; a < parPointLightParArray.Length; a++)
            //{
            //    parPointLightParArray[a].position = parPointLightArray.Elements[a].StructureMembers["position"];
            //    parPointLightParArray[a].color = parPointLightArray.Elements[a].StructureMembers["color"];
            //    parPointLightParArray[a].decay = parPointLightArray.Elements[a].StructureMembers["decay"];
            //    parPointLightParArray[a].range = parPointLightArray.Elements[a].StructureMembers["range"];
            //    parPointLightParArray[a].strength = parPointLightArray.Elements[a].StructureMembers["strength"];
            //    parPointLightParArray[a].specularIntensity = parPointLightArray.Elements[a].StructureMembers["specularIntensity"];


            //    parPointLightParArray[a].oldPos = parPointLightParArray[a].oldColor = Vector3.One * float.MaxValue;//隨便給個不可能的初始值
            //    parPointLightParArray[a].oldDecay = parPointLightParArray[a].oldRange =
            //        parPointLightParArray[a].oldStrength = parPointLightParArray[a].oldSpecularIntensity = float.MaxValue;
            //}

            //parPointLightAmount = effect.Parameters["PointLightAmount"];
            //oldPointLightAmount = parPointLightAmount.GetValueInt32();



            //parSpotLightArray = effect.Parameters["SPlightArray"];
            //for (int a = 0; a < parSpotLightParArray.Length; a++)
            //{
            //    parSpotLightParArray[a].position = parSpotLightArray.Elements[a].StructureMembers["position"];
            //    parSpotLightParArray[a].direction = parSpotLightArray.Elements[a].StructureMembers["direction"];
            //    parSpotLightParArray[a].angleTheta = parSpotLightArray.Elements[a].StructureMembers["angleTheta"];
            //    parSpotLightParArray[a].anglePhi = parSpotLightArray.Elements[a].StructureMembers["anglePhi"];
            //    parSpotLightParArray[a].strength = parSpotLightArray.Elements[a].StructureMembers["strength"];
            //    parSpotLightParArray[a].decay = parSpotLightArray.Elements[a].StructureMembers["decay"];
            //    parSpotLightParArray[a].color = parSpotLightArray.Elements[a].StructureMembers["color"];
            //    parSpotLightParArray[a].range = parSpotLightArray.Elements[a].StructureMembers["range"];

            //    parSpotLightParArray[a].oldPos = parSpotLightParArray[a].oldColor =
            //        parSpotLightParArray[a].oldDir = Vector3.One * float.MaxValue;//隨便給個不可能的初始值
            //    parSpotLightParArray[a].oldDecay = parSpotLightParArray[a].oldRange = parSpotLightParArray[a].oldAngleTheta
            //         = parSpotLightParArray[a].oldAnglePhi = parSpotLightParArray[a].oldStrength = float.MaxValue;
            //}

            //parSpotLightAmount = effect.Parameters["SpotLightAmount"];
            //oldSpotLightAmount = parSpotLightAmount.GetValueInt32();

            parShadowVolumeLightDirection = effect.Parameters["ShadowVolumeLightDirection"];
            if (parShadowVolumeLightDirection != null)
                oldShadowVolumeLightDirection = parShadowVolumeLightDirection.GetValueVector3();


            parbReflectMap = effect.Parameters["bReflectionMap"];
            if (parbReflectMap != null)
                oldbReflectionMap = parbReflectMap.GetValueBoolean();

            parReflectionView = effect.Parameters["ReflectionView"];
            if (parReflectionView != null)
                oldReflectionView = parReflectionView.GetValueMatrix();

            parReflectionMap = effect.Parameters["ReflectionMap"];
            if (parReflectionMap != null)
                oldReflectionMap = parReflectionMap.GetValueTexture2D();


            parIfRenderGodsRay = effect.Parameters["bRenderGodsRay"];
            if (parIfRenderGodsRay != null)
                oldIfRenderGodsRay = parIfRenderGodsRay.GetValueBoolean();

            parWindDirection = effect.Parameters["WindDirection"];
            if (parWindDirection != null)
                oldWindDirection = parWindDirection.GetValueVector3();

            parWindWaveSize = effect.Parameters["WindWaveSize"];
            if (parWindWaveSize != null)
                oldWindWaveSize = parWindWaveSize.GetValueSingle();

            parWindAmount = effect.Parameters["WindAmount"];
            if (parWindAmount != null)
                oldWindAmount = parWindAmount.GetValueSingle();

            //parRootHeight = effect.Parameters["RootHeight"];
            //oldRootHeight = parRootHeight.GetValueSingle();

            parTreeHeight = effect.Parameters["TreeHeight"];
            if (parTreeHeight != null)
                oldTreeHeight = parTreeHeight.GetValueSingle();

            parWindSpeed = effect.Parameters["WindSpeed"];
            if (parWindSpeed != null)
                oldWindSpeed = parWindSpeed.GetValueSingle();

            //parGrassBillboardHeight = effect.Parameters["BillboardHeight"];
            //if(parGrassBillboardHeight!=null)
            //    oldGrassBillboardHeight = parGrassBillboardHeight.GetValueSingle();

            parGrassBillboardWidth = effect.Parameters["BillboardWidth"];
            if (parGrassBillboardWidth != null)
                oldGrassBillboardWidth = parGrassBillboardWidth.GetValueSingle();

            parGrassWindRandomness = effect.Parameters["WindRandomness"];
            if (parGrassWindRandomness != null)
                oldGrassWindRandomness = parGrassWindRandomness.GetValueSingle();

            //parGrassWindSpeed = effect.Parameters["WindSpeed"];
            //oldGrassWindSpeed = parGrassWindSpeed.GetValueSingle();

            //parmWorldViewProjectionLast = effect.Parameters["mWorldViewProjectionLast"];
            //parbPowerTexture = effect.Parameters["bPowerTexture"];
            //parPowerTexture = effect.Parameters["PowerTexture"];
            //parbReflectionMap = effect.Parameters["bReflectionMap"];



            parBoneArray = effect.Parameters["BoneArray"];
            //oldBoneArray = null;

            parBigShadowMapRatio = effect.Parameters["BigShadowMapRatio"];
            if (parBigShadowMapRatio != null)
                oldBigShadowMapRatio = parBigShadowMapRatio.GetValueVector3();

            parBigShadowMapBias = effect.Parameters["BigShadowMapDepthBias"];
            if (parBigShadowMapBias != null)
                oldBigShadowMapBias = parBigShadowMapBias.GetValueSingle();

            //parBigShadowMapDepthBias = effect.Parameters["BigShadowMapDepthBias"];
            parbBigShadowMap = effect.Parameters["bBigShadowMap"];
            if (parbBigShadowMap != null)
                oldbBigShadowMap = parbBigShadowMap.GetValueBoolean();

            parBigShadowMapFrustum = effect.Parameters["BigShadowMapFrustum"];
            if (parBigShadowMapFrustum != null)
                oldBigShadowMapFrustum = parBigShadowMapFrustum.GetValueMatrix();

            parBigShadowMap = effect.Parameters["BigShadowMap"];
            if (parBigShadowMap != null)
                oldBigShadowMap = parBigShadowMap.GetValueTexture2D();

            parBigShadowMapMultiSampleOffset = effect.Parameters["BigShadowMapMultiSampleOffset"];
            if (parBigShadowMapMultiSampleOffset != null)
                oldBigShadowMapMultiSampleOffset = parBigShadowMapMultiSampleOffset.GetValueSingle();

            parSmallShadowMapRatio = effect.Parameters["SmallShadowMapRatio"];
            if (parSmallShadowMapRatio != null)
                oldSmallShadowMapRatio = parSmallShadowMapRatio.GetValueVector3();

            parSmallShadowMapBias = effect.Parameters["SmallShadowMapDepthBias"];
            if (parSmallShadowMapBias != null)
                oldSmallShadowMapBias = parSmallShadowMapBias.GetValueSingle();

            parbSmallShadowMap = effect.Parameters["bSmallShadowMap"];
            if (parbSmallShadowMap != null)
                oldbSmallShadowMap = parbSmallShadowMap.GetValueBoolean();

            parSmallShadowMapFrustum = effect.Parameters["SmallShadowMapFrustum"];
            if (parSmallShadowMapFrustum != null)
                oldSmallShadowMapFrustum = parSmallShadowMapFrustum.GetValueMatrix();

            parSmallShadowMap = effect.Parameters["SmallShadowMap"];
            if (parSmallShadowMap != null)
                oldSmallShadowMap = parSmallShadowMap.GetValueTexture2D();

            parSmallShadowMapMultiSampleOffset = effect.Parameters["SmallShadowMapMultiSampleOffset"];
            if (parSmallShadowMapMultiSampleOffset != null)
                oldSmallShadowMapMultiSampleOffset = parSmallShadowMapMultiSampleOffset.GetValueSingle();

            parBigShadowSampleLength = effect.Parameters["BigShadowSampleLength"];
            if (parBigShadowSampleLength != null)
                oldBigShadowSampleLength = parBigShadowSampleLength.GetValueSingle();

            parSmallShadowSampleLength = effect.Parameters["SmallShadowSampleLength"];
            if (parSmallShadowSampleLength != null)
                oldSmallShadowSampleLength = parSmallShadowSampleLength.GetValueSingle();

            //Get Effect paramater....
            //    m_SimpleTec = effect.Techniques["SimpleTransformAndTexture"];
            m_ScreenV_NormalMapTec = effect.Techniques["ScreenV_NormalMapTechnique"];
            m_NormalMapTec = effect.Techniques["NormalMapTechnique"];
            m_ScreenV_GodsRay_NormalMapTec = effect.Techniques["ScreenV_GodsRay_NormalMapTechnique"];
            m_ScreenV_GodsRay_Emmisive_NormalMapTec = effect.Techniques["ScreenV_GodsRay_Emmisive_NormalMapTechnique"];
            m_Emmisive_NormalMapTec = effect.Techniques["Emmisive_NormalMapTechnique"];

            m_ScreenV_DrawWithShadowMapTec = effect.Techniques["ScreenV_DrawWithShadowMap"];
            m_DrawWithShadowMapTec = effect.Techniques["SimpleDrawWithShadowMap"];
            m_ScreenV_GodsRay_DrawWithShadowMapTec = effect.Techniques["ScreenV_GodsRay_DrawWithShadowMap"];
            m_ScreenV_GodsRay_Emmisive_DrawWithShadowMapTec = effect.Techniques["ScreenV_GodsRay_Emmisive_DrawWithShadowMap"];
            m_Emmisive_DrawWithShadowMapTec = effect.Techniques["Emmisive_DrawWithShadowMap"];

            m_ScreenV_GodsRay_Emmisive_NormalMapSkinned = effect.Techniques["ScreenV_GodsRay_Emmisive_NormalMapSkinned"];
            m_ScreenV_GodsRay_SkinnedDrawWithShadowMap = effect.Techniques["ScreenV_GodsRay_SkinnedDrawWithShadowMap"];
            m_Emmisive_NormalMapSkinned = effect.Techniques["Emmisive_NormalMapSkinned"];
            m_Emmisive_SkinnedDrawWithShadowMap = effect.Techniques["Emmisive_SkinnedDrawWithShadowMap"];
            m_NormalMapSkinned = effect.Techniques["NormalMapSkinned"];
            m_SkinnedDrawWithShadowMap = effect.Techniques["SkinnedDrawWithShadowMap"];

            m_HardwareInstancing = effect.Techniques["HardwareInstancing"];
        }


        Matrix oldWorld, oldView, oldProjection, oldDepthMapFrustum, oldBigShadowMapFrustum, oldSmallShadowMapFrustum,
            oldReflectionView;
        Texture2D oldBaseTexture, oldNormalTexture, oldReflectPowerMap, oldSpecularMap, oldNextTexture, oldEmmisiveMap,
            oldBigShadowMap, oldSmallShadowMap, oldReflectionMap;
        TextureCube oldEnvCubeMap;
        bool oldHasTexture, oldLightEnable, oldbReflectPowerMap, oldbSpecularMap, oldbEnvCubeMap,
            //oldMipMapEnable, 
            oldbEmmisiveMap, oldbBigShadowMap, oldbSmallShadowMap, oldbReflectionMap,
            oldIfRenderGodsRay, oldbFogEnable;

        Vector4 oldBlendColor;
        Vector3 oldLightDirection, oldLightAmbient, oldLightDiffuse, oldLightSpecular,
            oldMaterialAmbient, oldMaterialDiffuse, oldMaterialSpecular, oldShadowVolumeLightDirection, oldWindDirection,
            oldBigShadowMapRatio, oldSmallShadowMapRatio;
        int oldMaterialPower, oldPointLightAmount, oldSpotLightAmount;
        float oldTotalEllapseMillisecond, oldNextTexturePercent, oldEmmisiveAlpha, oldObjectAlpha, oldRefractMapScale,
            oldReflectIntensity, oldBigShadowMapMultiSampleOffset,
            oldSmallShadowMapMultiSampleOffset, oldBigShadowMapBias, oldSmallShadowMapBias,
             oldWindWaveSize, oldWindAmount, oldRootHeight, oldTreeHeight, oldWindSpeed, oldGrassWindRandomness,
             oldGrassWindSpeed, oldGrassBillboardWidth, oldFresnelPower,
             oldBigShadowSampleLength, oldSmallShadowSampleLength, oldMaterialBankID, oldAlphaTestReference;

        //Matrix[] oldBoneArray;





        Vector2 oldTranslateUVSinLoopSpeed, oldTranslateUVSpeed;
        Vector3 oldCenterScaleUV;
        //       object oldPointLightArray;



        public void SetFog(float start, float end, float fogMaxRatio, ref Vector3 fogColor)
        {
            parFogStart.SetValue(start);
            parFogEnd.SetValue(end);
            parFogColor.SetValue(fogColor);

            parFogMaxRatio.SetValue(fogMaxRatio);
        }

        public void SetBigShadowMap(ref Matrix BigShadowMapFrustum, Texture2D bigShadowmap, float BigShadowSampleLength)
        {
            bool bShadowMap = false;
            if (bigShadowmap != null)
                bShadowMap = true;
            if (oldbBigShadowMap != bShadowMap)
            {
                oldbBigShadowMap = bShadowMap;
                parbBigShadowMap.SetValue(oldbBigShadowMap);
            }

            if (oldBigShadowMapFrustum != BigShadowMapFrustum)
            {
                oldBigShadowMapFrustum = BigShadowMapFrustum;
                parBigShadowMapFrustum.SetValue(oldBigShadowMapFrustum);
            }

            if (oldBigShadowMap != bigShadowmap)
            {
                oldBigShadowMap = bigShadowmap;
                parBigShadowMap.SetValue(oldBigShadowMap);
            }

            //if (oldBigShadowSampleLength != BigShadowSampleLength)
            //{
            //    oldBigShadowSampleLength = BigShadowSampleLength;
            //    parBigShadowSampleLength.SetValue(oldBigShadowSampleLength);
            //}

            //if (oldBigShadowMapMultiSampleOffset != MultiSampleOffset)
            //{
            //    oldBigShadowMapMultiSampleOffset = MultiSampleOffset;
            //    parBigShadowMapMultiSampleOffset.SetValue(MultiSampleOffset);
            //}

            //if (oldBigShadowMapBias != bias)
            //{
            //    oldBigShadowMapBias = bias;
            //    parBigShadowMapBias.SetValue(bias);          
            //}
        }

        ///// <summary>
        ///// SmallShadowmap傳入null就會不設定small shadow map
        ///// </summary>
        //public void SetSmallShadowMap(ref Matrix smallShadowMapFrustum, Texture2D SmallShadowmap)
        //{
        //    bool bShadowMap = false;
        //    if (SmallShadowmap != null)
        //        bShadowMap = true;
        //    if (oldbSmallShadowMap != bShadowMap)
        //    {
        //        oldbSmallShadowMap = bShadowMap;
        //        parbSmallShadowMap.SetValue(oldbSmallShadowMap);
        //    }

        //    if (oldSmallShadowMapFrustum != smallShadowMapFrustum)
        //    {
        //        oldSmallShadowMapFrustum = smallShadowMapFrustum;
        //        parSmallShadowMapFrustum.SetValue(oldSmallShadowMapFrustum);
        //    }

        //    if (oldSmallShadowMap != SmallShadowmap)
        //    {
        //        oldSmallShadowMap = SmallShadowmap;
        //        parSmallShadowMap.SetValue(oldSmallShadowMap);
        //    }

        //    //if (oldSmallShadowSampleLength != SmallShadowSampleLength)
        //    //{
        //    //    oldSmallShadowSampleLength = SmallShadowSampleLength;
        //    //    parSmallShadowSampleLength.SetValue(oldSmallShadowSampleLength);
        //    //}

        //    //if (oldSmallShadowMapMultiSampleOffset != MultiSampleOffset)
        //    //{
        //    //    oldSmallShadowMapMultiSampleOffset = MultiSampleOffset;
        //    //    parSmallShadowMapMultiSampleOffset.SetValue(MultiSampleOffset);
        //    //}

        //    //if (oldSmallShadowMapBias != bias)
        //    //{
        //    //    oldSmallShadowMapBias = bias;
        //    //    parSmallShadowMapBias.SetValue(bias);
        //    //}
        //}

        public void SetDirectionalLight1(ref Vector3 lightAmbient, ref Vector3 lightDiffuse, ref Vector3 lightSpecular, ref Vector3 lightDirection)
        {
            if (oldLightDirection != lightDirection)
            {
                oldLightDirection = lightDirection;
                parLightDirection.SetValue(oldLightDirection);

                //     parShadowVolumeLightDirection.SetValue(oldLightDirection);//抓平行光的來用
            }

            if (oldLightAmbient != lightAmbient)
            {
                oldLightAmbient = lightAmbient;
                parDLight_a.SetValue(oldLightAmbient);
            }

            if (oldLightDiffuse != lightDiffuse)
            {
                oldLightDiffuse = lightDiffuse;
                parDLight_d.SetValue(oldLightDiffuse);
            }

            if (oldLightSpecular != lightSpecular)
            {
                oldLightSpecular = lightSpecular;
                parDLight_s.SetValue(oldLightSpecular);
            }
        }

        //public void SetSpotLight(SpotLight[] spotLightArray, int spotLightAmount)
        //{
        //    if (oldSpotLightAmount != spotLightAmount)
        //    {
        //        oldSpotLightAmount = spotLightAmount;
        //        parSpotLightAmount.SetValue(oldSpotLightAmount);
        //    }

        //    if (spotLightArray != null)
        //    {
        //        for (int a = 0; a < spotLightAmount; a++)
        //        {
        //            SpotLight spLight = spotLightArray[a];

        //            if (parSpotLightParArray[a].oldPos != spLight.position)
        //            {
        //                parSpotLightParArray[a].oldPos = spLight.position;
        //                parSpotLightParArray[a].position.SetValue(parSpotLightParArray[a].oldPos);
        //            }
        //            if (parSpotLightParArray[a].oldColor != spLight.colorValue)
        //            {
        //                parSpotLightParArray[a].oldColor = spLight.colorValue;
        //                parSpotLightParArray[a].color.SetValue(parSpotLightParArray[a].oldColor);
        //            }
        //            if (parSpotLightParArray[a].oldDir != spLight.direction)
        //            {
        //                parSpotLightParArray[a].oldDir = spLight.direction;
        //                parSpotLightParArray[a].direction.SetValue(parSpotLightParArray[a].oldDir);
        //            }
        //            if (parSpotLightParArray[a].oldAngleTheta != spLight.angleTheta)
        //            {
        //                parSpotLightParArray[a].oldAngleTheta = spLight.angleTheta;
        //                parSpotLightParArray[a].angleTheta.SetValue(parSpotLightParArray[a].oldAngleTheta);
        //            }
        //            if (parSpotLightParArray[a].oldAnglePhi != spLight.anglePhi)
        //            {
        //                parSpotLightParArray[a].oldAnglePhi = spLight.anglePhi;
        //                parSpotLightParArray[a].anglePhi.SetValue(parSpotLightParArray[a].oldAnglePhi);
        //            }
        //            if (parSpotLightParArray[a].oldDecay != spLight.decay)
        //            {
        //                parSpotLightParArray[a].oldDecay = spLight.decay;
        //                parSpotLightParArray[a].decay.SetValue(parSpotLightParArray[a].oldDecay);
        //            }
        //            if (parSpotLightParArray[a].oldRange != spLight.range)
        //            {
        //                parSpotLightParArray[a].oldRange = spLight.range;
        //                parSpotLightParArray[a].range.SetValue(parSpotLightParArray[a].oldRange);
        //            }
        //            if (parSpotLightParArray[a].oldStrength != spLight.strength)
        //            {
        //                parSpotLightParArray[a].oldStrength = spLight.strength;
        //                parSpotLightParArray[a].strength.SetValue(parSpotLightParArray[a].oldStrength);
        //            }
        //        }

        //    }
        //}

        void SetEffectWorld(ref Matrix world)
        {
            Matrix TempMat = world;
            if (Matrix.Equals(oldWorld, TempMat) == false)
            {
                oldWorld = TempMat;
                parWorld.SetValue(TempMat);
            }
        }

        //void SetEffectTexture(Texture2D texture)
        //{
        //    parBaseTexture.SetValue(oldBaseTexture);//貼圖每次都要重設，每次RENDER都繪圖不見，會閃，所以要重設。
        //    //if (bTexture == true)
        //    //{
        //    //    if (oldBaseTexture != texture)
        //    //    {
        //    //        oldBaseTexture = texture;
        //    //        parBaseTexture.SetValue(oldBaseTexture);
        //    //    }
        //    //}
        //}

        public void SetSkinnedMeshEffectPar(Matrix[] boneArray, Matrix parentWorld)
        {
            //設定骨架資訊給MESH
            SetEffectWorld(ref parentWorld);

            //if (oldBoneArray != boneArray)
            // {
            //    oldBoneArray = boneArray;
            parBoneArray.SetValue(boneArray);
            //}
        }

        public void SetSkinnedMeshPartEffectPar(Texture2D texture, SceneNode node)
        {
            //設定每個貼圖資訊給mesh Part，並commitChange();
            //      SetEffectTexture(texture);
            parBaseTexture.SetValue(texture);//貼圖每次都要重設，每次RENDER都繪圖不見，會閃，所以要重設。

            if (oldAlphaTestReference != (float)node.AlphaTestReference / 255f)
                parAlphaTestReference.SetValue((float)node.AlphaTestReference / 255f);

            m_RenderEffect.CommitChanges();
        }

        public void SetShadowVolumeNodeEffectPar(Matrix World)
        {
            //if(Vector3.Equals(shadowVolumeLightDirection, oldShadowVolumeLightDirection) == false)
            //{
            //    oldShadowVolumeLightDirection = shadowVolumeLightDirection;
            //    parShadowVolumeLightDirection.SetValue(oldShadowVolumeLightDirection);
            //}            

            SetEffectWorld(ref World);
            m_RenderEffect.CommitChanges();
        }

        public void SetMeshNodeEffectPar_Commit(Matrix World)
        {
            SetEffectWorld(ref World);
            m_RenderEffect.CommitChanges();
        }

        public void SetInstanceMeshEffectPar(Matrix World)
        {
            //SetEffectWorld(ref World);
            m_RenderEffect.CommitChanges();
        }

        /// <summary>
        /// 設定畫草的最後確定參數，之後會m_RenderEffect.CommitChanges();
        /// </summary>
        /// <param name="windData"></param>
        /// <param name="World"></param>
        public void SetGrassMeshEffectPar(SpecialMeshDataManager.WindData windData, Matrix World)
        {
            //if (oldGrassBillboardHeight != windData.TreeHeight)
            //{
            //    oldGrassBillboardHeight = windData.TreeHeight;
            //    parGrassBillboardHeight.SetValue(oldGrassBillboardHeight);
            //}
            if (oldGrassBillboardWidth != windData.grassData.width)
            {
                oldGrassBillboardWidth = windData.grassData.width;
                parGrassBillboardWidth.SetValue(oldGrassBillboardWidth);
            }
            if (oldGrassWindRandomness != windData.grassData.windRandomness)
            {
                oldGrassWindRandomness = windData.grassData.windRandomness;
                parGrassWindRandomness.SetValue(oldGrassWindRandomness);
            }
            //if(oldGrassWindSpeed != grassData.windSpeed)
            //{
            //    oldGrassWindSpeed = grassData.windSpeed;
            //    parGrassWindSpeed.SetValue(oldGrassWindSpeed);
            //}

            SetEffectWorld(ref World);
            //m_RenderEffect.Parameters["WorldMatrix"].SetValue(World);
            m_RenderEffect.CommitChanges();
        }

        public void SetGodsRayEffectPar(SceneNode sceneNode)
        {
            if (oldIfRenderGodsRay != sceneNode.IfRenderGodsRay)
            {
                oldIfRenderGodsRay = sceneNode.IfRenderGodsRay;
                parIfRenderGodsRay.SetValue(oldIfRenderGodsRay);
            }
        }

        void SetCreateDepthMapEffectPar(Matrix DepthMapFrustum)
        {
            if (Matrix.Equals(oldDepthMapFrustum, DepthMapFrustum) == false)
            {
                oldDepthMapFrustum = DepthMapFrustum;
                parDepthMapFrustum.SetValue(oldDepthMapFrustum);
            }
        }

        /// <summary>
        /// 設定view 和 projrction給shader
        /// </summary>
        public void SetBasicCommonEffectPar(Matrix view, Matrix projection, Matrix inverseView, SceneNode node, bool bViewPosition)
        {
            //Matrix TempMat = node.WorldMatrix;
            //if (Matrix.Equals(oldWorld, TempMat) == false)
            //{
            //    oldWorld = TempMat;
            //    parWorld.SetValue(TempMat);
            //}

            if (Matrix.Equals(oldProjection, projection) == false)
            {
                oldProjection = projection;
                parProjection.SetValue(projection);
            }

            if (Matrix.Equals(oldView, view) == false)
            {
                oldView = view;
                parView.SetValue(view);

                if (bViewPosition)
                    parViewPosition.SetValue(inverseView.Translation);
            }
        }

        /// <summary>
        /// 設定Node
        /// </summary>
        /// <param name="node"></param>
        public void SetSceneNodeEffectPar(SceneNode node)
        {
            if (oldbFogEnable != node.IfFogEnable)
            {
                oldbFogEnable = node.IfFogEnable;
                parbFogEnable.SetValue(node.IfFogEnable);
            }

            #region 物件材質有關
            //if (Vector3.Equals(oldMaterialAmbient, node.MaterialAmbient) == false)
            //{
            //    oldMaterialAmbient = node.MaterialAmbient;
            //    park_a.SetValue(node.MaterialAmbient);
            //}

            //if (Vector3.Equals(oldMaterialDiffuse, node.MaterialDiffuse) == false)
            //{
            //    oldMaterialDiffuse = node.MaterialDiffuse;
            //    park_d.SetValue(node.MaterialDiffuse);
            //}

            //if (Vector3.Equals(oldMaterialSpecular, node.MaterialSpecular) == false)
            //{
            //    oldMaterialSpecular = node.MaterialSpecular;
            //    park_s.SetValue(node.MaterialSpecular);
            //}

            //if (oldMaterialPower != node.MaterialPower)
            //{
            //    oldMaterialPower = node.MaterialPower;
            //    parPower.SetValue(node.MaterialPower);
            //}

            if (oldReflectPowerMap != node.ReflectIntensityMap)
            {
                oldReflectPowerMap = node.ReflectIntensityMap;

                bool bSet = false;
                if (node.ReflectIntensityMap != null)
                {
                    bSet = true;
                    parReflectPowerMap.SetValue(node.ReflectIntensityMap);
                }

                if (oldbReflectPowerMap != bSet)
                {
                    oldbReflectPowerMap = bSet;
                    parbReflectPowerMap.SetValue(bSet);
                }
            }

            if (oldSpecularMap != node.SpecularMap)
            {
                oldSpecularMap = node.SpecularMap;

                bool bSet = false;
                if (node.SpecularMap != null)
                {
                    bSet = true;
                    parSpecularMap.SetValue(node.SpecularMap);
                }

                if (oldbSpecularMap != bSet)
                {
                    oldbSpecularMap = bSet;
                    parbSpecularMap.SetValue(bSet);
                }
            }

            if (oldEnvCubeMap != node.EnvCubeMap)
            {
                oldEnvCubeMap = node.EnvCubeMap;

                bool bSet = false;
                if (node.EnvCubeMap != null)
                {
                    bSet = true;
                    parEnvCubeMap.SetValue(node.EnvCubeMap);
                }

                if (oldbEnvCubeMap != bSet)
                {
                    oldbEnvCubeMap = bSet;
                    parbEnvCubeMap.SetValue(bSet);
                }
            }

            SetEmmisiveEffectPar(node);
            #endregion

            #region 一直更新時間進去shader
            float totalEllapseMillisecond = (float)Environment.TickCount / 1000f;//1代表1秒
            if (oldTotalEllapseMillisecond != totalEllapseMillisecond)
            {
                oldTotalEllapseMillisecond = totalEllapseMillisecond;
                parTotalEllapsedMilliseonds.SetValue(totalEllapseMillisecond);
            }
            #endregion

            #region 控制貼圖軸的動態ˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇ
            if (oldTranslateUVSinLoopSpeed != node.TranslateUVSinLoopSpeed)
            {
                oldTranslateUVSinLoopSpeed = node.TranslateUVSinLoopSpeed;
                parTranslateUVSinLoopSpeed.SetValue(node.TranslateUVSinLoopSpeed);
            }

            if (oldTranslateUVSpeed != node.TranslateUVSpeed)
            {
                oldTranslateUVSpeed = node.TranslateUVSpeed;
                parTranslateUVSpeed.SetValue(node.TranslateUVSpeed);
            }

            if (oldCenterScaleUV != node.centerScaleUV)
            {
                oldCenterScaleUV = node.centerScaleUV;
                parCenterScaleUV.SetValue(node.centerScaleUV);
            }
            #endregion

            #region 傳入繪圖特徵參數ˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇ
            //可再根據材質細分繪出的物件群組
            //光材質
            if (oldLightEnable != node.IfLightEnable)
            {
                oldLightEnable = node.IfLightEnable;
                parLightEnable.SetValue(node.IfLightEnable);
            }

            SetBlendColorEffectPar(node);
            SetObjectAlphaEffectPar(node);

            if (oldRefractMapScale != node.RefractScale)
            {
                oldRefractMapScale = node.RefractScale;
                parRefractMapScale.SetValue(node.RefractScale);
            }

            if (oldBigShadowMapRatio != node.BigShadowMapRatio)
            {
                oldBigShadowMapRatio = node.BigShadowMapRatio;
                parBigShadowMapRatio.SetValue(node.BigShadowMapRatio);
            }

            if (oldSmallShadowMapRatio != node.SmallShadowMapRatio)
            {
                oldSmallShadowMapRatio = node.SmallShadowMapRatio;
                parSmallShadowMapRatio.SetValue(node.SmallShadowMapRatio);
            }

            if (oldBigShadowMapBias != node.BigShadowMapBias)
            {
                oldBigShadowMapBias = node.BigShadowMapBias;
                parBigShadowMapBias.SetValue(node.BigShadowMapBias);
            }

            if (oldSmallShadowMapBias != node.SmallShadowMapBias)
            {
                oldSmallShadowMapBias = node.SmallShadowMapBias;
                parSmallShadowMapBias.SetValue(node.SmallShadowMapBias);
            }

            if (oldBigShadowMapMultiSampleOffset != node.BigShadowSampleOffset)
            {
                oldBigShadowMapMultiSampleOffset = node.BigShadowSampleOffset;
                parBigShadowMapMultiSampleOffset.SetValue(node.BigShadowSampleOffset);
            }

            if (oldSmallShadowMapMultiSampleOffset != node.SmallShadowSampleOffset)
            {
                oldSmallShadowMapMultiSampleOffset = node.SmallShadowSampleOffset;
                parSmallShadowMapMultiSampleOffset.SetValue(node.SmallShadowSampleOffset);
            }

            if (oldBigShadowSampleLength != node.BigShadowSampleLength)
            {
                oldBigShadowSampleLength = node.BigShadowSampleLength;
                parBigShadowSampleLength.SetValue(oldBigShadowSampleLength);
            }

            if (oldSmallShadowSampleLength != node.SmallShadowSampleLength)
            {
                oldSmallShadowSampleLength = node.SmallShadowSampleLength;
                parSmallShadowSampleLength.SetValue(oldSmallShadowSampleLength);
            }
            #endregion

            //#region 與貼圖有關&動態圖用參數^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            //SetTextureEffectPar(node);
            //#endregion 

            //#region 點光源資訊^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            //if (node.PointLightList != null)
            //{
            //    //    if (oldPointLightArray != node.PointLightList) //fx理預設是TRUE
            //    //     {
            //    //         oldPointLightArray = node.PointLightList;
            //    for (int a = 0; a < node.PointLightAmount; a++)
            //    {
            //        PointLightNode PLNode = node.PointLightList[a];

            //        if (parPointLightParArray[a].oldPos != PLNode.WorldPosition)
            //        {
            //            parPointLightParArray[a].oldPos = PLNode.WorldPosition;
            //            parPointLightParArray[a].position.SetValue(parPointLightParArray[a].oldPos);
            //        }

            //        if (parPointLightParArray[a].oldDecay != PLNode.Decay)
            //        {
            //            parPointLightParArray[a].oldDecay = PLNode.Decay;
            //            parPointLightParArray[a].decay.SetValue(parPointLightParArray[a].oldDecay);
            //        }

            //        if (parPointLightParArray[a].oldRange != PLNode.RangeValue)
            //        {
            //            parPointLightParArray[a].oldRange = PLNode.RangeValue;
            //            parPointLightParArray[a].range.SetValue(parPointLightParArray[a].oldRange);
            //        }

            //        if (parPointLightParArray[a].oldColor != PLNode.LightColor.ToVector3())
            //        {
            //            parPointLightParArray[a].oldColor = PLNode.LightColor.ToVector3();
            //            parPointLightParArray[a].color.SetValue(parPointLightParArray[a].oldColor);
            //        }
            //        if (parPointLightParArray[a].oldStrength != PLNode.Strength)
            //        {
            //            parPointLightParArray[a].oldStrength = PLNode.Strength;
            //            parPointLightParArray[a].strength.SetValue(parPointLightParArray[a].oldStrength);
            //        }

            //        if (parPointLightParArray[a].oldSpecularIntensity != PLNode.SpecularIntensity)
            //        {
            //            parPointLightParArray[a].oldSpecularIntensity = PLNode.SpecularIntensity;
            //            parPointLightParArray[a].specularIntensity.SetValue(parPointLightParArray[a].oldSpecularIntensity);
            //        }
            //    }
            //    //    }
            //}

            //if (oldPointLightAmount != node.PointLightAmount)
            //{
            //    oldPointLightAmount = node.PointLightAmount;
            //    parPointLightAmount.SetValue(node.PointLightAmount);
            //}
            //#endregion

            #region 反射貼圖
            if (oldReflectIntensity != node.ReflectIntensity)
            {
                oldReflectIntensity = node.ReflectIntensity;
                parReflectIntensity.SetValue(oldReflectIntensity);

                if (oldReflectIntensity > 0)
                {
                    if (oldFresnelPower != node.FresnelPower)
                    {
                        oldFresnelPower = node.FresnelPower;
                        parFresnelPower.SetValue(oldFresnelPower);
                    }
                }
            }

            //if (oldReflectionMap != node.ReflectMap)
            //{
            //    oldReflectionMap = node.ReflectMap;

            //    bool bSet = false;
            //    if (node.ReflectMap != null)
            //    {
            //        bSet = true;
            //        parReflectionMap.SetValue(node.ReflectMap);
            //    }

            //    if (oldbReflectionMap != bSet)
            //    {
            //        oldbReflectionMap = bSet;
            //        parbReflectMap.SetValue(bSet);
            //    }

            //    if (bSet)
            //    {
            //        Matrix refletView = node.ReflectView;
            //        if (oldReflectionView != refletView)
            //        {
            //            oldReflectionView = refletView;
            //            parReflectionView.SetValue(refletView);
            //        }
            //    }
            //}
            #endregion
            //m_RenderEffect.CommitChanges();

            #region WindWave
            SetWindWaveEffectpar(node);
            #endregion
        }

        /// <summary>
        /// 設定BlendColor，必須ObjectAlpha也要設定，應位在Shader裡面
        /// diffuseColor = lerp(BlendColor, diffuseColor, diffuseColor.a * ObjectAlpha);
        /// </summary>
        /// <param name="node"></param>
        public void SetBlendColorEffectPar(SceneNode node)
        {
            Vector4 blendColor = node.BlendColor.ToVector4();
            if (oldBlendColor != blendColor)
            {
                oldBlendColor = blendColor;
                parBlendColor.SetValue(blendColor);
            }
        }

        /// <summary>
        /// 設定ObjectAlpha，必須BlendColor也要設定，應位在Shader裡面
        /// diffuseColor = lerp(BlendColor, diffuseColor, diffuseColor.a * ObjectAlpha);
        /// </summary>
        /// <param name="node"></param>
        public void SetObjectAlphaEffectPar(SceneNode node)
        {
            float ObjectAlphaRatio = (float)node.ObjectAlpha / 255f;
            if (oldObjectAlpha != ObjectAlphaRatio)
            {
                oldObjectAlpha = ObjectAlphaRatio;
                parObjectAlpha.SetValue(ObjectAlphaRatio);
            }
        }

        ///// <summary>
        ///// 設定物件是否與光和霧做計算
        ///// </summary>
        ///// <param name="node"></param>
        //public void SetFogLightEnable(SceneNode node)
        //{
        //    if (oldbFogEnable != node.IfFogEnable)
        //    {
        //        oldbFogEnable = node.IfFogEnable;
        //        parbFogEnable.SetValue(node.IfFogEnable);
        //    }

        //    if (oldLightEnable != node.IfLightEnable)
        //    {
        //        oldLightEnable = node.IfLightEnable;
        //        parLightEnable.SetValue(node.IfLightEnable);
        //    }
        //}             

        /// <summary>
        /// 設定node.WindData的資料給shader
        /// </summary>        
        public void SetWindWaveEffectpar(SceneNode node)
        {
            if (node.WindData != null)
            {
                if (oldWindAmount != node.WindData.WindAmount) //fx理預設是TRUE
                {
                    oldWindAmount = node.WindData.WindAmount;
                    parWindAmount.SetValue(oldWindAmount);
                }

                if (oldWindAmount != 0)//沒windAmount就不用搖了
                {
                    if (oldWindDirection != node.WindData.WorldWindDirection) //fx理預設是TRUE
                    {
                        oldWindDirection = node.WindData.WorldWindDirection;
                        parWindDirection.SetValue(oldWindDirection);
                    }
                    if (oldWindWaveSize != node.WindData.WindWaveSize) //fx理預設是TRUE
                    {
                        oldWindWaveSize = node.WindData.WindWaveSize;
                        parWindWaveSize.SetValue(oldWindWaveSize);
                    }

                    //if (oldRootHeight != node.WindData.RootHeight) //fx理預設是TRUE
                    //{
                    //    oldRootHeight = node.WindData.RootHeight;
                    //    parRootHeight.SetValue(oldRootHeight);
                    //}
                    if (oldTreeHeight != node.WindData.TreeHeight) //fx理預設是TRUE
                    {
                        oldTreeHeight = node.WindData.TreeHeight;
                        parTreeHeight.SetValue(oldTreeHeight);
                    }

                    if (oldWindSpeed != node.WindData.WindSpeed) //fx理預設是TRUE
                    {
                        oldWindSpeed = node.WindData.WindSpeed;
                        parWindSpeed.SetValue(oldWindSpeed);
                    }
                }
            }
            else
            {
                if (oldWindAmount != 0) //fx理預設是TRUE
                {
                    oldWindAmount = 0;
                    parWindAmount.SetValue(oldWindAmount);
                }
            }
        }

        /// <summary>
        /// 設定EmmisiveMap和EmmisiveAlpha
        /// </summary>    
        public void SetEmmisiveEffectPar(SceneNode node)
        {
            if (oldEmmisiveMap != node.EmmisiveMap)
            {
                oldEmmisiveMap = node.EmmisiveMap;

                bool bSet = false;
                if (node.EmmisiveMap != null)
                {
                    bSet = true;
                    parEmmisiveMap.SetValue(node.EmmisiveMap);
                }

                if (oldbEmmisiveMap != bSet)
                {
                    oldbEmmisiveMap = bSet;
                    parbEmmisiveMap.SetValue(bSet);
                }
            }

            if (oldEmmisiveAlpha != node.EmmisiveAlpha)
            {
                oldEmmisiveAlpha = node.EmmisiveAlpha;
                parEmmisiveAlpha.SetValue(node.EmmisiveAlpha);
            }
        }

        /// <summary>
        /// 與貼圖有關&動態圖用參數，
        /// bSetNormalMap設定是否要設定normal map的參數給shader，像是畫影子depth map的話就不需要傳normal map參數。
        /// </summary>
        /// <param name="node"></param>
        /// <param name="bSetNormalMap"></param>
        public void SetTextureEffectPar(SceneNode node, bool bSetNormalMap)
        {
            if (oldHasTexture != node.IfTextured) //fx理預設是TRUE
            {
                oldHasTexture = node.IfTextured;
                parbTexture.SetValue(oldHasTexture);
            }

            parBaseTexture.SetValue(node.BaseTexture);//貼圖每次都要重設，每次RENDER都繪圖不見，會閃，所以要重設。
            //SetEffectTexture(node.BaseTexture);


            float alphaRef = (float)node.AlphaTestReference / 255f;
            if (oldAlphaTestReference != alphaRef)
            {
                oldAlphaTestReference = alphaRef;
                parAlphaTestReference.SetValue(alphaRef);
            }

            if (oldHasTexture == true)
            {
                if (node.IfTangentFrame && bSetNormalMap)
                {
                    if (node.NormalTexture == null)
                    {
                        oldNormalTexture = null;
                        parNormalMap.SetValue(oldNormalTexture);
                    }
                    else if (oldNormalTexture != node.NormalTexture)
                    {
                        oldNormalTexture = node.NormalTexture;
                        parNormalMap.SetValue(oldNormalTexture);
                    }
                }

                if (node.DynamicTexture != null)
                {
                    if (oldNextTexturePercent != node.DynamicTexture.GetPlayPercent)
                    {
                        oldNextTexturePercent = node.DynamicTexture.GetPlayPercent;
                        parNextTexturePercent.SetValue(oldNextTexturePercent);
                    }

                    if (oldNextTexture != node.DynamicTexture.GetNextTexture())
                    {
                        oldNextTexture = node.DynamicTexture.GetNextTexture();
                        parNextTexture.SetValue(oldNextTexture);
                    }
                }
                else
                {
                    if (oldNextTexturePercent != 0)//沒動態圖的話撥放比率要改回0，0會完全偏原始貼圖。
                    {
                        oldNextTexturePercent = 0;
                        parNextTexturePercent.SetValue(oldNextTexturePercent);
                    }
                }
            }
        }

        //float oldMipMapLevel = 0;
        //public int RenderNode(NodeArray nodeArray, GraphicsDevice device,
        //    bool bRenderAlphaBlending, //bool bEmmisiveOnly, 
        //    bool bForceRender, bool bIgnoreCullMode,
        //    Matrix? DepthMapFrustum,
        //    Matrix? View, Matrix? Projection, Matrix? InverseView, ref uint triangle)
        //{
        //    int renderCount = 0;
        //    for (int a = 0; a < nodeArray.nowNodeAmount; a++)
        //    {
        //        SceneNode sceneNode = nodeArray.nodeArray[a] as SceneNode;

        //        if (bForceRender == false && sceneNode.BeCulled == true)
        //            continue;

        //        if (sceneNode.Visible == false)
        //            continue;

        //        if ((CurrentRenderTechnique & SceneNode.RenderTechnique.DepthMap) != 0 &&
        //            sceneNode.IfRenderShadowMap == false)
        //            continue;

        //        //若是反鋸齒情況畫出EMM圖，深度緩衝無法重複使用所以必須全都畫出
        //        //if (MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)MeshNode.RenderTechnique.EmmisiveOnly)
        //        //    && sceneNode.EmmisiveAlpha <= 0)
        //        //    continue;

        //        if (//sceneNode == null ||
        //            sceneNode.IfAlphaBlenbing != bRenderAlphaBlending)
        //            continue;

        //        //判斷目前technique是否符合基本SceneNode的繪出條件
        //        //if (sceneNode.ConsiderCurrentTechniqueOK(CurrentRenderTechnique) == false)
        //        //    continue;

        //        //根據目前node要畫的東西，設定effect paramater
        //        if (DepthMapFrustum != null)
        //        {
        //            SetCreateDepthMapEffectPar(DepthMapFrustum.Value);
        //            SetTextureEffectPar(sceneNode, false);
        //        }
        //        else if (sceneNode.GetType() == typeof(ShadowVolumeNode))
        //        {
        //            SetBasicCommonEffectPar(View.Value, Projection.Value, InverseView.Value, sceneNode, false);
        //        }
        //        else if (MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)MeshNode.RenderTechnique.EmmisiveOnly))
        //        {
        //            SetBasicCommonEffectPar(View.Value, Projection.Value, InverseView.Value, sceneNode, false);
        //            SetEmmisiveEffectPar(sceneNode);
        //            SetWindWaveEffectpar(sceneNode);
        //            SetBlendColorEffectPar(sceneNode);
        //            SetObjectAlphaEffectPar(sceneNode);
        //            SetTextureEffectPar(sceneNode, false);
        //        }
        //        else
        //        {
        //            SetBasicCommonEffectPar(View.Value, Projection.Value, InverseView.Value, sceneNode, true);
        //            SetSceneNodeEffectPar(sceneNode);
        //            SetTextureEffectPar(sceneNode, true);

        //            if (MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)MeshNode.RenderTechnique.GodsRayMap))
        //            {
        //                SetGodsRayEffectPar(sceneNode);
        //            }
        //        }

        //        bool recAlphatest = true;
        //        if (sceneNode.IfAlphaTestEnable == false)
        //        {
        //            recAlphatest = device.RenderState.AlphaTestEnable;
        //            device.RenderState.AlphaTestEnable = false;
        //        }

        //        float RecMipMapLevel = device.SamplerStates[0].MipMapLevelOfDetailBias;
        //        if (sceneNode.MipmapLevel != 0 && sceneNode.MipmapLevel != oldMipMapLevel)//不為0的才吃他的設定值
        //        {
        //            oldMipMapLevel = sceneNode.MipmapLevel;
        //            device.SamplerStates[0].MipMapLevelOfDetailBias = oldMipMapLevel;
        //        }

        //        if (sceneNode.IfRenderDepth == false)
        //            device.RenderState.DepthBufferWriteEnable = false;

        //        //function外已經過濾了所以這邊一概使用force Render畫出
        //        //sceneNode.Render(this, bForceRender, bIgnoreCullMode, ref triangle, device);

        //        //還原
        //        if (sceneNode.IfRenderDepth == false)
        //            device.RenderState.DepthBufferWriteEnable = true;

        //        if (sceneNode.IfAlphaTestEnable == false)
        //            device.RenderState.AlphaTestEnable = recAlphatest;

        //        if (oldMipMapLevel != RecMipMapLevel)
        //        {
        //            device.SamplerStates[0].MipMapLevelOfDetailBias = RecMipMapLevel;
        //            oldMipMapLevel = RecMipMapLevel;
        //        }

        //        renderCount++;
        //    }
        //    return renderCount;
        //}

        void UpdateTime(SceneNode node)
        {
            #region 一直更新時間進去shader
            float totalEllapseMillisecond = (float)Environment.TickCount / 1000f;//1代表1秒
            if (oldTotalEllapseMillisecond != totalEllapseMillisecond)
            {
                oldTotalEllapseMillisecond = totalEllapseMillisecond;
                parTotalEllapsedMilliseonds.SetValue(totalEllapseMillisecond);
            }
            #endregion
        }

        void UpdateUVAnimation(SceneNode node)
        {
            #region 控制貼圖軸的動態ˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇ
            if (oldTranslateUVSinLoopSpeed != node.TranslateUVSinLoopSpeed)
            {
                oldTranslateUVSinLoopSpeed = node.TranslateUVSinLoopSpeed;
                parTranslateUVSinLoopSpeed.SetValue(node.TranslateUVSinLoopSpeed);
            }

            if (oldTranslateUVSpeed != node.TranslateUVSpeed)
            {
                oldTranslateUVSpeed = node.TranslateUVSpeed;
                parTranslateUVSpeed.SetValue(node.TranslateUVSpeed);
            }

            if (oldCenterScaleUV != node.centerScaleUV)
            {
                oldCenterScaleUV = node.centerScaleUV;
                parCenterScaleUV.SetValue(node.centerScaleUV);
            }
            #endregion
        }


    }


}
