﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using I_XNAUtility;

namespace I_XNAComponent
{
    public class MeshNode : SceneNode
    {
        public static MeshNode Create()
        {
            MeshNode nnn = new MeshNode();
            ReleaseCheck.AddCheck(nnn);
            return nnn;
            // return ReleaseCheck.CreateNew(typeof(MeshNode)) as MeshNode;
        }

        //提供給外部傳入處理過後的vertexbuffer繪出用。
        DynamicVertexBuffer m_DynamicVertBuffer = null;
        Array m_VBData = null;
        VertexDeclaration m_OutsideVertexDeclaration = null;
        int m_VertStride = 0;
        //IndexBuffer m_OutsideIndexBuffer = null;

        ModelMesh m_BasicModelMesh = null;//模型第一個mesh
        int m_PartID = -1;
        //    Actor m_PhysXActor = null;
        public override ModelMesh GetModelMeshPart(out int partID)
        {
            partID = m_PartID;



            return m_BasicModelMesh;
        }

        protected MeshNode()
        {
        }

        protected override void v_Dispose()
        {
            m_BasicModelMesh = null;
            m_DynamicVertBuffer = null;
            m_OutsideVertexDeclaration = null;

            m_MeshPartData.Dispose();
        }

        /// <summary>
        /// 建立一個mesh node需要的程序。傳入meshPartID表示要處理的part編號。
        /// </summary>  
        public static void ProcessMeshNodeInfo(string NodeName, Matrix transform, ModelMesh mesh, int meshPartID,
            MeshNode meshNode)
        {
            meshNode.NodeName = NodeName;
            meshNode.LocalMatrix = transform;

            meshNode.AnalyzeMeshTexture((Dictionary<string, object>)mesh.Tag, mesh, meshPartID);
            meshNode.AnalyzeMeshBoundingData((Dictionary<string, object>)mesh.Tag, out meshNode.m_IndexBufferSize);
        }

        /// <summary>
        /// 將此MeshNode裡面的模型取得IndexBuffer和VertexBuffer。
        /// 並利用SetIBVB()設定SceneNode底下的IndexBuffer和VertexBuffer資訊。
        /// </summary>
        public void GetBasicIBVBData()
        {
            if (m_BasicModelMesh != null)
            {
                List<uint> indexList = new List<uint>();
                List<Vector3> positionList = new List<Vector3>();
                Collision.GetMeshVBIB(m_BasicModelMesh, indexList, positionList, null, null, null, null);
                SetIBVB(indexList.ToArray(), positionList.ToArray());

                indexList.Clear();
                positionList.Clear();
                indexList = null;
                positionList = null;
            }
        }

        /// <summary>
        /// 將底下所有的MeshNode利用GetBasicIBVBData()，取得IndexBuffer和VertexBuffer。
        /// </summary>
        static public void SetAllTreeIBVBData(FrameNode rootNode)
        {
            MeshNode meshNode = rootNode as MeshNode;
            if (meshNode != null)
                meshNode.GetBasicIBVBData();

            FrameNode children = rootNode.FirstChild as FrameNode;
            while (children != null)
            {
                MeshNode.SetAllTreeIBVBData(children);
                children = children.Sibling as FrameNode;
            }
        }

        override public void AnalyzeMeshTexture(Dictionary<string, object> meshTag, ModelMesh mesh, int partID)
        {
            base.AnalyzeMeshTexture(meshTag, mesh, partID);
            m_BasicModelMesh = mesh;
            m_PartID = partID;

            ModelMeshPart part = m_BasicModelMesh.MeshParts[partID];
            m_MeshPartData = new RenderMeshPartData(m_BasicModelMesh, part);
        }

        //public override void Render(GraphicsDevice device, bool bForce, ref UInt32 renderTriangleAmount, ref Matrix View, ref Matrix Projection, ref Matrix InverseView)
        //{
        //    throw new NotImplementedException();
        //}

        //EffectRenderManager m_EffectRenderManager;
        //public void Render(GraphicsDevice device, EffectRenderManager effectRenderManager, 
        //    bool bForce, ref UInt32 renderTriangleAmount, ref Matrix View, ref Matrix Projection, ref Matrix InverseView)
        //{
        //    m_EffectRenderManager = effectRenderManager;

        //    if (ConsiderCurrentTechniqueOK(m_EffectRenderManager.CurrentRenderTechnique))
        //        base.Render(device, bForce, ref renderTriangleAmount, ref View, ref Projection, ref InverseView);
        //}


        /// <summary>
        /// 提供外部傳入自行處理後的vertexbuffer。
        /// </summary>
        public void SetOutSideVertexBuffer(DynamicVertexBuffer dvb, int vertStride, VertexDeclaration vdec, Array vbData)
        {
            m_VertStride = vertStride;
            m_DynamicVertBuffer = dvb;
            m_OutsideVertexDeclaration = vdec;
            m_VBData = vbData;
        }

        //ModelMeshPart m_MeshPart;

        public class RenderMeshPartData : System.IDisposable
        {
            public RenderMeshPartData(ModelMesh modelMesh, ModelMeshPart part)
            {
                m_StreamOffset = part.StreamOffset;
                m_VB = modelMesh.VertexBuffer;
                m_VertexStride = part.VertexStride;
                m_VDec = part.VertexDeclaration;
                m_IndexBuffer = modelMesh.IndexBuffer;
                m_BaseVertex = part.BaseVertex;
                m_NumVertices = part.NumVertices;
                m_StartIndex = part.StartIndex;
                m_PrimitiveCount = part.PrimitiveCount;
            }

            public void Dispose()
            {
                // m_StreamOffset =
                m_VB = null;
                //    m_VertexStride =
                m_VDec = null;
                m_IndexBuffer = null;
                //    m_BaseVertex =
                //     m_NumVertices =
                //   m_StartIndex =
                //     m_PrimitiveCount = null;
            }

            public int m_StreamOffset;
            public VertexBuffer m_VB;
            public int m_VertexStride;
            public VertexDeclaration m_VDec;
            public IndexBuffer m_IndexBuffer;
            public int m_BaseVertex, m_NumVertices, m_StartIndex, m_PrimitiveCount;
        }
        RenderMeshPartData m_MeshPartData;

        protected override void v_Render(EffectRenderManager effectRenderManager, GraphicsDevice device)
        {
            UpdateViewCullingBoxPose(this);

            if (effectRenderManager != null)
                effectRenderManager.SetMeshNodeEffectPar_Commit(WorldMatrix);

            //ModelMeshPart part = m_BasicModelMesh.MeshParts[m_PartID];

            ////如果有外部傳入給他畫的vertex buffer，就去畫外部傳入的。
            //if (m_DynamicVertBuffer != null)
            //    m_DynamicVertBuffer.SetData((VertexPositionNormalTexture[])m_VBData, 0, m_VBData.Length, SetDataOptions.NoOverwrite);

            //VertexBuffer vb = m_DynamicVertBuffer;
            //int vertexStride = m_VertStride;
            //if (vb == null)
            //{
            //    vb = m_BasicModelMesh.VertexBuffer;
            //    vertexStride = part.VertexStride;
            //}

            //VertexDeclaration vdec = m_OutsideVertexDeclaration;
            //if (vdec == null)
            //    vdec = part.VertexDeclaration;

            device.VertexDeclaration = m_MeshPartData.m_VDec;

            // Set vertex buffer and index buffer
            device.Vertices[0].SetSource(
               m_MeshPartData.m_VB, m_MeshPartData.m_StreamOffset,
                m_MeshPartData.m_VertexStride);
            device.Indices = m_MeshPartData.m_IndexBuffer;

            // And render all primitives
            device.DrawIndexedPrimitives(
                PrimitiveType.TriangleList,
                m_MeshPartData.m_BaseVertex, 0, m_MeshPartData.m_NumVertices,
                m_MeshPartData.m_StartIndex, m_MeshPartData.m_PrimitiveCount);

            device.VertexDeclaration = null;
            device.Indices = null;
        }

        //protected override void v_Render(EffectRenderManager effectRenderManager, GraphicsDevice device)
        //{
        //    UpdateViewCullingBoxPose(this);

        //    effectRenderManager.SetMeshNodeEffectPar_Commit(WorldMatrix);

        //    ModelMeshPart part = m_BasicModelMesh.MeshParts[m_PartID];

        //    //如果有外部傳入給他畫的vertex buffer，就去畫外部傳入的。
        //    if (m_DynamicVertBuffer != null)
        //        m_DynamicVertBuffer.SetData((VertexPositionNormalTexture[])m_VBData, 0, m_VBData.Length, SetDataOptions.NoOverwrite);

        //    VertexBuffer vb = m_DynamicVertBuffer;
        //    int vertexStride = m_VertStride;
        //    if (vb == null)
        //    {
        //        vb = m_BasicModelMesh.VertexBuffer;
        //        vertexStride = part.VertexStride;
        //    }

        //    VertexDeclaration vdec = m_OutsideVertexDeclaration;
        //    if (vdec == null)
        //        vdec = part.VertexDeclaration;

        //    device.VertexDeclaration = vdec;

        //    // Set vertex buffer and index buffer
        //    device.Vertices[0].SetSource(
        //       vb, part.StreamOffset,
        //        vertexStride);
        //    device.Indices = m_BasicModelMesh.IndexBuffer;

        //    // And render all primitives
        //    device.DrawIndexedPrimitives(
        //        PrimitiveType.TriangleList,
        //        part.BaseVertex, 0, part.NumVertices,
        //        part.StartIndex, part.PrimitiveCount);

        //    device.VertexDeclaration = null;
        //    device.Indices = null;
        //}

        //protected override bool v_ConsiderCurrentTechniqueOK(SceneNode.RenderTechnique CurrentRenderTechnique)
        //{
        //    if (MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.DepthMap))
        //    {
        //        if (MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.SkinnedMesh) ||
        //            MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.SkinnedTengentMesh) ||
        //            MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.SimpleInstanceMesh) ||
        //            MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.InstanceTengentMesh))
        //            return false;
        //        return true;//如果是畫depth map就畫吧！不用考慮太多...
        //    }
        //    else if (MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.SimpleMesh))
        //    {
        //        //若此物件是畫折射圖用的，就再考慮折射圖technique。
        //        bool bR = MyMath.ConsiderBit((uint)CurrentRenderTechnique, (uint)SceneNode.RenderTechnique.NormalRefractMap);
        //        if (RefractScale > 0 && bR == true)
        //            return true;
        //        else if (RefractScale == 0 && bR == false)
        //            return true;
        //        else
        //            return false;
        //    }
        //    return false;
        //}




    }


}
