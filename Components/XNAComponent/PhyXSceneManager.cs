﻿//using System;
//using System.Collections.Generic;
//using System.IO;

//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Audio;
//using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
//using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Net;
//using Microsoft.Xna.Framework.Storage;

//using I_XNAUtility;
//using StillDesign.PhysX;
//using HighMapEditor;
//using System.ComponentModel;

//using FilterMask = I_Com.FilterMask;

//namespace I_XNAComponent
//{
//    public abstract class PhyXSceneManager : RenderManager
//    {
//        static public GroupsMask GetGroupMask(FilterMask filter)
//        {
//            return MyPhyX.GetGroupMask(filter);
//        }

//        //protected bool RenderPhyXDebug = true;
//        private Engine _engine;
//        private Core _core;
//        private Scene _scene;
//        //MyObjectContainer m_VehicleList = null;

//        public Scene GetPhysXScene { get { return _scene; } }


//        new public void Initialize(IServiceProvider iServiceProvider, GraphicsDevice device, Game game, MySound sound)
//        {
//            try
//            {
//                _engine = new Engine(device);
//                _engine.Initalize();

//                // For convenience
//                _core = _engine.Core;
//                _scene = _engine.Scene;


//                //設定collision filter
//                _scene.SetFilterOperations((FilterOperation)1, (FilterOperation)1, (FilterOperation)0);//(or, or, and)
//                _scene.SetFilterBoolean(true);
//                GroupsMask zeroMask = new GroupsMask(0, 0, 0, 0);
//                _scene.SetFilterConstant0(zeroMask);
//                _scene.SetFilterConstant1(zeroMask);


//                PhyX_SetEnableDebugLine(false);
//            }
//            catch (Exception e)
//            {
//                Utility.ShowErrorMessage(e);
//            }

//            //m_VehicleList = new MyObjectContainer(128);

//            base.Initialize(iServiceProvider, device, game, sound);

//        }

//        new public void Dispose()
//        {
//            base.Dispose();//PhyX產生的Actor要在unload裡面釋放，才能把phyX核心釋放。

//            //for (int a = 0; a < m_VehicleList.NowUsedAmount; a++)
//            //    ((Vehicle)m_VehicleList.GetObjectArrayValue(a)).Dispose();
//            //m_VehicleList.Dispose();
//            //m_VehicleList = null;

//            //ThreadManager.ReleaseThreadID(ref PhyXThreadID);
//            //while (_scene.Materials.Count > 0)
//            //    _scene.Materials[0].Dispose();
//            _scene.Dispose();
//            _core.Dispose();
//            _engine.Dispose();
//            _engine = null;
//            _core = null;
//            _scene = null;
//        }

//        //int PhyXThreadID = -1;
//        protected object EngineLock = new object();
//        new public void Update()
//        {

//            lock (EngineLock)
//            {
//                //if (m_VehicleList != null)
//                //{
//                //    //車子vehicle裡面有轉方向盤，和輪胎力量與速度的計算需要update。
//                //    Vehicle vehicle = null;
//                //    for (int a = 0; a < m_VehicleList.NowUsedAmount; a++)
//                //    {
//                //        vehicle = m_VehicleList.GetObjectArrayValue(a) as Vehicle;
//                //        //vehicle.UpdateTireForceFunction(GetTime.ellipseUpdateMillisecond);
//                //        //vehicle.UpdateMotorTorque(GetTime.ellipseUpdateMillisecond);
//                //        //vehicle.UpdateTireStiffnessFactor(GetTime.ellipseUpdateMillisecond);
//                //    }
//                //}

//                //if (PhyXThreadID == -1)
//                //{
//                //    PhyXThreadID = ThreadManager.CreateThread(PhyXThreadFunction, null, "Phyx Update Thread 0.016", System.Threading.ThreadPriority.Highest);
//                //}

//                //physX update()....

//                _engine.Update(
//                    (float)I_GetTime.ellipseUpdateMillisecond / 1000f);//沒用多執行續確認frame rate就要丟入經過時間。
//                //0.016f);
//                //Utility.ElapsedTimeRatio );//(float)GetTime.ellipseUpdateMillisecond/1000f);//Utility.TargetElapsedTime);/


//                ////更新PhysX物件，給場景裡的node。
//                //IEnumerator<Actor> itr = _scene.Actors.GetEnumerator();
//                //while(itr.MoveNext())
//                //{
//                //    FrameNode node = itr.Current.UserData as FrameNode;
//                //    if (node != null)
//                //        node.WorldMatrix = Matrix.CreateScale(node.GetScale()) * itr.Current.GlobalPose;
//                //}
//                //itr.Dispose();
//            }

//            //先上面都算完，再做update
//            base.Update();
//        }

//        //public void PhyXThreadFunction(object o)
//        //{
//        //    while (true)
//        //    {
//        //        lock (EngineLock)
//        //        {
//        //            _engine.Update(0.016f);
//        //        }
//        //        ThreadManager.Sleep(16);//phyX用每秒60跑
//        //    }
//        //}

//        new public void Render()
//        {
//            base.Render();

//            //if (RenderPhyXDebug)

//            lock (EngineLock)
//            {
//                _engine.Draw(ref ViewMatrix, ref ProjectionMatrix);
//            }
//        }

//        //CCDSkeleton CreateCCDSkeleton(Vector3 size)
//        //{
//        //    CCDSkeleton ccdSkeletonForBox;

//        //    // Create a CCD Skeleton
//        //    {
//        //        //Vector3 size = sizenew Vector3(1, 1, 1);

//        //        int[] indices =
//        //        {
//        //            0, 1, 3,
//        //            0, 3, 2,
//        //            3, 7, 6,
//        //            3, 6, 2,
//        //            1, 5, 7,
//        //            1, 7, 3,
//        //            4, 6, 7,
//        //            4, 7, 5,
//        //            1, 0, 4,
//        //            5, 1, 4,
//        //            4, 0, 2,
//        //            4, 2, 6
//        //        };

//        //        Vector3[] vertices =
//        //        {
//        //            new Vector3( size.X, -size.Y, -size.Z ),
//        //            new Vector3( size.X, -size.Y, size.Z ),
//        //            new Vector3( size.X, size.Y, -size.Z ),
//        //            new Vector3( size.X,  size.Y,  size.Z ),
//        //            new Vector3( -size.X, -size.Y, -size.Z ),
//        //            new Vector3( -size.X, -size.Y, size.Z ),
//        //            new Vector3( -size.X,  size.Y, -size.Z ),
//        //            new Vector3( -size.X,  size.Y,  size.Z )
//        //        };

//        //        TriangleMeshDescription triangleMeshDesc = new TriangleMeshDescription();
//        //        triangleMeshDesc.AllocateVertices<Vector3>(vertices.Length);
//        //        triangleMeshDesc.AllocateTriangles<int>(indices.Length);

//        //        triangleMeshDesc.VerticesStream.SetData(vertices);
//        //        triangleMeshDesc.TriangleStream.SetData(indices);

//        //        triangleMeshDesc.VertexCount = vertices.Length;
//        //        triangleMeshDesc.TriangleCount = indices.Length / 3;

//        //        ccdSkeletonForBox = _core.CreateCCDSkeleton(triangleMeshDesc);

//        //        // Enable CCD and CCD Visualization
//        //        _core.SetParameter(PhysicsParameter.ContinuousCollisionDetection, true);
//        //        _core.SetParameter(PhysicsParameter.VisualizeContinuousCollisionDetectionTests, true);
//        //    }

//        //    return ccdSkeletonForBox;
//        //}

//        protected Actor PhyXCreatePlaneGround(Vector3 normal, float d, FilterMask filterMask)
//        {
//            Actor act = null;
//            lock (EngineLock)
//            {
//                act = MyPhyX.PhyXCreatePlaneGround(_scene, normal, d, filterMask);
//                //PlaneShapeDescription planeShapeDesc = new PlaneShapeDescription();
//                //planeShapeDesc.Normal = normal;
//                //planeShapeDesc.Distance = d;

//                //planeShapeDesc.GroupsMask = MyPhyX.GetGroupMask(filterMask);
//                //ActorDescription actorDesc = new ActorDescription();
//                ////actorDesc.Flags = ActorFlag.ContactModification;
//                //actorDesc.Shapes.Add(planeShapeDesc);

//                //act = CreateActor(_scene, actorDesc);
//            }
//            return act;
//        }

//        //class CarContactReport : UserContactReport
//        //{
//        //    public override void OnContactNotify(ContactPair contactInformation, ContactPairFlag events)
//        //    {
//        //    //    contactInformation.ActorA.GlobalPosition = new Vector3(0, 1000, 0);
//        //  //      contactInformation.ActorB. = new Vector3(0, 1000, 0);
//        //        contactInformation.ActorA.AngularVelocity = Vector3.Zero;
//        //    }
//        //};
//        //CarContactReport carContactReport = new CarContactReport();



//        /// <summary>
//        /// 釋放Actor專用
//        /// </summary>        
//        static public void DisposeActor(ref Actor actor)
//        {
//            MyPhyX.DisposeActor(ref actor);
//            //ReleaseCheck.DisposeCheckCount(actor);

//            //if (actor != null)
//            //    actor.Dispose();
//            //actor = null;
//        }

//        static public Actor CreateActor(Scene scene, ref ActorDescription actorDesc)
//        {
//            return MyPhyX.CreateActor(scene, ref actorDesc);
//            //Actor actor = scene.CreateActor(actorDesc);
//            //ReleaseCheck.AddOtherNew(actor);
//            //return actor;
//        }

//        protected Actor PhyXCreateBoxActor(Vector3[] size, Vector3[] localPosOffset,
//             float mass, FilterMask filterMask)
//        {
//            Actor act = null;
//            lock (EngineLock)
//            {
//                act = MyPhyX.PhyXCreateBoxActor(_scene, size, localPosOffset, mass, filterMask);
//            }
//            return act;
//        }

//        protected Actor PhyXCreateBoxActor(Vector3 size, float mass, FilterMask filterMask)
//        {
//            Vector3[] sss = new Vector3[1];
//            sss[0] = size;
//            //Vector3[] lll = new Vector3[1];
//            //lll[0] = Vector3.Zero;

//            Actor act = null;
//            lock (EngineLock)
//            {
//                act = MyPhyX.PhyXCreateBoxActor(_scene, sss, null, mass, filterMask);
//            }
//            return act;
//            //BoxShapeDescription boxShapeDesc = new BoxShapeDescription(size.X, size.Y, size.Z);
//            //boxShapeDesc.GroupsMask = GetGroupMask(filterMask);

//            //BodyDescription bodyDesc = new BodyDescription(mass);

//            //ActorDescription actorDesc = new ActorDescription()
//            //{
//            //    Name = "BoxShape",//String.Format("Box {0}", x),
//            //    BodyDescription = bodyDesc,
//            //    GlobalPose = Matrix.CreateTranslation( position),

//            //    Shapes = { boxShapeDesc },
//            //};

//            //Actor actor = CreateActor(_scene, actorDesc);

//            //bodyDesc.Dispose();
//            //bodyDesc = null;
//            //boxShapeDesc.Dispose();
//            //boxShapeDesc = null;
//            //actorDesc.Dispose();
//            //actorDesc = null;

//            //if (node != null)
//            //{
//            //    actor.Name = node.NodeName;
//            //    actor.UserData = node;
//            //}

//            //return actor;
//        }

//        protected Actor PhyXCreateSphereActor(FrameNode node, float radius, Vector3 position, float mass, FilterMask filterMask)
//        {
//            Actor act = null;
//            lock (EngineLock)
//            {
//                act = MyPhyX.PhyXCreateSphereActor(_scene, node, radius, position, mass, filterMask);
//            }
//            return act;

//            //SphereShapeDescription sphereShapeDesc = new SphereShapeDescription(radius);
//            //sphereShapeDesc.GroupsMask = GetGroupMask(filterMask);

//            //BodyDescription bodyDesc = new BodyDescription(mass);

//            //ActorDescription actorDesc = new ActorDescription()
//            //{
//            //    Name = "BoxShape",//String.Format("Box {0}", x),
//            //    BodyDescription = bodyDesc,
//            //    GlobalPose = Matrix.CreateTranslation(position),
//            //    Shapes = { sphereShapeDesc },
//            //};

//            //Actor actor = CreateActor(_scene, actorDesc);


//            //bodyDesc.Dispose();
//            //bodyDesc = null;
//            //sphereShapeDesc.Dispose();
//            //sphereShapeDesc = null;
//            //actorDesc.Dispose();
//            //actorDesc = null;

//            //if (node != null)
//            //{
//            //    actor.Name = node.NodeName;
//            //    actor.UserData = node;
//            //}
//            //return actor;
//        }

//        //protected Actor PhyXCreateStaticMesh(string name, Vector3[] vertices, uint[] indices, MyPhyX.FilterMask mask)
//        //{
//        //    Actor act = null;
//        //    lock (EngineLock)
//        //    {
//        //        GroupsMask groupMask = MyPhyX.GetGroupMask(mask);
//        //        act = MyPhyX.PhyXCreateStaticMesh(_scene, _core, name, vertices, indices, groupMask);
//        //    }
//        //    return act;
//        //}

//        protected Actor PhyXCreateStaticMesh(string name, Vector3[] vertices, uint[] indices, GroupsMask groupMask)
//        {
//            Actor act = null;
//            lock (EngineLock)
//            {
//                act = MyPhyX.PhyXCreateStaticMesh(_scene, _core, name, vertices, indices, groupMask);
//            }
//            return act;
//            //TriangleMeshDescription triangleMeshDesc = new TriangleMeshDescription();
//            //triangleMeshDesc.TriangleCount = indices.Length / 3;
//            //triangleMeshDesc.VertexCount = vertices.Length;

//            //triangleMeshDesc.AllocateTriangles<int>(triangleMeshDesc.TriangleCount);
//            //triangleMeshDesc.AllocateVertices<Vector3>(triangleMeshDesc.VertexCount);

//            //triangleMeshDesc.TriangleStream.SetData(indices);
//            //triangleMeshDesc.VerticesStream.SetData(vertices);

//            //MemoryStream s = new MemoryStream();

//            //Cooking.InitializeCooking();
//            //Cooking.CookTriangleMesh(triangleMeshDesc, s);
//            //Cooking.CloseCooking();

//            //s.Position = 0;
//            //TriangleMesh triangleMesh = _core.CreateTriangleMesh(s);

//            //MaterialDescription materialDesc = new MaterialDescription()
//            //{
//            //    Restitution = 0.5f,
//            //    StaticFriction = 0.1f,
//            //    DynamicFriction = 0.1f,
//            //    //DynamicFrictionV = 0.8f,
//            //    //StaticFrictionV = 1f,
//            //    //DirectionOfAnisotropy = new Vector3(0, 0, 1),
//            //    //Flags = MaterialFlag.Ansiotropic,
//            //};
//            //Material material = _scene.CreateMaterial(materialDesc);

//            //TriangleMeshShapeDescription triangleMeshShapeDesc = new TriangleMeshShapeDescription()
//            //{
//            //    TriangleMesh = triangleMesh,
//            //    Material = material,
//            //    GroupsMask = GetGroupMask(filterMask),
//            //};
//            ////triangleMeshShapeDesc.Flags |= ShapeFlag.Visualization;

//            //ActorDescription actorDesc = new ActorDescription()
//            //{              
//            //    //Name = "StaticMesh",
//            //    Shapes = { triangleMeshShapeDesc }
//            //};
//            //if (name != null)
//            //    actorDesc.Name = name;
//            //else
//            //    actorDesc.Name = "StaticMesh";

//            //Actor actor = CreateActor(_scene, actorDesc);


//            //triangleMeshDesc.Dispose();
//            //triangleMeshDesc = null;
//            //s.Dispose();
//            //s = null;
//            //triangleMeshShapeDesc.Dispose();
//            //triangleMeshShapeDesc = null;

//            //actor.Name = Utility.GetOppositeFilePath( name);

//            //return actor;
//        }

//        protected Actor PhyXCreateConvexMesh(Vector3[] vertices, Vector3 position, float mass,
//            FilterMask filterMask)
//        {
//            Actor act = null;
//            lock (EngineLock)
//            {
//                act = MyPhyX.PhyXCreateConvexMesh(_scene, _core, vertices, position, mass, filterMask);
//            }
//            return act;

//            //// Allocate memory for the points and triangles
//            //var convexMeshDesc = new ConvexMeshDescription()
//            //{
//            //    PointCount = vertices.Length
//            //};
//            //convexMeshDesc.Flags |= ConvexFlag.ComputeConvex;
//            //convexMeshDesc.AllocatePoints<Vector3>(vertices.Length);

//            //convexMeshDesc.PointsStream.SetData(vertices);


//            //// Cook to memory or to a file
//            //MemoryStream stream = new MemoryStream();
//            ////FileStream stream = new FileStream( @"Convex Mesh.cooked", FileMode.CreateNew );

//            //Cooking.InitializeCooking(new ConsoleOutputStream());
//            //Cooking.CookConvexMesh(convexMeshDesc, stream);
//            //Cooking.CloseCooking();

//            //stream.Position = 0;

//            //ConvexMesh convexMesh = _core.CreateConvexMesh(stream);

//            //ConvexShapeDescription convexShapeDesc = new ConvexShapeDescription(convexMesh)
//            //{
//            //    GroupsMask = GetGroupMask(filterMask),
//            //};

//            //ActorDescription actorDesc = new ActorDescription()
//            //{
//            //    Name = "ConvexMesh",
//            //    BodyDescription = new BodyDescription(mass),
//            //    GlobalPose = Matrix.CreateTranslation(position)
//            //};
//            //actorDesc.Shapes.Add(convexShapeDesc);

//            //Actor _torusActor = CreateActor(_scene, ref actorDesc);


//            //convexMeshDesc.Dispose();
//            //convexMeshDesc = null;
//            //stream.Dispose();
//            //stream = null;
//            //convexShapeDesc.Dispose();
//            //convexShapeDesc = null;
//            //actorDesc.Dispose();
//            //actorDesc = null;

//            //if (node != null)
//            //{
//            //    _torusActor.Name = node.NodeName;
//            //    _torusActor.UserData = node;
//            //}


//            //return _torusActor;
//        }



//        //protected Vehicle PhyXCreateVehicle(FrameNode BodyNode,
//        //    Vector3 position, Vehicle.VEHICLE_PAR v_par,
//        //    FilterMask filterMask)
//        //{
//        //    //Vehicle.VEHICLE_PAR v_par = Vehicle.VEHICLE_PAR.SetDefault();

//        //    Vehicle vehicle = new Vehicle(_scene, null,//CreateCCDSkeleton(new Vector3(50)),
//        //        v_par, filterMask);

//        //    vehicle.VehicleActor.GlobalPose = Matrix.CreateTranslation(position);
//        // //   vehicle.VehicleActor.Sleep();

//        //    if (BodyNode != null)
//        //    {
//        //        vehicle.VehicleActor.Name = BodyNode.NodeName;
//        //        vehicle.VehicleActor.UserData = BodyNode;
//        //    }

//        //    m_VehicleList.AddObject(vehicle);

//        //    //_scene.UserContactReport = carContactReport;
//        //    //vehicle.VehicleActor.ContactReportFlags = ContactPairFlag.OnEndTouchForceThreshold | ContactPairFlag.OnStartTouchForceThreshold;

//        //    ReleaseCheck.AddOtherNew(vehicle);            
//        //    return vehicle;
//        //}

//        //static protected void DisposeVehicle(ref Vehicle vehicle)
//        //{
//        //    ReleaseCheck.DisposeCheckCount(vehicle);

//        //    if (vehicle != null)
//        //        vehicle.Dispose();
//        //    vehicle = null;
//        //}

//        protected Actor PhyXCreateHeightField(string hmFilename, Material material, FilterMask filterMask)
//        {
//            Actor act = null;
//            lock (EngineLock)
//            {
//                GroupsMask groupMask = MyPhyX.GetGroupMask(filterMask);
//                act = PhyXCreateHeightField(hmFilename, material, groupMask);
//            }
//            return act;
//        }

//        static protected byte? PhyXHeightFieldHoleMaterialIndex;
//        protected Actor PhyXCreateHeightField(string hmFilename, Material material, GroupsMask groupMask)
//        {
//            Actor act = null;
//            lock (EngineLock)
//            {
//                act = MyPhyX.PhyXCreateHeightField(_scene, _core, hmFilename, material, groupMask);
//            }
//            return act;
//            //HighMapEditor.HighMap heightMap = new HighMapEditor.HighMap();
//            //heightMap.LoadHighMap(hmFilename);

//            ////MaterialDescription materialDesc = new MaterialDescription();
//            ////materialDesc.Restitution = heightMap.PhyXRestitution;
//            ////materialDesc.StaticFriction = heightMap.PhyXStaticFriction;
//            ////materialDesc.DynamicFriction = heightMap.PhyXDynamicFriction;
//            ////byte materialIndex = (byte)_scene.CreateMaterial(materialDesc).Index;
//            ////materialDesc.Dispose();
//            ////materialDesc = null;

//            ////建立一下洞的material
//            //if (PhyXHeightFieldHoleMaterialIndex.HasValue == false)
//            //{
//            //    MaterialDescription materialDesc = new MaterialDescription();
//            //    PhyXHeightFieldHoleMaterialIndex = (byte)_scene.CreateMaterial(materialDesc).Index;
//            //    materialDesc.Dispose();
//            //    materialDesc = null;
//            //}
//            ////建立一下洞的material           

//            //HeightFieldSample[] samples = new HeightFieldSample[heightMap.BloclSizeX * heightMap.BloclSizeZ];
//            //HighMapEditor.HighMap.HighmapData data;
//            //for (int r = 0; r < heightMap.BloclSizeX; r++)
//            //{
//            //    for (int c = 0; c < heightMap.BloclSizeZ; c++)
//            //    {
//            //        //HeightFieldSample sample = new HeightFieldSample();
//            //        //sample.Height = (short)h;
//            //        //sample.MaterialIndex0 = 0;
//            //        //sample.MaterialIndex1 = 1;
//            //        //sample.TessellationFlag = 0;

//            //        heightMap.GetHimapBlockData(r, c, out data);

//            //        if (data.bHasData == false)
//            //        {
//            //            samples[r * heightMap.BloclSizeZ + c].Height = short.MinValue;
//            //            samples[r * heightMap.BloclSizeZ + c].TessellationFlag = 0;
//            //            samples[r * heightMap.BloclSizeZ + c].MaterialIndex0 = PhyXHeightFieldHoleMaterialIndex.Value;
//            //            samples[r * heightMap.BloclSizeZ + c].MaterialIndex1 = PhyXHeightFieldHoleMaterialIndex.Value;
//            //        }
//            //        else
//            //        {
//            //            samples[r * heightMap.BloclSizeZ + c].Height = (short)data.m_HighY;
//            //            samples[r * heightMap.BloclSizeZ + c].TessellationFlag = 0;
//            //            //samples[r * heightMap.BloclSizeZ + c].MaterialIndex0 = materialIndex;
//            //            //samples[r * heightMap.BloclSizeZ + c].MaterialIndex1 = materialIndex;
//            //        }
//            //    }
//            //}

//            //HeightFieldDescription heightFieldDesc = new HeightFieldDescription()
//            //{
//            //    NumberOfRows = heightMap.BloclSizeX,
//            //    NumberOfColumns = heightMap.BloclSizeZ,
//            //    Samples = samples
//            //};

//            //HeightField heightField = _core.CreateHeightField(heightFieldDesc);

//            //HeightFieldShapeDescription heightFieldShapeDesc = new HeightFieldShapeDescription()
//            //{
//            //    HeightField = heightField,
//            //    HoleMaterial = PhyXHeightFieldHoleMaterialIndex.Value,
//            //    // The max height of our samples is short.MaxValue and we want it to be 1
//            //    //HeightScale = 1.0f / (float)short.MaxValue,
//            //    RowScale = heightMap.BlockWidth,
//            //    ColumnScale = heightMap.BlockHeight,

//            //    GroupsMask = GetGroupMask(filterMask),
//            //};

//            ////phyX的height map是以以左上角算(0,0)，我的height map左上角是MapMinX, Z。
//            //heightFieldShapeDesc.LocalPosition = new Vector3(heightMap.MapMinX, 0, heightMap.MapMinZ);
//            //    //new Vector3(
//            //    //    -0.5f * heightMap.BloclSizeX * 1 * heightFieldShapeDesc.RowScale, 
//            //    //    0,
//            //    //    -0.5f * heightMap.BloclSizeZ * 1 * heightFieldShapeDesc.ColumnScale);

//            //ActorDescription actorDesc = new ActorDescription()
//            //{

//            //    GlobalPose = Matrix.CreateTranslation(0, 0, 0),
//            //    Shapes = { heightFieldShapeDesc }
//            //};

//            //Actor actor = CreateActor(_scene, actorDesc);

//            //heightMap.Dispose();
//            //heightMap = null;

//            //actor.Name = Utility.GetOppositeFilePath(hmFilename);
//            //return actor;
//        }


//        //protected PhyX_RaycastHit PhyXRaycastClosestShape_StaticScene(Vector3 rayPos, Vector3 rayDirection)
//        //{
//        //    //if (rayDirection == Vector3.Zero)
//        //    //    return null;
//        //    //StillDesign.PhysX.Ray ray = new StillDesign.PhysX.Ray(rayPos, rayDirection);
//        //    //RaycastHit hit = _scene.RaycastClosestShape(ray, ShapesType.All);
//        //    //return hit;

//        //    return PhyXRaycastClosestShape_StaticScene(ref rayPos, ref rayDirection, float.MaxValue, FilterMask.All);
//        //}

//        //   protected RaycastHit PhyXRaycastClosestShape_AllScene(ref Vector3 rayPos, ref Vector3 rayDirection)
//        //   {
//        //       StillDesign.PhysX.Ray ray = new StillDesign.PhysX.Ray(rayPos, rayDirection);
//        //       return _scene.RaycastClosestShape(ray, ShapesType.All);
//        ////       return PhyXRaycastClosestShape(ref rayPos, ref rayDirection, _scene, ShapesType.All, maxDistance, mask);
//        //   }

//        protected RaycastHit PhyXRaycastClosestShape(ref Vector3 rayPos, ref Vector3 rayDirection,
//            ShapesType shapType, float maxDistance, FilterMask mask)
//        {
//            RaycastHit hit = null;
//            lock (EngineLock)
//            {
//                hit = PhyXRaycastClosestShape(ref rayPos, ref rayDirection, _scene, shapType, maxDistance, mask);
//            }

//            return hit;
//        }

//        static public RaycastHit PhyXRaycastClosestShape(ref Vector3 rayPos, ref Vector3 rayDirection, Scene scene,
//            ShapesType shapType, float maxDistance, FilterMask mask)
//        {

//            return MyPhyX.PhyXRaycastClosestShape(ref rayPos, ref rayDirection, scene,
//            shapType, maxDistance, mask);
//            //if (rayDirection == Vector3.Zero)
//            //    return null;
//            //StillDesign.PhysX.Ray ray = new StillDesign.PhysX.Ray(rayPos, rayDirection);
//            //GroupsMask GMask = GetGroupMask(mask);
//            //RaycastHit hit = scene.RaycastClosestShape(ray, shapType, 0xffffffff, maxDistance, 0xffffffff, GMask);

//            //return hit;
//        }

//        /// <summary>
//        /// 設定是否要抓取phyX除錯線。
//        /// </summary>
//        protected void PhyX_GetDebugLineOnce()
//        {
//            _engine.GetDebugLineOnce();
//            //_engine.SetDrawDebugLine(bbb);
//        }

//        public void PhyX_SetEnableDebugLine(bool bbb)
//        {
//            _engine.SetEnableDebugLine(bbb);
//        }

//        /// <summary>
//        /// 設定是否啟動PhyX內部產生debug line的參數控制。
//        /// </summary>
//        //protected void PhyX_EnableDebugLine(bool bbb)
//        //{
//        //    _engine.SetDrawDebugLine(bbb);
//        //}

//        /// <summary>
//        /// 設定是否啟動內部將phyX的debug line繪出的函式，若為false則會跳過該函式。
//        /// </summary>
//        protected void PhyX_SetRenderDebugLine(bool bbb)
//        {
//            _engine.SetRenderDebugLine(bbb);
//        }

//        /// <summary>
//        /// 取得內部phyX產生的debug line，當想要自己畫出phyX除錯線條時可以用。
//        /// </summary>
//        protected DebugLine[] PhyX_GetDebugLine()
//        {
//            return _engine.GetDebugLine();
//        }

//        protected void PhyX_SetGravity(Vector3 gravity)
//        {
//            _engine.SetGravity(ref gravity);
//        }


//        public Shape[] ConsiderViewFrustumCulling(Scene scene, ShapesType shapType,
//            FilterMask mask,
//            ref Matrix VxP)
//        {
//            return MyPhyX.ConsiderViewFrustumCulling(_scene, shapType, mask, ref VxP);
//        }
//    }
//}
