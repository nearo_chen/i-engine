﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        /// <summary>
        /// 作SSAO畫面處理，會將Gbuffer的OtherMap拿來畫AO圖。
        ///  回傳true或false表示是否要畫SSAO處理
        /// </summary>    
        protected bool DrawSSAOToOtherMap(int sceneID, Texture2D PSSMOcclusion)
        {
            I_SceneManager scene = GetScene(sceneID);

            I_Camera camera = scene.GetMainCamera();
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return false;

            if (camera.SSAOSampleRadius <= 0)
                return false;

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;


            m_GraphicDevice.RenderState.AlphaTestEnable = false;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.DepthBufferEnable = false;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = false;

            #region Draw SSAO to m_OtherMap
            m_GraphicDevice.SetRenderTarget(0, m_OtherMap);//拿m_OtherMap來用

            if (PSSMOcclusion != null)
            {
                m_SSAOEffect.Parameters["IfPSSMOcclusionMap"].SetValue(true);
                m_SSAOEffect.Parameters["PSSMOcclusionMap"].SetValue(PSSMOcclusion);         
            }
            else
                m_SSAOEffect.Parameters["IfPSSMOcclusionMap"].SetValue(false);

            //m_SSAOEffect.Parameters["sampleTimes"].SetValue(camera.SSAOSampleTime); //取樣的for迴圈數
            m_SSAOEffect.Parameters["sampleLOD"].SetValue(camera.SSAOSampleLOD);//周圍SAMPLING中由深度圖LOD中取得深度，由LOD越大值去SAMPLE速度會較快。
            m_SSAOEffect.Parameters["SSAOFadeOutDistance"].SetValue(camera.SSAOFadeOutDistance);
            m_SSAOEffect.Parameters["sampleRadius"].SetValue(camera.SSAOSampleRadius);
            m_SSAOEffect.Parameters["distanceScale"].SetValue(camera.SSAODistanceScale);
            m_SSAOEffect.Parameters["InvertProjection"].SetValue(Matrix.Invert(projectionMat));
            m_SSAOEffect.Parameters["Projection"].SetValue(projectionMat);
            m_SSAOEffect.Parameters["depthTexture"].SetValue(GetTexture_DepthRT());
            m_SSAOEffect.Parameters["randomNormalTexture"].SetValue(m_RandomNormalTexture);

            m_SSAOEffect.Parameters["halfPixel"].SetValue(halfPixel);
            m_SSAOEffect.CommitChanges();

            m_SSAOEffect.Begin();
            m_SSAOEffect.Techniques[0].Passes[0].Begin();
            //draw a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
            m_SSAOEffect.Techniques[0].Passes[0].End();
            m_SSAOEffect.End();

            m_GraphicDevice.SetRenderTarget(0, null);
            #endregion

          

            //#region Combine AO MAP to m_ColorRT
            //if (renderRT == RenderRT.Backbuffer)
            //    m_GraphicDevice.SetRenderTarget(0, null);
            //else if (renderRT == RenderRT.ColorRT)
            //    m_GraphicDevice.SetRenderTarget(0, m_ColorRT);

            //SSAOMap = GetTexture_OtherMap();
            //BlendTextureToScene(SSAOMap, sceneTexture, 
            //#endregion


            return true;
        }

        /// <summary>
        /// OtherMap做Blur畫在OtherMap上，blur時考慮normal，判斷取樣點法線平行Kernel法線的的取樣。
        /// </summary>
        protected void BlurOtherMapConsiderNormal(float BlurScale)
        {
            //blur共用
            m_SSAOBlurEffect.Parameters["halfPixel"].SetValue(halfPixel);
            //m_SSAOBlurEffect.Parameters["normalMap"].SetValue(GetTexture_NormalRT());

            #region Blur SSAO to Other Map Horizonal
            Texture2D OcclusionMap = GetTexture_OtherMap();
            m_GraphicDevice.SetRenderTarget(0, m_OtherMap);
            m_GraphicDevice.Clear(Color.White);

            m_SSAOBlurEffect.Parameters["SSAOTexture"].SetValue(OcclusionMap);
            //m_SSAOBlurEffect.Parameters["normalMap"].SetValue(GetTexture_NormalRT());
            m_SSAOBlurEffect.Parameters["blurDirection"].SetValue(new Vector2(1f / (float)OcclusionMap.Width * BlurScale, 0));

            //m_SSAOBlurEffect.Parameters["halfPixel"].SetValue(halfPixel);
            m_SSAOBlurEffect.CommitChanges();

            m_SSAOBlurEffect.Begin();
            m_SSAOBlurEffect.Techniques[0].Passes[0].Begin();
            //draw a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
            m_SSAOBlurEffect.Techniques[0].Passes[0].End();
            m_SSAOBlurEffect.End();

            m_GraphicDevice.SetRenderTarget(0, null);
            #endregion

            #region Blur SSAO to Other Map Vertical
            OcclusionMap = GetTexture_OtherMap();
            m_GraphicDevice.SetRenderTarget(0, m_OtherMap);
            m_GraphicDevice.Clear(Color.White);

            m_SSAOBlurEffect.Parameters["SSAOTexture"].SetValue(OcclusionMap);
            //m_SSAOBlurEffect.Parameters["normalMap"].SetValue(GetTexture_NormalRT());
            m_SSAOBlurEffect.Parameters["blurDirection"].SetValue(new Vector2(0, 1f / (float)OcclusionMap.Height * BlurScale));

            //m_SSAOBlurEffect.Parameters["halfPixel"].SetValue(halfPixel);
            m_SSAOBlurEffect.CommitChanges();

            m_SSAOBlurEffect.Begin();
            m_SSAOBlurEffect.Techniques[0].Passes[0].Begin();
            //draw a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
            m_SSAOBlurEffect.Techniques[0].Passes[0].End();
            m_SSAOBlurEffect.End();

            m_GraphicDevice.SetRenderTarget(0, null);
            #endregion
        }

        /// <summary>
        /// 將AOTexture當作影子，畫素直接成上sceneTexture，畫在renderRT上。
        /// </summary>
        protected void BlendAOToScene(Texture2D AOTexture, Texture2D sceneTexture, int width, int height, RenderRT renderRT)
        {
            if (renderRT == RenderRT.Backbuffer)
                m_GraphicDevice.SetRenderTarget(0, null);
            else if (renderRT == RenderRT.BaseRT)
                m_GraphicDevice.SetRenderTarget(0, GetBaseRT());
            m_GraphicDevice.SetRenderTarget(1, null);
            m_GraphicDevice.SetRenderTarget(2, null);
            m_GraphicDevice.SetRenderTarget(3, null);

            m_SSAOFinalEffect.Parameters["SSAOTex"].SetValue(AOTexture);


            m_SpriteBatch.Begin(SpriteBlendMode.None,
                              SpriteSortMode.Immediate,
                              SaveStateMode.None);

            m_SSAOFinalEffect.Begin();
            m_SSAOFinalEffect.CurrentTechnique.Passes[0].Begin();

            // Draw the quad.
            m_SpriteBatch.Draw(sceneTexture, new Rectangle(0, 0, width, height), Color.White);
            m_SpriteBatch.End();

            // End the custom effect.
            m_SSAOFinalEffect.CurrentTechnique.Passes[0].End();
            m_SSAOFinalEffect.End();

            m_GraphicDevice.Textures[0] = null;
            m_GraphicDevice.Textures[1] = null;
        }
    }
}
