﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;
using StillDesign.PhysX;

namespace I_Component
{
    public partial class PhysXParticle
    {
        StillDesign.PhysX.Actor m_actor;
        Vector3 m_ParticleForce;
        DateTime createdTime;
        TimeSpan lifeTimer_millisecond;

        /// <summary>
        /// 設定粒子的相關資料
        /// </summary>
        /// <param name="lifeTime">生存時間</param>
        public PhysXParticle(int lifeTime)
        {
            SetActor(null);
            m_ParticleForce = Vector3.Zero;
            lifeTimer_millisecond = new TimeSpan(0, 0, 0, 0, lifeTime);
            createdTime = DateTime.Now;
        }

        /// <summary>
        /// 設定粒子的相關資料
        /// </summary>
        /// <param name="setActor"></param>
        /// <param name="thermoDynamicAcceleration"></param>
        /// <param name="lifeTime">生存時間</param>
        public PhysXParticle(Actor setActor, Vector3 thermoDynamicAcceleration, int lifeTime)
        {
            SetActor(setActor);
            m_ParticleForce = setActor.Mass * thermoDynamicAcceleration;
            lifeTimer_millisecond = new TimeSpan(0, 0, 0, 0, lifeTime);
            createdTime = DateTime.Now;
        }

        /// <summary>
        /// 設定此顆粒子的Actor
        /// </summary>
        /// <param name="actor"></param>
        void SetActor(Actor actor)
        {
            m_actor = actor;
        }

        public void UpdateParticle()
        {
            if (m_actor.IsDisposed == false)
                m_actor.AddForce(m_ParticleForce, ForceMode.Force);
        }

        public void RemoveParticle()
        {
            m_actor.Dispose();
        }

        /// <summary>
        /// 此顆粒子是否應該要掛了
        /// </summary>
        /// <returns></returns>
        public bool isTimesupForParticle()
        {
            if (DateTime.Now.Subtract(createdTime) > lifeTimer_millisecond)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 取得此顆粒子的Actor
        /// </summary>
        /// <returns></returns>
        public Actor getPhysXParticle_Actor()
        {
            return m_actor;
        }
    }

    public partial class PhysXParticleEmitter : System.IDisposable
    {
        public int m_ParticleID = 0;

        public string m_ParticleName = "Particle";
        //要用的參數
        int m_ParticleCount = 0;
        //頭索引
        int m_HeadIndex = 0;
        //尾索引
        int m_TailIndex = 0;

        //隨機用參數 最大值
        const float randomMax = 10000.0f;

        float m_ParticleVelocityScale;
        //最大數量
        public int m_ParticleMaxmum { get; set; }

        //所有的Particle
        PhysXParticle[] m_ParticleList = null;

        DateTime nowTime;
        //新增當時時間
        DateTime addedTime;
        //經過的時間
        TimeSpan passTime;

        int DurationTime = 5000;
        DateTime StartTime;

        //質量
        public float m_mass { get; set; }
        //密度
        public float m_densify { get; set; }
        //圓大小
        float m_radius = .1f;
        //建構式輸入參數
        Scene m_Scene;
        public Vector3 m_ParticleStartPosition { get; set; }
        Vector3 m_ParticleVelocity;
        public Vector3 m_ParticleAcceleration { get; set; }
        public int m_particleLife_millisecond { get; set; }

        void SetParticleStartingVelocity(float startingVelocity)
        {
            m_ParticleVelocityScale = (1.0f / randomMax) * startingVelocity;
        }

        /// <summary>
        /// 建立粒子發射器
        /// </summary>
        /// <param name="setScene">場景</param>
        /// <param name="startingPosition">起始位置</param>
        /// <param name="particleMaxmum">粒子最大數量</param>
        /// <param name="persecondParticles">每秒生幾個粒子</param>
        /// <param name="startingVelocity">起始速度</param>
        /// <param name="particleAcceleration">加速</param>
        /// <param name="particleGroup"></param>
        /// <param name="mass">質量</param>
        /// <param name="densify">密度</param>
        /// <param name="life_millisecond">生命週期</param>
        public PhysXParticleEmitter(Scene setScene, Vector3 startingPosition, int particleMaxmum, float persecondParticles,
            float startingVelocity, Vector3 particleAcceleration, short particleGroup,
            float mass, float densify,
            int life_millisecond)
        {
            m_ParticleCount = 0;
            m_HeadIndex = 0;
            m_TailIndex = 0;
            m_Scene = setScene;
            m_ParticleStartPosition = startingPosition;
            if (persecondParticles == 0)
                return;
            passTime = new TimeSpan(0, 0, 0, 0, (int)(1000.0f / persecondParticles));
            m_ParticleMaxmum = particleMaxmum;
            m_ParticleList = new PhysXParticle[m_ParticleMaxmum];
            m_ParticleVelocityScale = (1.0f / randomMax) * startingVelocity;
            m_ParticleAcceleration = particleAcceleration;
            m_mass = mass;
            m_densify = densify;
            m_particleLife_millisecond = life_millisecond;
            addedTime = DateTime.Now;
            setScene.SetGroupCollisionFlag(particleGroup, particleGroup, true);
            Random rd = new Random(Guid.NewGuid().GetHashCode());
            m_ParticleID = rd.Next();
            StartTime = DateTime.Now;
        }

        /// <summary>
        /// 取得particle內部資訊輸出設定格式資料
        /// 根據I_PhysXParticle資訊取得I_PhysXParticleData，可以去做存檔。
        /// </summary>
        public I_SpecialMeshData.I_PhysXParticleData GetPhysXParticleData()
        {
            I_SpecialMeshData.I_PhysXParticleData data = new I_SpecialMeshData.I_PhysXParticleData(m_ParticleID,
                m_ParticleName, m_ParticleMaxmum, (int)(1000.0f / passTime.Milliseconds), m_ParticleStartPosition,
                m_ParticleVelocityScale, m_ParticleAcceleration, m_particleLife_millisecond, m_mass, m_densify);
            return data;
        }

        public void SetPhysXParticleData(I_SpecialMeshData.I_PhysXParticleData particleData)
        {
            m_ParticleName = particleData.ParticleName;
            m_ParticleMaxmum = particleData.ParticleMaxmum;
            passTime = new TimeSpan(0, 0, 0, 0, (int)(1000.0f / particleData.PersecondParticles));
            m_ParticleStartPosition = particleData.ParticleStartPosition;
            m_ParticleVelocityScale = particleData.StartingVelocity;
            m_ParticleAcceleration = particleData.ParticleAcceleration;
            m_particleLife_millisecond = particleData.ParticleLife_millisecond;
            m_mass = particleData.Mass;
            m_densify = particleData.Densify;
        }

        ~PhysXParticleEmitter()
        {
            Dispose();
        }

        public void Dispose()
        {
            while (m_ParticleCount > 0)
            {
                RemoveParticle();
            }
        }

        public void ParticleEmitterUpdate()
        {
            nowTime = DateTime.Now;
            if (nowTime.Subtract(addedTime) > passTime && DateTime.Now.Subtract(StartTime) < TimeSpan.FromMilliseconds((double)DurationTime))
            {
                ParticleEmitterAdd();
                addedTime = nowTime;
            }

            int updateCount = 0;
            for (int i = m_TailIndex; updateCount < m_ParticleCount; i = (i + 1) % m_ParticleMaxmum)
            {
                m_ParticleList[i].UpdateParticle();
                ++updateCount;
                if (m_ParticleList[i] != null && m_ParticleList[i].isTimesupForParticle())
                {
                    RemoveParticle();
                    --updateCount;
                }
            }
        }

        void ParticleEmitterAdd()
        {
            //滿了就不能增加  先移除再增加
            if ((m_HeadIndex == m_TailIndex) && m_ParticleCount != 0)
            {
                RemoveParticle();
            }

            ActorDescription actorDesc = new ActorDescription();
            BodyDescription bodyDesc = new BodyDescription();
            SphereShapeDescription sphereDesc = new SphereShapeDescription(m_radius);

            actorDesc.Shapes.Add(sphereDesc);

            actorDesc.BodyDescription = bodyDesc;
            actorDesc.Density = 10.0f;
            actorDesc.GlobalPose = Matrix.CreateTranslation(m_ParticleStartPosition);

            Actor m_newActor = m_Scene.CreateActor(actorDesc);
            //設定碰撞旗號
            m_newActor.Shapes[0].Group = 1;
            Random rd = new Random(Guid.NewGuid().GetHashCode());
            m_ParticleVelocity = new Vector3((rd.Next((int)randomMax) - randomMax / 2) * m_ParticleVelocityScale, (rd.Next((int)randomMax) - randomMax / 2) * m_ParticleVelocityScale, (rd.Next((int)randomMax) - randomMax / 2) * m_ParticleVelocityScale);
            m_newActor.LinearVelocity = m_ParticleVelocity;

            m_ParticleList[m_HeadIndex] = new PhysXParticle(m_newActor, m_ParticleAcceleration, m_particleLife_millisecond);

            m_HeadIndex = (m_HeadIndex + 1) % m_ParticleMaxmum;
            ++m_ParticleCount;

        }

        void RemoveParticle()
        {
            m_ParticleList[m_TailIndex].RemoveParticle();
            m_TailIndex = (m_TailIndex + 1) % m_ParticleMaxmum;
            --m_ParticleCount;
        }

        public List<Vector3> GetAllDebugPos()
        {
            List<Vector3> sphereListPos = new List<Vector3>();
            foreach (PhysXParticle particle in m_ParticleList)
            {
                if (particle != null && particle.getPhysXParticle_Actor().IsDisposed != true)
                    sphereListPos.Add(particle.getPhysXParticle_Actor().GlobalPosition);
            }
            return sphereListPos;
        }
    }
}
