﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        /// <summary>
        /// 根據GBuffer畫出emmisive map，順便將emmisive map做bloom處理。
        /// 以後可以在這控制emmisive的bloom程度。
        /// </summary>
        protected void DrawEmmisiveMap()
        {
            m_GraphicDevice.RenderState.AlphaTestEnable = false;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.DepthBufferEnable = false;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = false;

            m_GraphicDevice.SetRenderTarget(0, m_EmmisiveRT);

            //clear all components to 0
            m_GraphicDevice.Clear(Color.Black);

            m_EmmisiveEffect.Parameters["colorMap"].SetValue(m_ColorRT.GetTexture());
            m_EmmisiveEffect.Parameters["otherMap"].SetValue(m_OtherMap.GetTexture());
            m_EmmisiveEffect.Parameters["halfPixel"].SetValue(halfPixel);
            m_EmmisiveEffect.CommitChanges();

            m_EmmisiveEffect.Begin();
            m_EmmisiveEffect.Techniques[0].Passes[0].Begin();
            //draw a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
            m_EmmisiveEffect.Techniques[0].Passes[0].End();
            m_EmmisiveEffect.End();

            m_GraphicDevice.SetRenderTarget(0, null);

            Texture2D emmMap = m_EmmisiveRT.GetTexture();
            RenderBloom(emmMap, m_EmmisiveRT);
        }

        /// <summary>
        /// 將整個畫面做Gausian Blur之後傳入DOF shader裡面，根據深度做景深模糊
        /// </summary>
        protected bool DrawDepthOfFieldMap(int sceneID, Texture2D sceneTexture, RenderRT renderRT)
        {
            I_SceneManager scene = GetScene(sceneID);

            I_Camera camera = scene.GetMainCamera();
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return false;
            if (camera.DOFIntensity <= 0)
                return false;

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;

            m_GraphicDevice.RenderState.AlphaTestEnable = false;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.DepthBufferEnable = false;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = false;

            //做blur圖
            m_MyBloom.ImportTextureToRenderTarget1(sceneTexture);
            m_MyBloom.RenderBothCaussianBlur();
            Texture2D blureTexture = m_MyBloom.GetTexture_RenderTarget1();

            //開始處理DOF
            if (renderRT == RenderRT.Backbuffer)
                m_GraphicDevice.SetRenderTarget(0, null);
            else if (renderRT == RenderRT.BaseRT)
                m_GraphicDevice.SetRenderTarget(0, GetBaseRT());

            float DOFNear = camera.DOFFocalDistance - camera.DOFFocalRange;
            float DOFFar = camera.DOFFocalDistance + camera.DOFFocalRange;
            m_DOFCombineEffect.Parameters["DOFStart"].SetValue(DOFNear);
            m_DOFCombineEffect.Parameters["DOFEnd"].SetValue(DOFFar);
            m_DOFCombineEffect.Parameters["DOFRange"].SetValue(camera.DOFDecayRange);
            m_DOFCombineEffect.Parameters["DOFIntensity"].SetValue(camera.DOFIntensity);

            //需要座標
            m_DOFCombineEffect.Parameters["halfPixel"].SetValue(halfPixel);
            m_DOFCombineEffect.Parameters["InvertProjection"].SetValue(Matrix.Invert(projectionMat));
            m_DOFCombineEffect.Parameters["DepthMap"].SetValue(GetTexture_DepthRT());

            m_DOFCombineEffect.Parameters["BlurTexture"].SetValue(blureTexture);
            m_DOFCombineEffect.Parameters["BaseTexture"].SetValue(sceneTexture);

            m_DOFCombineEffect.CommitChanges();

            m_DOFCombineEffect.Begin();
            m_DOFCombineEffect.Techniques[0].Passes[0].Begin();

            //render a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);

            m_DOFCombineEffect.Techniques[0].Passes[0].End();
            m_DOFCombineEffect.End();

            return true;
        }




        /// <summary>
        /// 將GBuffer全部做計算，畫在BaseRT()裡，另外還做霧的計算。
        /// </summary>
        protected void CombineBuffers(int sceneID)
        {
            m_GraphicDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            m_GraphicDevice.RenderState.DepthBufferEnable = false;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = false;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.AlphaTestEnable = false;

            I_SceneManager scene = GetScene(sceneID);

            I_Camera camera = scene.GetMainCamera();
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return;

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;

            Texture2D colorTexture = m_ColorRT.GetTexture();
            m_GraphicDevice.SetRenderTarget(0, GetBaseRT());
            m_GraphicDevice.Clear(Color.CornflowerBlue);

            //Vector2 hhh;
            //hhh.X = 0.5f / (float)camera.BackBufferW;
            //hhh.Y = 0.5f / (float)camera.BackBufferH;
            //set the effect parameters

            //畫PSSM
            if (IfDrawPSSMLevel)
            {
                m_FinalCombineEffect.Parameters["ShowPSSMLevel"].SetValue(camera.PSSMLevelAmount);
                m_FinalCombineEffect.Parameters["InvertViewProjection"].SetValue(Matrix.Invert(viewMatrix * projectionMat));
                m_FinalCombineEffect.Parameters["PSSMMap0Frustum"].SetValue(m_PSSMSceneDependentFrustum[0]);
                m_FinalCombineEffect.Parameters["PSSMMap1Frustum"].SetValue(m_PSSMSceneDependentFrustum[1]);
                m_FinalCombineEffect.Parameters["PSSMMap2Frustum"].SetValue(m_PSSMSceneDependentFrustum[2]);
                m_FinalCombineEffect.Parameters["PSSMMap3Frustum"].SetValue(m_PSSMSceneDependentFrustum[3]);
            }
            else
                m_FinalCombineEffect.Parameters["ShowPSSMLevel"].SetValue(0);

            //需要座標算FOG
            m_FinalCombineEffect.Parameters["InvertProjection"].SetValue(Matrix.Invert(projectionMat));
            m_FinalCombineEffect.Parameters["depthMap"].SetValue(GetTexture_DepthRT());

            //fog的參數
            Vector3 fogColor;
            float FogStart, FogEnd, FogMaxRatio;
            scene.GetFog(out fogColor, out FogStart, out FogEnd, out FogMaxRatio);
            m_FinalCombineEffect.Parameters["FogEnd"].SetValue(FogEnd);
            m_FinalCombineEffect.Parameters["FogStart"].SetValue(FogStart);
            m_FinalCombineEffect.Parameters["FogMaxRatio"].SetValue(FogMaxRatio);
            m_FinalCombineEffect.Parameters["FogColor"].SetValue(fogColor);
            m_FinalCombineEffect.Parameters["cameraPosition"].SetValue(cameraPosition);


            m_FinalCombineEffect.Parameters["colorMap"].SetValue(colorTexture);
            m_FinalCombineEffect.Parameters["lightMap"].SetValue(m_LightRT.GetTexture());
            m_FinalCombineEffect.Parameters["halfPixel"].SetValue(halfPixel);

            m_FinalCombineEffect.CommitChanges();

            m_FinalCombineEffect.Begin();
            m_FinalCombineEffect.Techniques[0].Passes[0].Begin();

            //render a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);

            m_FinalCombineEffect.Techniques[0].Passes[0].End();
            m_FinalCombineEffect.End();

            m_GraphicDevice.SetRenderTarget(0, null);
            m_GraphicDevice.SetRenderTarget(1, null);

            Texture2D finalScene = GetTexture_BaseRT(); //m_ColorRT.GetTexture();

            if (I_Utility.IfDrawDebug)
            {
                RestoreBuffersBaseRT(false, finalScene);//將depth Buffer寫回去


                SetAlphaBlendRenderState(m_GraphicDevice);

                //這是把之前設定的都畫出來，應為之前是deferred shading，DeferredShading_Begin()有DrawLine.Begin()
                DrawLine.Draw(viewMatrix, projectionMat);

                DrawDebugLine_DirectionalLight(sceneID, camera);
                DrawDebugLine_PointLight(sceneID, camera);
                DrawDebugLine_SpotLight(sceneID, camera);
                DrawDebugLine_PhysXParticle(sceneID, camera);
                DrawDebugLine_PhysXFluids(sceneID, camera);

                if (IfDrawCullingPhysicsDebugLine)
                    scene.DrawCullingPhysicsDebugLine(m_GraphicDevice, viewMatrix, projectionMat);

                m_GraphicDevice.SetRenderTarget(0, null);
                m_GraphicDevice.SetRenderTarget(1, null);
            }



        }

        /// <summary>
        /// 是否畫物理除錯線
        /// </summary>
        public bool IfDrawCullingPhysicsDebugLine = false;

        /// <summary>
        /// 是否畫PSSM的depth map
        /// </summary>
        public bool IfDrawPSSMMap = false;

        /// <summary>
        /// 是否將PSSM的層級畫出來
        /// </summary>
        public bool IfDrawPSSMLevel = false;

        //public static float DepthBias = 0;
        //void DrawDrawLine(Matrix viewProj)
        //{
        //    SetAlphaBlendRenderState(m_GraphicDevice);
        //    //  m_GraphicDevice.RenderState.DepthBias = -0.00001f;
        //    DrawLine.DeferredDraw(viewProj, GetTexture_DepthRT(), m_DeferredDrawLine, DepthBias);
        //    //  m_GraphicDevice.RenderState.DepthBias = 0;
        //}

        /// <summary>        
        /// 此function會m_GraphicDevice.SetRenderTarget(0, rt)，設定等等要畫的RT，並回存顯卡深度緩衝。
        /// Restore資料回存完畢，後面就可以做基本的畫圖動作。
        /// </summary>
        void RestoreDepthOnly(RenderTarget2D rt)
        {
            m_GraphicDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            m_GraphicDevice.RenderState.DepthBufferEnable = true;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = true;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.AlphaTestEnable = false;

            m_GraphicDevice.SetRenderTarget(0, rt);
            m_GraphicDevice.SetRenderTarget(1, null);
            m_GraphicDevice.SetRenderTarget(2, null);
            m_GraphicDevice.SetRenderTarget(3, null);
            m_RestoreDepthEffect.CurrentTechnique = m_RestoreDepthEffect.Techniques["RestoreDepth"];

            m_RestoreDepthEffect.Parameters["halfPixel"].SetValue(halfPixel);
            m_RestoreDepthEffect.Parameters["DepthTexture"].SetValue(GetTexture_DepthRT());

            m_RestoreDepthEffect.CommitChanges();
            m_RestoreDepthEffect.Begin();
            m_RestoreDepthEffect.Techniques[0].Passes[0].Begin();

            //render a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);

            m_RestoreDepthEffect.Techniques[0].Passes[0].End();
            m_RestoreDepthEffect.End();

        }

        /// <summary>
        ///將目前畫面貼圖以及GBuffer的深度圖(depthMap)寫回BaseRT()以及顯卡深度緩衝
        ///bEmmisive，表示要寫回emmisive map。      
        ///此function會m_GraphicDevice.SetRenderTarget(0, m_ColorRT)或m_GraphicDevice.SetRenderTarget(1, m_EmmisiveRT);
        ///Restore資料回存完畢，後面就可以做基本的畫圖動作。
        /// </summary>
        protected void RestoreBuffersBaseRT(bool bEmmisive, Texture2D sceneTexture)
        {
            m_GraphicDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            m_GraphicDevice.RenderState.DepthBufferEnable = true;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = true;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.AlphaTestEnable = false;

            //根據是否要回存emmisive map做設定的判斷
            if (bEmmisive == true)
            {
                Texture2D emmTexture = m_EmmisiveRT.GetTexture();
                m_GraphicDevice.SetRenderTarget(0, GetBaseRT());
                m_GraphicDevice.SetRenderTarget(1, m_EmmisiveRT);
                m_GraphicDevice.Clear(Color.CornflowerBlue);
                m_RestoreDepthEffect.CurrentTechnique = m_RestoreDepthEffect.Techniques["RestoreColorEmmisiveDepth"];

                m_RestoreDepthEffect.Parameters["EmmTexture"].SetValue(emmTexture);
            }
            else
            {
                m_GraphicDevice.SetRenderTarget(0, GetBaseRT());
                m_GraphicDevice.Clear(Color.CornflowerBlue);
                m_RestoreDepthEffect.CurrentTechnique = m_RestoreDepthEffect.Techniques["RestoreColorDepth"];
            }

            m_RestoreDepthEffect.Parameters["halfPixel"].SetValue(halfPixel);
            m_RestoreDepthEffect.Parameters["DepthTexture"].SetValue(GetTexture_DepthRT());
            m_RestoreDepthEffect.Parameters["ColorTexture"].SetValue(sceneTexture);

            m_RestoreDepthEffect.CommitChanges();
            m_RestoreDepthEffect.Begin();
            m_RestoreDepthEffect.Techniques[0].Passes[0].Begin();

            //render a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);

            m_RestoreDepthEffect.Techniques[0].Passes[0].End();
            m_RestoreDepthEffect.End();
        }

        void DrawDebugLine_PhysXFluids(int sceneID, I_Camera camera)
        {
            DrawLine.AddLineBegin(m_GraphicDevice);

            #region get
            I_SceneManager scene = GetScene(sceneID);

            //I_Camera camera;
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return;

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;
            #endregion

            PhysXFluidSystem[] fluids = scene.GetPhysXFluidAsset();
            foreach (PhysXFluidSystem fluid in fluids)
            {
                List<Vector3>.Enumerator allPos = fluid.GetPhysXFluidAllDebugPos().GetEnumerator();
                while (allPos.MoveNext())
                {
                    DrawLine.AddLine(allPos.Current - Vector3.Up, allPos.Current - Vector3.Down, Color.Yellow);
                    DrawLine.AddLine(allPos.Current - Vector3.Left, allPos.Current - Vector3.Right, Color.Green);
                }
                allPos.Dispose();
            }

            DrawLine.Draw(viewMatrix, projectionMat); //???
        }

        void DrawDebugLine_PhysXParticle(int sceneID, I_Camera camera)
        {
            DrawLine.AddLineBegin(m_GraphicDevice);

            #region get
            I_SceneManager scene = GetScene(sceneID);

            //I_Camera camera;
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return;

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;
            #endregion

            PhysXParticleEmitter[] particles = scene.GetPhysXParticleAsset();
            foreach (PhysXParticleEmitter particleEmitter in particles)
            {
                List<Vector3>.Enumerator allPos = particleEmitter.GetAllDebugPos().GetEnumerator();
                while (allPos.MoveNext())
                {
                    DrawLine.AddLine(allPos.Current - Vector3.Up, allPos.Current - Vector3.Down, Color.Red);
                    DrawLine.AddLine(allPos.Current - Vector3.Left, allPos.Current - Vector3.Right, Color.Blue);
                }
                allPos.Dispose();
            }

            DrawLine.Draw(viewMatrix, projectionMat); //???
        }


    }
}
