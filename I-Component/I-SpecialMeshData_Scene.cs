﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
//using System.Xml.Serialization;
using System.IO;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public partial class I_SpecialMeshData
    {
        //管理SceneManager資訊的存檔與讀檔  
        [Serializable]
        public class I_SceneData
        {
            public I_SceneData() { }
            public I_SceneData(string name)
            {
                SceneName = name;
                LoadersName = null;
                SceneLightFogData = new I_SceneLightFogData();
                CamerasData = null;
                MaterialBank = null;          
            }
            public string SceneName;
            public string[] LoadersName;
            public I_SceneLightFogData SceneLightFogData;
            public I_CameraData[] CamerasData;
            public I_Material [] MaterialBank;
        }

        /// <summary>
        /// 此scene中的基本光和霧及MipmapLevel資訊
        /// </summary>
        [Serializable]
        public class I_SceneLightFogData
        {
            public I_SceneLightFogData()
            {
                Light1Direction = Light1Ambient = Light1Diffuse = Light1Specular = FogColor = Vector3.One;
                FogStart = FogEnd = 0;
                FogMaxRatio = 0;
                //MipmapLevel = 0;
            }
            public Vector3 Light1Direction;
            public Vector3 Light1Ambient;
            public Vector3 Light1Diffuse;
            public Vector3 Light1Specular;
            
            public Vector3 FogColor;
            public float FogStart;
            public float FogEnd;
            public float FogMaxRatio;
            //public float MipmapLevel; mipmap目前先暫訂於camera中設定

            public static void AsignBtoA(I_SceneLightFogData A, I_SceneLightFogData B)
            {
                A.Light1Direction = B.Light1Direction;
                A.Light1Ambient = B.Light1Ambient;
                A.Light1Diffuse = B.Light1Diffuse;
                A.Light1Specular = B.Light1Specular;
                A.FogColor = B.FogColor;
                A.FogStart = B.FogStart;
                A.FogEnd = B.FogEnd;
                A.FogMaxRatio = B.FogMaxRatio;
                //A.MipmapLevel = B.MipmapLevel;
            }
        }

    }
}
