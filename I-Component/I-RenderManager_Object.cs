﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        /// <summary>
        /// 建立或更新Camera資訊
        /// </summary>
        /// <param name="cameraData"></param>
        public void RefreshCameraInfo(int sceneID, I_SpecialMeshData.I_CameraData cameraData)
        {
            lock (LockObject)
            {
                GetScene(sceneID).RefreshCameraInfo(m_GraphicDevice, cameraData);
            }
        }

        /// <summary>
        /// 新增一個Camera於選定的scene中，因為讀檔時會要Lock住Update及Render，所以在這邊處理。
        /// </summary>
        public void AddNewCamera(int sceneID)
        {
            lock (LockObject)
            {                
                GetScene(sceneID).AddNewCamera(m_GraphicDevice);

                I_Camera mainCamera = GetScene(sceneID).GetMainCamera();//如果沒有Maincamera就設定這一個camera為main Camera
                if (mainCamera == null)
                    GetScene(sceneID).GetLastCamera().IfMainCamera = true;
            }
        }

        public BasicMeshLoader AddLoaderXML(int sceneID, ref string name, out SpecialMeshDataManager.TatalSpecialMeshData total)
        {
            lock (LockObject)
            {                
                return GetScene(sceneID).AddLoaderXML(ref name, out total);
            }
        }

        public BasicMeshLoader PreAddLoaderXNB_SceneMesh(int sceneID, ref string name, out SpecialMeshDataManager.TatalSpecialMeshData total)
        {
            lock (LockObject)
            {
              return  GetScene(sceneID).PreAddLoaderXNB(ref name, SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.SceneMesh, out total);
            }
        }

        public BasicMeshLoader PreAddLoaderXNB_SkinningMesh(int sceneID, ref string name, out SpecialMeshDataManager.TatalSpecialMeshData total)
        {
            lock (LockObject)
            {
              return  GetScene(sceneID).PreAddLoaderXNB(ref name, SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.SkinnedMesh, out total);
            }
        }

        public BasicMeshLoader PreAddLoaderXNB_KeyframeMesh(int sceneID, ref string name, out SpecialMeshDataManager.TatalSpecialMeshData total)
        {
            lock (LockObject)
            {
                return GetScene(sceneID).PreAddLoaderXNB(ref name, SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.KeyframeMesh, out total);
            }
        }

        /// <summary>
        /// 建立一個Scene
        /// </summary>
        public int AddNewScene(IServiceProvider provider, GraphicsDevice device, I_SpecialMeshData.I_SceneData sceneData)
        {
            lock (LockObject)
            {
                I_SceneManager scene = I_SceneManager.Create(provider, device, m_sceneIDCount, sceneData);
                m_SceneList.Add(scene);
                m_sceneIDCount++;
                return m_sceneIDCount - 1;
            }
        }

        /// <summary>
        /// 幹掉傳入的Scene
        /// </summary>
        public void RemoveScene(ref int sceneID)
        {
            lock (LockObject)
            {
                if (sceneID < 0)
                    return;
                I_SceneManager scene = null;
                List<I_SceneManager>.Enumerator itr = m_SceneList.GetEnumerator();
                while (itr.MoveNext())
                {
                    if (itr.Current.GetSceneID() == sceneID)
                    {
                        scene = itr.Current;
                        break;
                    }
                }
                itr.Dispose();

                m_SceneList.Remove(scene);
                scene.Dispose();
                scene = null;

                sceneID = -1;

                Utility.GCClear();
            }
        }

        /// <summary>
        /// 傳入檔名讀取一個Scene的設定資料I_SceneData，並根據資訊建立整個場景。
        /// </summary>
        public int LoadSceneData(ref string xmlFilename)
        {
            lock (LockObject)
            {
                I_SpecialMeshData.I_SceneData sceneData = I_IO.LoadXMLFile(ref xmlFilename, typeof(I_SpecialMeshData.I_SceneData)) as I_SpecialMeshData.I_SceneData;
                if (sceneData == null)
                    return -1;

                int sceneID = AddNewScene(m_Content.ServiceProvider, m_GraphicDevice, sceneData);
                I_SceneManager scene = GetScene(sceneID);

                //根據LoadersName建立所有的loader
                for (int a = 0; a < sceneData.LoadersName.Length; a++)
                {
                    string xmlFileName;
                    Utility.RemoveSubFileName(ref sceneData.LoadersName[a], out xmlFileName);
                    I_String.SetString(ref Utility.WorkingDirectory, ref xmlFileName);
                    I_String.AddString(".xml");
                    I_String.GetString(out xmlFileName);
                    SpecialMeshDataManager.TatalSpecialMeshData total;
                    scene.AddLoaderXML(ref xmlFileName, out total);                  
                }

                //根據CameraData建立I_Camera
                //for (int a = 0; a < sceneData.CamerasData.Length; a++)
                //{
                //    scene.RefreshCameraInfo(m_GraphicDevice, sceneData.CamerasData[a]);
                //}
                scene.RefreshCameraInfo(m_GraphicDevice, sceneData.CamerasData[0]);//目前一個Scene只吃一個Camera

                //建立material bank
                scene.ClearMaterialBank();//先清空

                if (sceneData.MaterialBank == null)//讀檔近來沒material就給個defualt值
                {
                    scene.InitMaterialBank();
                }
                else
                {
                    for (int a = 0; a < sceneData.MaterialBank.Length; a++)//設定讀檔的material bank資訊給scene
                    {
                        if (sceneData.MaterialBank[a] == null)
                            continue;
                        I_SpecialMeshData.I_Material material = scene.CreateNewMaterial();
                        I_SpecialMeshData.I_Material.AssignAtoB(sceneData.MaterialBank[a], material);
                        //material.ambient = new Vector3(0.1f, 0, 0);
                        //material.diffuse = new Vector3(0, 0.7f, 0);
                    }
                }
                //scene.GetMaterialBankData()[9] = new I_SpecialMeshData.I_Material();
                //scene.GetMaterialBankData()[9].ambient = new Vector3(0.0f, 0f, 0.1f);
                //scene.GetMaterialBankData()[9].diffuse = new Vector3(0.0f, 1f, 0f);

                //scene.GetMaterialBankData()[4] = new I_SpecialMeshData.I_Material();
                //scene.GetMaterialBankData()[4].ambient = new Vector3(0.0f, 0.1f, 0.0f);
                //scene.GetMaterialBankData()[4].diffuse = new Vector3(0.0f, 0.0f, 1.0f);
                scene.RefreshAllMaterialID();

                return sceneID;
            }
        }
    }
}
