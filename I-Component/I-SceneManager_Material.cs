﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public partial class I_SceneManager : IDisposable
    {
        /// <summary>
        /// 新增一個material並給他一個名字，若是material bank客滿就會要不到東西回傳null
        /// </summary>
        public I_SpecialMeshData.I_Material CreateNewMaterial()
        {
            for (int a = 0; a < m_MaterialBank.Length; a++)
            {
                if (m_MaterialBank[a] == null)
                {
                    I_SpecialMeshData.I_Material material = new I_SpecialMeshData.I_Material();

                    int nnn = MyMath.GetNext(100, 9999);
                    material.NameID = "material" + nnn.ToString();

                    m_MaterialBank[a] = material;
                    return m_MaterialBank[a];
                }
            }
            return null;
        }

        /// <summary>
        /// 依據名稱移除material
        /// </summary>
        public void RemoveMaterial(ref string materialName)
        {
            for (int a = 0; a < m_MaterialBank.Length; a++)
            {
                if (m_MaterialBank[a] != null && m_MaterialBank[a].NameID == materialName)
                {
                    m_MaterialBank[a] = null;
                    return;
                }
            }
        }

        /// <summary>
        /// 清空MaterialBank
        /// </summary>
        public void ClearMaterialBank()
        {
            for (int a = 0; a < m_MaterialBank.Length; a++)
                m_MaterialBank[a] = null;
        }

        public int GetMaterialID(ref string materialName)
        {
            if (materialName == null)
                return 0;

            for (int a = 0; a < m_MaterialBank.Length; a++)
            {
                if (m_MaterialBank[a] != null && m_MaterialBank[a].NameID == materialName)
                    return a;
            }
            return -1;
        }

        public I_SpecialMeshData.I_Material GetMaterial(ref string materialName)
        {
            if (materialName == null)
                return null;

            for (int a = 0; a < m_MaterialBank.Length; a++)
            {
                if (m_MaterialBank[a] != null && m_MaterialBank[a].NameID == materialName)
                    return m_MaterialBank[a];
            }
            return null;
        }

        /// <summary>
        /// 依據materialName名稱設定各種屬性給他
        /// </summary>     
        public void SetMaterialData(string materialName,
             string NameID, Vector3? ambient, Vector3? diffuse, Vector3? specular,
            byte? Spower, 
            //bool? fogEnable, 
            float? reflectIntensity, float? refractScale)
        {
            for (int a = 0; a < m_MaterialBank.Length; a++)
            {
                if (m_MaterialBank[a] != null && m_MaterialBank[a].NameID == materialName)
                {
                    if (NameID != null)
                        m_MaterialBank[a].NameID = NameID;
                    if (ambient != null)
                        m_MaterialBank[a].ambient = ambient.Value;
                    if (diffuse != null)
                        m_MaterialBank[a].diffuse = diffuse.Value;
                    if (specular != null)
                        m_MaterialBank[a].specular = specular.Value;
                    if (Spower != null)
                        m_MaterialBank[a].Spower = Spower.Value;
                    //if (fogEnable != null)
                    //    m_MaterialBank[a].fogEnable = fogEnable.Value;
                    if (reflectIntensity != null)
                        m_MaterialBank[a].reflectIntensity = reflectIntensity.Value;
                    //if (refractScale != null)
                    //    m_MaterialBank[a].refractScale = refractScale.Value;
                    return;
                }
            }
        }

        /// <summary>
        /// 根據Material Bank更新所有底下sceneNode裡的材質ID資訊。
        /// </summary>
        public void RefreshAllMaterialID()
        {
            List<BasicMeshLoader>.Enumerator itr = m_LoaderAsset.GetEnumerator();
            while (itr.MoveNext())
            {
                SetLoaderMaterialID(itr.Current.GetRootNode);
            }
            itr.Dispose();
        }

        void SetLoaderMaterialID(FrameNode parentNode)//, I_SpecialMeshData.I_Material[] materialBank)
        {
            SceneNode snode = parentNode as SceneNode;
            if (snode != null)
                snode.iMaterialBankID = GetMaterialID(ref snode.MaterialBankID);

            FrameNode child = parentNode.FirstChild as FrameNode;
            while (child != null)
            {
                SetLoaderMaterialID(child);
                child = child.Sibling as FrameNode;
            }
        }
    }
}
