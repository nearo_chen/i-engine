﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;
using StillDesign.PhysX;
//using PSEditor.EditorUI;

namespace I_Component
{
    public partial class BasicParticleSystem : System.IDisposable
    {
        public string m_BasicParticleName = "BasicParticle";

        ParticleEngine.InputSettings m_BasicSetting;
        /// <summary>
        /// 取得particle內部資訊輸出設定格式資料
        /// 根據I_PhysXParticle資訊取得I_PhysXParticleData，可以去做存檔。
        /// </summary>
        public I_SpecialMeshData.I_BasicParticleSystemData GetBasicParticleSystemData(ParticleEngine.InputSettings basicSetting)
        {
            m_BasicSetting = basicSetting;
         //   m_BasicSetting = m_BasicParticle.GetBasicParticleSystemSetting();
            I_SpecialMeshData.I_BasicParticleSystemData data = new I_SpecialMeshData.I_BasicParticleSystemData(
                m_BasicSetting.particleSettings, m_BasicSetting.ParticlesPerSecond, m_BasicSetting.UseEmitter, m_BasicSetting.UseDirectionParticle);
            return data;
        }

        public void Dispose()
        {

        }

    }
}
