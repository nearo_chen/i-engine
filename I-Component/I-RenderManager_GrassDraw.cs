﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        /// <summary>
        /// 將scene裡的camera要畫的物件以Grass shader畫出。
        /// </summary>
        void RenderCamera_GrassNode(int sceneID, I_Camera[] cameras)
        {
            m_GrassEffect.SetRenderBegin_Simple(SceneNode.RenderTechnique.GrassMesh);

            I_SceneManager scene = GetScene(sceneID);
            //if (camerasName == null)
            //    camerasName = new string[] { scene.GetLastCamera().Name };

            for (int a = 0; a < cameras.Length; a++)
            {
                I_Camera camera = cameras[a];
                //camera = scene.GetCamera(ref camerasName[a]);
                if (camera == null)
                    continue;

                Matrix projectionMat, cameraMat, invCameraMat;
                camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
                camera.GetCameraMatrix(out cameraMat, out invCameraMat);
                Vector3 cameraPosition = cameraMat.Translation;
                Matrix viewMatrix = invCameraMat;
                Matrix invViewMatrix = cameraMat;

                Vector3 fogColor;
                float FogStart, FogEnd, FogMaxRatio;
                scene.GetFog(out fogColor, out FogStart, out FogEnd, out FogMaxRatio);

                Vector3 LightDirection = Vector3.Zero;
                Vector3 LightAmbient = Vector3.Zero;
                Vector3 LightDiffuse = Vector3.Zero;
                Vector3 LightSpecular = Vector3.Zero;
                scene.GetDirectionalLight(ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

                m_GrassEffect.Simple_SetCommonEffectPar(ref projectionMat, ref viewMatrix, ref cameraPosition,
                   ref  fogColor, FogStart, FogEnd, FogMaxRatio,
                   ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

                //由I-Camera中取得要畫出的Node
                List<GrassNode>.Enumerator grassNodeItr = camera.GetAllGrassNodeItr();//畫出所有mesh node
                GrassNode grassNode;
                while (grassNodeItr.MoveNext())
                {
                    grassNode = grassNodeItr.Current;

                    if (grassNode.BeCulled)
                        continue;

                    m_GrassEffect.Simple_RenderGrassMeshNode(grassNode, ref RenderTriangles, m_GraphicDevice);
                    RenderNodesOld++;
                }
                grassNodeItr.Dispose();
            }

            m_GrassEffect.SetRenderEnd();
        }
    }
}