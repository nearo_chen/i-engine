﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    /// <summary>
    /// I_CameraNode控制一個Camera及最後畫到Texture的畫面，此class管理view frustum culling，
    /// 於編輯器中設定Camera，有介面可設定傳入要繪出的Loader，於Render時，Camera會根據要畫出的loader先做view culling，
    /// 得到要畫出的 [ ] Nodes，再設定camera的renderTarget，然後根據camera所設定使用的特效繪圖管線處理，如打光處理，
    /// 點光源處理，影子處理，hdr..等，將最後結果畫在camera Texture上。
    /// </summary>
    public partial class I_Camera : IDisposable
    {
        /// <summary>        
        /// main Camera主要是畫出場景的Camera
        /// main camera才有辦法畫出PSSM 和SSAO特效
        /// main camera才會做Frustum Culling，反之會全都畫出來。
        /// 目前一個scene只有一個camera所以，一定會是true。
        /// </summary>
        public bool IfMainCamera = false;

        /// <summary>
        /// 是否要做view culling
        /// </summary>
        public bool IfViewCulling = false;

        public string Name;

        public float FovDegree;
        public float Near;
        public float Far;

        Matrix CameraMatrix;//Camera Matrix可直接由此取得
        //public int BackBufferW;//backbuffer材質的寬高。
        //public int BackBufferH;//backbuffer材質的寬高。
        List<BasicMeshLoader> m_RenderLoader = null;//loading時建立的資訊，紀錄此camera會畫出哪些loader
        List<MeshNode> m_MeshNodes;//用來記錄目前Camera中所有要畫出的各型態Node
        List<SkinningMeshNode> m_SkinningMeshNodes;
        List<GrassNode> m_GrassNodes;
      

        //RenderTarget2D m_cameraRT = null;


        /// <summary>
        /// 建立camera時傳入camera backbuffer材質的寬高。
        /// </summary>
        /// <param name="bufferW"></param>
        /// <param name="?"></param>
        public I_Camera(GraphicsDevice device, I_SceneManager scene, I_SpecialMeshData.I_CameraData cameraData)
        {
            m_RenderLoader = new List<BasicMeshLoader>(32);
            m_MeshNodes = new List<MeshNode>(CountNode(typeof(MeshNode)));
            m_SkinningMeshNodes = new List<SkinningMeshNode>(CountNode(typeof(SkinningMeshNode)));
            m_GrassNodes = new List<GrassNode>(CountNode(typeof(GrassNode)));

            SetCameraData(device, scene, cameraData);
        }

        /// <summary>
        /// 將設定資訊設定給camera內部資料
        /// </summary>
        /// <param name="cameraData"></param>
        void SetCameraData(GraphicsDevice device,
            I_SceneManager scene, I_SpecialMeshData.I_CameraData cameraData)
        {
            //BackBufferW = cameraData.RTWidth;
            //BackBufferH = cameraData.RTHeight;

            //m_cameraRT = new RenderTarget2D(device, BackBufferW, BackBufferH, 1, SurfaceFormat.Color);

            Name = cameraData.Name;
            FovDegree = cameraData.FovDegree;
            Near = cameraData.Near;
            Far = cameraData.Far;

            DOFFocalDistance = cameraData.DOFFocalDistance;
            DOFFocalRange = cameraData.DOFFocalRange;
            DOFDecayRange = cameraData.DOFDecayRange;
            DOFIntensity = cameraData.DOFIntensity;

            PSSMLevelAmount = cameraData.PSSMLevelAmount;
            PSSMSplitRatioLambda = cameraData.PSSMSplitRatioLambda;
            PSSM0Bias = cameraData.PSSM0Bias;
            PSSM1Bias = cameraData.PSSM1Bias;
            PSSM2Bias = cameraData.PSSM2Bias;
            PSSM3Bias = cameraData.PSSM3Bias;

            SSAOSampleRadius = cameraData.SSAOSampleRadius;
            SSAOSampleTime = cameraData.SSAOSampleTime;
            SSAOSampleLOD = cameraData.SSAOSampleLOD;
            SSAODistanceScale = cameraData.SSAODistanceScale;
            SSAOFadeOutDistance = cameraData.SSAOFadeOutDistance;

            IfMainCamera = cameraData.IfMainCamera;
            IfViewCulling = cameraData.IfViewCulling;

            cameraData.RTMat.GetMatrix(out CameraMatrix);

            BasicMeshLoader[] loaders = scene.GetLoaderAsset();

            if (cameraData.RenderLoaders != null && cameraData.RenderLoaders.Length >0)
            {
                //找一下最後camera設定要畫的最後一個loader
                BasicMeshLoader finalLoader = null;
                for (int a = 0; a < loaders.Length; a++)
                {
                    if (loaders[a].GetRootNode.NodeName == cameraData.RenderLoaders[cameraData.RenderLoaders.Length - 1])
                    {
                        finalLoader = loaders[a];
                        break;
                    }
                }


                for (int a = 0; a < cameraData.RenderLoaders.Length; a++)
                {
                    for (int b = 0; b < loaders.Length; b++)
                    {
                        if (cameraData.RenderLoaders[a] == loaders[b].GetRootNode.NodeName)
                        {
                            AddRenderLoader(loaders[b], finalLoader);
                            break;
                        }
                    }
                }
            }

        }

        /// <summary>
        /// 取得camera內部資訊輸出設定格式資料
        /// 根據I_Camera資訊取得I_CameraData，可以去做存檔。
        /// </summary>
        public I_SpecialMeshData.I_CameraData GetCameraData()
        {
            I_SpecialMeshData.I_CameraData data = new I_SpecialMeshData.I_CameraData(
                //  BackBufferW, BackBufferH, 
                Name, FovDegree, Near, Far,
                DOFFocalDistance, DOFFocalRange, DOFDecayRange, DOFIntensity
                );
            if (m_RenderLoader != null)
            {
                data.RenderLoaders = new string[m_RenderLoader.Count];
                for (int a = 0; a < m_RenderLoader.Count; a++)
                    data.RenderLoaders[a] = m_RenderLoader[a].GetRootNode.NodeName;
            }

            data.PSSMLevelAmount = PSSMLevelAmount;
            data.PSSMSplitRatioLambda = PSSMSplitRatioLambda;
            data.PSSM0Bias = PSSM0Bias;
            data.PSSM1Bias = PSSM1Bias;
            data.PSSM2Bias = PSSM2Bias;
            data.PSSM3Bias = PSSM3Bias;

            data.SSAOSampleRadius = SSAOSampleRadius;
            data.SSAOSampleTime = SSAOSampleTime;
            data.SSAOSampleLOD = SSAOSampleLOD;
            data.SSAODistanceScale = SSAODistanceScale;
            data.SSAOFadeOutDistance = SSAOFadeOutDistance;

            data.IfMainCamera = IfMainCamera;
            data.IfViewCulling = IfViewCulling;

            data.RTMat.SetData(CameraMatrix);
            return data;
        }

        public void Dispose()
        {
            m_RenderLoader.Clear();
            m_RenderLoader = null;

            m_MeshNodes.Clear();
            m_MeshNodes = null;

            m_SkinningMeshNodes.Clear();
            m_SkinningMeshNodes = null;

            m_GrassNodes.Clear();
            m_GrassNodes = null;
            //if (m_cameraRT == null)
            //    m_cameraRT.Dispose();
            //m_cameraRT = null;
        }

        /// <summary>
        /// 加入此camera會參考畫出的Loader(傳入finalLoader表示，loader與finalLoader一樣時
        /// 會做Camera render node的分類處理)
        /// </summary>
        public void AddRenderLoader(BasicMeshLoader loader, BasicMeshLoader finalLoader)
        {
            m_RenderLoader.Add(loader);

            if (loader == finalLoader)
                RefreshCameraRenderNodeInformation();
        }

        /// <summary>
        /// 加總所有Loader裡面同型態的Node數量
        /// </summary>
        int CountNode(Type nodeType)
        {
            int count = 0;
            List<BasicMeshLoader>.Enumerator itr = m_RenderLoader.GetEnumerator(); //GetRenderLoadersItr();
            while (itr.MoveNext())
                SceneNode.CountNodeSameType(itr.Current.GetRootNode, nodeType, ref count);
            itr.Dispose();

            if (count < 16)
                count = 16;
            return count;
        }

        /// <summary>
        /// 整理目前Scene裡的Loader底下的所有Node，將他們做歸類，方便畫出。
        /// </summary>
        public void RefreshCameraRenderNodeInformation()
        {
            List<BasicNode> m_TmpNodeBuffer = null;
            if (m_TmpNodeBuffer == null)
                m_TmpNodeBuffer = new List<BasicNode>(512);       

            m_MeshNodes.Clear();
            m_SkinningMeshNodes.Clear();
            m_GrassNodes.Clear();
            List<BasicMeshLoader>.Enumerator RenderLoaderItr = m_RenderLoader.GetEnumerator(); //GetRenderLoadersItr();
            while (RenderLoaderItr.MoveNext())
            {
                BasicMeshLoader loader = RenderLoaderItr.Current;
                List<BasicNode>.Enumerator basicNodeItr;

                SceneNode.AddNodeSameType(loader.GetRootNode, m_TmpNodeBuffer, typeof(MeshNode));
                basicNodeItr = m_TmpNodeBuffer.GetEnumerator();
                while (basicNodeItr.MoveNext())
                    m_MeshNodes.Add((MeshNode)basicNodeItr.Current);
                basicNodeItr.Dispose();
                m_TmpNodeBuffer.Clear();

                SceneNode.AddNodeSameType(loader.GetRootNode, m_TmpNodeBuffer, typeof(SkinningMeshNode));
                basicNodeItr = m_TmpNodeBuffer.GetEnumerator();
                while (basicNodeItr.MoveNext())
                    m_SkinningMeshNodes.Add((SkinningMeshNode)basicNodeItr.Current);
                basicNodeItr.Dispose();
                m_TmpNodeBuffer.Clear();

                SceneNode.AddNodeSameType(loader.GetRootNode, m_TmpNodeBuffer, typeof(GrassNode));
                basicNodeItr = m_TmpNodeBuffer.GetEnumerator();
                while (basicNodeItr.MoveNext())
                    m_GrassNodes.Add((GrassNode)basicNodeItr.Current);
                basicNodeItr.Dispose();
                m_TmpNodeBuffer.Clear();
            }
            RenderLoaderItr.Dispose();

            m_TmpNodeBuffer = null;
        }

        /// <summary>
        /// 於呼叫RefreshCameraRenderNodeInformation()之後可以取得，
        /// 取得Camera要畫出的所有MeshNode，取得所有Loader裡面的MeshNode
        /// </summary>
        public List<MeshNode>.Enumerator GetAllMeshNodeItr()
        {
            return m_MeshNodes.GetEnumerator();
        }

        /// <summary>
        /// 於呼叫RefreshCameraRenderNodeInformation()之後可以取得，
        /// 取得Camera要畫出的Loader裡面的所有SkinningMeshNode
        /// </summary>
        public List<SkinningMeshNode>.Enumerator GetAllSkinningMeshNodeItr()
        {
            return m_SkinningMeshNodes.GetEnumerator();
        }

        /// <summary>
        /// 於呼叫RefreshCameraRenderNodeInformation()之後可以取得，
        /// 取得Camera要畫出的Loader裡面的所有GrassNode
        /// </summary>
        public List<GrassNode>.Enumerator GetAllGrassNodeItr()
        {
            return m_GrassNodes.GetEnumerator();
        }

        /// <summary>
        /// 取得camera裡面所有要畫的node的數量
        /// </summary>
        public int GetMeshNodeAmount()
        {
            return m_MeshNodes.Count;
        }

        /// <summary>
        /// 取得camera裡面所有要畫的node的數量
        /// </summary>
        /// <returns></returns>
        public int GetSkinningMeshNodeAmount()
        {
            return m_SkinningMeshNodes.Count;
        }

        /// <summary>
        /// 取得camera裡面所有要畫的node的數量
        /// </summary>
        /// <returns></returns>
        public int GetGrassNodeAmount()
        {
            return m_GrassNodes.Count;
        }
    }
}
