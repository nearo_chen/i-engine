﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
//using System.Xml.Serialization;
using System.IO;
using I_XNAComponent;
using I_XNAUtility;
using MeshDataDefine;

namespace I_Component
{
    public partial class I_SpecialMeshData
    {
        [Serializable]
        public class I_Material
        {
            public I_Material()
            {
                //NameID = "material";
                ambient = new Vector3(0.7f, 0.7f, 0.7f);
                diffuse = new Vector3(0.7f, 0.7f, 0.7f);
                specular = new Vector3(0.7f, 0.7f, 0.7f);
                Spower = 8;

                //fogEnable = true;
                //lightEnable = true;
                reflectIntensity = 0;
                //refractScale = 0;
            }
            public string NameID;
            public Vector3 ambient;
            public Vector3 diffuse;
            public Vector3 specular;
            public byte Spower;
            //public bool fogEnable;
            //public bool lightEnable;
            public float reflectIntensity;
            //  public float refractScale;

            public static void AssignAtoB(I_Material a, I_Material b)
            {
                b.NameID = a.NameID;
                b.ambient = a.ambient;
                b.diffuse = a.diffuse;
                b.specular = a.specular;
                b.Spower = a.Spower;
                //b.fogEnable = a.fogEnable;
                //b.lightEnable = a.lightEnable;
                b.reflectIntensity = a.reflectIntensity;
                //b.refractScale = a.refractScale;
            }
        }
    }
}

