﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        /// <summary>
        /// 丟一張圖進去做bloom處理，然後把bloom的結果畫到傳入的renderTarget上。
        /// </summary>    
        protected void RenderBloom(Texture2D texture, RenderTarget2D renderTarget)
        {
            m_MyBloom.Draw(texture, renderTarget, 7);
        }

        /// <summary>
        /// 丟一張圖進去做bloom處理，然後把bloom的結果畫到傳入的renderTarget上。
        /// </summary>    
        protected void RenderBloom2(Texture2D texture, RenderTarget2D renderTarget)
        {
            m_MyBloom.Draw(texture, renderTarget, 6);
        }

        /// <summary>
        /// 將傳入貼圖做水平與垂直高斯模糊
        /// </summary>
        protected void RenderBothGausianBlur()
        {
        }

        protected enum RenderRT
        {
            Backbuffer, BaseRT,
        }

        /// <summary>
        /// 將texture1根據他的alpha畫到sceneTexture上，輸出在rt。
        /// 畫在width和height的範圍中
        /// </summary>      
        protected void BlendTextureToScene(Texture2D texture1, Texture2D sceneTexture, int width, int height, RenderRT renderRT)
        {
            if (renderRT == RenderRT.Backbuffer)
                m_GraphicDevice.SetRenderTarget(0, null);
            else if (renderRT == RenderRT.BaseRT)
                m_GraphicDevice.SetRenderTarget(0, GetBaseRT());
            m_GraphicDevice.SetRenderTarget(1, null);
            m_GraphicDevice.SetRenderTarget(2, null);
            m_GraphicDevice.SetRenderTarget(3, null);

            m_GraphicDevice.Textures[1] = texture1;

            m_SpriteBatch.Begin(SpriteBlendMode.None,
                              SpriteSortMode.Immediate,
                              SaveStateMode.None);

            m_EmmBlendEffect.Begin();
            m_EmmBlendEffect.CurrentTechnique.Passes[0].Begin();

            // Draw the quad.
            m_SpriteBatch.Draw(sceneTexture, new Rectangle(0, 0, width, height), Color.White);
            m_SpriteBatch.End();

            // End the custom effect.
            m_EmmBlendEffect.CurrentTechnique.Passes[0].End();
            m_EmmBlendEffect.End();

            m_GraphicDevice.Textures[0] = null;
            m_GraphicDevice.Textures[1] = null;
        }

        /// <summary>
        /// 使用Render Refract Effect，與secneTexture計算去折射，畫出折射圖，於目前設定的RT上。
        /// </summary>
        /// <param name="secneTexture"></param>
        protected void RenderWithRefractDistortMap(Texture2D secneTexture, int width, int height, RenderRT renderRT)
        {
            if (renderRT == RenderRT.Backbuffer)
                m_GraphicDevice.SetRenderTarget(0, null);
            else if (renderRT == RenderRT.BaseRT)
                m_GraphicDevice.SetRenderTarget(0, GetBaseRT());

            //一定要這樣畫post process的map，effect才會有用。
            m_GraphicDevice.Textures[1] = GetTexture_RefractRT();
            m_SpriteBatch.Begin(SpriteBlendMode.None,
                             SpriteSortMode.Immediate,
                             SaveStateMode.None);

            // Begin the custom effect, if it is currently enabled. If the user
            // has selected one of the show intermediate buffer options, we still
            // draw the quad to make sure the image will end up on the screen,
            // but might need to skip applying the custom pixel shader.
            m_RenderRefract.Begin();
            m_RenderRefract.CurrentTechnique.Passes[0].Begin();

            // Draw the quad.
            m_SpriteBatch.Draw(secneTexture, 
                //Vector2.Zero,
                new Rectangle(0, 0, width, height), 
                Color.White);
            m_SpriteBatch.End();

            // End the custom effect.
            m_RenderRefract.CurrentTechnique.Passes[0].End();
            m_RenderRefract.End();
            m_GraphicDevice.Textures[1] = null;
        }

        /// <summary>
        /// 設定depth map，使能夠繼續畫出深度於其上。
        /// </summary>
        void ContinueDrawDepthMap()
        {
            //首先回存深度緩衝，
            //或事直接傳入深度緩衝，shader內用判斷深度做clip

            m_GraphicDevice.RenderState.AlphaTestEnable = false;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.DepthBufferEnable = false;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = false;
        }

        /// <summary>
        /// //畫出camera的loader裡面的particle之類的
        /// </summary>
        protected void RenderCamera_LoaderEffects(int sceneID, I_Camera[] cameras)
        {
             I_SceneManager scene = GetScene(sceneID);
            //if (camerasName == null)
            //    camerasName = new string[] { scene.GetLastCamera().Name };

             for (int a = 0; a < cameras.Length; a++)
            {
                I_Camera camera = cameras[a];
                //camera = scene.GetCamera(ref camerasName[a]);
                if (camera == null)
                    continue;

                Matrix projectionMat, cameraMat, invCameraMat;
                camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
                camera.GetCameraMatrix(out cameraMat, out invCameraMat);
                Vector3 cameraPosition = cameraMat.Translation;
                Matrix viewMatrix = invCameraMat;
                Matrix invViewMatrix = cameraMat;

                Vector3 fogColor;
                float FogStart, FogEnd, FogMaxRatio;
                scene.GetFog(out fogColor, out FogStart, out FogEnd, out FogMaxRatio);

                Vector3 LightDirection = Vector3.Zero;
                Vector3 LightAmbient = Vector3.Zero;
                Vector3 LightDiffuse = Vector3.Zero;
                Vector3 LightSpecular = Vector3.Zero;
                scene.GetDirectionalLight(ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

                foreach (BasicMeshLoader loader in camera.GetLoaders())
                    RenderSpecialEffect(loader, ref viewMatrix, ref projectionMat, ref invViewMatrix);
            }
        }

        /// <summary>
        /// 將mesh loader的particle和特效畫出來
        /// </summary>      
        void RenderSpecialEffect(BasicMeshLoader meshLoader, ref Matrix view, ref Matrix projection, ref Matrix invView)
        {
            if (meshLoader == null)
                return;
            //SetAlphaBlendRenderState(m_GraphicDevice);
            //meshLoader.EffectObjectList_Render(ref view, ref invView, ref projection, m_GraphicDevice, m_Content,
            //    DirectionLight1, DirectionLight1A, DirectionLight1D, DirectionLight1S,
            //    FogColor, FogStart, FogEnd, m_CubeMapCreator);

            SetAlphaBlendRenderState(m_GraphicDevice);

            Vector3 viewPosition = invView.Translation;
            meshLoader.RenderParticle(ref view, ref projection, ref viewPosition);
        }
    }
}
