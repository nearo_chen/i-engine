﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

using StillDesign.PhysX;

namespace I_Component
{
    public partial class PhysXFluidSystem : System.IDisposable
    {
        [Serializable]
        public class PhysXFluidData
        {
            public PhysXFluidData()
            {

            }

            public PhysXFluidData(int iMaximumParticles)
            {
                MaximumParticles = iMaximumParticles;
            }
            public int MaximumParticles;
        }
        [Serializable]
        public class PhysXFluidEmitterData
        {
            public PhysXFluidEmitterData()
            {

            }

            public PhysXFluidEmitterData(float iDimensionX, float iDimensionY, FluidEmitterFlag iFlags,
                float iFluidVelocityMagnitude, Shape iFrameShape, int iMaximumParticles,
                string iName, float iParticleLifetime, float iRandomAngle, Vector3 iRandomPosition,
                float iRate, Matrix iRelativePose, float iRepulsionCoefficient, EmitterShape iShape,
                EmitterType iType, Object iUserData)
            {
                DimensionX = iDimensionX;
                DimensionY = iDimensionY;
                Flags = iFlags;
                FluidVelocityMagnitude = iFluidVelocityMagnitude;
                FrameShape = iFrameShape;
                MaximumParticles = iMaximumParticles;
                Name = iName;
                ParticleLifetime = iParticleLifetime;
                RandomAngle = iRandomAngle;
                RandomPosition = iRandomPosition;
                Rate = iRate;
                RelativePose = iRelativePose;
                RepulsionCoefficient = iRepulsionCoefficient;
                Shape = iShape;
                Type = iType;
                UserData = iUserData;
            }

            public string m_FluidName;

            //產生範圍的寬度
            public float DimensionX;
            //產生範圍的高度
            public float DimensionY;
            public FluidEmitterFlag Flags;
            //?    正值
            public float FluidVelocityMagnitude;
            public Shape FrameShape;
            //粒子最大數量    正值
            public int MaximumParticles;

            public string Name;
            //粒子的存活時間(s)
            public float ParticleLifetime;
            //隨機產生的角度
            public float RandomAngle;
            //隨機產生的座標 (正值)
            public Vector3 RandomPosition;
            //每秒產生的速度
            public float Rate;
            //產生的位置
            public Matrix RelativePose;
            //?    正值
            public float RepulsionCoefficient;
            //發射器的形狀
            public EmitterShape Shape;

            public EmitterType Type;
            public Object UserData;
        }

        FluidEmitterDescription fluidEmitterDesc = new FluidEmitterDescription();
        FluidDescription fluidDesc = new FluidDescription();
        Fluid fluid;
        Scene scene;
        public string m_FluidName = "Fluid";

        public PhysXFluidSystem(Scene iScene)
        {
            fluidEmitterDesc.DimensionX = 0;
            fluidEmitterDesc.DimensionY = 0;
            fluidEmitterDesc.ParticleLifetime = 5.0f;
            fluidDesc.Emitters.Add(fluidEmitterDesc);
            fluidDesc.MaximumParticles = 1000;
            fluidDesc.ParticleWriteData.SetToDefault();
            fluidDesc.ParticleWriteData.AllocatePositionBuffer<Vector3>(fluidDesc.MaximumParticles);
            fluidDesc.ParticleWriteData.NumberOfParticles = fluidDesc.MaximumParticles;
            scene = iScene;
            fluid = scene.CreateFluid(fluidDesc);
        }

        public void PhysXFluidSetDefault()
        {
            fluidEmitterDesc.SetToDefault();
        }

        //設定FluidEmitter屬性
        public void SetPhysXFluidEmitterAttribute(PhysXFluidEmitterData Data)
        {
            fluidEmitterDesc.DimensionX = Data.DimensionX;
            fluidEmitterDesc.DimensionY = Data.DimensionY;
            fluidEmitterDesc.Flags = Data.Flags;
            fluidEmitterDesc.FluidVelocityMagnitude = Data.FluidVelocityMagnitude;
            fluidEmitterDesc.FrameShape = Data.FrameShape;
            fluidEmitterDesc.MaximumParticles = Data.MaximumParticles;
            fluidEmitterDesc.Name = Data.Name;
            fluidEmitterDesc.ParticleLifetime = Data.ParticleLifetime;
            fluidEmitterDesc.RandomAngle = Data.RandomAngle;
            fluidEmitterDesc.RandomPosition = Data.RandomPosition;
            fluidEmitterDesc.Rate = Data.Rate;
            fluidEmitterDesc.RelativePose = Data.RelativePose;
            fluidEmitterDesc.RepulsionCoefficient = Data.RepulsionCoefficient;
            fluidEmitterDesc.Shape = Data.Shape;
            fluidEmitterDesc.Type = Data.Type;
            fluidEmitterDesc.UserData = Data.UserData;
            scene.Fluids[0].Dispose();
            fluidDesc.Emitters.RemoveAt(fluidDesc.Emitters.Count - 1);
            fluidDesc.SetToDefault();
            fluidDesc.Emitters.Add(fluidEmitterDesc);
            fluidDesc.ParticleWriteData.SetToDefault();
            fluidDesc.ParticleWriteData.AllocatePositionBuffer<Vector3>(fluidDesc.MaximumParticles);
            fluidDesc.ParticleWriteData.NumberOfParticles = fluidDesc.MaximumParticles;
            scene.CreateFluid(fluidDesc);
        }

        //取得FluidEmitter屬性
        public PhysXFluidEmitterData GetPhysXFluidEmitterAttribute()
        {
            PhysXFluidEmitterData mFluidEmitterData = new PhysXFluidEmitterData(fluidEmitterDesc.DimensionX, fluidEmitterDesc.DimensionY,
                fluidEmitterDesc.Flags, fluidEmitterDesc.FluidVelocityMagnitude, fluidEmitterDesc.FrameShape,
                fluidEmitterDesc.MaximumParticles, fluidEmitterDesc.Name, fluidEmitterDesc.ParticleLifetime,
                fluidEmitterDesc.RandomAngle, fluidEmitterDesc.RandomPosition, fluidEmitterDesc.Rate, fluidEmitterDesc.RelativePose,
                fluidEmitterDesc.RepulsionCoefficient, fluidEmitterDesc.Shape, fluidEmitterDesc.Type, fluidEmitterDesc.UserData);
            return mFluidEmitterData;
        }

        /// <summary>
        /// 設定Fluid屬性
        /// </summary>
        /// <param name="Data"></param>
        private void SetPhysXFluidAttribute(PhysXFluidData Data)
        {
            fluidDesc.MaximumParticles = Data.MaximumParticles;
            fluidDesc.ParticleWriteData.SetToDefault();
            fluidDesc.ParticleWriteData.AllocatePositionBuffer<Vector3>(fluidDesc.MaximumParticles);
            fluidDesc.ParticleWriteData.NumberOfParticles = fluidDesc.MaximumParticles;
        }

        /// <summary>
        /// 取得Fluid屬性
        /// </summary>
        /// <returns></returns>
        private PhysXFluidData GetPhysXFluidAttribute()
        {
            PhysXFluidData mFluidData = new PhysXFluidData(fluidDesc.MaximumParticles);

            return mFluidData;
        }

        /// <summary>
        /// 取得Fluid的所有Particle座標
        /// </summary>
        /// <returns></returns>
        public List<Vector3> GetPhysXFluidAllDebugPos()
        {
            List<Vector3> AllPos = null;
            if (fluidDesc != null)
            {
                //fluidDesc.ParticleWriteData.LifeBuffer.GetData<int>();
                AllPos = fluidDesc.ParticleWriteData.PositionBuffer.GetData<Vector3>().ToList();
                AllPos.RemoveRange((int)fluidDesc.ParticleWriteData.NumberOfParticles, AllPos.Count - (int)fluidDesc.ParticleWriteData.NumberOfParticles);
            }
            return AllPos;
        }

        public void Dispose()
        {
            if (scene.Fluids.Count != 0)
            {
                scene.Fluids[0].Dispose();
                fluidDesc.SetToDefault();
                if (fluid.IsDisposed == false)
                    fluid.Dispose();
            }
        }
    }
}
