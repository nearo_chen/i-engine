﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;
using MeshDataDefine;
using I_Component.PhysicsType;

namespace I_Component
{
    /// <summary>
    /// 此類別管理一個scene中所有會用到的資源，如loader, camera, lights..., fog...particle，Ocean 之類的資源，
    /// 還有管理場景中的階層架構，可以管理由編輯器中架構的場景，控制讀檔與釋放的操作
    /// 更進一步可以建立physX的一個scene，可做view culling的管理(先暫定但不一定要這裡做)。
    /// </summary>
    public partial class I_SceneManager : IDisposable
    {
        /// <summary>
        /// 畫出做視野裁切用的物理BOX除錯線
        /// </summary>
        public void DrawCullingPhysicsDebugLine(GraphicsDevice device, Matrix view, Matrix projection)
        {
            m_BasicPhysics.DrawDebugLine(device, view, projection);
        }

        //class CullingLoaderData
        //{
        //    public CullingLoaderData()
        //    {
        //        LoaderList = new List<BasicMeshLoader>();
        //        MeshNodeCulling = false;
        //        InstanceNodeCulling = false;
        //        SkinningNodeCulling = false;
        //    }
        //    public List<BasicMeshLoader> LoaderList;
        //    public bool MeshNodeCulling;//說明此filter已做MeshNodeCulling
        //    public bool InstanceNodeCulling;//說明此filter已做InstanceNodeCulling
        //    public bool SkinningNodeCulling;//說明此filter已做SkinningNodeCulling
        //}
        //Dictionary<MyPhyX.FilterMask, CullingLoaderData> m_LoaderCullingData = new Dictionary<MyPhyX.FilterMask, CullingLoaderData>(128);

        /// <summary>
        /// 針對camera以myViewProjection做camera底下node的culling。
        /// 回傳被卡掉的node數量。
        /// </summary>
        public int ProcessCameraFrustumCulling(I_Camera camera, ref Matrix myViewProjection)
        {
            return ProcessCameraFrustumCulling(camera, 0, 0, myViewProjection);
        }

        /// <summary>
        /// 針對camera以view port的寬高做camera底下node的culling。
        /// 回傳被卡掉的node數量。
        /// </summary>
        public int ProcessCameraFrustumCulling(I_Camera camera, int viewW, int viewH)
        {
            return ProcessCameraFrustumCulling(camera, viewW, viewH, null);
        }

        

        /// <summary>
        /// 處理將Camera的frustum對底下的loader作culling計算，倍culling調的node，BeCulled屬性會是false，在render時會被略過。
        /// 回傳被卡掉的BOX數量
        /// myViewProjection 用來設定frustum是否要使用Camer的frustum，要用camera本身的就傳null。
        /// 若是要傳myViewProjection，那viewW, viewH傳0就好。
        /// </summary>
        int ProcessCameraFrustumCulling(I_Camera camera, int viewW, int viewH, Matrix? myViewProjection)
        {
            //I_Camera camera = null;
            //if (cameraName == null)
            //    camera = GetLastCamera();
            //else
            //    camera = GetCamera(ref cameraName);

            camera.SetAllCameraNodeBeCulled(true);

            Matrix viewProjection;
            if (myViewProjection.HasValue == false)
            {
                Matrix projectionMat, cameraMat, invCameraMat;
                camera.GetProjectionMatrix(viewW, viewH, out projectionMat);
                camera.GetCameraMatrix(out cameraMat, out invCameraMat);
                // Vector3 cameraPosition = cameraMat.Translation;
                Matrix viewMatrix = invCameraMat;
                Matrix invViewMatrix = cameraMat;

                viewProjection = viewMatrix * projectionMat;
            }
            else
                viewProjection = myViewProjection.Value;

            List<EntityTag> entityList = m_BasicPhysics.FrustumCulling(ref viewProjection);
            List<EntityTag>.Enumerator itr = entityList.GetEnumerator();
            int okBox = entityList.Count;
            while (itr.MoveNext())
            {           
                ViewCullingMeshNode vcNode = itr.Current.sceneNode as ViewCullingMeshNode;
                if (vcNode.ViewFrustumCullingBoundingBox(ref viewProjection) == true)
                    okBox--;
            }
            itr.Dispose();

            return m_BasicPhysics.GetEntityAmount() - okBox;

            //if (IfCullingScene == false)
            //    return afterCullingObject;

            //把所有場景 不管要不要偵測 全都關了       
            //while (cullingDataItr.MoveNext())
            //{
            //    bool bMeshNodeCulling = cullingDataItr.Current.Value.MeshNodeCulling;
            //    bool bInstanceNodeCulling = cullingDataItr.Current.Value.InstanceNodeCulling;
            //    bool bSkinningNodeCulling = cullingDataItr.Current.Value.SkinningNodeCulling;
            //    loaderItr = cullingDataItr.Current.Value.LoaderList.GetEnumerator();
            //    while (loaderItr.MoveNext())
            //    {
            //        if (bMeshNodeCulling)
            //            SceneNode.SetAllBeCulled(loaderItr.Current.GetRootNode, typeof(MeshNode));
            //        if (bInstanceNodeCulling)
            //            SceneNode.SetAllBeCulled(loaderItr.Current.GetRootNode, typeof(InstanceSimpleMeshNode));
            //        if (bSkinningNodeCulling)
            //            SceneNode.SetAllBeCulled(loaderItr.Current.GetRootNode, typeof(SkinningMeshNode));
            //    }
            //}

            ////把Instance Node清空
            //CullingInstanceNodeArray.Reset();
            //loaderItr = m_LoaderCullingData[filter].LoaderList.GetEnumerator();
            //while (loaderItr.MoveNext())
            //{
            //    SceneNode.AddNodeSameType(
            //        loaderItr.Current.GetRootNode, CullingInstanceNodeArray, typeof(InstanceSimpleMeshNode));
            //}
            //for (int a = 0; a < CullingInstanceNodeArray.nowNodeAmount; a++)
            //    ((InstanceSimpleMeshNode)CullingInstanceNodeArray.nodeArray[a]).RefreshInstanceMatrix_Clear();

            ////culling 偵測，只開filter有match的。
            //StillDesign.PhysX.Shape[] shapes = MyPhyX.ConsiderViewFrustumCulling(
            //    scene, StillDesign.PhysX.ShapesType.All, filter, ref ViewFrustrum);

            //for (int a = 0; a < shapes.Length; a++)
            //{
            //    if (shapes[a].Actor.UserData.GetType() == typeof(MeshNode))
            //    {
            //        MeshNode meshNode = shapes[a].Actor.UserData as MeshNode;
            //        meshNode.BeCulled = false;
            //    }
            //    else if (shapes[a].Actor.UserData.GetType() == typeof(InstanceCullingData))
            //    {
            //        InstanceCullingData insdata = shapes[a].Actor.UserData as InstanceCullingData;
            //        insdata.m_InstanceSimpleMeshNode.RefreshInstanceMatrix_Add(
            //            insdata.m_LocatorNode.WorldMatrix);
            //        insdata.m_InstanceSimpleMeshNode.BeCulled = false;
            //    }
            //    else if (shapes[a].Actor.UserData.GetType() == typeof(SkinningCullingData))
            //    {
            //        SkinningCullingData skinData = shapes[a].Actor.UserData as SkinningCullingData;
            //        skinData.m_SkinningMeshNode.BeCulled = false;
            //    }
            //}

            ////把Instance Noe設定進去
            //for (int a = 0; a < CullingInstanceNodeArray.nowNodeAmount; a++)
            //{
            //    ((InstanceSimpleMeshNode)CullingInstanceNodeArray.nodeArray[a]).RefreshInstanceMatrix_SetRenderArray();
            //}

            //return shapes.Length;
        }



        /// <summary>
        /// 將有建立OBB的loader儲存起來，在render all mesh前把他們全都culled = true，
        /// 然後再view culling把有範圍內的都設定成culled = false，
        /// 沒有經過此function設定的，就不存在list內，不做視野裁切。
        /// </summary>     
        protected void CreateViewFrustumCulling_MeshNode(BasicMeshLoader loader)//, bool bStaticObject)
        {
            int count = 0;
            SceneNode.CountTreeAllNode(loader.GetRootNode, ref count, typeof(MeshNode));
            NodeArray nodeArray = new NodeArray(count);
            SceneNode.AddNodeSameType(loader.GetRootNode, nodeArray,
                typeof(MeshNode), true, true);//沒貼圖不用建

            RSTMatrix rstMat = RSTMatrix.Init();

            for (int a = 0; a < nodeArray.nowNodeAmount; a++)
            {
                MeshNode meshNode = nodeArray.nodeArray[a] as MeshNode;

                rstMat.SetData(meshNode.WorldMatrix);
                object actor = m_BasicPhysics.CreateBox(1,
                    meshNode.BoundingBox,
                    rstMat.m_Scale,
                    meshNode);

                //算bound box中心的相對node圓點距離
                Vector3 bpos = ((meshNode.BoundingBox.Max + meshNode.BoundingBox.Min) * 0.5f);
                //m_BasicPhysics.SetBoundingBoxLocal(actor, bpos);

                RTMatrix rtMat = RTMatrix.Init();
                rtMat.SetData(meshNode.WorldMatrix);
                //Matrix mat = meshNode.WorldMatrix;
                m_BasicPhysics.SetPose(actor, ref rtMat.m_Quaternion, ref rtMat.m_Translation);
                //m_BasicPhysics.SetFreeze(actor, true);//這邊關active似乎沒有用

                ViewFrustumCullingActorArray viewFrustumCullingActorArray = new ViewFrustumCullingActorArray(m_BasicPhysics, 1);
                viewFrustumCullingActorArray.Add(actor);

                meshNode.SetCullingData(viewFrustumCullingActorArray);
                //meshNode.SetLocalMatrixCallback = ViewCullingMeshNode.SetLocalMatrixDone;
            }

        }

    }
}
