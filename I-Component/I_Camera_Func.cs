﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;


namespace I_Component
{
    public partial class I_Camera
    {
        /// <summary>
        /// 判斷此I_Camera是否為傳入的名字
        /// </summary>      
        public bool IfName(ref string CheckName)
        {
            return Name == CheckName;
        }

        /// <summary>
        /// 傳入viewport 寬高計算aspectRatio，取得透視投影
        /// </summary>       
        public void GetProjectionMatrix(float viewportW, float viewportH, out Matrix proMat)
        {
            float aspectRatio = viewportW / viewportH;

            Matrix.CreatePerspectiveFieldOfView(
               MathHelper.ToRadians(FovDegree), aspectRatio, Near, Far, out proMat);
        }

        /// <summary>
        /// 取得camera的Matrix
        /// </summary>
        public void GetCameraMatrix(out Matrix cameraMat, out Matrix invCameraMat)
        {
            cameraMat = CameraMatrix;
            Matrix.Invert(ref cameraMat, out invCameraMat);
        }
       
          /// <summary>
        /// 取得camera的Matrix
        /// </summary>
        public void SetCameraMatrix(ref Matrix cameraMat)
        {
            CameraMatrix = cameraMat;
        
        }

        /// <summary>
        /// 取得Camera要畫出的Loader
        /// </summary>
        /// <returns></returns>
        public BasicMeshLoader[] GetLoaders()
        {
            return m_RenderLoader.ToArray();
        }


        /// <summary>
        /// 檢查loader是否位於camera的畫出清單中
        /// </summary>
        public bool IfInRenderLoader(BasicMeshLoader loader)
        {
            return m_RenderLoader.Contains(loader);
        }

        ///// <summary>
        ///// 取得此camera專用的Render target
        ///// </summary>
        //public RenderTarget2D RenderTarget()
        //{
        //    return m_cameraRT;
        //}


        /// <summary>
        /// 取得m_RenderLoader的Enumerator，讓外部可以得到loader資料。
        /// </summary>
        //public List<BasicMeshLoader>.Enumerator GetRenderLoadersItr()
        //{
        //    return m_RenderLoader.GetEnumerator();
        //}



        /// <summary>
        /// 設定Camera底下的所有Node狀態是否為倍Cull掉
        /// </summary>
        public void SetAllCameraNodeBeCulled(bool bCulled)
        {
            //先將camera底下的MeshNode都設為被Culled
            List<MeshNode>.Enumerator mNodeItr = GetAllMeshNodeItr();
            while (mNodeItr.MoveNext())
                mNodeItr.Current.BeCulled = bCulled;
            mNodeItr.Dispose();

            //將camera底下的SkinningMeshNode如果有骨架上的Box設定檔，都設為被Culled。
            //若是沒有骨架上的Box設定檔，則表示不要做Culling
            List<SkinningMeshNode>.Enumerator mSkinNodeItr = GetAllSkinningMeshNodeItr();
            while (mSkinNodeItr.MoveNext())
                mSkinNodeItr.Current.BeCulled = bCulled;
            mSkinNodeItr.Dispose();

       //Grass Node暫時不做culling計算，因為他的bounding box需要拿生GrassNode的模型來設定，還沒寫
            List<GrassNode>.Enumerator mGrassNodeItr = GetAllGrassNodeItr();
            while (mGrassNodeItr.MoveNext())
                mGrassNodeItr.Current.BeCulled = false;
            mGrassNodeItr.Dispose();
        }
    }
}
