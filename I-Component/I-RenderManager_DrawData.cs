﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        EffectParameter parPointLightArray = null;
        struct PointLightPar
        {
            public EffectParameter position;
            public EffectParameter color;
            public EffectParameter decay;
            public EffectParameter range;
            public EffectParameter strength;
            public EffectParameter specularIntensity;
       //     public Vector3 oldPos, oldColor;
        //    public float oldDecay, oldRange, oldStrength, oldSpecularIntensity;
        }
        PointLightPar[] parPointLightParArray = null;//new PointLightPar[SceneNode.ShaderMaxPointLight];
        EffectParameter parPointLightAmount = null;

        EffectParameter parSpotLightArray = null;
        struct SpotLightPar
        {
            public EffectParameter position;
            public EffectParameter direction;
            public EffectParameter angleTheta;
            public EffectParameter anglePhi;
            public EffectParameter strength;
            public EffectParameter decay;
            public EffectParameter color;
            public EffectParameter range;
       //     public Vector3 oldPos, oldColor, oldDir;
         //   public float oldDecay, oldRange, oldStrength, oldAngleTheta, oldAnglePhi;
        }
        SpotLightPar[] parSpotLightParArray = null;//new SpotLightPar[ShaderMaxSpotLight];
        EffectParameter parSpotLightAmount = null;


        /// <summary>
        /// 取得point light effect裡面的point light陣列par
        /// </summary>
        void GetPointLightPar()
        {
            if (parPointLightParArray != null)
                return;

            parPointLightArray = m_PointLightEffect.Parameters["PointLightArray"];
            MaxPointLightPerPass = parPointLightArray.Elements.Count;
            parPointLightParArray = new PointLightPar[MaxPointLightPerPass];
            for (int a = 0; a < parPointLightParArray.Length; a++)
            {
                parPointLightParArray[a].position = parPointLightArray.Elements[a].StructureMembers["position"];
                parPointLightParArray[a].color = parPointLightArray.Elements[a].StructureMembers["color"];
                parPointLightParArray[a].decay = parPointLightArray.Elements[a].StructureMembers["decay"];
                parPointLightParArray[a].range = parPointLightArray.Elements[a].StructureMembers["range"];
                parPointLightParArray[a].strength = parPointLightArray.Elements[a].StructureMembers["strength"];
                parPointLightParArray[a].specularIntensity = parPointLightArray.Elements[a].StructureMembers["specularIntensity"];
            }

            parPointLightAmount = m_PointLightEffect.Parameters["PointLightAmount"];
        }

        /// <summary>
        /// 取得spot light effect裡面的spot light陣列par
        /// </summary>
        void GetSpotLightPar()
        {
            if (parSpotLightParArray != null)
                return;

            parSpotLightArray = m_SpotLightEffect.Parameters["SPlightArray"];
            MaxSpotLightPerPass = parSpotLightArray.Elements.Count;
            parSpotLightParArray = new SpotLightPar[MaxSpotLightPerPass];
            for (int a = 0; a < parSpotLightParArray.Length; a++)
            {
                parSpotLightParArray[a].position = parSpotLightArray.Elements[a].StructureMembers["position"];
                parSpotLightParArray[a].direction = parSpotLightArray.Elements[a].StructureMembers["direction"];
                parSpotLightParArray[a].color = parSpotLightArray.Elements[a].StructureMembers["color"];
                parSpotLightParArray[a].decay = parSpotLightArray.Elements[a].StructureMembers["decay"];
                parSpotLightParArray[a].range = parSpotLightArray.Elements[a].StructureMembers["range"];
                parSpotLightParArray[a].strength = parSpotLightArray.Elements[a].StructureMembers["strength"];

                parSpotLightParArray[a].angleTheta = parSpotLightArray.Elements[a].StructureMembers["angleTheta"];
                parSpotLightParArray[a].anglePhi = parSpotLightArray.Elements[a].StructureMembers["anglePhi"];
           
         //       parSpotLightParArray[a].specularIntensity = parPointLightArray.Elements[a].StructureMembers["specularIntensity"];
            }

            parSpotLightAmount = m_SpotLightEffect.Parameters["SpotLightAmount"];
        }

        /// <summary>
        /// 設定點光源參數給shader
        /// </summary>
        void SetPointLightPar(PointLightNode [] PLNodes, int lightAmount)
        {
            parPointLightAmount.SetValue(lightAmount);
            for (int a = 0; a < lightAmount; a++)
            {
                parPointLightParArray[a].position.SetValue(PLNodes[a].WorldPosition);
                parPointLightParArray[a].color .SetValue(PLNodes[a].LightColor.ToVector3());
                parPointLightParArray[a].decay.SetValue(PLNodes[a].Decay);
                parPointLightParArray[a].range.SetValue(PLNodes[a].RangeValue);
                parPointLightParArray[a].strength.SetValue(PLNodes[a].Strength);
                parPointLightParArray[a].specularIntensity.SetValue(PLNodes[a].SpecularIntensity);
            }
        }

        /// <summary>
        /// 設定Spot light光源參數給shader
        /// </summary>
        void SetSpotLightPar(SpotLightNode[] SPLNodes, int lightAmount)
        {
            parSpotLightAmount.SetValue(lightAmount);
            for (int a = 0; a < lightAmount; a++)
            {
                parSpotLightParArray[a].position.SetValue(SPLNodes[a].WorldPosition);
                parSpotLightParArray[a].direction.SetValue(SPLNodes[a].WorldForward *-1f );
                parSpotLightParArray[a].color.SetValue(SPLNodes[a].ColorVector3());
                parSpotLightParArray[a].decay.SetValue(SPLNodes[a].Data.decay);
                parSpotLightParArray[a].range.SetValue(SPLNodes[a].Data.range);
                parSpotLightParArray[a].strength.SetValue(SPLNodes[a].Data.strength);
                parSpotLightParArray[a].angleTheta.SetValue(  (180f- SPLNodes[a].Data.angleTheta) /180f);
                parSpotLightParArray[a].anglePhi.SetValue(  (180f - SPLNodes[a].Data.anglePhi)/180f);
            }
        }
    }
}
