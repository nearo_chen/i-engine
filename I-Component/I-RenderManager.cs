﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

using YYY = I_XNAComponent.I_ParticleManager;

namespace I_Component
{
    /// <summary>
    /// 一個render manager是一個繪圖核心，一個camera是一個攝影機，render manager負責處理camera會需要用到的處理材料，
    /// 將資訊傳入camera中，camera再將資料做處理(使用deferred shading計算材質 + HDR + godsray...等)最後輸出成一個畫面texture
    /// 繪製shadow map資訊時也是仿照此作法。
    /// camera資訊會有project資訊，位置角度資訊，lensflare，HDR，灰階，景深，動態模糊，AO...之類的shader處理效果
    /// 
    /// 結論：
    /// 整合了camera與render manager的概念：
    /// camera必須負責處理視野內的物件裁切，裁切好的物件送去render manager繪出defferred shading需要用到的資訊，
    /// 之後在使用各項後處理輸出最後畫面為一個I_RenderManager最後輸出的texture，該texture為一個scene的畫面。
    /// 
    /// 在場景樹狀圖上至少會有一個mainCamera(I_CameraRenderNode)，CameraRenderNode繼承LocatorNode，
    /// 於編輯器上可以設定Active狀態會將該camera的場景顯示在右邊主化面中(將來可以開許多小視窗顯示其他不同的
    /// camera內容，或將camera可視範圍標出來做調整)
    /// </summary>
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        /// <summary>
        /// 使用者另外處理畫於畫面半透明
        /// </summary>
        protected abstract void UserRenderAlphaBlending();


        /// <summary>
        /// 使用者另外處理畫於畫面最上層
        /// </summary>
        protected abstract void UserRenderTopMost();

        LensFlareComponent m_LensFlareComponent = null;
        MyBloom m_MyBloom;

        EffectRenderManager m_GrassEffect;
        EffectRenderManager m_DeferredSimpleEffect;
        EffectRenderManager m_DeferredSkinnedEffect;
        EffectRenderManager m_SimpleEffect;
        EffectRenderManager m_SimpleSkinningEffect;
        //   EffectRenderManager m_InstanceMeshEffectRenderManager;
        //  EffectRenderManager m_NowRenderManager;
        //EffectRenderManager m_DeferredEffectManager;
        int m_sceneIDCount = 0;
        List<I_SceneManager> m_SceneList;

        /// <summary>
        /// 底層提供用來化2D圖用的spriteBatch
        /// </summary>
        protected SpriteBatch m_SpriteBatch;

        public enum ScreenRatio { ratio4x3, ratio16x9 }
        protected ScreenRatio m_ScreenRatio = ScreenRatio.ratio4x3;
        protected int m_GBufferWidth;
        protected int m_GBufferHeight;


        //DepthStencilBuffer m_GDepthBuffer;
        RenderTarget2D m_ColorRT;
        RenderTarget2D m_NormalRT;
        RenderTarget2D m_DepthRT;
        RenderTarget2D m_OtherMap;
        RenderTarget2D m_LightRT;
        RenderTarget2D m_EmmisiveRT;

        RenderTarget2D m_RefractRT;
        public DepthStencilBuffer m_SuperBigDepthBuffer;//請於發部版本將這位幹掉

        RenderTarget2D m_FinalRT;

        Effect m_EmmBlendEffect = null;
        Effect m_ClearBufferEffect;
        Effect m_EmmisiveEffect;
        Effect m_DirectionalLightEffect, m_PointLightEffect, m_SpotLightEffect;
        Effect m_FinalCombineEffect;
        //Effect m_DeferredDrawLine;
        Effect m_RestoreDepthEffect;
        Effect m_RefractMapEffect;
        Effect m_RenderRefract;
        Effect m_DOFCombineEffect;

        Effect m_SSAOEffect, m_SSAOBlurEffect, m_SSAOFinalEffect, m_PSSMEffect, m_PSSMBlurEffect;
        Texture2D m_RandomNormalTexture;


        Vector2 halfPixel;
        float MaxMaterialBankSize = 0;
        int MaxPointLightPerPass = 0;
        int MaxSpotLightPerPass = 0;
        override public void Initialize(IServiceProvider iServiceProvider, GraphicsDevice device, MySound sound)
        {
            base.Initialize(iServiceProvider, device, sound);

            string tempString;
            m_SceneList = new List<I_SceneManager>(256);
            m_SpriteBatch = new SpriteBatch(device);

            if (Utility.IfContentOutSide)
            {
                m_GrassEffect = new EffectRenderManager();
                tempString = "XNBData\\Content\\fx\\render\\GrassMesh";
                Utility.GetFullPath(ref tempString, out tempString);
                m_GrassEffect.LoadEffect(m_Content, tempString);

                m_DeferredSimpleEffect = new EffectRenderManager();
                tempString = "XNBData\\Content\\fx\\render\\DeferredEffect";
                Utility.GetFullPath(ref tempString, out tempString);
                m_DeferredSimpleEffect.LoadEffect(m_Content, tempString);

                m_DeferredSkinnedEffect = new EffectRenderManager();
                tempString = "XNBData\\Content\\fx\\render\\DeferredSkinning";
                Utility.GetFullPath(ref tempString, out tempString);
                m_DeferredSkinnedEffect.LoadEffect(m_Content, tempString);

                m_SimpleEffect = new EffectRenderManager();
                tempString = "XNBData\\Content\\fx\\render\\SimpleEffect";
                Utility.GetFullPath(ref tempString, out tempString);
                m_SimpleEffect.LoadEffect(m_Content, tempString);

                m_SimpleSkinningEffect = new EffectRenderManager();
                tempString = "XNBData\\Content\\fx\\render\\SimpleSkinning";
                Utility.GetFullPath(ref tempString, out tempString);
                m_SimpleSkinningEffect.LoadEffect(m_Content, tempString);

                tempString = "XNBData\\Content\\fx\\ClearGBuffer";
                Utility.GetFullPath(ref tempString, out tempString);
                m_ClearBufferEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\fx\\DirectionalLight";
                Utility.GetFullPath(ref tempString, out tempString);
                m_DirectionalLightEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\fx\\PointLight";
                Utility.GetFullPath(ref tempString, out tempString);
                m_PointLightEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\fx\\SpotLight";
                Utility.GetFullPath(ref tempString, out tempString);
                m_SpotLightEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\fx\\CombineFinal";
                Utility.GetFullPath(ref tempString, out tempString);
                m_FinalCombineEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\fx\\EmmisiveMap";
                Utility.GetFullPath(ref tempString, out tempString);
                m_EmmisiveEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\fx\\EmmisiveBlend";
                Utility.GetFullPath(ref tempString, out tempString);
                m_EmmBlendEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                //tempString = "XNBData\\Content\\fx\\DeferredRestoreDepth";
                //Utility.GetFullPath(ref tempString, out tempString);
                //m_DeferredDrawLine = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\fx\\RestoreDepth";
                Utility.GetFullPath(ref tempString, out tempString);
                m_RestoreDepthEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\fx\\render\\SimpleRefract";
                Utility.GetFullPath(ref tempString, out tempString);
                m_RefractMapEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\fx\\Effects\\RefractDistort";
                Utility.GetFullPath(ref tempString, out tempString);
                m_RenderRefract = Utility.ContentLoad_Effect(m_Content, tempString);
                m_RenderRefract.CurrentTechnique = m_RenderRefract.Techniques["Distort"];

                tempString = "XNBData\\Content\\fx\\DOFCombine";
                Utility.GetFullPath(ref tempString, out tempString);
                m_DOFCombineEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\ssao\\SSAOEffect";
                Utility.GetFullPath(ref tempString, out tempString);
                m_SSAOEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\ssao\\SSAOBlurEffect";
                Utility.GetFullPath(ref tempString, out tempString);
                m_SSAOBlurEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\ssao\\SSAOFinalPass";
                Utility.GetFullPath(ref tempString, out tempString);
                m_SSAOFinalEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\ssao\\RANDOMNORMAL";
                Utility.GetFullPath(ref tempString, out tempString);
                m_RandomNormalTexture = Utility.ContentLoad_Texture2D(m_Content, tempString);

                tempString = "XNBData\\Content\\PSSM\\PSSM";
                Utility.GetFullPath(ref tempString, out tempString);
                m_PSSMEffect = Utility.ContentLoad_Effect(m_Content, tempString);

                tempString = "XNBData\\Content\\PSSM\\PSSMBlurEffect";
                Utility.GetFullPath(ref tempString, out tempString);
                m_PSSMBlurEffect = Utility.ContentLoad_Effect(m_Content, tempString);

            }
            else
            {
                m_GrassEffect = new EffectRenderManager();
                m_GrassEffect.LoadEffect(m_Content, "fx\\render\\GrassMesh");

                m_DeferredSimpleEffect = new EffectRenderManager();
                m_DeferredSimpleEffect.LoadEffect(m_Content, "fx\\render\\DeferredEffect");

                m_DeferredSkinnedEffect = new EffectRenderManager();
                m_DeferredSkinnedEffect.LoadEffect(m_Content, "fx\\render\\DeferredSkinning");

                m_ClearBufferEffect = Utility.ContentLoad_Effect(m_Content, "fx\\ClearGBuffer");
                m_DirectionalLightEffect = Utility.ContentLoad_Effect(m_Content, "fx\\DirectionalLight");
                m_PointLightEffect = Utility.ContentLoad_Effect(m_Content, "fx\\PointLight");
                m_SpotLightEffect = Utility.ContentLoad_Effect(m_Content, "fx\\SpotLight");
                m_FinalCombineEffect = Utility.ContentLoad_Effect(m_Content, "fx\\CombineFinal");
                m_EmmisiveEffect = Utility.ContentLoad_Effect(m_Content, "fx\\EmmisiveMap");
                m_EmmBlendEffect = Utility.ContentLoad_Effect(m_Content, "fx\\EmmisiveBlend");
                //m_DeferredDrawLine = Utility.ContentLoad_Effect(m_Content, "fx\\DeferredDrawLine");
                m_RestoreDepthEffect = Utility.ContentLoad_Effect(m_Content, "fx\\RestoreDepth");

                m_RefractMapEffect = Utility.ContentLoad_Effect(m_Content, "fx\\render\\SimpleRefract");

                m_SimpleEffect = new EffectRenderManager();
                m_SimpleEffect.LoadEffect(m_Content, "fx\\render\\SimpleEffect");

                m_SimpleSkinningEffect = new EffectRenderManager();
                m_SimpleSkinningEffect.LoadEffect(m_Content, "fx\\render\\SimpleSkinning");

                m_RenderRefract = Utility.ContentLoad_Effect(m_Content, "fx\\Effects\\RefractDistort");
                m_RenderRefract.CurrentTechnique = m_RenderRefract.Techniques["Distort"];

                m_DOFCombineEffect = Utility.ContentLoad_Effect(m_Content, "fx\\DOFCombine");

                m_SSAOEffect = Utility.ContentLoad_Effect(m_Content, "ssao\\SSAOEffect");
                m_SSAOBlurEffect = Utility.ContentLoad_Effect(m_Content, "ssao\\SSAOBlurEffect");
                m_SSAOFinalEffect = Utility.ContentLoad_Effect(m_Content, "ssao\\SSAOFinalPass");

                m_RandomNormalTexture = Utility.ContentLoad_Texture2D(m_Content, "ssao\\RANDOMNORMAL");

                m_PSSMEffect = Utility.ContentLoad_Effect(m_Content, "PSSM\\PSSM");
                m_PSSMBlurEffect = Utility.ContentLoad_Effect(m_Content, "PSSM\\PSSMBlurEffect");
            }

            m_SuperBigDepthBuffer = new DepthStencilBuffer(device, 2048, 2048, DepthFormat.Depth24);

            m_MyBloom = new MyBloom();
            m_MyBloom.LoadContent(device, m_Content, m_SpriteBatch);

            m_LensFlareComponent = new LensFlareComponent(m_Content, device);

            MaxMaterialBankSize = m_DirectionalLightEffect.Parameters["MaterialBank"].Elements.Count;
            GetPointLightPar();
            GetSpotLightPar();

            CreatePSSM();

            LoadContent(m_Content, device, m_SpriteBatch);
        }

        /// <summary>
        /// 根據目前螢幕的寬度，根據螢幕寬高比產生合適的GBuffer size，
        /// 不然視窗解析度由小變大後會降低。
        /// (注意：GBuffer解析度變高後，對於往後只需要繪出小解析度的Camera圖，
        /// 在繪製GBuffer時也會使用高解析的GBuffer作畫圖，然後再貼於小解析Camera RT上，
        /// 有浪費效能的顧慮，應該另外產生低解析的GBuffer會較有效率)
        /// </summary>
        public void SetGBufferSize(int bufferWidth, ScreenRatio ratio)
        {
            m_ScreenRatio = ratio;
            //建立deferred shading用到的buffer
            m_GBufferWidth = 0;
            m_GBufferHeight = 0;
            GetOutputWH(bufferWidth, out m_GBufferWidth, out m_GBufferHeight);
            //if (m_ScreenRatio == ScreenRatio.ratio4x3)
            //    m_GBufferHeight = (int)((float)m_GBufferWidth * 3f / 4f);
            //else if (m_ScreenRatio == ScreenRatio.ratio16x9)
            //    m_GBufferHeight = (int)((float)m_GBufferWidth * 9f / 16f);

            if (m_ColorRT != null)
            {
                DisposeAllRT();
            }

            m_FinalRT = new RenderTarget2D(m_GraphicDevice, m_GBufferWidth, m_GBufferHeight, 1, SurfaceFormat.Color, RenderTargetUsage.DiscardContents);
            //m_GDepthBuffer = new DepthStencilBuffer(m_GraphicDevice, m_GBufferWidth, m_GBufferHeight, DepthFormat.Depth24);
            m_ColorRT = new RenderTarget2D(m_GraphicDevice, m_GBufferWidth, m_GBufferHeight, 1, SurfaceFormat.Color, RenderTargetUsage.DiscardContents);
            m_NormalRT = new RenderTarget2D(m_GraphicDevice, m_GBufferWidth, m_GBufferHeight, 1, SurfaceFormat.Color, RenderTargetUsage.DiscardContents);
            m_DepthRT = new RenderTarget2D(m_GraphicDevice, m_GBufferWidth, m_GBufferHeight, 0, SurfaceFormat.Single, RenderTargetUsage.DiscardContents);
            m_OtherMap = new RenderTarget2D(m_GraphicDevice, m_GBufferWidth, m_GBufferHeight, 1, SurfaceFormat.Color, RenderTargetUsage.DiscardContents);

            m_LightRT = new RenderTarget2D(m_GraphicDevice, m_GBufferWidth, m_GBufferHeight, 1, SurfaceFormat.Color, RenderTargetUsage.DiscardContents);

            //計算自發光map之後alpha blending貼在全畫面上，不需要做pixel計算，所以解析度可以使用小一點的。
            m_EmmisiveRT = new RenderTarget2D(m_GraphicDevice, m_GBufferWidth, m_GBufferHeight, 1, SurfaceFormat.Color, RenderTargetUsage.DiscardContents);

            m_RefractRT = new RenderTarget2D(m_GraphicDevice, m_GBufferWidth, m_GBufferHeight, 1, SurfaceFormat.Color, RenderTargetUsage.DiscardContents);

            halfPixel.X = -1f * 0.5f / (float)m_GBufferWidth;
            halfPixel.Y = -1f * 0.5f / (float)m_GBufferHeight;
        }

        void DisposeAllRT()
        {
            m_ColorRT.Dispose();
            m_NormalRT.Dispose();
            m_DepthRT.Dispose();
            m_OtherMap.Dispose();
            m_LightRT.Dispose();
            m_EmmisiveRT.Dispose();
            m_RefractRT.Dispose();
            m_FinalRT.Dispose();
        }

        override public void Dispose()
        {
            UnLoadContent();

            m_MyBloom.Dispose();
            m_MyBloom = null;

            m_LensFlareComponent.Dispose();
            m_LensFlareComponent = null;

            //dispose all effect
            m_GrassEffect.Dispose();
            m_DeferredSimpleEffect.Dispose();
            m_DeferredSkinnedEffect.Dispose();
            m_SimpleEffect.Dispose();
            m_SimpleSkinningEffect.Dispose();

            m_EmmBlendEffect.Dispose();
            m_ClearBufferEffect.Dispose();
            m_DirectionalLightEffect.Dispose();
            m_PointLightEffect.Dispose();
            m_SpotLightEffect.Dispose();
            m_FinalCombineEffect.Dispose();
            m_EmmisiveEffect.Dispose();
            //m_DeferredDrawLine.Dispose();
            m_RestoreDepthEffect.Dispose();
            m_RefractMapEffect.Dispose();
            m_RenderRefract.Dispose();
            m_DOFCombineEffect.Dispose();

            m_SSAOEffect.Dispose();
            m_SSAOBlurEffect.Dispose();
            m_SSAOFinalEffect.Dispose();
            m_RandomNormalTexture.Dispose();

            m_PSSMEffect.Dispose();
            m_PSSMBlurEffect.Dispose();

            m_SpriteBatch.Dispose();
            m_SpriteBatch = null;

            List<I_SceneManager>.Enumerator itr = m_SceneList.GetEnumerator();
            if (itr.MoveNext())
                itr.Current.Dispose();
            itr.Dispose();
            m_SceneList.Clear();
            m_SceneList = null;

            DisposeAllRT();

            m_SuperBigDepthBuffer.Dispose();
            m_SuperBigDepthBuffer = null;


            DisposePSSM();

            //tmpNodes.Dispose();
            //tmpNodes = null;
            base.Dispose();
        }

        //protected void StartDrawLine()
        //{
        //    DrawLine.AddLineBegin(m_GraphicDevice);
        //}

        //protected void EndDrawLine(int sceneID, string cameraName)
        //{
        //    #region get
        //    I_SceneManager scene = GetScene(sceneID);

        //    I_Camera camera;
        //    if (cameraName == null)
        //        camera = scene.GetLastCamera();
        //    else
        //        camera = scene.GetCamera(ref cameraName);
        //    if (camera == null)
        //        return;

        //    Matrix projectionMat, cameraMat, invCameraMat;
        //    camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
        //    camera.GetCameraMatrix(out cameraMat, out invCameraMat);
        //    Vector3 cameraPosition = cameraMat.Translation;
        //    Matrix viewMatrix = invCameraMat;
        //    Matrix invViewMatrix = cameraMat;
        //    #endregion

        //    DrawLine.Draw(viewMatrix, projectionMat);
        //}

        /// <summary>
        /// 設定Deferred shading需要用到的Render Target及參數，設定之後可以呼叫RenderCameraScreen()，
        /// 將想要畫出的Camera及場景，一起畫在Gbuffer上。
        /// </summary>
        protected void DeferredShading_Begin()
        {
            if (I_Utility.IfDrawDebug)
                DrawLine.AddLineBegin(m_GraphicDevice);

            //m_GraphicDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.CornflowerBlue, 1.0f, 0);

            //請把設定放在SetGBuffer();ClearGBuffer();之前
            m_GraphicDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            m_GraphicDevice.RenderState.DepthBufferEnable = true;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = true;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.SeparateAlphaBlendEnabled = false;
            m_GraphicDevice.RenderState.AlphaTestEnable = false;//透空寫在shader裡clip( )了。


            //設定deferred shading的render state 和render target
            SetGBuffer();
            ClearGBuffer();
        }

        /// <summary>
        /// 取消設定GBuffer的Rrender Target狀態，之後可以將Gbuffer呼叫GetTexture()取出，並做
        /// Deferred shading後處理。
        /// </summary>
        protected void DeferredShading_End()
        {
            ResolveGBuffer();
        }

        /// <summary>
        /// 將scene裡的camera要化的物件畫出GBuffer。
        /// </summary>
        protected void DeferredRenderCameraScreen_MeshNode(int sceneID, I_Camera[] cameras, bool bDrawTangentMesh)
        {
            //由I-Camera中取得要畫出的Node，使用deferred shading的effect畫出GBuffer
            if (bDrawTangentMesh)
                m_DeferredSimpleEffect.SetRenderBegin_DeferredShading(SceneNode.RenderTechnique.DeferredShading | SceneNode.RenderTechnique.SimpleMesh | SceneNode.RenderTechnique.TengentMesh);
            else
                m_DeferredSimpleEffect.SetRenderBegin_DeferredShading(SceneNode.RenderTechnique.DeferredShading | SceneNode.RenderTechnique.SimpleMesh);

            I_SceneManager scene = GetScene(sceneID);
            //if (camerasName == null)
            //    camerasName = new string[] { scene.GetLastCamera().Name };

            for (int a = 0; a < cameras.Length; a++)
            {
                I_Camera camera = cameras[a];
                //camera = scene.GetCamera(ref camerasName[a]);
                if (camera == null)
                    continue;

                Matrix projectionMat, cameraMat, invCameraMat;
                camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
                camera.GetCameraMatrix(out cameraMat, out invCameraMat);
                Vector3 cameraPosition = cameraMat.Translation;
                Matrix viewMatrix = invCameraMat;
                Matrix invViewMatrix = cameraMat;

                m_DeferredSimpleEffect.DeferredShading_SetCommonEffects(ref projectionMat, ref viewMatrix);
                //     Type meshType = typeof(MeshNode);
                List<MeshNode>.Enumerator meshNodeItr = camera.GetAllMeshNodeItr();//畫出所有mesh node
                MeshNode meshNode;
                while (meshNodeItr.MoveNext())
                {
                    meshNode = meshNodeItr.Current;
                    if (meshNode.BeCulled)
                    {
                        //int add = 0; add++;
                        continue;
                    }

                    //if (meshNode.GetType() != meshType)
                    //    continue;
                    //     else 
                    if (meshNode.RefractScale > 0)//折射物體另外獨立畫折射圖的地方處理，並且要為半透明才有意義。
                        continue;
                    if (meshNode.IfLightEnable == false)//不跟光計算的模型使用舊的方式畫圖
                        continue;
                    if (meshNode.IfAlphaBlenbing == true)
                        continue;
                    else if (meshNode.IfTangentFrame == true && bDrawTangentMesh == true && meshNode.NormalTexture == null)
                        continue; //如果是tangent mesh但卻沒貼normal map，在畫normal map時，不要畫。
                    else if (meshNode.IfTangentFrame == false && bDrawTangentMesh == true)
                        continue;
                    else if (meshNode.IfTangentFrame == true && bDrawTangentMesh == false)
                    {
                        if (meshNode.NormalTexture == null)
                        { }//不畫normal map時，畫出是tangent mesh但沒normal map模型，使用自身的normal
                        else
                        { continue; }
                    }
                    //if (meshNode.IfTextured ==false)
                    //    continue;

                    m_DeferredSimpleEffect.DeferredShading_RenderMeshNode(meshNode, ref  RenderTriangles, m_GraphicDevice, bDrawTangentMesh, MaxMaterialBankSize);
                    RenderNodesDeferred++;
                }
                meshNodeItr.Dispose();
            }
            m_DeferredSimpleEffect.SetRenderEnd();
        }

        /// <summary>
        /// 將scene裡的camera要畫的物件畫出GBuffer。
        /// </summary>
        protected void DeferredRenderCameraScreen_SkinningMeshNode(int sceneID, I_Camera[] cameras, bool bDrawTangentMesh)
        {
            //由I-Camera中取得要畫出的Node，使用deferred shading的effect畫出GBuffer
            if (bDrawTangentMesh)
                m_DeferredSkinnedEffect.SetRenderBegin_DeferredShading(SceneNode.RenderTechnique.DeferredShading | SceneNode.RenderTechnique.SkinnedMesh | SceneNode.RenderTechnique.TengentMesh);
            else
                m_DeferredSkinnedEffect.SetRenderBegin_DeferredShading(SceneNode.RenderTechnique.DeferredShading | SceneNode.RenderTechnique.SkinnedMesh);

            I_SceneManager scene = GetScene(sceneID);
            //if (camerasName == null)
            //    camerasName = new string[] { scene.GetLastCamera().Name };

            for (int a = 0; a < cameras.Length; a++)
            {
                I_Camera camera = cameras[a];// scene.GetCamera(ref camerasName[a]);
                if (camera == null)
                    continue;

                Matrix projectionMat, cameraMat, invCameraMat;
                camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
                camera.GetCameraMatrix(out cameraMat, out invCameraMat);
                Vector3 cameraPosition = cameraMat.Translation;
                Matrix viewMatrix = invCameraMat;
                Matrix invViewMatrix = cameraMat;

                m_DeferredSkinnedEffect.DeferredShading_SetCommonEffects(ref projectionMat, ref viewMatrix);

                //Type skinType = typeof(SkinningMeshNode);
                List<SkinningMeshNode>.Enumerator skinItr = camera.GetAllSkinningMeshNodeItr();
                SkinningMeshNode skinNode;
                while (skinItr.MoveNext())
                {
                    skinNode = skinItr.Current;
                    if (skinNode.BeCulled)
                        continue;
                    //if (skinNode.GetType() != skinType)
                    //    continue;
                    //   else 
                    if (skinNode.RefractScale > 0)//折射物體另外獨立畫折射圖的地方處理，並且要為半透明才有意義。
                        continue;
                    if (skinNode.IfLightEnable == false)//不跟光計算的模型使用舊的方式畫圖
                        continue;
                    if (skinNode.IfAlphaBlenbing == true)
                        continue;
                    else if (skinNode.IfTangentFrame == true && bDrawTangentMesh == true && skinNode.NormalTexture == null)
                        continue; //如果是tangent mesh但卻沒貼normal map，在畫normal map時，不要畫。
                    else if (skinNode.IfTangentFrame == false && bDrawTangentMesh == true)
                        continue;
                    else if (skinNode.IfTangentFrame == true && bDrawTangentMesh == false)
                    {
                        if (skinNode.NormalTexture == null)
                        { }//不畫normal map時，畫出是tangent mesh但沒normal map模型，使用自身的normal
                        else
                        { continue; }
                    }

                    m_DeferredSkinnedEffect.DeferredShading_RenderSkinningMeshNode(skinNode, ref RenderTriangles, m_GraphicDevice);
                    RenderNodesDeferred++;
                }
                skinItr.Dispose();
            }
            m_DeferredSkinnedEffect.SetRenderEnd();
        }

        void SetGBuffer()
        {
            m_GraphicDevice.SetRenderTarget(0, m_ColorRT);
            m_GraphicDevice.SetRenderTarget(1, m_NormalRT);
            m_GraphicDevice.SetRenderTarget(2, m_DepthRT);
            m_GraphicDevice.SetRenderTarget(3, m_OtherMap);
        }

        void ResolveGBuffer()
        {
            //set all rendertargets to null. In XNA 2.0, switching a rendertarget causes the resolving of the previous rendertarget.   
            // In XNA 1.1, we needed to call GraphicsDevice.ResolveRenderTarget(i);  
            m_GraphicDevice.SetRenderTarget(0, null);
            m_GraphicDevice.SetRenderTarget(1, null);
            m_GraphicDevice.SetRenderTarget(2, null);
            m_GraphicDevice.SetRenderTarget(3, null);
        }

        /// <summary>
        /// 清除Deferred shading的G buffer資訊
        /// </summary>
        private void ClearGBuffer()
        {
            m_ClearBufferEffect.Begin();
            m_ClearBufferEffect.Techniques[0].Passes[0].Begin();
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
            m_ClearBufferEffect.Techniques[0].Passes[0].End();
            m_ClearBufferEffect.End();
        }

        /// <summary>
        /// 取得G Buffer上的ColorRT圖(必須在ResolveGBuffer()之後呼叫)
        /// </summary>
        /// <returns></returns>
        public Texture2D GetTexture_ColorRT()
        {

            return m_ColorRT.GetTexture();
        }

        /// <summary>
        /// 取得G Buffer上的DepthRT圖(必須在ResolveGBuffer()之後呼叫)
        /// </summary>
        /// <returns></returns>
        public Texture2D GetTexture_DepthRT()
        {
            return m_DepthRT.GetTexture();
        }

        /// <summary>
        /// 取得G Buffer上的NormalRT圖(必須在ResolveGBuffer()之後呼叫)
        /// </summary>
        /// <returns></returns>
        public Texture2D GetTexture_NormalRT()
        {
            return m_NormalRT.GetTexture();
        }

        /// <summary>
        /// 取得m_LightRT(必須在draw light()之後呼叫)
        /// </summary>
        /// <returns></returns>
        public Texture2D GetTexture_LightRT()
        {
            return m_LightRT.GetTexture();
        }

        /// <summary>
        /// 取得m_EmmisiveRT(必須在DrawEmmisiveMap()之後呼叫)
        /// </summary>
        /// <returns></returns>
        public Texture2D GetTexture_EmmisiveRT()
        {
            return m_EmmisiveRT.GetTexture();
        }

        /// <summary>
        /// 取得m_RefractMap(必須在DrawRafractMap()之後呼叫)
        /// </summary>
        /// <returns></returns>
        public Texture2D GetTexture_RefractRT()
        {
            return m_RefractRT.GetTexture();
        }

        /// <summary>
        /// 取得m_OtherMap(必須在DrawRafractMap()之後呼叫)
        /// </summary>
        /// <returns></returns>
        public Texture2D GetTexture_OtherMap()
        {
            return m_OtherMap.GetTexture();
        }

        public override void Update()
        {
            base.Update();

            List<I_SceneManager>.Enumerator itr = m_SceneList.GetEnumerator();
            while (itr.MoveNext())
            {
                itr.Current.Update();
            }
            itr.Dispose();
        }
    }
}
