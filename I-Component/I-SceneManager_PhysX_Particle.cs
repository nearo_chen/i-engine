﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;
using StillDesign.PhysX;

namespace I_Component
{
    public partial class I_SceneManager : System.IDisposable
    {
        static I_PhysX m_PhysX = null;
        int m_PhysXSceneID = -1;

        List<PhysXParticleEmitter> m_PhysXParticleManager = new List<PhysXParticleEmitter>(32);
        Actor m_GroundPlane = null;
        void CreatePhysXScene()
        {
            if (m_PhysX == null)
            {
                m_PhysX = I_PhysX.Create(m_GraphicDevice, SimulationType.Software);// new I_PhysX(m_GraphicDevice);
            }

            m_PhysXSceneID = m_PhysX.AddScene(Vector3.Up * -9.8f);

            // m_PhysX.GetGroupNumber
        }

       

        public void AddGroundPlane()
        {
            if (m_GroundPlane != null)
            {
                m_GroundPlane.Dispose();
                return;
            }
            BoxShapeDescription boxShapeDesc = new BoxShapeDescription(500,0.1f,500);
            ActorDescription groundActorDesc = new ActorDescription()
            {
                GlobalPose = Matrix.CreateTranslation(0,-100,0),
                Group = 5,
                Shapes = {boxShapeDesc}
            };
           m_GroundPlane = m_PhysX.GetSceneFromSceneID(m_PhysXSceneID).CreateActor(groundActorDesc);
        }

        /// <summary>
        /// 於scene中建立一個physX的particle
        /// </summary>
        /// <returns>ParticleEmitterID</returns>
        public int AddPhysXParticle()
        {
            return AddPhysXParticle(Vector3.Zero, 1000, 100,
                 10.0f, Vector3.Down, 0, 1.0f, 10.0f, 5000);
        }

        /// <summary>
        /// 於scene中建立一個physX的particle，回傳該particle的ID
        /// </summary>
        /// <returns>ParticleEmitterID</returns>
        public int AddPhysXParticle(Vector3 startingPosition, int particleMaxmum, float persecondParticles,
            float startingVelocity, Vector3 particleAcceleration, short particleGroup,
            float mass, float densify,
            int life_millisecond)
        {
        //    if (m_PhysXParticleManager == null)
          //      m_PhysXParticleManager = new List<PhysXParticleEmitter>(32);
            PhysXParticleEmitter newParticle = new PhysXParticleEmitter(m_PhysX.GetSceneFromSceneID(m_PhysXSceneID), startingPosition, particleMaxmum, persecondParticles,
                startingVelocity, particleAcceleration, particleGroup, mass, densify, life_millisecond);
            Random rd = new Random(Guid.NewGuid().GetHashCode());
            int particleEmitterId = rd.Next();
            m_PhysXParticleManager.Add(newParticle);
            return particleEmitterId;
        }

        //public List<PhysXParticleEmitter> GetPhysXParticles()
        //{
        //    return m_PhysXParticleManager;
        //}

        /// <summary>
        /// 取得所有particle資源
        /// </summary>
        public PhysXParticleEmitter[] GetPhysXParticleAsset()
        {
            if (m_PhysXParticleManager != null)
                return m_PhysXParticleManager.ToArray();
            return null;
        }

        void RemovePhysXParticle(int ParticleEmitterID)
        {
            for (int i = 0; i < m_PhysXParticleManager.Count; i++)
            {
                if (ParticleEmitterID == m_PhysXParticleManager[i].m_ParticleID)
                {
                    m_PhysXParticleManager.RemoveAt(i);
                    return;
                }
            }     
        }

        void UpdatePhysXParticle()
        {
            if (m_PhysX != null )
            {
                m_PhysX.UpdateScene(m_PhysXSceneID);

                if (m_PhysXParticleManager != null)
                {
                    List<PhysXParticleEmitter>.Enumerator itr = m_PhysXParticleManager.GetEnumerator();
                    while (itr.MoveNext())
                    {
                        itr.Current.ParticleEmitterUpdate();
                    }
                    itr.Dispose();
                }
            }
        }
    }
}
