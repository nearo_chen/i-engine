﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;
using I_Component.PhysicsType;

namespace I_Component
{
    /// <summary>
    /// 此類別管理一個scene中所有會用到的資源，如loader, camera, lights..., fog...particle，Ocean 之類的資源，
    /// 還有管理場景中的階層架構，可以管理由編輯器中架構的場景，控制讀檔與釋放的操作
    /// 更進一步可以建立physX的一個scene，可做view culling的管理(先暫定但不一定要這裡做)。
    /// </summary>
    public partial class I_SceneManager : IDisposable
    {
        public I_SpecialMeshData.I_SceneData m_SceneData;//目前此scene中的所有資訊，若有任何與I_SceneData有關的及時修改，都依定要更新此資料(比如說camera資訊有變，或是loader加減，或是霧的資訊變更)。

        int m_sceneID = -1;
        int m_LoaderAssetMaxSize = 1024;
        List<BasicMeshLoader> m_LoaderAsset = null;
        List<I_Camera> m_CameraAsset = null;
        I_SpecialMeshData.I_Material[] m_MaterialBank = null;

        ContentManager m_Content;
        GraphicsDevice m_GraphicDevice;

        MyPhysics m_BasicPhysics = null;//主要是拿來做view culling

        public static I_SceneManager Create(IServiceProvider provider, GraphicsDevice device, int id, I_SpecialMeshData.I_SceneData sceneData)
        {
            I_SceneManager iii = new I_SceneManager(provider, device, id, sceneData);
            I_XNAComponent.ReleaseCheck.AddCheck(iii);
            return iii;
        }

        public const int MaterialBankSize = 11;
        I_SceneManager(IServiceProvider provider, GraphicsDevice device, int id, I_SpecialMeshData.I_SceneData sceneData)
        {
            m_GraphicDevice = device;
            m_Content = new ContentManager(provider);
            m_LoaderAsset = new List<BasicMeshLoader>(m_LoaderAssetMaxSize);
            m_sceneID = id;

            m_SceneData = sceneData;

            m_CameraAsset = new List<I_Camera>(8);
            InitMaterialBank();

            m_BasicPhysics = new MyPhysics();
            m_BasicPhysics.InitializePhysics(Vector3.Zero);
            //CreatePhysXScene();
        }

        ~I_SceneManager()
        {
            I_XNAComponent.ReleaseCheck.DisposeCheckCount(this);
        }

        public void InitMaterialBank()
        {
            m_MaterialBank = new I_SpecialMeshData.I_Material[MaterialBankSize];
            m_MaterialBank[0] = new I_SpecialMeshData.I_Material();
            m_MaterialBank[0].NameID = "default";//第一個給定default值，因為是大家共用的預設值，所以千萬不能刪除，一定要在陣列0。
        }

        public void Dispose()
        {
          
            if (m_MySound != null)
                m_MySound.Dispose();
            m_MySound = null;

            //m_PhysX.RemoveScene(m_PhysXSceneID);

            I_XNAComponent.ReleaseCheck.DisposeCheck(this);

            List<BasicMeshLoader>.Enumerator itr = m_LoaderAsset.GetEnumerator();
            while (itr.MoveNext())
                itr.Current.Dispose();
            itr.Dispose();
            m_LoaderAsset.Clear();
            m_LoaderAsset = null;

            List<I_Camera>.Enumerator cameraItr = m_CameraAsset.GetEnumerator();
            while (cameraItr.MoveNext())
                cameraItr.Current.Dispose();
            cameraItr.Dispose();
            m_CameraAsset.Clear();
            m_CameraAsset = null;

            for (int a = 0; a < m_MaterialBank.Length; a++)
                m_MaterialBank[a] = null;
            m_MaterialBank = null;

            if (m_PhysXParticleManager != null)
            {
                List<PhysXParticleEmitter>.Enumerator particleItr = m_PhysXParticleManager.GetEnumerator();
                while (particleItr.MoveNext())
                    particleItr.Current.Dispose();
                particleItr.Dispose();
                m_PhysXParticleManager.Clear();
            }
            m_PhysXParticleManager = null;

            m_Content.Unload();
            m_Content.Dispose();
            m_Content = null;

            if (m_BasicPhysics != null)
                m_BasicPhysics.Dispose();
            m_BasicPhysics = null;

        }

        /// <summary>
        /// 取得m_LoaderAsset裡面每個loader的Root Node的名稱(為XML檔名資訊)
        /// </summary>
        public string[] GetLoadersName()
        {
            string[] LoadersName = new string[m_LoaderAsset.Count];
            int count = 0;
            foreach (BasicMeshLoader bml in m_LoaderAsset)
            {
                LoadersName[count] = bml.GetRootNode.NodeName;
                count++;
            }
            return LoadersName;
        }

        /// <summary>
        /// 取得m_CameraAsset裡面每個camera轉換為I_SpecialMeshData.I_CameraData的資料
        /// </summary>
        public I_SpecialMeshData.I_CameraData[] GetCamerasData()
        {
            I_SpecialMeshData.I_CameraData[] camerasData = new I_SpecialMeshData.I_CameraData[m_CameraAsset.Count];
            int count = 0;
            foreach (I_Camera ccc in m_CameraAsset)
            {
                camerasData[count] = ccc.GetCameraData();
                count++;
            }
            return camerasData;
        }

        public I_SpecialMeshData.I_Material[] GetMaterialBankData()
        {
            return m_MaterialBank;
        }

        public void Update()
        {
            m_BasicPhysics.Update();

            BasicMeshLoader loader;
            List<BasicMeshLoader>.Enumerator itr = m_LoaderAsset.GetEnumerator();
            while (itr.MoveNext())
            {
                loader = itr.Current;
                Matrix mmm = Matrix.Identity;
                loader.Update(I_GetTime.ellipseUpdateMillisecond, m_Content, m_GraphicDevice, null, ref mmm);

                //檢查loader中是否有需要parent的node將他更新matrix
                if (loader.ParentLoaderName != null && loader.ParentNode == null)
                {
                    if (loader.ParentNodeName != null)
                        loader.ParentNode = (FrameNode)GetLoader(ref loader.ParentLoaderName).GetRootNode.SeekFirstNodeAllName(ref loader.ParentNodeName);
                    else
                        loader.ParentNode = GetLoader(ref loader.ParentLoaderName).GetRootNode;
                }
                else if (loader.ParentLoaderName == null && loader.ParentNode != null)
                    loader.ParentNode = null;

                if (loader.ParentNode != null)
                    loader.GetRootNode.WorldMatrix = loader.ParentNode.WorldMatrix;
            }
            itr.Dispose();

            UpdatePhysXParticle();
        }

        /// <summary>
        /// 取得此scene的content
        /// </summary>
        /// <returns></returns>
        public ContentManager GetContent()
        {
            return m_Content;
        }
    }
}
