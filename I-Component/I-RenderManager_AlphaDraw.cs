﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        /// <summary>
        /// 將scene裡的camera要化的物件以傳統shader畫出。
        /// bAlphaBlending為目前是否要畫AlphaBlending
        /// bOnlyRenderAlphaRractModel(預設false)是指目前階段，是否要畫出"只"有設定折射的模型
        /// </summary>
        protected void RenderCameraScreen_MeshNode(int sceneID, I_Camera[] cameras, bool bDrawTangentMesh, bool bAlphaBlending)
        {
            RenderCameraScreen_MeshNode(sceneID, cameras, bDrawTangentMesh, bAlphaBlending, false);
        }

        /// <summary>
        /// 將scene裡的camera要化的物件以傳統shader畫出。
        /// bAlphaBlending為true, 
        /// bOnlyRenderAlphaRractModel(預設false)是指目前階段，是否要畫出"只"有設定折射的模型，一定是以alpha blending形式畫出，為true。
        /// </summary>
        protected void RenderCameraScreen_MeshNode_RefractOnly(int sceneID, I_Camera[] cameras, bool bDrawTangentMesh)
        {
            RenderCameraScreen_MeshNode(sceneID, cameras, bDrawTangentMesh, true, true);
        }

        /// <summary>
        /// 將scene裡的camera要畫的物件以傳統shader畫出。
        /// bAlphaBlending為目前是否要畫AlphaBlending
        /// bOnlyRenderRractModelAlphaBlending是指目前階段，是否要畫出"只"有設定折射的模型
        /// </summary>
        void RenderCameraScreen_MeshNode(int sceneID, I_Camera[] cameras, bool bDrawTangentMesh, bool bAlphaBlending, bool bOnlyRenderRractModelAlphaBlending)
        {
            if (bDrawTangentMesh)
                m_SimpleEffect.SetRenderBegin_Simple(SceneNode.RenderTechnique.SimpleMesh | SceneNode.RenderTechnique.TengentMesh);
            else
                m_SimpleEffect.SetRenderBegin_Simple(SceneNode.RenderTechnique.SimpleMesh);

            I_SceneManager scene = GetScene(sceneID);
            //if (camerasName == null)
            //    camerasName = new string[] { scene.GetLastCamera().Name };

            for (int a = 0; a < cameras.Length; a++)
            {
                I_Camera camera = cameras[a];
                //camera = scene.GetCamera(ref camerasName[a]);
                if (camera == null)
                    continue;

                Matrix projectionMat, cameraMat, invCameraMat;
                camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
                camera.GetCameraMatrix(out cameraMat, out invCameraMat);
                Vector3 cameraPosition = cameraMat.Translation;
                Matrix viewMatrix = invCameraMat;
                Matrix invViewMatrix = cameraMat;

                Vector3 fogColor;
                float FogStart, FogEnd, FogMaxRatio;
                scene.GetFog(out fogColor, out FogStart, out FogEnd, out FogMaxRatio);

                Vector3 LightDirection = Vector3.Zero;
                Vector3 LightAmbient = Vector3.Zero;
                Vector3 LightDiffuse = Vector3.Zero;
                Vector3 LightSpecular = Vector3.Zero;
                scene.GetDirectionalLight(ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

                m_SimpleEffect.Simple_SetCommonEffectPar(ref projectionMat, ref viewMatrix, ref cameraPosition,
                   ref  fogColor, FogStart, FogEnd, FogMaxRatio,
                   ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

                //由I-Camera中取得要畫出的Node
                List<MeshNode>.Enumerator meshNodeItr = camera.GetAllMeshNodeItr();//畫出所有mesh node
                MeshNode meshNode;
                while (meshNodeItr.MoveNext())
                {
                    meshNode = meshNodeItr.Current;

                    if (meshNode.BeCulled)
                        continue;

                    ////不跟光計算才能在這邊使用原始方法畫
                    //if (meshNode.IfLightEnable == true)
                    //    continue;

                    if (
                        //要與平行光計算，必須要為alpha blending物件才能在這邊畫，
                        //因為不為alpha blending物件的光計算是在deferred shading中計算了
                        (meshNode.IfLightEnable && meshNode.IfAlphaBlenbing == false)
                       )
                    {
                        //int x = 0;
                        //x++;
                        continue;
                    }

                    //折射於最後最後折射場景後畫出，判斷有設定折射的模型於最後階段以Alpha blending形式在補畫出來
                    if (bOnlyRenderRractModelAlphaBlending == false && meshNode.RefractScale > 0)//若現在是畫出普通的半透明，有設定折射的就不畫
                        continue;

                    //若現在是只畫有設定折射模型以alpha blending畫出半透明，沒折射的就不畫
                    if (bOnlyRenderRractModelAlphaBlending && meshNode.RefractScale == 0)
                        continue;

                    //如果目前是要畫alpha blending但此物件不為alpha blending就掰掰
                    if (bAlphaBlending && meshNode.IfAlphaBlenbing == false)
                        continue;
                    //如果目前不畫alpha blending但此物件為alpha blending就掰掰
                    if (bAlphaBlending == false && meshNode.IfAlphaBlenbing == true)
                        continue;
                    else if (meshNode.IfTangentFrame == true && bDrawTangentMesh == true && meshNode.NormalTexture == null)
                        continue; //如果是tangent mesh但卻沒貼normal map，在畫normal map時，不要畫。
                    else if (meshNode.IfTangentFrame == false && bDrawTangentMesh == true)
                        continue;
                    else if (meshNode.IfTangentFrame == true && bDrawTangentMesh == false)
                    {
                        if (meshNode.NormalTexture == null)
                        { }//不畫normal map時，畫出是tangent mesh但沒normal map模型，使用自身的normal
                        else
                        { continue; }
                    }
                    //if (meshNode.IfTextured ==false)
                    //    continue;

                    m_SimpleEffect.Simple_RenderMeshNode(meshNode, ref  RenderTriangles, m_GraphicDevice, bDrawTangentMesh);
                    RenderNodesOld++;
                }
                meshNodeItr.Dispose();
            }

            m_SimpleEffect.SetRenderEnd();
        }

        /// <summary>
        /// 將scene裡的camera要化的物件以傳統shader畫出。
        /// bAlphaBlending為目前是否要畫AlphaBlending
        /// </summary>
        protected void RenderCameraScreen_SkinningMeshNode(int sceneID, I_Camera[] cameras, bool bDrawTangentMesh, bool bAlphaBlending)
        {
            if (bDrawTangentMesh)
                m_SimpleSkinningEffect.SetRenderBegin_Simple(SceneNode.RenderTechnique.SkinnedMesh | SceneNode.RenderTechnique.TengentMesh);
            else
                m_SimpleSkinningEffect.SetRenderBegin_Simple(SceneNode.RenderTechnique.SkinnedMesh);

            I_SceneManager scene = GetScene(sceneID);
            //if (camerasName == null)
            //    camerasName = new string[] { scene.GetLastCamera().Name };

            for (int a = 0; a < cameras.Length; a++)
            {
                I_Camera camera = cameras[a];
                //camera = scene.GetCamera(ref camerasName[a]);
                if (camera == null)
                    continue;

                Matrix projectionMat, cameraMat, invCameraMat;
                camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
                camera.GetCameraMatrix(out cameraMat, out invCameraMat);
                Vector3 cameraPosition = cameraMat.Translation;
                Matrix viewMatrix = invCameraMat;
                Matrix invViewMatrix = cameraMat;

                Vector3 fogColor;
                float FogStart, FogEnd, FogMaxRatio;
                scene.GetFog(out fogColor, out FogStart, out FogEnd, out FogMaxRatio);

                Vector3 LightDirection = Vector3.Zero;
                Vector3 LightAmbient = Vector3.Zero;
                Vector3 LightDiffuse = Vector3.Zero;
                Vector3 LightSpecular = Vector3.Zero;
                scene.GetDirectionalLight(ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

                m_SimpleSkinningEffect.Simple_SetCommonEffectPar(ref projectionMat, ref viewMatrix, ref cameraPosition,
                      ref  fogColor, FogStart, FogEnd, FogMaxRatio,
                      ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

                //由I-Camera中取得要畫出的Node
                List<SkinningMeshNode>.Enumerator skinNodeItr = camera.GetAllSkinningMeshNodeItr();//畫出所有mesh node
                SkinningMeshNode skinNode;
                while (skinNodeItr.MoveNext())
                {
                    skinNode = skinNodeItr.Current;

                    if (skinNode.BeCulled)
                        continue;

                    if (
                        ////不跟光計算才能在這邊使用原始方法畫
                        //(meshNode.IfLightEnable == true)

                     //要與平行光計算，必須要為alpha blending物件才能在這邊畫
                        //因為不為alpha blending物件的光計算是在deferred shading中計算了
                     (skinNode.IfLightEnable && skinNode.IfAlphaBlenbing == false)
                    )
                        continue;


                    if (bAlphaBlending && skinNode.IfAlphaBlenbing == false)
                        continue;
                    if (bAlphaBlending == false && skinNode.IfAlphaBlenbing == true)
                        continue;
                    else if (skinNode.IfTangentFrame == true && bDrawTangentMesh == true && skinNode.NormalTexture == null)
                        continue; //如果是tangent mesh但卻沒貼normal map，在畫normal map時，不要畫。
                    else if (skinNode.IfTangentFrame == false && bDrawTangentMesh == true)
                        continue;
                    else if (skinNode.IfTangentFrame == true && bDrawTangentMesh == false)
                    {
                        if (skinNode.NormalTexture == null)
                        { }//不畫normal map時，畫出是tangent mesh但沒normal map模型，使用自身的normal
                        else
                        { continue; }
                    }

                    //if (meshNode.IfTextured ==false)
                    //    continue;

                    m_SimpleSkinningEffect.Simple_RenderSkinningMeshNode(skinNode, ref  RenderTriangles, m_GraphicDevice, bDrawTangentMesh);
                    RenderNodesOld++;
                }
                skinNodeItr.Dispose();
            }

            m_SimpleSkinningEffect.SetRenderEnd();
        }

        /// <summary>
        /// 將camera裡要畫的Model畫在m_RefractRT上，並還原RT設為null。
        /// </summary>       
        protected void DrawRafractMap(int sceneID, I_Camera [] cameras)
        {
            RestoreDepthOnly(m_RefractRT);//回存深度的同時，設定RT才對

            m_GraphicDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            m_GraphicDevice.RenderState.DepthBufferEnable = true;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = true;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.AlphaTestEnable = false;

            //m_GraphicDevice.SetRenderTarget(0, m_RefractRT);//這裡設定RT，depth buffer就跑掉了。
            //m_GraphicDevice.SetRenderTarget(1, null);
            //m_GraphicDevice.SetRenderTarget(2, null);
            //m_GraphicDevice.SetRenderTarget(3, null);
            //  m_GraphicDevice.Clear(Color.TransparentBlack);

            RenderCameraScreen_MeshNode_RefractMap(sceneID, cameras, false);
            RenderCameraScreen_MeshNode_RefractMap(sceneID, cameras, true);

            m_GraphicDevice.SetRenderTarget(0, null);
        }

        /// <summary>
        /// 使用m_RefractMapEffect畫出折射圖，畫在m_RefractMap上。
        /// </summary>    
        void RenderCameraScreen_MeshNode_RefractMap(int sceneID, I_Camera[] cameras, bool bDrawTangentMesh)
        {
            if (bDrawTangentMesh)
                m_RefractMapEffect.CurrentTechnique = m_RefractMapEffect.Techniques["SimpleMesh_TengentMesh_NormalRefractMap"];
            else
                m_RefractMapEffect.CurrentTechnique = m_RefractMapEffect.Techniques["SimpleMesh_NormalRefractMap"];

            m_RefractMapEffect.Begin();
            m_RefractMapEffect.CurrentTechnique.Passes[0].Begin();

            //I_SceneManager scene = GetScene(sceneID);
            //if (camerasName == null)
            //    camerasName = new string[] { scene.GetLastCamera().Name };

            for (int a = 0; a < cameras.Length; a++)
            {
                I_Camera camera = cameras[a];
                //camera = scene.GetCamera(ref camerasName[a]);
                if (camera == null)
                    continue;

                Matrix projectionMat, cameraMat, invCameraMat;
                camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
                camera.GetCameraMatrix(out cameraMat, out invCameraMat);
                Vector3 cameraPosition = cameraMat.Translation;
                Matrix viewMatrix = invCameraMat;
                Matrix invViewMatrix = cameraMat;

                m_RefractMapEffect.Parameters["ViewMatrix"].SetValue(viewMatrix);//view 每個node都一樣傳一次就好。

                //由I-Camera中取得要畫出的Node
                List<MeshNode>.Enumerator meshNodeItr = camera.GetAllMeshNodeItr();//畫出所有mesh node
                MeshNode meshNode;
                while (meshNodeItr.MoveNext())
                {
                    meshNode = meshNodeItr.Current;
                    if (meshNode.BeCulled)
                        continue;
                    if (meshNode.RefractScale <= 0)//只畫折射物體
                        continue;
                    if (bDrawTangentMesh && meshNode.IfTangentFrame == false)
                        continue;
                    if (bDrawTangentMesh == false && meshNode.IfTangentFrame)
                        continue;

                    m_RefractMapEffect.Parameters["WorldMatrix"].SetValue(meshNode.WorldMatrix);
                    m_RefractMapEffect.Parameters["WVPMatrix"].SetValue(meshNode.WorldMatrix * viewMatrix * projectionMat);

                    m_RefractMapEffect.Parameters["RefractMapScale"].SetValue(meshNode.RefractScale);


                    if (bDrawTangentMesh && meshNode.IfTangentFrame && meshNode.NormalTexture != null)
                        m_RefractMapEffect.Parameters["NormalMap"].SetValue(meshNode.NormalTexture);

                    m_RefractMapEffect.CommitChanges();
                    meshNode.Render(null, ref RenderTriangles, m_GraphicDevice);

                    RenderRefractNodes++;
                }
                meshNodeItr.Dispose();
            }

            m_RefractMapEffect.CurrentTechnique.Passes[0].End();
            m_RefractMapEffect.End();
        }

    }
}
