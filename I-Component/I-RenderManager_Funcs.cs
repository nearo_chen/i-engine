﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        /// <summary>
        /// 根據ID取得scene
        /// </summary>
        public I_SceneManager GetScene(int sceneID)
        {
            List<I_SceneManager>.Enumerator itr = m_SceneList.GetEnumerator();
            while (itr.MoveNext())
                if (itr.Current.GetSceneID() == sceneID)
                    return itr.Current;
            itr.Dispose();
            return null;
        }

        public int GetScenesCount()
        {
            return m_SceneList.Count;
        }

        /// <summary>
        /// 取得render manager裡面許多scene裡面的最後一個scene，他的ID資訊。
        /// </summary>
        /// <returns></returns>
        public int GetLastSceneID()
        {
            if (m_SceneList != null && m_SceneList.Count > 0)
                return m_SceneList.Last().GetSceneID();
            return -1;
        }

     
        /// <summary>
        /// 取得所有scene的ID陣列
        /// </summary>
        /// <returns></returns>
        public int[] GetScenesIDList()
        {
            int[] iii = new int[m_SceneList.Count];
         
            List<I_SceneManager>.Enumerator itr = m_SceneList.GetEnumerator();
            int count = 0 ;
            while (itr.MoveNext())
            {
                iii[count] = itr.Current.GetSceneID();
                count++;
            }
            itr.Dispose();

            return iii;
        }

        /// <summary>
        /// 取得所有scene的name陣列
        /// </summary>
        /// <returns></returns>
        public string[] GetScenesNameList()
        {
            string[] iii = new string[m_SceneList.Count];

            List<I_SceneManager>.Enumerator itr = m_SceneList.GetEnumerator();
            int count = 0;
            while (itr.MoveNext())
            {
                iii[count] = itr.Current.m_SceneData.SceneName;
                count++;
            }
            itr.Dispose();

            return iii;
        }


        /// <summary>
        /// 設定camera資訊給BasicCameraMatrixSet 還有 I_Camera。
        /// </summary>
        public void SetCameraFocusLoader(int sceneID, string cameraName, BasicMeshLoader loader)
        {
            Matrix cameraMat;
            MyMath.GetCameraFocusMatrix(loader.BoundingSphere.Center, loader.BoundingSphere.Radius, out cameraMat);
            BasicCameraMatrixSet(cameraMat);

            I_Camera camera;
            I_SceneManager scene = GetScene(sceneID);
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            camera = scene.GetCamera(ref cameraName);
            camera.SetCameraMatrix(ref cameraMat);
        }

        /// <summary>
        /// 設定camera資訊給BasicCameraMatrixSet 還有 I_Camera。
        /// </summary>
        public void SetCameraFocusNode(int sceneID, string cameraName, FrameNode node,
            Vector3 centerOffset, float radius)
        {
            Matrix cameraMat;
            Vector3 pos = Vector3.Transform(centerOffset, node.WorldMatrix);
            MyMath.GetCameraFocusMatrix(pos, radius, out cameraMat);
            BasicCameraMatrixSet(cameraMat);

            I_Camera camera;
            I_SceneManager scene = GetScene(sceneID);
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            camera = scene.GetCamera(ref cameraName);
            camera.SetCameraMatrix(ref cameraMat);
        }




        protected Ray GetCursorRay(int sceneID, string cameraName)
        {
            I_SceneManager scene = GetScene(sceneID);

            I_Camera camera;
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            camera = scene.GetCamera(ref cameraName);
            //if (camera == null)
            //    return Ray;

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;

            Vector2 Position = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

            //create 2 positions in screenspace using the cursor position. 0 is as
            // close as possible to the camera, 1 is as far away as possible.
            Vector3 nearSource = new Vector3(Position, 0f);
            Vector3 farSource = new Vector3(Position, 1f);


            // use Viewport.Unproject to tell what those two screen space positions
            // would be in world space. we'll need the projection matrix and view
            // matrix, which we have saved as member variables. We also need a world
            // matrix, which can just be identity.
            Vector3 nearPoint = m_GraphicDevice.Viewport.Unproject(nearSource,
                projectionMat, viewMatrix, Matrix.Identity);

            Vector3 farPoint = m_GraphicDevice.Viewport.Unproject(farSource,
                projectionMat, viewMatrix, Matrix.Identity);

            // find the direction vector that goes from the nearPoint to the farPoint
            // and normalize it....
            Vector3 direction = farPoint - nearPoint;
            direction.Normalize();

            // and then create a new ray using nearPoint as the source.
            return new Ray(nearPoint, direction);
        }

        /// <summary>
        /// 取得畫圖流程最終畫面會畫出的RT，
        /// 目前baseRT為m_ColorRT
        /// </summary>
        /// <returns></returns>
        protected RenderTarget2D GetBaseRT()
        {
            return m_ColorRT;
        }

        /// <summary>
        /// 取得BaseRT的圖，同時設定SetRenderTarget(0123, null);
        /// 目前baseRT為m_ColorRT
        /// </summary>
        protected Texture2D GetTexture_BaseRT()
        {
            m_GraphicDevice.SetRenderTarget(0, null);
            m_GraphicDevice.SetRenderTarget(1, null);
            m_GraphicDevice.SetRenderTarget(2, null);
            m_GraphicDevice.SetRenderTarget(3, null);

            return m_ColorRT.GetTexture();
        }
    }
}
