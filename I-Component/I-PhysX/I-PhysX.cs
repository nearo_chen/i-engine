﻿using System;
using System.Collections.Generic;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using I_XNAUtility;
using StillDesign.PhysX;
//using HighMapEditor;


using System.ComponentModel;


namespace I_Component
{
    public partial class I_PhysX : System.IDisposable
    {
        public enum I_CollisionFilter
        {
            None,
            All,
            TypeAAll,
            TypeA1,
            TypeA2,
            TypeA3,
            TypeA4,
            TypeA5,
            TypeA6,
            TypeA7,
            TypeA8,
            TypeA9,

            TypeBAll,
            TypeB1,
            TypeB2,
            TypeB3,
            TypeB4,
            TypeB5,
            TypeB6,
            TypeB7,
            TypeB8,

            TypeCAll,
            TypeC1,
            TypeC2,
            TypeC3,
            TypeC4,
            TypeC5,
            TypeC6,
            TypeC7,
            TypeC8,

            TypeDAll,
            TypeD1,
            TypeD2,
            TypeD3,
            TypeD4,
            TypeD5,
            TypeD6,
            TypeD7,
            TypeD8,
            //TypeViewFrustumCulling,

            Amount,
        };

        /// <summary>
        /// 轉換我自己定義的Filter轉換為Group Mask
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        static GroupsMask GetGroupMask(I_CollisionFilter filter)
        {
            switch (filter)
            {
                case I_CollisionFilter.TypeAAll: return new GroupsMask(0xffffffff, 0, 0, 0);
                case I_CollisionFilter.TypeA1: return new GroupsMask(1 << 0, 0, 0, 0);
                case I_CollisionFilter.TypeA2: return new GroupsMask(1 << 1, 0, 0, 0);
                case I_CollisionFilter.TypeA3: return new GroupsMask(1 << 2, 0, 0, 0);
                case I_CollisionFilter.TypeA4: return new GroupsMask(1 << 3, 0, 0, 0);
                case I_CollisionFilter.TypeA5: return new GroupsMask(1 << 4, 0, 0, 0);
                case I_CollisionFilter.TypeA6: return new GroupsMask(1 << 5, 0, 0, 0);
                case I_CollisionFilter.TypeA7: return new GroupsMask(1 << 6, 0, 0, 0);
                case I_CollisionFilter.TypeA8: return new GroupsMask(1 << 7, 0, 0, 0);
                case I_CollisionFilter.TypeA9: return new GroupsMask(1 << 8, 0, 0, 0);

                case I_CollisionFilter.TypeBAll: return new GroupsMask(0, 0xffffffff, 0, 0);
                case I_CollisionFilter.TypeB1: return new GroupsMask(0, 1 << 0, 0, 0);
                case I_CollisionFilter.TypeB2: return new GroupsMask(0, 1 << 1, 0, 0);
                case I_CollisionFilter.TypeB3: return new GroupsMask(0, 1 << 2, 0, 0);
                case I_CollisionFilter.TypeB4: return new GroupsMask(0, 1 << 3, 0, 0);
                case I_CollisionFilter.TypeB5: return new GroupsMask(0, 1 << 4, 0, 0);
                case I_CollisionFilter.TypeB6: return new GroupsMask(0, 1 << 5, 0, 0);
                case I_CollisionFilter.TypeB7: return new GroupsMask(0, 1 << 6, 0, 0);
                case I_CollisionFilter.TypeB8: return new GroupsMask(0, 1 << 7, 0, 0);

                case I_CollisionFilter.TypeCAll: return new GroupsMask(0, 0, 0xffffffff, 0);
                case I_CollisionFilter.TypeC1: return new GroupsMask(0, 0, 1 << 0, 0);
                case I_CollisionFilter.TypeC2: return new GroupsMask(0, 0, 1 << 1, 0);
                case I_CollisionFilter.TypeC3: return new GroupsMask(0, 0, 1 << 2, 0);
                case I_CollisionFilter.TypeC4: return new GroupsMask(0, 0, 1 << 3, 0);
                case I_CollisionFilter.TypeC5: return new GroupsMask(0, 0, 1 << 4, 0);
                case I_CollisionFilter.TypeC6: return new GroupsMask(0, 0, 1 << 5, 0);
                case I_CollisionFilter.TypeC7: return new GroupsMask(0, 0, 1 << 6, 0);
                case I_CollisionFilter.TypeC8: return new GroupsMask(0, 0, 1 << 7, 0);

                case I_CollisionFilter.TypeDAll: return new GroupsMask(0, 0, 0, 0xffffffff);
                case I_CollisionFilter.TypeD1: return new GroupsMask(0, 0, 0, 1 << 0);
                case I_CollisionFilter.TypeD2: return new GroupsMask(0, 0, 0, 1 << 1);
                case I_CollisionFilter.TypeD3: return new GroupsMask(0, 0, 0, 1 << 2);
                case I_CollisionFilter.TypeD4: return new GroupsMask(0, 0, 0, 1 << 3);
                case I_CollisionFilter.TypeD5: return new GroupsMask(0, 0, 0, 1 << 4);
                case I_CollisionFilter.TypeD6: return new GroupsMask(0, 0, 0, 1 << 5);
                case I_CollisionFilter.TypeD7: return new GroupsMask(0, 0, 0, 1 << 6);
                //D8當frustum culling用
                //case I_CollisionFilter.TypeViewFrustumCulling: return new GroupsMask(0, 0, 0, 1 << 7);
                case I_CollisionFilter.TypeD8: return new GroupsMask(0, 0, 0, 1 << 7);

                case I_CollisionFilter.All: return new GroupsMask(0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff);
                case I_CollisionFilter.None:
                default: return new GroupsMask(0, 0, 0, 0);
            }
        }

        /// <summary>
        /// 2種GroupMask合併
        /// </summary>
        /// <param name="g1"></param>
        /// <param name="g2"></param>
        /// <returns></returns>
        static public GroupsMask CombineMask(GroupsMask g1, GroupsMask g2)
        {
            GroupsMask ggg = new GroupsMask(g1.Bits0 | g2.Bits0,
                g1.Bits1 | g2.Bits1,
                g1.Bits2 | g2.Bits2,
                g1.Bits3 | g2.Bits3);
            return ggg;
        }

        Engine _engine;
        Core _core;
        SimulationType m_SimType = SimulationType.Software;

        public static I_PhysX Create(GraphicsDevice device, SimulationType simType)
        {
            I_PhysX iii = new I_PhysX(device, simType);
            I_XNAComponent.ReleaseCheck.AddCheck(iii);
            return iii;
        }

        ~I_PhysX()
        {
            I_XNAComponent.ReleaseCheck.DisposeCheckCount(this);
        }

        I_PhysX(GraphicsDevice device, SimulationType simType)
        {
            m_SimType = simType;
            try
            {
                _engine = new Engine(device);
                _engine.Initalize();

                // For convenience
                _core = _engine.Core;
                //_scene = _engine.Scene;

                PhyX_SetEnableDebugLine(false);

                m_SceneGroupUsedList = new Dictionary<int, List<GroupUsedList>>(64);
            }
            catch (Exception e)
            {
                //OutputBox.SwitchOnOff = true;
                OutputBox.ShowMessage(e.Message);
            }
        }

        /// <summary>
        /// 建立一個PhysX的Scene
        /// </summary>
        /// <param name="gravity"></param>
        /// <returns></returns>
        public int AddScene(Vector3 gravity)
        {
            return _engine.AddScene(gravity, m_SimType);
        }

        /// <summary>
        /// 移除Scene，但目前一直重複加減scene會造成破碎記體。
        /// </summary>
        /// <param name="sceneID"></param>
        public void RemoveScene(int sceneID)
        {
            List<GroupUsedList> gggsss = null;
            if (m_SceneGroupUsedList.TryGetValue(sceneID, out gggsss))
            {
                foreach (GroupUsedList ggg in gggsss)
                    ggg.Dispose();
                gggsss.Clear();

                m_SceneGroupUsedList.Remove(sceneID);
            }

            _engine.RemoveScene(sceneID);
        }

        /// <summary>
        /// 得到由SceneID指定的Scene
        /// </summary>
        /// <param name="SceneID"></param>
        /// <returns></returns>
        public Scene GetSceneFromSceneID(int SceneID)
        {
            return _engine.GetScene(SceneID);
        }

        /// <summary>
        /// 得到目前的Scene 數量
        /// </summary>
        /// <returns></returns>
        public int GetSceneIDCount()
        {
            return _engine.GetNowSceneIDCount();
        }

        public void Dispose()
        {
            I_XNAComponent.ReleaseCheck.DisposeCheck(this);

            //while (_scene.Materials.Count > 0)
            //    _scene.Materials[0].Dispose();
            //  _scene.Dispose();
            //   _core.Dispose();
            _core = null;

            _engine.Dispose();
            _engine = null;

            //_scene = null;


            Dictionary<int, List<GroupUsedList>>.Enumerator itr = m_SceneGroupUsedList.GetEnumerator();
            while (itr.MoveNext())
                foreach (GroupUsedList ggg in itr.Current.Value)
                    ggg.Dispose();
            itr.Dispose();
            m_SceneGroupUsedList.Clear();
            m_SceneGroupUsedList = null;
        }

        /// <summary>
        /// 看目前哪個Scene作用中，Update某個Scene
        /// </summary>
        /// <param name="sceneID"></param>
        public void UpdateScene(int sceneID)
        {
            _engine.UpdateScene(sceneID, I_GetTime.ellipseUpdateSecond);
            //   _engine.Update(sceneID, GetTime.ellipseUpdateSecond);
        }

        //public void Update()
        //{
        //    //Utility.ElapsedTimeRatio
        //    _engine.Update(GetTime.ellipseUpdateSecond);//Utility.ElapsedTimeRatio);//(float)GetTime.ellipseUpdateMillisecond/1000f);//Utility.TargetElapsedTime);/
        //}

        /// <summary>
        /// 呼叫scene的render畫出Debug線
        /// </summary>
        /// <param name="sceneID"></param>
        /// <param name="ViewMatrix"></param>
        /// <param name="ProjectionMatrix"></param>
        public void Render(int sceneID, ref Matrix ViewMatrix, ref Matrix ProjectionMatrix)
        {
            _engine.Draw(sceneID, ref ViewMatrix, ref ProjectionMatrix);
        }

        public Actor PhyXCreatePlaneGround(int sceneID, Vector3 normal, float d, I_CollisionFilter filterMask)
        {
            return PhyXCreatePlaneGround(_engine.GetScene(sceneID), ref normal, d, filterMask);
        }

        static Actor PhyXCreatePlaneGround(Scene _scene, ref Vector3 normal, float d, I_CollisionFilter filterMask)
        {
            PlaneShapeDescription planeShapeDesc = new PlaneShapeDescription();
            planeShapeDesc.Normal = normal;
            planeShapeDesc.Distance = d;

            planeShapeDesc.GroupsMask = GetGroupMask(filterMask);
            ActorDescription actorDesc = new ActorDescription();
            //actorDesc.Flags = ActorFlag.ContactModification;
            actorDesc.Shapes.Add(planeShapeDesc);

            Actor act = CreateActor(_scene, ref actorDesc);

            planeShapeDesc.Dispose();
            planeShapeDesc = null;
            return act;
        }

        /// <summary>
        /// 釋放Actor專用,但一直呼叫這個會造成破碎記憶體
        /// </summary>
        static public void DisposeActor(ref Actor actor)
        {
            //ReleaseCheck.DisposeCheck(actor);

            if (actor != null)
            {
                while (actor.Shapes.Count > 0)
                {
                    actor.Shapes[0].Dispose();
                }
                actor.UserData = null;
                actor.Dispose();
            }
            actor = null;
        }

        static Actor CreateActor(Scene scene, ref ActorDescription actorDesc)
        {
            Actor actor = scene.CreateActor(actorDesc);
            //   ReleaseCheck.AddOtherNew(actor);

            actorDesc.Dispose();
            actorDesc = null;
            return actor;
        }

        /// <summary>
        /// 針對傳入的bounding box建立物理BOX
        /// </summary>
        public Actor PhyXCreateBoxActor(int sceneID, string groupName, BoundingBox boundBox, Vector3 scale,
            float mass, I_CollisionFilter filterMask)
        {
            Actor actor = null;
            actor = TryGetExistActor(sceneID, groupName);

            if (actor != null)
                return actor;

            actor = PhyXCreateBoxActor(_engine.GetScene(sceneID), ref boundBox, ref scale,
            mass, filterMask);

            SetActorToGroup(sceneID, groupName, actor);

            return actor;
        }

        /// <summary>
        /// 根據傳入的Bounding Box建立BOX ACTOR
        /// </summary>
        static Actor PhyXCreateBoxActor(Scene _scene, ref BoundingBox boundBox, ref Vector3 scale,
             float mass, I_CollisionFilter filterMask)
        {
            Vector3[] size = new Vector3[1];
            Vector3[] pos = new Vector3[1];

            size[0] = (boundBox.Max - boundBox.Min);
            pos[0] = (boundBox.Max + boundBox.Min) * 0.5f;

            size[0] *= scale;
            size[0] += Vector3.One * 0.0000001f;//預防有一邊小於0，給他加上一個微小的數。
            if (size[0].X < 0)
                size[0].X *= -1;
            if (size[0].Y < 0)
                size[0].Y *= -1;
            if (size[0].Z < 0)
                size[0].Z *= -1;

            return PhyXCreateBoxActor(_scene, size, pos, 10, filterMask);
        }

        /// <summary>
        /// 根據傳入的Size 建立Box
        /// </summary>
        public Actor PhyXCreateBoxActor(int sceneID, string groupName, Vector3[] size, Vector3[] localPosOffset,
             float mass, I_CollisionFilter filterMask)
        {
            Actor actor = null;
            actor = TryGetExistActor(sceneID, groupName);

            if (actor != null)
                return actor;

            actor = PhyXCreateBoxActor(_engine.GetScene(sceneID), size, localPosOffset, mass, filterMask);

            SetActorToGroup(sceneID, groupName, actor);

            return actor;
        }

        static Actor PhyXCreateBoxActor(Scene _scene, Vector3[] size, Vector3[] localPosOffset,
             float mass, I_CollisionFilter filterMask)
        {
            BoxShapeDescription[] boxShapeDesc = new BoxShapeDescription[size.Length];
            for (int a = 0; a < boxShapeDesc.Length; a++)
            {
                boxShapeDesc[a] = new BoxShapeDescription(size[a].X, size[a].Y, size[a].Z);

                if (localPosOffset == null)
                    boxShapeDesc[a].LocalPosition = Vector3.Zero;
                else
                    boxShapeDesc[a].LocalPosition = localPosOffset[a];

                boxShapeDesc[a].GroupsMask = GetGroupMask(filterMask);
            }

            BodyDescription bodyDesc = new BodyDescription(mass);

            ActorDescription actorDesc = new ActorDescription()
            {
                Name = "BoxShape",//String.Format("Box {0}", x),
                BodyDescription = bodyDesc,

                //GlobalPose = Matrix.CreateTranslation(position),
                //Shapes = { boxShapeDesc },           
            };
            for (int a = 0; a < boxShapeDesc.Length; a++)
                actorDesc.Shapes.Add(boxShapeDesc[a]);

            Actor actor = CreateActor(_scene, ref actorDesc);

            bodyDesc.Dispose();
            bodyDesc = null;
            for (int a = 0; a < boxShapeDesc.Length; a++)
            {
                boxShapeDesc[a].Dispose();
                boxShapeDesc[a] = null;
            }
            //boxShapeDesc.Dispose();
            boxShapeDesc = null;
            //actorDesc.Dispose();
            //actorDesc = null;

            //if (node != null)
            //{
            //    actor.Name = node.NodeName;
            //    actor.UserData = node;
            //}

            return actor;
        }

        public Actor PhyXCreateStaticMesh(int sceneID, string groupName, string ActorName, Vector3[] vertices, uint[] indices, I_CollisionFilter filterMask)
        {
            Actor actor = null;
            actor = TryGetExistActor(sceneID, groupName);

            if (actor != null)
                return actor;

            actor = PhyXCreateStaticMesh(_engine.GetScene(sceneID), _core, ActorName, vertices, indices, GetGroupMask(filterMask));

            SetActorToGroup(sceneID, groupName, actor);

            return actor;
        }

        static Actor PhyXCreateStaticMesh(Scene _scene, Core _core,
            string name, Vector3[] vertices, uint[] indices, GroupsMask groupsMask)
        {
            TriangleMeshDescription triangleMeshDesc = new TriangleMeshDescription();
            triangleMeshDesc.TriangleCount = indices.Length / 3;
            triangleMeshDesc.VertexCount = vertices.Length;

            triangleMeshDesc.AllocateTriangles<int>(triangleMeshDesc.TriangleCount);
            triangleMeshDesc.AllocateVertices<Vector3>(triangleMeshDesc.VertexCount);

            triangleMeshDesc.TriangleStream.SetData(indices);
            triangleMeshDesc.VerticesStream.SetData(vertices);

            MemoryStream s = new MemoryStream();

            Cooking.InitializeCooking();
            Cooking.CookTriangleMesh(triangleMeshDesc, s);
            Cooking.CloseCooking();

            s.Position = 0;
            TriangleMesh triangleMesh = _core.CreateTriangleMesh(s);

            MaterialDescription materialDesc = new MaterialDescription()
            {
                Restitution = 0.5f,
                StaticFriction = 0.1f,
                DynamicFriction = 0.1f,
                //DynamicFrictionV = 0.8f,
                //StaticFrictionV = 1f,
                //DirectionOfAnisotropy = new Vector3(0, 0, 1),
                //Flags = MaterialFlag.Ansiotropic,
            };
            Material material = _scene.CreateMaterial(materialDesc);

            TriangleMeshShapeDescription triangleMeshShapeDesc = new TriangleMeshShapeDescription()
            {
                TriangleMesh = triangleMesh,
                Material = material,
                GroupsMask = groupsMask,
            };
            //triangleMeshShapeDesc.Flags |= ShapeFlag.Visualization;

            ActorDescription actorDesc = new ActorDescription()
            {
                //Name = "StaticMesh",
                Shapes = { triangleMeshShapeDesc }
            };


            if (name != null)
                actorDesc.Name = name;
            else
                actorDesc.Name = "StaticMesh";

            Actor actor = CreateActor(_scene, ref actorDesc);


            triangleMeshDesc.Dispose();
            triangleMeshDesc = null;
            s.Dispose();
            s = null;
            triangleMeshShapeDesc.Dispose();
            triangleMeshShapeDesc = null;
            //actorDesc.Dispose();
            //actorDesc = null;

            if (name != null)
                actor.Name = Utility.GetOppositeFilePath(name);

            return actor;
        }

        static Actor PhyXCreateConvexMesh(Scene _scene, Core _core, Vector3[] vertices, Vector3 position, float mass,
            I_CollisionFilter filterMask)
        {
            // Allocate memory for the points and triangles
            var convexMeshDesc = new ConvexMeshDescription()
            {
                PointCount = vertices.Length
            };
            convexMeshDesc.Flags |= ConvexFlag.ComputeConvex;
            convexMeshDesc.AllocatePoints<Vector3>(vertices.Length);

            convexMeshDesc.PointsStream.SetData(vertices);


            // Cook to memory or to a file
            MemoryStream stream = new MemoryStream();
            //FileStream stream = new FileStream( @"Convex Mesh.cooked", FileMode.CreateNew );

            //    Cooking.InitializeCooking(new ConsoleOutputStream());
            Cooking.InitializeCooking();
            Cooking.CookConvexMesh(convexMeshDesc, stream);
            Cooking.CloseCooking();

            stream.Position = 0;

            ConvexMesh convexMesh = _core.CreateConvexMesh(stream);

            ConvexShapeDescription convexShapeDesc = new ConvexShapeDescription(convexMesh)
            {
                GroupsMask = GetGroupMask(filterMask),
            };

            ActorDescription actorDesc = new ActorDescription()
            {
                Name = "ConvexMesh",
                BodyDescription = new BodyDescription(mass),
                GlobalPose = Matrix.CreateTranslation(position)
            };
            actorDesc.Shapes.Add(convexShapeDesc);

            Actor _torusActor = CreateActor(_scene, ref actorDesc);


            convexMeshDesc.Dispose();
            convexMeshDesc = null;
            stream.Dispose();
            stream = null;
            convexShapeDesc.Dispose();
            convexShapeDesc = null;
            actorDesc.Dispose();
            actorDesc = null;

            //if (node != null)
            //{
            //    _torusActor.Name = node.NodeName;
            //    _torusActor.UserData = node;
            //}


            return _torusActor;
        }

        static public Actor PhyXCreateSphereActor(Scene _scene,
          FrameNode node, float radius, Vector3 position, float mass, I_CollisionFilter filterMask)
        {

            SphereShapeDescription sphereShapeDesc = new SphereShapeDescription(radius);
            sphereShapeDesc.GroupsMask = GetGroupMask(filterMask);

            BodyDescription bodyDesc = new BodyDescription(mass);

            ActorDescription actorDesc = new ActorDescription()
            {
                Name = "SphereShape",//String.Format("Box {0}", x),
                BodyDescription = bodyDesc,
                GlobalPose = Matrix.CreateTranslation(position),
                Shapes = { sphereShapeDesc },
            };

            Actor actor = CreateActor(_scene, ref actorDesc);


            bodyDesc.Dispose();
            bodyDesc = null;
            sphereShapeDesc.Dispose();
            sphereShapeDesc = null;
            //actorDesc.Dispose();
            //actorDesc = null;

            if (node != null)
            {
                actor.Name = node.NodeName;
                actor.UserData = node;
            }
            return actor;
        }


        //public Actor PhyXCreateHeightField(string hmFilename, Material material, GroupsMask groupMask)
        //{
        //    return PhyXCreateHeightField(_scene, _core, hmFilename, material, groupMask);
        //}

        //static private byte? PhyXHeightFieldHoleMaterialIndex;
        //static public Actor PhyXCreateHeightField(Scene scene,Core core, string hmFilename, Material material,
        //    GroupsMask groupMask)
        //{
        //    HighMapEditor.HighMap heightMap = new HighMapEditor.HighMap();
        //    heightMap.LoadHighMap(hmFilename);

        //    //MaterialDescription materialDesc = new MaterialDescription();
        //    //materialDesc.Restitution = heightMap.PhyXRestitution;
        //    //materialDesc.StaticFriction = heightMap.PhyXStaticFriction;
        //    //materialDesc.DynamicFriction = heightMap.PhyXDynamicFriction;
        //    //byte materialIndex = (byte)_scene.CreateMaterial(materialDesc).Index;
        //    //materialDesc.Dispose();
        //    //materialDesc = null;

        //    //建立一下洞的material
        //    if (PhyXHeightFieldHoleMaterialIndex.HasValue == false)
        //    {
        //        MaterialDescription materialDesc = new MaterialDescription();
        //        PhyXHeightFieldHoleMaterialIndex = (byte)scene.CreateMaterial(materialDesc).Index;
        //        materialDesc.Dispose();
        //        materialDesc = null;
        //    }
        //    //建立一下洞的material           

        //    HeightFieldSample[] samples = new HeightFieldSample[heightMap.BloclSizeX * heightMap.BloclSizeZ];
        //    HighMapEditor.HighMap.HighmapData data;
        //    for (int r = 0; r < heightMap.BloclSizeX; r++)
        //    {
        //        for (int c = 0; c < heightMap.BloclSizeZ; c++)
        //        {
        //            //HeightFieldSample sample = new HeightFieldSample();
        //            //sample.Height = (short)h;
        //            //sample.MaterialIndex0 = 0;
        //            //sample.MaterialIndex1 = 1;
        //            //sample.TessellationFlag = 0;

        //            heightMap.GetHimapBlockData(r, c, out data);

        //            if (data.bHasData == false)
        //            {
        //                samples[r * heightMap.BloclSizeZ + c].Height = short.MinValue;
        //                samples[r * heightMap.BloclSizeZ + c].TessellationFlag = 0;
        //                samples[r * heightMap.BloclSizeZ + c].MaterialIndex0 = PhyXHeightFieldHoleMaterialIndex.Value;
        //                samples[r * heightMap.BloclSizeZ + c].MaterialIndex1 = PhyXHeightFieldHoleMaterialIndex.Value;
        //            }
        //            else
        //            {
        //                samples[r * heightMap.BloclSizeZ + c].Height = (short)data.m_HighY;
        //                samples[r * heightMap.BloclSizeZ + c].TessellationFlag = 0;
        //                samples[r * heightMap.BloclSizeZ + c].MaterialIndex0 = (byte)material.Index;//材質一定要設定
        //                samples[r * heightMap.BloclSizeZ + c].MaterialIndex1 = (byte)material.Index;
        //            }
        //        }
        //    }

        //    HeightFieldDescription heightFieldDesc = new HeightFieldDescription()
        //    {
        //        NumberOfRows = heightMap.BloclSizeX,
        //        NumberOfColumns = heightMap.BloclSizeZ,
        //        Samples = samples
        //    };
        //    //heightFieldDesc.SetSamples(samples);

        //  //  heightFieldDesc.VerticalExtent = -1000;
        ////    heightFieldDesc.Thickness

        //    HeightField heightField = core.CreateHeightField(heightFieldDesc);

        //    heightFieldDesc.Dispose();
        //    heightFieldDesc = null;

        //    HeightFieldShapeDescription heightFieldShapeDesc = new HeightFieldShapeDescription()
        //    {
        //        HeightField = heightField,
        //        HoleMaterial = PhyXHeightFieldHoleMaterialIndex.Value,
        //        // The max height of our samples is short.MaxValue and we want it to be 1
        //        //HeightScale = 1.0f / (float)short.MaxValue,
        //        RowScale = heightMap.BlockWidth,
        //        ColumnScale = heightMap.BlockHeight,

        //        GroupsMask = groupMask,
        //    };


        //    //phyX的height map是以以左上角算(0,0)，我的height map左上角是MapMinX, Z。
        //    heightFieldShapeDesc.LocalPosition = new Vector3(heightMap.MapMinX, 0, heightMap.MapMinZ);
        //    //new Vector3(
        //    //    -0.5f * heightMap.BloclSizeX * 1 * heightFieldShapeDesc.RowScale, 
        //    //    0,
        //    //    -0.5f * heightMap.BloclSizeZ * 1 * heightFieldShapeDesc.ColumnScale);

        //    ActorDescription actorDesc = new ActorDescription()
        //    {
        //        GlobalPose = Matrix.CreateTranslation(0, 0, 0),
        //        Shapes = { heightFieldShapeDesc }
        //    };

        //    heightFieldShapeDesc.Dispose();
        //    heightFieldShapeDesc = null;

        //    Actor actor = CreateActor(scene, ref actorDesc);

        //    heightMap.Dispose();
        //    heightMap = null;

        //    actor.Name = Utility.GetOppositeFilePath(hmFilename);
        //    return actor;
        //}

        //public RaycastHit PhyXRaycastClosestShape(ref Vector3 rayPos, ref Vector3 rayDirection,
        //    ShapesType shapType, float maxDistance, FilterMask mask)
        //{
        //    return PhyXRaycastClosestShape(ref rayPos, ref rayDirection, _scene, ShapesType.Static, maxDistance, mask);
        //}


        public RaycastHit PhyXRaycastClosestShape(ref Vector3 rayPos, ref Vector3 rayDirection, int sceneID,
           ShapesType shapType, float maxDistance, I_CollisionFilter mask)
        {
            return PhyXRaycastClosestShape(ref rayPos, ref rayDirection, _engine.GetScene(sceneID),
            shapType, maxDistance, GetGroupMask(mask));
        }

        static RaycastHit PhyXRaycastClosestShape(ref Vector3 rayPos, ref Vector3 rayDirection, Scene scene,
            ShapesType shapType, float maxDistance, GroupsMask GMask)
        {
            if (rayDirection == Vector3.Zero)
                return null;
            StillDesign.PhysX.Ray ray = new StillDesign.PhysX.Ray(rayPos, rayDirection);

            if (float.IsNaN(ray.Direction.X) || float.IsNaN(ray.Direction.Y) || float.IsNaN(ray.Direction.Z))
                return null;

            RaycastHit hit = scene.RaycastClosestShape(ray, shapType, 0xffffffff, maxDistance, 0xffffffff/*RaycastBit.All*/, GMask);
            return hit;
        }

        //static List<Actor> RaycastAllShapesActors = new List<Actor>();
        //static public Actor [] PhyXRaycastAllShapes(ref Vector3 rayPos, ref Vector3 rayDirection,
        //    Scene scene, ShapesType shapType, float maxDistance, FilterMask mask)
        //{
        //    RaycastAllShapesActors.Clear();

        //    if (rayDirection == Vector3.Zero)
        //        return null;
        //    uint hintflag=0;

        //    StillDesign.PhysX.Ray ray = new StillDesign.PhysX.Ray(rayPos, rayDirection);
        //    GroupsMask GMask = GetGroupMask(mask);
        //    scene.RaycastAllShapes(ray, MyRaycastReport, shapType, 0xffffffff, maxDistance,
        //        (uint)RaycastBit.Distance,// | (uint)RaycastBit.Shape,
        //        GMask);

        //    Actor [] aaa = RaycastAllShapesActors.ToArray();
        //    RaycastAllShapesActors.Clear();
        //    return aaa;
        //}

        //static public class MyRaycastReport : UserRaycastReport
        //{
        //    static public override bool OnHit(RaycastHit hits)
        //    {
        //        Actor hitActor = hits.Shape.Actor;
        //        RaycastAllShapesActors.Add(hitActor);
        //        return true;
        //    }
        //}

        //enum FrustumSide { RIGHT , LEFT, BOTTOM, TOP, FRONT, BACK };
        static Plane[] FrustumPlane = new Plane[6];

        public static Plane[] GetFrustumPlanes(ref Matrix VxP)
        {
            //BoundingFrustum bbfff = new BoundingFrustum(VxP);
            //Vector3[] vv = bbfff.GetCorners();
            //Plane[] planesss = new Plane[6];
            //planesss[0] = new Plane(vv[2], vv[1], vv[0]);
            //planesss[1] = new Plane(vv[4], vv[5], vv[6]);
            //planesss[2] = new Plane(vv[0], vv[4], vv[7]);
            //planesss[3] = new Plane(vv[5], vv[1], vv[2]);
            //planesss[4] = new Plane(vv[7], vv[6], vv[2]);
            //planesss[5] = new Plane(vv[0], vv[1], vv[5]);

            //return planesss;

            int RIGHT = 0;// (int)FrustumSide.RIGHT;
            int LEFT = 1;// (int)FrustumSide.LEFT;
            int BOTTOM = 2;// (int)FrustumSide.BOTTOM;
            int TOP = 3;// (int)FrustumSide.TOP;
            int FRONT = 4;// (int)FrustumSide.FRONT;
            int BACK = 5;// (int)FrustumSide.BACK;

            //FrustumPlane[RIGHT] = new Plane(
            //    VxP.M14 - VxP.M11, VxP.M24 - VxP.M21, VxP.M34 - VxP.M31, VxP.M44 - VxP.M41) ;
            //FrustumPlane[LEFT] = new Plane(
            //    VxP.M14 + VxP.M11, VxP.M24 + VxP.M21, VxP.M34 + VxP.M31, VxP.M44 + VxP.M41);
            //FrustumPlane[BOTTOM] = new Plane(
            //    VxP.M14 + VxP.M12, VxP.M24 + VxP.M22, VxP.M34 + VxP.M32, VxP.M44 + VxP.M42);
            //FrustumPlane[TOP] = new Plane(
            //    VxP.M14 - VxP.M12, VxP.M24 - VxP.M22, VxP.M34 - VxP.M32, VxP.M44 - VxP.M42);
            //FrustumPlane[FRONT] = new Plane(
            //    VxP.M14 + VxP.M13, VxP.M24 + VxP.M23, VxP.M34 + VxP.M33, VxP.M44 + VxP.M43);
            //FrustumPlane[BACK] = new Plane(
            //    VxP.M14 - VxP.M13, VxP.M24 - VxP.M23, VxP.M34 - VxP.M33, VxP.M44 - VxP.M43);

            //FrustumPlane[RIGHT].Normalize();
            //FrustumPlane[LEFT].Normalize();
            //FrustumPlane[BOTTOM].Normalize();
            //FrustumPlane[TOP].Normalize();
            //FrustumPlane[FRONT].Normalize();
            //FrustumPlane[BACK].Normalize();

            //FrustumPlane[RIGHT].Normal *= -1f;
            //FrustumPlane[LEFT].Normal *= -1f;
            //FrustumPlane[BOTTOM].Normal *= -1f;
            //FrustumPlane[TOP].Normal *= -1f;
            //FrustumPlane[FRONT].Normal *= -1f;
            //FrustumPlane[BACK].Normal *= -1f;

            //FrustumPlane[RIGHT].D *= -1f;
            //FrustumPlane[LEFT].D *= -1f;
            //FrustumPlane[BOTTOM].D *= -1f;
            //FrustumPlane[TOP].D *= -1f;
            //FrustumPlane[FRONT].D *= -1f;
            //FrustumPlane[BACK].D *= -1f;


            //right clipping plane        
            FrustumPlane[RIGHT].Normal.X = VxP.M14 - VxP.M11;
            FrustumPlane[RIGHT].Normal.Y = VxP.M24 - VxP.M21;
            FrustumPlane[RIGHT].Normal.Z = VxP.M34 - VxP.M31;
            FrustumPlane[RIGHT].D = VxP.M44 - VxP.M41;
            FrustumPlane[RIGHT].Normal *= -1f;
            FrustumPlane[RIGHT].D *= -1f;

            //normalize
            FrustumPlane[RIGHT].Normalize();

            //left clipping plane
            FrustumPlane[LEFT].Normal.X = VxP.M14 + VxP.M11;
            FrustumPlane[LEFT].Normal.Y = VxP.M24 + VxP.M21;
            FrustumPlane[LEFT].Normal.Z = VxP.M34 + VxP.M31;
            FrustumPlane[LEFT].D = VxP.M44 + VxP.M41;
            FrustumPlane[LEFT].Normal *= -1f;
            FrustumPlane[LEFT].D *= -1f;

            //normalize
            FrustumPlane[LEFT].Normalize();

            //bottom clipping plane
            FrustumPlane[BOTTOM].Normal.X = VxP.M14 + VxP.M12;
            FrustumPlane[BOTTOM].Normal.Y = VxP.M24 + VxP.M22;
            FrustumPlane[BOTTOM].Normal.Z = VxP.M34 + VxP.M32;
            FrustumPlane[BOTTOM].D = VxP.M44 + VxP.M42;
            FrustumPlane[BOTTOM].Normal *= -1f;
            FrustumPlane[BOTTOM].D *= -1f;

            //normalize
            FrustumPlane[BOTTOM].Normalize();

            //top clipping plane 
            FrustumPlane[TOP].Normal.X = VxP.M14 - VxP.M12;
            FrustumPlane[TOP].Normal.Y = VxP.M24 - VxP.M22;
            FrustumPlane[TOP].Normal.Z = VxP.M34 - VxP.M32;
            FrustumPlane[TOP].D = VxP.M44 - VxP.M42;
            FrustumPlane[TOP].Normal *= -1f;
            FrustumPlane[TOP].D *= -1f;

            //normalize
            FrustumPlane[TOP].Normalize();
            //near clipping plane
            FrustumPlane[FRONT].Normal.X = VxP.M14 + VxP.M13;
            FrustumPlane[FRONT].Normal.Y = VxP.M24 + VxP.M23;
            FrustumPlane[FRONT].Normal.Z = VxP.M34 + VxP.M33;
            FrustumPlane[FRONT].D = VxP.M44 + VxP.M43;
            FrustumPlane[FRONT].Normal *= -1f;
            FrustumPlane[FRONT].D *= -1f;

            //normalize
            FrustumPlane[FRONT].Normalize();

            //far clipping plane
            FrustumPlane[BACK].Normal.X = VxP.M14 - VxP.M13;
            FrustumPlane[BACK].Normal.Y = VxP.M24 - VxP.M23;
            FrustumPlane[BACK].Normal.Z = VxP.M34 - VxP.M33;
            FrustumPlane[BACK].D = VxP.M44 - VxP.M43;
            FrustumPlane[BACK].Normal *= -1f;
            FrustumPlane[BACK].D *= -1f;

            //normalize
            FrustumPlane[BACK].Normalize();

            return FrustumPlane;
        }

        static public Shape[] ConsiderViewFrustumCulling(Scene scene, ShapesType shapType,
             I_CollisionFilter mask,
             ref Matrix VxP)
        {
            return scene.CullShapes(GetFrustumPlanes(ref VxP), ShapesType.All, 0xffffffff, GetGroupMask(mask));
        }

        /// <summary>
        /// 設定是否啟動PhyX內部產生debug line的參數控制。
        /// </summary>
        //public void PhyX_EnableDebugLine(bool bbb)
        //{
        //    _engine.SetDrawDebugLine(bbb);
        //}

        public void PhyX_GetDebugLineOnce()
        {
            _engine.GetDebugLineOnce();
        }

        /// <summary>
        /// 設定是否啟動內部將phyX的debug line繪出的函式，若為false則會跳過該函式。
        /// </summary>
        public void PhyX_SetRenderDebugLine(bool bbb)
        {
            _engine.SetRenderDebugLine(bbb);
        }

        public void PhyX_SetEnableDebugLine(bool bbb)
        {
            _engine.SetEnableDebugLine(bbb);
        }

        /// <summary>
        /// 取得內部phyX產生的debug line，當想要自己畫出phyX除錯線條時可以用。
        /// </summary>
        public DebugLine[] PhyX_GetDebugLine(int sceneID)
        {
            return _engine.GetDebugLine(sceneID);
        }

        public void PhyX_RenderDebugLine(int sceneID, ref Matrix View, ref Matrix Proj)
        {
            _engine.Draw(sceneID, ref View, ref Proj);
        }

        //public void PhyX_SetGravity(Vector3 gravity)
        //{
        //    _engine.SetGravity(ref gravity);
        //}



    }
}
