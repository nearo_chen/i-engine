﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using StillDesign.PhysX;
using I_XNAUtility;

namespace I_Component
{
    class Engine
    {
        static int SceneIDCount = 0;
        Dictionary<int, Scene> m_Scenes = new Dictionary<int, Scene>(64);
        BasicEffect _visualizationEffect;
        VertexDeclaration DebugVertexDesc;

        GraphicsDevice m_Device;
        UserOutput output;

        public Engine(GraphicsDevice device)
        {
            m_Device = device;
        }

        public void Initalize()
        {
            if (m_Device != null)
            {
                DebugVertexDesc = new VertexDeclaration(m_Device, VertexPositionColor.VertexElements);

                _visualizationEffect = new BasicEffect(m_Device, null)
                {
                    VertexColorEnabled = true
                };
            }

            CoreDescription coreDesc = new CoreDescription();
            output = new UserOutput();
            this.Core = new Core(coreDesc, output);
            coreDesc.Dispose();

            var core = this.Core;

            SetDrawDebugLine(I_Utility.IfDrawDebug);

            //SceneDescription sceneDesc = new SceneDescription()
            //{
            //    SimulationType = SimulationType.Software,
            //    //SimulationType = SimulationType.Hardware,
            //    Gravity = new Vector3(0.0f, -9.81f * 100, 0.0f),
            //    GroundPlaneEnabled = false,
            //};

            //this.Scene = core.CreateScene(sceneDesc);
            //sceneDesc.Dispose();

            //HardwareVersion ver = Core.HardwareVersion;
            //SimulationType simType = this.Scene.SimulationType;

            //// Connect to the remote debugger if its there
            //core.Foundation.RemoteDebugger.Connect("localhost");
        }

        public int AddScene(Vector3 gravity, SimulationType simType)
        {
            SceneDescription sceneDesc = new SceneDescription()
            {
                SimulationType = simType,//SimulationType.Software,
                //SimulationType = SimulationType.Hardware,
                Gravity =gravity,// new Vector3(0.0f, -9.81f * 100, 0.0f),
                GroundPlaneEnabled = false,
            };

            Scene scene = null;
            int FailedCount = 0;
            while (FailedCount < 3)
            {
                try
                {
                    scene = Core.CreateScene(sceneDesc);
                    break;
                }
                catch
                {
                    System.Threading.Thread.Sleep(1000);
                    FailedCount++;
                }
            }

            m_Scenes.Add(SceneIDCount, scene);

            sceneDesc.Dispose();

            //設定collision filter
            scene.SetFilterOperations((FilterOperation)1, (FilterOperation)1, (FilterOperation)0);//(or, or, and)
            scene.SetFilterBoolean(true);
            GroupsMask zeroMask = new GroupsMask(0, 0, 0, 0);
            scene.SetFilterConstant0(zeroMask);
            scene.SetFilterConstant1(zeroMask);

            SceneIDCount++;
            return SceneIDCount - 1;
        }

        //public void SetGravity(ref Vector3 gravity)
        //{
        //    Scene.Gravity = gravity;
        //}

        /// <summary>
        /// 移除scene
        /// </summary>
        /// <param name="sceneID"></param>
        public void RemoveScene(int sceneID)
        {
            m_Scenes[sceneID].Dispose();
            m_Scenes.Remove(sceneID);
        }

        public void Dispose()
        {
            //output.Dispose();
            //output = null; 

            _visualizationEffect.Dispose();
            _visualizationEffect = null;
            DebugVertexDesc.Dispose();
            DebugVertexDesc = null;

            Dictionary<int, Scene>.Enumerator itr = m_Scenes.GetEnumerator();
            while(itr.MoveNext())
                itr.Current.Value.Dispose();
            itr.Dispose();
        
            m_Scenes.Clear();
            m_Scenes = null;

            Core.Dispose();
            Core = null;

            //Scene.Dispose();
            //Scene = null;

            output.Dispose();
            output = null;
        }

        public Scene GetScene(int sceneID)
        {
            return m_Scenes[sceneID];
        }

        public int GetNowSceneIDCount()
        {
            return SceneIDCount;
        }

        public void UpdateScene(int sceneID, float ElapsedTime)
        {
            m_Scenes[sceneID].Simulate((float)ElapsedTime);
            m_Scenes[sceneID].FlushStream();
            m_Scenes[sceneID].FetchResults(SimulationStatus.RigidBodyFinished, true);
        }

        //float m_EllapseTime = 0;
        //public void Update(float ElapsedTime)
        //{
        //    this.Scene.Simulate((float)ElapsedTime);
        //    //this.Scene.Simulate(1.0f / 60.0f);
        //    this.Scene.FlushStream();
        //    this.Scene.FetchResults(SimulationStatus.RigidBodyFinished, true);
        //}

        bool bDrawDebugLine = true;
        public void SetRenderDebugLine(bool bbb)
        {
            bDrawDebugLine = bbb;
        }

        /// <summary>
        /// 設定是否啟動內部將phyX的debug line繪出的函式，若為false則會跳過該函式。
        /// </summary>
        public void SetEnableDebugLine(bool bbb)
        {
            SetDrawDebugLine(bbb);
        }

        public DebugLine[] GetDebugLine(int sceneID)
        {
            try
            {
                DebugRenderable data = m_Scenes[sceneID].GetDebugRenderable();
                return data.GetDebugLines();
            }
            catch (Exception e)
            {
            }
            return null;
        }

        int countDebugLineOnce = 0;
        bool bGetDebugLineOnce = false;
        public void GetDebugLineOnce()
        {
            countDebugLineOnce = 0;
            bGetDebugLineOnce = true;
        }

        DebugRenderable DebugRenderableData = null;
        VertexPositionColor[] LineVertices;
        public void Draw(int sceneID, ref Matrix view, ref Matrix projection)
        {
            if (m_Device == null)
                return;

            if (bDrawDebugLine == false)
                return;

            bool refreshData = false;
            if (bGetDebugLineOnce)
            {
                refreshData = true;
                DebugRenderableData = null;
                try
                {
                    DebugRenderableData = m_Scenes[sceneID].GetDebugRenderable();
                }
                catch (Exception e)
                {
                }

                countDebugLineOnce++;
                //SetDrawDebugLine(false);
            }

            if (countDebugLineOnce > 10)
            {
                bGetDebugLineOnce = false;
            }

            DebugRenderable data = DebugRenderableData;
            if (data == null)
                return;

            //   this.Device.Clear(Color.LightBlue);

            m_Device.VertexDeclaration = DebugVertexDesc;

            _visualizationEffect.World = Matrix.Identity;
            _visualizationEffect.View = view;
            _visualizationEffect.Projection = projection;

            _visualizationEffect.Begin();

            foreach (EffectPass pass in _visualizationEffect.CurrentTechnique.Passes)
            {
                pass.Begin();

                if (data.PointCount > 0)
                {
                    DebugPoint[] points = data.GetDebugPoints();

                    m_Device.DrawUserPrimitives<DebugPoint>(PrimitiveType.PointList, points, 0, points.Length);
                }

                if (data.LineCount > 0)
                {
                    DebugLine[] lines = data.GetDebugLines();

                    if (refreshData)
                    {
                        if (LineVertices == null || LineVertices.Length != data.LineCount * 2)
                            LineVertices = new VertexPositionColor[data.LineCount * 2];

                        for (int x = 0; x < data.LineCount; x++)
                        {
                            DebugLine line = lines[x];

                            LineVertices[x * 2 + 0] = new VertexPositionColor(line.Point0, Int32ToColor(line.Color));
                            LineVertices[x * 2 + 1] = new VertexPositionColor(line.Point1, Int32ToColor(line.Color));
                            LineVertices[x * 2 + 0].Color.A = 255;
                            LineVertices[x * 2 + 1].Color.A = 255;
                        }
                    }


                    m_Device.DrawUserPrimitives<VertexPositionColor>(
                        PrimitiveType.LineList, LineVertices, 0, lines.Length);
                }

                if (data.TriangleCount > 0)
                {
                    DebugTriangle[] triangles = data.GetDebugTriangles();

                    VertexPositionColor[] vertices = new VertexPositionColor[data.TriangleCount * 3];
                    for (int x = 0; x < data.TriangleCount; x++)
                    {
                        DebugTriangle triangle = triangles[x];

                        vertices[x * 3 + 0] = new VertexPositionColor(triangle.Point0, Int32ToColor(triangle.Color));
                        vertices[x * 3 + 1] = new VertexPositionColor(triangle.Point1, Int32ToColor(triangle.Color));
                        vertices[x * 3 + 2] = new VertexPositionColor(triangle.Point2, Int32ToColor(triangle.Color));
                        vertices[x * 3 + 0].Color.A = 255;
                        vertices[x * 3 + 1].Color.A = 255;
                        vertices[x * 3 + 2].Color.A = 255;
                    }

                    m_Device.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.TriangleList, vertices, 0, triangles.Length);
                }

                pass.End();
            }

            _visualizationEffect.End();

            m_Device.VertexDeclaration = null;
        }

        #region Properties
        //public Game Game
        //{
        //    get;
        //    private set;
        //}
        //public Camera Camera
        //{
        //    get;
        //    private set;
        //}

        public Core Core
        {
            get;
            private set;
        }
        //public Scene Scene
        //{
        //    get;
        //    private set;
        //}

        //public GraphicsDeviceManager DeviceManager
        //{
        //    get;
        //    private set;
        //}
        //public GraphicsDevice Device
        //{
        //    get
        //    {
        //        return m_Device;
        //    }
        //}


        #endregion


        public static Color Int32ToColor(int color)
        {
            byte a = (byte)((color & 0xFF000000) >> 24);
            byte r = (byte)((color & 0x00FF0000) >> 16);
            byte g = (byte)((color & 0x0000FF00) >> 8);
            byte b = (byte)((color & 0x000000FF) >> 0);

            return new Color(r, g, b, a);
        }
        public static int ColorToArgb(Color color)
        {
            int a = (int)(color.A);
            int r = (int)(color.R);
            int g = (int)(color.G);
            int b = (int)(color.B);

            return (a << 24) | (r << 16) | (g << 8) | (b << 0);
        }

        void SetDrawDebugLine(bool bbb)
        {
            Core.SetParameter(PhysicsParameter.VisualizationScale, 1.0f);
            Core.SetParameter(PhysicsParameter.VisualizeCollisionShapes, bbb);
            Core.SetParameter(PhysicsParameter.VisualizeClothMesh, bbb);
            Core.SetParameter(PhysicsParameter.VisualizeJointLocalAxes, bbb);
            Core.SetParameter(PhysicsParameter.VisualizeJointLimits, bbb);
            Core.SetParameter(PhysicsParameter.VisualizeFluidPosition, bbb);
            Core.SetParameter(PhysicsParameter.VisualizeFluidEmitters, bbb); // Slows down rendering a bit to much
            Core.SetParameter(PhysicsParameter.VisualizeForceFields, bbb);
            Core.SetParameter(PhysicsParameter.VisualizeSoftBodyMesh, bbb);
            //Core.SetParameter(PhysicsParameter.VisualizeBodyAxes, 100);
            Core.SetParameter(PhysicsParameter.VisualizeBodyLinearVelocity, bbb);
        }


        public class UserOutput : UserOutputStream
        {
            public override void Print(string message)
            {
                Console.WriteLine("PhysX: " + message);
            }
            public override AssertResponse ReportAssertionViolation(string message, string file, int lineNumber)
            {
                Console.WriteLine("PhysX: " + message);

                return AssertResponse.Continue;
            }
            public override void ReportError(ErrorCode errorCode, string message, string file, int lineNumber)
            {
                Console.WriteLine("PhysX: " + message);
            }
        }
    }
}
