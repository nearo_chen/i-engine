﻿using System;
using System.Collections.Generic;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

using I_XNAUtility;
using StillDesign.PhysX;
//using HighMapEditor;


using System.ComponentModel;


namespace I_Component
{
    public partial class I_PhysX : System.IDisposable
    {
        /// <summary>
        /// 用來存放scene底下所生出來的Actor，生出來就存住不要釋放，可提供後續同樣關卡重複使用。
        /// key: sceneID, value: GroupUsedList
        /// </summary>
        Dictionary<int, List<GroupUsedList>> m_SceneGroupUsedList;

        Actor TryGetExistActor(int sceneID, string groupName)
        {
            List<GroupUsedList> groups;
            if (m_SceneGroupUsedList.TryGetValue(sceneID, out groups) == false)
            {
                groups = new List<GroupUsedList>(32);
                m_SceneGroupUsedList.Add(sceneID, groups);
            }

            GroupUsedList nowGroup=null;
            List<GroupUsedList>.Enumerator groupsItr = groups.GetEnumerator();
            while(groupsItr.MoveNext())
            {
                if (groupsItr.Current.GroupName == groupName)
                {
                    nowGroup = groupsItr.Current;
                    break;
                }
            }

            if (nowGroup == null)
            {
                nowGroup = new GroupUsedList(groupName);
                groups.Add(nowGroup);
            }

            return nowGroup.TryGetActor();
        }

        void SetActorToGroup(int sceneID, string groupName, Actor actor)
        {
            List<GroupUsedList> groups;
            if (m_SceneGroupUsedList.TryGetValue(sceneID, out groups) == false)
            {
                groups = new List<GroupUsedList>(32);
                m_SceneGroupUsedList.Add(sceneID, groups);
            }

            GroupUsedList nowGroup = null;
            List<GroupUsedList>.Enumerator groupsItr = groups.GetEnumerator();
            while (groupsItr.MoveNext())
            {
                if (groupsItr.Current.GroupName == groupName)
                {
                    nowGroup = groupsItr.Current;
                    break;
                }
            }
            groupsItr.Dispose();

            nowGroup.AddActor(actor);
        }

        public void ResetSceneAllGroup(int sceneID)
        {
            List<GroupUsedList> groups;
            m_SceneGroupUsedList.TryGetValue(sceneID, out groups);

            List<GroupUsedList>.Enumerator groupsItr = groups.GetEnumerator();
            while (groupsItr.MoveNext())
            {
                groupsItr.Current.ResetActorUnUsed();
            }
            groupsItr.Dispose();
        }

        public int GetGroupNumber(int sceneID)
        {
             List<GroupUsedList> groups;
             if (m_SceneGroupUsedList.TryGetValue(sceneID, out groups) == false)
                 return 0;
             else
                 return groups.Count;
        }

        public int GetActorTotalNumber(int sceneID)
        {
            List<GroupUsedList> groups;
            if (m_SceneGroupUsedList.TryGetValue(sceneID, out groups) == false)
                return 0;

            int total = 0;
            List<GroupUsedList>.Enumerator groupsItr = groups.GetEnumerator();
            while (groupsItr.MoveNext())
            {
                total += groupsItr.Current.GetActorNumber();
            }

            return total;
        }


        /// <summary>
        /// 管理一個群組，
        /// 群組內的actor重複使用管理
        /// </summary>
        class GroupUsedList : System.IDisposable
        {
            class USED : System.IDisposable
            {
                public USED(){ bUsed = false; actor = null; }
                public bool bUsed;
                public Actor actor;
                public void Dispose() { actor = null; }               
            }

            List<USED> m_UsedList;
            public string GroupName;

            public GroupUsedList(string groupName)
            {
                GroupName = groupName;
                m_UsedList = new List<USED>(256);
            }

            public Actor TryGetActor()
            {         
                List<USED>.Enumerator itr = m_UsedList.GetEnumerator();
                while (itr.MoveNext())
                {
                    if (itr.Current.bUsed == false)
                        return itr.Current.actor;
                }
                itr.Dispose();

                return null;
            }

            public void AddActor(Actor actor)
            {
                USED used = new USED();
                used.actor = actor;
                used.bUsed = true;
                m_UsedList.Add(used);
            }

            public int GetActorNumber()
            {
                return m_UsedList.Count;
            }

            public void ResetActorUnUsed()
            {
                List<USED>.Enumerator itr = m_UsedList.GetEnumerator();
                while (itr.MoveNext())
                {
                    itr.Current.bUsed = false;   
                }
                itr.Dispose();
            }

            public void Dispose()
            {
                List<USED>.Enumerator itr = m_UsedList.GetEnumerator();
                while (itr.MoveNext())
                    itr.Current.Dispose();
                itr.Dispose();

                m_UsedList.Clear();
                m_UsedList = null;
            }
        }
        
    }
}
