﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
//using System.Xml.Serialization;
using System.IO;
using I_XNAComponent;
using I_XNAUtility;
using MeshDataDefine;

namespace I_Component
{
    public partial class I_SpecialMeshData
    {
        [Serializable]
        public class I_CameraData
        {
          
            public I_CameraData() { }
            public I_CameraData(//int rtW, int rtH, 
                string name, float fovDegree, float near, float far,
                float dofFocalDistance, float dofFocalRange, float dofDecayRange, float dofIntensity
            
                )
            {
                //RTWidth = rtW; RTHeight = rtH;
                Name = name;
                FovDegree = fovDegree; Near = near; Far = far;
                RTMat.m_Quaternion = Quaternion.Identity;
                DOFFocalDistance = dofFocalDistance;
                DOFFocalRange = dofFocalRange;
                DOFDecayRange = dofDecayRange;
                DOFIntensity = dofIntensity;
                //IfViewCulling = bIfViewCulling;
            }

            public string Name;
            public RTMatrix RTMat;
            public float FovDegree;
            public float Near;
            public float Far;

            public string[] RenderLoaders;//此camera要畫出的loader
            public CameraPostProcessing[] CameraPostProcessing;

            public float DOFFocalDistance; //焦距的距離
            public float DOFFocalRange; //焦距的範圍
            public float DOFDecayRange; //超過範圍後多遠最模糊
            public float DOFIntensity; //DOFIntensity 0表示不做DOF

            //public int RTWidth;//此camera render target的寬高
            //public int RTHeight;

            //camera Depth of View 景深模糊的參數
            //camera mipmap level 調整此camera的mipmap程度

            public int PSSMLevelAmount = 2;
            public float PSSMSplitRatioLambda = 0.7f;
            public float PSSM0Bias = 0.001f;
            public float PSSM1Bias = 0.001f;
            public float PSSM2Bias = 0.001f;
            public float PSSM3Bias = 0.001f;

            public float SSAOSampleRadius = 0;
            public float SSAOSampleTime = 8;
            public float SSAOSampleLOD = 2;
            public float SSAODistanceScale = 1;
            public float SSAOFadeOutDistance = 10;

            public bool IfMainCamera = false;
            public bool IfViewCulling = false;
        }

        /// <summary>
        /// camera上的post processing效果
        /// </summary>         
        [Serializable]
        public enum CameraPostProcessing
        {
            //DeferredProcessing,
            HDR,
            GodsRay,
            PointLight,
            Shadow,
        }

    }
}
