﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        /// <summary>
        /// 傳入screenWidth，根據目前設定的螢幕長寬比m_ScreenRatio計算出正確的高度
        /// </summary>    
        protected void GetOutputWH(int screenWidth, out int w, out int h)
        {
            w = screenWidth;// m_MainSceneControl.Width;
            h = 0;
            if (m_ScreenRatio == ScreenRatio.ratio4x3)
                h = (int)(screenWidth * 3f / 4f);
            else if (m_ScreenRatio == ScreenRatio.ratio16x9)
                h = (int)(screenWidth * 9f / 16f);
        }

        /// <summary>
        /// 目前一個Scene只有一個Camera所以此函式會回傳流程所繪出的Texture，
        /// 畫出sceneID主要的畫圖函式，傳入screenWidth表示目前顯示畫面的寬高，
        /// CulledNodesCount回傳此scene被卡掉的node數量
        /// </summary>
        protected Texture2D MainRender(out int CulledNodesCount, int sceneID, int renderWidth, int renderHeight)
        {
            //畫圖之前先對scene裡面所有的Camera都做view Culling，
            //先不考慮2個Camera內有同樣Loader的情況。
            CulledNodesCount = 0;
            I_Camera[] cameras = GetScene(sceneID).GetCameraAsset();
            foreach (I_Camera ccc in cameras)
            {
                //目前一個scene只有一個camera，一定會是MainCamera
                //目前只針對Main Camera做Frustum culling，
                if (//ccc.IfMainCamera ||
                    ccc.IfViewCulling)
                    CulledNodesCount += GetScene(sceneID).ProcessCameraFrustumCulling(ccc, renderWidth, renderHeight);
                else
                    ccc.SetAllCameraNodeBeCulled(false);
            }

            DeferredShading_Begin();
            DeferredRenderCameraScreen_MeshNode(sceneID, cameras, false);
            DeferredRenderCameraScreen_MeshNode(sceneID, cameras, true);
            DeferredRenderCameraScreen_SkinningMeshNode(sceneID, cameras, false);
            DeferredRenderCameraScreen_SkinningMeshNode(sceneID, cameras, true);
            DeferredShading_End();

            DrawLightsMap(sceneID);

            DrawEmmisiveMap();

            CombineBuffers(sceneID);
            Texture2D finalTex = GetTexture_BaseRT();//GetTexture_ColorRT();          

            //width, height為目前view port與gbuffer的WH
            //int width = 0;
            //int height = 0;
            //GetOutputWH(screenWidth, out width, out height);

            //goto PIPLINE_END;

            //Render alpha blending...設定m_ColorRT並將資料回存並使用Simple shader方法繼續畫圖。
            RestoreBuffersBaseRT(true, finalTex);//將depth Buffer寫回去

            //使用原始方法畫圖，一併畫出不跟光計算還有alpha blending。
            #region 畫 不跟光計算模型，使用簡易shader，通常是介面UI，無深度問題。
            SetCommonRenderState(m_GraphicDevice);
            RenderCameraScreen_MeshNode(sceneID, cameras, false, false);
            //RenderCameraScreen_MeshNode(sceneID, null, true, false);//不跟光計算沒必要畫出tangent mesh
            RenderCameraScreen_SkinningMeshNode(sceneID, cameras, false, false);

            RenderCamera_GrassNode(sceneID, cameras);
            //RenderCameraScreen_SkinningMeshNode(sceneID, null, true, false);//不跟光計算沒必要畫出tangent mesh
            #endregion

            //天空的物件必須選擇不畫深度，以及不打光，才會在上面以不打光方式畫出，之後在馬上畫lens flare才會有效
            RenderLensFlare(sceneID, cameras);

            #region 畫 半透明，半透明可畫tangent mesh，會有簡單的平行光計算，和有環境(折射移到後面畫)霧，半透明無深度問題。
            SetAlphaBlendRenderState(m_GraphicDevice);
            RenderCameraScreen_MeshNode(sceneID, cameras, false, true);
            RenderCameraScreen_MeshNode(sceneID, cameras, true, true);
            RenderCameraScreen_SkinningMeshNode(sceneID, cameras, false, true);
            RenderCameraScreen_SkinningMeshNode(sceneID, cameras, true, true);

            //畫particle先塞這邊
            UserRenderAlphaBlending();

            //畫出camera的loader裡面的particle之類的
            RenderCamera_LoaderEffects(sceneID, cameras);

            #endregion


            finalTex = GetTexture_BaseRT();//GetTexture_ColorRT();

            #region 獨立繪製refract map
            DrawRafractMap(sceneID, cameras);
            //畫出折射MAP，可利用simpleEffect.fx 裡面原有畫出narmal map的shader，去根據normal專門畫出折射程度的MAP
            //    effect Render manager裡面新增一個technique專門畫出折射，畫出骨架動畫折射目前不需要
            #endregion

            #region 場景畫上emmisive map
            //Blend Emmisive Map上去           


            BlendTextureToScene(GetTexture_EmmisiveRT(), finalTex, renderWidth, renderHeight, RenderRT.BaseRT);
            finalTex = GetTexture_BaseRT();//GetTexture_ColorRT();
            #endregion

            #region 場景畫折射上去
            RenderWithRefractDistortMap(finalTex, renderWidth, renderHeight, RenderRT.BaseRT);
            #endregion

            #region 折射的最後，將剛剛設定折射，卻沒畫出的模型，補畫出來
            finalTex = GetTexture_BaseRT();//GetTexture_ColorRT();
            RestoreBuffersBaseRT(false, finalTex);

            SetAlphaBlendRenderState(m_GraphicDevice);
            RenderCameraScreen_MeshNode_RefractOnly(sceneID, cameras, false);
            RenderCameraScreen_MeshNode_RefractOnly(sceneID, cameras, true);
            #endregion

            #region 畫除錯線，疊在最上層
            UserRenderTopMost();
            #endregion

            finalTex = GetTexture_BaseRT();

            #region 於最後階段，將剛剛畫折射並且為半透明的模型，將深度畫上depth map，此後depth map為有半透明物件的深度，使用畫shadow map深度圖的shader畫於其上
            //ContinueDrawDepthMap();
            #endregion

            #region MainCamera才會有的功能
            #region 拿Other map 來做 PSSM & SSAO 一起處理
            //處理PSSM深度圖
            Texture2D PSSMOcclusion = null;
            if (PSSMProcessViewsFrustum(sceneID, m_GBufferWidth, m_GBufferHeight))
            {
                PSSMRenderDepthMap(sceneID);
                DrawPSSMShadowOcclusionToOtherMap(sceneID);
                //BlurOtherMapConsiderNormal(1);

                //移到下面與AO一起計算Blur                    
                PSSMOcclusion = GetTexture_OtherMap();
                //BlendAOToScene(PSSMOcclusion, finalTex, width, height, RenderRT.ColorRT);
                //finalTex = GetTexture_ColorRT();
            }

            if (DrawSSAOToOtherMap(sceneID, PSSMOcclusion))
            {
                BlurOtherMapConsiderNormal(1);

                // m_MyBloom.ImportTextureToRenderTarget1(GetTexture_OtherMap());
                // m_MyBloom.RenderBothCaussianBlur();
                //Texture2D blur = m_MyBloom.GetTexture_RenderTarget1();

                BlendAOToScene(GetTexture_OtherMap(), finalTex, renderWidth, renderHeight, RenderRT.BaseRT);
                finalTex = GetTexture_BaseRT();//GetTexture_ColorRT();
            }
            else if (PSSMOcclusion != null)
            {
                BlurOtherMapConsiderNormal(1);
                BlendAOToScene(GetTexture_OtherMap(), finalTex, renderWidth, renderHeight, RenderRT.BaseRT);
                finalTex = GetTexture_BaseRT(); //GetTexture_ColorRT();
            }
            #endregion

            if (DrawDepthOfFieldMap(sceneID, finalTex, RenderRT.BaseRT))
                finalTex = GetTexture_BaseRT(); //GetTexture_ColorRT();

PIPLINE_END:
            //m_SpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None);
            //m_SpriteBatch.Draw(finalTex, new Rectangle(0, 0, renderWidth, renderHeight), Color.White);
            //m_SpriteBatch.End();
            #endregion


            return finalTex;
        }

        /// <summary>
        /// 將scene list畫完並輸出finalRT的texture，
        /// 傳入被卡掉的所有node，還有畫出的螢幕寬度範圍
        /// </summary>
        protected Texture2D MainRenderList(out int CulledNodesCount, int screenWidth)
        {
            int width = 0;
            int height = 0;
            GetOutputWH(screenWidth, out width, out height);

            CulledNodesCount = 0;
            int[] scenesID = GetScenesIDList();
            for (int a = 0; a < scenesID.Length; a++)
            {
                Texture2D newTex = MainRender(out CulledNodesCount, scenesID[a], width, height);

                //先抓出底圖
                Texture2D oldTex = null;
                if (a != 0)
                    oldTex = m_FinalRT.GetTexture();

                m_GraphicDevice.SetRenderTarget(0, m_FinalRT);

                //底圖再畫上新圖疊上去
                m_SpriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Immediate, SaveStateMode.None);
                if (oldTex != null)
                    m_SpriteBatch.Draw(oldTex, new Rectangle(0, 0, width, height), Color.White);
                m_SpriteBatch.Draw(newTex, new Rectangle(0, 0, width, height), Color.White);
                m_SpriteBatch.End();

                m_GraphicDevice.SetRenderTarget(0, null);
            }

            return m_FinalRT.GetTexture();
        }

        /// <summary>
        /// 畫lens Flare在這之前深度緩衝必需回存正確
        /// </summary>
        protected void RenderLensFlare(int sceneID, I_Camera [] cameras)
        {
            I_SceneManager scene = GetScene(sceneID);
            for (int a = 0; a < cameras.Length; a++)
            {
                I_Camera camera = cameras[a];
                //camera = scene.GetCamera(ref camerasName[a]);
                if (camera == null)
                    continue;

                Matrix projectionMat, cameraMat, invCameraMat;
                camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
                camera.GetCameraMatrix(out cameraMat, out invCameraMat);
                Vector3 cameraPosition = cameraMat.Translation;
                Matrix viewMatrix = invCameraMat;
                Matrix invViewMatrix = cameraMat;

                Vector3 LightDirection = Vector3.Zero;
                Vector3 LightAmbient = Vector3.Zero;
                Vector3 LightDiffuse = Vector3.Zero;
                Vector3 LightSpecular = Vector3.Zero;
                scene.GetDirectionalLight(ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

                m_LensFlareComponent.LightDirection = LightDirection;
                m_LensFlareComponent.View = viewMatrix;
                m_LensFlareComponent.Projection = projectionMat;
                //m_LensFlareComponent.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(60), m_GraphicDevice.Viewport.AspectRatio, 0.1f, 10000000f);

                SetAlphaBlendRenderState(m_GraphicDevice);
                m_LensFlareComponent.Draw(m_GraphicDevice, m_SpriteBatch);
            }
        }
    }
}
