﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public partial class I_SceneManager : IDisposable
    {
        ///// <summary>
        ///// 讀取LocatorList存檔並轉換成Point Light，回傳Point Light的Parent
        ///// </summary>
        //public static LocatorNode LoadPointLightListFromLocatorList(SpecialMeshDataManager.LocatorList locatorList)
        //{
        //    LocatorNode PointLightRootNode = LocatorNode.Create();
        //    PointLightRootNode.NodeName = "PointLight_RootNode";
        //    for (int a = 0; a < locatorList.GlobalPoses.Length; a++)
        //    {
        //        Vector3 vvv = locatorList.GlobalPoses[a].translation;
        //        PointLight plight = PointLight.Default(vvv);//從locator就給default值
        //        PointLightNode pointLightNode = PointLightNode.Create();
        //        pointLightNode.SetPointLightValu(plight);

        //        PointLightRootNode.AttachChild(pointLightNode);
        //        //pointLightNode.NodeName = "PointLight_" + a.ToString();
        //    }
        //    return PointLightRootNode;
        //}

        /// <summary>
        /// 取得此scene中方向光的資訊(一個場景中一盞方向光應該就夠，參數傳null表示不取該值)
        /// </summary>
        public void GetDirectionalLight(ref Vector3 LightDirection, ref Vector3 LightAmbient,
            ref Vector3 LightDiffuse, ref Vector3 LightSpecular)
        {
            if (LightDirection != null)
                LightDirection = m_SceneData.SceneLightFogData.Light1Direction;
            if (LightAmbient != null)
                LightAmbient = m_SceneData.SceneLightFogData.Light1Ambient;
            if (LightDiffuse != null)
                LightDiffuse = m_SceneData.SceneLightFogData.Light1Diffuse;
            if (LightSpecular != null)
                LightSpecular = m_SceneData.SceneLightFogData.Light1Specular;
        }

        /// <summary>
        /// 設定此scene中方向光的資訊(一個場景中一盞方向光應該就夠，參數傳null表示不設定該值)
        /// </summary>
        public void SetDirectionalLight(ref Vector3 LightDirection, ref Vector3 LightAmbient,
            ref Vector3 LightDiffuse, ref Vector3 LightSpecular)
        {
            if (LightDirection != null)
            {
                m_SceneData.SceneLightFogData.Light1Direction = LightDirection;
                m_SceneData.SceneLightFogData.Light1Direction.Normalize();
            }
            if (LightAmbient != null)
                m_SceneData.SceneLightFogData.Light1Ambient = LightAmbient;
            if (LightDiffuse != null)
                m_SceneData.SceneLightFogData.Light1Diffuse = LightDiffuse;
            if (LightSpecular != null)
                m_SceneData.SceneLightFogData.Light1Specular = LightSpecular;
        }

        List<BasicNode> m_AllLightNode = new List<BasicNode>(128);

        /// <summary>
        /// 取得此scene中所有loader裡面的point light資訊
        /// </summary>
        public List<BasicNode> GetPointLights()
        {
            m_AllLightNode.Clear();
            List<BasicMeshLoader>.Enumerator itr = GetLoaderAssetItr();
            while (itr.MoveNext())
            {
                SceneNode.AddNodeSameType(itr.Current.GetRootNode, m_AllLightNode, typeof(PointLightNode));
            }
            return m_AllLightNode;
        }

        /// <summary>
        /// 取得此scene中所有loader裡面的spot light資訊
        /// </summary>
        public List<BasicNode> GetSpotLights()
        {
            m_AllLightNode.Clear();
            List<BasicMeshLoader>.Enumerator itr = GetLoaderAssetItr();
            while (itr.MoveNext())
            {
                SceneNode.AddNodeSameType(itr.Current.GetRootNode, m_AllLightNode, typeof(SpotLightNode));
            }
            return m_AllLightNode;
        }
    }
}
