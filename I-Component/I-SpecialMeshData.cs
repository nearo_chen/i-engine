﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
//using System.Xml.Serialization;
using System.IO;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public partial class I_SpecialMeshData
    {
        /// <summary>
        /// 根據特殊設定檔讀取場景並設定它，傳出場景模型檔名，和檔案型態(普通場景或骨架動畫)，
        /// 還有傳出場景物件(BasicMeshLoader)。
        /// </summary>
        public static BasicMeshLoader LoadSceneFromSpecialXML(ref string XMLfilename, ContentManager content, GraphicsDevice device,
            out SpecialMeshDataManager.TatalSpecialMeshData total,
            out AnimationTableDataArray animationTableDataArray)
        {
            animationTableDataArray = null;
            object loadObj = I_IO.LoadXMLFile(ref XMLfilename, typeof(SpecialMeshDataManager.TatalSpecialMeshData));
            total = loadObj as SpecialMeshDataManager.TatalSpecialMeshData;

            if (total == null)
                return null;

            //讀模型檔
            BasicMeshLoader loadScene = null;
            if (string.IsNullOrEmpty(total.ModelFileName) == false)
            {
                switch ((SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE)total.ModelType)
                {
                    case SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.SceneMesh:
                        {
                            string fileName;
                            Utility.RemoveSubFileName(ref total.ModelFileName, out fileName);
                            Utility.GetFullPath(ref fileName, out fileName);

                            SceneMeshLoader sceneLoader = null;
                            if (SceneMeshLoader.LoadSceneMesh(fileName, out sceneLoader, content, device))
                            {
                                //SceneNode.SetAllLightEnable(sceneLoader.GetRootNode, true);
                                loadScene = sceneLoader;
                            }
                            //loadScene = SpecialMeshDataManager.LoadScene_XNB(total.ModelFileName, content, device);
                        }
                        break;
                    case SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.KeyframeMesh:
                        {
                            string fileName;
                            Utility.RemoveSubFileName(ref total.ModelFileName, out fileName);
                            Utility.GetFullPath(ref fileName, out fileName);

                            KeyframeMeshLoader keyframeMeshLoader;
                            animationTableDataArray = KeyframeMeshLoader.LoadKeyFrameMesh(fileName, content, device, out keyframeMeshLoader);
                            loadScene = keyframeMeshLoader;
                        }
                        break;
                    case SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.SkinnedMesh:
                        {
                            string fileName;
                            Utility.RemoveSubFileName(ref total.ModelFileName, out fileName);
                            Utility.GetFullPath(ref fileName, out fileName);

                            SkinningMeshLoader skinLoader;
                            animationTableDataArray = SkinningMeshLoader.LoadSkinningMeshLoader(fileName, content, device, out skinLoader);
                            loadScene = skinLoader;
                        }
                        break;
                    default:
                        loadScene = null;
                        break;
                }
            }

            if (loadScene == null)
                return null;

            if (loadScene.GetRootNode != null)
                loadScene.GetRootNode.NodeName = total.ModelFileName;

            if (loadScene != null)
                SpecialMeshDataManager.CreateAllSceneData_SceneObject(loadScene, total, content, device);

            return loadScene;
        }
    }
}
