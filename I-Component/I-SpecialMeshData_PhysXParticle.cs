﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
//using System.Xml.Serialization;
using System.IO;
using I_XNAComponent;
using I_XNAUtility;
using MeshDataDefine;

namespace I_Component
{
    public partial class I_SpecialMeshData
    {
        [Serializable]
        public class I_PhysXParticleData
        {
            public I_PhysXParticleData() { }
            public I_PhysXParticleData(int particleId, string particleName, int particleMaxmum,
                float persecondParticles, Vector3 particleStartPosition,
                float startingVelocity, Vector3 particleAcceleration,
                int particleLife_millisecond, float mass, float densify)
            {
                ParticleId = particleId;
                ParticleName = particleName;
                ParticleMaxmum = particleMaxmum;
                PersecondParticles = persecondParticles;
                ParticleStartPosition = particleStartPosition;
                ParticleAcceleration = particleAcceleration;
                ParticleLife_millisecond = particleLife_millisecond;
                StartingVelocity = startingVelocity;
                Mass = mass;
                Densify = densify;
            }
            public int ParticleId;
            public string ParticleName;
            //最大數量
            public int ParticleMaxmum;
            public float PersecondParticles;
            public Vector3 ParticleStartPosition;
            public Vector3 ParticleAcceleration;
            public int ParticleLife_millisecond;
            public float StartingVelocity;
            //質量
            public float Mass;
            //密度
            public float Densify;

        }
    }
}
