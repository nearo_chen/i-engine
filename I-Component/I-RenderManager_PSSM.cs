﻿//#define PSSMTest_Mine

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {
        //地型若是很大塊，幾乎在PSSM各階層都會有同樣地型，要是面數又高的話殺傷力會很強，
        //最好的做法是將地形切碎，以利frustum culling。

        Effect m_CreateDepthMapEffect = null;

        //處理畫PSSM的不同階層
        //int m_PSSMLevelAmount = 0;
        DepthMapCreator[] m_PSSMRT = null;
        Matrix[] m_PSSMViewfrustums = null;
        float[] m_PSSMCameraFarPlane;//記錄每一階段PSSM的遠平面距離
        Matrix[] m_PSSMSceneDependentFrustum = null;

        int m_PSSMDepthMapSize = 512;

        /// <summary>
        /// 建立PSSM的Render Target
        /// </summary>
        /// <param name="pssmNum">設定PSSM的切割數量，1~4</param>
        void CreatePSSM()
        {
            if (Utility.IfContentOutSide)
            {
                string tempString = "XNBData\\Content\\fx\\render\\SimpleDepthMap";
                Utility.GetFullPath(ref tempString, out tempString);
                m_CreateDepthMapEffect = Utility.ContentLoad_Effect(m_Content, tempString);
            }
            else
            {
                m_CreateDepthMapEffect = Utility.ContentLoad_Effect(m_Content, "fx\\render\\SimpleDepthMap");
            }

            parAlphaTestReference = m_CreateDepthMapEffect.Parameters["AlphaTestReference"];
            oldAlphaTestReference = parAlphaTestReference.GetValueSingle();

            //m_PSSMLevelAmount = pssmNum;
            m_PSSMRT = new DepthMapCreator[4];
            m_PSSMRT[0] = DepthMapCreator.Create();
            m_PSSMRT[1] = DepthMapCreator.Create();
            m_PSSMRT[2] = DepthMapCreator.Create();
            m_PSSMRT[3] = DepthMapCreator.Create();
            m_PSSMRT[0].Initialize(m_PSSMDepthMapSize, Vector3.One, m_GraphicDevice);
            m_PSSMRT[1].Initialize(m_PSSMDepthMapSize, Vector3.One, m_GraphicDevice);
            m_PSSMRT[2].Initialize(m_PSSMDepthMapSize, Vector3.One, m_GraphicDevice);
            m_PSSMRT[3].Initialize(m_PSSMDepthMapSize, Vector3.One, m_GraphicDevice);

            //for (int a = 0; a < m_PSSMRT.Length; a++)
            //{
            //    m_PSSMRT[a] = DepthMapCreator.Create();
            //    int size = 256;
            //    if (a == 0)
            //        size = 1024;
            //    if (a == 1)
            //        size = 512;
            //    m_PSSMRT[a].Initialize(size, Vector3.One, m_GraphicDevice);
            //}

            m_PSSMViewfrustums = new Matrix[4];
            m_PSSMCameraFarPlane = new float[4];
            m_PSSMSceneDependentFrustum = new Matrix[4];
        }

        void DisposePSSM()
        {
            m_CreateDepthMapEffect.Dispose();
            m_CreateDepthMapEffect = null;

            for (int a = 0; a < m_PSSMRT.Length; a++)
            {
                m_PSSMRT[a].Dispose();
                m_PSSMRT[a] = null;
            }
            m_PSSMRT = null;

            parAlphaTestReference = null;
        }

        /// <summary>
        /// 根據camera做PSSM各階層的view frustum，splitRatioLambda為調整線性與log曲線的區域畫分比率。
        /// splitRatioLambda越1吃LOG比重越重，PSSM0會越小。
        /// 回傳true表示要畫PSSM Shadow Map
        /// </summary>
        protected bool PSSMProcessViewsFrustum(int sceneID, float viewportW, float viewportH)
        {
            I_SceneManager scene = GetScene(sceneID);
            I_Camera camera = scene.GetMainCamera();
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return false;

            if (camera.PSSMLevelAmount <= 0)
                return false;

            Matrix //projectionMat, 
                cameraMat, invCameraMat;
            //camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;

            //測試用MATRIX         
            //viewMatrix = Matrix.CreateLookAt(Vector3.Zero, Vector3.UnitX, Vector3.Up);

            //先話分PSSM的view的近、遠平面，計算各為多少，然後得到各level的view frustum。
            float cameraNear = camera.Near;
            for (int i = 1; i <= camera.PSSMLevelAmount; i++)
            {
                float CLog = camera.Near * (camera.Far / camera.Near);
                CLog = (float)Math.Pow(CLog, ((float)i / (float)camera.PSSMLevelAmount));

                float CUni = (camera.Far - camera.Near) * i;
                CUni = CUni / (float)camera.PSSMLevelAmount;
                CUni = camera.Near + CUni;

                float CI = camera.PSSMSplitRatioLambda * CLog + (1f - camera.PSSMSplitRatioLambda) * CUni;

                //計算投影矩陣
                Matrix.CreatePerspectiveFieldOfView(
                          MathHelper.ToRadians(camera.FovDegree), viewportW / viewportH, cameraNear, CI, out m_PSSMViewfrustums[i - 1]);

                //計算Crop               
                m_PSSMViewfrustums[i - 1] = viewMatrix * m_PSSMViewfrustums[i - 1];

#if PSSMTest_Mine
#else
                new BoundingFrustum(m_PSSMViewfrustums[i - 1]).GetCorners(PSSMGetCorner);
                m_PSSMSceneDependentFrustum[i - 1] = CalculateProjectionCropMatrix(PSSMGetCorner,
                    scene.m_SceneData.SceneLightFogData.Light1Direction);
#endif

                cameraNear = CI;//遠平面變成下一階段的近平面
                m_PSSMCameraFarPlane[i - 1] = CI;
            }

            return true;
        }

#if PSSMTest_Mine
#else
        public Matrix CalculateProjectionCropMatrix(Vector3[] SplitFrustumCorners, Vector3 lightDir)
        {
            float _fLightFar = 1000000;
            float maxSize = 1000000;
            Matrix _mLightView, _mLightProj;
            float _fLightNear = 1;

            // calculate standard view and projection matrices for light     
            _mLightView = Matrix.CreateLookAt(Vector3.Zero, -lightDir, Vector3.Up);
            _mLightProj = Matrix.CreateOrthographic(maxSize, maxSize, _fLightNear, _fLightFar);

            // Next we will find the min and max values of the current        
            // frustum split in lights post-projection space        
            // (where coordinate range is from -1.0 to 1.0)      
            //     
            float fMaxX = -float.MaxValue;
            float fMaxY = -float.MaxValue;
            float fMinX = float.MaxValue;
            float fMinY = float.MaxValue;
            float fMaxZ = -float.MaxValue;
            //float fMinZ = float.MaxValue;
            Matrix mLightViewProj = _mLightView * _mLightProj;
            // for each corner point
            for (int i = 0; i < 8; i++)
            {
                // transform point
                Vector4 vTransformed = Vector4.Transform(SplitFrustumCorners[i], mLightViewProj);
                // project x and y
                //  vTransformed /= vTransformed.W;
                vTransformed.X /= vTransformed.W;
                vTransformed.Y /= vTransformed.W;
                //vTransformed.Z /= vTransformed.W;

                // find min and max values          
                if (vTransformed.X > fMaxX) fMaxX = vTransformed.X;
                if (vTransformed.Y > fMaxY) fMaxY = vTransformed.Y;
                if (vTransformed.Y < fMinY) fMinY = vTransformed.Y;
                if (vTransformed.X < fMinX) fMinX = vTransformed.X;
                // find largest z distance
                if (vTransformed.Z > fMaxZ) fMaxZ = vTransformed.Z;
                //if (vTransformed.Z < fMinZ) fMinZ = vTransformed.Z;
            }
            // set values to valid range (post-projection)
            fMaxX = MathHelper.Clamp(fMaxX, -1.0f, 1.0f);
            fMaxY = MathHelper.Clamp(fMaxY, -1.0f, 1.0f);
            fMinX = MathHelper.Clamp(fMinX, -1.0f, 1.0f);
            fMinY = MathHelper.Clamp(fMinY, -1.0f, 1.0f);
            // Adjust the far plane of the light to be at the farthest
            // point of the frustum split. Some bias may be necessary.
            //  

            //fMaxZ = MathHelper.Clamp(fMaxZ, -1.0f, 1.0f);
            //fMinZ = MathHelper.Clamp(fMinZ, -1.0f, 1.0f);
            _fLightFar = 300f;
            //(fMaxZ - fMinZ) *_fLightFar;//_fLightNear + fMaxZ * (_fLightFar - _fLightNear)* 0.5f;//500f;// fMaxZ + _fLightNear;// +1.5f;

            // re-calculate lights matrices with the new far plane
            _mLightProj = Matrix.CreateOrthographic(maxSize, maxSize, _fLightNear, _fLightFar);


            // Next we build a special matrix for cropping the lights view
            // to only contain points of the current frustum split    
            //
            float fScaleX = 2.0f / (fMaxX - fMinX);
            float fScaleY = 2.0f / (fMaxY - fMinY);
            float fOffsetX = -0.5f * (fMaxX + fMinX) * fScaleX;
            float fOffsetY = -0.5f * (fMaxY + fMinY) * fScaleY;

            float fScaleZ = -2f / (_fLightFar - _fLightNear);

            Matrix mCropView = new Matrix(
                fScaleX, 0.0f, 0.0f, 0.0f,
                0.0f, fScaleY, 0.0f, 0.0f,
                0.0f, 0.0f, fScaleZ, 0.0f,
                fOffsetX, fOffsetY, 0.5f, 1.0f);//0.5f改變Z中心點offset

            // multiply the projection matrix with it
            _mLightProj *= mCropView;

            // finally modify projection matrix for linearized depth        
            //_mLightProj.M33 /= _fLightFar;
            //_mLightProj.M43 /= _fLightFar;

            return _mLightView * _mLightProj;
        }
#endif

        Vector3[] PSSMGetCorner = new Vector3[BoundingFrustum.CornerCount];
        NodeArray m_PSSMShowfrustumNodes = null;
        /// <summary>
        /// 將各階層的PSSM view frustum，計算陰影frustum再做culling，然後畫在深度圖上。
        /// </summary>
        protected void PSSMRenderDepthMap(int sceneID)
        {
            m_GraphicDevice.RenderState.DepthBufferEnable = true;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = true;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.AlphaTestEnable = false;//使用Alphareference所以不用alpha test了

            I_SceneManager scene = GetScene(sceneID);
            I_Camera camera = scene.GetMainCamera();
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return;
            Matrix cameraMat, invCameraMat;
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);

            //Viewport oldViewPort = m_GraphicDevice.Viewport;

#if PSSMTest_Mine
            Matrix sceneDependentViewMat, sceneDependentProMat;
#endif

            uint triangles = 0;
            //畫出各view區段的DepthMap
            for (int a = 0; a < camera.PSSMLevelAmount; a++)
            {
                BoundingFrustum PSSMViewFrustum = new BoundingFrustum(m_PSSMViewfrustums[a]);
                PSSMViewFrustum.GetCorners(PSSMGetCorner);

                //先取得view裡面，區段a的frustum來計算陰影frustum。
                //先採用場景無關方法計算frustum，也就是拿區段a的frustum來計算陰影frustum。
#if PSSMTest_Mine
                Matrix? viewMatrix; Matrix? projMatrix;
                m_PSSMRT[a].SetRenderTarget(
                    m_GraphicDevice, scene.m_SceneData.SceneLightFogData.Light1Direction, cameraMat.Forward,
                    PSSMGetCorner, 1,
                    out viewMatrix, out projMatrix);

                //再計算culling陰影frustum內的node。
                int nodeAmount = scene.ProcessCameraFrustumCulling(ref cameraName, ref m_PSSMRT[a].DepthMapFrustum);
#else
                m_PSSMRT[a].SetRenderTarget(m_GraphicDevice);
                //再計算culling陰影frustum內的node。
                int nodeAmount = scene.ProcessCameraFrustumCulling(camera, ref m_PSSMSceneDependentFrustum[a]);//m_PSSMRT[a].DepthMapFrustum);
#endif

                //將可以畫出的Node整理成一個NodeArray
                if (m_PSSMShowfrustumNodes == null || m_PSSMShowfrustumNodes.nodeArray.Length < camera.GetMeshNodeAmount())
                    m_PSSMShowfrustumNodes = new NodeArray(camera.GetMeshNodeAmount());

                List<MeshNode>.Enumerator meshNodeItr = camera.GetAllMeshNodeItr();
                while (meshNodeItr.MoveNext())
                {
                    MeshNode meshNode = meshNodeItr.Current;
                    if (meshNode.BeCulled)
                        continue;
                    if (meshNode.IfRenderShadowMap == false)
                        continue;
                    m_PSSMShowfrustumNodes.Add(meshNode);
                }
                if (m_PSSMShowfrustumNodes.nowNodeAmount == 0)//NODE都被咖掉就別畫下去啦~
                    continue;

                //Viewport cameraViewport = m_GraphicDevice.Viewport;
                //cameraViewport.MinDepth = (float)a * (1.0f / (float)camera.PSSMLevelAmount);
                //cameraViewport.MaxDepth = ((float)a + 1) * (1.0f / (float)camera.PSSMLevelAmount);
                //m_GraphicDevice.Viewport = cameraViewport;

                //再使用場景相關方法，根據陰影frustum內的node，計算新的較match的陰影frustum。
                //場景相依很煩，會有個超大mesh邊切到的話，計算後的frustum會變超大
#if PSSMTest_Mine
                //m_PSSMRT[a].CreateLightViewProjectionMatrix(m_PSSMShowfrustumNodes,
                //    ref scene.m_SceneData.SceneLightFogData.Light1Direction,
                //    out sceneDependentViewMat, out sceneDependentProMat, m_PSSMRT[a].NowCalculateAABB);
                //Matrix.Multiply(ref sceneDependentViewMat, ref sceneDependentProMat, out m_PSSMSceneDependentFrustum[a]);
                m_PSSMSceneDependentFrustum[a] = m_PSSMRT[a].DepthMapFrustum;
#endif

                //拿frustum與culling後的node去畫出深度圖
                RenderNodesShadowMap_MeshNode(m_PSSMShowfrustumNodes, ref m_PSSMSceneDependentFrustum[a], ref triangles);
                //RenderNodesShadowMap_SkinningMeshNode(camera, ref m_PSSMRT[a].DepthMapFrustum);
                m_PSSMShowfrustumNodes.Reset();
            }


            //m_GraphicDevice.Viewport = oldViewPort;
            m_GraphicDevice.SetRenderTarget(0, null);
        }





        EffectParameter parAlphaTestReference = null;
        float oldAlphaTestReference;

        /// <summary>
        /// 自己設定Effect參數，除了IfRenderShadowMap不要畫的，將Camera內的MeshNode都畫出來，
        /// </summary>
        void RenderNodesShadowMap_MeshNode(NodeArray nodeArray, ref Matrix depthMapFrustum, ref uint triangle)
        {
            m_CreateDepthMapEffect.CurrentTechnique = m_CreateDepthMapEffect.Techniques["SimpleMesh_DepthMap"];

            m_CreateDepthMapEffect.Begin();
            m_CreateDepthMapEffect.CurrentTechnique.Passes[0].Begin();

            m_CreateDepthMapEffect.Parameters["DepthMapFrustum"].SetValue(depthMapFrustum);

            //List<MeshNode>.Enumerator meshNodeItr = camera.GetAllMeshNodeItr();
            //while (meshNodeItr.MoveNext())
            for (int a = 0; a < nodeArray.nowNodeAmount; a++)
            {
                MeshNode meshNode = nodeArray.nodeArray[a] as MeshNode;// meshNodeItr.Current;
                if (meshNode.BeCulled)
                    continue;
                if (meshNode.IfRenderShadowMap == false)
                    continue;

                //畫模型深度
                m_CreateDepthMapEffect.Parameters["WorldMatrix"].SetValue(meshNode.WorldMatrix);

                float alphaRef = (float)meshNode.AlphaTestReference / 255f;
                if (oldAlphaTestReference != alphaRef)
                {
                    oldAlphaTestReference = alphaRef;
                    parAlphaTestReference.SetValue(oldAlphaTestReference);
                }

                //以後要實作畫深度圖時做透空測試的開關，以減少材質取樣
               // m_CreateDepthMapEffect.Parameters["Texture"].SetValue(meshNode.BaseTexture);
                m_CreateDepthMapEffect.CommitChanges();

                //m_GraphicDevice.RenderState.CullMode = CullMode.None;
                meshNode.Render(null, ref triangle, m_GraphicDevice);
                //m_GraphicDevice.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            }
            //meshNodeItr.Dispose();

            m_CreateDepthMapEffect.CurrentTechnique.Passes[0].End();
            m_CreateDepthMapEffect.End();
        }

        //List<SkinningMeshNode>.Enumerator skinNodeItr = camera.GetAllSkinningMeshNodeItr();
        //      while (skinNodeItr.MoveNext())
        //      {
        //          if (skinNodeItr.Current.IfRenderShadowMap == false)
        //              continue;
        //          //畫模型深度
        //      }
        //      skinNodeItr.Dispose();

        /// <summary>
        /// 把PSSM的RT畫出來看看
        /// </summary>
        protected void RenderPSSMRT(int sceneID)
        {
            if (I_Utility.IfDrawDebug == false || IfDrawPSSMMap == false)
                return;

            I_SceneManager scene = GetScene(sceneID);
            I_Camera camera = scene.GetMainCamera();
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return;

            m_SpriteBatch.Begin(SpriteBlendMode.None);
            int y = 0;
            //foreach (DepthMapCreator dmc in m_PSSMRT)
            for (int a = 0; a < camera.PSSMLevelAmount; a++)
            {
                m_SpriteBatch.Draw(m_PSSMRT[a].GetDepthMap, new Rectangle(0, y, 128, 128), Color.White);
                y += 128;
            }
            m_SpriteBatch.End();
        }


        EffectParameter[] parPSSMMapDepthBias = null;
        EffectParameter[] parPSSMMapFrustum;
        //EffectParameter[] parIfPSSMMap;
        EffectParameter[] parPSSMLevelMap;
        //EffectParameter[] parSampleDirection;

        /// <summary>
        /// 將PSSM的shadow map與GBUFFER計算成一張影子圖，畫在Other map上。
        /// 回傳PSSM的OcclusionMAP
        /// </summary>
        protected void DrawPSSMShadowOcclusionToOtherMap(int sceneID)
        {
            I_SceneManager scene = GetScene(sceneID);

            I_Camera camera = scene.GetMainCamera();
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return;

            if (camera.PSSMLevelAmount <= 0)
                return;

            Vector3 LightDirection = Vector3.Zero;
            Vector3 LightAmbient = Vector3.Zero;
            Vector3 LightDiffuse = Vector3.Zero;
            Vector3 LightSpecular = Vector3.Zero;
            scene.GetDirectionalLight(ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;

            m_GraphicDevice.RenderState.AlphaTestEnable = false;
            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.RenderState.DepthBufferEnable = false;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = false;

            //PSSM的參數^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            #region PSSM用到的Effect Paramater
            if (parPSSMMapDepthBias == null)
            {
                parPSSMMapDepthBias = new EffectParameter[4];
                parPSSMMapFrustum = new EffectParameter[4];
                //parIfPSSMMap = new EffectParameter[4];
                parPSSMLevelMap = new EffectParameter[4];
                //parSampleDirection = new EffectParameter[4];
                parPSSMMapDepthBias[0] = m_PSSMEffect.Parameters["PSSMMap0DepthBias"];
                parPSSMMapDepthBias[1] = m_PSSMEffect.Parameters["PSSMMap1DepthBias"];
                parPSSMMapDepthBias[2] = m_PSSMEffect.Parameters["PSSMMap2DepthBias"];
                parPSSMMapDepthBias[3] = m_PSSMEffect.Parameters["PSSMMap3DepthBias"];
                parPSSMMapFrustum[0] = m_PSSMEffect.Parameters["PSSMMap0Frustum"];
                parPSSMMapFrustum[1] = m_PSSMEffect.Parameters["PSSMMap1Frustum"];
                parPSSMMapFrustum[2] = m_PSSMEffect.Parameters["PSSMMap2Frustum"];
                parPSSMMapFrustum[3] = m_PSSMEffect.Parameters["PSSMMap3Frustum"];
                //parIfPSSMMap[0] = m_PSSMEffect.Parameters["bPSSMMap0"];
                //parIfPSSMMap[1] = m_PSSMEffect.Parameters["bPSSMMap1"];
                //parIfPSSMMap[2] = m_PSSMEffect.Parameters["bPSSMMap2"];
                //parIfPSSMMap[3] = m_PSSMEffect.Parameters["bPSSMMap3"];
                parPSSMLevelMap[0] = m_PSSMEffect.Parameters["PSSMMap0"];
                parPSSMLevelMap[1] = m_PSSMEffect.Parameters["PSSMMap1"];
                parPSSMLevelMap[2] = m_PSSMEffect.Parameters["PSSMMap2"];
                parPSSMLevelMap[3] = m_PSSMEffect.Parameters["PSSMMap3"];
                //parSampleDirection[0] = m_PSSMEffect.Parameters["SampleDirection0"];
                //parSampleDirection[1] = m_PSSMEffect.Parameters["SampleDirection1"];
                //parSampleDirection[2] = m_PSSMEffect.Parameters["SampleDirection2"];
                //parSampleDirection[3] = m_PSSMEffect.Parameters["SampleDirection3"];
            }
            #endregion

            for (int a = 0; a < camera.PSSMLevelAmount; a++)
            {
                Texture2D dmap = m_PSSMRT[a].GetDepthMap;
                //parIfPSSMMap[a].SetValue(true);
                parPSSMLevelMap[a].SetValue(dmap);
                parPSSMMapDepthBias[a].SetValue(camera.GetPSSMBias(a));
                parPSSMMapFrustum[a].SetValue(m_PSSMSceneDependentFrustum[a]);

                //kernel
                //parSampleDirection[a].SetValue(new Vector2(1f / m_PSSMDepthMapSize, 1f / m_PSSMDepthMapSize));
            }
            //for (int a = camera.PSSMLevelAmount; a < 4; a++)
            //    parIfPSSMMap[a].SetValue(false);

            //#region 傳入光線與模型法線角度，試圖減輕閃爍，讓成90度的面較沒影子
            //m_PSSMEffect.Parameters["LightDirection"].SetValue(LightDirection);
            //m_PSSMEffect.Parameters["normalMap"].SetValue(GetTexture_NormalRT());
            //#endregion

            m_PSSMEffect.Parameters["SampleDirection"].SetValue(new Vector2(1f / m_PSSMDepthMapSize, 1f / m_PSSMDepthMapSize));
            //m_PSSMEffect.Parameters["SampleDirection"].SetValue(Vector2.Zero);
            m_PSSMEffect.Parameters["PSSMLevelAmount"].SetValue(camera.PSSMLevelAmount);
            //m_PSSMEffect.Parameters["sampleTimes"].SetValue(9);// 5 or 9
            m_PSSMEffect.Parameters["InvertViewProjection"].SetValue(Matrix.Invert(viewMatrix * projectionMat));
            m_PSSMEffect.Parameters["depthMap"].SetValue(GetTexture_DepthRT());
            m_PSSMEffect.Parameters["halfPixel"].SetValue(halfPixel);
            //PSSM的參數^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            m_PSSMEffect.CommitChanges();

            m_GraphicDevice.SetRenderTarget(0, m_OtherMap);//拿m_OtherMap來用

            m_PSSMEffect.Begin();
            m_PSSMEffect.Techniques[0].Passes[0].Begin();
            //draw a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
            m_PSSMEffect.Techniques[0].Passes[0].End();
            m_PSSMEffect.End();

            m_GraphicDevice.SetRenderTarget(0, null);

            return;
            #region 做blur圖
            //做blur圖
            //m_MyBloom.ImportTextureToRenderTarget1(m_OtherMap.GetTexture());
            //m_MyBloom.RenderBothCaussianBlur();
            //return m_MyBloom.GetTexture_RenderTarget1();

            #region Blur PSSM to Other Map Horizonal
            Texture2D PSSMMap = GetTexture_OtherMap();
            m_GraphicDevice.SetRenderTarget(0, m_OtherMap);

            m_PSSMBlurEffect.Parameters["PSSMTexture"].SetValue(PSSMMap);
            m_PSSMBlurEffect.Parameters["blurDirection"].SetValue(new Vector2(1f / (float)PSSMMap.Width * 2f, 0));
            m_PSSMBlurEffect.CommitChanges();

            m_PSSMBlurEffect.Begin();
            m_PSSMBlurEffect.Techniques[0].Passes[0].Begin();
            //draw a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
            m_PSSMBlurEffect.Techniques[0].Passes[0].End();
            m_PSSMBlurEffect.End();

            m_GraphicDevice.SetRenderTarget(0, null);
            #endregion

            #region Blur PSSM to Other Map Vertical
            PSSMMap = GetTexture_OtherMap();
            m_GraphicDevice.SetRenderTarget(0, m_OtherMap);

            m_PSSMBlurEffect.Parameters["PSSMTexture"].SetValue(PSSMMap);
            m_PSSMBlurEffect.Parameters["blurDirection"].SetValue(new Vector2(0, 1f / (float)PSSMMap.Height * 2f));
            m_PSSMBlurEffect.CommitChanges();

            m_PSSMBlurEffect.Begin();
            m_PSSMBlurEffect.Techniques[0].Passes[0].Begin();
            //draw a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
            m_PSSMBlurEffect.Techniques[0].Passes[0].End();
            m_PSSMBlurEffect.End();

            m_GraphicDevice.SetRenderTarget(0, null);
            #endregion

            m_GraphicDevice.SetRenderTarget(0, null);

            #endregion
        }
    }
}
