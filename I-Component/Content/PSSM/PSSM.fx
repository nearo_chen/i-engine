//#define RENDER_PSSM_LEVEL

#include "..\fx\render\SimpleTool.inc"



float2 SampleDirection;//取樣的位移
//float2 SampleDirection1;//取樣的位移
//float2 SampleDirection2;//取樣的位移
//float2 SampleDirection3;//取樣的位移

float4x4 InvertViewProjection;
int PSSMLevelAmount=0;

float PSSMMap0DepthBias;
float4x4 PSSMMap0Frustum;
//bool bPSSMMap0=false;
texture PSSMMap0;
sampler PSSMMap0Sampler = sampler_state
{
    Texture = (PSSMMap0);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

float PSSMMap1DepthBias;
float4x4 PSSMMap1Frustum;
//bool bPSSMMap1=false;
texture PSSMMap1;
sampler PSSMMap1Sampler = sampler_state
{
    Texture = (PSSMMap1);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

float PSSMMap2DepthBias;
float4x4 PSSMMap2Frustum;
//bool bPSSMMap2=false;
texture PSSMMap2;
sampler PSSMMap2Sampler = sampler_state
{
    Texture = (PSSMMap2);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = Anisotropic;
    MinFilter = Anisotropic;
    Mipfilter = Anisotropic;
};

float PSSMMap3DepthBias;
float4x4 PSSMMap3Frustum;
//bool bPSSMMap3=false;
texture PSSMMap3;
sampler PSSMMap3Sampler = sampler_state
{
    Texture = (PSSMMap3);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = Anisotropic;
    MinFilter = Anisotropic;
    Mipfilter = Anisotropic;
};
/*
float3 LightDirection;
texture normalMap;
sampler normalSampler = sampler_state
{
    Texture = (normalMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};
*/
texture depthMap;
  sampler depthSampler = sampler_state
{
    Texture = (depthMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};


struct VertexShaderInput
{
    float3 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

float2 halfPixel;
VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    output.TexCoord = input.TexCoord - halfPixel;
    return output;
}



float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
//↑↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓資訊取得↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	float depthVal = tex2Dlod(depthSampler, float4(input.TexCoord, 0, 0) ).r;
	//float depthVal = tex2D(depthSampler, input.TexCoord ).r;
	
    //  float4 otherData = tex2D(otherSampler,input.TexCoord);
  
   /*   float4 normalData = tex2D(normalSampler,input.TexCoord);
    float3 normal = 2.0f * normalData.xyz - 1.0f;
    */
    
    //compute screen-space position
    float4 positionWorld;
    positionWorld.x = input.TexCoord.x * 2.0f - 1.0f;
    positionWorld.y = -(input.TexCoord.y * 2.0f - 1.0f);
    positionWorld.z = depthVal;
    positionWorld.w = 1.0f;
    
    positionWorld = mul(positionWorld, InvertViewProjection);
    positionWorld /= positionWorld.w;        
//↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑資訊取得↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
    
    int sampleTimes=9;
    float4 finalColor = 1;
    
    DepthSampleData data;
    data.IfInFrustum = false;
    
	if(PSSMLevelAmount>0 && data.IfInFrustum == false)
	{
		data = 
			ConsiderInFrustum_Kernel9(positionWorld, PSSMMap0Frustum, PSSMMap0Sampler, PSSMMap0DepthBias, SampleDirection, sampleTimes);
		finalColor *= data.depthRatio;
	}

	if(PSSMLevelAmount>1 && data.IfInFrustum == false)
	{
		data = 
			ConsiderInFrustum_Kernel9(positionWorld, PSSMMap1Frustum, PSSMMap1Sampler, PSSMMap1DepthBias, SampleDirection, sampleTimes);
		finalColor *= data.depthRatio;
	}
	
	if(PSSMLevelAmount>2 && data.IfInFrustum == false)
	{
		data = 
			 ConsiderInFrustum_Kernel9(positionWorld, PSSMMap2Frustum, PSSMMap2Sampler, PSSMMap2DepthBias, SampleDirection, sampleTimes);
		 finalColor *=data.depthRatio;
	}
			
	if(PSSMLevelAmount>3 && data.IfInFrustum == false)
	{
		data = 
			 ConsiderInFrustum_Kernel9(positionWorld, PSSMMap3Frustum, PSSMMap3Sampler, PSSMMap3DepthBias, SampleDirection, sampleTimes);
		finalColor *=data.depthRatio;
	}

	finalColor.r= clamp(finalColor.r, 0.5f, 1)/* +
	 (
		 0.5f * 
		 (
			 1-  abs(dot(LightDirection, normal))
		 ) 
	  )*/
	  ;
	
													//
    //float4 finalColor = 1;
    ////判斷shadow map
    //float depth = -1;
    //if(bPSSMMap0 && depth <0 )
    //{
		 //depth = ConsiderInFrustum(positionWorld, PSSMMap0Frustum, PSSMMap0Sampler, PSSMMap0DepthBias);
		 //if(depth >= 0)
		 //{
			 //finalColor.rgb = 0.5f;
		 //}
			 //
//#ifdef RENDER_PSSM_LEVEL
		//if(depth != -1)
			//finalColor.r *= 2;
//#endif
    //}
    //
  //
    //if(bPSSMMap1 && depth <0 )
    //{    
		 //depth = ConsiderInFrustum(positionWorld, PSSMMap1Frustum, PSSMMap1Sampler, PSSMMap1DepthBias);
		  //if(depth>=0)
			 //finalColor.rgb=0.5f;
			 //
//#ifdef RENDER_PSSM_LEVEL
		//if(depth != -1)
			//finalColor.g *= 2;
//#endif
    //}
    //
    //if(bPSSMMap2 && depth <0 )
    //{    
		 //depth = ConsiderInFrustum(positionWorld, PSSMMap2Frustum, PSSMMap2Sampler, PSSMMap2DepthBias);
		  //if(depth>=0)
			 //finalColor.rgb=0.5f;
			 //
//#ifdef RENDER_PSSM_LEVEL
		//if(depth != -1)
			//finalColor.b *= 2;
//#endif    
	//}
    //
    //if(bPSSMMap3 && depth <0 )
    //{    
		 //depth = ConsiderInFrustum(positionWorld, PSSMMap3Frustum, PSSMMap3Sampler, PSSMMap3DepthBias);
		  //if(depth>=0)
			 //finalColor.rgb=0.5f;
			 //
//#ifdef RENDER_PSSM_LEVEL
		//if(depth != -1)
			//finalColor.rgb *= float3(2, 1, 2);
//#endif
    //}
    
    return finalColor;
}


technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}
