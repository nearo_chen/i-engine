
float2 blurDirection;
texture PSSMTexture;
sampler2D PSSMTextureSampler = sampler_state
{
    Texture = <PSSMTexture>;
    ADDRESSU = CLAMP;
    ADDRESSV = CLAMP;
    MAGFILTER = LINEAR;
    MINFILTER = LINEAR;
}
;
float4 PixelShaderFunction(float2 TexCoord :TEXCOORD0) : COLOR0
{
    float color = tex2D( PSSMTextureSampler, TexCoord).r;
    float num = 1;
    int blurSamples = 8;
    for( int i = -blurSamples/2; i <= blurSamples/2; i+=1)
	{
        float2 newTexCoord = TexCoord + i * blurDirection.xy;
        float sample = tex2D(PSSMTextureSampler, newTexCoord).r;
        num += (blurSamples/2 - abs(i));
        color += sample * (blurSamples/2 - abs(i));
    }
    
	return color / num;
}


technique PSSMBlur
{
    pass Pass1
    {
        //	VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}

