
//sampler baseSampler : register(s0);

float2 blurDirection;



/*
//depth
texture depthMap;
sampler depthSampler = sampler_state
{
    Texture = (depthMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};
*/

// normals, and specularPower in the alpha channel
//texture normalMap;
//sampler normalSampler = sampler_state
//{
    //Texture = (normalMap);
    //AddressU = CLAMP;
    //AddressV = CLAMP;
    //MagFilter = POINT;
    //MinFilter = POINT;
    //Mipfilter = POINT;
//};

texture SSAOTexture;
sampler2D SSAOTextureSampler = sampler_state
{
	Texture = <SSAOTexture>;
    ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
};



struct VertexShaderInput
{
    float3 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

float2 halfPixel;
VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    output.TexCoord = input.TexCoord - halfPixel;
    return output;
}

float4 PixelShaderFunction(float2 TexCoord :TEXCOORD0) : COLOR0
{
//	TexCoord += halfPixel * 0.5f;

	//get normal data from the normalMap
    //float4 normalData = tex2D(normalSampler, TexCoord);
    //tranform normal back into [-1,1] range
    
    //float3 normal = 2.0f * normalData.xyz - 1.0f;
    
    float color = tex2D( SSAOTextureSampler, TexCoord).r;
   
    float num = 1;
    int blurSamples = 8; 
	
	for( int i = -blurSamples/2; i <= blurSamples/2; i+=1)
	{
		float2 newTexCoord = TexCoord + i * blurDirection.xy;
		
		float sample = tex2D(SSAOTextureSampler, newTexCoord).r;
		//normalData = tex2D(normalSampler, newTexCoord);
		//float3 samplenormal =  2.0f * normalData.xyz - 1.0f;
			
		//if ( abs(dot(samplenormal, normal)) > 0.99 )
		{
			num += (blurSamples/2 - abs(i));
			color += sample * (blurSamples/2 - abs(i));
		}
	}

	return  color / num;
}

technique SSAOBlur
{
    pass Pass1
    {
		VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}

