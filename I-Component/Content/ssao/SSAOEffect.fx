float sampleRadius;
float distanceScale;
float SSAOFadeOutDistance;

bool IfPSSMOcclusionMap=false;
texture PSSMOcclusionMap;
sampler2D PSSMOcclusionMapSampler = sampler_state
{
	Texture = <PSSMOcclusionMap>;
     AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

float4x4 InvertProjection;
float4x4 Projection;

texture depthTexture;
texture randomNormalTexture;

float sampleTimes = 8;//取樣的for迴圈數
float sampleLOD = 0;//周圍SAMPLING中由深度圖LOD中取得深度，由LOD越大值去SAMPLE速度會較快。

sampler2D DepthMapSampler = sampler_state
{
	Texture = <depthTexture>;
     AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler2D RandNormal = sampler_state
{
	Texture = <randomNormalTexture>;
    ADDRESSU = WRAP;
	ADDRESSV = WRAP;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
};

struct VertexShaderInput
{
    float3 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

float2 halfPixel;
VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    output.TexCoord = input.TexCoord - halfPixel;
    return output;
}





float4 samples8[8] =
{
float4(0.355512,  -0.709318,  -0.102371, 0.0 ),
float4(0.534186,  0.71511,  -0.115167, 0.0 ),
float4(-0.87866,  0.157139,  -0.115167, 0.0 ),
float4(0.140679,  -0.475516,  -0.0639818, 0.0 ),
float4(-0.207641,  0.414286,  0.187755, 0.0 ),
float4(-0.277332,  -0.371262,  0.187755, 0.0 ),
float4(0.63864,  -0.114214,  0.262857, 0.0 ),
float4(-0.184051,  0.622119,  0.262857, 0.0 )
};

float4 SSAOPixelShader(float2 texCoord : TEXCOORD0) : COLOR0
{
//float4 samples16[8] =
//{
	//float4(0.355512, 	-0.709318, 	-0.102371,	0.0 ),
	//float4(0.534186, 	0.71511, 	-0.115167,	0.0 ),
	//float4(-0.87866, 	0.157139, 	-0.115167,	0.0 ),
	//float4(0.140679, 	-0.475516, 	-0.0639818,	0.0 ),
	//float4(-0.0796121, 	0.158842, 	-0.677075,	0.0 ),
	//float4(-0.0759516, 	-0.101676, 	-0.483625,	0.0 ),
	//float4(0.12493, 	-0.0223423,	-0.483625,	0.0 ),
	//float4(-0.0720074, 	0.243395, 	-0.967251,	0.0 ),
	//float4(-0.207641, 	0.414286, 	0.187755,	0.0 ),
	//float4(-0.277332, 	-0.371262, 	0.187755,	0.0 ),
	//float4(0.63864, 	-0.114214, 	0.262857,	0.0 ),
	//float4(-0.184051, 	0.622119, 	0.262857,	0.0 ),
	//float4(0.110007, 	-0.219486, 	0.435574,	0.0 ),
	//float4(0.235085, 	0.314707, 	0.696918,	0.0 ),
	//float4(-0.290012, 	0.0518654, 	0.522688,	0.0 ),
	//float4(0.0975089, 	-0.329594, 	0.609803,	0.0 )
//};

	// Get the current depth stored in the shadow map
	float depthVal = tex2D(DepthMapSampler, texCoord).r;
	
	//↑↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓資訊取得↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	
      //compute screen-space position
    float4 positionView;
    positionView.x = texCoord.x * 2.0f - 1.0f;
    positionView.y = -(texCoord.y * 2.0f - 1.0f);
    positionView.z = depthVal;
    positionView.w = 1.0f;
    //transform to view space
    positionView = mul(positionView, InvertProjection);
    positionView /= positionView.w;
//↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑資訊取得↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

	float3 se = positionView.xyz;

	float3 randNormal = tex2D( RandNormal, texCoord * 200.0 ).rgb;

	//float3 normal = tex2D(depthSampler, texCoord).rgb;
	float finalColor = 0.0f;

	//離Camera越近取樣越少
	//float fadeOutSampleTimes = sampleTimes;// *  saturate(positionView.z / SSAOFadeOutDistance);

	for (int i = 0; i < sampleTimes/*fadeOutSampleTimes*/; i++)
	{
		//根據random normal map作取樣ray擾動
		float3 ray = //samples[i].xyz * sampleRadius;
			reflect(samples8[i].xyz, randNormal) * sampleRadius;
		
		//if (dot(ray, normal) < 0)
		//	ray += normal * sampleRadius;
			
		float4 sample = float4(se + ray, 1.0f);
		float4 ss = mul(sample, Projection);

		ss /= ss.w;
		float2 sampleTexCoord = float2(   (ss.x +1)*0.5f , (1-ss.y)*0.5f);
		//sampleTexCoord -= halfPixel;
				
		 //用view 的深度來計算occlusion
		 //float sampleDepth = tex2D(DepthMapSampler, sampleTexCoord ).r;
		float sampleDepth = tex2Dlod(DepthMapSampler, float4(sampleTexCoord, 0, sampleLOD) ).r;
		
		float4 samplePositionView;
		samplePositionView.x = sampleTexCoord.x * 2.0f - 1.0f;
		samplePositionView.y = -(sampleTexCoord.y * 2.0f - 1.0f);
		samplePositionView.z = sampleDepth;
		samplePositionView.w = 1.0f;
		//transform to view space
		samplePositionView = mul(samplePositionView, InvertProjection);
		samplePositionView /= samplePositionView.w;

		if (sampleDepth == 1.0)
		{
			finalColor ++;
		}
		else
		{
			float occlusion = distanceScale* max(samplePositionView.z - positionView.z, 0.0f);//max(sampleDepth - depthVal, 0.0f);
			finalColor += 1.0f / (1.0f + occlusion * occlusion);// * 0.1);
		}
	}

	float4 combineColor = finalColor / sampleTimes;
	combineColor.a =1;	
	
	if(IfPSSMOcclusionMap)
	{
		combineColor.rgb *= tex2D( PSSMOcclusionMapSampler, texCoord).rgb;	
	}

	return combineColor;
}

technique SSAO
{
    pass P0
    {          
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader  = compile ps_3_0 SSAOPixelShader();
    }
}