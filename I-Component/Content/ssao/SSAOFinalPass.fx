
//sampler baseSampler : register(s0);
texture SSAOTex;

sampler2D SSAOSampler = sampler_state
{
	Texture = <SSAOTex>;
    ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
};

sampler SceneTexture :register(s0);

float4 PixelShaderFunction(float2 TexCoord :TEXCOORD0) : COLOR0
{	
	float4 finalColor = tex2D(SceneTexture, TexCoord);
	float4 aoColor = tex2D(SSAOSampler, TexCoord);
	
	finalColor.rgb = finalColor.rgb * aoColor.r;
	return finalColor;

}


technique Merge
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}