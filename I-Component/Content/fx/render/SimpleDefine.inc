#ifndef  _SimpleDefine_inc_
#define _SimpleDefine_inc_


struct Simple_VSIn
{
    float4 Position : POSITION0;
    float3 Normal   : NORMAL0;
    float2 TexCoord : TEXCOORD0;
};

struct Simple_VSOut
{
	float4 WVPPosition : POSITION0;
	float2 TexCoord : TEXCOORD0;
    float3 WorldNormal   : TEXCOORD1;  
    float3 WorldViewPos : TEXCOORD2; 
    float3 WorldPos : TEXCOORD3;
};	

struct Simple_PSIn
{
    float2 TexCoord : TEXCOORD0;    
    float3 WorldNormal   : TEXCOORD1;  
    float3 WorldViewPos : TEXCOORD2; 
    float3 WorldPos : TEXCOORD3;   
};



// NOTE: even though the tangentToWorld matrix is only marked 
// with TEXCOORD3, it will actually take TEXCOORD3, 4, and 5.
struct NormalMap_VSOut
{
    float4 WVPPosition : POSITION0;
	float2 TexCoord : TEXCOORD0;
    float3 WorldViewPos : TEXCOORD1; 

    float3x3 tangentToWorld    : TEXCOORD2;

    float3 WorldPos : TEXCOORD5;
};


//-----------------------------------------------------------------------------
// Pixel shader output structure
//-----------------------------------------------------------------------------
struct MULTIPS_OUTPUT2
{
    // The pixel shader can output 2+ values simulatanously if 
    // d3dcaps.NumSimultaneousRTs > 1
    
    float4 RGBColor1 : COLOR0;  // Pixel color    
    float4 RGBColor2 : COLOR1; 
};

struct MULTIPS_OUTPUT3
{
    // The pixel shader can output 2+ values simulatanously if 
    // d3dcaps.NumSimultaneousRTs > 1
    
    float4 RGBColor1 : COLOR0;  // Pixel color    
    float4 RGBColor2 : COLOR1; 
    float4 RGBColor3 : COLOR2; 
};

struct MULTIPS_OUTPUT4
{
    // The pixel shader can output 2+ values simulatanously if 
    // d3dcaps.NumSimultaneousRTs > 1
    
    float4 RGBColor1 : COLOR0;  // Pixel color    
    float4 RGBColor2 : COLOR1; 
    float4 RGBColor3 : COLOR2; 
    float4 RGBColor4 : COLOR3; 
};
#endif