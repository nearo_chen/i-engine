//used for EmmisiveTexture...

float EmmisiveAlpha = 0;//控制emmisive的強度
//const float EmmisiveThreshold = 0.4f;
//static shared	bool bTruColor = false;
//static shared float4 EmmisiveColor=0;

bool bEmmisiveMap = false;
texture2D EmmisiveMap;
sampler2D EmmisiveMapSampler = sampler_state
{
	Texture = (EmmisiveMap);
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = wrap;
	AddressV  = wrap;
};


float GetEmmisiveMapValue(float2 texcoord)
{
	if(bEmmisiveMap)
	{
	    //如果有自發光設定圖，要取出MAP值與EmmisiveAlpha相乘一起做強度計算。
		return tex2D(EmmisiveMapSampler, texcoord).r * EmmisiveAlpha;
	}
	else
	{
		//如果沒用圖設定，自發光強度是全由EmmisiveAlpha控制，所以越不自發光要越跟光做計算
		return EmmisiveAlpha;
	}
}
