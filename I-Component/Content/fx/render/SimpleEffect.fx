#include "Simpledefine.inc"
#include "Maps.inc"
#include "NormalMap.inc"
#include "SimpleLights.inc"
#include "SimpleFog.inc"
#include "Emmisive.inc"


float4x4 WorldMatrix : WORLD;
float4x4 ViewMatrix : VIEW;
float4x4 ProjectionMatrix : PROJECTION;
float3 ViewPosition;

Simple_VSOut SimpleMeshVS(Simple_VSIn input, float4x4 world)
{
	Simple_VSOut Out;

	// Transform the models verticies and normal
    float4 PWorld = mul(input.Position, world);
    
    //PWorld.xyz = WindWaveHeight(input.Position.y, PWorld, 1, WindSpeed);
    
    float4 PWorldView =  mul(PWorld, ViewMatrix);

    //final data....
    Out.WVPPosition = mul(PWorldView, ProjectionMatrix);

	//計算貼圖軸位移
	Out.TexCoord= TexCoordinateMove(input.TexCoord);

    Out.WorldNormal =  normalize(mul(input.Normal, world));
    Out.WorldPos = PWorld;
    Out.WorldViewPos = PWorldView;

	return Out;
}

Simple_VSOut SimpleMeshVS(Simple_VSIn input)
{
    Simple_VSOut Output;
	// Transform the models verticies and normal
    Output = SimpleMeshVS(input, WorldMatrix);
    return Output;
}

NormalMap_VSOut NormalMapVS(Simple_VSIn input,
															float3 binormal : BINORMAL0,
															float3 tangent   : TANGENT0)
{
    Simple_VSOut simpleOut = SimpleMeshVS(input, WorldMatrix);

   	NormalMap_VSOut output;
   	
    // calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors.  the pixel shader will normalize these
    // in case the world matrix has scaling.
    output.tangentToWorld[0] = mul(tangent,    WorldMatrix);
    output.tangentToWorld[1] = mul(binormal,    WorldMatrix);
    output.tangentToWorld[2] = simpleOut.WorldNormal;

	output.WVPPosition = simpleOut.WVPPosition;
	output.TexCoord = simpleOut.TexCoord;
	output.WorldViewPos = simpleOut.WorldViewPos;

	// Save the vertices postion in world space
	output.WorldPos = simpleOut.WorldPos;  //mul(input.Position, World);

	return output;
}


float3 SimplePS_Lighting(float3 WorldN, 
							float3 WorldPos,
							float3 TextureColor,							
							float2 TexCoord)
{
	float3 rV = normalize(ViewPosition - WorldPos);
	float3 RV = reflect(-rV, WorldN);//改用視野觀點去算折射
	/*
	// Approximate a Fresnel coefficient for the environment map.
	// This makes the surface less reflective when you are looking
	// straight at it, and more reflective when it is viewed edge-on.
	float fFacing  = 1.0 - max( dot( rV, worldN ), 0 );
	float fresnelTerm =// fFresnelBias + ( 1.0 - fFresnelBias ) * 
																							  pow( fFacing, FresnelPower);
																							  
	TextureColor.rgb = CalculateEnvCubeMap(fresnelTerm, TextureColor.rgb, RV, reflectIntensity);
	TextureColor.rgb = CalculateReflectColor(ProjectionMatrix, TextureColor.rgb, WorldPos, worldN,  fresnelTerm, reflectIntensity);*/
	
	TextureColor = CalculateDirectionLight(WorldN, WorldPos, TextureColor, RV);
	return TextureColor;
}

//計算平行光與霧，以後增加，吃環境貼圖，折射圖，EMMISIVE，shadow map
float4 SimpleMeshPS_DLight(float WorldViewPosZ, float3 WorldN, float3 WorldPos, float2 TexCoord, float4 TextureColor)
{
    float fogFactor = ComputeFogFactor(abs(WorldViewPosZ));
	if(bLightEnable)
	{
		TextureColor.rgb = SimplePS_Lighting(WorldN, WorldPos, TextureColor.rgb, TexCoord);					
	}

	TextureColor.rgb = lerp( TextureColor.rgb, FogColor, fogFactor);
	TextureColor.a *=  ObjectAlpha;
	return TextureColor;
}

float4 SimpleMeshPS(Simple_PSIn input) : COLOR
{
	float4 color = GetNowTextureColor(input.TexCoord);
	clip(  color.a - AlphaTestReference);
	
	return SimpleMeshPS_DLight(input.WorldViewPos.z, input.WorldNormal, input.WorldPos, input.TexCoord, color);
}

float4 NormalMapPS(NormalMap_VSOut input) : COLOR
{
	float4 color = GetNowTextureColor(input.TexCoord);
	clip(  color.a - AlphaTestReference);
	
	// look up the normal from the normal map, and transform from tangent space
	// into world space using the matrix created above.  normalize the result
	// in case the matrix contains scaling.
	float3 normalFromMap = tex2D(NormalMapSampler, input.TexCoord);
	normalFromMap = mul(normalFromMap, input.tangentToWorld);
	normalFromMap = normalize(normalFromMap.rgb);
	
	return SimpleMeshPS_DLight(input.WorldViewPos.z, normalFromMap, input.WorldPos, input.TexCoord, color);
}


technique SimpleMesh
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 SimpleMeshVS();
        PixelShader = compile ps_3_0 SimpleMeshPS();
    }
}

technique SimpleMesh_TengentMesh
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 NormalMapVS();
        PixelShader = compile ps_3_0 NormalMapPS();
    }
}




