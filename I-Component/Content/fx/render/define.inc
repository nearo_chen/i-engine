#ifndef  _define_inc_
#define _define_inc_

struct Simple_VSIn
{
    float4 Position : POSITION0;
    float3 Normal   : NORMAL0;
    float2 TexCoord : TEXCOORD0;
};

struct Simple_VSOut
{
	float4 WVPPosition : POSITION0;
	float2 TexCoord : TEXCOORD0;
    float3 WorldNormal   : TEXCOORD1;  
    float3 WorldViewPos : TEXCOORD2; 
    float3 WorldPos : TEXCOORD3;
};	

struct Simple_PSIn
{
    float2 TexCoord : TEXCOORD0;    
    float3 WorldNormal   : TEXCOORD1;  
    float3 WorldViewPos : TEXCOORD2; 
    float3 WorldPos : TEXCOORD3;   
};




//-----------------------------------------------------------------------------
// Pixel shader output structure
//-----------------------------------------------------------------------------
struct MULTIPS_OUTPUT2
{
    // The pixel shader can output 2+ values simulatanously if 
    // d3dcaps.NumSimultaneousRTs > 1
    
    float4 RGBColor1 : COLOR0;  // Pixel color    
    float4 RGBColor2 : COLOR1; 
};

struct MULTIPS_OUTPUT3
{
    // The pixel shader can output 2+ values simulatanously if 
    // d3dcaps.NumSimultaneousRTs > 1
    
    float4 RGBColor1 : COLOR0;  // Pixel color    
    float4 RGBColor2 : COLOR1; 
    float4 RGBColor3 : COLOR2; 
};

struct MULTIPS_OUTPUT4
{
    // The pixel shader can output 2+ values simulatanously if 
    // d3dcaps.NumSimultaneousRTs > 1
    
    float4 RGBColor1 : COLOR0;  // Pixel color    
    float4 RGBColor2 : COLOR1; 
    float4 RGBColor3 : COLOR2; 
    float4 RGBColor4 : COLOR3; 
};


struct DeferredVSOut
{  
	float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;  
	float3 Normal : TEXCOORD1;   
	float2 Depth : TEXCOORD2;
};

// NOTE: even though the tangentToWorld matrix is only marked 
// with TEXCOORD1, it will actually take TEXCOORD1, 2, and 3.
struct NormalMap_DeferredVSOut
{
	float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;  
    float3x3 tangentToWorld    : TEXCOORD1;
    float2 Depth : TEXCOORD4;
};


struct DeferredPSOut
{
    // The pixel shader can output 2+ values simulatanously if 
    // d3dcaps.NumSimultaneousRTs > 1
    
    float4 RGBColor1 : COLOR0;  // Pixel color    
    float4 RGBColor2 : COLOR1; 
    float4 Depth : COLOR2; 
    float4 RGBColor4 : COLOR3; 
};
#endif