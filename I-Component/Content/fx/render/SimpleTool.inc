

bool CheckInFrustum(float3 worldPosition, float4x4 ViewProjection)
{
	// Find the position of this pixel in light space
    float4 lightingPosition = mul( float4(worldPosition, 1), ViewProjection);
    
    // Find the position in the shadow map for this pixel
    float2 ShadowTexCoord = lightingPosition.xy * 0.5f / lightingPosition.w + float2( 0.5, 0.5 );
    ShadowTexCoord.y = 1.0f - ShadowTexCoord.y;
    
    bool bbb=false;
   if(ShadowTexCoord.x>0 && ShadowTexCoord.x <1 && ShadowTexCoord.y>0 && ShadowTexCoord.y <1)
	   bbb = true;
	   
   return bbb;
} 

//判斷傳入的點是否位於frustum裡面，回傳深度值，-1為不受影子影響。
float ConsiderInFrustum(float3 worldPosition, float4x4 ViewProjection, sampler2D depthMapSampler, float depthBias)
{
	// Find the position of this pixel in light space
    float4 lightingPosition = mul( float4(worldPosition, 1), ViewProjection);
    
    // Find the position in the shadow map for this pixel
   // float2 ShadowTexCoord = (lightingPosition.xy / lightingPosition.w + 1)/2;
    float2 ShadowTexCoord = lightingPosition.xy * 0.5f / lightingPosition.w + float2( 0.5, 0.5 );
    ShadowTexCoord.y = 1.0f - ShadowTexCoord.y;
    
    float depth=-1;
    //bInShadowFrustum = false;
    if(max(ShadowTexCoord.x, ShadowTexCoord.y)<1 &&  min(ShadowTexCoord.x, ShadowTexCoord.y)>0) 
  // if(ShadowTexCoord.x>0 && ShadowTexCoord.x <1 && ShadowTexCoord.y>0 && ShadowTexCoord.y <1)
	{		
		// Get the current depth stored in the shadow map
		float shadowdepth = tex2D(depthMapSampler, ShadowTexCoord).r;

		// Calculate the current pixel depth
		// The bias is used to prevent folating point errors that occur when
		// the pixel of the occluder is being drawn
		depth = lightingPosition.z/lightingPosition.w - shadowdepth - depthBias;
		
		//bInShadowFrustum = true;
    }
    
    return depth;  
}

struct DepthSampleData
{
	bool IfInFrustum;//是否位於MAP的frustum內
	float depthRatio;//0~1的受深度影響比率，0為完全影響，1為完全沒影響。
};

//由取樣為中心往周圍8個點取樣，回傳0~1的受深度影響比率，0為完全影響，1為完全沒影響。
DepthSampleData ConsiderInFrustum_Kernel9(float3 worldPosition, float4x4 ViewProjection, sampler2D depthMapSampler, float depthBias,
													float2 fTexelSize, float sampleTime)
{
	DepthSampleData data;
	data.IfInFrustum = false;
	data.depthRatio = 1;

	// Find the position of this pixel in light space
    float4 lightingPosition = mul( float4(worldPosition, 1), ViewProjection);
    
    // Find the position in the shadow map for this pixel 
    float2 kernelTex = lightingPosition.xy * 0.5f / lightingPosition.w + float2( 0.5, 0.5 );
    kernelTex.y = 1.0f - kernelTex.y;
    
   
	float fShadowTerm = sampleTime;//9.0f;
	if(max(kernelTex.x, kernelTex.y)<1 &&  min(kernelTex.x, kernelTex.y)>0)
	{
			data.IfInFrustum = true;
	
			fShadowTerm =0;
			
			// 建立kernel，Generate the 9 texture co-ordinates for a 3x3 PCF kernel
			float2 vTexCoords[9];
			// Generate the tecture co-ordinates for the specified depth-map size
			// 4 3 5
			// 1 0 2
			// 7 6 8
			vTexCoords[0] = kernelTex;
			vTexCoords[1] = kernelTex + float2( -fTexelSize.x, 0.0f );
			vTexCoords[2] = kernelTex + float2(  fTexelSize.x, 0.0f );
			vTexCoords[3] = kernelTex + float2( 0.0f, -fTexelSize.y );
			vTexCoords[6] = kernelTex + float2( 0.0f,  fTexelSize.y);
			vTexCoords[4] = kernelTex + float2( -fTexelSize.x, -fTexelSize.y );
			vTexCoords[5] = kernelTex + float2(  fTexelSize.x, -fTexelSize.y);
			vTexCoords[7] = kernelTex + float2( -fTexelSize.x,  fTexelSize.y);
			vTexCoords[8] = kernelTex + float2(  fTexelSize.x,  fTexelSize.y);
			
     
			float B  = lightingPosition.z/lightingPosition.w - depthBias;
		    
			// Sample each of them checking whether the pixel under test is shadowed or not
				 
		   for( int i = 0; i < sampleTime; i++ )
		   {
			  float A = tex2D(depthMapSampler, vTexCoords[i]).r;
			  
			  // Texel is shadowed		
			  if(A<B)//影子取樣點小於座標點表示受影子影響+0變黑
				  fShadowTerm +=  0;
			  else
				  fShadowTerm +=1;
		   }
	
   }
   
   // Get the average
   fShadowTerm /= sampleTime ;//9.0f;
   
   data.depthRatio = fShadowTerm;
 
   return data;
}

