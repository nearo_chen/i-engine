
float AlphaTestReference = 0;

float4 BlendColor = {1, 1, 1, 1}; //設定此物件的混色 diffuseColor = lerp(BlendColor, diffuseColor, diffuseColor.a * ObjectAlpha)

//deferred shading不做半透明，所以ObjectAlpha沒屁用。
float ObjectAlpha = 1.0f; //設定此物件的alpha整體程度，但要配合alpha blending使用

float TotalEllapsedMilliseonds =0; //1代表1秒

//#define NO_TEX_LOD
//bool bMipmapEnable = false;
bool bTexture=true;

float2 TranslateUVSinLoopSpeed = 0;
float2 TranslateUVSpeed = 0;
float3 centerScaleUV = {0, 0, 0};

texture2D Texture;
sampler2D textureSampler = sampler_state
{
	Texture = (Texture);
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;   
	AddressU  = wrap;
	AddressV  = wrap;
};

float2 TexCoordinateMove(float2 texCoord)
{
	//計算水平位移貼圖軸sin位移
    texCoord.x += sin(TotalEllapsedMilliseonds * TranslateUVSinLoopSpeed.x * 1.570795f);
	texCoord.y += sin(TotalEllapsedMilliseonds * TranslateUVSinLoopSpeed.y * 1.570795f);
	
	//計算水平位移貼圖軸線性位移
	texCoord += TotalEllapsedMilliseonds * TranslateUVSpeed * 1.570795f;	
  
	//計算以中心點縮放的貼圖軸
	float2 CenterScale = 1 + centerScaleUV.xy * 0.5 * sin(TotalEllapsedMilliseonds * (1+centerScaleUV.z) * 1.570795f);
	CenterScale = (texCoord - 0.5) / CenterScale;
	texCoord = CenterScale + 0.5; //換算放大後回來倍率要是1
	return texCoord;
}



float NextTexturePercent = -1;
texture2D NextTexture;
sampler2D NextTextureSampler = sampler_state
{
	Texture = (NextTexture);
	MinFilter = Linear;
	MagFilter = Linear;
	MipFilter = Linear;
	AddressU  = wrap;
	AddressV  = wrap;
};

//-----------------------------------------------------------------------------
// Calculate dynamic texture fading in&out.........
//-----------------------------------------------------------------------------
float4 ConsiderChangeTexture(float4 diffuseColor, float2 coord)
{    
	//if(NextTexturePercent >0)//省下取樣的效率，但多了一個if
	//{
	//	float4 nextColor = tex2D(NextTextureSampler, coord);
	//	diffuseColor = diffuseColor * (1-NextTexturePercent) + nextColor * NextTexturePercent;
	//}
	
	float4 nextColor = tex2D(NextTextureSampler, coord);
	return lerp(diffuseColor, nextColor, NextTexturePercent);
}


float4 GetNowTextureColor(float2 inputTexCoord)
{
	float4 diffuseColor = 1;
    if(bTexture)
   {
		/*if(bMipmapEnable == false)
		{
			float4 texCoord = float4(inputTexCoord, 0 , 0);
			diffuseColor = tex2Dlod(textureSampler, texCoord);
			//diffuseColor = 1;//tex2D(textureSampler, inputTexCoord);
		}
		else
		{		
			diffuseColor = tex2D(textureSampler, inputTexCoord);
		}*/
		
		diffuseColor = tex2D(textureSampler, inputTexCoord);
		
		diffuseColor = ConsiderChangeTexture(diffuseColor, inputTexCoord);
		
		//deferred shading不做半透明，所以ObjectAlpha只能拿來做BlendColor權重計算用。
		diffuseColor = lerp(BlendColor, diffuseColor, diffuseColor.a * ObjectAlpha);//貼圖透空越吃BlendColor，越不透明越吃diffuseColor
	}
	
	return diffuseColor;
}

//不判斷tex2Dlod的textureSampler
/*float4 GetNowTextureColor_Simple(float2 inputTexCoord)
{
	float4 diffuseColor = 1;
    if(bTexture)
   {	
		diffuseColor = tex2D(textureSampler, inputTexCoord);
		diffuseColor = ConsiderChangeTexture(diffuseColor, inputTexCoord);
	}
	return diffuseColor;
}*/


