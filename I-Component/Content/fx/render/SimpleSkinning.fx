#include "SkinningDefine.inc"
#include "SimpleEffect.fx"


Simple_VSOut SimpleSkinningVS(  Simple_VSIn input ,
														float4 BoneIndices : BLENDINDICES0,
														float4 BoneWeights : BLENDWEIGHT0)
{
    // Blend between the weighted bone matrices.
    float4x4 skinTransform = GetBoneTransform(WorldMatrix, BoneIndices, BoneWeights);
    return SimpleMeshVS(input, skinTransform);
}


NormalMap_VSOut NormalMap_SimpleSkinningVS(  Simple_VSIn input ,
														float4 BoneIndices : BLENDINDICES0,
														float4 BoneWeights : BLENDWEIGHT0,
														float3 binormal : BINORMAL0,
														float3 tangent   : TANGENT0)
{
	// Blend between the weighted bone matrices.
    float4x4 skinTransform = GetBoneTransform(WorldMatrix, BoneIndices, BoneWeights);
    Simple_VSOut simpleOut = SimpleMeshVS(input, skinTransform);

   	NormalMap_VSOut output;
   	
   	// calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors.  the pixel shader will normalize these
    // in case the world matrix has scaling.
    output.tangentToWorld[0] = mul(tangent,    skinTransform);
    output.tangentToWorld[1] = mul(binormal,    skinTransform);
    output.tangentToWorld[2] = simpleOut.WorldNormal;

	output.WVPPosition = simpleOut.WVPPosition;
    output.TexCoord = simpleOut.TexCoord;
    output.WorldViewPos = simpleOut.WorldViewPos;

	// Save the vertices postion in world space
	output.WorldPos = simpleOut.WorldPos;
	
	return output;
}

technique SkinningMesh
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 SimpleSkinningVS();
        PixelShader = compile ps_3_0 SimpleMeshPS();
    }
}


technique SkinningMesh_TengentMesh
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 NormalMap_SimpleSkinningVS();
        PixelShader = compile ps_3_0 SimpleMeshPS();
    }
}