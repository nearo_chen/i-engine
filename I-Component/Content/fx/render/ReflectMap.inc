


//環境圖和反射圖共用的強度參數
float m_ReflectIntensity;//沒有ReflectIntensityMap就以此數值為準
bool bReflectPowerMap = false;
texture2D ReflectIntensityMap;
sampler2D ReflectIntensityMapSampler = sampler_state
{
	Texture = (ReflectIntensityMap);
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = wrap;
	AddressV  = wrap;
};

float GetReflectMapColor(float2 texCoord)
{
	if(bReflectPowerMap)
		return tex2D(ReflectIntensityMapSampler, texCoord).r;
	else
		return 1;		
}
