#include "NormalMap.inc"

float4x4 WorldMatrix : WORLD;
float4x4 ViewMatrix : VIEW; //傳一次就可
float4x4 WVPMatrix;

float RefractMapScale = 0;

struct RefractVSIn
{
    float4 Position : POSITION0;
    float3 Normal   : NORMAL0;
    float2 TexCoord : TEXCOORD0;
};

struct RefractVSOut
{
	float4 Position : POSITION;
    float2 TexCoord : TEXCOORD0;
	float3 WorldNormal   : TEXCOORD1;  
};

struct RefractNormalMapVSOut
{
	float4 Position : POSITION;
    float2 TexCoord : TEXCOORD0;

    float3x3 tangentToWorld    : TEXCOORD2;
};


RefractVSOut CreateRefractMapVS(RefractVSIn input)
{
	RefractVSOut Out;
    Out.Position =  mul(input.Position, WVPMatrix); 
	Out.TexCoord =  input.TexCoord;
   
    Out.WorldNormal =  normalize(mul(input.Normal, WorldMatrix));
    
    return Out;
}

RefractNormalMapVSOut CreateRefractNormalMapVS(RefractVSIn input,
															float3 binormal : BINORMAL0,
															float3 tangent   : TANGENT0)
{    
    RefractVSOut simpleOut = CreateRefractMapVS(input);

   	RefractNormalMapVSOut output;
   	
    // calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors.  the pixel shader will normalize these
    // in case the world matrix has scaling.
    output.tangentToWorld[0] = mul(tangent,    WorldMatrix);
    output.tangentToWorld[1] = mul(binormal,    WorldMatrix);
    output.tangentToWorld[2] = simpleOut.WorldNormal;

	output.TexCoord = simpleOut.TexCoord;
	output.Position = simpleOut.Position;

	return output;
}

float2 RefractScreenUV(float3 WorldViewNormal)
{
   WorldViewNormal.y = -WorldViewNormal.y;
   
   float amount = dot(WorldViewNormal, float3(0,0,1)) * RefractMapScale;
   return float2(.5,.5) + float2(amount * WorldViewNormal.xy);
}

float4 SimpleMesh_RefractMapPS(RefractVSOut input): COLOR
{
	input.WorldNormal = mul(input.WorldNormal, ViewMatrix);
	return float4(RefractScreenUV(input.WorldNormal), 0, 1);
}

float4 Normalmap_RefractMapPS(RefractNormalMapVSOut input) : COLOR
{
    float3 normalFromMap = tex2D(NormalMapSampler, input.TexCoord);
		normalFromMap = mul(normalFromMap, input.tangentToWorld);
		normalFromMap = normalize(normalFromMap);
	normalFromMap = mul(normalFromMap, ViewMatrix);
	return float4(RefractScreenUV(normalFromMap), 0, 1);
}


technique SimpleMesh_NormalRefractMap
{
    pass P0
    {
        vertexShader = compile vs_2_0 CreateRefractMapVS();
        pixelShader  = compile ps_3_0 SimpleMesh_RefractMapPS();       
    }  
}

technique SimpleMesh_TengentMesh_NormalRefractMap
{
    pass P0
    {
        vertexShader = compile vs_2_0 CreateRefractNormalMapVS();
        pixelShader  = compile ps_3_0 Normalmap_RefractMapPS();       
    }  
}
