

float WindSpeed = 4; //影響時間的速度

float3 WindDirection = float3(1, 0, 0); //world的風向
float WindWaveSize = 0.1; //sin波的擾動範圍大小，越大波越密越明顯，越小波越寬越稀疏。改這個就可以看得出差別了CreateVegetationGeometry("grass.tga",5, 5, 10, identity);
float WindAmount = 0; //影響到震幅大小，應為是草所以振幅只影響左右UV(Local 的XZ)軸
float TreeHeight = 100;//樹的身高

float3 WindWaveHeight(float localY,  float3 WorldPosition, float Random, float WindSpeed)
{
	//0就不搖了
	if(WindAmount != 0)
	{
		//風搖動**********************************************************************
		// Work out how this vertex should be affected by the wind effect.
		float waveOffset = dot(WorldPosition, WindDirection) * WindWaveSize;
	    
		waveOffset += Random;
	    
		// Wind makes things wave back and forth in a sine wave pattern.
		float wind = sin(TotalEllapsedMilliseonds * WindSpeed + waveOffset) * WindAmount;
	    
		// But it should only affect the top two vertices of the billboard!
		//wind *= (1 - input.TexCoord.y);
		wind *= localY / TreeHeight;
	    
		WorldPosition += WindDirection * wind;
    }
    
    return WorldPosition;
	//風搖動**********************************************************************
}

float3 WindWaveCoordV(float3 WorldPosition, float Random, float coordV,  float WindSpeed)
{
	//風搖動**********************************************************************
    // Work out how this vertex should be affected by the wind effect.
    float waveOffset = dot(WorldPosition, WindDirection) * WindWaveSize;
    
    waveOffset += Random;
    
    // Wind makes things wave back and forth in a sine wave pattern.
    float wind = sin(TotalEllapsedMilliseonds * WindSpeed + waveOffset) * WindAmount;
    
    // But it should only affect the top two vertices of the billboard!
    wind *= (1 - coordV);
    //wind *= 1- (max(0, (WorldPosition.y - RootHeight)) / TreeHeight);
    
    WorldPosition += WindDirection * wind;
    return WorldPosition;
	//風搖動**********************************************************************
}