bool bFogEnable = true;
float FogEnd = 100000;
float FogStart = 0;
float FogMaxRatio = 1;
float3 FogColor = 1;


//-----------------------------------------------------------------------------
// Compute fog factor 目前衰減為線性
//-----------------------------------------------------------------------------
float ComputeFogFactor(float d)
{
    return 
    clamp((d - FogStart) / (FogEnd - FogStart), 0, 1)  *  bFogEnable * FogMaxRatio;
}
