

bool bSpecularMap=false;
texture2D SpecularMap;
sampler2D SpecularMapSampler = sampler_state
{
	Texture = (SpecularMap);
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = wrap;
	AddressV  = wrap;
};


float ConsiderObjectSpecularMapColor(float2 texcoord)
{	
	if(bSpecularMap)
		return tex2D(SpecularMapSampler, texcoord).r;
	else
		return 1;
}