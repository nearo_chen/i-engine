#ifndef  _DeferredDefine_inc_
#define _DeferredDefine_inc_

struct Simple_VSIn
{
    float4 Position : POSITION0;
    float3 Normal   : NORMAL0;
    float2 TexCoord : TEXCOORD0;
};

struct DeferredVSOut
{  
	float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;  
	float3 Normal : TEXCOORD1;   
	float2 Depth : TEXCOORD2;
};

// NOTE: even though the tangentToWorld matrix is only marked 
// with TEXCOORD1, it will actually take TEXCOORD1, 2, and 3.
struct NormalMap_DeferredVSOut
{
	float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;  
    float3x3 tangentToWorld    : TEXCOORD1;
    float2 Depth : TEXCOORD4;
};



struct DeferredPSOut
{
    // The pixel shader can output 2+ values simulatanously if 
    // d3dcaps.NumSimultaneousRTs > 1
    
    float4 RGBColor1 : COLOR0;  // Pixel color    
    float4 RGBColor2 : COLOR1; 
    float4 Depth : COLOR2; 
    float4 RGBColor4 : COLOR3; 
};
#endif