
bool bLightEnable=false;
float3 DLightDirection;
float3 DLight_a = { 1.0f, 1.0f, 1.0f};    // ambient
float3 DLight_d = { 1.0f, 1.0f, 1.0f};    // diffuse
float3 DLight_s = { 1.0f, 1.0f, 1.0f};     // specular

// material reflectivity
float3 Material_a = { 0.8f, 0.8f, 0.8f };    // ambient
float3 Material_d = { 0.5f, 0.5f, 0.5f};    // diffuse
float3 Material_s = {0.0f, 0.0f, 0.0f};    // specular
int MaterialSPower = 16;


float3 CalculateDirectionLight(float3 WorldN,
											  float3 WorldPos,
											  float3 TextureColor,
											  float3 RV)
{
	float dotDiffuse = dot(WorldN, -DLightDirection);
	float3 diffuse = max(0 , dotDiffuse) * DLight_d * Material_d + DLight_a * Material_a; 
	
	float3 S = pow( saturate( dot( RV, -DLightDirection) ) , MaterialSPower);
	return (TextureColor.rgb *  diffuse) + (DLight_s * MaterialSPower * S);
}