//static shared bool bInShadowFrustum;

#include "Maps.inc"

float4x4 WorldMatrix;
float4x4 DepthMapFrustum;

struct Simple_VSIn
{
    float4 Position : POSITION0;
    float3 Normal   : NORMAL0;
    float2 TexCoord : TEXCOORD0;
};


struct DepthMap_VSOut
{
    float4 Position : POSITION;
    float2 TexCoord : TEXCOORD0;
    float2 Depth     : TEXCOORD1;
};

DepthMap_VSOut CreateDepthMapVS(Simple_VSIn input, float4x4 world)
{
	DepthMap_VSOut Out;
    Out.Position =  mul(input.Position, world);
    Out.Position = mul(Out.Position, DepthMapFrustum);

    Out.Depth.x = Out.Position.z;
    Out.Depth.y = Out.Position.w;
    Out.TexCoord =  input.TexCoord;
    return Out; 
}


// Transforms the model into light space an renders out the depth of the object
DepthMap_VSOut CreateDepthMap_VertexShader(Simple_VSIn input)
{
    return CreateDepthMapVS(input, WorldMatrix);
}

// Saves the depth value out to the 32bit floating point texture
float4 CreateDepthMap_PixelShader(DepthMap_VSOut input) : COLOR
{ 
    //float4 diffuseColor = tex2D(textureSampler, input.TexCoord);//只用簡單的材質取樣 //GetNowTextureColor(input.TexCoord);
    //clip(  diffuseColor.a - AlphaTestReference);//0.5f  );
    
    float depth = input.Depth.x / input.Depth.y;
    return float4(depth, 0, 0, 0);
}





// Technique for creating the shadow map
technique SimpleMesh_DepthMap
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 CreateDepthMap_VertexShader();
        PixelShader = compile ps_3_0 CreateDepthMap_PixelShader();
    }
}
//
//// Technique for creating the shadow map
//technique SimpleMesh_TengentMesh_DepthMap
//{
    //pass Pass1
    //{
        //VertexShader = compile vs_2_0 CreateDepthMap_VertexShader();
        //PixelShader = compile ps_3_0 CreateDepthMap_PixelShader();
    //}
//}