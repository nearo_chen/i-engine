
#include "DeferredEffect.fx"
#include "SkinningDefine.inc"





DeferredVSOut DeferredSkinningTransform(  Simple_VSIn input ,
														float4 BoneIndices : BLENDINDICES0,
														float4 BoneWeights : BLENDWEIGHT0)
{
    // Blend between the weighted bone matrices.
    float4x4 skinTransform = GetBoneTransform(WorldMatrix, BoneIndices, BoneWeights);
    return DeferredVS(input, skinTransform);
}

NormalMap_DeferredVSOut NormalMap_DeferredSkinningTransform(  Simple_VSIn input ,
														float4 BoneIndices : BLENDINDICES0,
														float4 BoneWeights : BLENDWEIGHT0,
														float3 binormal : BINORMAL0,
														float3 tangent   : TANGENT0)
{
	// Blend between the weighted bone matrices.
    float4x4 skinTransform = GetBoneTransform(WorldMatrix, BoneIndices, BoneWeights);
    DeferredVSOut simpleOut = DeferredVS(input, skinTransform);

   	NormalMap_DeferredVSOut output;
   	
   	// calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors.  the pixel shader will normalize these
    // in case the world matrix has scaling.
    output.tangentToWorld[0] = mul(tangent,    skinTransform);
    output.tangentToWorld[1] = mul(binormal,    skinTransform);
    output.tangentToWorld[2] = simpleOut.Normal;

	output.Position = simpleOut.Position;
    output.TexCoord = simpleOut.TexCoord;
    output.Depth = simpleOut.Depth;
	return output;
}

technique SkinningMesh_Deferred
{
    pass P0
    {
        vertexShader = compile vs_2_0 DeferredSkinningTransform();
        pixelShader  = compile ps_2_0 DeferredPS();
    }
}

technique SkinningMesh_TengentMesh_Deferred
{
    pass P0
    {
        vertexShader = compile vs_2_0 NormalMap_DeferredSkinningTransform();
        pixelShader  = compile ps_2_0 NormalMap_DeferredPS();
    }
}