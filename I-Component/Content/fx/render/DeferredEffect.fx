#include "DeferredDefine.inc"
#include "Maps.inc"
#include "SpecularMaterial.inc"
#include "Emmisive.inc"
#include "NormalMap.inc"
#include "ReflectMap.inc"
#include "WindWave.inc"

//通用的
float4x4 WorldMatrix : WORLD;
float4x4 ViewMatrix : VIEW;
float4x4 ProjectionMatrix : PROJECTION;

float MaterialBankID = 0; //deferred shading中使用的材質庫ID ，0為預設值 0~1之間做ID縮放

//====================================================================================================
//																														Deferred shading
//====================================================================================================

DeferredVSOut DeferredVS (Simple_VSIn input, float4x4 world)
{
	DeferredVSOut output;

	float4 worldPosition = mul(input.Position, world);

	worldPosition.xyz = WindWaveHeight(input.Position.y, worldPosition.xyz, 1, WindSpeed);

	float4 viewPosition = mul(worldPosition, ViewMatrix);
	output.Position = mul(viewPosition, ProjectionMatrix);

	//pass the texture coordinates further
	output.TexCoord =TexCoordinateMove(input.TexCoord);// input.TexCoord;

    //get normal into world space
	output.Normal = mul(input.Normal, world);

	output.Depth.x = output.Position.z;
	output.Depth.y = output.Position.w;
	return output;
}

DeferredVSOut DeferredVS (Simple_VSIn input)
{
	return DeferredVS (input, WorldMatrix);
}

NormalMap_DeferredVSOut NormalMap_DeferredVS(Simple_VSIn input, float3 binormal : BINORMAL0, float3 tangent   : TANGENT0)
{
	DeferredVSOut simpleOut = DeferredVS(input, WorldMatrix);
	NormalMap_DeferredVSOut output;

	 // calculate tangent space to world space matrix using the world space tangent,
    // binormal, and normal as basis vectors.  the pixel shader will normalize these
    // in case the world matrix has scaling.
    output.tangentToWorld[0] = mul(tangent,    WorldMatrix);
    output.tangentToWorld[1] = mul(binormal,    WorldMatrix);
    output.tangentToWorld[2] = simpleOut.Normal;

    output.Position = simpleOut.Position;
    output.TexCoord = simpleOut.TexCoord;
    output.Depth = simpleOut.Depth;
    return output;
}


DeferredPSOut DeferredPS(DeferredVSOut input)
{	
	float4 color = GetNowTextureColor(input.TexCoord);
	clip(  color.a - AlphaTestReference);//0.5f  );
	
	DeferredPSOut output;
	output.RGBColor1.rgb = color.rgb;
	output.RGBColor1.a =  1;
	
	output.RGBColor2.rgb = 0.5f * (input.Normal + 1.0f);	
	output.RGBColor2.a = MaterialBankID;
	
	output.Depth =0;
	output.Depth.r = input.Depth.x / input.Depth.y;	
	
	output.RGBColor4.r = ConsiderObjectSpecularMapColor(input.TexCoord);
	output.RGBColor4.g = GetReflectMapColor(input.TexCoord);
	output.RGBColor4.b = GetEmmisiveMapValue(input.TexCoord);
	output.RGBColor4.a = 0;
	return output;
	
}


DeferredPSOut NormalMap_DeferredPS(NormalMap_DeferredVSOut input)
{
	float4 color = GetNowTextureColor(input.TexCoord);
	clip(  color.a - AlphaTestReference);//0.5f  );

	// look up the normal from the normal map, and transform from tangent space
	// into world space using the matrix created above.  normalize the result
	// in case the matrix contains scaling.
	float3 normalFromMap = tex2D(NormalMapSampler, input.TexCoord);
	normalFromMap = mul(normalFromMap, input.tangentToWorld);
	normalFromMap = normalize(normalFromMap.rgb);
	
	DeferredPSOut output;
	output.RGBColor1.rgb = color.rgb;
	output.RGBColor1.a = 1;
	
	output.RGBColor2.rgb = 0.5f * (normalFromMap + 1.0f);	
	output.RGBColor2.a = MaterialBankID;	
	
	output.Depth = 0;
	output.Depth.r = input.Depth.x / input.Depth.y;	
	
	output.RGBColor4.r = ConsiderObjectSpecularMapColor(input.TexCoord);
	output.RGBColor4.g = GetReflectMapColor(input.TexCoord);
	output.RGBColor4.b = GetEmmisiveMapValue(input.TexCoord);
	output.RGBColor4.a = 0;
	return output;
}





technique SimpleMesh_Deferred
{
    pass P0
    {
        vertexShader = compile vs_2_0 DeferredVS();
        pixelShader  = compile ps_3_0 DeferredPS();
    }
}

technique SimpleMesh_TengentMesh_Deferred
{
    pass P0
    {
        vertexShader = compile vs_2_0 NormalMap_DeferredVS();
        pixelShader  = compile ps_3_0 NormalMap_DeferredPS();
    }
}