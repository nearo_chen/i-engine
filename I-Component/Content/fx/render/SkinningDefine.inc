// Maximum number of bone matrices we can render using shader 2.0 in a single pass.
// If you change this, update SkinnedModelProcessor.cs to match.
//#define MaxBones 50

float4x4 BoneArray[50];

float4x4 GetBoneTransform(float4x4 World, float4 BoneIndices, float4 BoneWeights)
{
	float4x4 skinTransform= BoneArray[BoneIndices.x] * BoneWeights.x;
	skinTransform += BoneArray[BoneIndices.y] * BoneWeights.y;
	skinTransform += BoneArray[BoneIndices.z] * BoneWeights.z;
	skinTransform += BoneArray[BoneIndices.w] * BoneWeights.w;
	skinTransform = mul(World, skinTransform);
	return skinTransform;
}