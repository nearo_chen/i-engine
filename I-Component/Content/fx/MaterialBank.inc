#define MaxMaterialBankSize 11
  
struct Material
{
	float4 AmbientReflectIntensity;
	float3 Diffuse;
	float4 SpecularSpower;
//	float Spower;

//	float reflectIntensity;
//	float refractScale;
};

Material MaterialBank[MaxMaterialBankSize];