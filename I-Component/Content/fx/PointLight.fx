#include "MaterialBank.inc"

//position of the camera, for specular light
float3 cameraPosition;
//this is used to compute the world-position
float4x4 InvertViewProjection;
// diffuse color, and specularIntensity in the alpha channel
//texture colorMap;
// normals, and specularPower in the alpha channel
texture normalMap;
//depth
texture depthMap;
//other settings
//texture otherMap;

//sampler colorSampler = sampler_state
//{
    //Texture = (colorMap);
    //AddressU = CLAMP;
    //AddressV = CLAMP;
    //MagFilter = LINEAR;
    //MinFilter = LINEAR;
    //Mipfilter = LINEAR;
//};

sampler depthSampler = sampler_state
{
    Texture = (depthMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler normalSampler = sampler_state
{
    Texture = (normalMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

/*
sampler otherSampler = sampler_state
{
    Texture = (otherMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};
*/
struct VertexShaderInput
{
    float3 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

float2 halfPixel;
VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    //align texture coordinates
    output.TexCoord = input.TexCoord - halfPixel;
    return output;
}


struct PointLight 
{
    float3 color;
    float3 position;
    float decay;
    //的衰減強度
    float range;
    float strength;
    //計算後光的強度在加強的倍數
    float specularIntensity;
    //控制光的specular強度，控制地上反射的小光點強度。
};
PointLight PointLightArray[16];
int PointLightAmount = 0;

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    //↑↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓資訊取得↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    //float4 colorData = tex2D(colorSampler,input.TexCoord);    

    //get normal data from the normalMap
    float4 normalData = tex2D(normalSampler,input.TexCoord);
    //tranform normal back into [-1,1] range
    float3 normal = 2.0f * normalData.xyz - 1.0f;
    float materialID = round(normalData.a * 10.0f);
    //用round抓4捨五入
    
    Material material = MaterialBank[materialID];
    //get specular power, and get it into [0,255] range]
    float specularPower = material.SpecularSpower.a;
    // * 255;
  
    //get specular intensity from the colorMap
     float3 materialSpecular = material.SpecularSpower.rgb;
    float3 materialAmbient = material.AmbientReflectIntensity.rgb;
    float3 materialDiffuse = material.Diffuse;
    //read depth
    float depthVal = tex2D(depthSampler,input.TexCoord).r;
    //  float4 otherData = tex2D(otherSampler,input.TexCoord);
  
    //compute screen-space position
    float4 position;
    position.x = input.TexCoord.x * 2.0f - 1.0f;
    position.y = -(input.TexCoord.y * 2.0f - 1.0f);
    position.z = depthVal;
    position.w = 1.0f;
    //transform to world space
    position = mul(position, InvertViewProjection);
    position /= position.w;
    //  	return float4(position.xyz, 1);
//↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑資訊取得↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

	float4 final=0;
    for(int a=0; a<PointLightAmount ; a++)
	{
        float3 PLightDirection = position - PointLightArray[a].position;
        float lightDist = length(PLightDirection);
        PLightDirection = PLightDirection / lightDist;
        
     //   if(PointLightArray[a].range > lightDist)
	//	{
            //calculate the intensity of the light with exponential falloff
			float baseIntensity = pow
			(
					saturate(
						(PointLightArray[a].range - lightDist)
									/ 
						PointLightArray[a].range
					),	

					PointLightArray[a].decay//DECAY不能為0，不然在pow()時，0次方會變成1數字會錯
			);
			float diffuseIntensity = saturate( dot(normal, -PLightDirection));
		
			float3 diffuse = diffuseIntensity * PointLightArray[a].color * materialDiffuse;//material.diffuse;
			// * TextureColor.rgb;

			//calculate Phong components per-pixel
			float3 reflectionVector = normalize(reflect(PLightDirection, normal));
			float3 directionToCamera = normalize(cameraPosition - position);
			
			//calculate specular component
			float specular = 
			saturate
			(
					PointLightArray[a].color.r * PointLightArray[a].specularIntensity *
					materialSpecular.r * 
					pow
					(
							saturate(dot(reflectionVector, directionToCamera)), 
							specularPower
					)
			);
			final += baseIntensity * PointLightArray[a].strength * float4(diffuse, specular);	
		//}
	}
	return final;
}


technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }

}
