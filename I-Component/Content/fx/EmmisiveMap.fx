
// diffuse color, and specularIntensity in the alpha channel
texture colorMap;

//other settings
texture otherMap;
sampler otherSampler = sampler_state
{
    Texture = (otherMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
}
;

sampler colorSampler = sampler_state
{
    Texture = (colorMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    Mipfilter = LINEAR;
}
;

struct VertexShaderInput
{
    float3 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

float2 halfPixel;
VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    //align texture coordinates
    output.TexCoord = input.TexCoord - halfPixel;
    return output;
}


float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    //Get emmisive alpha value 
    float emmisiveAlpha = tex2D(otherSampler,input.TexCoord).b;
    float3 colorData = tex2D(colorSampler,input.TexCoord);    
    
    float4 EmmisiveColor = 1;
    //透空部分把它弄成黑的，越黑就越不亮，alpha就不需要了。
    EmmisiveColor.rgb = lerp(0, colorData.rgb, emmisiveAlpha);
    
	return EmmisiveColor;
}


technique Technique0
{
    pass Pass0
    {
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}