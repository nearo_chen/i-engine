//#define RENDER_PSSM_LEVEL
#include "DeferredFog.inc"
#include "render\SimpleTool.inc"

int ShowPSSMLevel=0;
float4x4 InvertViewProjection;

//float PSSMMap0DepthBias;
float4x4 PSSMMap0Frustum;
//bool bPSSMMap0=false;
//texture PSSMMap0;
//sampler PSSMMap0Sampler = sampler_state
//{
    //Texture = (PSSMMap0);
    //AddressU = CLAMP;
    //AddressV = CLAMP;
    //MagFilter = Linear;
    //MinFilter = Linear;
    //Mipfilter = Linear;
//};
//
//float PSSMMap1DepthBias;
float4x4 PSSMMap1Frustum;
//bool bPSSMMap1=false;
//texture PSSMMap1;
//sampler PSSMMap1Sampler = sampler_state
//{
    //Texture = (PSSMMap1);
    //AddressU = CLAMP;
    //AddressV = CLAMP;
    //MagFilter = Linear;
    //MinFilter = Linear;
    //Mipfilter = Linear;
//};
//
//float PSSMMap2DepthBias;
float4x4 PSSMMap2Frustum;
//bool bPSSMMap2=false;
//texture PSSMMap2;
//sampler PSSMMap2Sampler = sampler_state
//{
    //Texture = (PSSMMap2);
    //AddressU = CLAMP;
    //AddressV = CLAMP;
    //MagFilter = Linear;
    //MinFilter = Linear;
    //Mipfilter = Linear;
//};
//
//float PSSMMap3DepthBias;
float4x4 PSSMMap3Frustum;
//bool bPSSMMap3=false;
//texture PSSMMap3;
//sampler PSSMMap3Sampler = sampler_state
//{
    //Texture = (PSSMMap3);
    //AddressU = CLAMP;
    //AddressV = CLAMP;
    //MagFilter = Linear;
    //MinFilter = Linear;
    //Mipfilter = Linear;
//};

float4x4 InvertProjection;
float3 cameraPosition;

texture colorMap;
texture lightMap;

texture depthMap;
  sampler depthSampler = sampler_state
{
    Texture = (depthMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};

sampler colorSampler = sampler_state
{
    Texture = (colorMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = LINEAR;
    MinFilter = LINEAR;
    Mipfilter = LINEAR;
};
sampler lightSampler = sampler_state
{
    Texture = (lightMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    /*
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
    */
     MagFilter = LINEAR;
    MinFilter = LINEAR;
    Mipfilter = LINEAR;
};






struct VertexShaderInput
{
    float3 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

float2 halfPixel;
VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    output.TexCoord = input.TexCoord - halfPixel;
    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
//↑↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓資訊取得↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	float depthVal = tex2D(depthSampler,input.TexCoord).r;
    //  float4 otherData = tex2D(otherSampler,input.TexCoord);
  
    //compute screen-space position
    float4 positionView;//, positionWorld;
    positionView.x = input.TexCoord.x * 2.0f - 1.0f;
    positionView.y = -(input.TexCoord.y * 2.0f - 1.0f);
    positionView.z = depthVal;
    positionView.w = 1.0f;
    
    //positionWorld = positionView;
    
    //transform to view space
    positionView = mul(positionView, InvertProjection);
    positionView /= positionView.w;
    
    //positionWorld = mul(positionWorld, InvertViewProjection);
    //positionWorld /= positionWorld.w;    
//↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑資訊取得↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
	
	float vLength =abs( positionView.z);// length(cameraPosition - position);
	float fogFactor = ComputeFogFactor(vLength);

    float4 diffuseColor = tex2D(colorSampler,input.TexCoord);
    
    float4 light = tex2D(lightSampler,input.TexCoord);
    float3 diffuseLight = light.rgb;
    float specularLight = light.a;
    float4 finalColor = float4((diffuseColor * diffuseLight + specularLight), diffuseColor.a);
    
    finalColor.rgb = lerp( finalColor.rgb, FogColor, fogFactor);
    
    if(ShowPSSMLevel > 0)
    {
		float4 positionWorld;
		positionWorld.x = input.TexCoord.x * 2.0f - 1.0f;
		positionWorld.y = -(input.TexCoord.y * 2.0f - 1.0f);
		positionWorld.z = depthVal;
		positionWorld.w = 1.0f;
		positionWorld = mul(positionWorld, InvertViewProjection);
		positionWorld /= positionWorld.w;    
		
		if(CheckInFrustum(positionWorld, PSSMMap0Frustum))
			finalColor.r *= 2;
			else
			{
				if(ShowPSSMLevel > 1 && CheckInFrustum(positionWorld, PSSMMap1Frustum))
					finalColor.g *= 2;
					else
					{
						if(ShowPSSMLevel > 2  && CheckInFrustum(positionWorld, PSSMMap2Frustum))
							finalColor.b *= 2;
						else
						{
							if(ShowPSSMLevel > 3  && CheckInFrustum(positionWorld, PSSMMap3Frustum))
								finalColor.rg *= 2;
						}
					}
			}
		
    }
    
    //判斷shadow map
    //float depth = -1;
    //if(bPSSMMap0 && depth <0 )
    //{
		 //depth = ConsiderInFrustum(positionWorld, PSSMMap0Frustum, PSSMMap0Sampler, PSSMMap0DepthBias);
		 //if(depth >= 0)
			 //finalColor.rgb *= 0.5f;
			 //
//#ifdef RENDER_PSSM_LEVEL
		//if(depth != -1)
			//finalColor.r *= 2;
//#endif
    //}
    //
  //
    //if(bPSSMMap1 && depth <0 )
    //{    
		 //depth = ConsiderInFrustum(positionWorld, PSSMMap1Frustum, PSSMMap1Sampler, PSSMMap1DepthBias);
		  //if(depth>=0)
			 //finalColor.rgb*=0.5f;
			 //
//#ifdef RENDER_PSSM_LEVEL
		//if(depth != -1)
			//finalColor.g *= 2;
//#endif
    //}
    //
    //if(bPSSMMap2 && depth <0 )
    //{    
		 //depth = ConsiderInFrustum(positionWorld, PSSMMap2Frustum, PSSMMap2Sampler, PSSMMap2DepthBias);
		  //if(depth>=0)
			 //finalColor.rgb*=0.5f;
			 //
//#ifdef RENDER_PSSM_LEVEL
		//if(depth != -1)
			//finalColor.b *= 2;
//#endif    
	//}
    //
    //if(bPSSMMap3 && depth <0 )
    //{    
		 //depth = ConsiderInFrustum(positionWorld, PSSMMap3Frustum, PSSMMap3Sampler, PSSMMap3DepthBias);
		  //if(depth>=0)
			 //finalColor.rgb*=0.5f;
			 //
//#ifdef RENDER_PSSM_LEVEL
		//if(depth != -1)
			//finalColor.rgb *= float3(2, 1, 2);
//#endif
    //}
    
    return finalColor;
}

technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}
