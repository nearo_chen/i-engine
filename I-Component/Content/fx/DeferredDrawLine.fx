float4x4 WorldMatrix;
float4x4 ViewProjection;
float depthBias=0;
texture DepthTexture;


struct VOut
{
    float4 WVPPos: POSITION0;
    float3 worldPos: TEXCOORD0;
    float4 Color : COLOR0;
};


struct VInput
{
    float4 position : POSITION0;
    float4 Color : COLOR0;
};


sampler DepthSampler  = sampler_state
{
    Texture = (DepthTexture);
    MinFilter = POINT;
    MagFilter = POINT;
    MipFilter = NONE;
    AddressU = Clamp;
    AddressV = Clamp;
};

VOut VertexShaderFunction(VInput input)
{
    VOut output;
    float4 www = mul(input.position, WorldMatrix);
    output.WVPPos = mul(www, ViewProjection);
    
    output.worldPos = www;//output.WVPPos;
    
    //align texture coordinates
    output.Color = input.Color;
    return output;
}

void RestoreBuffersPixelShader(in VOut input,
								 out float4 color: COLOR0)
								// ,out float depth : DEPTH)
{

//這邊算錯了，原因如下：目前已暫不使用
  //obtain textureCoordinates corresponding to the current pixel
    //the screen coordinates are in [-1,1]*[1,-1]
    //the texture coordinates need to be in [0,1]*[0,1]

	// Find the position of this pixel in light space
    float4 lightingPosition = mul( input.worldPos, ViewProjection);
    
    // Find the position in the shadow map for this pixel
    //float2 ShadowTexCoord = (lightingPosition.xy / lightingPosition.w + 1)/2;
    float2 ShadowTexCoord = lightingPosition.xy * 0.5f / lightingPosition.w + float2( 0.5, 0.5 );
    ShadowTexCoord.y = 1.0f - ShadowTexCoord.y;
    float shadowdepth = tex2D(DepthSampler, ShadowTexCoord).r;    
    
    float ourdepth = (lightingPosition.z / lightingPosition.w) - depthBias;
    //clip(ourdepth - shadowdepth);
 
	//write the color
	color = input.Color;
	
	if (shadowdepth < ourdepth)
    {
        // Shadow the pixel by lowering the intensity
        color.rbg =float3(1, 0,0);
       
    };
    
	
	
	//write the depth
//	depth = shadowdepth;
}

technique Technique1
{
    pass Pass1
    {
		VertexShader = compile vs_3_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 RestoreBuffersPixelShader();
    }
}
