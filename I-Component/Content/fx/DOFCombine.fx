float4x4 InvertProjection;

texture BaseTexture;
sampler BaseTextureSampler = sampler_state
{
    Texture = <BaseTexture>;
    MinFilter = linear;
    MagFilter = linear;
    MipFilter = NONE;
    AddressU = Clamp;
    AddressV = Clamp;
};

texture BlurTexture;
sampler BlurTextureSampler = sampler_state
{
    Texture = <BlurTexture>;
    MinFilter = linear;
    MagFilter = linear;
    MipFilter = NONE;
    AddressU = Clamp;
    AddressV = Clamp;
};

texture DepthMap;
sampler DepthMapSampler = sampler_state
{
    Texture = <DepthMap>;
    MinFilter = linear;
    MagFilter = linear;
    MipFilter = NONE;
    AddressU = Clamp;
    AddressV = Clamp;
};

float DOFStart = 100;
float DOFEnd = 300;
float DOFRange = 800;
float DOFIntensity = 1;
//float DOFPower = 2; 
/*
float Distance;
float Range;
float Near;
float Far;
*/

float4 PixelShader(float2 texCoord : TEXCOORD0) : COLOR0
{
    // Look up the bloom and original base image colors.
    float4 bloom = tex2D(BlurTextureSampler, texCoord);
    float4 base = tex2D(BaseTextureSampler, texCoord);


    // Get the current depth stored in the shadow map
	float depth = tex2D(DepthMapSampler, texCoord).r;
	
	
	//↑↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓資訊取得↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
	
      //compute screen-space position
    float4 positionView;
    positionView.x = texCoord.x * 2.0f - 1.0f;
    positionView.y = -(texCoord.y * 2.0f - 1.0f);
    positionView.z = depth;
    positionView.w = 1.0f;
    //transform to view space
    positionView = mul(positionView, InvertProjection);
    positionView /= positionView.w;
//↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑資訊取得↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑


/*
	//請參考http://digitalerr0r.wordpress.com/2009/05/16/xna-shader-programming-tutorial-20-depth-of-field/

    // Invert the depth texel so the background is white and the nearest objects are black
	depth = 1 - depth;

	// Calculate the distance from the selected distance and range on our DoF effect, set from the application
	float fSceneZ = ( -Near * Far ) / ( depth - Far);
	float blurFactor = saturate(abs(fSceneZ-Distance)/Range);

	// Based on how far the texel is from "distance" in Distance, stored in blurFactor, mix the scene
    return lerp(base, bloom, blurFactor);
    */
    
    float blurFactor = 0;
     
  /*   if(DOFStart ==0 && DOFEnd == 0 && DOFRange ==0)
     {
     blurFactor=0;
     }
     else
     {*/
		   // END和start之間必須完全清楚，然後借線之外靠衰減值控制慢慢越糊
			float blurFactorStart = abs(positionView.z) - DOFStart;//saturate( (abs(positionView.z)  - DOFStart) / (DOFEnd - DOFStart));
			//blurFactor = saturate( abs(blurFactor) / DOFRange) ;
			//blurFactor = pow(blurFactor, 2);
			if(blurFactorStart < 0)
			{
				blurFactor = saturate( abs(blurFactorStart) / DOFRange);
			}
			
			float blurFactorEnd = abs(positionView.z) - DOFEnd;
			if(blurFactorEnd>0)
			{
				blurFactor = saturate( abs(blurFactorEnd) / DOFRange);
			}
//	}
	

 //  float blurFactor = 0;
 /*   if(
    abs(viewPosition.z) > 5000
    //depth < 0.3 && 
  //  depth >0.998
    )
    {
 //   blurFactor += (depth-0.998) / 0.002;
      blurFactor =1;// saturate((depth - DOFStart)/DOFRange);
      }*/
    return  lerp(base, bloom, blurFactor * DOFIntensity);
}




struct VertexShaderInput
{
    float3 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};



float2 halfPixel;
VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    output.TexCoord = input.TexCoord - halfPixel;
    return output;
}

technique BloomCombine
{
    pass Pass1
    {
         VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShader();
    }
}
