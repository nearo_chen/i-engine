texture ColorTexture;
texture DepthTexture;

texture EmmTexture;

sampler ColorSampler  = sampler_state
{
    Texture = (ColorTexture);
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler DepthSampler  = sampler_state
{
    Texture = (DepthTexture);
    MinFilter = POINT;
    MagFilter = POINT;
    MipFilter = NONE;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler EmmSampler  = sampler_state
{
    Texture = (EmmTexture);
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};

struct VertexShaderInput
{
    float3 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

float2 halfPixel;
VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    output.TexCoord = input.TexCoord - halfPixel;
    return output;
}

void RestoreDepth(in float2 texCoord : TEXCOORD0, 
								out float4 color: COLOR0,
								 out float depth : DEPTH)
{
	//write the color
	color = 0;//�õ���
	
	//write the depth
	depth = tex2D(DepthSampler, texCoord).r;
}

void RestoreColorDepth(in float2 texCoord : TEXCOORD0, 
								out float4 color: COLOR0,
								 out float depth : DEPTH)
{
	//write the color
	color = tex2D(ColorSampler, texCoord);
	
	//write the depth
	depth = tex2D(DepthSampler, texCoord).r;
}

void RestoreColorEmmisiveDepth(in float2 texCoord : TEXCOORD0, 
								out float4 color: COLOR0,
								out float4 emm: COLOR1,
								 out float depth : DEPTH)
{
	//write the color
	color = tex2D(ColorSampler, texCoord);
	
	//write the depth
	depth = tex2D(DepthSampler, texCoord).r;
	
	emm = tex2D(EmmSampler, texCoord);
}

technique RestoreDepth
{
    pass Pass1
    {
       VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 RestoreDepth();
    }
}

technique RestoreColorDepth
{
    pass Pass1
    {
       VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 RestoreColorDepth();
    }
}

technique RestoreColorEmmisiveDepth
{
    pass Pass1
    {
       VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 RestoreColorEmmisiveDepth();
    }
}