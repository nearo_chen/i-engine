#include "MaterialBank.inc"

//position of the camera, for specular light
float3 cameraPosition;
//this is used to compute the world-position
float4x4 InvertViewProjection;
// diffuse color, and specularIntensity in the alpha channel
//texture colorMap;
// normals, and specularPower in the alpha channel
texture normalMap;
//depth
texture depthMap;
//other settings
//texture otherMap;

//sampler colorSampler = sampler_state
//{
    //Texture = (colorMap);
    //AddressU = CLAMP;
    //AddressV = CLAMP;
    //MagFilter = LINEAR;
    //MinFilter = LINEAR;
    //Mipfilter = LINEAR;
//};
sampler depthSampler = sampler_state
{
    Texture = (depthMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};
sampler normalSampler = sampler_state
{
    Texture = (normalMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};
/*
sampler otherSampler = sampler_state
{
    Texture = (otherMap);
    AddressU = CLAMP;
    AddressV = CLAMP;
    MagFilter = POINT;
    MinFilter = POINT;
    Mipfilter = POINT;
};
*/
struct VertexShaderInput
{
    float3 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
}
;
struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
}
;
float2 halfPixel;
VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    //align texture coordinates
    output.TexCoord = input.TexCoord - halfPixel;
    return output;
}


struct SpotLight
{
	float3 position;
	float3 direction;     
	float angleTheta;  //SPOT的內圈
	float anglePhi;		//SPOT的外圈
	float strength;	//計算後光的強度在加強的倍數
	float decay;       //SPOT的衰減強度
	float3 color;      

	float range; //SPOT最遠照多遠
};
SpotLight SPlightArray[16]; //陣列不能給1的樣子
int SpotLightAmount = 0;

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    //↑↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓資訊取得↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    //float4 colorData = tex2D(colorSampler,input.TexCoord);    

    //get normal data from the normalMap
    float4 normalData = tex2D(normalSampler,input.TexCoord);
    //tranform normal back into [-1,1] range
    float3 normal = 2.0f * normalData.xyz - 1.0f;
    float materialID = round(normalData.a * 10.0f);
    //用round抓4捨五入
    
    Material material = MaterialBank[materialID];
    //get specular power, and get it into [0,255] range]
    float specularPower = material.SpecularSpower.a;
    // * 255;
  
    //get specular intensity from the colorMap
    float3 materialSpecular = material.SpecularSpower.rgb;
    float3 materialAmbient = material.AmbientReflectIntensity.rgb;
    float3 materialDiffuse = material.Diffuse;
    //read depth
    float depthVal = tex2D(depthSampler,input.TexCoord).r;
    //  float4 otherData = tex2D(otherSampler,input.TexCoord);
  
    //compute screen-space position
    float4 position;
    position.x = input.TexCoord.x * 2.0f - 1.0f;
    position.y = -(input.TexCoord.y * 2.0f - 1.0f);
    position.z = depthVal;
    position.w = 1.0f;
    //transform to world space
    position = mul(position, InvertViewProjection);
    position /= position.w;
    //  	return float4(position.xyz, 1);
//↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑資訊取得↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

	float4 final=0;
	for(int a=0 ;a<SpotLightAmount ; a++)
	{
		float3 directionToLight = SPlightArray[a].position - position;
		float lightDist = length(directionToLight);
		directionToLight = directionToLight / lightDist;
	
		float coneDot = dot(-directionToLight, SPlightArray[a].direction);
	
		float coneAttenuation=0;

//越接近內圈數值會與光向dot為1，所以 內圈數值會比外圈大。
		if(coneDot > SPlightArray[a].anglePhi)//可以把if 弄掉，內圈的dot一定會比外圈大
		{
			if(coneDot > SPlightArray[a].angleTheta)
				coneAttenuation = 1;
			else
				coneAttenuation = pow(  (coneDot-SPlightArray[a].anglePhi) / (SPlightArray[a].angleTheta - SPlightArray[a].anglePhi) , SPlightArray[a].decay);
		
			coneAttenuation *= pow
									  ( 
											saturate
											(
											    (SPlightArray[a].range - lightDist) / SPlightArray[a].range
											)
										   , SPlightArray[a].decay
                                      );

		float diffuseIntensity = saturate( dot(normal, directionToLight) );
		float3 diffuse = diffuseIntensity * SPlightArray[a].color * materialDiffuse;

		float3 reflectionVector = normalize(reflect(-directionToLight, normal));
		float3 directionToCamera = normalize(cameraPosition - position);
		
		//calculate specular component
		float specular = saturate(
											   SPlightArray[a].color * materialSpecular.r * //specularIntensity * 
											   pow(   
													  saturate(dot(reflectionVector, directionToCamera)),
													  specularPower
													  )
											  );			
									
		final += coneAttenuation * SPlightArray[a].strength * float4( diffuse,  specular);			
		}
	}
   
	return final;
}


technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }

}
