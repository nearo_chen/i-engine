﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;


namespace I_Component
{
    public partial class I_SceneManager : IDisposable
    {
        /// <summary>
        /// 取得scene ID
        /// </summary>
        public int GetSceneID()
        {
            return m_sceneID;
        }

        ///// <summary>
        ///// 取得scene ID
        ///// </summary>
        //public string GetSceneName()
        //{
        //    return m_SceneData.SceneName;
        //}

        /// <summary>
        /// 重新整理目前Scene的資料，更新m_SceneData，已取得正確的m_SceneData資訊，
        /// 可傳入sceneName做為存檔相對路徑
        /// </summary>
        public void RefreshSceneData(string sceneName)
        {
            if (sceneName != null)
                m_SceneData.SceneName = sceneName;
            m_SceneData.CamerasData = GetCamerasData();
            m_SceneData.LoadersName = GetLoadersName();
            m_SceneData.MaterialBank = GetMaterialBankData();

            //m_SceneData.SceneLightFogData = m_SceneData.SceneLightFogData; set裡面就已經直接修改了，所以這邊不用設定
        }

        /// <summary>
        /// 取得m_SceneData.SceneLightFogData的複製版
        /// </summary>
        /// <returns></returns>
        public I_SpecialMeshData.I_SceneLightFogData CloneLightFogData()
        {
            I_SpecialMeshData.I_SceneLightFogData lightFogData = new I_SpecialMeshData.I_SceneLightFogData();
            I_SpecialMeshData.I_SceneLightFogData.AsignBtoA(lightFogData, m_SceneData.SceneLightFogData);
            return lightFogData;
        }

        /// <summary>
        /// 取得此scene中霧的資訊(參數傳null表示不取該值)
        /// </summary>
        public void GetFog(out Vector3 FogColor, out float FogStart,
            out float FogEnd, out float FogMaxRatio)
        {
            //    if (FogColor != null)
            FogColor = m_SceneData.SceneLightFogData.FogColor;
            //  if (FogStart != null)
            FogStart = m_SceneData.SceneLightFogData.FogStart;
            //   if (FogEnd != null)
            FogEnd = m_SceneData.SceneLightFogData.FogEnd;
            //    if (FogMaxRatio != null)
            FogMaxRatio = m_SceneData.SceneLightFogData.FogMaxRatio;
        }

        /// <summary>
        /// 設定此scene中霧的資訊(參數傳null表示不設定該值)
        /// </summary>
        public void SetFog(Vector3? FogColor, float? FogStart,
             float? FogEnd, float? FogMaxRatio)
        {
            if (FogColor != null)
                m_SceneData.SceneLightFogData.FogColor = FogColor.Value;
            if (FogStart != null)
                m_SceneData.SceneLightFogData.FogStart = FogStart.Value;
            if (FogEnd != null)
                m_SceneData.SceneLightFogData.FogEnd = FogEnd.Value;
            if (FogMaxRatio != null)
                m_SceneData.SceneLightFogData.FogMaxRatio = FogMaxRatio.Value;
        }


        ///// <summary>
        ///// 取得scene名稱
        ///// </summary>
        //public string GetSceneName()
        //{
        //    return m_SceneData.SceneName;
        //}

        /// <summary>
        /// 取得所有camera資源
        /// </summary>
        public I_Camera[] GetCameraAsset()
        {
            return m_CameraAsset.ToArray();
        }

        /// <summary>
        /// 根據名稱取得loader
        /// </summary>
        public BasicMeshLoader GetLoader(ref string loaderName)
        {
            List<BasicMeshLoader>.Enumerator itr = m_LoaderAsset.GetEnumerator();
            while (itr.MoveNext())
                if (itr.Current.GetRootNode.NodeName == loaderName)
                    return itr.Current;
            return null;
        }

        /// <summary>
        /// 取得所有loader資源
        /// </summary>
        public BasicMeshLoader[] GetLoaderAsset()
        {
            return m_LoaderAsset.ToArray();
        }

        public List<BasicMeshLoader>.Enumerator GetLoaderAssetItr()
        {
            return m_LoaderAsset.GetEnumerator();
        }

        /// <summary>
        /// 讀取模型檔，讀入Scene資源中。
        /// </summary>
        public BasicMeshLoader AddLoaderXML(ref string filename, out SpecialMeshDataManager.TatalSpecialMeshData total)
        {
            AnimationTableDataArray animationTableDataArray;
            BasicMeshLoader loader = I_SpecialMeshData.LoadSceneFromSpecialXML(ref filename, m_Content, m_GraphicDevice,
                 out total, out animationTableDataArray);
            m_LoaderAsset.Add(loader);


            switch ((I_XNAComponent.SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE)total.ModelType)
            {
                case SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.KeyframeMesh:
                    {
                        //將此Loader內的Node建立Bounding volume，用來做frustum culling。
                        CreateViewFrustumCulling_MeshNode(loader);
                    }
                    break;
                case SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE.SkinnedMesh:
                    {
                    }
                    break;
                default:
                    {
                        CreateViewFrustumCulling_MeshNode(loader);
                    }
                    break;
            }

            return loader;
        }

        /// <summary>
        /// XNB模型檔的前處理
        /// 由XNB讀取骨架模型檔，讀入Scene資源中，讀檔成功後，幫此Loader建立XML資訊，
        /// 將XML檔名資訊記錄在m_SceneData中。
        /// </summary>
        public BasicMeshLoader PreAddLoaderXNB(ref string filename, SpecialMeshDataManager.TatalSpecialMeshData.MODELTYPE modelType,
            out SpecialMeshDataManager.TatalSpecialMeshData total)
        {
            //幫此Loader建立XML資訊，並將XML檔名資訊記錄在m_SceneData中
            SpecialMeshDataManager.TatalSpecialMeshData totalSpecialMeshData = new SpecialMeshDataManager.TatalSpecialMeshData();
            totalSpecialMeshData.ModelFileName = Utility.GetOppositeFilePath(filename);
            totalSpecialMeshData.ModelType = (int)modelType;

            string xmlFileName = filename;
            xmlFileName = Utility.RemoveSubFileName(xmlFileName) + ".xml";
            if (Utility.IfFileExist(xmlFileName) == false)
                Utility.SaveXMLFile(xmlFileName, totalSpecialMeshData);

            BasicMeshLoader loader = AddLoaderXML(ref xmlFileName, out total);
            if (loader == null)
                Utility.DeleteFile(xmlFileName);
            else
                return loader;
            return null;
        }



        /// <summary>
        /// 取得最後加入的camera
        /// </summary>
        /// <returns></returns>
        public I_Camera GetLastCamera()
        {
            if (m_CameraAsset.Count == 0)
                return null;
            return m_CameraAsset.Last();
        }

        /// <summary>
        /// 由此scene中找出於編輯器設定的I_Camera資訊
        /// </summary> 
        public I_Camera GetCamera(string name)
        {
            if (name == null)
                return m_CameraAsset.Last();
            return GetCamera(ref name);
        }

        /// <summary>
        /// 由此scene中找出於編輯器設定的I_Camera資訊
        /// </summary> 
        public I_Camera GetCamera(ref string name)
        {
            I_Camera camera = null;
            List<I_Camera>.Enumerator itr = m_CameraAsset.GetEnumerator();
            while (itr.MoveNext())
            {
                if (itr.Current.IfName(ref name))
                {
                    camera = itr.Current;
                    break;
                }
            }
            return camera;
        }

        static int CameranameCount = 0;

        /// <summary>
        /// 於此場景中建立Camera資訊，目前為一個Scene只能有一個Camera。
        /// </summary>
        public void AddNewCamera(GraphicsDevice device)
        {            
       //     if (CameranameCount >= 1)//目前為一個Scene只能有一個Camera。
      //          return;

            I_SpecialMeshData.I_CameraData cameraData =
                new I_SpecialMeshData.I_CameraData(//256, 256, 
                    "Camera", 45, 0.01f, 1000f,
                    100, 500, 500, 1);
            CameranameCount++;
            cameraData.Name += CameranameCount.ToString();
            I_Camera camera = new I_Camera(device, this, cameraData);
            m_CameraAsset.Add(camera);
        }

        /// <summary>
        /// 重建更新Camera設定資訊
        /// </summary>
        /// <param name="cameraData"></param>
        public void RefreshCameraInfo(GraphicsDevice device, I_SpecialMeshData.I_CameraData cameraData)
        {
            //幹掉舊的
            foreach (I_Camera ccc in m_CameraAsset)
            {
                if (ccc.Name == cameraData.Name)
                {
                    m_CameraAsset.Remove(ccc);
                    ccc.Dispose();
                    break;
                }
            }

            //重新加入新的
            m_CameraAsset.Add(new I_Camera(device, this, cameraData));
        }

        /// <summary>
        /// 取得繪圖功能大的Main Camera
        /// </summary>
        /// <returns></returns>
        public I_Camera GetMainCamera()
        {
            return m_CameraAsset.Last();//應為我現在限制一個scene只能有一個camera，所以取得main Camera跟取得last camera是一樣的

            //foreach (I_Camera ccc in m_CameraAsset)
            //    if (ccc.IfMainCamera)
            //        return ccc;
            //return null;
        }

        /// <summary>
        /// 設定該Camera為main camera，若設定成功，就會回傳true
        /// </summary>
        /// <returns></returns>
        public bool SetMainCamera()
        {
            I_Camera ccc = GetMainCamera();
            if (ccc != null)
            {
                return false;
            }

            ccc.IfMainCamera = true;
            return true;
        }

        /// <summary>
        /// 根據loader找出他所屬的Camera的view和projection
        /// </summary>
        //public I_Camera GetCameraViewFromLoader(string loaderName, out Matrix v)
        //{
        //    v = Matrix.Identity;
        //    BasicMeshLoader loader = GetLoader(ref loaderName);
        //    foreach (I_Camera camera in m_CameraAsset)
        //    {
        //        if (camera.IfInRenderLoader(loader))
        //        {
        //            Matrix cameramat;
        //            camera.GetCameraMatrix(out cameramat, out v);
        //            return camera;
        //        }
        //    }
        //    return null;
        //}

        /// <summary>
        /// 設定WindData資訊給node，看是要加GrassNode，還是只設定物件搖動
        /// </summary>
        public void SetWindData(SpecialMeshDataManager.WindData windData, SceneNode node)
        {
            SceneNode.NodeWindDataChange(windData, node, m_GraphicDevice, m_Content);

            //如果windData是null表示做刪除動作，有可能有刪掉grassNode所以camera也要更新看看
            //有草的資訊會生出GrassNode，就要將Camera更新一次，加入繪圖陣列裡。
            if (windData == null || windData.grassData != null)
            {
                foreach (I_Camera camera in GetCameraAsset())
                    camera.RefreshCameraRenderNodeInformation();
            }
        }
    }
}
