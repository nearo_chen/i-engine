﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;
using StillDesign.PhysX;

namespace I_Component
{
    public partial class I_SceneManager : System.IDisposable
    {
        List<PhysXFluidSystem> m_PhysXFluidManager = new List<PhysXFluidSystem>(32);

        /// <summary>
        /// 於scene中建立一個physX的particle，回傳該particle的ID
        /// </summary>
        /// <returns>ParticleEmitterID</returns>
        public void AddPhysXFluid()
        {
            PhysXFluidSystem newFluid = new PhysXFluidSystem(m_PhysX.GetSceneFromSceneID(m_PhysXSceneID));
            m_PhysXFluidManager.Add(newFluid);
        }

        /// <summary>
        /// 取得所有particle資源
        /// </summary>
        public PhysXFluidSystem[] GetPhysXFluidAsset()
        {
            if (m_PhysXFluidManager != null)
                return m_PhysXFluidManager.ToArray();
            return null;
        }
    }
}
