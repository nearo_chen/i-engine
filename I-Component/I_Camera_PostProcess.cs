﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    /// <summary>
    /// I_CameraNode控制一個Camera及最後畫到Texture的畫面，此class管理view frustum culling，
    /// 於編輯器中設定Camera，有介面可設定傳入要繪出的Loader，於Render時，Camera會根據要畫出的loader先做view culling，
    /// 得到要畫出的 [ ] Nodes，再設定camera的renderTarget，然後根據camera所設定使用的特效繪圖管線處理，如打光處理，
    /// 點光源處理，影子處理，hdr..等，將最後結果畫在camera Texture上。
    /// </summary>
    public partial class I_Camera : IDisposable
    {
        public float DOFFocalDistance;
        public float DOFFocalRange;
        public float DOFDecayRange;
        public float DOFIntensity;

        /// <summary>
        /// PSSMLevelAmount為設定view frustum要切成幾等份，為0就表示關閉PSSM功能
        /// </summary>
        public int PSSMLevelAmount = 2;
        public float PSSMSplitRatioLambda =0.7f;//splitRatioLambda越1吃LOG比重越重，PSSM0會越小。
        public float PSSM0Bias=0.001f;
        public float PSSM1Bias = 0.001f;
        public float PSSM2Bias = 0.001f;
        public float PSSM3Bias = 0.001f;
        public float GetPSSMBias(int PSSMLevel)
        {
            if (PSSMLevel == 0)
                return PSSM0Bias;
            if (PSSMLevel == 1)
                return PSSM1Bias;
            if (PSSMLevel == 2)
                return PSSM2Bias;
            if (PSSMLevel == 3)
                return PSSM3Bias;
            return 0;
        }

        /// <summary>
        /// SSAOSampleRadius為0表示關閉SSAO功能。
        /// </summary>
        public float SSAOSampleRadius = 0;
        public float SSAOSampleTime = 8;
        public float SSAOSampleLOD = 2;
        public float SSAODistanceScale = 1;
        public float SSAOFadeOutDistance = 10;
    }
}
