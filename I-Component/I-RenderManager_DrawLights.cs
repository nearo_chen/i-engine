﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using I_XNAComponent;
using I_XNAUtility;

namespace I_Component
{
    public abstract partial class I_RenderManager : BasicSceneManager
    {

        private void DrawDirectionalLight(ref Vector3 lightDirection,
            ref Vector3 LightAmbient, ref Vector3 LightDiffuse, ref Vector3 LightSpecular,
            ref Vector3 cameraPosition, ref Matrix viewMatrix, ref Matrix projectionMatrix)
        {
            //set all parameters
            //m_DirectionalLightEffect.Parameters["colorMap"].SetValue(m_ColorRT.GetTexture());
            m_DirectionalLightEffect.Parameters["normalMap"].SetValue(m_NormalRT.GetTexture());
            m_DirectionalLightEffect.Parameters["depthMap"].SetValue(m_DepthRT.GetTexture());
            //m_DirectionalLightEffect.Parameters["otherMap"].SetValue(m_OtherMap.GetTexture());
            m_DirectionalLightEffect.Parameters["lightDirection"].SetValue(lightDirection);

            m_DirectionalLightEffect.Parameters["LightAmbient"].SetValue(LightAmbient);
            m_DirectionalLightEffect.Parameters["LightDiffuse"].SetValue(LightDiffuse);
            m_DirectionalLightEffect.Parameters["LightSpecular"].SetValue(LightSpecular);

            m_DirectionalLightEffect.Parameters["cameraPosition"].SetValue(cameraPosition);
            m_DirectionalLightEffect.Parameters["InvertViewProjection"].SetValue(Matrix.Invert(viewMatrix * projectionMatrix));
            m_DirectionalLightEffect.Parameters["halfPixel"].SetValue(halfPixel);
            m_DirectionalLightEffect.CommitChanges();

            m_DirectionalLightEffect.Begin();
            m_DirectionalLightEffect.Techniques[0].Passes[0].Begin();
            //draw a full-screen quad
            I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
            m_DirectionalLightEffect.Techniques[0].Passes[0].End();
            m_DirectionalLightEffect.End();
        }


        void DrawPointLight(I_SceneManager scene,
            ref Vector3 cameraPosition, ref Matrix viewMatrix, ref Matrix projectionMatrix)
        {
            m_PointLightEffect.Parameters["cameraPosition"].SetValue(cameraPosition);
            m_PointLightEffect.Parameters["InvertViewProjection"].SetValue(Matrix.Invert(viewMatrix * projectionMatrix));
            m_PointLightEffect.Parameters["halfPixel"].SetValue(halfPixel);

            m_PointLightEffect.Parameters["normalMap"].SetValue(m_NormalRT.GetTexture());
            m_PointLightEffect.Parameters["depthMap"].SetValue(m_DepthRT.GetTexture());

            int count = 0;
            PointLightNode[] tmpPLNodes = new PointLightNode[MaxPointLightPerPass];
            List<BasicNode> allPointLights = scene.GetPointLights();
            List<BasicNode>.Enumerator itr = allPointLights.GetEnumerator();
            while (itr.MoveNext())
            {
                int arrayID = count % MaxPointLightPerPass;
                tmpPLNodes[arrayID] = itr.Current as PointLightNode;
                count++;

                if (//buffer滿了或是到末端了就畫圖
                    arrayID == MaxPointLightPerPass - 1 ||
                    count == allPointLights.Count
                    )
                {
                    SetPointLightPar(tmpPLNodes, arrayID + 1);

                    m_PointLightEffect.CommitChanges();

                    m_PointLightEffect.Begin();
                    m_PointLightEffect.Techniques[0].Passes[0].Begin();
                    //draw a full-screen quad
                    I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
                    m_PointLightEffect.Techniques[0].Passes[0].End();
                    m_PointLightEffect.End();
                }

            }
            itr.Dispose();
        }

        void DrawSpotLight(I_SceneManager scene,
           ref Vector3 cameraPosition, ref Matrix viewMatrix, ref Matrix projectionMatrix)
        {
            m_SpotLightEffect.Parameters["cameraPosition"].SetValue(cameraPosition);
            m_SpotLightEffect.Parameters["InvertViewProjection"].SetValue(Matrix.Invert(viewMatrix * projectionMatrix));
            m_SpotLightEffect.Parameters["halfPixel"].SetValue(halfPixel);

            m_SpotLightEffect.Parameters["normalMap"].SetValue(m_NormalRT.GetTexture());
            m_SpotLightEffect.Parameters["depthMap"].SetValue(m_DepthRT.GetTexture());

            int count = 0;
            SpotLightNode[] tmpSPLNodes = new SpotLightNode[MaxSpotLightPerPass];
            List<BasicNode> allSpotLights = scene.GetSpotLights();
            List<BasicNode>.Enumerator itr = allSpotLights.GetEnumerator();
            while (itr.MoveNext())
            {
                int arrayID = count % MaxSpotLightPerPass;
                tmpSPLNodes[arrayID] = itr.Current as SpotLightNode;
                count++;

                if (//buffer滿了或是到末端了就畫圖
                    arrayID == MaxSpotLightPerPass - 1 ||
                    count == allSpotLights.Count
                    )
                {
                    SetSpotLightPar(tmpSPLNodes, arrayID + 1);

                    m_SpotLightEffect.CommitChanges();

                    m_SpotLightEffect.Begin();
                    m_SpotLightEffect.Techniques[0].Passes[0].Begin();
                    //draw a full-screen quad
                    I_XNATools.QuadRenderComponent.Render(Vector2.One * -1, Vector2.One, m_GraphicDevice);
                    m_SpotLightEffect.Techniques[0].Passes[0].End();
                    m_SpotLightEffect.End();
                }
            }
            itr.Dispose();
        }

        /// <summary>
        /// 只根據Main Camera計算場景內所有的打光，
        /// 計算打平行光後的圖，傳入sceneID看要使用哪個scene裡面的平行光設定資訊，
        /// 還有要根據哪個camera視點做打光計算(cameraName傳null表示使用後加入的那個camera)
        /// </summary>
        protected void DrawLightsMap(int sceneID)
        {
            I_SceneManager scene = GetScene(sceneID);

            I_Camera camera = scene.GetMainCamera(); ;
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return;

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;

            m_GraphicDevice.SetRenderTarget(0, m_LightRT);

            //clear all components to 0
            m_GraphicDevice.Clear(Color.TransparentBlack);

            m_GraphicDevice.RenderState.AlphaBlendEnable = true;
            //use additive blending, and make sure the blending factors are as we need them
            m_GraphicDevice.RenderState.AlphaBlendOperation = BlendFunction.Add;
            m_GraphicDevice.RenderState.SourceBlend = Blend.One;
            m_GraphicDevice.RenderState.DestinationBlend = Blend.One;
            //m_GraphicDevice.RenderState.AlphaBlendEnable = false;

            //use the same operation on the alpha channel
            m_GraphicDevice.RenderState.SeparateAlphaBlendEnabled = false;

            m_GraphicDevice.RenderState.DepthBufferEnable = false;
            m_GraphicDevice.RenderState.DepthBufferWriteEnable = false;



            //draw some lights
            Vector3 LightDirection = Vector3.Zero;
            Vector3 LightAmbient = Vector3.Zero;
            Vector3 LightDiffuse = Vector3.Zero;
            Vector3 LightSpecular = Vector3.Zero;
            scene.GetDirectionalLight(ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

            SetEffectMaterialBank(m_DirectionalLightEffect, scene.GetMaterialBankData());
            DrawDirectionalLight(ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular,
                ref cameraPosition, ref viewMatrix, ref projectionMat);

            SetEffectMaterialBank(m_PointLightEffect, scene.GetMaterialBankData());
            DrawPointLight(scene, ref cameraPosition, ref viewMatrix, ref projectionMat);

            SetEffectMaterialBank(m_SpotLightEffect, scene.GetMaterialBankData());
            DrawSpotLight(scene, ref cameraPosition, ref viewMatrix, ref projectionMat);


            m_GraphicDevice.RenderState.AlphaBlendEnable = false;
            m_GraphicDevice.SetRenderTarget(0, null);
        }


        void DrawDebugLine_SpotLight(int sceneID, I_Camera camera)
        {
            DrawLine.AddLineBegin(m_GraphicDevice);
            I_SceneManager scene = GetScene(sceneID);

            //I_Camera camera;
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return;

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;

            List<BasicNode>.Enumerator itr = scene.GetSpotLights().GetEnumerator();
            while (itr.MoveNext())
            {
                SpotLightNode SPLNode = itr.Current as SpotLightNode;
                DrawLine.AddCone(SPLNode.WorldMatrix, SPLNode.Data.anglePhi * 0.5f, SPLNode.Data.range,
                    new Color(SPLNode.Data.colorR, SPLNode.Data.colorG, SPLNode.Data.colorB, 128)
                        , 20);
                DrawLine.AddLine(SPLNode.WorldPosition, SPLNode.WorldPosition + SPLNode.WorldRight * SPLNode.Data.range, new Color(Color.Red, 50));
                DrawLine.AddLine(SPLNode.WorldPosition, SPLNode.WorldPosition + SPLNode.WorldUp * SPLNode.Data.range, new Color(Color.Green, 50));
                DrawLine.AddLine(SPLNode.WorldPosition, SPLNode.WorldPosition + SPLNode.WorldForward * SPLNode.Data.range, new Color(Color.Blue, 50));
            }
            itr.Dispose();

            DrawLine.Draw(viewMatrix, projectionMat);
        }

        void DrawDebugLine_PointLight(int sceneID, I_Camera camera)
        {
            DrawLine.AddLineBegin(m_GraphicDevice);

            I_SceneManager scene = GetScene(sceneID);

            //I_Camera camera;
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return;

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;

            List<BasicNode>.Enumerator itr = scene.GetPointLights().GetEnumerator();
            while (itr.MoveNext())
            {
                PointLightNode PLNode = itr.Current as PointLightNode;
                DrawLine.AddSphereLine(PLNode.WorldMatrix, Vector3.Zero,
                    PLNode.RangeValue, PLNode.LightColor, 20, 20);

                //DrawLine.AddLine(PLNode.WorldPosition, PLNode.WorldPosition + PLNode.WorldRight * PLNode.RangeValue, new Color(Color.Red, 100));
                //DrawLine.AddLine(PLNode.WorldPosition, PLNode.WorldPosition + PLNode.WorldUp * PLNode.RangeValue, new Color(Color.Green, 100));
                //DrawLine.AddLine(PLNode.WorldPosition, PLNode.WorldPosition + PLNode.WorldForward * PLNode.RangeValue, new Color(Color.Blue, 100));                
            }
            itr.Dispose();

            DrawLine.Draw(viewMatrix, projectionMat);
        }

        void DrawDebugLine_DirectionalLight(int sceneID, I_Camera camera)
        {
            DrawLine.AddLineBegin(m_GraphicDevice);

            I_SceneManager scene = GetScene(sceneID);

            //I_Camera camera;
            //if (cameraName == null)
            //    camera = scene.GetLastCamera();
            //else
            //    camera = scene.GetCamera(ref cameraName);
            if (camera == null)
                return;

            Matrix projectionMat, cameraMat, invCameraMat;
            camera.GetProjectionMatrix(m_GBufferWidth, m_GBufferHeight, out projectionMat);
            camera.GetCameraMatrix(out cameraMat, out invCameraMat);
            Vector3 cameraPosition = cameraMat.Translation;
            Matrix viewMatrix = invCameraMat;
            Matrix invViewMatrix = cameraMat;

            Vector3 LightDirection = Vector3.Zero;
            Vector3 LightAmbient = Vector3.Zero;
            Vector3 LightDiffuse = Vector3.Zero;
            Vector3 LightSpecular = Vector3.Zero;
            scene.GetDirectionalLight(ref LightDirection, ref LightAmbient, ref LightDiffuse, ref LightSpecular);

            Vector3 pos = invViewMatrix.Translation + invViewMatrix.Forward * 100f;
            Vector3 end = pos + 100f * LightDirection;
            DrawLine.AddLine(pos, end, Color.Yellow);//光的方向

            Vector3 right = Vector3.Cross(Vector3.Up, LightDirection);
            right.Normalize();
            //end = pos + 10000f * right;
            //DrawLine.AddLine(pos, end, Color.Brown);

            Vector3 up = Vector3.Cross(LightDirection, right);
            up.Normalize();
            //end = pos + 10000f * up;
            //DrawLine.AddLine(pos, end, Color.BurlyWood);

            Vector3 arrow = pos + 10f * LightDirection;
            Color color = new Color(LightAmbient);
            DrawLine.AddLine(arrow, pos + 10f * right, color);
            DrawLine.AddLine(arrow, pos + -10f * right, color);

            pos = arrow;
            arrow = pos + 8f * LightDirection;
            color = new Color(LightDiffuse);
            //DrawLine.AddLine(arrow, pos + 5f * up, color);
            //DrawLine.AddLine(arrow, pos + -5f * up, color);

            //DrawLine.AddLine(arrow, pos + 5f * right, color);
            //DrawLine.AddLine(arrow, pos + -5f * right, color);

            DrawLine.AddLine(arrow, pos + 2.5f * (up + right), color);
            DrawLine.AddLine(arrow, pos + -2.5f * (up + right), color);

            DrawLine.AddLine(arrow, pos + 2.5f * (-up + right), color);
            DrawLine.AddLine(arrow, pos + -2.5f * (-up + right), color);

            if (IfDrawPSSMLevel)
            {
                //增加PSSM的Debug線段
                //Vector3 nearPos = cameraMat.Translation;
                for (int a = 0; a < camera.PSSMLevelAmount; a++)
                {
                    Color ccc = Color.Lerp(Color.Yellow, Color.Red, (float)a / (float)camera.PSSMLevelAmount);
                    ccc.A = 255;
                    //Vector3 farPos = cameraMat.Translation + cameraMat.Forward * m_PSSMCameraFarPlane[a];

                    ////再遠平面的位置畫一顆球
                    //DrawLine.AddSphereLine(Matrix.CreateTranslation(farPos), Vector3.Zero, camera.Far * 0.01f,
                    //  ccc, 6, 6);
                    //nearPos = farPos;

                    //畫出每階層的shadow frustum
                    DrawLine.AddFrustumLine(new BoundingFrustum(m_PSSMViewfrustums[a]), ccc);

                    ccc = Color.Lerp(Color.Green, Color.LightBlue, (float)a / (float)camera.PSSMLevelAmount);
                    ccc.A = 255;
                    DrawLine.AddFrustumLine(new BoundingFrustum(m_PSSMSceneDependentFrustum[a]), ccc);
                }
            }

            DrawLine.Draw(viewMatrix, projectionMat);
            //    DrawLine.Clear();
            //      DrawDrawLine(viewMatrix * projectionMat);
        }

        void SetEffectMaterialBank(Effect effect, I_Component.I_SpecialMeshData.I_Material[] materials)
        {
            for (int a = 0; a < materials.Length; a++)
            {
                if (materials[a] == null)
                    continue;

                Vector4 values = Vector4.Zero;
                values.X = materials[a].ambient.X; values.Y = materials[a].ambient.Y; values.Z = materials[a].ambient.Z; values.W = materials[a].reflectIntensity;
                effect.Parameters["MaterialBank"].Elements[a].StructureMembers["AmbientReflectIntensity"].SetValue(values);

                // values.X = materials[a].diffuse.X; values.Y = materials[a].diffuse.Y; values.Z = materials[a].diffuse.Z; values.W = materials[a].refractScale;
                effect.Parameters["MaterialBank"].Elements[a].StructureMembers["Diffuse"].SetValue(materials[a].diffuse);

                values.X = materials[a].specular.X; values.Y = materials[a].specular.Y; values.Z = materials[a].specular.Z; values.W = materials[a].Spower;
                effect.Parameters["MaterialBank"].Elements[a].StructureMembers["SpecularSpower"].SetValue(values);
                //     effect.Parameters["MaterialBank"].Elements[a].StructureMembers["Spower"].SetValue(materials[a].Spower);
                //effect.Parameters["MaterialBank"].Elements[a].StructureMembers["fogEnable"].SetValue(materials[a].fogEnable);
                //       effect.Parameters["MaterialBank"].Elements[a].StructureMembers["reflectIntensity"].SetValue(materials[a].reflectIntensity);
                //     effect.Parameters["MaterialBank"].Elements[a].StructureMembers["refractScale"].SetValue(materials[a].refractScale);
            }
        }


    }
}
