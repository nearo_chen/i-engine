﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using I_XNAUtility;
using System.Runtime.Serialization.Formatters.Soap;

namespace I_Component
{ 
    /// <summary>
    /// I_IO管理需要讀檔寫檔的操作
    /// </summary>
    public static class I_IO
    {
        /// <summary>
        /// 開關，設定暫存讀檔資料的Dictionary，檔案只會讀硬碟一次之後都由記憶體中取出。
        /// </summary>
        public static bool IfUseLoadingCache = true;

        static Dictionary<string, object> m_XMLList = new Dictionary<string, object>(1024);

        /// <summary>
        ///  I_IO，會將讀好的XML資料記錄下來，於下次讀取同一個檔名時，先於Dictionary中尋找是否之前已成功讀取過，
        /// 不會重複讀取同一XML檔。
        /// </summary>
        public static object LoadXMLFile(ref string fileName, Type type)
        {
            return LoadXMLFile(ref fileName, type, false);
        }

        /// <summary>
        ///  I_IO，會將讀好的XML資料記錄下來，於下次讀取同一個檔名時，先於Dictionary中尋找是否之前已成功讀取過，
        /// 不會重複讀取同一XML檔。
        /// </summary>
        /// <param name="warning">讀不到檔案時是否會出現警告視窗</param>  
        public static object LoadXMLFile(ref string fileName, Type type, bool warning)
        {
            object xmlData = null;

            if (IfUseLoadingCache)
            {
                if (m_XMLList.TryGetValue(fileName, out xmlData))
                    return xmlData;
            }

            xmlData = Utility.LoadXMLFile(fileName, type, warning);
            //if (File.Exists(fileName))
            //{
            //    try
            //    {
            //        FileStream fp = File.Open(fileName, FileMode.Open);
            //        SoapFormatter formatter = new SoapFormatter();
            //        xmlData = formatter.Deserialize(fp);
            //        //XmlSerializer xmlSerializer = new XmlSerializer(type);
            //        //xmlData = xmlSerializer.Deserialize(fp);
            //        fp.Close();
            //        fp.Dispose();
            //        //xmlSerializer = null;
            //        formatter = null;
            //    }
            //    catch (Exception e)
            //    {
            //        OutputBox.ShowMessage(e.Message);
            //    }
            //}
            //else
            //{
            //    if (warning)
            //    {
            //        OutputBox.ShowMessage(fileName + " File not found!!!");
            //    }
            //}

            if (IfUseLoadingCache)
            {
                m_XMLList.Add(fileName, xmlData);
            }

            return xmlData;
        }

    }
}
