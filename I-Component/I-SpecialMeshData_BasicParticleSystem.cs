﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
//using System.Xml.Serialization;
using System.IO;
using I_XNAComponent;
using I_XNAUtility;
using MeshDataDefine;
using ParticleEngine;

namespace I_Component
{
    public partial class I_SpecialMeshData
    {
        [Serializable]
        public class I_BasicParticleSystemData
        {
            public I_BasicParticleSystemData() { }
            public I_BasicParticleSystemData(ParticleSettings basicParticleSetting, int particlesPerSecond, bool useEmitter, bool useDirectionParticle)
            {
                BasicParticleSetting = basicParticleSetting;
                ParticlesPerSecond = particlesPerSecond;
                UseEmitter = useEmitter;
                UseDirectionParticle = useDirectionParticle;
            }
            public ParticleSettings BasicParticleSetting;
            public int ParticlesPerSecond;
            public bool UseEmitter;
            public bool UseDirectionParticle;
        }
    }
}
