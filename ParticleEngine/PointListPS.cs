﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using I_XNAUtility;


namespace ParticleEngine
{
    public class PointListPS : ParticleSystem, IDisposable
    {
        GraphicsDevice m_Device;

        ContentManager m_Content;

        Texture2D m_Texture;

        ParticleSettings m_Settings;

        PointListPS_Vertex[] m_Particles;

        DynamicVertexBuffer m_VertexBuffer;

        VertexDeclaration m_VertexDeclaration;

        Effect m_ParticleEffect;

        string m_TechniqueName; //是否要用rotation的technique

        EffectParameter m_EffectWorld;
        EffectParameter m_EffectView;
        EffectParameter m_EffectProjection;
        EffectParameter m_EffectViewportHeight;
        EffectParameter m_EffectTime;
        EffectParameter m_EffectDuration;
        EffectParameter m_EffectDurationRandomness;
        EffectParameter m_EffectGravity;
        EffectParameter m_EffectEndVelocity;
        EffectParameter m_EffectMinColor;
        EffectParameter m_EffectMaxColor;
        EffectParameter m_EffectRotateSpeed;
        EffectParameter m_EffectRotateOnce;
        EffectParameter m_EffectTexture;
        EffectParameter m_EffectUseCurveAlpha;
        EffectParameter m_EffectUseLocalWorld;
        EffectParameter m_EffectDoNotChangeAlpha;

        EffectParameter m_EffectStartSize;
        EffectParameter m_EffectEndSize;

        static Random m_Random = new Random();



        public PointListPS(ContentManager content, GraphicsDevice device, ParticleSettings settings)
            : base(settings.Duration, settings.MaxParticles)
        {
            m_Device = device;
            m_Content = content;
            m_Settings = settings;

            LoadContent(device);
        }


        void LoadContent(GraphicsDevice device)
        {
            m_Texture = Utility.Load_Texture2D(m_Device, m_Content, m_Settings.TextureName, false);

            m_Particles = new PointListPS_Vertex[m_Settings.MaxParticles];

            Initialize_ParticleEffect();

            m_VertexDeclaration = new VertexDeclaration(m_Device,
                                          PointListPS_Vertex.VertexElements);

            int size = PointListPS_Vertex.SizeInBytes * m_Particles.Length;

            m_VertexBuffer = new DynamicVertexBuffer(m_Device, size,
                                       BufferUsage.WriteOnly |
                                       BufferUsage.Points);
        }


        #region 設定Effect

        void Initialize_ParticleEffect()
        {
            Effect effect = m_Content.Load<Effect>("Content\\PointListPS");
            m_ParticleEffect = effect.Clone(m_Device);

            EffectParameterCollection parameters = m_ParticleEffect.Parameters;

            m_EffectView = parameters["View"];
            m_EffectProjection = parameters["Projection"];
            m_EffectViewportHeight = parameters["ViewportHeight"];
            m_EffectTime = parameters["CurrentTime"];
            m_EffectTexture = parameters["Texture"];
            m_EffectMinColor = parameters["MinColor"];
            m_EffectMaxColor = parameters["MaxColor"];
            m_EffectWorld = parameters["World"];
            m_EffectDuration = parameters["Duration"];
            m_EffectDurationRandomness = parameters["DurationRandomness"];
            m_EffectGravity = parameters["Gravity"];
            m_EffectEndVelocity = parameters["EndVelocity"];
            m_EffectRotateSpeed = parameters["RotateSpeed"];
            m_EffectRotateOnce = parameters["RotateOnce"];
            m_EffectStartSize = parameters["StartSize"];
            m_EffectEndSize = parameters["EndSize"];
            m_EffectUseCurveAlpha = parameters["UseCurveAlpha"];
            m_EffectUseLocalWorld = parameters["UseLocalWorld"];
            m_EffectDoNotChangeAlpha = parameters["DoNotChangeAlpha"];

            if (m_Settings.MinRotateSpeed == 0 && m_Settings.MaxRotateSpeed == 0)
                m_TechniqueName = "NonRotatingParticles";
            else
                m_TechniqueName = "RotatingParticles";

            m_ParticleEffect.CurrentTechnique =
                        m_ParticleEffect.Techniques[m_TechniqueName];

            m_EffectTexture.SetValue(m_Texture);
            m_EffectDuration.SetValue(m_Settings.Duration);
            m_EffectDurationRandomness.SetValue(m_Settings.DurationRandomness);
            m_EffectGravity.SetValue(m_Settings.Gravity);
            m_EffectEndVelocity.SetValue(m_Settings.EndVelocity);
            m_EffectUseLocalWorld.SetValue(m_Settings.UseLocalWorld);
            m_EffectUseCurveAlpha.SetValue(m_Settings.UseCurveAlpha);
            m_EffectDoNotChangeAlpha.SetValue(m_Settings.DoNotChangeAlpha);
            SetMinColor(ref m_Settings.MinColor);
            SetMaxColor(ref m_Settings.MaxColor);
            m_EffectRotateOnce.SetValue(m_Settings.RotateOnce);
            m_EffectRotateSpeed.SetValue(new Vector2(m_Settings.MinRotateSpeed, m_Settings.MaxRotateSpeed));
            m_EffectStartSize.SetValue(new Vector2(m_Settings.MinStartSize, m_Settings.MaxStartSize));
            m_EffectEndSize.SetValue(new Vector2(m_Settings.MinEndSize, m_Settings.MaxEndSize));
        }


        public void SetMinColor(ref Vector4 color)
        {
            Vector4 tempColor;

            tempColor.X = color.X / 255f;
            tempColor.Y = color.Y / 255f;
            tempColor.Z = color.Z / 255f;
            tempColor.W = color.W / 255f;

            m_EffectMinColor.SetValue(tempColor);
        }


        public void SetMaxColor(ref Vector4 color)
        {
            Vector4 tempColor;

            tempColor.X = color.X / 255f;
            tempColor.Y = color.Y / 255f;
            tempColor.Z = color.Z / 255f;
            tempColor.W = color.W / 255f;

            m_EffectMaxColor.SetValue(tempColor);
        }


        public override void SetCamera(ref Matrix view, ref Matrix projection, ref Vector3 camPos)
        {
            m_EffectView.SetValue(view);
            m_EffectProjection.SetValue(projection);
        }


        public override void SetWorld(Matrix world)
        {
            m_EffectWorld.SetValue(world);
        }


        public override void SetTexture(Texture2D texture)
        {
            m_EffectTexture.SetValue(texture);
        }


        #endregion


        void AddNewParticlesToVertexBuffer()
        {
            int stride = PointListPS_Vertex.SizeInBytes;

            if (m_FirstNewParticle < m_FirstFreeParticle)
            {
                // If the new m_Particles are all in one consecutive range,
                // we can upload them all in a single call.
                m_VertexBuffer.SetData(m_FirstNewParticle * stride, m_Particles,
                                     m_FirstNewParticle,
                                     m_FirstFreeParticle - m_FirstNewParticle,
                                     stride, SetDataOptions.NoOverwrite);
            }
            else
            {
                // If the new particle range wraps past the end of the queue
                // back to the start, we must split them over two upload calls.
                m_VertexBuffer.SetData(m_FirstNewParticle * stride, m_Particles,
                                     m_FirstNewParticle,
                                     m_Particles.Length - m_FirstNewParticle,
                                     stride, SetDataOptions.NoOverwrite);

                if (m_FirstFreeParticle > 0)
                {
                    m_VertexBuffer.SetData(0, m_Particles,
                                         0, m_FirstFreeParticle,
                                         stride, SetDataOptions.NoOverwrite);
                }
            }

            // Move the m_Particles we just uploaded from the new to the active queue.
            m_FirstNewParticle = m_FirstFreeParticle;
        }


        void SetParticleRenderStates(RenderState renderState)
        {
            // Enable point sprites.
            renderState.PointSpriteEnable = true;
            renderState.PointSizeMax = 1024;

            // Set the alpha blend mode.
            renderState.AlphaBlendEnable = true;
            renderState.AlphaBlendOperation = BlendFunction.Add;
            renderState.SourceBlend = m_Settings.SourceBlend;
            renderState.DestinationBlend = m_Settings.DestinationBlend;

            // Set the alpha test mode.
            renderState.AlphaTestEnable = true;
            renderState.AlphaFunction = CompareFunction.Greater;
            renderState.ReferenceAlpha = 0;

            // Enable the depth buffer (so m_Particles will not be visible through
            // solid objects like the ground plane), but disable depth writes
            // (so m_Particles will not obscure other m_Particles).
            renderState.DepthBufferEnable = true;
            renderState.DepthBufferWriteEnable = false;
        }



        public override void Draw()
        {
            // Restore the vertex buffer contents if the graphics m_Device was lost.
            if (m_VertexBuffer.IsContentLost)
            {
                m_VertexBuffer.SetData(m_Particles);
            }

            // If there are any m_Particles waiting in the newly added queue,
            // we'd better upload them to the GPU ready for drawing.
            if (m_FirstNewParticle != m_FirstFreeParticle)
            {
                AddNewParticlesToVertexBuffer();
            }

            // If there are any active m_Particles, draw them now!
            if (m_FirstActiveParticle != m_FirstFreeParticle)
            {
                SetParticleRenderStates(m_Device.RenderState);

                //// Set an effect parameter describing the viewport size. This is needed
                //// to convert particle sizes into screen space point sprite sizes.
                m_EffectViewportHeight.SetValue(m_Device.Viewport.Height);

                //// Set an effect parameter describing the current time. All the vertex
                //// shader particle animation is keyed off this value.
                m_EffectTime.SetValue(m_CurrentTime);

                //// Set the particle vertex buffer and vertex declaration.
                m_Device.Vertices[0].SetSource(m_VertexBuffer, 0,
                                             PointListPS_Vertex.SizeInBytes);

                m_Device.VertexDeclaration = m_VertexDeclaration;

                // Activate the particle effect.
                m_ParticleEffect.Begin();

                foreach (EffectPass pass in m_ParticleEffect.CurrentTechnique.Passes)
                {
                    pass.Begin();

                    if (m_FirstActiveParticle < m_FirstFreeParticle)
                    {
                        // If the active m_Particles are all in one consecutive range,
                        // we can draw them all in a single call.
                        m_Device.DrawPrimitives(PrimitiveType.PointList,
                                              m_FirstActiveParticle,
                                              m_FirstFreeParticle - m_FirstActiveParticle);
                    }
                    else
                    {
                        // If the active particle range wraps past the end of the queue
                        // back to the start, we must split them over two draw calls.
                        m_Device.DrawPrimitives(PrimitiveType.PointList,
                                              m_FirstActiveParticle,
                                              m_Particles.Length - m_FirstActiveParticle);

                        if (m_FirstFreeParticle > 0)
                        {
                            m_Device.DrawPrimitives(PrimitiveType.PointList,
                                                  0,
                                                  m_FirstFreeParticle);
                        }
                    }

                    pass.End();
                }

                m_ParticleEffect.End();

                //// Reset a couple of the more unusual renderstates that we changed,
                //// so as not to mess up any other subsequent drawing.
                m_Device.RenderState.PointSpriteEnable = false;
                m_Device.RenderState.DepthBufferWriteEnable = true;
            }

            m_DrawCounter++;
        }


        public override void AddParticle(Vector3 position, Vector3 velocity)
        {
            // Figure out where in the circular queue to allocate the new particle.
            int nextFreeParticle = m_FirstFreeParticle + 1;

            if (nextFreeParticle >= m_Particles.Length)
                nextFreeParticle = 0;

            // If there are no free m_Particles, we just have to give up.
            if (nextFreeParticle == m_FirstRetiredParticle)
                return;

            // Adjust the input velocity based on how much
            // this particle system wants to be affected by it.
            velocity *= m_Settings.EmitterVelocitySensitivity;

            // Add in some m_Random amount of horizontal velocity.
            float horizontalVelocity = MathHelper.Lerp(m_Settings.MinHorizontalVelocity,
                                                       m_Settings.MaxHorizontalVelocity,
                                                       (float)m_Random.NextDouble());

            double horizontalAngle = m_Random.NextDouble() * MathHelper.TwoPi;

            velocity.X += horizontalVelocity * (float)Math.Cos(horizontalAngle);
            velocity.Z += horizontalVelocity * (float)Math.Sin(horizontalAngle);

            // Add in some m_Random amount of vertical velocity.
            velocity.Y += MathHelper.Lerp(m_Settings.MinVerticalVelocity,
                                          m_Settings.MaxVerticalVelocity,
                                          (float)m_Random.NextDouble());

            // Choose four m_Random control values. These will be used by the vertex
            // shader to give each particle a different size, rotation, and color.
            Color randomValues = new Color((byte)m_Random.Next(255),
                                           (byte)m_Random.Next(255),
                                           (byte)m_Random.Next(255),
                                           (byte)m_Random.Next(255));

            // Fill in the particle vertex structure.
            m_Particles[m_FirstFreeParticle].Position = position;
            m_Particles[m_FirstFreeParticle].Velocity = velocity;
            m_Particles[m_FirstFreeParticle].Random = randomValues;
            m_Particles[m_FirstFreeParticle].Time = m_CurrentTime;

            m_ParticleAge[m_FirstFreeParticle] = m_CurrentTime;

            m_FirstFreeParticle = nextFreeParticle;
        }


        public override void AddParticle(ref Matrix locationRotateMatrix, float force)
        {
            m_AddParticleVelocity = m_DefaultParticleDirection * force;

            // Figure out where in the circular queue to allocate the new particle.
            int nextFreeParticle = m_FirstFreeParticle + 1;

            if (nextFreeParticle >= m_Particles.Length)
                nextFreeParticle = 0;

            // If there are no free m_Particles, we just have to give up.
            if (nextFreeParticle == m_FirstRetiredParticle)
                return;

            // Adjust the input velocity based on how much
            // this particle system wants to be affected by it.
            m_AddParticleVelocity *= m_Settings.EmitterVelocitySensitivity;

            // Add in some m_Random amount of horizontal velocity.
            float horizontalVelocity = MathHelper.Lerp(m_Settings.MinHorizontalVelocity,
                                                       m_Settings.MaxHorizontalVelocity,
                                                       (float)m_Random.NextDouble());

            double horizontalAngle = m_Random.NextDouble() * MathHelper.TwoPi;

            m_AddParticleVelocity.X += horizontalVelocity * (float)Math.Cos(horizontalAngle);
            m_AddParticleVelocity.Z += horizontalVelocity * (float)Math.Sin(horizontalAngle);

            // Add in some m_Random amount of vertical velocity.
            m_AddParticleVelocity.Y += MathHelper.Lerp(m_Settings.MinVerticalVelocity,
                                          m_Settings.MaxVerticalVelocity,
                                          (float)m_Random.NextDouble());

            // Choose four m_Random control values. These will be used by the vertex
            // shader to give each particle a different size, rotation, and color.
            Color randomValues = new Color((byte)m_Random.Next(255),
                                           (byte)m_Random.Next(255),
                                           (byte)m_Random.Next(255),
                                           (byte)m_Random.Next(255));

            force = m_AddParticleVelocity.Length();

            if (force > 0)
            {
                m_AddParticleVelocity.X /= force;
                m_AddParticleVelocity.Y /= force;
                m_AddParticleVelocity.Z /= force;
            }

            Vector3.TransformNormal(ref m_AddParticleVelocity, ref locationRotateMatrix, out m_AddParticleVelocity);
            m_AddParticleVelocity *= force;

            // Fill in the particle vertex structure.
            m_Particles[m_FirstFreeParticle].Position = locationRotateMatrix.Translation;
            m_Particles[m_FirstFreeParticle].Velocity = m_AddParticleVelocity;
            m_Particles[m_FirstFreeParticle].Random = randomValues;
            m_Particles[m_FirstFreeParticle].Time = m_CurrentTime;

            m_ParticleAge[m_FirstFreeParticle] = m_CurrentTime;

            m_FirstFreeParticle = nextFreeParticle;
        }


        public override void Dispose()
        {
            m_Texture.Dispose();
            m_Texture = null;
            m_VertexBuffer.Dispose();
            m_VertexBuffer = null;
            m_VertexDeclaration.Dispose();
            m_VertexDeclaration = null;
            m_ParticleEffect.Dispose();
            m_ParticleEffect = null;
            m_Particles = null;
            m_ParticleAge = null;
        }

    }

}
