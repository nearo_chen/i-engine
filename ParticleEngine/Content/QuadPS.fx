//-----------------------------------------------------------------------------
// QuadPS.fx
//-----------------------------------------------------------------------------


// Camera parameters.
float4x4 View;
float4x4 Projection;
float4x4 World;

// The current time, in seconds.
float CurrentTime;
float OldCurrentTime;

// Parameters describing how the particles animate.
float Duration;
float DurationRandomness;
float3 Gravity;
float EndVelocity;
float4 MinColor;
float4 MaxColor;

float3 CamPos;
float2 StartHeight;
float2 EndHeight;
float2 StartWidth;
float2 EndWidth;


// Particle texture and sampler.
texture Texture;

bool UseCurveAlpha;
bool DoNotChangeAlpha;
bool UseLocalWorld;

//*****************************************************************
float3 NormalVelocity;


sampler Sampler = sampler_state
{
    Texture = (Texture);
    
    MinFilter = None;
    MagFilter = None;
    MipFilter = None;
    
    AddressU = Clamp;
    AddressV = Clamp;
};



struct VertexShaderInput
{
    float3 Position : POSITION0;
    float3 Velocity : NORMAL0;
    float4 Random : COLOR0;
    float Time : TEXCOORD0;
    float2 TexCoord : TEXCOORD1;
};


struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
    float4 Color : COLOR0;
};


// Vertex shader helper for computing the position of a particle.
float3 ComputeParticlePosition(float3 position, float3 velocity,
                               float age, float normalizedAge)
{
    float startVelocity = length(velocity);

    // Work out how fast the particle should be moving at the end of its life,
    // by applying a constant scaling factor to its starting velocity.
    float endVelocity = startVelocity * EndVelocity;
    
    // Our particles have constant acceleration, so given a starting velocity
    // S and ending velocity E, at time T their velocity should be S + (E-S)*T.
    // The particle position is the sum of this velocity over the range 0 to T.
    // To compute the position directly, we must integrate the velocity
    // equation. Integrating S + (E-S)*T for T produces S*T + (E-S)*T*T/2.

    float velocityIntegral = startVelocity * normalizedAge +
                             (endVelocity - startVelocity) * normalizedAge *
                                                             normalizedAge / 2;
                                                                      
    position += normalize(velocity) * velocityIntegral * Duration;
    
    // Apply the gravitational force.
    position += Gravity * age * normalizedAge;
    		
	return position;
}


// Vertex shader helper for computing the size of a particle.
float ComputeParticleSize(float2 startRange, float2 endRange,
                          float randomValue, float normalizedAge)
{
    // Apply a random factor to make each particle a slightly different size.
    float currentStartRange = lerp(startRange.x, startRange.y, randomValue);
    float currentEndRange = lerp(endRange.x, endRange.y, randomValue);

    // Compute the actual size based on the age of the particle.
    return lerp(currentStartRange, currentEndRange, normalizedAge);
}


// Vertex shader helper for computing the color of a particle.
float4 ComputeParticleColor(float4 projectedPosition,
                            float4 randomValue, float normalizedAge)
{
    // Apply a random factor to make each particle a slightly different color.
    float4 color;
    
	color = lerp(MinColor, MaxColor, randomValue.b);
    
    // Fade the alpha based on the age of the particle. This curve is hard coded
    // to make the particle fade in fairly quickly, then fade out more slowly:
    // plot x*(1-x)*(1-x) for x=0:1 in a graphing program if you want to see what
    // this looks like. The 6.7 scaling factor normalizes the curve so the alpha
    // will reach all the way up to fully solid.
    
    if(DoNotChangeAlpha == false)
    {
		if(UseCurveAlpha)
			color.a *= normalizedAge * (1-normalizedAge) * (1-normalizedAge) * 16.7;
		else
			color.a *= (1 - normalizedAge);
	}
		        
    return color;
}


// Custom vertex shader animates particles entirely on the GPU.
VertexShaderOutput VertexShader(VertexShaderInput input)
{
    VertexShaderOutput output;
    
    // Compute the age of the particle.
    float age = CurrentTime - input.Time;
    
    // Apply a random factor to make different particles age at different rates.
    age *= 1 + input.Random.x * DurationRandomness;
    
    // Normalize the age into the range zero to one.
    float normalizedAge = saturate(age / Duration);
    
    float currentHeight = ComputeParticleSize(StartHeight, EndHeight, input.Random.y, normalizedAge);
    float currentWidth = ComputeParticleSize(StartWidth, EndWidth, input.Random.y, normalizedAge);
    float3 newPosition = ComputeParticlePosition(input.Position, input.Velocity, age, normalizedAge);
    
    age = OldCurrentTime - input.Time;
    age *= 1 + input.Random.x * DurationRandomness;
    normalizedAge = saturate(age / Duration);
    
    float3 oldPosition = ComputeParticlePosition(input.Position, input.Velocity, age, normalizedAge);
    
    /*===================================================*/
    /*這段程式會讓particle永遠面對鏡頭*/
    /*
    float3 eyeVector = mul(View, Projection)._m02_m12_m22;
    
    float3 sideVector = normalize(cross(eyeVector, cross(float3(-1,0,0),float3(0,0,-1))));
    float3 up = normalize(cross(sideVector,eyeVector));
        
    newPosition += (input.TexCoord.x - 0.5f) * sideVector * currentWidth;
    newPosition += (0.5f - input.TexCoord.y) * up * currentWidth;
    */
    
    
    /*===================================================*/
    /*這段程式會讓particle的點朝著行進方向做延伸*/
    
    NormalVelocity = normalize(newPosition - oldPosition);
    
    float3 tempVector = newPosition - CamPos;   
    float3 sideVector = normalize(cross(tempVector, NormalVelocity));
    
    //檢查目前延伸出去的點是否在出生點後面，不是的話才套用延伸的點位置
    tempVector = newPosition + (0.5f - input.TexCoord.y) * NormalVelocity * currentHeight;   
    
    //dot大於0表示在出生點的前方
    if(dot((tempVector - input.Position), NormalVelocity) > 0)
        newPosition = tempVector;

    newPosition += (input.TexCoord.x - 0.5f) * sideVector * currentWidth;
    if(UseLocalWorld)
		newPosition = mul(float4(newPosition, 1), World);
		
    /*===================================================*/
	
		
        
    output.Position = mul(mul(float4(newPosition, 1), View), Projection);                
    output.TexCoord = input.TexCoord;
    output.Color = ComputeParticleColor(output.Position, input.Random, normalizedAge);
        
    return output;
}


float4 PixelShader(VertexShaderOutput input) : COLOR0
{
/*
	float4 Color;
	 
	// Get the texel from ColorMapSampler using a modified texture coordinate. This
	// gets the texels at the neighbour texels and adds it to Color.
		   Color  = tex2D( Sampler, float2(input.TexCoord.x+BlurDistance, input.TexCoord.y+BlurDistance));
		   Color += tex2D( Sampler, float2(input.TexCoord.x-BlurDistance, input.TexCoord.y-BlurDistance));
		   Color += tex2D( Sampler, float2(input.TexCoord.x+BlurDistance, input.TexCoord.y-BlurDistance));
			Color += tex2D( Sampler, float2(input.TexCoord.x-BlurDistance, input.TexCoord.y+BlurDistance));
	// We need to devide the color with the amount of times we added
	// a color to it, in this case 4, to get the avg. color
	Color = Color / 4;
	 
	// returned the blurred color
	return Color;
	*/

    return tex2D(Sampler, input.TexCoord) * input.Color;
}


// Effect technique for drawing particles that can rotate. Requires shader 2.0.
technique RotatingParticles
{
    pass P0
    {
        VertexShader = compile vs_2_0 VertexShader();
        PixelShader = compile ps_2_0 PixelShader();
    }
}
