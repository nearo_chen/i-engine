﻿#region File Description
//-----------------------------------------------------------------------------
// ParticleSettings.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace ParticleEngine
{
    /// <summary>
    /// Settings class describes all the tweakable options used
    /// to control the appearance of a particle system.
    /// </summary>
    [Serializable]
    public class InputSettings
    {
        public ParticleSettings particleSettings = new ParticleSettings();

        public int ParticlesPerSecond = 50;

        public bool UseEmitter = false;

        public bool UseDirectionParticle = false;
    }
}
