﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using I_XNAUtility;


namespace ParticleEngine
{
    public struct ParticleLocationData
    {
        public Matrix WorldMatrix;
        public int emitterIndex;
    }


    public class ParticleSystemWithEmitter : IDisposable
    {
        ContentManager m_Content;

        GraphicsDevice m_Device;

        public InputSettings m_Settings;

        ParticleSystem m_ParticleSystem;

        ParticleEmitter[] m_ParticleEmitters;

        ParticleLocationData[] m_LocationData;

        float[] m_AddParticleTimeLeftOver;

        int m_LocationNum = 0;
        int m_LastLocationNum = 0;
        int m_MaxLocationNum = 10;


        public InputSettings GetInputSettings()
        {
            return m_Settings;
        }


        public ParticleSystemWithEmitter(ContentManager content, GraphicsDevice device, string settingsFileName, ParticleLocationData[] particleLocationData)
        {
            //string assemblyLocation = Assembly.GetExecutingAssembly().Location;
            //string relativePath = Path.GetDirectoryName(assemblyLocation);

            InputSettings settings = Utility.LoadXMLFile(settingsFileName, typeof(InputSettings), true) as InputSettings;
            Initialize(content, device, settings, particleLocationData);
        }


        public ParticleSystemWithEmitter(ContentManager content, GraphicsDevice device, InputSettings settings, ParticleLocationData[] particleLocationData)
        {
            Initialize(content, device, settings, particleLocationData);
        }


        public void Initialize(ContentManager content, GraphicsDevice device, InputSettings settings, ParticleLocationData[] particleLocationData)
        {
            m_Content = content;
            m_Device = device;

            m_Settings = settings;

            m_MaxLocationNum = m_LocationNum = particleLocationData.Length;

            m_LocationData = new ParticleLocationData[m_MaxLocationNum];

            if (m_Settings.UseDirectionParticle)
                m_ParticleSystem = new QuadPS(m_Content, m_Device, m_Settings.particleSettings);
            else
                m_ParticleSystem = new PointListPS(m_Content, m_Device, m_Settings.particleSettings);

            InitialAddParticleTimeLeftOver(m_MaxLocationNum);

            m_ParticleEmitters = new ParticleEmitter[m_MaxLocationNum];

            for (int a = 0; a < m_MaxLocationNum; a++)
            {
                m_LocationData[a].WorldMatrix = particleLocationData[a].WorldMatrix;

                m_ParticleEmitters[a] = new ParticleEmitter(m_ParticleSystem,
                    m_Settings.ParticlesPerSecond,
                    m_LocationData[a].WorldMatrix.Translation);
            }
        }


        void InitialAddParticleTimeLeftOver(int bornLocationNum)
        {
            m_AddParticleTimeLeftOver = new float[bornLocationNum];
        }


        public void Update(float elapsedSeconds)
        {
            //出生位置的數量大於0，就加particle到出生位置
            for (int a = 0; a < m_LocationNum; a++)
            {
                if (m_Settings.UseEmitter)
                    m_ParticleEmitters[m_LocationData[a].emitterIndex].Update(elapsedSeconds, m_LocationData[a].WorldMatrix.Translation);
                else
                {
                    m_AddParticleTimeLeftOver[a] += elapsedSeconds;

                    m_ParticleSystem.AddParticleByTime(ref m_LocationData[a].WorldMatrix,
                        m_Settings.particleSettings.StartVelocity,
                        m_Settings.ParticlesPerSecond,
                        ref m_AddParticleTimeLeftOver[a]);
                }
            }

            //不需要加particle時，設定讓Emitter在下次加particle前先更新出生位置
            if (m_LocationNum == 0 && m_LastLocationNum != 0)
            {
                for (int a = 0; a < m_ParticleEmitters.Length; a++)
                {
                    m_ParticleEmitters[a].m_bWaitForResetPosition = true;
                }
            }

            m_ParticleSystem.Update(elapsedSeconds);

            m_LastLocationNum = m_LocationNum;
        }


        public void Draw(ref Matrix View, ref Matrix projection, ref Vector3 camPos)
        {
            m_ParticleSystem.SetCamera(ref View, ref projection, ref camPos);
            m_ParticleSystem.Draw();
        }


        public void UpdateWorldLocationData(ParticleLocationData[] worldLocData, int locationNum)
        {
            m_LocationNum = locationNum;
            Array.ConstrainedCopy(worldLocData, 0, m_LocationData, 0, locationNum);
        }


        public void UpdateLocalLocationData(ParticleLocationData[] localLocData, Matrix world, int locationNum)
        {
            m_ParticleSystem.SetWorld(world);

            m_LocationNum = locationNum;
            Array.ConstrainedCopy(localLocData, 0, m_LocationData, 0, locationNum);
        }


        public void SePSWorldMatrix(ref Matrix World)
        {
            m_ParticleSystem.SetWorld(World);
        }


        public void ChangeParticleTexture(string XNBTextrueName)
        {
            m_ParticleSystem.SetTexture(Utility.Load_Texture2D(m_Device, m_Content, XNBTextrueName, false));
            m_Settings.particleSettings.TextureName = XNBTextrueName;
        }


        public void Dispose()
        {
            for (int a = 0; a < m_LocationNum; a++)
            {
                m_ParticleEmitters[a] = null;
            }

            m_ParticleSystem.Dispose();
            m_ParticleSystem = null;
        }

    }
}