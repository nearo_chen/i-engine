﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace ParticleEngine
{
    public abstract class ParticleSystem
    {
        protected int m_FirstActiveParticle;
        protected int m_FirstNewParticle;
        protected int m_FirstFreeParticle;
        protected int m_FirstRetiredParticle;

        int m_MaxParticles;
        protected int m_DrawCounter;

        float m_ParticleDuration;
        float m_TimeBetweenParticles = 0;
        protected float m_CurrentTime;  // Store the current time, in seconds.
        protected float m_OldCurrentTime; //用來計算實際的行進方向
        protected float[] m_ParticleAge;

        protected Vector3 m_AddParticleVelocity = Vector3.Zero;
        protected Vector3 m_DefaultParticleDirection = Vector3.Up;



        public ParticleSystem(float particleDuration, int maxParticles)
        {
            m_ParticleDuration = particleDuration;
            m_ParticleAge = new float[maxParticles];

            m_MaxParticles = maxParticles;
        }


        public void Update(float elapsedSeconds)
        {
            m_OldCurrentTime = m_CurrentTime;

            m_CurrentTime += elapsedSeconds;

            RetireActiveParticles();
            FreeRetiredParticles();

            // If we let our timer go on increasing for ever, it would eventually
            // run out of floating point precision, at which point the m_Particles
            // would render incorrectly. An easy way to prevent this is to notice
            // that the time value doesn't matter when no m_Particles are being drawn,
            // so we can reset it back to zero any time the active queue is empty.

            if (m_FirstActiveParticle == m_FirstFreeParticle)
                m_CurrentTime = 0;

            if (m_FirstRetiredParticle == m_FirstActiveParticle)
                m_DrawCounter = 0;
        }


        void RetireActiveParticles()
        {
            float particleDuration = m_ParticleDuration;

            while (m_FirstActiveParticle != m_FirstNewParticle)
            {
                // Is this particle old enough to retire?
                float particleAge = m_CurrentTime - m_ParticleAge[m_FirstActiveParticle];

                if (particleAge < m_ParticleDuration)
                    break;

                // Remember the time at which we retired this particle.
                m_ParticleAge[m_FirstActiveParticle] = m_DrawCounter;

                // Move the particle from the active to the retired queue.
                m_FirstActiveParticle++;

                if (m_FirstActiveParticle >= m_MaxParticles)
                    m_FirstActiveParticle = 0;
            }
        }


        void FreeRetiredParticles()
        {
            while (m_FirstRetiredParticle != m_FirstActiveParticle)
            {
                // Has this particle been unused long enough that
                // the GPU is sure to be finished with it?
                int age = m_DrawCounter - (int)m_ParticleAge[m_FirstActiveParticle];

                // The GPU is never supposed to get more than 2 frames behind the CPU.
                // We add 1 to that, just to be safe in case of buggy drivers that
                // might bend the rules and let the GPU get further behind.
                if (age < 3)
                    break;

                // Move the particle from the retired to the free queue.
                m_FirstRetiredParticle++;

                if (m_FirstRetiredParticle >= m_MaxParticles)
                    m_FirstRetiredParticle = 0;
            }
        }


        public void AddParticleByTime(Vector3 position, Vector3 velocity, int particlesPerSecond, ref float addParticleTimeLeftOver)
        {
            m_TimeBetweenParticles = 1.0f / particlesPerSecond;

            while (addParticleTimeLeftOver > m_TimeBetweenParticles)
            {
                addParticleTimeLeftOver -= m_TimeBetweenParticles;

                // Create the particle.
                AddParticle(position, velocity);
            }
        }


        public void AddParticleByTime(ref Matrix locationRotateMatrix, float force, int particlesPerSecond, ref float addParticleTimeLeftOver)
        {
            m_TimeBetweenParticles = 1.0f / particlesPerSecond;

            while (addParticleTimeLeftOver > m_TimeBetweenParticles)
            {
                addParticleTimeLeftOver -= m_TimeBetweenParticles;

                // Create the particle.
                AddParticle(ref locationRotateMatrix, force);
            }
        }


        public abstract void SetCamera(ref Matrix view, ref Matrix projection, ref Vector3 camPos);

        public abstract void SetWorld(Matrix world);

        public abstract void SetTexture(Texture2D texture);

        public abstract void Draw();

        public abstract void AddParticle(Vector3 position, Vector3 velocity);

        public abstract void AddParticle(ref Matrix locationRotateMatrix, float force);

        public abstract void Dispose();
    }
}
