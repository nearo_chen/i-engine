﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ParticleEngine
{
    public static class SettingWriter
    {
        static public void Write(string fileName, InputSettings settings)
        {
            FileStream saveFile = File.Open(fileName, FileMode.Create);
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(InputSettings));

            xmlSerializer.Serialize(saveFile, settings);
            saveFile.Close();
        }
    }
}