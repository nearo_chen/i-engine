﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ParticleEngine
{
    public static class SettingReader
    {
        static public InputSettings Read(string fileName)
        {
            InputSettings settings = new InputSettings();

            if (File.Exists(fileName))
            {
                FileStream loadedFile = File.Open(fileName, FileMode.Open);
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(InputSettings));
                settings = (InputSettings)xmlSerializer.Deserialize(loadedFile);

                loadedFile.Close();
            }

            return settings;
        }
    }
}